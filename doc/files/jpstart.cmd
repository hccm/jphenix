@echo off

rem 设置JDK根路径 %cd%
set JAVA_HOME=E:/programs/jdk1.7.0_45_x64

rem 网站根路径
set WEB_BASE_PATH=E:/javacodes/zhenghe/zh_jphenix/jp_server

rem 设置启动类 类路径
set CLASS_PATH=%WEB_BASE_PATH%/WEB-INF/lib/jphenix_sdk.jar

rem 设置内存大小 -Xms256M -Xmx256M 
set MEM_ARGS= -XX:PermSize=64m -XX:MaxPermSize=128m

rem 调试模式
rem 作为客户端 通常用来调试启动部分
rem set DEBUG_MODE=-Xdebug -Xrunjdwp:transport=dt_socket,address=localhost:8000,server=n,suspend=y

rem 作为服务端 通常用来调用业务部分
rem set DEBUG_MODE=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n

:restart
%JAVA_HOME%\bin\java %MEM_ARGS% %DEBUG_MODE% -Djava.awt.headless=true -classpath %CLASS_PATH% com.jphenix.one.boot.WebBootStartup 1 80
goto restart
pause