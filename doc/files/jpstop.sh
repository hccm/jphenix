#!/bin/SH
BIN_PATH="$(dirname "$(readlink -f "$0")")"
cd ${BIN_PATH}
FOLDER_NAME="jp_server"
PID_PATH="${BIN_PATH}/www/${FOLDER_NAME}/WEB-INF/lib/jphenix.pid"
PID_VALUE=$(cat ${PID_PATH})

echo $'\n\n\n'
echo "******************************************************************"
echo $'\n'
echo "**** Before Stop Process:[${PID_VALUE}] ****"
echo $'\n'
echo "ps -ef | grep ${FOLDER_NAME}"
ps -ef | grep ${FOLDER_NAME}
echo $'\n'
echo "*******************************************************************"
echo $'\n'
kill -9 ${PID_VALUE}
RETURN_VALUE="$?"
echo "**** Stop Proess:[${PID_VALUE}] Return:[${RETURN_VALUE}] ****"
echo "**** After Info ****"
echo "ps -ef | grep ${FOLDER_NAME}"
ps -ef | grep ${FOLDER_NAME}
echo $'\n'
echo "*******************************************************************"
echo $'\n\n\n'
