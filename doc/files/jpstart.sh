#!/bin/sh
BIN_PATH="$(dirname "$(readlink -f "$0")")"
cd ${BIN_PATH}
JAVA_HOME="${BIN_PATH}/server/java1.7"
CLASS_PATH="${BIN_PATH}/www/jp_server/WEB-INF/lib/jphenix_sdk.jar"
MEM_ARGS=" -XX:PermSize=64m -XX:MaxPermSize=128m"
while true ; do
${JAVA_HOME}/bin/java ${MEM_ARGS} -Djava.awt.headless=true -classpath ${CLASS_PATH} com.jphenix.one.boot.WebBootStartup 1 8341
RETURN_VALUE="$?"

echo $'\n\n\n'
echo "***** The Process Has Stopped Return:[${RETURN_VALUE}] *****"
echo $'\n\n\n'

if [ "$RETURN_VALUE" != "1" ] ; then
  exit 1;
fi
done
