#!/bin/SH
BIN_PATH="$(dirname "$(readlink -f "$0")")"
cd ${BIN_PATH}
JAVA_HOME="${BIN_PATH}/../../jdk1.7.0_75"
WEB_BASE_PATH="${BIN_PATH}/../../query_mobile_server"
CLASS_PATH="${WEB_BASE_PATH}/WEB-INF/lib/jphenix_sdk.jar"
MEM_ARGS=" -XX:PermSize=64m -XX:MaxPermSize=128m"
while true ; do
${JAVA_HOME}/bin/java ${MEM_ARGS} -Djava.awt.headless=true -classpath ${CLASS_PATH} com.jphenix.one.boot.Startup 1 org.apache.catalina.startup.Bootstrap ${BIN_PATH}:${BIN_PATH}/../common/lib:${BIN_PATH}/../server/lib:${WEB_BASE_PATH}/WEB-INF/classes start
RETURN_VALUE="$?"

echo $'\n\n\n'
echo "***** The Process Has Stopped Return:[${RETURN_VALUE}] *****"
echo $'\n\n\n'

if [ "$RETURN_VALUE" != "1" ] ; then
  exit 1;
fi
done
