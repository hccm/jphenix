@echo off

rem 设置JDK根路径 %cd%
set JAVA_HOME=E:/programs/jdk1.7.0_45_x64

@rem 设置启动类 类路径
set CLASS_PATH=%cd%\..\..\wechat_server\WEB-INF\lib\jphenix_sdk.jar

@rem 设置内存大小 -Xms256M -Xmx256M 
set MEM_ARGS= -XX:PermSize=64m -XX:MaxPermSize=128m

@rem 调试模式
@rem 作为客户端 通常用来调试启动部分
rem set DEBUG_MODE=-Xdebug -Xrunjdwp:transport=dt_socket,address=localhost:8000,server=n,suspend=y

@rem 作为服务端 通常用来调用业务部分
rem set DEBUG_MODE=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n

@rem 参数说明： 
@rem            第一个：1 从后续传入参数中读取信息（非配置文件中读取）
@rem            第二个：org.apache.catalina.startup.Bootstrap 启动目标类路径
@rem            第三个：%cd%/..lib 启动扫描类的文件夹，多个用分号分隔（linux用冒号分隔），这个路径是tomcat的lib文件夹
@rem            第四个：start 是tomcat的启动参数
:restart
%JAVA_HOME%\bin\java %MEM_ARGS% %DEBUG_MODE% -Djava.awt.headless=true -classpath %CLASS_PATH% com.jphenix.one.boot.Startup 1 org.apache.catalina.startup.Bootstrap %cd%;%cd%\..\lib start
rem goto restart
pause