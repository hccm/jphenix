/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月27日
 * V4.0
 */
package org.apache.tomcat;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解决：Tomcat中，避免扫描 .tld 文件的很操蛋的功能。
 * 
 * 扫描类型枚举类
 * 
 * @author MBG
 * 2018年6月27日
 */
@ClassInfo({"2018-06-27 17:04","tomcat-api.jar包中的 扫描类型枚举类"})
public enum JarScanType {
	
	  TLD,  PLUGGABILITY,  OTHER;
	  
	  JarScanType() {}
}
