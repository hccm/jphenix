/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月27日
 * V4.0
 */
package org.apache.tomcat;

import javax.servlet.ServletContext;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解决：Tomcat中，避免扫描 .tld 文件的很操蛋的功能。
 * 
 * 每次启动Tomcat时，为了支持JSP中的自定义标签库，要扫描所有Jar包中是否包含.tld文件
 * 这愚蠢的功能，咱用不上啊，故屏蔽之
 * 
 * @author MBG
 * 2018年6月27日
 */
@ClassInfo({"2018-06-27 17:04","tomcat-api.jar包中的接口"})
public interface JarScanner {

	/**
	 * 实现搜索方法
	 * @param paramJarScanType          搜索类型
	 * @param paramServletContext       程序上下文
	 * @param paramJarScannerCallback   回调类实例
	 * 2018年6月27日
	 * @author MBG
	 */
	void scan(JarScanType paramJarScanType, ServletContext paramServletContext, JarScannerCallback paramJarScannerCallback);
	  
	/**
	 * 获取搜索过滤器类实例
	 * @return 过滤器类实例
	 * 2018年6月27日
	 * @author MBG
	 */
	JarScanFilter getJarScanFilter();
	  
	/**
	 * 设置搜索过滤器类实例
	 * @param paramJarScanFilter 搜索过滤器类实例
	 * 2018年6月27日
	 * @author MBG
	 */
	void setJarScanFilter(JarScanFilter paramJarScanFilter);
}
