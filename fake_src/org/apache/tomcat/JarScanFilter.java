/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月27日
 * V4.0
 */
package org.apache.tomcat;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解决：Tomcat中，避免扫描 .tld 文件的很操蛋的功能。
 * 
 * 搜索过滤器
 * 
 * @author MBG
 * 2018年6月27日
 */
@ClassInfo({"2018-06-27 17:04","tomcat-api.jar包中的 搜索过滤器"})
public interface JarScanFilter {

	/**
	 * 无用方法
	 * @param paramJarScanType 搜索类型
	 * @param paramString      过滤参数
	 * @return                 是否有效
	 * 2018年6月27日
	 * @author MBG
	 */
	boolean check(JarScanType paramJarScanType, String paramString);
}
