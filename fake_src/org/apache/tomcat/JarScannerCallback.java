/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月27日
 * V4.0
 */
package org.apache.tomcat;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解决：Tomcat中，避免扫描 .tld 文件的很操蛋的功能。
 * 
 * 搜索回调处理接口
 * 
 * @author MBG
 * 2018年6月27日
 */
@ClassInfo({"2018-06-27 17:04","tomcat-api.jar包中的 搜索回调处理接口"})
public interface JarScannerCallback {

	/**
	 * 母鸡
	 * @param paramJarURLConnection 母鸡
	 * @param paramString           母鸡
	 * @param paramBoolean          母鸡
	 * @throws IOException          异常
	 * 2018年6月27日
	 * @author MBG
	 */
	void scan(JarURLConnection paramJarURLConnection, String paramString, boolean paramBoolean) throws IOException;
			  
	/**
	 * 搜索指定文件
	 * @param paramFile    母鸡
	 * @param paramString  母鸡
	 * @param paramBoolean 母鸡
	 * @throws IOException 异常
	 * 2018年6月27日
	 * @author MBG
	 */
	void scan(File paramFile, String paramString, boolean paramBoolean) throws IOException;
			  
	/**
	 * 搜索 WEB-INF/classes 
	 * @throws IOException 异常
	 * 2018年6月27日
	 * @author MBG
	 */
	void scanWebInfClasses() throws IOException;
}
