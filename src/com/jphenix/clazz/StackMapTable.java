/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>stack_map</code> attribute.
 *
 * <p>This is an entry in the attributes table of a Code attribute.
 * It was introduced by J2SE 6 for the verification by
 * typechecking.
 *
 * @see StackMap
 * @since 3.4
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","StackMapTable"})
public class StackMapTable extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"StackMapTable"</code>.
     */
    public static final String tag = "StackMapTable";

    /**
     * <code>Top_variable_info.tag</code>.
     */
    public static final int TOP = 0;

    /**
     * <code>Integer_variable_info.tag</code>.
     */
    public static final int INTEGER = 1;

    /**
     * <code>Float_variable_info.tag</code>.
     */
    public static final int FLOAT = 2;

    /**
     * <code>Double_variable_info.tag</code>.
     */
    public static final int DOUBLE = 3;

    /**
     * <code>Long_variable_info.tag</code>.
     */
    public static final int LONG = 4;

    /**
     * <code>Null_variable_info.tag</code>.
     */
    public static final int NULL = 5;

    /**
     * <code>UninitializedThis_variable_info.tag</code>.
     */
    public static final int THIS = 6;

    /**
     * <code>Object_variable_info.tag</code>.
     */
    public static final int OBJECT = 7;

    /**
     * <code>Uninitialized_variable_info.tag</code>.
     */
    public static final int UNINIT = 8;


    /**
     * Returns the tag of the type specified by the
     * descriptor.  This method returns <code>INTEGER</code>
     * unless the descriptor is either D (double), F (float),
     * J (long), L (class type), or [ (array).
     *
     * @param descriptor        the type descriptor.
     * @see Descriptor
     */
    public static int typeTagOf(char descriptor) {
        switch (descriptor) {
        case 'D' :
            return DOUBLE;
        case 'F' :
            return FLOAT;
        case 'J' :
            return LONG;
        case 'L' :
        case '[' :
            return OBJECT;
        // case 'V' :
        default :
            return INTEGER;
        }
    }
    

    /**
     * Constructs a <code>stack_map</code> attribute.
     */
    public StackMapTable(ConstPool cp, byte[] newInfo) {
        super(cp, tag, newInfo);
    }

    public StackMapTable(ConstPool cp, int name_id, DataInputStream in)
        throws IOException
    {
        super(cp, name_id, in);
    }
}
