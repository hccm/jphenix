/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import com.jphenix.standard.docs.ClassInfo;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * Constant pool table.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:53","Constant pool table."})
public final class ConstPool {
    
    LongVector items;
    int numOfItems;
    int thisClassInfo;
    HashMap<ConstInfo,ConstInfo> itemsCache;

    /**
     * <code>CONSTANT_Class</code>
     */
    public static final int CONST_Class = ClassInfoBean.tag;

    /**
     * <code>CONSTANT_Fieldref</code>
     */
    public static final int CONST_Fieldref = FieldrefInfo.tag;

    /**
     * <code>CONSTANT_Methodref</code>
     */
    public static final int CONST_Methodref = MethodrefInfo.tag;

    /**
     * <code>CONSTANT_InterfaceMethodref</code>
     */
    public static final int CONST_InterfaceMethodref
                                        = InterfaceMethodrefInfo.tag;

    /**
     * <code>CONSTANT_String</code>
     */
    public static final int CONST_String = StringInfo.tag;

    /**
     * <code>CONSTANT_Integer</code>
     */
    public static final int CONST_Integer = IntegerInfo.tag;

    /**
     * <code>CONSTANT_Float</code>
     */
    public static final int CONST_Float = FloatInfo.tag;

    /**
     * <code>CONSTANT_Long</code>
     */
    public static final int CONST_Long = LongInfo.tag;

    /**
     * <code>CONSTANT_Double</code>
     */
    public static final int CONST_Double = DoubleInfo.tag;

    /**
     * <code>CONSTANT_NameAndType</code>
     */
    public static final int CONST_NameAndType = NameAndTypeInfo.tag;

    /**
     * <code>CONSTANT_Utf8</code>
     */
    public static final int CONST_Utf8 = Utf8Info.tag;

    /**
     * <code>Cosnt_MethodHandle</code>
     */
    public static final int CONST_MethodHandle = MethodHandleInfo.tag;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_getField = 1;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_getStatic = 2;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_putField = 3;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_putStatic = 4;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_invokeVirtual = 5;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_invokeStatic = 6;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_invokeSpecial = 7;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_newInvokeSpecial = 8;

    /**
     * <code>reference_kind</code> of <code>CONSTANT_MethodHandle_info</code>.
     */
    public static final int REF_invokeInterface = 9;

    /**
     * Constructs a constant pool table.
     *
     * @param thisclass         the name of the class using this constant
     *                          pool table
     */
    public ConstPool(String thisclass) {
        items = new LongVector();
        itemsCache = null;
        numOfItems = 0;
        addItem0(null);          // index 0 is reserved by the JVM.
        thisClassInfo = addClassInfo(thisclass);
    }

    /**
     * Constructs a constant pool table from the given byte stream.
     *
     * @param in        byte stream.
     */
    public ConstPool(DataInputStream in) throws IOException {
        itemsCache = null;
        thisClassInfo = 0;
        /* read() initializes items and numOfItems, and do addItem(null).
         */
        read(in);
    }

    void prune() {
        itemsCache = null;
    }

    /**
     * Returns the number of entries in this table.
     */
    public int getSize() {
        return numOfItems;
    }

    /**
     * Returns the name of the class using this constant pool table.
     */
    public String getClassName() {
        return getClassInfo(thisClassInfo);
    }

    /**
     * Returns the index of <code>CONSTANT_Class_info</code> structure
     * specifying the class using this constant pool table.
     */
    public int getThisClassInfo() {
        return thisClassInfo;
    }

    void setThisClassInfo(int i) {
        thisClassInfo = i;
    }

    ConstInfo getItem(int n) {
        return items.elementAt(n);
    }

    /**
     * Returns the <code>tag</code> field of the constant pool table
     * entry at the given index.
     */
    public int getTag(int index) {
        return getItem(index).getTag();
    }

    /**
     * Reads <code>CONSTANT_Class_info</code> structure
     * at the given index.
     *
     * @return  a fully-qualified class or interface name specified
     *          by <code>name_index</code>.  If the type is an array
     *          type, this method returns an encoded name like
     *          <code>[Ljava.lang.Object;</code> (note that the separators
     *          are not slashes but dots).
     * @see //javassist.ClassPool#getCtClass(String)
     */
    public String getClassInfo(int index) {
        ClassInfoBean c = (ClassInfoBean)getItem(index);
        if (c == null) {
            return null;
        } else {
            return Descriptor.toJavaName(getUtf8Info(c.name));
        }
    }

    /**
     * Reads <code>CONSTANT_Class_info</code> structure
     * at the given index.
     *
     * @return  the descriptor of the type specified
     *          by <code>name_index</code>.
     * @see //javassist.ClassPool#getCtClass(String)
     * @since 3.15
     */
    public String getClassInfoByDescriptor(int index) {
        ClassInfoBean c = (ClassInfoBean)getItem(index);
        if (c == null) {
            return null;
        } else {
            String className = getUtf8Info(c.name);
            if (className.charAt(0) == '[') {
                return className;
            } else {
                return Descriptor.of(className);
            }
        }
    }

    /**
     * Reads the <code>name_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * at the given index.
     */
    public int getNameAndTypeName(int index) {
        NameAndTypeInfo ntinfo = (NameAndTypeInfo)getItem(index);
        return ntinfo.memberName;
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * at the given index.
     */
    public int getNameAndTypeDescriptor(int index) {
        NameAndTypeInfo ntinfo = (NameAndTypeInfo)getItem(index);
        return ntinfo.typeDescriptor;
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_Fieldref_info</code>,
     * <code>CONSTANT_Methodref_info</code>,
     * or <code>CONSTANT_Interfaceref_info</code>,
     * structure at the given index.
     *
     * @since 3.6
     */
    public int getMemberClass(int index) {
        MemberrefInfo minfo = (MemberrefInfo)getItem(index);
        return minfo.classIndex;
    }

    /**
     * Reads the <code>name_and_type_index</code> field of the
     * <code>CONSTANT_Fieldref_info</code>,
     * <code>CONSTANT_Methodref_info</code>,
     * or <code>CONSTANT_Interfaceref_info</code>,
     * structure at the given index.
     *
     * @since 3.6
     */
    public int getMemberNameAndType(int index) {
        MemberrefInfo minfo = (MemberrefInfo)getItem(index);
        return minfo.nameAndTypeIndex;
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_Fieldref_info</code> structure
     * at the given index.
     */
    public int getFieldrefClass(int index) {
        FieldrefInfo finfo = (FieldrefInfo)getItem(index);
        return finfo.classIndex;
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_Fieldref_info</code> structure
     * at the given index.
     *
     * @return the name of the class at that <code>class_index</code>.
     */
    public String getFieldrefClassName(int index) {
        FieldrefInfo f = (FieldrefInfo)getItem(index);
        if (f == null) {
            return null;
        } else {
            return getClassInfo(f.classIndex);
        }
    }

    /**
     * Reads the <code>name_and_type_index</code> field of the
     * <code>CONSTANT_Fieldref_info</code> structure
     * at the given index.
     */
    public int getFieldrefNameAndType(int index) {
        FieldrefInfo finfo = (FieldrefInfo)getItem(index);
        return finfo.nameAndTypeIndex;
    }

    /**
     * Reads the <code>name_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to a <code>CONSTANT_Fieldref_info</code>.
     * @return  the name of the field.
     */
    public String getFieldrefName(int index) {
        FieldrefInfo f = (FieldrefInfo)getItem(index);
        if (f == null) {
            return null;
        } else {
            NameAndTypeInfo n = (NameAndTypeInfo)getItem(f.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.memberName);
            }
        }
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to a <code>CONSTANT_Fieldref_info</code>.
     * @return  the type descriptor of the field.
     */
    public String getFieldrefType(int index) {
        FieldrefInfo f = (FieldrefInfo)getItem(index);
        if (f == null) {
            return null;
        } else {
            NameAndTypeInfo n = (NameAndTypeInfo)getItem(f.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.typeDescriptor);
            }
        }
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_Methodref_info</code> structure
     * at the given index.
     */
    public int getMethodrefClass(int index) {
        MethodrefInfo minfo = (MethodrefInfo)getItem(index);
        return minfo.classIndex;
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_Methodref_info</code> structure
     * at the given index.
     *
     * @return the name of the class at that <code>class_index</code>.
     */
    public String getMethodrefClassName(int index) {
        MethodrefInfo minfo = (MethodrefInfo)getItem(index);
        if (minfo == null) {
            return null;
        } else {
            return getClassInfo(minfo.classIndex);
        }
    }

    /**
     * Reads the <code>name_and_type_index</code> field of the
     * <code>CONSTANT_Methodref_info</code> structure
     * at the given index.
     */
    public int getMethodrefNameAndType(int index) {
        MethodrefInfo minfo = (MethodrefInfo)getItem(index);
        return minfo.nameAndTypeIndex;
    }

    /**
     * Reads the <code>name_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to a <code>CONSTANT_Methodref_info</code>.
     * @return  the name of the method.
     */
    public String getMethodrefName(int index) {
        MethodrefInfo minfo = (MethodrefInfo)getItem(index);
        if (minfo == null) {
            return null;
        } else {
            NameAndTypeInfo n
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.memberName);
            }
        }
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to a <code>CONSTANT_Methodref_info</code>.
     * @return  the descriptor of the method.
     */
    public String getMethodrefType(int index) {
        MethodrefInfo minfo = (MethodrefInfo)getItem(index);
        if (minfo == null) {
            return null;
        } else {
            NameAndTypeInfo n
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.typeDescriptor);
            }
        }
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_InterfaceMethodref_info</code> structure
     * at the given index.
     */
    public int getInterfaceMethodrefClass(int index) {
        InterfaceMethodrefInfo minfo
            = (InterfaceMethodrefInfo)getItem(index);
        return minfo.classIndex;
    }

    /**
     * Reads the <code>class_index</code> field of the
     * <code>CONSTANT_InterfaceMethodref_info</code> structure
     * at the given index.
     *
     * @return the name of the class at that <code>class_index</code>.
     */
    public String getInterfaceMethodrefClassName(int index) {
        InterfaceMethodrefInfo minfo
            = (InterfaceMethodrefInfo)getItem(index);
        return getClassInfo(minfo.classIndex);
    }

    /**
     * Reads the <code>name_and_type_index</code> field of the
     * <code>CONSTANT_InterfaceMethodref_info</code> structure
     * at the given index.
     */
    public int getInterfaceMethodrefNameAndType(int index) {
        InterfaceMethodrefInfo minfo
            = (InterfaceMethodrefInfo)getItem(index);
        return minfo.nameAndTypeIndex;
    }

    /**
     * Reads the <code>name_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to
     *                  a <code>CONSTANT_InterfaceMethodref_info</code>.
     * @return  the name of the method.
     */
    public String getInterfaceMethodrefName(int index) {
        InterfaceMethodrefInfo minfo
            = (InterfaceMethodrefInfo)getItem(index);
        if (minfo == null) {
            return null;
        } else {
            NameAndTypeInfo n
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.memberName);
            }
        }
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to
     *                  a <code>CONSTANT_InterfaceMethodref_info</code>.
     * @return  the descriptor of the method.
     */
    public String getInterfaceMethodrefType(int index) {
        InterfaceMethodrefInfo minfo
            = (InterfaceMethodrefInfo)getItem(index);
        if (minfo == null) {
            return null;
        } else {
            NameAndTypeInfo n
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.typeDescriptor);
            }
        }
    }
    /**
     * Reads <code>CONSTANT_Integer_info</code>, <code>_Float_info</code>,
     * <code>_Long_info</code>, <code>_Double_info</code>, or
     * <code>_String_info</code> structure.
     * These are used with the LDC instruction.
     *
     * @return a <code>String</code> value or a wrapped primitive-type
     * value.
     */
    public Object getLdcValue(int index) {
        ConstInfo constInfo = this.getItem(index);
        Object value = null;
        if (constInfo instanceof StringInfo) {
            value = this.getStringInfo(index);
        } else if (constInfo instanceof FloatInfo) {
            value = new Float(getFloatInfo(index));
        } else if (constInfo instanceof IntegerInfo) {
            value = new Integer(getIntegerInfo(index));
        } else if (constInfo instanceof LongInfo) {
            value = new Long(getLongInfo(index));
        } else if (constInfo instanceof DoubleInfo) {
            value = new Double(getDoubleInfo(index));
        } else {
            value = null;
        }

        return value;
    }

    /**
     * Reads <code>CONSTANT_Integer_info</code> structure
     * at the given index.
     *
     * @return the value specified by this entry.
     */
    public int getIntegerInfo(int index) {
        IntegerInfo i = (IntegerInfo)getItem(index);
        return i.value;
    }

    /**
     * Reads <code>CONSTANT_Float_info</code> structure
     * at the given index.
     *
     * @return the value specified by this entry.
     */
    public float getFloatInfo(int index) {
        FloatInfo i = (FloatInfo)getItem(index);
        return i.value;
    }

    /**
     * Reads <code>CONSTANT_Long_info</code> structure
     * at the given index.
     *
     * @return the value specified by this entry.
     */
    public long getLongInfo(int index) {
        LongInfo i = (LongInfo)getItem(index);
        return i.value;
    }

    /**
     * Reads <code>CONSTANT_Double_info</code> structure
     * at the given index.
     *
     * @return the value specified by this entry.
     */
    public double getDoubleInfo(int index) {
        DoubleInfo i = (DoubleInfo)getItem(index);
        return i.value;
    }

    /**
     * Reads <code>CONSTANT_String_info</code> structure
     * at the given index.
     *
     * @return the string specified by <code>string_index</code>.
     */
    public String getStringInfo(int index) {
        StringInfo si = (StringInfo)getItem(index);
        return getUtf8Info(si.string);
    }

    /**
     * Reads <code>CONSTANT_utf8_info</code> structure
     * at the given index.
     *
     * @return the string specified by this entry.
     */
    public String getUtf8Info(int index) {
        Utf8Info utf = (Utf8Info)getItem(index);
        return utf.string;
    }

    /**
     * Reads the <code>reference_kind</code> field of the
     * <code>CONSTANT_MethodHandle_info</code> structure
     * at the given index.
     *
     * @see #REF_getField
     * @see #REF_getStatic
     * @see #REF_invokeInterface
     * @see #REF_invokeSpecial
     * @see #REF_invokeStatic
     * @see #REF_invokeVirtual
     * @see #REF_newInvokeSpecial
     * @see #REF_putField
     * @see #REF_putStatic
     * @since 3.17
     */
    public int getMethodHandleKind(int index) {
        MethodHandleInfo mhinfo = (MethodHandleInfo)getItem(index);
        return mhinfo.refKind;
    }

    /**
     * Reads the <code>reference_index</code> field of the
     * <code>CONSTANT_MethodHandle_info</code> structure
     * at the given index.
     *
     * @since 3.17
     */
    public int getMethodHandleIndex(int index) {
        MethodHandleInfo mhinfo = (MethodHandleInfo)getItem(index);
        return mhinfo.refIndex;
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_MethodType_info</code> structure
     * at the given index.
     *
     * @since 3.17
     */
    public int getMethodTypeInfo(int index) {
        MethodTypeInfo mtinfo = (MethodTypeInfo)getItem(index);
        return mtinfo.descriptor;
    }

    /**
     * Reads the <code>bootstrap_method_attr_index</code> field of the
     * <code>CONSTANT_InvokeDynamic_info</code> structure
     * at the given index.
     *
     * @since 3.17
     */
    public int getInvokeDynamicBootstrap(int index) {
        InvokeDynamicInfo iv = (InvokeDynamicInfo)getItem(index);
        return iv.bootstrap;
    }

    /**
     * Reads the <code>name_and_type_index</code> field of the
     * <code>CONSTANT_InvokeDynamic_info</code> structure
     * at the given index.
     *
     * @since 3.17
     */
    public int getInvokeDynamicNameAndType(int index) {
        InvokeDynamicInfo iv = (InvokeDynamicInfo)getItem(index);
        return iv.nameAndType;
    }

    /**
     * Reads the <code>descriptor_index</code> field of the
     * <code>CONSTANT_NameAndType_info</code> structure
     * indirectly specified by the given index.
     *
     * @param index     an index to a <code>CONSTANT_InvokeDynamic_info</code>.
     * @return  the descriptor of the method.
     * @since 3.17
     */
    public String getInvokeDynamicType(int index) {
        InvokeDynamicInfo iv = (InvokeDynamicInfo)getItem(index);
        if (iv == null) {
            return null;
        } else {
            NameAndTypeInfo n = (NameAndTypeInfo)getItem(iv.nameAndType);
            if(n == null) {
                return null;
            } else {
                return getUtf8Info(n.typeDescriptor);
            }
        }
    }

    /**
     * Determines whether <code>CONSTANT_Methodref_info</code>
     * structure at the given index represents the constructor
     * of the given class.
     *
     * @return          the <code>descriptor_index</code> specifying
     *                  the type descriptor of the that constructor.
     *                  If it is not that constructor,
     *                  <code>isConstructor()</code> returns 0.
     */
    public int isConstructor(String classname, int index) {
        return isMember(classname, MethodInfo.nameInit, index);
    }

    /**
     * Determines whether <code>CONSTANT_Methodref_info</code>,
     * <code>CONSTANT_Fieldref_info</code>, or
     * <code>CONSTANT_InterfaceMethodref_info</code> structure
     * at the given index represents the member with the specified
     * name and declaring class.
     *
     * @param classname         the class declaring the member
     * @param membername        the member name
     * @param index             the index into the constant pool table
     *
     * @return          the <code>descriptor_index</code> specifying
     *                  the type descriptor of that member.
     *                  If it is not that member,
     *                  <code>isMember()</code> returns 0.
     */
    public int isMember(String classname, String membername, int index) {
        MemberrefInfo minfo = (MemberrefInfo)getItem(index);
        if (getClassInfo(minfo.classIndex).equals(classname)) {
            NameAndTypeInfo ntinfo
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
            if (getUtf8Info(ntinfo.memberName).equals(membername)) {
                return ntinfo.typeDescriptor;
            }
        }

        return 0;       // false
    }

    /**
     * Determines whether <code>CONSTANT_Methodref_info</code>,
     * <code>CONSTANT_Fieldref_info</code>, or
     * <code>CONSTANT_InterfaceMethodref_info</code> structure
     * at the given index has the name and the descriptor
     * given as the arguments.
     *
     * @param membername        the member name
     * @param desc              the descriptor of the member.
     * @param index             the index into the constant pool table
     *
     * @return          the name of the target class specified by
     *                  the <code>..._info</code> structure
     *                  at <code>index</code>.
     *                  Otherwise, null if that structure does not 
     *                  match the given member name and descriptor.
     */
    public String eqMember(String membername, String desc, int index) {
        MemberrefInfo minfo = (MemberrefInfo)getItem(index);
        NameAndTypeInfo ntinfo
                = (NameAndTypeInfo)getItem(minfo.nameAndTypeIndex);
        if (getUtf8Info(ntinfo.memberName).equals(membername)
            && getUtf8Info(ntinfo.typeDescriptor).equals(desc)) {
            return getClassInfo(minfo.classIndex);
        } else {
            return null;       // false
        }
    }

    private int addItem0(ConstInfo info) {
        items.addElement(info);
        return numOfItems++;
    }

    private int addItem(ConstInfo info) {
        if (itemsCache == null) {
            itemsCache = makeItemsCache(items);
        }

        ConstInfo found = itemsCache.get(info);
        if (found != null) {
            return found.index;
        } else {
            items.addElement(info);
            itemsCache.put(info, info);
            return numOfItems++;
        }
    }

    int addConstInfoPadding() {
        return addItem0(new ConstInfoPadding(numOfItems));
    }


    /**
     * Adds a new <code>CONSTANT_Class_info</code> structure.
     *
     * <p>This also adds a <code>CONSTANT_Utf8_info</code> structure
     * for storing the class name.
     *
     * @param qname     a fully-qualified class name
     *                  (or the JVM-internal representation of that name).
     * @return          the index of the added entry.
     */
    public int addClassInfo(String qname) {
        int utf8 = addUtf8Info(Descriptor.toJvmName(qname));
        return addItem(new ClassInfoBean(utf8, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_NameAndType_info</code> structure.
     *
     * <p>This also adds <code>CONSTANT_Utf8_info</code> structures.
     *
     * @param name      <code>name_index</code>
     * @param type      <code>descriptor_index</code>
     * @return          the index of the added entry.
     */
    public int addNameAndTypeInfo(String name, String type) {
        return addNameAndTypeInfo(addUtf8Info(name), addUtf8Info(type));
    }

    /**
     * Adds a new <code>CONSTANT_NameAndType_info</code> structure.
     *
     * @param name      <code>name_index</code>
     * @param type      <code>descriptor_index</code>
     * @return          the index of the added entry.
     */
    public int addNameAndTypeInfo(int name, int type) {
        return addItem(new NameAndTypeInfo(name, type, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_Fieldref_info</code> structure.
     *
     * <p>This also adds a new <code>CONSTANT_NameAndType_info</code>
     * structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param name              <code>name_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @param type              <code>descriptor_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @return          the index of the added entry.
     */
    public int addFieldrefInfo(int classInfo, String name, String type) {
        int nt = addNameAndTypeInfo(name, type);
        return addFieldrefInfo(classInfo, nt);
    }

    /**
     * Adds a new <code>CONSTANT_Fieldref_info</code> structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param nameAndTypeInfo   <code>name_and_type_index</code>.
     * @return          the index of the added entry.
     */
    public int addFieldrefInfo(int classInfo, int nameAndTypeInfo) {
        return addItem(new FieldrefInfo(classInfo, nameAndTypeInfo, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_Methodref_info</code> structure.
     *
     * <p>This also adds a new <code>CONSTANT_NameAndType_info</code>
     * structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param name              <code>name_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @param type              <code>descriptor_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @return          the index of the added entry.
     */
    public int addMethodrefInfo(int classInfo, String name, String type) {
        int nt = addNameAndTypeInfo(name, type);
        return addMethodrefInfo(classInfo, nt);
    }

    /**
     * Adds a new <code>CONSTANT_Methodref_info</code> structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param nameAndTypeInfo   <code>name_and_type_index</code>.
     * @return          the index of the added entry.
     */
    public int addMethodrefInfo(int classInfo, int nameAndTypeInfo) {
         return addItem(new MethodrefInfo(classInfo, nameAndTypeInfo, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_InterfaceMethodref_info</code>
     * structure.
     *
     * <p>This also adds a new <code>CONSTANT_NameAndType_info</code>
     * structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param name              <code>name_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @param type              <code>descriptor_index</code>
     *                          of <code>CONSTANT_NameAndType_info</code>.
     * @return          the index of the added entry.
     */
    public int addInterfaceMethodrefInfo(int classInfo, String name,
                                         String type) {
        int nt = addNameAndTypeInfo(name, type);
        return addInterfaceMethodrefInfo(classInfo, nt);
    }

    /**
     * Adds a new <code>CONSTANT_InterfaceMethodref_info</code>
     * structure.
     *
     * @param classInfo         <code>class_index</code>
     * @param nameAndTypeInfo   <code>name_and_type_index</code>.
     * @return          the index of the added entry.
     */
    public int addInterfaceMethodrefInfo(int classInfo,
                                         int nameAndTypeInfo) {
        return addItem(new InterfaceMethodrefInfo(classInfo, nameAndTypeInfo,
                                                  numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_String_info</code>
     * structure.
     *
     * <p>This also adds a new <code>CONSTANT_Utf8_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addStringInfo(String str) {
        int utf = addUtf8Info(str);
        return addItem(new StringInfo(utf, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_Integer_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addIntegerInfo(int i) {
        return addItem(new IntegerInfo(i, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_Float_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addFloatInfo(float f) {
        return addItem(new FloatInfo(f, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_Long_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addLongInfo(long l) {
        int i = addItem(new LongInfo(l, numOfItems));
        if (i == numOfItems - 1)    // if not existing
        {
            addConstInfoPadding();
        }

        return i;
    }

    /**
     * Adds a new <code>CONSTANT_Double_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addDoubleInfo(double d) {
        int i = addItem(new DoubleInfo(d, numOfItems));
        if (i == numOfItems - 1)    // if not existing
        {
            addConstInfoPadding();
        }

        return i;
    }

    /**
     * Adds a new <code>CONSTANT_Utf8_info</code>
     * structure.
     *
     * @return          the index of the added entry.
     */
    public int addUtf8Info(String utf8) {
        return addItem(new Utf8Info(utf8, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_MethodHandle_info</code>
     * structure.
     *
     * @param kind      <code>reference_kind</code>
     *                  such as {@link #REF_invokeStatic <code>REF_invokeStatic</code>}.
     * @param index     <code>reference_index</code>.
     * @return          the index of the added entry.
     *
     * @since 3.17
     */
    public int addMethodHandleInfo(int kind, int index) {
        return addItem(new MethodHandleInfo(kind, index, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_MethodType_info</code>
     * structure.
     *
     * @param desc      <code>descriptor_index</code>.
     * @return          the index of the added entry.
     *
     * @since 3.17
     */
    public int addMethodTypeInfo(int desc) {
        return addItem(new MethodTypeInfo(desc, numOfItems));
    }

    /**
     * Adds a new <code>CONSTANT_InvokeDynamic_info</code>
     * structure.
     *
     * @param bootstrap     <code>bootstrap_method_attr_index</code>.
     * @param nameAndType   <code>name_and_type_index</code>.
     * @return          the index of the added entry.
     *
     * @since 3.17
     */
    public int addInvokeDynamicInfo(int bootstrap, int nameAndType) {
        return addItem(new InvokeDynamicInfo(bootstrap, nameAndType, numOfItems));
    }

    /**
     * Get all the class names.
     *
     * @return a set of class names (<code>String</code> objects).
     */
    public Set<String> getClassNames() {
        HashSet<String> result = new HashSet<String>();
        LongVector v = items;
        int size = numOfItems;
        for (int i = 1; i < size; ++i) {
            String className = v.elementAt(i).getClassName(this);
            if (className != null) {
                result.add(className);
            }
        }
        return result;
    }


    private void read(DataInputStream in) throws IOException {
        int n = in.readUnsignedShort();

        items = new LongVector(n);
        numOfItems = 0;
        addItem0(null);          // index 0 is reserved by the JVM.

        while (--n > 0) {       // index 0 is reserved by JVM
            int tag = readOne(in);
            if ((tag == LongInfo.tag) || (tag == DoubleInfo.tag)) {
                addConstInfoPadding();
                --n;
            }
        }
    }

    private static HashMap<ConstInfo,ConstInfo> makeItemsCache(LongVector items) {
        HashMap<ConstInfo,ConstInfo> cache = new HashMap<ConstInfo,ConstInfo>();
        int i = 1;
        while (true) {
            ConstInfo info = items.elementAt(i++);
            if (info == null) {
                break;
            } else {
                cache.put(info, info);
            }
        }

        return cache;
    }

    private int readOne(DataInputStream in) throws IOException {
        ConstInfo info;
        int tag = in.readUnsignedByte();
        switch (tag) {
        case Utf8Info.tag :                     // 1
            info = new Utf8Info(in, numOfItems);
            break;
        case IntegerInfo.tag :                  // 3
            info = new IntegerInfo(in, numOfItems);
            break;
        case FloatInfo.tag :                    // 4
            info = new FloatInfo(in, numOfItems);
            break;
        case LongInfo.tag :                     // 5
            info = new LongInfo(in, numOfItems);
            break;
        case DoubleInfo.tag :                   // 6
            info = new DoubleInfo(in, numOfItems);
            break;
        case ClassInfoBean.tag :                    // 7
            info = new ClassInfoBean(in, numOfItems);
            break;
        case StringInfo.tag :                   // 8
            info = new StringInfo(in, numOfItems);
            break;
        case FieldrefInfo.tag :                 // 9
            info = new FieldrefInfo(in, numOfItems);
            break;
        case MethodrefInfo.tag :                // 10
            info = new MethodrefInfo(in, numOfItems);
            break;
        case InterfaceMethodrefInfo.tag :       // 11
            info = new InterfaceMethodrefInfo(in, numOfItems);
            break;
        case NameAndTypeInfo.tag :              // 12
            info = new NameAndTypeInfo(in, numOfItems);
            break;
        case MethodHandleInfo.tag :             // 15
            info = new MethodHandleInfo(in, numOfItems);
            break;
        case MethodTypeInfo.tag :               // 16
            info = new MethodTypeInfo(in, numOfItems);
            break;
        case InvokeDynamicInfo.tag :            // 18
            info = new InvokeDynamicInfo(in, numOfItems);
            break;
        default :
            throw new IOException("invalid constant type: " + tag + " at " + numOfItems);
        }

        addItem0(info);
        return tag;
    }
}

abstract class ConstInfo {
    int index;
    public ConstInfo(int i) { index = i; }
    public abstract int getTag();
    public String getClassName(ConstPool cp) { return null; }
    @Override
    public String toString() {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        return bout.toString();
    }
}

/* padding following DoubleInfo or LongInfo.
 */
class ConstInfoPadding extends ConstInfo {
    public ConstInfoPadding(int i) { super(i); }

    @Override
    public int getTag() { return 0; }
}

class ClassInfoBean extends ConstInfo {
    static final int tag = 7;
    int name;

    public ClassInfoBean(int className, int index) {
        super(index);
        name = className;
    }

    public ClassInfoBean(DataInputStream in, int index) throws IOException {
        super(index);
        name = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return name; }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ClassInfoBean && ((ClassInfoBean)obj).name == name;
    }

    @Override
    public int getTag() { return tag; }

    @Override
    public String getClassName(ConstPool cp) {
        return cp.getUtf8Info(name);
    }
}

class NameAndTypeInfo extends ConstInfo {
    static final int tag = 12;
    int memberName;
    int typeDescriptor;

    public NameAndTypeInfo(int name, int type, int index) {
        super(index);
        memberName = name;
        typeDescriptor = type;
    }

    public NameAndTypeInfo(DataInputStream in, int index) throws IOException {
        super(index);
        memberName = in.readUnsignedShort();
        typeDescriptor = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return (memberName << 16) ^ typeDescriptor; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NameAndTypeInfo) {
            NameAndTypeInfo nti = (NameAndTypeInfo)obj;
            return nti.memberName == memberName && nti.typeDescriptor == typeDescriptor;
        }
        else {
            return false;
        }
    }
    @Override
    public int getTag() { return tag; }
}

abstract class MemberrefInfo extends ConstInfo {
    int classIndex;
    int nameAndTypeIndex;

    public MemberrefInfo(int cindex, int ntindex, int thisIndex) {
        super(thisIndex);
        classIndex = cindex;
        nameAndTypeIndex = ntindex;
    }

    public MemberrefInfo(DataInputStream in, int thisIndex) throws IOException {
        super(thisIndex);
        classIndex = in.readUnsignedShort();
        nameAndTypeIndex = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return (classIndex << 16) ^ nameAndTypeIndex; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MemberrefInfo) {
            MemberrefInfo mri = (MemberrefInfo)obj;
            return mri.classIndex == classIndex && mri.nameAndTypeIndex == nameAndTypeIndex
                   && mri.getClass() == this.getClass();
        }
        else {
            return false;
        }
    }
    public abstract String getTagName();
}

class FieldrefInfo extends MemberrefInfo {
    static final int tag = 9;

    public FieldrefInfo(int cindex, int ntindex, int thisIndex) {
        super(cindex, ntindex, thisIndex);
    }

    public FieldrefInfo(DataInputStream in, int thisIndex) throws IOException {
        super(in, thisIndex);
    }

    @Override
    public int getTag() { return tag; }

    @Override
    public String getTagName() { return "Field"; }

    protected int copy2(ConstPool dest, int cindex, int ntindex) {
        return dest.addFieldrefInfo(cindex, ntindex);
    }
}

class MethodrefInfo extends MemberrefInfo {
    static final int tag = 10;

    public MethodrefInfo(int cindex, int ntindex, int thisIndex) {
        super(cindex, ntindex, thisIndex);
    }

    public MethodrefInfo(DataInputStream in, int thisIndex) throws IOException {
        super(in, thisIndex);
    }

    @Override
    public int getTag() { return tag; }

    @Override
    public String getTagName() { return "Method"; }
}

class InterfaceMethodrefInfo extends MemberrefInfo {
    static final int tag = 11;

    public InterfaceMethodrefInfo(int cindex, int ntindex, int thisIndex) {
        super(cindex, ntindex, thisIndex);
    }

    public InterfaceMethodrefInfo(DataInputStream in, int thisIndex) throws IOException {
        super(in, thisIndex);
    }

    @Override
    public int getTag() { return tag; }

    @Override
    public String getTagName() { return "Interface"; }

    protected int copy2(ConstPool dest, int cindex, int ntindex) {
        return dest.addInterfaceMethodrefInfo(cindex, ntindex);
    }
}

class StringInfo extends ConstInfo {
    static final int tag = 8;
    int string;

    public StringInfo(int str, int index) {
        super(index);
        string = str;
    }

    public StringInfo(DataInputStream in, int index) throws IOException {
        super(index);
        string = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return string; }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof StringInfo && ((StringInfo)obj).string == string;
    }

    @Override
    public int getTag() { return tag; }
}

class IntegerInfo extends ConstInfo {
    static final int tag = 3;
    int value;

    public IntegerInfo(int v, int index) {
        super(index);
        value = v;
    }

    public IntegerInfo(DataInputStream in, int index) throws IOException {
        super(index);
        value = in.readInt();
    }

    @Override
    public int hashCode() { return value; }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof IntegerInfo && ((IntegerInfo)obj).value == value;
    }

    @Override
    public int getTag() { return tag; }
}

class FloatInfo extends ConstInfo {
    static final int tag = 4;
    float value;

    public FloatInfo(float f, int index) {
        super(index);
        value = f;
    }

    public FloatInfo(DataInputStream in, int index) throws IOException {
        super(index);
        value = in.readFloat();
    }

    @Override
    public int hashCode() { return Float.floatToIntBits(value); }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof FloatInfo && ((FloatInfo)obj).value == value;
    }

    @Override
    public int getTag() { return tag; }
}

class LongInfo extends ConstInfo {
    static final int tag = 5;
    long value;

    public LongInfo(long l, int index) {
        super(index);
        value = l;
    }

    public LongInfo(DataInputStream in, int index) throws IOException {
        super(index);
        value = in.readLong();
    }

    @Override
    public int hashCode() { return (int)(value ^ (value >>> 32)); }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof LongInfo && ((LongInfo)obj).value == value;
    }

    @Override
    public int getTag() { return tag; }
}

class DoubleInfo extends ConstInfo {
    static final int tag = 6;
    double value;

    public DoubleInfo(double d, int index) {
        super(index);
        value = d;
    }

    public DoubleInfo(DataInputStream in, int index) throws IOException {
        super(index);
        value = in.readDouble();
    }

    @Override
    public int hashCode() {
        long v = Double.doubleToLongBits(value);
        return (int)(v ^ (v >>> 32));
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DoubleInfo && ((DoubleInfo)obj).value == value;
    }

    @Override
    public int getTag() { return tag; }
}

class Utf8Info extends ConstInfo {
    static final int tag = 1;
    String string;

    public Utf8Info(String utf8, int index) {
        super(index);
        string = utf8;
    }

    public Utf8Info(DataInputStream in, int index) throws IOException {
        super(index);
        string = in.readUTF();
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Utf8Info && ((Utf8Info)obj).string.equals(string);
    }

    @Override
    public int getTag() { return tag; }
}

class MethodHandleInfo extends ConstInfo {
    static final int tag = 15;
    int refKind, refIndex;

    public MethodHandleInfo(int kind, int referenceIndex, int index) {
        super(index);
        refKind = kind;
        refIndex = referenceIndex;
    }

    public MethodHandleInfo(DataInputStream in, int index) throws IOException {
        super(index);
        refKind = in.readUnsignedByte();
        refIndex = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return (refKind << 16) ^ refIndex; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MethodHandleInfo) {
            MethodHandleInfo mh = (MethodHandleInfo)obj;
            return mh.refKind == refKind && mh.refIndex == refIndex; 
        }
        else {
            return false;
        }
    }

    @Override
    public int getTag() { return tag; }
}

class MethodTypeInfo extends ConstInfo {
    static final int tag = 16;
    int descriptor;

    public MethodTypeInfo(int desc, int index) {
        super(index);
        descriptor = desc;
    }

    public MethodTypeInfo(DataInputStream in, int index) throws IOException {
        super(index);
        descriptor = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return descriptor; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MethodTypeInfo) {
            return ((MethodTypeInfo)obj).descriptor == descriptor;
        } else {
            return false;
        }
    }

    @Override
    public int getTag() { return tag; }
}

class InvokeDynamicInfo extends ConstInfo {
    static final int tag = 18;
    int bootstrap, nameAndType;

    public InvokeDynamicInfo(int bootstrapMethod, int ntIndex, int index) {
        super(index);
        bootstrap = bootstrapMethod;
        nameAndType = ntIndex;
    }

    public InvokeDynamicInfo(DataInputStream in, int index) throws IOException {
        super(index);
        bootstrap = in.readUnsignedShort();
        nameAndType = in.readUnsignedShort();
    }

    @Override
    public int hashCode() { return (bootstrap << 16) ^ nameAndType; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof InvokeDynamicInfo) {
            InvokeDynamicInfo iv = (InvokeDynamicInfo)obj;
            return iv.bootstrap == bootstrap && iv.nameAndType == nameAndType;
        }
        else {
            return false;
        }
    }

    @Override
    public int getTag() { return tag; }
}
