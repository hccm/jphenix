/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 方法形态处理类
 * 
 * 2018-09-13 完善了代码格式
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","方法形态处理类"})
public class Modifier {
    
    public static final int PUBLIC       = AccessFlag.PUBLIC;
    public static final int PRIVATE      = AccessFlag.PRIVATE;
    public static final int PROTECTED    = AccessFlag.PROTECTED;
    public static final int STATIC       = AccessFlag.STATIC;
    public static final int FINAL        = AccessFlag.FINAL;
    public static final int SYNCHRONIZED = AccessFlag.SYNCHRONIZED;
    public static final int VOLATILE     = AccessFlag.VOLATILE;
    public static final int VARARGS      = AccessFlag.VARARGS;
    public static final int TRANSIENT    = AccessFlag.TRANSIENT;
    public static final int NATIVE       = AccessFlag.NATIVE;
    public static final int INTERFACE    = AccessFlag.INTERFACE;
    public static final int ABSTRACT     = AccessFlag.ABSTRACT;
    public static final int STRICT       = AccessFlag.STRICT;
    public static final int ANNOTATION   = AccessFlag.ANNOTATION;
    public static final int ENUM         = AccessFlag.ENUM;

    /**
     * 是否为公共类型
     */
    public static boolean isPublic(int mod) {
        return (mod & PUBLIC) != 0;
    }

    /**
     * 是否为私有类型
     */
    public static boolean isPrivate(int mod) {
        return (mod & PRIVATE) != 0;
    }

    /**
     * 是否为保护类型
     */
    public static boolean isProtected(int mod) {
        return (mod & PROTECTED) != 0;
    }

    /**
     * 是否为包对象
     */
    public static boolean isPackage(int mod) {
        return (mod & (PUBLIC | PRIVATE | PROTECTED)) == 0;
    }

    /**
     * 是否为静态
     */
    public static boolean isStatic(int mod) {
        return (mod & STATIC) != 0;
    }

    /**
     * 是否为最终态
     */
    public static boolean isFinal(int mod) {
        return (mod & FINAL) != 0;
    }

    /**
     * 是否为同步的
     */
    public static boolean isSynchronized(int mod) {
        return (mod & SYNCHRONIZED) != 0;
    }

    /**
     * 当写一个volatile变量时，JMM会把该线程对应的本地内存中的变量强制刷新到主内存中去
     */
    public static boolean isVolatile(int mod) {
        return (mod & VOLATILE) != 0;
    }

    /**
     * 一旦变量被transient修饰，变量将不再是对象持久化的一部分，该变量内容在序列化后无法获得访问。
     */
    public static boolean isTransient(int mod) {
        return (mod & TRANSIENT) != 0;
    }

    /**
     * 是否为内部的
     */
    public static boolean isNative(int mod) {
        return (mod & NATIVE) != 0;
    }

    /**
     * 是否为接口
     */
    public static boolean isInterface(int mod) {
        return (mod & INTERFACE) != 0;
    }

    /**
     * 是否为注解
     */
    public static boolean isAnnotation(int mod) {
        return (mod & ANNOTATION) != 0;
    }

    /**
     * 是否为枚举
     */
    public static boolean isEnum(int mod) {
        return (mod & ENUM) != 0;
    }

    /**
     * 是否为抽象
     */
    public static boolean isAbstract(int mod) {
        return (mod & ABSTRACT) != 0;
    }

    /**
     * strictfp 的意思是FP-strict，也就是说精确浮点的意思
     */
    public static boolean isStrict(int mod) {
        return (mod & STRICT) != 0;
    }

    /**
     * 覆盖方法
     */
    public static String toString(int mod) {
        return java.lang.reflect.Modifier.toString(mod);
    }
}
