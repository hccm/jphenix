/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>Synthetic_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:10","SyntheticAttribute"})
public class SyntheticAttribute extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"Synthetic"</code>.
     */
    public static final String tag = "Synthetic";

    SyntheticAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }

    /**
     * Constructs a Synthetic attribute.
     *
     * @param cp                a constant pool table.
     */
    public SyntheticAttribute(ConstPool cp) {
        super(cp, tag, new byte[0]);
    }
}
