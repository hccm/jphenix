/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>Deprecated_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","Deprecated_attribute"})
public class DeprecatedAttribute extends AttributeInfo {
    /**
     * The name of this attribute <code>"Deprecated"</code>.
     */
    public static final String tag = "Deprecated";

    protected DeprecatedAttribute(ConstPool cp, int n, DataInputStream in) throws IOException {
        super(cp, n, in);
    }

    /**
     * Constructs a Deprecated attribute.
     *
     * @param cp                a constant pool table.
     */
    public DeprecatedAttribute(ConstPool cp) {
        super(cp, tag, new byte[0]);
    }
}
