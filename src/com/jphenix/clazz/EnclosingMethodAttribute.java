/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>EnclosingMethod_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:56","EnclosingMethod_attribute"})
public class EnclosingMethodAttribute extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"EnclosingMethod"</code>.
     */
    public static final String tag = "EnclosingMethod";

    EnclosingMethodAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }


    /**
     * Returns the value of <code>class_index</code>.
     */
    public int classIndex() {
        return ByteArray.readU16bit(get(), 0);
    }

    /**
     * Returns the value of <code>method_index</code>.
     */
    public int methodIndex() {
        return ByteArray.readU16bit(get(), 2);
    }

    /**
     * Returns the name of the class specified by <code>class_index</code>.
     */
    public String className() {
        return getConstPool().getClassInfo(classIndex());
    }

    /**
     * Returns the method name specified by <code>method_index</code>.
     */
    public String methodName() {
        ConstPool cp = getConstPool();
        int mi = methodIndex();
        int ni = cp.getNameAndTypeName(mi);
        return cp.getUtf8Info(ni);
    }

    /**
     * Returns the method descriptor specified by <code>method_index</code>.
     */
    public String methodDescriptor() {
        ConstPool cp = getConstPool();
        int mi = methodIndex();
        int ti = cp.getNameAndTypeDescriptor(mi);
        return cp.getUtf8Info(ti);
    }
}
