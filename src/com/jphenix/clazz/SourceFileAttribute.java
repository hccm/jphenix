/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>SourceFile_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","SourceFile_attribute"})
public class SourceFileAttribute extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"SourceFile"</code>.
     */
    public static final String tag = "SourceFile";

    SourceFileAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }

    /**
     * Returns the file name indicated by <code>sourcefile_index</code>.
     */
    public String getFileName() {
        return getConstPool().getUtf8Info(ByteArray.readU16bit(get(), 0));
    }
}
