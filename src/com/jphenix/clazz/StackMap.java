/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Another <code>stack_map</code> attribute defined in CLDC 1.1 for J2ME.
 *
 * <p>This is an entry in the attributes table of a Code attribute.
 * It was introduced by J2ME CLDC 1.1 (JSR 139) for pre-verification.
 *
 * <p>According to the CLDC specification, the sizes of some fields are not 16bit
 * but 32bit if the code size is more than 64K or the number of the local variables
 * is more than 64K.  However, for the J2ME CLDC technology, they are always 16bit.
 * The implementation of the StackMap class assumes they are 16bit.  
 *
 * @see MethodInfo#doPreverify
 * @see StackMapTable
 * @since 3.12
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","StackMap"})
public class StackMap extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"StackMap"</code>.
     */
    public static final String tag = "StackMap";

    /**
     * <code>Top_variable_info.tag</code>.
     */
    public static final int TOP = 0;

    /**
     * <code>Integer_variable_info.tag</code>.
     */
    public static final int INTEGER = 1;

    /**
     * <code>Float_variable_info.tag</code>.
     */
    public static final int FLOAT = 2;

    /**
     * <code>Double_variable_info.tag</code>.
     */
    public static final int DOUBLE = 3;

    /**
     * <code>Long_variable_info.tag</code>.
     */
    public static final int LONG = 4;

    /**
     * <code>Null_variable_info.tag</code>.
     */
    public static final int NULL = 5;

    /**
     * <code>UninitializedThis_variable_info.tag</code>.
     */
    public static final int THIS = 6;

    /**
     * <code>Object_variable_info.tag</code>.
     */
    public static final int OBJECT = 7;

    /**
     * <code>Uninitialized_variable_info.tag</code>.
     */
    public static final int UNINIT = 8;
    
    /**
     * Constructs a <code>stack_map</code> attribute.
     */
    public StackMap(ConstPool cp, byte[] newInfo) {
        super(cp, tag, newInfo);
    }

    public StackMap(ConstPool cp, int name_id, DataInputStream in)  throws IOException {
        super(cp, name_id, in);
    }
}
