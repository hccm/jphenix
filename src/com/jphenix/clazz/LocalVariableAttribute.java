/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * <code>LocalVariableTable_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 20:01","LocalVariableTable_attribute"})
public class LocalVariableAttribute extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"LocalVariableTable"</code>.
     */
    public static final String tag = "LocalVariableTable";

    /**
     * The name of the attribute <code>"LocalVariableTypeTable"</code>.
     */
    public static final String typeTag = "LocalVariableTypeTable";


    LocalVariableAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }

    LocalVariableAttribute(ConstPool cp, String name, byte[] i) {
        super(cp, name, i);
    }


    /**
     * Returns <code>local_variable_table_length</code>.
     * This represents the number of entries in the table.
     */
    public int tableLength() {
        return ByteArray.readU16bit(info, 0);
    }

    /**
     * Returns <code>local_variable_table[i].start_pc</code>.
     * This represents the index into the code array from which the local
     * variable is effective.
     *
     * @param i         the i-th entry.
     */
    public int startPc(int i) {
        return ByteArray.readU16bit(info, i * 10 + 2);
    }

    /**
     * Returns <code>local_variable_table[i].length</code>.
     * This represents the length of the code region in which the local
     * variable is effective.
     *
     * @param i         the i-th entry.
     */
    public int codeLength(int i) {
        return ByteArray.readU16bit(info, i * 10 + 4);
    }


    /**
     * Returns the value of <code>local_variable_table[i].name_index</code>.
     * This represents the name of the local variable.
     *
     * @param i         the i-th entry.
     */
    public int nameIndex(int i) {
        return ByteArray.readU16bit(info, i * 10 + 6);
    }

    /**
     * Returns the name of the local variable
     * specified by <code>local_variable_table[i].name_index</code>.
     *
     * @param i         the i-th entry.
     */
    public String variableName(int i) {
        return getConstPool().getUtf8Info(nameIndex(i));
    }

    /**
     * Returns the value of
     * <code>local_variable_table[i].descriptor_index</code>.
     * This represents the type descriptor of the local variable.
     * <p>
     * If this attribute represents a LocalVariableTypeTable attribute,
     * this method returns the value of
     * <code>local_variable_type_table[i].signature_index</code>.
     * It represents the type of the local variable.
     *
     * @param i         the i-th entry.
     */
    public int descriptorIndex(int i) {
        return ByteArray.readU16bit(info, i * 10 + 8);
    }

    /**
     * This method is equivalent to <code>descriptorIndex()</code>.
     * If this attribute represents a LocalVariableTypeTable attribute,
     * this method should be used instead of <code>descriptorIndex()</code>
     * since the method name is more appropriate.
     * 
     * @param i         the i-th entry.
     * @see #descriptorIndex(int)
     * @see SignatureAttribute#toFieldSignature(String)
     */
    public int signatureIndex(int i) {
        return descriptorIndex(i);
    }

    /**
     * Returns the type descriptor of the local variable
     * specified by <code>local_variable_table[i].descriptor_index</code>.
     * <p>
     * If this attribute represents a LocalVariableTypeTable attribute,
     * this method returns the type signature of the local variable
     * specified by <code>local_variable_type_table[i].signature_index</code>.
      *
     * @param i         the i-th entry.
     */
    public String descriptor(int i) {
        return getConstPool().getUtf8Info(descriptorIndex(i));
    }

    /**
     * This method is equivalent to <code>descriptor()</code>.
     * If this attribute represents a LocalVariableTypeTable attribute,
     * this method should be used instead of <code>descriptor()</code>
     * since the method name is more appropriate.
     *
     * <p>To parse the string, call <code>toFieldSignature(String)</code>
     * in <code>SignatureAttribute</code>.
     *
     * @param i         the i-th entry.
     * @see #descriptor(int)
     * @see SignatureAttribute#toFieldSignature(String)
     */
    public String signature(int i) {
        return descriptor(i);
    }

    /**
     * Returns <code>local_variable_table[i].index</code>.
     * This represents the index of the local variable.
     *
     * @param i         the i-th entry.
     */
    public int index(int i) {
        return ByteArray.readU16bit(info, i * 10 + 10);
    }

    // LocalVariableTypeAttribute overrides this method.
    LocalVariableAttribute makeThisAttr(ConstPool cp, byte[] dest) {
        return new LocalVariableAttribute(cp, tag, dest);
    }
}
