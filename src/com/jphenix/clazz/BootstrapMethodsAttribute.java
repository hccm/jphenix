/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import java.io.DataInputStream;
import java.io.IOException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 构造属性信息
 * @author 马宝刚
 * 2014年6月10日
 */
@ClassInfo({"2014-06-10 19:46","构造属性信息"})
public class BootstrapMethodsAttribute extends AttributeInfo {
    /**
     * The name of this attribute <code>"BootstrapMethods"</code>.
     */
    public static final String tag = "BootstrapMethods";

    /**
     * An element of <code>bootstrap_methods</code>.
     */
    public static class BootstrapMethod {
        /**
         * Constructs an element of <code>bootstrap_methods</code>.
         *
         * @param method        <code>bootstrap_method_ref</code>.
         * @param args          <code>bootstrap_arguments</code>.
         */
        public BootstrapMethod(int method, int[] args) {
            methodRef = method;
            arguments = args;
        }

        /**
         * <code>bootstrap_method_ref</code>.
         * The value at this index must be a <code>CONSTANT_MethodHandle_info</code>.
         */
        public int methodRef;

        /**
         * <code>bootstrap_arguments</code>.
         */
        public int[] arguments;
    }

    BootstrapMethodsAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }


    /**
     * Obtains <code>bootstrap_methods</code> in this attribute.
     *
     * @return an array of <code>BootstrapMethod</code>.  Since it
     *          is a fresh copy, modifying the returned array does not
     *          affect the original contents of this attribute.
     */
    public BootstrapMethod[] getMethods() {
        byte[] data = this.get();
        int num = ByteArray.readU16bit(data, 0);
        BootstrapMethod[] methods = new BootstrapMethod[num];
        int pos = 2;
        for (int i = 0; i < num; i++) {
            int ref = ByteArray.readU16bit(data, pos);
            int len = ByteArray.readU16bit(data, pos + 2);
            int[] args = new int[len];
            pos += 4;
            for (int k = 0; k < len; k++) {
                args[k] = ByteArray.readU16bit(data, pos);
                pos += 2;
            }

            methods[i] = new BootstrapMethod(ref, args);
        }

        return methods;
    }
}
