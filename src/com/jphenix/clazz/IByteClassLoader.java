/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月21日
 * V4.0
 */
package com.jphenix.clazz;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 字节类加载器接口
 * @author 马宝刚
 * 2014年6月21日
 */
@ClassInfo({"2014-06-21 13:53","字节类加载器接口"})
public interface IByteClassLoader {

    /**
     * 加载指定类
     * @param className        类名
     * @param classBytes         类内容字节数组
     * @param reload              是否重新加载
     * @throws Exception        异常
     * @return                         类对象
     * 2014年6月21日
     * @author 马宝刚
     */
    Class<?> loadClass(String className,byte[] classBytes,boolean reload) throws Exception;
    
    
    /**
     * 加载指定类
     * @param className 类名
     * @param filePath 文件全路径
     * @return 加载的类
     * @throws Exception 异常
     * 2016年3月17日
     * @author 马宝刚
     */
    Class<?> loadClass(String className,String filePath) throws Exception;
    
    
    /**
     * 加载指定类
     * @param filePath 类文件路径
     * @return 类对象
     * @throws Exception 异常
     * 2014年6月21日
     * @author 马宝刚
     */
    Class<?> loadClassByPath(String filePath) throws Exception;
}
