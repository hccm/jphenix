/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import com.jphenix.standard.docs.ClassInfo;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * <code>InnerClasses_attribute</code>.
 * @author mabg
 */
@ClassInfo({"2014-06-10 19:55","InnerClasses_attribute"})
public class InnerClassesAttribute extends AttributeInfo {
    
    /**
     * The name of this attribute <code>"InnerClasses"</code>.
     */
    public static final String tag = "InnerClasses";

    InnerClassesAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }

    private InnerClassesAttribute(ConstPool cp, byte[] info) {
        super(cp, tag, info);
    }


    /**
     * Returns <code>number_of_classes</code>.
     */
    public int tableLength() { return ByteArray.readU16bit(get(), 0); }

    /**
     * Returns <code>classes[nth].inner_class_info_index</code>.
     */
    public int innerClassIndex(int nth) {
        return ByteArray.readU16bit(get(), nth * 8 + 2);
    }

    /**
     * Returns the class name indicated
     * by <code>classes[nth].inner_class_info_index</code>.
     *
     * @return null or the class name.
     */
    public String innerClass(int nth) {
        int i = innerClassIndex(nth);
        if (i == 0) {
            return null;
        } else {
            return constPool.getClassInfo(i);
        }
    }


    /**
     * Returns <code>classes[nth].outer_class_info_index</code>.
     */
    public int outerClassIndex(int nth) {
        return ByteArray.readU16bit(get(), nth * 8 + 4);
    }

    /**
     * Returns the class name indicated
     * by <code>classes[nth].outer_class_info_index</code>.
     *
     * @return null or the class name.
     */
    public String outerClass(int nth) {
        int i = outerClassIndex(nth);
        if (i == 0) {
            return null;
        } else {
            return constPool.getClassInfo(i);
        }
    }


    /**
     * Returns <code>classes[nth].inner_name_index</code>.
     */
    public int innerNameIndex(int nth) {
        return ByteArray.readU16bit(get(), nth * 8 + 6);
    }

    /**
     * Returns the simple class name indicated
     * by <code>classes[nth].inner_name_index</code>.
     *
     * @return null or the class name.
     */
    public String innerName(int nth) {
        int i = innerNameIndex(nth);
        if (i == 0) {
            return null;
        } else {
            return constPool.getUtf8Info(i);
        }
    }


    /**
     * Returns <code>classes[nth].inner_class_access_flags</code>.
     */
    public int accessFlags(int nth) {
        return ByteArray.readU16bit(get(), nth * 8 + 8);
    }
}
