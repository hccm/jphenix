/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-10
 * V4.0
 */
package com.jphenix.clazz;

import com.jphenix.standard.docs.ClassInfo;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * <code>LocalVariableTypeTable_attribute</code>.
 *
 * @since 3.11
 * @author mabg
 */
@ClassInfo({"2014-06-10 20:01","LocalVariableTypeTable_attribute"})
public class LocalVariableTypeAttribute extends LocalVariableAttribute {
    
    /**
     * The name of the attribute <code>"LocalVariableTypeTable"</code>.
     */
    public static final String tag = LocalVariableAttribute.typeTag;


    LocalVariableTypeAttribute(ConstPool cp, int n, DataInputStream in)
        throws IOException
    {
        super(cp, n, in);
    }

    private LocalVariableTypeAttribute(ConstPool cp, byte[] dest) {
        super(cp, tag, dest);
    }

    @Override
    LocalVariableAttribute makeThisAttr(ConstPool cp, byte[] dest) {
        return new LocalVariableTypeAttribute(cp, dest);
    }
}
