/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.propertie;

import com.jphenix.driver.propertie.instanceb.XmlConfProp;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 配置文件工厂类
 * 
 * 2018-11-08 去掉了是否为调试模式传参（无用） 
 * 2020-08-06 去掉了配置文件接口，改为直接使用实体类
 * 
 * @author 刘虻
 * 2010-2-2 下午12:47:18
 */
@ClassInfo({"2020-08-06 15:11","配置文件工厂类"})
public class FConfProp {

	/**
	 * 获取配置文件管理类
	 * 刘虻
	 * 2010-2-2 下午12:50:31
	 * @param filePath 文件全路径
	 * @param bf 类加载器
	 * @return 配置文件管理类
	 * @throws Exception 异常
	 */
	public static XmlConfProp getConfProp(String filePath,IBeanFactory bf) throws Exception {
		//构建返回值
		XmlConfProp xcp = new XmlConfProp();
		if(bf!=null) {
			xcp.setBeanFactory(bf);
		}
		xcp.setFilePath(filePath);
		return xcp;
	}
	
	
	/**
	 * 构造新的配置文件管理类
	 * @param bf 类加载器
	 * @return 新的配置文件管理类
	 * 2017年3月23日
	 * @author MBG
	 */
	public static XmlConfProp newInstance(IBeanFactory bf) {
		//构建返回值
		XmlConfProp xcp = new XmlConfProp();
		if(bf!=null) {
			xcp.setBeanFactory(bf);
		}
		return xcp;
	}
	
	
	
	/**
	 * 获取新的配置文件处理类实例
	 * 刘虻
	 * 2012-11-6 上午10:11:55
	 * @return 配置文件处理类实例
	 */
	public static XmlConfProp newInstance() {
		return new XmlConfProp();
	}
}
