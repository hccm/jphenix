/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月2日
 * V4.0
 */
package com.jphenix.driver.dbshell;

import com.jphenix.standard.docs.ClassInfo;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

/**
 * 允许底层数据处理事件类的调用事务处理类实例（壳）
 * @author MBG
 */
@ClassInfo({"2017-04-25 16:38","允许底层数据处理事件类的调用事务处理类实例（壳）"})
public class CallableStatementImpl extends PreparedStatementImpl implements CallableStatement {

    private CallableStatement kernel = null; //核心调用事务类
    
    /**
     * 构造函数
     * @param source 数据源主键
     * @param sql    处理语句
     * @param conn   连接类实例
     * @param kernel 真正的调用事务处理类
     * @param eVO    底层数据处理事件类实例容器
     */
    public CallableStatementImpl(String source,String sql,ConnectionImpl conn,CallableStatement kernel,EventVO eVO) {
        super(source,sql,conn,kernel,eVO);
        this.kernel = kernel;
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(int parameterIndex, int sqlType) throws SQLException {
        kernel.registerOutParameter(parameterIndex,sqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(int parameterIndex, int sqlType, int scale) throws SQLException {
        kernel.registerOutParameter(parameterIndex,sqlType,scale);
    }

    /**
     * 覆盖方法
     */
    @Override
    public boolean wasNull() throws SQLException {
        return kernel.wasNull();
    }

    /**
     * 覆盖方法
     */
    @Override
    public String getString(int parameterIndex) throws SQLException {
        return kernel.getString(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public boolean getBoolean(int parameterIndex) throws SQLException {
        return kernel.getBoolean(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public byte getByte(int parameterIndex) throws SQLException {
        return kernel.getByte(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public short getShort(int parameterIndex) throws SQLException {
        return kernel.getShort(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public int getInt(int parameterIndex) throws SQLException {
        return kernel.getInt(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public long getLong(int parameterIndex) throws SQLException {
        return kernel.getLong(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public float getFloat(int parameterIndex) throws SQLException {
        return kernel.getFloat(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public double getDouble(int parameterIndex) throws SQLException {
        return kernel.getDouble(parameterIndex);
    }

    /**
     * 覆盖方法
     * @deprecated
     */
    @Override
    public BigDecimal getBigDecimal(int parameterIndex, int scale) throws SQLException {
        return kernel.getBigDecimal(parameterIndex,scale);
    }

    /**
     * 覆盖方法
     */
    @Override
    public byte[] getBytes(int parameterIndex) throws SQLException {
        return kernel.getBytes(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Date getDate(int parameterIndex) throws SQLException {
        return kernel.getDate(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Time getTime(int parameterIndex) throws SQLException {
        return kernel.getTime(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Timestamp getTimestamp(int parameterIndex) throws SQLException {
        return kernel.getTimestamp(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Object getObject(int parameterIndex) throws SQLException {
        return kernel.getObject(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public BigDecimal getBigDecimal(int parameterIndex) throws SQLException {
        return kernel.getBigDecimal(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Object getObject(int parameterIndex, Map<String, Class<?>> map) throws SQLException {
        return kernel.getObject(parameterIndex,map);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Ref getRef(int parameterIndex) throws SQLException {
        return kernel.getRef(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Blob getBlob(int parameterIndex) throws SQLException {
        return kernel.getBlob(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Clob getClob(int parameterIndex) throws SQLException {
        return kernel.getClob(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Array getArray(int parameterIndex) throws SQLException {
        return kernel.getArray(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Date getDate(int parameterIndex, Calendar cal) throws SQLException {
        return kernel.getDate(parameterIndex,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Time getTime(int parameterIndex, Calendar cal) throws SQLException {
        return kernel.getTime(parameterIndex,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Timestamp getTimestamp(int parameterIndex, Calendar cal) throws SQLException {
        return kernel.getTimestamp(parameterIndex,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(int parameterIndex, int sqlType, String typeName) throws SQLException {
        kernel.registerOutParameter(parameterIndex,sqlType,typeName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(String parameterName, int sqlType) throws SQLException {
        kernel.registerOutParameter(parameterName,sqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(String parameterName, int sqlType, int scale) throws SQLException {
        kernel.registerOutParameter(parameterName,sqlType,scale);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void registerOutParameter(String parameterName, int sqlType, String typeName) throws SQLException {
        kernel.registerOutParameter(parameterName,sqlType,typeName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public URL getURL(int parameterIndex) throws SQLException {
        return kernel.getURL(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setURL(String parameterName, URL val) throws SQLException {
        kernel.setURL(parameterName,val);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNull(String parameterName, int sqlType) throws SQLException {
        kernel.setNull(parameterName,sqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBoolean(String parameterName, boolean x) throws SQLException {
        kernel.setBoolean(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setByte(String parameterName, byte x) throws SQLException {
        kernel.setByte(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setShort(String parameterName, short x) throws SQLException {
        kernel.setShort(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setInt(String parameterName, int x) throws SQLException {
        kernel.setInt(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setLong(String parameterName, long x) throws SQLException {
        kernel.setLong(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setFloat(String parameterName, float x) throws SQLException {
        kernel.setFloat(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDouble(String parameterName, double x) throws SQLException {
        kernel.setDouble(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBigDecimal(String parameterName, BigDecimal x) throws SQLException {
        kernel.setBigDecimal(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setString(String parameterName, String x) throws SQLException {
        kernel.setString(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBytes(String parameterName, byte[] x) throws SQLException {
        kernel.setBytes(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDate(String parameterName, Date x) throws SQLException {
        kernel.setDate(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTime(String parameterName, Time x) throws SQLException {
        kernel.setTime(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTimestamp(String parameterName, Timestamp x) throws SQLException {
        kernel.setTimestamp(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(String parameterName, InputStream x, int length) throws SQLException {
        kernel.setAsciiStream(parameterName,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(String parameterName, InputStream x, int length) throws SQLException {
        kernel.setBinaryStream(parameterName,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(String parameterName, Object x, int targetSqlType, int scale) throws SQLException {
        kernel.setObject(parameterName,x,targetSqlType,scale);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(String parameterName, Object x, int targetSqlType) throws SQLException {
        kernel.setObject(parameterName,x,targetSqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(String parameterName, Object x) throws SQLException {
        kernel.setObject(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(String parameterName, Reader reader, int length) throws SQLException {
        kernel.setCharacterStream(parameterName,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDate(String parameterName, Date x, Calendar cal) throws SQLException {
        kernel.setDate(parameterName,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTime(String parameterName, Time x, Calendar cal) throws SQLException {
        kernel.setTime(parameterName,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTimestamp(String parameterName, Timestamp x, Calendar cal) throws SQLException {
        kernel.setTimestamp(parameterName,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNull(String parameterName, int sqlType, String typeName) throws SQLException {
        kernel.setNull(parameterName,sqlType,typeName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public String getString(String parameterName) throws SQLException {
        return kernel.getString(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public boolean getBoolean(String parameterName) throws SQLException {
        return kernel.getBoolean(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public byte getByte(String parameterName) throws SQLException {
        return kernel.getByte(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public short getShort(String parameterName) throws SQLException {
        return kernel.getShort(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public int getInt(String parameterName) throws SQLException {
        return kernel.getInt(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public long getLong(String parameterName) throws SQLException {
        return kernel.getLong(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public float getFloat(String parameterName) throws SQLException {
        return kernel.getFloat(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public double getDouble(String parameterName) throws SQLException {
        return kernel.getDouble(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public byte[] getBytes(String parameterName) throws SQLException {
        return kernel.getBytes(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Date getDate(String parameterName) throws SQLException {
        return kernel.getDate(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Time getTime(String parameterName) throws SQLException {
        return kernel.getTime(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Timestamp getTimestamp(String parameterName) throws SQLException {
        return kernel.getTimestamp(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Object getObject(String parameterName) throws SQLException {
        return kernel.getObject(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public BigDecimal getBigDecimal(String parameterName) throws SQLException {
        return kernel.getBigDecimal(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Object getObject(String parameterName, Map<String, Class<?>> map) throws SQLException {
        return kernel.getObject(parameterName,map);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Ref getRef(String parameterName) throws SQLException {
        return kernel.getRef(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Blob getBlob(String parameterName) throws SQLException {
        return kernel.getBlob(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Clob getClob(String parameterName) throws SQLException {
        return kernel.getClob(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Array getArray(String parameterName) throws SQLException {
        return kernel.getArray(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Date getDate(String parameterName, Calendar cal) throws SQLException {
        return kernel.getDate(parameterName,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Time getTime(String parameterName, Calendar cal) throws SQLException {
        return kernel.getTime(parameterName,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Timestamp getTimestamp(String parameterName, Calendar cal) throws SQLException {
        return kernel.getTimestamp(parameterName,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public URL getURL(String parameterName) throws SQLException {
        return kernel.getURL(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public RowId getRowId(int parameterIndex) throws SQLException {
        return kernel.getRowId(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public RowId getRowId(String parameterName) throws SQLException {
        return kernel.getRowId(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setRowId(String parameterName, RowId x) throws SQLException {
        kernel.setRowId(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNString(String parameterName, String value) throws SQLException {
        kernel.setNString(parameterName,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNCharacterStream(String parameterName, Reader value, long length) throws SQLException {
        kernel.setNCharacterStream(parameterName,value,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(String parameterName, NClob value) throws SQLException {
        kernel.setNClob(parameterName,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(String parameterName, Reader reader, long length) throws SQLException {
        kernel.setClob(parameterName,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(String parameterName, InputStream inputStream, long length) throws SQLException {
        kernel.setBlob(parameterName,inputStream,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(String parameterName, Reader reader, long length) throws SQLException {
        kernel.setNClob(parameterName,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public NClob getNClob(int parameterIndex) throws SQLException {
        return kernel.getNClob(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public NClob getNClob(String parameterName) throws SQLException {
        return kernel.getNClob(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
        kernel.setSQLXML(parameterName,xmlObject);
    }

    /**
     * 覆盖方法
     */
    @Override
    public SQLXML getSQLXML(int parameterIndex) throws SQLException {
        return kernel.getSQLXML(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public SQLXML getSQLXML(String parameterName) throws SQLException {
        return kernel.getSQLXML(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public String getNString(int parameterIndex) throws SQLException {
        return kernel.getNString(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public String getNString(String parameterName) throws SQLException {
        return kernel.getNString(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Reader getNCharacterStream(int parameterIndex) throws SQLException {
        return kernel.getNCharacterStream(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Reader getNCharacterStream(String parameterName) throws SQLException {
        return kernel.getNCharacterStream(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Reader getCharacterStream(int parameterIndex) throws SQLException {
        return kernel.getCharacterStream(parameterIndex);
    }

    /**
     * 覆盖方法
     */
    @Override
    public Reader getCharacterStream(String parameterName) throws SQLException {
        return kernel.getCharacterStream(parameterName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(String parameterName, Blob x) throws SQLException {
        kernel.setBlob(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(String parameterName, Clob x) throws SQLException {
        kernel.setClob(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(String parameterName, InputStream x, long length) throws SQLException {
        kernel.setAsciiStream(parameterName,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(String parameterName, InputStream x, long length) throws SQLException {
        kernel.setBinaryStream(parameterName,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(String parameterName, Reader reader, long length) throws SQLException {
        kernel.setCharacterStream(parameterName,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(String parameterName, InputStream x) throws SQLException {
        kernel.setAsciiStream(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(String parameterName, InputStream x) throws SQLException {
        kernel.setBinaryStream(parameterName,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(String parameterName, Reader reader) throws SQLException {
        kernel.setCharacterStream(parameterName,reader);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNCharacterStream(String parameterName, Reader value) throws SQLException {
        kernel.setNCharacterStream(parameterName,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(String parameterName, Reader reader) throws SQLException {
        kernel.setClob(parameterName,reader);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(String parameterName, InputStream inputStream) throws SQLException {
        kernel.setBlob(parameterName,inputStream);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(String parameterName, Reader reader) throws SQLException {
        kernel.setNClob(parameterName,reader);
    }

	/**
	 * JDK1.7需要实现的方法
	 */
	@Override
    public void closeOnCompletion() throws SQLException {}

	/**
	 * JDK1.7需要实现的方法
	 */
	@Override
    public boolean isCloseOnCompletion() throws SQLException {
		return false;
	}

	/**
	 * JDK1.7需要实现的方法
	 */
	@Override
    public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
		return null;
	}

	/**
	 * JDK1.7需要实现的方法
	 */
	@Override
    public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
		return null;
	}
}
