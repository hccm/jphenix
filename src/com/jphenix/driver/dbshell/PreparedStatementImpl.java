/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月2日
 * V4.0
 */
package com.jphenix.driver.dbshell;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 允许底层数据处理事件类的处理后的事务处理类实例（壳）
 * @author MBG
 */
@ClassInfo({"2017-04-25 16:38","允许底层数据处理事件类的处理后的事务处理类实例（壳）"})
public class PreparedStatementImpl extends StatementImpl implements PreparedStatement {

    private PreparedStatement kernel = null; //核心事务类实例
    private String            sql    = null; //处理语句
    
    private Map<Integer,Object> propMap   = new HashMap<Integer,Object>(); //提交参数容器 key：参数索引 value：参数值
    private int                 maxIndex  = 0;                             //当前最大参数索引值
    
    /**
     * 构造函数
     * @param source 数据源主键
     * @param sql    处理语句
     * @param conn   连接类实例
     * @param kernel 核心事务类实例
     * @param eVO    底层数据处理事件类实例容器
     */
    public PreparedStatementImpl(String source,String sql,ConnectionImpl conn,PreparedStatement kernel,EventVO eVO) {
        super(source,conn,kernel,eVO);
        this.kernel = kernel;
        this.sql    = sql;
    }
    
    /**
     * 覆盖方法
     */
    @Override
    public ResultSet executeQuery() throws SQLException {
        //触发执行前事件
        Object eObj = eVO.beforeExecute(source,sql,maxIndex,propMap,this);
        ResultSet res = kernel.executeQuery();
        if(conn.autoCommit) {
        	eVO.afterExecute(source,eObj,true,-1);
        }else {
        	conn.dealInfoVec.add(new AfterDataVO(source,eObj,true,-1));
        }
        clearProp();
        return res;
    }

    /**
     * 覆盖方法
     */
    @Override
    public int executeUpdate() throws SQLException {
        //触发执行前事件
        Object eObj = eVO.beforeExecute(source,sql,maxIndex,propMap,this);
        int res = kernel.executeUpdate();
        if(conn.autoCommit) {
        	eVO.afterExecute(source,eObj,true,res);
        }else {
        	conn.dealInfoVec.add(new AfterDataVO(source,eObj,true,res));
        }
        clearProp();
        return res;
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,"");
        
        kernel.setNull(parameterIndex,sqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setBoolean(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setByte(int parameterIndex, byte x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setByte(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setShort(int parameterIndex, short x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setShort(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setInt(int parameterIndex, int x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setInt(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setLong(int parameterIndex, long x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setLong(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setFloat(int parameterIndex, float x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setFloat(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDouble(int parameterIndex, double x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setDouble(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setBigDecimal(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setString(int parameterIndex, String x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setString(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setBytes(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDate(int parameterIndex, Date x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setDate(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTime(int parameterIndex, Time x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setTime(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setTimestamp(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
        //不设置流对象到提交值对象中
        kernel.setAsciiStream(parameterIndex,x);
    }

    /**
     * 覆盖方法
     * @deprecated
     */
    @Override
    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setUnicodeStream(parameterIndex,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setBinaryStream(parameterIndex,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void clearParameters() throws SQLException {
        kernel.clearParameters();
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setObject(parameterIndex,x,targetSqlType);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(int parameterIndex, Object x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setObject(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public boolean execute() throws SQLException {
        //触发执行前事件
        Object eObj = eVO.beforeExecute(source,sql,maxIndex,propMap,this);
        boolean res = kernel.execute();
        if(conn.autoCommit) {
        	eVO.afterExecute(source,eObj,res,-1);
        }else {
        	conn.dealInfoVec.add(new AfterDataVO(source,eObj,res,-1));
        }
        clearProp();
        return res;
    }

    /**
     * 覆盖方法
     */
    @Override
    public void addBatch() throws SQLException {
        kernel.addBatch();
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setCharacterStream(parameterIndex,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setRef(int parameterIndex, Ref x) throws SQLException {
	//不设置对象到提交值对象中
        kernel.setRef(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(int parameterIndex, Blob x) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setBlob(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(int parameterIndex, Clob x) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setClob(parameterIndex,x);
    }
    
    /**
     * 覆盖方法
     */
    @Override
    public void setArray(int parameterIndex, Array x) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setArray(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        return kernel.getMetaData();
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setDate(parameterIndex,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setTime(parameterIndex,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setTimestamp(parameterIndex,x,cal);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,"");
        
        kernel.setNull(parameterIndex,sqlType,typeName);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setURL(int parameterIndex, URL x) throws SQLException {
        kernel.setURL(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        return kernel.getParameterMetaData();
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        kernel.setRowId(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,value);
        
        kernel.setNString(parameterIndex,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setNCharacterStream(parameterIndex,value,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setNClob(parameterIndex,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setClob(parameterIndex,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setBlob(parameterIndex,inputStream,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setNClob(parameterIndex,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        kernel.setSQLXML(parameterIndex,xmlObject);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
        if(parameterIndex>maxIndex) {
            maxIndex = parameterIndex;
        }
        propMap.put(parameterIndex,x);
        
        kernel.setObject(parameterIndex,x,targetSqlType,scaleOrLength);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setAsciiStream(parameterIndex,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setBinaryStream(parameterIndex,x,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setCharacterStream(parameterIndex,reader,length);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        //不设置流对象到提交值对象中
        kernel.setAsciiStream(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setBinaryStream(parameterIndex,x);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setCharacterStream(parameterIndex,reader);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
	//不设置流对象到提交值对象中
        kernel.setNCharacterStream(parameterIndex,value);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setClob(parameterIndex,reader);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setBlob(parameterIndex,inputStream);
    }

    /**
     * 覆盖方法
     */
    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
	//不设置大数据对象到提交值对象中
        kernel.setNClob(parameterIndex,reader);
    }
    
    /**
     * 清空提交值
     * 2018年5月4日
     * @author MBG
     */
    private void clearProp() {
        propMap.clear();
        maxIndex = 0;
    }
}
