/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月2日
 * V4.0
 */
package com.jphenix.driver.dbshell;

import com.jphenix.standard.docs.ClassInfo;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.Executor;

/**
 * 数据库连接对象
 * @author MBG
 */
@ClassInfo({"2017-04-25 16:38","数据库连接对象"})
public class ConnectionImpl implements Connection {

  private String     source = null; //数据源主键
  private Connection kernel = null; //核心数据库连接类实例
  private EventVO    eVO    = null; //底层数据处理事件类实例容器

  protected boolean autoCommit = false;                                  //是否自动提交
  protected Vector<AfterDataVO> dealInfoVec = new Vector<AfterDataVO>();  //在非自动提交模式中，保存执行数据事件信息序列


  /**
   * 构造函数
   * @param source 数据源主键
   * @param kernel 真正的数据库连接类实例
   * @param eVO    底层数据处理事件类实例容器
   */
  public ConnectionImpl(String source,Connection kernel,EventVO eVO) {
    super();
    this.source  = source;
    this.kernel  = kernel;
    this.eVO     = eVO;
  }

  /**
   * 覆盖方法
   */
  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    return kernel.unwrap(iface);
  }

  /**
   * 覆盖方法
   */
  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    return kernel.isWrapperFor(iface);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Statement createStatement() throws SQLException {
    return new StatementImpl(source,this,kernel.createStatement(),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql) throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public CallableStatement prepareCall(String sql) throws SQLException {
    return new CallableStatementImpl(source,sql,this,kernel.prepareCall(sql),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public String nativeSQL(String sql) throws SQLException {
    return kernel.nativeSQL(sql);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setAutoCommit(boolean autoCommit) throws SQLException {
    this.autoCommit = autoCommit;
    kernel.setAutoCommit(autoCommit);
  }

  /**
   * 覆盖方法
   */
  @Override
  public boolean getAutoCommit() throws SQLException {
    return kernel.getAutoCommit();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void commit() throws SQLException {
    kernel.commit();
    if(!autoCommit) {
      AfterDataVO adVO; //事件数据元素
      while(dealInfoVec.size()>0) {
        adVO = dealInfoVec.remove(0);
        eVO.afterExecute(adVO.source,adVO.paraObj,adVO.status,adVO.updateCount);
      }
    }
  }

  /**
   * 覆盖方法
   */
  @Override
  public void rollback() throws SQLException {
    kernel.rollback();
    if(!autoCommit) {
      AfterDataVO adVO; //事件数据元素
      while(dealInfoVec.size()>0) {
        adVO = dealInfoVec.remove(0);
        eVO.afterExecute(adVO.source,adVO.paraObj,false,adVO.updateCount);
      }
    }
  }

  /**
   * 覆盖方法
   */
  @Override
  public void close() throws SQLException {
    kernel.close();
  }

  /**
   * 覆盖方法
   */
  @Override
  public boolean isClosed() throws SQLException {
    return kernel.isClosed();
  }

  /**
   * 覆盖方法
   */
  @Override
  public DatabaseMetaData getMetaData() throws SQLException {
    return kernel.getMetaData();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setReadOnly(boolean readOnly) throws SQLException {
    kernel.setReadOnly(readOnly);
  }

  /**
   * 覆盖方法
   */
  @Override
  public boolean isReadOnly() throws SQLException {
    return kernel.isReadOnly();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setCatalog(String catalog) throws SQLException {
    kernel.setCatalog(catalog);
  }

  /**
   * 覆盖方法
   */
  @Override
  public String getCatalog() throws SQLException {
    return kernel.getCatalog();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setTransactionIsolation(int level) throws SQLException {
    kernel.setTransactionIsolation(level);
  }

  /**
   * 覆盖方法
   */
  @Override
  public int getTransactionIsolation() throws SQLException {
    return kernel.getTransactionIsolation();
  }

  /**
   * 覆盖方法
   */
  @Override
  public SQLWarning getWarnings() throws SQLException {
    return kernel.getWarnings();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void clearWarnings() throws SQLException {
    kernel.clearWarnings();
  }

  /**
   * 覆盖方法
   */
  @Override
  public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
    return new StatementImpl(source,this,kernel.createStatement(resultSetType,resultSetConcurrency),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
      throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql,resultSetType,resultSetConcurrency),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
    return new CallableStatementImpl(source,sql,this,kernel.prepareCall(sql,resultSetType,resultSetConcurrency),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Map<String, Class<?>> getTypeMap() throws SQLException {
    return kernel.getTypeMap();
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
    kernel.setTypeMap(map);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setHoldability(int holdability) throws SQLException {
    kernel.setHoldability(holdability);
  }

  /**
   * 覆盖方法
   */
  @Override
  public int getHoldability() throws SQLException {
    return kernel.getHoldability();
  }

  /**
   * 覆盖方法
   */
  @Override
  public Savepoint setSavepoint() throws SQLException {
    return kernel.setSavepoint();
  }

  /**
   * 覆盖方法
   */
  @Override
  public Savepoint setSavepoint(String name) throws SQLException {
    return kernel.setSavepoint(name);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void rollback(Savepoint savepoint) throws SQLException {
    kernel.rollback(savepoint);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void releaseSavepoint(Savepoint savepoint) throws SQLException {
    kernel.releaseSavepoint(savepoint);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
      throws SQLException {
    return new StatementImpl(source,this,kernel.createStatement(resultSetType,resultSetConcurrency,resultSetHoldability),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
      int resultSetHoldability) throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql,resultSetType,resultSetConcurrency,resultSetHoldability),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
      int resultSetHoldability) throws SQLException {
    return new CallableStatementImpl(source,sql,this,kernel.prepareCall(sql,resultSetType,resultSetConcurrency,resultSetHoldability),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql,autoGeneratedKeys),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql,columnIndexes),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
    return new PreparedStatementImpl(source,sql,this,kernel.prepareStatement(sql,columnNames),eVO);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Clob createClob() throws SQLException {
    return kernel.createClob();
  }

  /**
   * 覆盖方法
   */
  @Override
  public Blob createBlob() throws SQLException {
    return kernel.createBlob();
  }

  /**
   * 覆盖方法
   */
  @Override
  public NClob createNClob() throws SQLException {
    return kernel.createNClob();
  }

  /**
   * 覆盖方法
   */
  @Override
  public SQLXML createSQLXML() throws SQLException {
    return kernel.createSQLXML();
  }

  /**
   * 覆盖方法
   */
  @Override
  public boolean isValid(int timeout) throws SQLException {
    return kernel.isValid(timeout);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setClientInfo(String name, String value) throws SQLClientInfoException {
    kernel.setClientInfo(name,value);
  }

  /**
   * 覆盖方法
   */
  @Override
  public void setClientInfo(Properties properties) throws SQLClientInfoException {
    kernel.setClientInfo(properties);
  }

  /**
   * 覆盖方法
   */
  @Override
  public String getClientInfo(String name) throws SQLException {
    return kernel.getClientInfo(name);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Properties getClientInfo() throws SQLException {
    return kernel.getClientInfo();
  }

  /**
   * 覆盖方法
   */
  @Override
  public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
    return kernel.createArrayOf(typeName,elements);
  }

  /**
   * 覆盖方法
   */
  @Override
  public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
    return kernel.createStruct(typeName,attributes);
  }

  /**
   * JDK1.7需要实现的方法
   */
  @Override
  public void setSchema(String schema) throws SQLException {}

  /**
   * JDK1.7需要实现的方法
   */
  @Override
  public String getSchema() throws SQLException {
    return null;
  }

  /**
   * JDK1.7需要实现的方法
   */
  @Override
  public void abort(Executor executor) throws SQLException {}

  /**
   * JDK1.7需要实现的方法
   */
  @Override
  public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {}

  /**
   * JDK1.7需要实现的方法
   */
  @Override
  public int getNetworkTimeout() throws SQLException {
    return 0;
  }
}
