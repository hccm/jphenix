/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月2日
 * V4.0
 */
package com.jphenix.driver.dbshell;

import java.sql.Statement;
import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 底层数据处理事件接口
 * 
 * com.jphenix.driver.dbshell.IStatementEvent
 * 
 * @author MBG
 */
@ClassInfo({"2017-04-25 16:38","底层数据处理事件接口"})
public interface IStatementEvent {

    /**
     * 在执行更新语句前调用该方法
     * @param source         数据源主键
     * @param sql            更新语句
     * @param parameterCount 提交参数值数量
     * @param parameterMap   提交参数值容器 key：参数索引  value：参数值对象
     * @param stat           处理对象 可能为 Statement,PreparedStatement,CallableStatement
     * 
     * 如果为PreparedStatement,CallableStatement，可以通过 getParameterMetaData()
     * 方法获取到提交数据的值
     */
    Object beforeExecute(String source,String sql,int parameterCount,Map<Integer,Object> parameterMap,Statement stat);
    
    /**
     * 批量执行的语句调用前的方法
     * @param source       数据源主键
     * @param batchSqlList 需要批量执行的语句序列（执行该方法后会清空该序列）
     * @param stat         处理对象
     */
    Object beforeExecute(String source,List<String> batchSqlList,Statement stat);
    
    
    /**
     * 在执行语句后调用该方法
     * @param source      数据源主键
     * @param paramObj    在执行beforeExecute返回的对象（可以用来保存之前记录的数据）
     * @param status      执行结果  true成功  false无效
     * @param updateCount 更新记录数
     * 2018年5月2日
     * @author MBG
     */
    void afterExecute(String source,Object paramObj,boolean status,int updateCount);
}
