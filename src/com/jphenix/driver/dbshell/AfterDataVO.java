/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月14日
 * V4.0
 */
package com.jphenix.driver.dbshell;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 执行数据操作后的信息容器
 * @author MBG
 * 2018年5月14日
 */
@ClassInfo({"2017-05-14 14:02","执行数据操作后的信息容器"})
public class AfterDataVO {

	protected String source; 		//数据源主键
	protected Object paraObj;      //操作数据时相关的数据信息包对象（由数据事件处理类自定义）
	protected boolean status;     //执行结果 1成功 0无效
	protected int updateCount;     //影响了语句数量
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public AfterDataVO(String source,Object paraObj,boolean status,int updateCount) {
		super();
		this.source      = source;
		this.paraObj     = paraObj;
		this.status      = status;
		this.updateCount = updateCount;
	}
}
