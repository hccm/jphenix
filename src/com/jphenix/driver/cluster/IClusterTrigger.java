/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年10月8日
 * V4.0
 */
package com.jphenix.driver.cluster;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.standard.servlet.IResponse;

/**
 * 集群服务触发接口
 * @author MBG
 * 2019年10月8日
 */
@ClassInfo({"2019-10-08 14:36","集群服务触发接口"})
public interface IClusterTrigger {

	/**
	 * 集群管理类初始化后调用改方法
	 * @param cf 集群管理类
	 * 2017年3月29日
	 * @author MBG
	 */
	void clusterInited(ClusterFilter cf);
	
	
	/**
	 * 首次完成发送心跳到某个目标服务器后执行该方法
	 * @param siVO   目标服务器信息类
	 * 2019年10月8日
	 * @author MBG
	 */
	void validSended(ServerInfoVO siVO);
	
	
	/**
	 * 首次完成接收目标心跳后执行该方法
	 * @param siVO   发送方服务器信息类
	 * 2019年10月8日
	 * @author MBG
	 */
	void validReceived(ServerInfoVO siVO);
	
	/**
	 * 返回触发响应主键
	 * @return 触发响应主键
	 * 2019年10月8日
	 * @author MBG
	 */
	String triggerKey();

	/**
	 * 调用目标内部触发器 (调用的目标只能是当前同样的类）该方法在ClusterFilter(ICluster)中
	 * @param siVO    目标服务器信息类
	 * @param param   提交参数对象
	 * @param caller  调用者（当前类实例）
	 * @return
	 * 2019年10月8日
	 * @author MBG
	 * 
	 * Object triggerCall(ServerInfoVO siVO,Object param,IClusterTrigger caller);
	 */
	
	/**
	 * 响应发起服务器请求
	 * @param siVO   发起服务器信息类
	 * @param req    请求对象
	 * @param resp   反馈对象
	 * 2019年10月8日
	 * @author MBG
	 */
	void triggerRecive(ServerInfoVO siVO,IRequest req,IResponse resp);
}
