/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年11月24日
 * V4.0
 */
package com.jphenix.driver.cluster;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.ICluster;

/**
 * 集群处理类工厂
 * @author MBG
 * 2016年11月24日
 */
@ClassInfo({"2016-11-24 09:36","集群处理类工厂"})
public class FCluster {

	private static NoCluster noCluster = null; //非集群模式处理类
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public FCluster() {
		super();
	}

	/**
	 * 获取集群处理类
	 * （如果在非集群模式下，也能返回集群处理类，只针对本机有效）
	 * @param base 基类
	 * @return 集群处理类
	 * 2016年11月24日
	 * @author MBG
	 */
	public static ICluster cluster(ABase base) {
		if(base==null) {
			return null;
		}
		if(base.getBeanFactory().beanExists(ICluster.class)) {
			try {
				return base.getBean(ICluster.class);
			}catch(Exception e) {
				e.printStackTrace();
				if(noCluster==null) {
					noCluster = new NoCluster(base);
				}
				return noCluster;
			}
		}else {
			if(noCluster==null) {
				noCluster = new NoCluster(base);
			}
			return noCluster;
		}
	}
}