/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2021年03月10日
 * V4.0
 */
package com.jphenix.driver.regc.vo;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 动作请求VO
 */
@ClassInfo({"2021-03-10 19:27","动作请求VO"})
public class ActionVO {
  public String title             = null; //动作名标题
  public String actionPath        = null; //请求动作路径
  public String contextPath       = null; //动作上下文路径（虚拟路径）
  public String summaryId         = null; //动作小结主键（分组内分类）
  public String groupId           = null; //动作分组主键
  public String clusterServerId   = null; //集群服务器中，服务器主键
  public String clusterGroupId    = null; //集群服务器中，集群分组主键

  public String scriptId          = null; //需要调用的脚本代码
  public String sParameter        = null; //静态参数 key=value&key=value 格式

  public boolean isLocalServer    = true; //当前动作是否为本地动作

  //统计用
  public long   invokeCount       = 0;    //累计调用次数
  public long   invokeDayCount    = 0;    //本日调用次数
  public long   errorCount        = 0;    //累计出错次数
  public long   errorDayCount     = 0;    //本日出错次数
  public long   dayEndTime        = 0;    //当日结束时间
  public long   lastInvokeTime    = 0;    //上次调用时间
  public long   lastErrorTime     = 0;    //上次报错时间
  public long   lastInvokeUseTime = 0;    //上次执行耗费时间（毫秒）
  public String lastErrorMsg      = null; //上次报错内容
}
