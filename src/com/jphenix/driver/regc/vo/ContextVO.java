package com.jphenix.driver.regc.vo;

import java.util.HashMap;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 上下文（虚拟路径）VO
 */
@ClassInfo({"2021-03-12 18:48","上下文（虚拟路径）VO"})
public class ContextVO {
  public String               contextPath = null;            //动作上下文
  public Map<String,ActionVO> actionMap   = new HashMap<>(); //动作信息容器
  public boolean              localReg    = false;           //该动作是否为本地注册动作类
  public boolean              needProxy   = false;           //是否需要代理全部请求
}
