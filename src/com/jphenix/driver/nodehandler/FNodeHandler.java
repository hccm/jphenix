/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.nodehandler;

import java.io.File;
import java.io.InputStream;

import com.jphenix.driver.nodehandler.instancea.NodeHandler;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.INodeHandler;

/**
 * 节点处理类工厂
 * 
 * com.jphenix.driver.nodehandler.FNodeHandler
 * 
 * 2018-06-26 增加从读入流获取数据返回对象
 * 
 * @author 刘虻
 * 2010-6-30 上午11:21:22
 */
@ClassInfo({"2018-06-26 17:13","节点处理类工厂"})
public class FNodeHandler {

	/**
	 * 获取新的节点对象
	 * 刘虻
	 * 2010-6-30 上午11:23:00
	 * @param dealEncoding 编码
	 * @return 新的节点对象
	 */
	public static INodeHandler newNodeHandler(String dealEncoding) {
		//构建返回值
		NodeHandler nh = new NodeHandler();
		if(dealEncoding!=null) {
			nh.setDealEncode(dealEncoding);
		}else {
			nh.setDealEncode("UTF-8");
		}
		return nh;
	}
	
	
	/**
	 * 从读入流获取数据
	 * @param is  读入流对象
	 * @return    返回值
	 * 2018年6月26日
	 * @author MBG
	 */
	public static INodeHandler getNodeHandler(InputStream is) {
		//构建返回值
		NodeHandler nh = new NodeHandler();
		try {
			nh.setInputStream(is,"request");
		}catch(Exception e) {}
		return nh;
	}
	
	
	/**
	 * 获取新的节点对象
	 * 刘虻
	 * 2010-6-30 上午11:23:00
	 * @return 新的节点对象
	 */
	public static INodeHandler newNodeHandler() {
		return newNodeHandler(null);
	}
	
	/**
     * 获取新的节点对象
     * @param filePath 内容文件路径
     * @param encoding 内容编码
     * @return 节点对象
     * 2015年11月19日
     * @author 马宝刚
     */
    public static INodeHandler newPath(String filePath) {
        try {
            return newNodeHandler().setFilePath(filePath);
        }catch(Exception e) {
            e.printStackTrace();
            return newNodeHandler();
        }
    }
    
    /**
     * 获取新的节点对象
     * @param filePath 内容文件路径
     * @param encoding 内容编码
     * @return 节点对象
     * 2015年11月19日
     * @author 马宝刚
     */
    public static INodeHandler newPath(String filePath,String encoding) {
        try {
            return newNodeHandler(encoding).setFilePath(filePath);
        }catch(Exception e) {
            e.printStackTrace();
            return newNodeHandler(encoding);
        }
    }
	
	
	/**
     * 获取新的节点对象
     * @param file 内容文件对象
     * @param encoding 内容编码
     * @return 节点对象
     * 2015年11月19日
     * @author 马宝刚
     */
    public static INodeHandler newFile(File file) {
        try {
            return newNodeHandler().setFile(file);
        }catch(Exception e) {
            e.printStackTrace();
            return newNodeHandler();
        }
    }
    
	/**
	 * 获取新的节点对象
	 * @param file 内容文件对象
	 * @param encoding 内容编码
	 * @return 节点对象
	 * 2015年11月19日
	 * @author 马宝刚
	 */
	public static INodeHandler newFile(File file,String encoding) {
        try {
            return newNodeHandler(encoding).setFile(file);
        }catch(Exception e) {
            e.printStackTrace();
            return newNodeHandler(encoding);
        }
    }
	
	/**
	 * 获取新的节点对象
	 * 该方法就不用内容编码参数了，不妨先编好码再往里传
	 * @param nodeBody         节点内容 
	 * @return 节点对象
	 * 2015年11月19日
	 * @author 马宝刚
	 */
	public static INodeHandler newBody(String nodeBody) {
	    try {
	        return newNodeHandler().setNodeBody(nodeBody);
	    }catch(Exception e) {
	        e.printStackTrace();
	        return newNodeHandler();
	    }
	}
	
	/**
	 * 获取新节点对象
	 * @param nodeBytes 节点内容字节数组
	 * @return 节点对象
	 * 2017年10月16日
	 * @author MBG
	 */
	public static INodeHandler newBody(byte[] nodeBytes) {
	    try {
	        return newNodeHandler().setBytes(nodeBytes);
	    }catch(Exception e) {
	        e.printStackTrace();
	        return newNodeHandler();
	    }
	}
}
