/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.nodehandler.util;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.INodeHandler;
import com.jphenix.standard.viewhandler.IViewHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * 主子节点容器
 * @author 刘虻
 * 2008-5-13上午09:55:25
 */
@ClassInfo({"2014-06-04 12:49","主子节点容器"})
public class ParentChildNodeVO {

	protected IViewHandler vh = null; //当前节点
	
	protected ArrayList<ParentChildNodeVO> childNodeList = null; //子节点序列
	
	/**
	 * 构造函数
	 * 2008-5-13上午09:55:25
	 */
	public ParentChildNodeVO() {
		super();
	}

	
	/**
	 * 根据指定属性值获取主子节点信息容器
	 * @author 刘虻
	 * 2008-5-13上午09:59:48
	 * @param vh 指定节点
	 * @param key 属性主键
	 * @param value 属性值
	 * @return 返回节点信息容器
	 * 
	 */
	public static ParentChildNodeVO fixParentChildNodeVOByAttribute(
			IViewHandler vh,String key,String value) {
		//构建返回值
		ParentChildNodeVO reVO = new ParentChildNodeVO();
		reVO.setView(vh); //设置当前节点
		reVO.addChildNodeList(vh.getChildNodesByAttribute(key,value)); //添加其子节点
		return reVO;
	}
	
	
	/**
	 * 根据指定属性值获取主子节点信息容器
	 * @author 刘虻
	 * 2008-5-13上午09:59:48
	 * @param vh 指定节点
	 * @param key 属性主键
	 * @return 返回节点信息容器
	 * 
	 */
	public static ParentChildNodeVO fixParentChildNodeVOByAttribute(IViewHandler vh,String key) {
		//构建返回值
		ParentChildNodeVO reVO = new ParentChildNodeVO();
		reVO.setView(vh); //设置当前节点
		reVO.addChildNodeList(vh.getChildNodesByAttribute(key)); //添加其子节点
		return reVO;
	}
	
	/**
	 * 该节点是否存在子节点
	 * @author 刘虻
	 * 2008-5-13上午10:04:29
	 * @return true存在子节点
	 */
	public boolean hasChild() {
		return getChildList().size() > 0;
	}
	
	/**
	 * 获取当前节点
	 * @author 刘虻
	 * 2008-5-13上午10:05:13
	 * @return 当前节点
	 */
	public IViewHandler getView() {
		return vh;
	}
	
	/**
	 * 设置当前节点
	 * @author 刘虻
	 * 2008-5-13上午10:24:57
	 * @param vh 当前节点
	 */
	public void setView(IViewHandler vh) {
		this.vh = vh;
		childNodeList = null;
	}
	
	/**
	 * 获取子节点序列
	 * @author 刘虻
	 * 2008-5-13上午10:05:44
	 * @return 子节点序列
	 */
	public List<ParentChildNodeVO> getChildList() {
		if (childNodeList==null) {
			childNodeList = new ArrayList<ParentChildNodeVO>();
		}
		return childNodeList;
	}
	
	/**
	 * 添加子节点序列
	 * @author 刘虻
	 * 2008-5-13上午10:15:19
	 * @param childNodeList 子节点序列
	 */
	public void addChildNodeList(List<IViewHandler> childNodeList) {
		if (childNodeList==null) {
			return;
		}
		for (int i=0;i<childNodeList.size();i++) {
			addChildNode((INodeHandler)childNodeList.get(i));
		}
	}
	
	/**
	 * 获取是否为当前节点的子节点
	 * @author 刘虻
	 * 2008-5-13上午11:31:00
	 * @param pcn 指定节点
	 * @return 是否为当前节点的子节点
	 */
	public boolean isChildNode(ParentChildNodeVO pcn) {
		if (pcn==null || pcn.getView()==null) {
			return false;
		}
		return isChildNode(pcn.getView());
	}
	
	/**
	 * 获取是否为当前节点的子节点
	 * @author 刘虻
	 * 2008-5-13上午10:32:10
	 * @param childNode 判断节点
	 * @return 是否为当前节点的子节点
	 */
	public boolean isChildNode(IViewHandler childNode) {
		if (vh==null) {
			return false;
		}
		return vh.isChildNode(childNode);
	}
	
	/**
	 * 增加子节点
	 * @author 刘虻
	 * 2008-5-13上午10:07:15
	 * @param vh 子节点
	 */
	public void addChildNode(INodeHandler vh) {
		List<ParentChildNodeVO> cList = getChildList(); //获取当前节点的子节点
		for(ParentChildNodeVO pcv:cList) {
			if (pcv==null) {
				continue;
			}
			if (pcv.isChildNode(vh)) {
				pcv.addChildNode(vh);
				return;
			}
		}
		//构造子节点信息类
		ParentChildNodeVO pcv = new ParentChildNodeVO();
		pcv.setView(vh);
		cList.add(pcv); //放入子节点序列
	}
}
