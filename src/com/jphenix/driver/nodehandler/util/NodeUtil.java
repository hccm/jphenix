/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年10月31日
 * V4.0
 */
package com.jphenix.driver.nodehandler.util;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.standard.viewhandler.IViewHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 节点工具类（对外）
 * 
 * com.jphenix.driver.nodehandler.util.NodeUtil
 * 
 * @author MBG
 * 2017年10月31日
 */
@ClassInfo({"2017-10-31 17:07","节点工具类（对外）"})
public class NodeUtil {


	/**
	 * 将指定节点信息转换为Map对象
	 * 
	 * 比如：
	 * 
	 *       <path_a>
	 *         <path_b>
	 *            <key1>vlaue1</key1>
	 *            <key2>value2</key2>
	 *         </path_b>
	 *       </path_a>
	 *         
	 * Map res = getNodeInfoMap("/path_a/path_b");
	 * 
	 * 
	 * res:
	 *      key: key1   value: value1
	 *           key2   value: value2
	 *            
	 * 
	 * @param nodePath 指定节点路径
	 * @param vh       节点对象
	 * @return         参数值容器
	 * 2017年10月31日
	 * @author MBG
	 */
	public static Map<String,String> getNodeInfoMap(String nodePath,IViewHandler vh){
		//构建返回值
		Map<String,String> res = new HashMap<String,String>();
		if(vh==null) {
			return res;
		}
		if(nodePath.startsWith("/")) {
			nodePath.substring(1);
		}
		//分割点
		int point = nodePath.indexOf("/");
		String subPath; //当前节点名
		while(point>-1) {
			subPath = nodePath.substring(0,point);
			nodePath = nodePath.substring(point+1);
			vh = vh.fnn(subPath);
		}
		//获取其子节点
		List<IViewHandler> cList = vh.getChildDealNodes();
		String value; //参数值
		for(IViewHandler ele:cList) {
			value = ele.nt();
			if(value.length()>0) {
				res.put(ele.nn(),value);
			}
		}
		return res;
	}
	
	/**
	 * 从配置文件中获取参数名，然后从页面请求中获取参数值，
	 * 如果参数值为空，在从文件中获取默认的参数值
	 * 
	 * @param nodePath 指定参数块的节点路径
	 * @param vh       节点对象
	 * @param req      页面请求对象
	 * @return         参数值容器
	 * 2017年10月31日
	 * @author MBG
	 */
	public static Map<String,String> getNodeInfoRequest(String nodePath,IViewHandler vh,IRequest req) {
		//构建返回值
		Map<String,String> res = new HashMap<String,String>();
		if(vh==null) {
			return res;
		}
		//分割点
		int point = nodePath.indexOf("/");
		String subPath; //当前节点名
		while(point>-1) {
			if(point>0) {
				subPath = nodePath.substring(0,point);
				vh = vh.fnn(subPath);
			}
			nodePath = nodePath.substring(point+1);
			point = nodePath.indexOf("/");
		}
		if(nodePath.length()>0) {
			vh = vh.fnn(nodePath);
		}
		//获取其子节点
		List<IViewHandler> cList = vh.getChildDealNodes();
		String nodeName;      //参数名
		String value = null; //参数值
		for(IViewHandler ele:cList) {
			nodeName = ele.nn();
			if(req!=null) {
				value = req.getParameter(nodeName);
			}
			if(value==null || value.length()<1) {
				value = ele.nt();
			}
			if(value.length()>0) {
				res.put(ele.nn(),value);
			}
		}
		return res;
	}
}
