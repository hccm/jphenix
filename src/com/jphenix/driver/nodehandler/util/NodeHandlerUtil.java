/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.nodehandler.util;

import com.jphenix.standard.docs.ClassInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.ZipFile;

/**
 * 节点处理工具类
 * @author 刘虻
 * 2009-10-21上午11:18:22
 */
@ClassInfo({"2014-06-04 12:46","节点处理工具类"})
public class NodeHandlerUtil {

	/**
	 * 获取通过文件路径文件流
	 * @author 刘虻
	 * 2009-10-21上午11:20:16
	 * @param filePath 通过文件路径文件流
	 * @throws Exception 执行发生异常
	 * @return 文件流
	 */
	public static InputStream getFileStreamByPath(String filePath) throws Exception {
    	if (isZipUrl(filePath)) {
    		return getZipFileInputStreamByUrl(filePath);
    	}else {
    		return new FileInputStream(new File(filePath));
    	}
	}

	
    /**
     * 是否为压缩文件路径
     * @author 刘虻
     * 2008-7-26下午04:08:14
     * @param urlStr 测试路径
     * @return 是否为压缩文件路径
     */
    protected static boolean isZipUrl(String urlStr) {
    	if (urlStr==null) {
    		return false;
    	}
        return urlStr.startsWith("zip:");
    }
    
	
    /**
     * 获取压缩文件中指定文件读入流
     * @author 刘虻
     * 2008-7-26下午05:32:45
     * @param fileUrl 压缩文件中指定文件全路径
     * @return 压缩文件中指定文件读入流
     * @throws Exception 执行发生异常
     */
    protected static InputStream getZipFileInputStreamByUrl(String fileUrl) throws Exception {
    	if (!isZipUrl(fileUrl)) {
    		throw new Exception("This File Url["+fileUrl+"] Not Zip File URL");
    	}
    	//获取压缩文件
    	ZipFile zf = getZipFileByUrlString(fileUrl);
    	if (zf==null) {
    		throw new Exception("Not Find The Zip File["+fileUrl+"]");
    	}
    	//获取压缩文件中的子路径
    	String childPath = getZipFileChildPath(fileUrl);
    	if (childPath.startsWith("/")) {
    		childPath = childPath.substring(1);
    	}
    	try {
    		return zf.getInputStream(zf.getEntry(childPath));
    	}catch(Exception e) {
    		System.err.println("Not Find The Path:["+childPath+"] from Zip File:["+fileUrl+"]");
    	}
    	return null;
    }
    
    
    /**
     * 获取压缩文件内部相对路径
     * @author 刘虻
     * 2008-7-26下午05:19:07
     * @param fileUrl 压缩文件全路径
     * @return 压缩文件内部相对路径
     */
    protected static String getZipFileChildPath(String fileUrl) {
    	if (fileUrl==null) {
    		return "/";
    	}
    	//获取子路径分割点
    	int point = fileUrl.lastIndexOf("!");
    	if (point<0) {
    		return "/";
    	}
    	return fileUrl.substring(point+1);
    }
    
    
    /**
     * 通过URL路径获取压缩文件对象
     * @author 刘虻
     * 2008-7-26下午03:11:06
     * @param zipFileUrl URL路径
     * @return 压缩文件对象
     */
    protected static ZipFile getZipFileByUrlString(String zipFileUrl) {
    	//获取压缩文件路径
    	zipFileUrl = getZipFilePathByUrlString(zipFileUrl);
    	if (zipFileUrl==null) {
    		return null;
    	}
    	try {
    		return new ZipFile(zipFileUrl);
    	}catch(Exception e) {}
    	return null;
    }
    
    
    /**
     * 通过压缩文件URL获取压缩文件物理路径
     * 
     * 	如：zip:d:/zipfile/one.war!/com/Test.class
     * 
     *  返回 d:/zipfile/one.war
     * 
     * @author 刘虻
     * 2008-7-26下午03:13:49
     * @param zipFileUrl 压缩文件URL
     * @return 压缩文件物理路径
     */
    protected static String getZipFilePathByUrlString(String zipFileUrl) {
    	if (zipFileUrl==null) {
    		return null;
    	}
    	if (zipFileUrl.startsWith("zip:")) {
    		zipFileUrl = zipFileUrl.substring(4);
    	}
    	//获取压缩文件中子路径分隔符
    	int childFilePoint = zipFileUrl.lastIndexOf("!");
    	if(childFilePoint>-1) {
    		zipFileUrl = zipFileUrl.substring(0,childFilePoint);
    	}
    	return zipFileUrl;
    }
}
