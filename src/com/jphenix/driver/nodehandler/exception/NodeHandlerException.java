/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.nodehandler.exception;

import com.jphenix.standard.docs.ClassInfo;


/**
 * html解析异常管理类
 * @author 刘虻
 * 2006-10-25  15:13:46
 */
@ClassInfo({"2014-06-04 10:57","HTML解析异常管理类"})
public class NodeHandlerException extends Exception {

	
	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = 1477420041853208716L;

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NodeHandlerException() {
		super();
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NodeHandlerException(String message) {
		super(message);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NodeHandlerException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NodeHandlerException(String message, Throwable cause) {
		super(message, cause);
	}
}
