/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.log;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.log.ILog;

/**
 * 空的日志类
 * 
 * 2018-07-18 顺带修改了日志输出相关方法
 * 2018-07-30 增加了输出自定义日志（无用）
 * 2019-03-06 增加了个无用方法
 * 2019-03-15 增加了个无用方法
 * 2020-08-20 按照修改后的接口，将日志编码格式拆分成输出控制台日志编码格式和写文件日志编码格式
 * 2021-09-11 增加了几个无用方法
 * 
 * @author 刘虻
 * 2010-2-3 下午01:43:21
 */
@ClassInfo({"2021-09-11 17:39","空的日志类"})
public class NullLog implements ILog {

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NullLog() {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void error(Object content, Object e) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-4-28 下午02:35:47
	 */
	public void error(Object content, Object e, Class<?> exceptionCls)throws Exception {}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
    public String getConsoleEncoding() {
		return "";
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
	public String getFileEncoding() {
		return "";
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void log(Object content) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void log(String title,Object content) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public Object runBefore() {
		return null;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void sqlLog(Object sql) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void startLog(Object content) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void warning(Object content, Object e) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-3 下午01:43:21
	 */
	@Override
  public void writeRuntime(Object executeTimeVo, String title) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-10 下午04:52:34
	 */
	@Override
  public void destroy() {}
	
  /**
   * 覆盖方法
   */
  @Override
  public void info(Object content) {}

/**
 * 覆盖方法
 */
  @Override
  public void info(String title,Object content) {}

  /**
   * 覆盖方法
   */
  @Override
  public void debug(Object content) {}

  /**
   * 覆盖方法
   */
  @Override
  public void debug(String title,Object content) {}
    
  /**
   * 覆盖方法
   * 刘虻
   * 2010-9-10 下午04:53:35
   */
  public boolean debugMode() {
      return false;
  }

  /**
   * 输出交易数据日志
   * @param content 交易数据
   * 2016年6月7日
   * @author MBG
   */
	@Override
	public void dataLog(Object content) {}


	/**
	 * 输出临时日志（测试后需要删除的日志）
	 * @param content 日志内容
	 */
	@Override
	public void tlog(Object content) {}

	/**
	 * 输出临时日志（测试后需要删除的日志）
	 * @param content 日志内容
	 */
	@Override
	public void tlog(String title,Object content) {}
	
	/**
	 * 设置故障码
	 */
	@Override
	public void setAlertCode(String alertCode) {}
	
	
	/**
	 * 输出自定义日志
	 */
	@Override
	public void olog(Object content,String fileKey) {}
	
	/**
	 * 输出自定义日志
	 */
	@Override
	public void olog(String title,Object content,String fileKey) {}

	/**
	 * 输出sql语句日志
	 * @param sql       sql语句
	 * @param outSql    是否输出日志到控制台
	 * @param writeSql  是否将日志写入到文件
	 * 2019年3月6日
	 * @author MBG
	 */
	@Override
	public void sqlLog(Object sql,boolean outSql,boolean writeSql) {}
	
	
	/**
	 * 输出sql语句日志
	 * @param sql       sql语句
	 * @param outSql    是否输出日志到控制台
	 * @param fileKey   写入文件的文件名（不带扩展名）
	 * 2019年3月15日
	 * @author MBG
	 */
	@Override
	public void sqlLog(Object sql,boolean outSql,String fileKey) {}
}
