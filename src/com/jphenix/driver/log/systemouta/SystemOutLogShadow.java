/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.log.systemouta;

import com.jphenix.kernel.objectloader.instanceb.BaseBean;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.log.ILogShadow;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 系统输出日志影子类
 * 
 * 2018-07-02 接口发生变化，实现了接口新增的方法（无用） 
 * 2018-07-18 顺带修改了日志输出的相关方法
 * 2018-07-30 增加了写入自定义日志文件内容（无用）
 * 2019-03-15 按照接口修改了写文件方法（无用）
 * 2020-08-20 按照修改后的接口，将日志编码格式拆分成输出控制台日志编码格式和写文件日志编码格式
 * 
 * @author 刘虻
 * 2009-12-3 下午05:01:40
 */
@ClassInfo({"2020-08-20 10:10","系统输出日志影子类"})
public class SystemOutLogShadow extends BaseBean implements ILogShadow {
	
	protected String consoleEncoding;             //输出到控制台日志编码
	protected String fileEncoding;                //写入文件日志编码
	protected boolean
						outLog          = true,   //是否输出普通日志
						outWarning      = true,   //是否输出警告日志
						outStart        = true,   //是否输出启动日志
						outError        = true,   //是否输出错误日志
						outSql          = true,   //是否输出数据语句日志
						outRuntime      = true,   //是否输出耗费时间日志
						outInfo         = true,   //是否输出Info日志
						outData         = true,   //是否输出交易数据日志
						outTlog         = true;   //是否输出临时日志（调试后需要删除）
	
	//时间格式处理类
	protected static final SimpleDateFormat simpleDateFormat = 
	    		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S");
	
	
	/**
	 * 构造函数
	 * @author 刘虻 
	 */
	public SystemOutLogShadow() {
		super();
	}

	
	/**
	 * 获取当前时间
	 * 刘虻
	 * 2009-12-4 下午04:40:52
	 * @return 当前时间
	 */
	@Override
    public String now() {
    	return simpleDateFormat.format(new Date());
	}
	
	/**
	 * 设置编码格式
	 * 刘虻
	 * 2009-12-4 上午11:26:59
	 * @param encoding 编码格式
	 */
	public void setConsoleEncoding(String encoding) {
		this.consoleEncoding = encoding;
	}
	
	/**
	 * 设置写入文件编码格式
	 * @param encoding 编码格式
	 * 2020年8月20日
	 * @author MBG
	 */
	public void setFileEncoding(String encoding) {
		this.fileEncoding = encoding;
	}
	
	/**
	 * 获取输出到控制台日志编码格式
	 * 刘虻
	 * 2009-12-4 上午11:26:35
	 * @return 编码格式
	 */
	@Override
    public String getConsoleEncoding() {
		if(consoleEncoding==null) {
			consoleEncoding = System.getProperty("sun.jnu.encoding");
		}
		return consoleEncoding;
	}
	
	/**
	 * 获取写入文件日志编码格式
	 * 2020-08-20 10:04:00
	 * @return 编码格式
	 */
	@Override
	public String getFileEncoding() {
		if(fileEncoding==null || fileEncoding.length()<1) {
			fileEncoding = System.getProperty("file.encoding");
		}
		return fileEncoding;
	}
	
	/**
	 * 设置是否输出运行耗费时间日志
	 * 刘虻
	 * 2009-12-3 下午04:59:01
	 * @param outRuntime 是否输出运行耗费时间日志
	 */
	public void setOutRuntime(boolean outRuntime) {
		this.outRuntime = outRuntime;
	}
	
	/**
	 * 设置是否输出数据语句日志
	 * 刘虻
	 * 2009-12-3 下午04:57:49
	 * @param outSql 是否输出数据语句日志
	 */
	public void setOutSql(boolean outSql) {
		this.outSql = outSql;
	}
	
	/**
	 * 设置是否输出启动日志
	 * 刘虻
	 * 2009-12-3 下午04:57:07
	 * @param outStart 是否输出启动日志
	 */
	public void setOutStart(boolean outStart) {
		this.outStart = outStart;
	}
	
	/**
	 * 设置是否输出错误日志
	 * 刘虻
	 * 2009-12-3 下午04:56:29
	 * @param outError 是否输出错误日志
	 */
	public void setOutError(boolean outError) {
		this.outError = outError;
	}
	
	/**
	 * 设置是否输出警告日志
	 * 刘虻
	 * 2009-12-3 下午04:55:56
	 * @param outWarning 是否输出警告日志
	 */
	public void setOutWarning(boolean outWarning) {
		this.outWarning = outWarning;
	}
	
	/**
	 * 设置是否输出info日志
	 * @param outInfo 是否输出info日志
	 * 2014年6月17日
	 * @author 马宝刚
	 */
	public void setOutInfo(boolean outInfo) {
	    this.outInfo = outInfo;
	}
	
	/**
	 * 设置是否输出普通日志
	 * 刘虻
	 * 2009-12-3 下午04:55:22
	 * @param outLog 是否输出普通日志
	 */
	public void setOutLog(boolean outLog) {
		this.outLog = outLog;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:28:56
	 */
	@Override
    public void init() {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:29:02
	 */
	@Override
    public boolean isOutError() {
		return outError;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:29:38
	 */
	@Override
    public boolean isOutLog() {
		return outLog;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:29:46
	 */
	@Override
    public boolean isOutRuntime() {
		return outRuntime;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:29:54
	 */
	@Override
    public boolean isOutStart() {
		return outStart;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:02
	 */
	@Override
    public boolean isOutWarning() {
		return outWarning;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:14
	 */
	@Override
    public boolean isWriteError() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:19
	 */
	@Override
    public boolean isWriteLog() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:25
	 */
	@Override
    public boolean isWriteRuntime() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:29
	 */
	@Override
    public boolean isWriteStart() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:30:33
	 */
	@Override
    public boolean isWriteWarning() {
		return false;
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:37:30
	 */
	@Override
    public boolean isOutSql() {
		return outSql;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:37:34
	 */
	@Override
    public boolean isWriteSql() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:53:55
	 */
	@Override
    public void write(String content, boolean showLog, boolean writeLog, boolean nativeLog) {
		if(showLog) {
			System.out.print(content);
		}
	}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:54:01
	 */
	@Override
    public void write(byte[] bytes) {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:54:09
	 */
	@Override
    public void write(byte[] buf, int off, int len) {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:54:31
	 */
	@Override
    public void writeStart(String content, boolean showLog, boolean writeLog) {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-10 下午04:53:04
	 */
	@Override
    public void destroy() {}
	
    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:25:23
     */
    @Override
    public void writeLogEvent(String content) {}

    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    @Override
    public boolean isOutInfo() {
        return outInfo;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    @Override
    public boolean isWriteInfo() {
        return false;
    }
    
    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    public boolean hasLogEvent() {
        return false;
    }
    
    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
    @Override
    public void backupLogFile() {}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
    @Override
    public boolean isWriteDebug() {
        return false;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
	@Override
	public boolean isOutData() {
		return outData;
	}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
	@Override
	public boolean isWriteData() {
		return false;
	}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
	@Override
	public void setOutData(boolean outData) {
		this.outData = outData;
	}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
	@Override
	public void setWriteData(boolean writeData) {}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
	@Override
	public void writeData(String content, boolean showLog, boolean writeLog) {
		if(showLog) {
			System.out.print(content);	
		}
	}

	/**
	 * 返回是否写入内核调试日志到文件
	 * @return 是否写入内核调试日志到文件
	 * 2018年7月12日
	 * @author MBG
	 */
	public boolean isWriteNativeDebug() {
		return false;
	}
	
	/**
	 * 设置是否写入内核调试日志到文件
	 * @param writeNativeDebug 是否写入内核调试日志到文件
	 * 2018年7月12日
	 * @author MBG
	 */
	public void setWriteNativeDebug(boolean writeNativeDebug) {}
	
	/**
	 * 设置故障码
	 */
	@Override
    public void setAlertCode(String alertCode) {}
	
	/**
	 * 是否输出临时日志（测试后需要删除的日志）
	 * @return 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public boolean isOutTLog() {
		return outTlog;
	}
	
	/**
	 * 设置是否输出临时日志 （测试后需要删除的日志）
	 * @param outTLog 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public void setOutTLog(boolean outTLog) {
		this.outTlog = outTLog;
	}
	
	/**
	 * 是否需要写入临时日志到文件（测试后需要删除的日志）
	 * @return 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public boolean isWriteTLog() {
		return false;
	}
	
	/**
	 * 设置是否需要写入临时日志到文件 （测试后需要删除的日志）
	 * @param writeTLog 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public void setWriteTLog(boolean writeTLog) {}
	
	/**
	 * 获取输出日志令牌
	 */
	@Override
    public String getToken(String token) {
		return "";
	}

	/**
	 * 写入自定义日志文件内容
	 */
	@Override
	public void write(String content,boolean showLog,String fileKey) {}
}
