/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.log.xlogc;

import com.jphenix.driver.threadpool.ThreadSession;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.log.ILogShadow;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * 日志输出类
 * 2018-01-31 增加了判断是否不输出日志
 * 2019-02-01 修改了是否输出日志线程参数主键
 * 
 * @author 刘虻
 * 2010-8-24 上午10:52:30
 */
@ClassInfo({"2019-02-01 13:00","日志输出类"})
public class LogPrintStream extends PrintStream {

	protected ILogShadow logShadow = null;	//日志影子类
	protected OutputStream os = null; //输出流
	protected boolean
						showLog		= false		//是否输出日志
						,writeLog	= false;	//是否写入日志
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param out
	 */
	public LogPrintStream(
			OutputStream out
			,XLogFilter logShadow
			,boolean showLog
			,boolean writeLog) {
		super(out);
		os = out;
		this.logShadow = logShadow;
		this.showLog = showLog;
		this.writeLog = writeLog;
	}
	
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 上午11:14:44
	 */
	@Override
    public void write(byte[] buf, int off, int len) {
		if(SBoolean.valueOf(ThreadSession.get("_nolog_"))) {
			return;
		}
	    logShadow.writeLogEvent(new String(buf,off,len));
		if(showLog) {
			try {
				os.write(buf,off,len);
			}catch(Exception e) {}
		}
		if(writeLog) {
			logShadow.write(buf,off,len);
		}
	}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 上午11:15:04
	 */
	@Override
    public void write(byte[] b) {
		if(SBoolean.valueOf(ThreadSession.get("_nolog_"))) {
			return;
		}
	    logShadow.writeLogEvent(new String(b));
		if(showLog) {
			try {
				os.write(b);
			}catch(Exception e) {}
		}
		if(writeLog) {
			logShadow.write(b);
		}
	}
	
}
