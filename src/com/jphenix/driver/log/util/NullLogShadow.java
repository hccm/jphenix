/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.log.util;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.log.ILogShadow;

/**
 * 空日志影子类
 * 
 * 2018-07-02 接口发生变化，实现了接口新增的方法（无用） 
 * 2018-07-18 顺带修改了日志输出的相关方法
 * 2018-07-30 增加了写入自定义日志文件内容
 * 2019-03-15 增加了一个无用方法
 * 2020-08-20 按照修改后的接口，将日志编码格式拆分成输出控制台日志编码格式和写文件日志编码格式
 * 
 * @author 刘虻
 * 2010-3-31 上午10:42:25
 */
@ClassInfo({"2020-08-20 10:10","空日志影子类"})
public class NullLogShadow implements ILogShadow {
	
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NullLogShadow() {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public String getConsoleEncoding() {
		return System.getProperty("sun.jnu.encoding");
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
	public String getFileEncoding() {
		return System.getProperty("file.encoding");
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public void init() {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutError() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutLog() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutRuntime() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutSql() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutStart() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isOutWarning() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteError() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteLog() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteRuntime() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteSql() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteStart() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public boolean isWriteWarning() {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午10:42:25
	 */
	@Override
    public String now() {
		return null;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:51:12
	 */
	@Override
    public void write(String content, boolean showLog, boolean writeLog, boolean nativeLog) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:51:16
	 */
	@Override
    public void write(byte[] bytes) {}

	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:51:22
	 */
	@Override
    public void write(byte[] buf, int off, int len) {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-24 下午12:51:59
	 */
	@Override
    public void writeStart(String content, boolean showLog, boolean writeLog) {}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-10 下午04:52:18
	 */
	@Override
    public void destroy() {}
	
    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:25:23
     */
    @Override
    public void writeLogEvent(String content) {}
    
    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    @Override
    public boolean isOutInfo() {
        return false;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    @Override
    public boolean isWriteInfo() {
        return false;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2010-8-24 上午11:28:08
     */
    public boolean hasLogEvent() {
        return false;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
    @Override
    public void backupLogFile() {}

    /**
     * 覆盖方法
     * 刘虻
     * 2015-05-22 11:34:00
     */
    @Override
    public boolean isWriteDebug() {
        return false;
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2016-06-07 11:34:00
     */
	@Override
	public boolean isOutData() {
		return false;
	}

    /**
     * 覆盖方法
     * 刘虻
     * 2016-06-07 11:34:00
     */
	@Override
	public boolean isWriteData() {
		return false;
	}

    /**
     * 覆盖方法
     * 刘虻
     * 2016-06-07 11:34:00
     */
	@Override
	public void setOutData(boolean outData) {}

    /**
     * 覆盖方法
     * 刘虻
     * 2016-06-07 11:34:00
     */
	@Override
	public void setWriteData(boolean writeData) {}

    /**
     * 覆盖方法
     * 刘虻
     * 2016-06-07 11:34:00
     */
	@Override
	public void writeData(String content, boolean showLog, boolean writeLog) {}

	/**
	 * 返回是否输出内核调试日志
	 * @return 是否输出内核调试日志
	 * 2018年7月12日
	 * @author MBG
	 */
	public boolean isOutNativeDebug() {
		return false;
	}
	
	/**
	 * 设置是否输出内核调试日志
	 * @param outNativeDebug 是否输出内核调试日志
	 * 2018年7月12日
	 * @author MBG
	 */
	public void setOutNativeDebug(boolean outNativeDebug) {}
	
	/**
	 * 返回是否写入内核调试日志到文件
	 * @return 是否写入内核调试日志到文件
	 * 2018年7月12日
	 * @author MBG
	 */
	public boolean isWriteNativeDebug() {
		return false;
	}
	
	/**
	 * 设置是否写入内核调试日志到文件
	 * @param writeNativeDebug 是否写入内核调试日志到文件
	 * 2018年7月12日
	 * @author MBG
	 */
	public void setWriteNativeDebug(boolean writeNativeDebug) {}
	
	/**
	 * 设置故障码
	 */
	@Override
    public void setAlertCode(String alertCode) {}


	/**
	 * 是否输出临时日志（测试后需要删除的日志）
	 * @return 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public boolean isOutTLog() {
		return false;
	}
	
	/**
	 * 设置是否输出临时日志 （测试后需要删除的日志）
	 * @param outTLog 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public void setOutTLog(boolean outTLog) {}
	
	/**
	 * 是否需要写入临时日志到文件（测试后需要删除的日志）
	 * @return 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public boolean isWriteTLog() {
		return false;
	}
	
	/**
	 * 设置是否需要写入临时日志到文件 （测试后需要删除的日志）
	 * @param writeTLog 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	@Override
    public void setWriteTLog(boolean writeTLog) {}
	
	/**
	 * 获取输出日志令牌
	 */
	@Override
    public String getToken(String token) {
		return "";
	}

	/**
	 * 写入自定义日志文件内容
	 */
	@Override
	public void write(String content,boolean showLog,String fileKey) {}
}
