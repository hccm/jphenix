/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.driver.log.util;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 日志处理工具
 * 
 * 2020-08-26 将Exception换成了Throwable
 * 
 * @author 刘虻
 * 2009-12-4 下午04:47:40
 */
@ClassInfo({"2020-08-26 22:02","日志处理工具"})
public class LogUtil {

	/**
	 * 版本控制用
	 */
	final static String VER = "2009-12-04 16:48";
	
	/**
	 * 获取堆栈信息
	 * 刘虻
	 * 2009-12-4 下午04:45:54
	 * @param obj 指定类
	 * @return 堆栈信息
	 */
	public static String getStackTraceString(Object obj) {
	    //导入参数合法化
	    if (obj == null) {
	        return "";
	    }
	    //构造堆栈
	    Throwable throwable = new Throwable();
	    boolean printed = false; //是否输出了类明细
	    //构造返回信息值
	    StringBuffer returnSbf = new StringBuffer();
	    returnSbf.append("类：")
	    	.append(obj.getClass().getName())
	    	.append(":\n");
	    //构造堆栈信息
        for(StackTraceElement stacktraceelement:throwable.getStackTrace()){
            if (stacktraceelement.getClassName().equals(obj.getClass().getName())) {
                continue;
            }
            if (!printed) {
                printed = true;
                returnSbf.append("类：行[");
            }
            returnSbf.append(
                    stacktraceelement.getFileName()).append(":")
                    		.append(stacktraceelement.getLineNumber()).append(" ");
        }
        if (printed) {
            returnSbf.append("]\n");
        }
	    return returnSbf.toString();
	}
	
	
	

	/**
	 * 输出异常字符串
	 * @author 刘虻
	 * @param e 异常对象
	 * @return 异常信息
	 * 2006-1-26  17:46:24
	 */
	public static String getExceptionString(Throwable e) {
	    if (e == null) {
	        return "";
	    }
	    String className = e.getClass().getName(); //获取异常类名
	    if ("java.lang.Exception".equals(className)
	    		|| "java.lang.reflect.InvocationTargetException".equals(className)) {
	    	return getExceptionMessage(e);
	    }
	    //构造返回值容器
	    StringBuffer aStringBuffer = new StringBuffer("\nException：\n");
	    aStringBuffer.append(e.toString()).append("\n");
	    StackTraceElement[] exceptions = e.getStackTrace();
	    if (exceptions != null && exceptions.length > 0) {
	        for (int aInt=0;aInt<exceptions.length;aInt++) {
	            aStringBuffer.append(exceptions[aInt].toString()).append("\n");
	        }
	    }
	    return aStringBuffer.toString();
	}
	
	
    /**
     * 获取异常信息  如果没有异常信息，返回toString
     * @author 刘虻
     * 2007-10-9下午07:50:57
     * @param e 异常
     * @return 异常信息
     */
    protected static String getExceptionMessage(Throwable e) {
    	if (e==null) {
    		return null;
    	}
    	//获取来源
    	Throwable cause = e.getCause();
    	if(cause!=null) {
    		return getExceptionMessage(cause);
    	}
    	return e.toString();
    }
}
