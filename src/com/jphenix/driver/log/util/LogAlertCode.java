/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年7月17日
 * V4.0
 */
package com.jphenix.driver.log.util;

import java.util.HashMap;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 日志故障码信息类
 * 
 * 2018-07-31 增加了故障码标题对照容器
 * 
 * @author MBG
 * 2018年7月17日
 */
@ClassInfo({"2018-07-31 15:26","日志故障码信息类"})
public class LogAlertCode {

	/**
	 * 人为操作的普通错误（凡是调用了error()日志方法，都会发生）
	 */
	public final static String NORMAL_ERROR_FRONT   = "N000001";
	
	/**
	 * 后台运行程序报的普通错误 （凡是调用了error()日志方法，都会发生）
	 */
	public final static String 	NORMAL_ERROR_BACK = "N100001";
	
	/**
	 * 人为操作的普通警告（凡是调用了warning()日志方法，都会发生）
	 */
	public final static String NORMAL_WARNING_FRONT = "N000002";
	
	/**
	 * 后台运行程序报的普通警告（凡是调用了warning()日志方法，都会发生）
	 */
	public final static String NORMAL_WARNING_BACK = "N100002";
	
	
	/**
	 * 故障码标题对照容器
	 */
	private static HashMap<String,String> codeTitleMap = null;
	
	/**
	 * 故障码解释对照容器
	 */
	private static HashMap<String,String> codeExplainMap = null;
	
	
	/**
	 * 返回故障码标题对照容器
	 * @return 故障码标题对照容器
	 * 2018年7月31日
	 * @author MBG
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,String> getCodeTitleMap(){
		if(codeTitleMap==null) {
			codeTitleMap = new HashMap<String,String>();
			codeTitleMap.put("N000001","前台-普通-错误");
			codeTitleMap.put("N000002","前台-普通-警告");
			codeTitleMap.put("N100001","后台-普通-错误");
			codeTitleMap.put("N100002","后台-普通-警告");
		}
		return (Map<String,String>)codeTitleMap.clone();
	}
	
	/**
	 * 返回故障码解释对照容器
	 * @return 故障码解释对照容器
	 * 2018年7月17日
	 * @author MBG
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,String> getAlertInfo(){
		if(codeExplainMap==null) {
			codeExplainMap = new HashMap<String,String>();
			codeExplainMap.put("N000001","人为操作的普通错误（凡是调用了error()日志方法，都会发生）");
			codeExplainMap.put("N000002","人为操作的普通警告（凡是调用了warning()日志方法，都会发生）");
			codeExplainMap.put("N100001","后台运行程序报的普通错误 （凡是调用了error()日志方法，都会发生）");
			codeExplainMap.put("N100002","后台运行程序报的普通警告（凡是调用了warning()日志方法，都会发生）");
		}
		return (Map<String,String>)codeExplainMap.clone();
	}
}
