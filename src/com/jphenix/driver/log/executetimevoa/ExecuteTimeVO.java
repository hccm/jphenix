/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.driver.log.executetimevoa;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 日志耗费时间容器
 * @author 刘虻
 * 2009-12-3 下午04:49:42
 */
@ClassInfo({"2014-06-04 10:04","日志耗费时间容器"})
public class ExecuteTimeVO {
	
	protected long startTime = 0; //方法开始执行时间
	
	/**
	 * 构造函数 
	 * @author 刘虻
	 * 2005-10-30 15:24:45
	 */
	public ExecuteTimeVO() {
	    super();
	    setFirstTime(); //设置开始时间
	}
	
    
	/**
	 * 设置执行方法开始时间
	 * 覆盖方法 
	 * @author 刘虻
	 * 2005-10-30 15:24:53
	 */
	public void setFirstTime() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * 返回调用方法时间
	 * 覆盖方法 
	 * @author 刘虻
	 * 2005-10-30 15:25:54
	 */
	public long getRunTime() {
		return System.currentTimeMillis()-startTime;
	}
	
	
	/**
	 * 获取解析后的时间信息
	 * @return 解析后的运行时间信息
	 * 2016年6月29日
	 * @author MBG
	 */
	public String getRunTimeInfo() {
		//获取运行毫秒
		long ms = System.currentTimeMillis()-startTime;
		if(ms<1000) {
			return ms+" ms";
		}
		//余数
		long ys = ms % 1000;
		ms = ms / 1000;
		if(ms<60) {
			return ms+"."+ys+" s";
		}
		ys = ms % 60;
		ms = ms /60;
		if(ms<60) {
			return ms+"."+ys+" m";
		}
		ys = ms % 60;
		ms = ms /60;
		return ms+"."+ys+" h";
	}
}
