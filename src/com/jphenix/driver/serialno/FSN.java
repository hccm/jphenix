/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.serialno;

import com.jphenix.driver.serialno.instancea.SN;
import com.jphenix.share.lang.SListMap;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.serialno.ISN;

/**
 * 序列号生成器工厂
 * 
 * 2019-06-26 重构了序列号工厂类
 * 
 * @author 刘虻
 * 2008-7-31下午05:49:27
 */
@ClassInfo({"2019-06-26 10:58","序列号生成器工厂"})
public class FSN {
	
	//驻留内存的序列号类
	protected static SListMap<ISN> snMap = new SListMap<ISN>();
	
	/**
	 * 构造函数
	 * 2008-7-31下午05:49:28
	 */
	public FSN() {
		super();
	}
	
	/**
	 * 加载并驻留内存序列号生成器
	 * @author 刘虻
	 * 2008-7-31下午06:00:20
	 * @return 序列号生成器
	 */
	public static ISN load() {
		return load(null,20);
	}
	
	/**
	 * 设置统一的序列号
	 * 避免集群中的服务生成出同样的主键值，
	 * 在这里设置当前服务器在集群中的索引
	 * @param keyHead 集群序列号
	 * 2016年12月11日
	 * @author MBG
	 */
	public static void setIndex(String index) {
		for(int i=0;i<snMap.size();i++) {
			snMap.get(i).index(index);
		}
	}
	
	/**
	 * 加载并驻留内存序列号生成器
	 * @author 刘虻
	 * 2008-7-31下午06:01:25
	 * @param head 序列号头
	 * @param size 序列号长度
	 * @return 序列号生成器
	 * @throws 异常：尾部序列号长度小于1
	 */
	public static ISN load(String head,int size) {
		if(head==null || head.length()<1) {
			head = "PX";
		}
		ISN sn = snMap.get(head);
		if(sn==null) {
			try {
				sn = newInstance(head,size);
			}catch(Exception e) {
				e.printStackTrace();
			}
			snMap.put(head,sn);
		}
		return sn;
	}
	
	/**
	 * 加载并驻留内存序列号生成器
	 * @author 刘虻
	 * 2008-7-31下午06:03:42
	 * @param size 序列号长度
	 * @return 序列号生成器
	 */
	public static ISN load(int size,boolean noDbSn) {
		return load(null,size);
	}
	
	/**
	 * 获取新的类实例
	 * @author 刘虻
	 * 2008-7-31下午05:58:38
	 * @return 新的类实例
	 */
	public static ISN newInstance() {
		return newInstance(null,20);
	}
	
	/**
	 * 获取新的类实例
	 * @author 刘虻
	 * 2008-7-31下午05:58:03
	 * @param head   序列号头
	 * @param size   序列号长度
	 * @return 序列号生成器
	 */
	public static ISN newInstance(String head,int size) {
		if(head==null || head.length()<1) {
			head = ISN.DEFAULT_HEAD;
		}
		//先从容器中获取实例
		ISN sn = snMap.get(head);
		if(sn==null) {
			//构造返回值
			sn = SN.instance(head,size);
			snMap.put(head,sn);
		}
		return sn;
	}
	
	/**
	 * 获取新的类实例
	 * @author 刘虻
	 * 2008-7-31下午05:58:50
	 * @param size 序列号长度
	 * @return 新的类实例
	 */
	public static ISN newInstance(int size){
		return newInstance(null,size);
	}
	
	/**
	 * 获取新的类实例
	 * @author 刘虻
	 * 2008-7-31下午05:59:11
	 * @param head 序列号头
	 * @return 新的类实例
	 */
	public static ISN newInstance(String head){
		return newInstance(head,20);
	}
}
