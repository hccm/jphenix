/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年7月3日
 * V4.0
 */
package com.jphenix.driver.serialno.instanceb;

import java.util.ArrayList;
import java.util.List;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.serialno.ISN;

/**
 * 使用UUID格式的随机字符串
 * 
 * com.jphenix.driver.serialno.instanceb.UUID
 * 
 * 2019-06-26 按照修改后的接口修改了内容
 * 
 * @author MBG
 * 2017年7月3日
 */
@ClassInfo({"2019-06-26 11:08","使用UUID格式的随机字符串"})
public class UUID implements ISN {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public UUID() {
		super();
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
	public String index() {
		return "";
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
	public void index(String index) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public String head() {
		return "";
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public int size() {
		return 36;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String[] getSIDs(int countInt) {
		if(countInt<1) {
			return new String[0];
		}
		//构建返回值
		String[] reStrs = new String[countInt];
		for(int i=0;i<countInt;i++) {
			reStrs[i] = java.util.UUID.randomUUID().toString();
		}
		return reStrs;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<String> getSIDList(int countInt) {
		if(countInt<1) {
			return new ArrayList<String>();
		}
		//构建返回值
		List<String> reList = new ArrayList<String>(countInt);
		for(int i=0;i<countInt;i++) {
			reList.add(java.util.UUID.randomUUID().toString());
		}
		return reList;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getSID() {
		return java.util.UUID.randomUUID().toString();
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String sid() {
		return java.util.UUID.randomUUID().toString();
	}
}
