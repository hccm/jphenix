/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.driver.threadpool;

import java.util.HashMap;

import com.jphenix.share.lang.SString;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 线程会话类
 * 注意：比如action动作，别以为动作开始时放入一个值，在动作执行完毕后就会自动释放。
 *            如果采用了线程池，该值就会被其他动作获取到。所以在动作结束时，需要手动清空设置的值
 *            
 * 2018-07-31 增加了判断是否存在指定主键值，移除指定对象
 * 
 * @author 刘虻
 * 2009-12-20下午08:08:21
 */
@ClassInfo({"2018-07-31 17:37","线程会话类"})
public class ThreadSession {

	//线程级变量容器
	protected static ThreadLocal<HashMap<Object,Object>> tl = 
	                                            new ThreadLocal<HashMap<Object,Object>>();
	
	/**
	 * 构造函数
	 * 2009-12-20下午08:08:21
	 */
	public ThreadSession() {
		super();
	}
	

	/**
	 * 获取线程信息容器
	 * @author 刘虻
	 * 2009-12-20下午08:11:13
	 * @return 线程信息容器
	 */
	public HashMap<Object,Object> getMap() {
		//获取返回值
		HashMap<Object,Object> reMap = tl.get();
		if(reMap==null) {
			reMap = new HashMap<Object,Object>();
			tl.set(reMap);
		}
		return reMap;
	}
	
	/**
	 * 在线程会话中设置指定的主键值 同put
	 * 刘虻
	 * 2010-7-7 下午05:30:57
	 * @param key 主键
	 * @param value 值
	 */
	public static ThreadSession put(Object key,Object value) {
		return (new ThreadSession()).nPut(key,value);
	}
	
	
	/**
	 * 在线程会话中设置指定的主键值 同put
	 * 刘虻
	 * 2010-7-7 下午05:30:57
	 * @param key 主键
	 * @param value 值
	 */
	public static ThreadSession set(Object key,Object value) {
		return (new ThreadSession()).nPut(key,value);
	}
	

	/**
	 * 在线程会话中设置值
	 * 刘虻
	 * 2013-4-12 下午3:39:48
	 * @param key		主键
	 * @param value		值
	 * @return 当前类实例
	 */
	public ThreadSession nPut(Object key,Object value) {
		getMap().put(key,value);
		return this;
	}
	
	
	/**
	 * 清空线程回话中的信息
	 * 2015年4月21日
	 * @author 马宝刚
	 */
	public static void clear() {
	    (new ThreadSession()).getMap().clear();
	}
	
	
	/**
	 * 在线程会话类中获取指定的主键值
	 * 刘虻
	 * 2010-6-2 下午03:40:28
	 * @param key 主键
	 * @return 主键值
	 */
	public static Object get(Object key) {
		return (new ThreadSession()).getMap().get(key);
	}
	
	/**
	 * 判断是否存在指定对象
	 * @param key 主键
	 * @return 是否存在指定对象值
	 * 2018年7月31日
	 * @author MBG
	 */
	public static boolean containsKey(Object key) {
		return (new ThreadSession()).getMap().containsKey(key);
	}
	
	/**
	 * 移除指定对象
	 * @param key 指定主键
	 * @return 移除的对应主键的值
	 * 2018年7月31日
	 * @author MBG
	 */
	public static Object remove(Object key) {
		return (new ThreadSession()).getMap().remove(key);
	}
	
	/**
	 * 线程会话类中获取字符串值
	 * 刘虻
	 * 2013-4-12 下午3:33:55
	 * @param key 主键
	 * @return 字符串值
	 */
	public static String getString(String key) {
		return  SString.valueOf((new ThreadSession()).getMap().get(key));
	}
}
