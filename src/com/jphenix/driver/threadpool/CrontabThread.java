/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月5日
 * V4.0
 */
package com.jphenix.driver.threadpool;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 定时任务线程父类
 * 
 * 
 * 
 * 
 * 
 * @author 马宝刚
 * 2014年7月5日
 */
@ClassInfo({"2014-07-05 00:21:00","定时任务线程"})
public class CrontabThread extends ThreadEle {

    private CrontabVO cvo = null; //定时任务信息类
    private boolean disabled = false; //是否暂停执行
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public CrontabThread(CrontabVO cvo) {
        super(cvo.getName(),true);
        setBase(cvo); //设置基类
        this.cvo = cvo;
        super.init(); //执行初始化
    }
    
    /**
     * 获取定时任务信息类
     * @return 定时任务信息类
     * 2016年5月9日
     * @author MBG
     */
    public CrontabVO getCrontabVO() {
    	return cvo;
    }
    
    /**
     * 设置是否禁止执行任务
     * @param disabled 是否禁止执行
     * 2014年7月5日
     * @author 马宝刚
     */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * 是否禁止执行任务
     * @return 是否禁止执行任务
     * 2014年7月5日
     * @author 马宝刚
     */
    public boolean isDisabled() {
        return disabled;
    }
    
    
    /**
     * 覆盖方法
     */
    @Override
    public  void run() {
        try {
        	while(!_beanFactory.isStartRun()) {
        		//判断是否加载完毕
                Thread.sleep(1000);
        	}
        }catch(Exception e) {}
        while(true) {
            if(cvo.onTime() && !disabled) {
                try {
                	cvo.invokeTask();
                }catch(Exception e) {
                    e.printStackTrace();
                    log.error("The Crontab Task ["+cvo.getName()+"] Execute Exception",e);
                }
            }
            try {
                //秒级轮询
                Thread.sleep(1000);
            }catch(Exception e) {}
        }
    }
}
