/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月4日
 * V4.0
 */
package com.jphenix.driver.threadpool;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.threadpool.IThreadManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 线程管理类
 * com.jphenix.driver.threadpool.ThreadManager
 * 
 * 2018-12-21 增加了通过线程ID值终止指定线程
 *            增加了通过线程的HashCode终止指定线程
 * 
 * @author 马宝刚
 * 2014年7月4日
 */
@ClassInfo({"2018-12-21 14:05","线程管理类"})
@BeanInfo({"threadmanager","0","","","1"})
@Running({"90"})
public class ThreadManager extends ABase implements IThreadManager {

    //线程对象序列
    private List<ThreadEle> threadList = new ArrayList<ThreadEle>();
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ThreadManager() {
        super();
    }
    
    /**
     * 将线程元素放入管理类
     * @param ele 线程元素
     * 2014年7月4日
     * @author 马宝刚
     */
    @Override
    public void addThread(ThreadEle ele) {
        if(ele==null || threadList.contains(ele)) {
            return;
        }
        if(_beanFactory!=null) {
            ele.setBase(this);
        }
        threadList.add(ele);
        if(!ele.isAlive()) {
            ele.start();
        }
    }

    /**
     * 获取线程序列
     * @return 线程序列
     * 2014年7月4日
     * @author 马宝刚
     */
    @Override
    public List<ThreadEle> getThreadList(){
        return threadList;
    }
    
    
    /**
     * 终止全部线程
     * 2014年7月4日
     * @author 马宝刚
     */
    @Override
    @SuppressWarnings("deprecation")
    public void stop() {
        for(ThreadEle ele:threadList) {
            ele.stop();
        }
    }
    
    
    /**
     * 干掉指定HashCode值的线程
     * @param hashCode 线程HashCode
     * 2018年12月21日
     * @author MBG
     */
    @SuppressWarnings("deprecation")
    public static void killThreadByHashCode(int hashCode) {
        //获取顶级线程池
        ThreadGroup group = Thread.currentThread().getThreadGroup();  
        ThreadGroup topGroup = group;  
        //遍历线程组树，获取根线程组
        while (group != null) {  
            topGroup = group;  
            group = group.getParent();  
        } 
        //准备获取全部线程
        Thread[] threads = new Thread[topGroup.activeCount()*3]; 
        //当前线程数
        int threadCount = topGroup.enumerate(threads);
        for(int i=0;i<threadCount;i++) {
            if(threads[i]==null) {
                continue;
            }
            if(threads[i].hashCode()==hashCode) {
            	System.err.println("\n\n---Kill-Thread ID:["+threads[i].getId()+"] HashCode:["
                 +threads[i].hashCode()+"] Name:["
                 +threads[i].getName()+"] ClasPath:["+threads[i].getClass().getName()
                 +"]--by HashCode:["+hashCode+"]--\n\n");
                try {
                    threads[i].stop();
                }catch(Exception e) {}
            }
        }
    }
    
    
    /**
     * 干掉指定id的线程
     * @param id 线程ID
     * 2018年12月21日
     * @author MBG
     */
    @SuppressWarnings("deprecation")
    public static void killThreadByID(long id) {
        //获取顶级线程池
        ThreadGroup group = Thread.currentThread().getThreadGroup();  
        ThreadGroup topGroup = group;  
        //遍历线程组树，获取根线程组
        while (group != null) {  
            topGroup = group;  
            group = group.getParent();  
        } 
        //准备获取全部线程
        Thread[] threads = new Thread[topGroup.activeCount()*3]; 
        //当前线程数
        int threadCount = topGroup.enumerate(threads);
        for(int i=0;i<threadCount;i++) {
            if(threads[i]==null) {
                continue;
            }
            if(threads[i].getId()==id) {
            	System.err.println("\n\n---Kill-Thread ID:["+id+"] HashCode:["+threads[i].hashCode()+"] Name:["
                 +threads[i].getName()+"] ClasPath:["+threads[i].getClass().getName()+"]--by ID:["+id+"]--\n\n");
                try {
                    threads[i].stop();
                }catch(Exception e) {}
            }
        }
    }
    
    
    /**
     * 干掉指定关键字开头类名的线程
     * @param classNameKey 指定关键字（类开头名字，用来获取起内部线程类）
     * 2015年8月14日
     * @author 马宝刚
     */
    @SuppressWarnings("deprecation")
    public static void killThreadByClassNameKey(String classNameKey) {
        //获取顶级线程池
        ThreadGroup group = Thread.currentThread().getThreadGroup();  
        ThreadGroup topGroup = group;  
        //遍历线程组树，获取根线程组
        while (group != null) {  
            topGroup = group;  
            group = group.getParent();  
        } 
        //准备获取全部线程
        Thread[] threads = new Thread[topGroup.activeCount()*3]; 
        //当前线程数
        int threadCount = topGroup.enumerate(threads);
        //线程类名
        String threadClassName;
        for(int i=0;i<threadCount;i++) {
            if(threads[i]==null) {
                continue;
            }
            threadClassName = threads[i].getClass().getName();
            if(threadClassName.startsWith(classNameKey)) {
            	System.err.println("\n\n---Kill-Thread ID:["+threads[i].getId()+"] HashCode:["+threads[i].hashCode()+"] Name:["
                        +threads[i].getName()+"] ClasPath:["+threads[i].getClass().getName()+"]--by Name:["+classNameKey+"]--\n\n");
                try {
                    threads[i].stop();
                }catch(Exception e) {}
            }
        }
    }
}
