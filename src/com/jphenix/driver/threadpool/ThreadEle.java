/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月4日
 * V4.0
 */
package com.jphenix.driver.threadpool;

import com.jphenix.driver.log.xlogc.XLog;
import com.jphenix.driver.propertie.instanceb.XmlConfProp;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.share.bean.instancea.FilesUtil;
import com.jphenix.share.lang.SString;
import com.jphenix.standard.beans.IBase;
import com.jphenix.standard.beans.IFilesUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.internationalization.ILanguage;
import com.jphenix.standard.internationalization.ILanguageManager;
import com.jphenix.standard.log.ILog;
import com.jphenix.standard.threadpool.IThreadManager;

/**
 * 线程元素父类
 * 
 * 2018-07-18 修改了日志输出调用方法
 * 2018-09-05 beanFactory.getConfProp()方法无需用IBeanFactoryManager接口强制转换
 * 2020-08-06 去掉了配置文件接口，改用实体类
 * 
 * @author 马宝刚
 * 2014年7月4日
 */
@ClassInfo({"2020-08-06 15:11","线程元素父类"})
public abstract class ThreadEle extends Thread implements IBase {

    protected String           _beanID                  = null; //类主键
    protected IBeanFactory     _beanFactory             = null; //类加载器
    protected ILog             log                      = null; //日志类
    protected XmlConfProp      prop                     = null; //获取配置文件管理类
    protected ILanguageManager languageManager          = null; //获取语言管理类
    protected IFilesUtil       filesUtil                = null; //文件处理工具
    protected IThreadManager   tm                       = null; //线程管理类
    private   Boolean          disabledLanguageManager  = null; //禁用语言管理
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ThreadEle(String threadName) {
        super(threadName);
        init();
    }
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ThreadEle(String threadName,boolean noInit) {
        super(threadName);
        if(!noInit) {
            init();
        }
    }
    
    
    /**
     * 执行初始化
     * 2014年10月20日
     * @author 马宝刚
     */
    public void init() {
        setDaemon(true); //设置为守护进程，否则在进程重启时会新起一个线程，而原来的线程还在运行，成为了野线程
        getManager().addThread(this); //放入管理类
    }
    
    /**
     * 执行线程
     */
    @Override
    public abstract void run();
    
    /**
     * 获取线程管理类
     * @return 线程管理类
     * 2014年7月4日
     * @author 马宝刚
     */
    public IThreadManager getManager() {
        if(tm==null) {
            try {
                tm = (IThreadManager)getBean(IThreadManager.class);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return tm;
    }

    
    /**
     * 终止并从线程管理类中移出
     * 2014年7月5日
     * @author 马宝刚
     */
    @SuppressWarnings("deprecation")
    public void stopRemove() {
        getManager().getThreadList().remove(this);
        try {
            stop();
        }catch(Exception e) {}
    }

    /**
     * 设置基本类信息从目标类
     * @author 刘虻
     * 2007-11-7下午05:21:42
     * @param obj 目标类
     */
    @Override
    public void setBase(Object obj) {
        if (obj==null || !(obj instanceof IBase)) {
            return;
        }
        setBeanFactory(((IBase)obj).getBeanFactory());
    }
    
    /**
     * 覆盖方法
     * 刘虻
     * 2013-4-9 下午4:20:10
     */
	public ILanguageManager getLanguageManager() {
		if(languageManager==null && disabledLanguageManager==null) {
			if(!getBeanFactory().beanExists(ILanguageManager.class)) {
				disabledLanguageManager = new Boolean(true);
				return null;
			}
			try {
				//从类加载器中获取
				languageManager =
                        getBeanFactory()
                            .getObject(ILanguageManager.class,this);
				disabledLanguageManager = new Boolean(false);
			}catch(Exception e) {
				e.printStackTrace();
				disabledLanguageManager = new Boolean(true);
			}
		}
		return languageManager;
	}
	
	/**
	 * 通过浏览器中的语言代码转换为内部语言代码
	 * （有可能多个外部语言代码对应内部同一个语言代码）
	 * 比如 key:zh_cn  value:chs   key小写
	 * @param key 浏览器中的语言代码
	 * @return 内部语言代码
	 * 2017年2月23日
	 * @author MBG
	 */
	public String getLanguageCode(String key) {
		if(disabledLanguageManager==null) {
			getLanguageManager();
			if(languageManager!=null) {
				return SString.valueOf(languageManager.getLanguageCodeMap().get(key));
			}
		}
		if(disabledLanguageManager) {
			return key;
		}
		return SString.valueOf(languageManager.getLanguageCodeMap().get(key));
	}

    
    /**
     * 覆盖方法
     * 刘虻
     * 2010-4-28 下午05:02:53
     */
    @Override
    public void setBeanFactory(IBeanFactory beanFactory) {
        _beanFactory = beanFactory;
       //构建并设置日志影子类
        log = new XLog(beanFactory.getLogShadow());
        ((XLog)log).setBoss(this);
        //设置配置文件信息类
        prop = beanFactory.getConfProp();
        filesUtil = new FilesUtil();
        filesUtil.setWebInfPath(beanFactory.getWebInfPath());
        filesUtil.setTempPath(prop.getParameter("TempPath"));
        filesUtil.setUploadBasePath(prop.getParameter("UploadPath"));
    }

    /**
     * 通过类接口获取指定类实例
     * 刘虻
     * 2010-7-27 下午02:15:36
     * @param interfaceCls 接口类
     * @return 指定类实例
     * @throws Exception 执行发生异常
     */
    protected Object getBean(Class<?> interfaceCls) throws Exception {
        return getBeanFactory().getObject(interfaceCls,this);
    }
    
    /**
     * 覆盖方法
     * 刘虻
     * 2010-2-2 下午03:20:04
     */
    protected ILanguage getLanguage(String languageCode) {
		if(disabledLanguageManager==null) {
			getLanguageManager();
		}
		if(disabledLanguageManager) {
			return null;
		}
        return languageManager.getLanguge(this);
    }
    
    /**
     * 构建一个新的类，如果这个类的父类也是ABase，则自动设置ABase中的信息
     * 注意：该类构建函数中没有任何参数
     * 刘虻
     * 2011-9-26 上午11:05:47
     * @param cls 要构建的类
     * @throws Exception 异常
     * @return 类实例
     */
    protected Object newBean(Class<?> cls) throws Exception {
        if(cls==null) {
            return null;
        }
        //构建类
        Object res = cls.newInstance();
        if(res instanceof IBase) {
            ((IBase)res).setBase(this);
        }
        return res;
    }


    /**
     * 覆盖方法
     * 刘虻
     * 2010-2-2 下午03:20:40
     */
    protected String i(
            String contentKey,String defaultContent,String[] paraValues) {
		if(disabledLanguageManager==null) {
			getLanguageManager();
		}
		if(disabledLanguageManager) {
			return defaultContent;
		}
        return languageManager.getLanguge(this).i(contentKey,defaultContent,paraValues);
    }
    
    /**
     * 输出指定的语言信息
     * 刘虻
     * 2013-4-7 上午10:41:24
     * @param contentKey            语言信息主键
     * @param defaultContent        默认语言
     * @return  指定的语言信息
     */
    protected String i(String contentKey,String defaultContent) {
		if(disabledLanguageManager==null) {
			getLanguageManager();
		}
		if(disabledLanguageManager) {
			return defaultContent;
		}
        return languageManager.getLanguge(this).i(contentKey,defaultContent,null);
    }
    
    /**
     * 输出指定的语言信息
     * 刘虻
     * 2013-4-7 上午10:41:24
     * @param contentKey            语言信息主键
     * @return  指定的语言信息
     */
    protected String i(String contentKey) {
		if(disabledLanguageManager==null) {
			getLanguageManager();
		}
		if(disabledLanguageManager) {
			return contentKey;
		}
        return languageManager.getLanguge(this).i(contentKey,null,null);
    }


    /**
     * 覆盖方法
     * 刘虻
     * 2010-2-1 下午04:06:51
     */
    @Override
    public IBeanFactory getBeanFactory() {
        return _beanFactory;
    }


    /**
     * 覆盖方法
     * 刘虻
     * 2010-5-5 下午04:25:35
     */
    @Override
    public String getBeanID() {
        if(_beanID==null) {
            _beanID = "";
        }
        return _beanID;
    }
    
    
	/**
	 * 返回 WEB-INF 文件夹的绝对路径
	 * @return WEB-INF 文件夹的绝对路径
	 * 2017年12月1日
	 * @author MBG
	 */
	@Override
    public String webInfPath() {
		return _beanFactory.getWebInfPath();
	}
}
