/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年6月10日
 * V4.0
 */
package com.jphenix.driver.ftp;

import com.jphenix.sdk.ftp.FtpDirEntry;
import com.jphenix.sdk.ftp.FtpDirEntry.Type;
import com.jphenix.sdk.ftp.FtpDirParser;
import com.jphenix.share.lang.SDate;
import com.jphenix.share.lang.SInteger;
import com.jphenix.share.lang.SLong;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 微软FTP服务目录解析类
 * com.jphenix.driver.ftp.MsDirParser
 * @author MBG
 * 2016年6月10日
 */
@ClassInfo({"2016-06-10 07:48","微软FTP服务目录解析类"})
public class MsDirParser implements FtpDirParser {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public MsDirParser() {
		super();
	}


	/**
	 * 解析行
	 */
	@Override
	public FtpDirEntry parseLine(String line) {
		if(line==null) {
			return null;
		}
		line = fixLine(line);
		//分割信息段
		String[] infoEles = BaseUtil.split(line," ");
		if(infoEles.length<4) {
			//无法解析该信息
			return null;
		}
		//构建返回值
		FtpDirEntry ftpDir = createFtpDir(infoEles);
		fixDate(ftpDir,infoEles); //设置建立日期
		if("<DIR>".equals(infoEles[2])) {
			//目录
			ftpDir.setType(Type.DIR);
		}else {
			//文件
			ftpDir.setType(Type.FILE);
			//设置文件大小
			ftpDir.setSize(SLong.valueOf(infoEles[2]));
		}
		return ftpDir;
	}
	
	
	/**
	 * 将行信息中多个空格变成一个空格
	 * @param line 待处理字符串
	 * @return 处理后字符串
	 * 2016年6月13日
	 * @author MBG
	 */
	public String fixLine(String line) {
		line = BaseUtil.swapString(line,"\t"," ");
		while(line.indexOf("  ")>0) {
			line = BaseUtil.swapString(line,"  "," ");
		}
		return line;
	}
	
	/**
	 * 构建返回值
	 * @param infos 行信息段数组
	 * @return 返回值
	 * 2016年6月10日
	 * @author MBG
	 */
	protected FtpDirEntry createFtpDir(String[] infos) {
		String name = ""; //构建名字
		for(int i=3;i<infos.length;i++) {
			name += infos[i];
		}
		return new FtpDirEntry(name);
	}

	
	/**
	 * 设置日期
	 * @param ftpDir 返回值对象
	 * @param infos 信息数组
	 * 2016年6月10日
	 * @author MBG
	 */
	protected void fixDate(FtpDirEntry ftpDir,String[] infos) {
		//拼装日期字符串
		String dateStr = infos[0]+" ";
		
		if(infos[1].endsWith("PM")) {
			dateStr += (12+SInteger.valueOf(infos[1].substring(0,2)))+":"+infos[1].substring(3,5)+":00";
		}else if(infos[1].endsWith("AM")){
			dateStr += infos[1].substring(0,5)+":00";
		}else {
			dateStr += infos[1]+":00";
		}
		ftpDir.setCreated((new SDate(dateStr)).getDate());
	}
}
