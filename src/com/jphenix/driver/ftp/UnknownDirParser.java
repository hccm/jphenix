/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年6月13日
 * V4.0
 */
package com.jphenix.driver.ftp;

import com.jphenix.sdk.ftp.FtpDirEntry;
import com.jphenix.sdk.ftp.FtpDirEntry.Type;
import com.jphenix.sdk.ftp.FtpDirParser;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 未知目录解析类（将目录行信息写到文件名中）
 * com.jphenix.driver.ftp.UnknownDirParser
 * @author MBG
 * 2016年6月13日
 */
@ClassInfo({"2016-06-13 09:42","未知目录解析类（将目录行信息写到文件名中）"})
public class UnknownDirParser implements FtpDirParser {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public UnknownDirParser() {
		super();
	}


	/**
	 * 解析行
	 */
	@Override
	public FtpDirEntry parseLine(String line) {
		if(line==null) {
			return null;
		}
		//构建返回值
		FtpDirEntry res = new FtpDirEntry(line);
		res.setType(Type.FILE);
		
		return res;
	}
}
