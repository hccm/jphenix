/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.driver.internationalization;

import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.share.lang.SString;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.internationalization.ILanguage;

/**
 * 空的语言处理类
 * @author 刘虻
 * 2010-3-31 上午09:54:51
 */
@ClassInfo({"2014-06-03 18:42","空的语言处理类"})
public class NullLanguage implements ILanguage {

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NullLanguage() {
		super();
	}
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param bean 调用类实例
	 */
	public NullLanguage(IBean bean) {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午09:54:51
	 */
	@Override
    public String i(
			String contentKey
			,String defaultContent
			,String[] paraValues) {
		if(defaultContent==null) {
			return contentKey;
		}
		return fixParameter(defaultContent,paraValues);
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2013-4-12 下午2:31:14
	 */
	@Override
    public String i(
			String languageCode
			,String contentKey
			,String defaultContent
			,String[] paraValues) {
		if(defaultContent==null) {
			return contentKey;
		}
		return fixParameter(defaultContent,paraValues);
	}
	
	
	/**
	 * 处理字符串中的变量
	 * 将字符串中的<?>按照从前往后的顺序替换成参数值序列中的元素
	 * 刘虻
	 * 2010-6-2 下午05:08:16
	 * @param content 待处理字符串
	 * @param paraValues 参数值数组
	 * @return 处理后的字符串
	 */
	protected String fixParameter(String content,String[] paraValues) {
		if(paraValues==null) {
			return content;
		}
		if(content==null || content.length()<1) {
			return "";
		}
		//构建返回值
		StringBuffer reSbf = new StringBuffer();
		int lastPoint = 0; //上一个节点位置
		for(String value:paraValues) {
			//获取替换参数位置
			int point = content.indexOf("<?>",lastPoint);
			if(point<0) {
				reSbf.append(content.substring(lastPoint));
				break;
			}
			reSbf.append(content, lastPoint, point).append(SString.valueOf(value));
			lastPoint = point+3;
		}
		return reSbf.toString();
	}
}
