/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.driver.internationalization;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.internationalization.ILanguageManager;

/**
 * 语言管理工厂类
 * @author 刘虻
 * 2014-6-3 下午02:08:28
 */
@ClassInfo({"2014-06-03 13:38","语言管理工厂类"})
public class FLanguageManager {

	/**
	 * 获取语言管理类
	 * 刘虻
	 * 2010-2-2 下午03:12:59
	 * @param bean 调用类
	 */
	public static ILanguageManager getLanguageManager() {
		return new LanguageManager();
	}
}
