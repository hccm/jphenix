/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.driver.internationalization;

import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.internationalization.ILanguage;
import com.jphenix.standard.internationalization.ILanguageManager;

import java.util.HashMap;

/**
 * 空的语言管理类
 * @author 刘虻
 * 2010-3-31 上午09:54:30
 */
@ClassInfo({"2014-06-03 18:45","空的语言管理类"})
public class NullLanguageManager implements ILanguageManager {
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NullLanguageManager() {
		super();
	}
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param bean 调用类实例
	 */
	public NullLanguageManager(IBean bean) {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-3-31 上午09:54:30
	 */
	@Override
    public ILanguage getLanguge(Object obj) {
		return new NullLanguage();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2013-4-9 下午5:15:56
	 */
	@Override
    public ILanguage getLanguageByClass(Class<?> cls) {
		return new NullLanguage();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-7 下午05:30:44
	 */
	@Override
    public String getDefaultLanguageCode() {
		return "";
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-7 下午05:30:50
	 */
	public void setDefaultLanguageCode(String languageCode) {}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2013-4-12 下午2:55:03
	 */
	@Override
    public HashMap<String,String> getLanguageCodeMap() {
		return new HashMap<String,String>();
	}
}
