/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.driver.internationalization;

import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.internationalization.ILanguage;
import com.jphenix.standard.internationalization.ILanguageManager;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

/**
 * 语言管理器
 * com.jphenix.driver.internationalization.LanguageManager
 * 
 * 规则：语言配置文件需要放到使用类的同级文件夹中。
 * 文件名为：类名_语言代码.properties
 * 文件内容采用UTF-8格式
 * 
 * @author 刘虻
 * 2010-2-2 下午01:07:58
 */
@ClassInfo({"2014-06-03 18:40","语言管理器"})
@BeanInfo({"languagemanager","0","","","1"})
@Running({"90","","init"})
public class LanguageManager  implements ILanguageManager {

	protected String defLanguageCode = null; //语言编码
	protected HashMap<String,String> languageCodeMap = null; //语言代码对照容器 比如 key:zh_cn  value:chs   key小写
	
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public LanguageManager() {
		super();
	}

	/**
	 * 设置默认值
	 * 2016年4月25日
	 * @author MBG
	 */
	public void init() {
		defLanguageCode = "CHS"; //默认简体中文
		
		languageCodeMap = new HashMap<String,String>();
		languageCodeMap.put("ZH_CN","CHS"); //中文简体
		languageCodeMap.put("ZH","EN"); //简体中文
		languageCodeMap.put("ZH_TW","CHT"); //中文繁体
		languageCodeMap.put("ZH_HK","CHT"); //中文繁体
		languageCodeMap.put("EN_US","EN"); //英文
		languageCodeMap.put("EN_GB","EN"); //英文
		languageCodeMap.put("EN_CA","EN"); //英文
		languageCodeMap.put("EN_NZ","EN"); //英文
		languageCodeMap.put("EN","EN"); //英文
	}
	
	
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 下午01:07:58
	 */
	@Override
    public ILanguage getLanguge(Object obj) {
		if(obj==null) {
			return new Language(null,null,null,null);
		}
		return getLanguageByClass(obj.getClass());
	}
	
	
	/**
	 * 通过使用国际化的类，获取国际化处理类实例
	 * 刘虻
	 * 2013-4-9 下午5:13:43
	 * @param cls 使用国际化的类
	 * @return 国际化处理类实例
	 */
	@Override
    public ILanguage getLanguageByClass(Class<?> cls) {
		if(cls!=null) {
			return new Language(
					cls.getProtectionDomain().getCodeSource().getLocation()
					,cls.getName().replaceAll("(\\.)","/")
					,getDefaultLanguageCode()
					,getLanguageCodeMap());
		}
		return new Language(null,null,null,null);
	}
	
	
	/**
	 * 通过类文件路径获取国际化文本输入流
	 * 刘虻
	 * 2013-4-9 下午4:23:46
	 * @param url 类文件路径
	 * @param subPath 配置文件相对路径
	 * @return 国际化文本输入流
	 */
	protected InputStream getResourceInputStreamByUrl(URL url,String subPath) {
		if(url==null) {
			return null;
		}
		//获取类根路径
		String basePath = url.toString();
		boolean isZip = false; //是否在压缩包内
		if(basePath.length()>0) {
			//检测字符串
			String checkStr = 
				basePath.substring(basePath.length()-4).toLowerCase();
			if(".jar".equals(checkStr) || ".zip".equals(checkStr)) {
				isZip = true;
			}
		}
		if(isZip) {
			//将file: 换成   zip:
			basePath = "zip"+basePath.substring(4)+"!/";
		}else {
			//去掉 file: 头
			basePath = basePath.substring(5);
		}
		if(SFilesUtil.isFileExist(subPath,basePath)) {
			try {
				return SFilesUtil.getFileInputStreamByUrl(subPath,basePath);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 获取类的相对路径
	 * 刘虻
	 * 2010-2-2 下午01:50:58
	 * @param obj
	 * @return
	 */
	protected String getClassSubPath(Object obj) {
		if(obj==null) {
			return "";
		}
		return obj.getClass().getName().replaceAll("(\\.)","/")
								+"_"+getDefaultLanguageCode()+".properties";
	}
	
	/**
	 * 设置语言编码
	 * 刘虻
	 * 2010-2-2 下午01:13:51
	 * @param defLanguageCode 语言编码
	 */
	public void setDefaultLanguageCode(String defLanguageCode) {
		this.defLanguageCode = defLanguageCode;
	}

	/**
	 * 获取语言编码
	 * 刘虻
	 * 2010-2-2 下午01:14:02
	 * @return 语言编码
	 */
	@Override
    public String getDefaultLanguageCode() {
		return defLanguageCode;
	}
	
	/**
	 * 获取语言代码对照容器
	 * 刘虻
	 * 2013-4-12 下午2:22:48
	 * @return 语言代码对照容器
	 */
	@Override
    public HashMap<String,String> getLanguageCodeMap() {
		return languageCodeMap;
	}
	
	/**
	 * 设置语言代码对照容器
	 * 刘虻
	 * 2013-4-12 下午2:22:30
	 * @param languageCodeMap 语言代码对照容器
	 */
	public void setLanguageCodeMap(HashMap<String,String> languageCodeMap) {
		this.languageCodeMap =  languageCodeMap;
	}
}
