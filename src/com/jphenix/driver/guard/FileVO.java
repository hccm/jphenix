/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月26日
 * V4.0
 */
package com.jphenix.driver.guard;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 文件信息容器
 * 
 * 2018-06-29 增加了文件所属群组信息
 * 2018-07-26 增加了MD5值
 * 2020-10-04 增加了是否忽略该文件不做同步
 * 2020-10-06 修改了toString方法，完善了变量值信息输出
 * 
 * @author MBG
 * 2018年6月26日
 */
@ClassInfo({"2020-10-04 22:27","文件信息容器"})
public class FileVO {

	public String  id          = null;  //文件路径的MD5值
	public String  basePath    = null;  //文件跟路径（配置信息中的路径）
	public String  path        = null;  //文件路径
	public String  md5         = null;  //文件的MD5值
	public long    time        = 0;     //文件最后修改时间
	public long    length      = 0;     //文件大小
	public boolean needReboot  = false; //处理该文件后是否需要重启整个服务
	public boolean changed     = false; //文件是否发生变化
  public boolean deleted     = false; //是否被删除
  public boolean ignore      = false; //是否忽略
	public boolean isDir       = false; //是否为文件夹
	public String  group       = null;  //目标集群分组名
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public FileVO() {
		super();
	}
	
	
	/**
	 * 覆盖方法
	 */
	@Override
    public String toString() {
      return "group:[" + group + "]\nid:[" + id + "]\npath:[" + path + "]\nbasePath:[" + basePath + "]\nisDir:[" + isDir
          + "]\ntime:[" + time + "]\nlength:[" + length + "]\n md5:[" + md5 + "]\nneedReboot:[" + needReboot
          + "]\nchanged:[" + changed + "]\ndeleted:[" + deleted + "] \nignore:[" + ignore + "]";
	}
}
