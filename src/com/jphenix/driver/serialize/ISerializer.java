/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年12月21日
 * V4.0
 */
package com.jphenix.driver.serialize;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.standard.servlet.IResponse;

/**
 * 将对象实例序列化成字符串接口
 * 
 * 2020-01-20 增加了参数，判断如果返回参数是异常对象，是否抛出该异常
 * 
 * 
 * @author MBG
 * 2019年12月21日
 */
@ClassInfo({"2020-01-20 12:42","将对象实例序列化成字符串接口"})
public interface ISerializer {

	
	/**
	 * 将类实例序列化成字符串
	 * @param     obj 类实例
	 * @return    对应的字符串
	 * 2019年12月20日
	 * @author MBG
	 */
	String serialize(Object obj);
	
	
	/**
	 * 将字符串反序列化为对应的类实例
	 * @param str              字符串
	 * @param req              请求对象（可为空，只有返回IActionContext实例时才会用到）
	 * @param resp             反馈对象（可为空，只有返回IActionContext实例时才会用到）
	 * @param triggerException 如果解析的值未异常对象，是否触发抛出该异常
	 * @return                 对应的类实例
	 * 2019年12月20日
	 * @author MBG
	 */
	Object unserialize(String str,IRequest req,IResponse resp,boolean triggerException) throws Exception;
}
