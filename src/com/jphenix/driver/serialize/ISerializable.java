/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年12月21日
 * V4.0
 */
package com.jphenix.driver.serialize;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 可以被序列化的类实例接口
 * @author MBG
 * 2019年12月21日
 */
@ClassInfo({"2019-12-21 13:34","可以被序列化的类实例接口"})
public interface ISerializable {

	/**
	 * 将当前类实例进行序列化
	 * @return 序列化的数据字符串
	 * 2019年12月21日
	 * @author MBG
	 */
	String serialize();
	
	/**
	 * 将序列化字符串反向成类实例
	 * @param data 序列化数据字符串
	 * @return     对应的类实例
	 * 2019年12月21日
	 * @author MBG
	 */
	Object unserialize(String data);
}
