/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月24日
 * V4.0
 */
package com.jphenix.service.db.common.instancea;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.driver.threadpool.ThreadSession;
import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.service.db.datamanager.interfaceclass.IDataManager;
import com.jphenix.share.lang.SDate;
import com.jphenix.share.lang.SString;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.db.IBlobOuter;
import com.jphenix.standard.db.IDBQuery;
import com.jphenix.standard.db.IDBQueryExt;
import com.jphenix.standard.db.QueryPageVO;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;

/**
 * 扩展数据查询类
 * 
 * 2018-08-01 增加了分页查询方法，直接传入页号和每页记录数
 * 2018-11-08 增加了支持MsSql获取字段信息方法
 * 2018-12-24 去掉了获取表状态抛异常。操作表状态时，可以通过别名识别真实数据源
 * 2019-02-14 增加了查询并返回字符串方法，增加了分隔符参数
 * 2019-03-06 简化了程序代码
 * 2019-06-18 将表信息服务迁移到了DBQuery中，可以识别更新语句中的表名，用来更新表状态
 * 2019-08-13 增加了exec方法调用存储过程返回记录集
 * 2019-11-06 提取了拼装语句值的方法到接口中
 * 2020-09-05 增加了qb方法，查询并缓存记录集方法（自动刷新缓存）
 * 
 * @author 马宝刚
 * 2014年9月24日
 */
@ClassInfo({"2020-09-09 20:01","扩展数据查询类"})
public class DBQueryExt extends ABase implements IDBQueryExt {

	protected IDBQuery dbq                = null; //数据库操作核心类
	private String     userNameSessionKey = "_user_name_"; //用户名会话主键
	private String     userIDSessionKey   = "_user_id_"; //用户信息主键会话主键

	/**
	 * 构造函数
	 * @author 马宝刚
	 */
	public DBQueryExt(IDBQuery dbq) {
		super();
		setBase(dbq);
		this.dbq = dbq;
	}

	/**
	 * 设置用户名会话主键
	 * @param unsk 用户名会话主键
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	public void setUserNameSessionKey(String unsk) {
		this.userNameSessionKey = unsk;
	}

	/**
	 * 设置用户主键会话主键
	 * @param uisk 用户主键会话主键
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	public void setUserIDSessionKey(String uisk) {
		this.userIDSessionKey = uisk;
	}


	/**
	 * 分页查询记录
	 * 刘虻
	 * 2011-6-2 下午03:12:49
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	@Override
	public void queryPage(QueryPageVO qp) throws Exception {
		dbq.queryPage(qp);
	}

	/**
	 * 指定表数据是否发生变化
	 * @param tableName 表名
	 * @param tableState 表状态
	 * @return true表数据发生变化
	 * @throws Exception 异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	@Override
    public boolean dataChange(String tableName, String tableState) throws Exception {
		if(tableState==null || tableState.length()<1) {
			return true;
		}
		return !dbq.getDbs().getTableState(tableName,dbq.getSourceName()).equals(tableState);
	}

	/**
	 * 获取最新的表状态
	 * @param tableName 表名
	 * @return 表状态值
	 * 2014年9月26日
	 * @author 马宝刚
	 */
	@Override
    public String getTableState(String tableName){
		return dbq.getDbs().getTableState(tableName,dbq.getSourceName());
	}
	
	/**
	 * 设置最新的表状态
	 * @param tableName  表名
	 * @return           新的表状态值
	 * 2018年4月23日
	 * @author MBG
	 */
	@Override
    public String setTableState(String tableName){
		return dbq.getDbs().refreshTableState(tableName,dbq.getSourceName());
	}

	/**
	 * 通过主键值更新指定记录，如果不存在主键值，则新增记录
	 * 
	 * 数据值从提交参数中获取 简称： upk
	 * 
	 * @param tableName 表名
	 * @return 返回更新主键值
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String updateByPK(String tableName) throws Exception {
		return upk(tableName);
	}

	/**
	 * 通过主键值更新指定记录，如果不存在主键值，则新增记录
	 * 
	 * 数据值从提交参数中获取 全程 updateByPK
	 * 
	 * @param tableName 表名
	 * @return 返回更新主键值
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String upk(String tableName) throws Exception {
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			return "";
		}
		return updateByPK(tableName,ac.getSingleParameterMap());
	}

	/**
	 * 从页面请求中获取数据更新到表中 简称：ut
	 * @param tableName 表名
	 * @param where 更新条件
	 * @param uList 条件中对应的参数数组
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2014年9月26日
	 * @author 马宝刚
	 */
	@Override
    public int updateTable(String tableName, String where, String[] uList) throws Exception {
		return ut(tableName,where,uList);
	}

	/**
	 * 从页面请求中获取数据更新到表中  全称：updateTable
	 * @param tableName 表名
	 * @param where 更新条件
	 * @param uList 条件中对应的参数数组
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2014年9月26日
	 * @author 马宝刚
	 */
	@Override
    public int ut(String tableName, String where, String[] uList) throws Exception {
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			return 0;
		}
		//字段名序列
		List<String> fieldList = getFieldList(tableName);
		//提交数据容器
		HashMap<String,String> dataMap = new HashMap<String,String>();

		String fieldName; //字段名
		String fieldValue; //字段值
		for(int i=0;i<fieldList.size();i++) {
			fieldName = fieldList.get(i);
			if(!ac.hasParameter(fieldName)) {
				continue;
			}
			fieldValue = ac.getParameter(fieldName);
			dataMap.put(fieldName,fieldValue);
		}
		return updateTable(tableName,dataMap,where,uList);
	}




	/**
	 * 更新表数据
	 * @param tableName 表名
	 * @param dataMap 提交数据容器
	 * @param where 更新条件（不包含where关键字）
	 * @param uList 条件中对应的参数数组
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2014年9月26日
	 * @author 马宝刚
	 */
	@Override
    public int updateTable(String tableName, Map<String,String> dataMap, String where, String[] uList) throws Exception {
		String userName; //会话中保存的用户名
		String userID; //会话中保存的用户信息主键
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			userName = "";
			userID = "";
		}else {
			userName = SString.valueOf(ac.getSessionAttribute(userNameSessionKey));
			userID = SString.valueOf(ac.getSessionAttribute(userIDSessionKey));
		}
		//获取提交值主键序列
		List<String> keyList = BaseUtil.getMapKeyList(dataMap);
		//更新
		StringBuffer sqlSbf = new StringBuffer(); //更新语句
		sqlSbf.append("update ").append(tableName).append(" set ");
		String fieldName; //字段名
		String fieldValue; //字段值
		boolean nextValue = false;
		List<String> paraList = new ArrayList<String>(); //提交参数序列
		for(int i=0;i<keyList.size();i++) {
			fieldName = SString.valueOf(keyList.get(i));
			fieldValue = SString.valueOf(dataMap.get(fieldName));
			if(fieldValue.length()<1) {
				if("utime".equals(fieldName)) { //更新时间
					fieldValue = SDate.nowDateTimeString();
				}else if("ts".equals(fieldName)) { //时间戳
					fieldValue = SDate.getNowTS();
				}else if("uuser".equals(fieldName)) { //更新人信息主键
					if(userID.length()<1) {
						continue;
					}
					fieldValue = userID;
				}else if("uman".equals(fieldName)) { //更新人名
					if(userName.length()<1) {
						continue;
					}
					fieldValue = userName;
				}else if("uip".equals(fieldName)
						|| "aip".equals(fieldName)
						|| "ip".equals(fieldName)) {
					fieldValue = ac.cip();
				}
			}
			if(nextValue) {
				sqlSbf.append(",");
			}else {
				nextValue = true;
			}
			sqlSbf.append(fieldName).append("=?");
			paraList.add(fieldValue);
		}
		sqlSbf.append(" where ").append(where);
		if(uList!=null) {
			for(int i=0;i<uList.length;i++) {
				paraList.add(uList[i]);
			}
		}
		//执行更新
		return dbq.executeUpdate(sqlSbf.toString(),paraList);
	}

	/**
	 * 通过主键值更新指定记录，如果不存在主键值，则新增记录 简称：upk
	 * @param tableName 表名
	 * @param dataMap 数据容器
	 * @return 主键值
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String updateByPK(String tableName, Map<String,String> dataMap) throws Exception {
		return upk(tableName,dataMap);
	}

	/**
	 * 通过主键更新指定记录，如果不存在主键值，则新增记录   用defDataMap覆盖页面请求值
	 * @param tableName 表名
	 * @param defDataMap 覆盖页面提交值的容器
	 * @return 主键值
	 * @throws Exception 异常
	 * 2016年10月26日
	 * @author MBG
	 */
	@Override
    public String upkd(String tableName, Map<String,String> defDataMap) throws Exception {
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			return "";
		}
		//页面请求值
		Map<String,String> pMap = new HashMap<String,String>();
		//先放入页面请求值（不直接用页面请求对象是因为避免修改了页面请求中的值，影响后续程序）
		pMap.putAll(ac.getSingleParameterMap());
		//用defDataMap覆盖页面请求值
		pMap.putAll(defDataMap);
		return updateByPK(tableName,pMap);
	}


	/**
	 * 通过主键值更新指定记录，如果不存在主键值，则新增记录 全称：updateByPK
	 * @param tableName 表名
	 * @param dataMap 数据容器
	 * @return 主键值
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String upk(String tableName, Map<String,String> dataMap) throws Exception {
		//主键名
		String pkName = getPKFieldName(tableName);
		//主键值
		String pkValue = SString.valueOf(dataMap.get(pkName));

		//字段名序列
		List<String> fieldList = getFieldList(tableName);

		//字段名序列
		List<String> keyList = BaseUtil.getMapKeyList(dataMap);
		String fieldName; //字段名
		String fieldValue; //字段值

		String userName; //会话中保存的用户名
		String userID; //会话中保存的用户信息主键
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			userName = "";
			userID = "";
		}else {
			userName = SString.valueOf(ac.getSessionAttribute(userNameSessionKey));
			userID = SString.valueOf(ac.getSessionAttribute(userIDSessionKey));
		}

		List<String> paraList = new ArrayList<String>(); //提交值序列
		boolean nextValue = false;
		if(pkValue.length()>0) {
			//更新
			StringBuffer sqlSbf = new StringBuffer(); //更新语句
			sqlSbf.append("update ").append(tableName).append(" set ");
			for(int i=0;i<keyList.size();i++) {
				fieldName = SString.valueOf(keyList.get(i));
				if(!fieldList.contains(fieldName)) {
					continue;
				}
				if(fieldName.equals(pkName)) {
					continue;
				}
				fieldValue = SString.valueOf(dataMap.get(fieldName));
				if(nextValue) {
					sqlSbf.append(",");
				}else {
					nextValue = true;
				}
				sqlSbf.append(fieldName).append("=?");
				paraList.add(fieldValue);
			}
			if(fieldList.contains("utime") && !dataMap.containsKey("utime")) { //更新时间
				if(nextValue) {
					sqlSbf.append(",");
				}else {
					nextValue = true;
				}
				sqlSbf.append("utime").append("=?");
				paraList.add(SDate.nowDateTimeString());
			}
			if(fieldList.contains("ts")) { //时间戳
				if(nextValue) {
					sqlSbf.append(",");
				}else {
					nextValue = true;
				}
				sqlSbf.append("ts").append("=?");
				paraList.add(SDate.getNowTS());
			}
			if(fieldList.contains("uuser") && !dataMap.containsKey("uuser")) { //更新人信息主键
				if(userID.length()>0) {
					if(nextValue) {
						sqlSbf.append(",");
					}else {
						nextValue = true;
					}
					sqlSbf.append("uuser").append("=?");
					paraList.add(userID);
				}
			}
			if(fieldList.contains("uman") && !dataMap.containsKey("uman")) { //更新人名
				if(userName.length()>0) {
					if(nextValue) {
						sqlSbf.append(",");
					}else {
						nextValue = true;
					}
					sqlSbf.append("uman").append("=?");
					paraList.add(userName);
				}
			}
			sqlSbf.append(" where ").append(pkName).append("=?");
			paraList.add(pkValue);

			//执行更新
			dbq.executeUpdate(sqlSbf.toString(),paraList);
		}else {
			//新增
			pkValue = dbq.getSID();
			dataMap.put(pkName,pkValue);
			if(!keyList.contains(pkName)) {
				keyList.add(0,pkName); //放入主键字段名
			}
			StringBuffer sqlSbfA = new StringBuffer(); //新增语句A
			StringBuffer sqlSbfB = new StringBuffer(); //新增语句B

			sqlSbfA.append("insert into ").append(tableName).append("(");
			for(int i=0;i<keyList.size();i++) {
				fieldName = SString.valueOf(keyList.get(i));
				if(!fieldList.contains(fieldName)) {
					continue;
				}
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append(fieldName);
				sqlSbfB.append("?");

				paraList.add(SString.valueOf(dataMap.get(fieldName)));
			}

			if(fieldList.contains("utime") && !dataMap.containsKey("utime")) { //更新时间
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append("utime");
				sqlSbfB.append("?");

				paraList.add(SDate.nowDateTimeString());
			}
			if(fieldList.contains("ts") && !dataMap.containsKey("ts")) { //时间戳
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append("ts");
				sqlSbfB.append("?");

				paraList.add(SDate.getNowTS());
			}
			if(fieldList.contains("uuser") && !dataMap.containsKey("uuser")) { //更新人主键
				if(userID.length()>0) {
					if(nextValue) {
						sqlSbfA.append(",");
						sqlSbfB.append(",");
					}else {
						nextValue = true;
					}
					sqlSbfA.append("uuser");
					sqlSbfB.append("?");

					paraList.add(userID);
				}
			}
			if(fieldList.contains("uman") && !dataMap.containsKey("uman")) { //更新人名
				if(userName.length()>0) {
					if(nextValue) {
						sqlSbfA.append(",");
						sqlSbfB.append(",");
					}else {
						nextValue = true;
					}
					sqlSbfA.append("uman");
					sqlSbfB.append("?");

					paraList.add(userName);
				}
			}
			if(fieldList.contains("ctime") && !dataMap.containsKey("ctime")) { //建立时间
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append("ctime");
				sqlSbfB.append("?");

				paraList.add(SDate.nowDateTimeString());
			}
			if(fieldList.contains("cuser") && !dataMap.containsKey("cuser")) { //建立人信息主键
				if(userID.length()>0) {
					if(nextValue) {
						sqlSbfA.append(",");
						sqlSbfB.append(",");
					}else {
						nextValue = true;
					}
					sqlSbfA.append("cuser");
					sqlSbfB.append("?");

					paraList.add(userID);
				}
			}
			if(fieldList.contains("cman") && !dataMap.containsKey("cman")) { //建立人名
				if(userName.length()>0) {
					if(nextValue) {
						sqlSbfA.append(",");
						sqlSbfB.append(",");
					}else {
						nextValue = true;
					}
					sqlSbfA.append("cman");
					sqlSbfB.append("?");

					paraList.add(userName);
				}
			}
			if(fieldList.contains("dr") && !dataMap.containsKey("dr")) { //删除标识
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append("dr");
				sqlSbfB.append("?");

				paraList.add("0");
			}
			if(fieldList.contains("freeze") && !dataMap.containsKey("freeze")) { //冻结标识
				if(nextValue) {
					sqlSbfA.append(",");
					sqlSbfB.append(",");
				}else {
					nextValue = true;
				}
				sqlSbfA.append("freeze");
				sqlSbfB.append("?");

				paraList.add("0");
			}
			sqlSbfA.append(") values(").append(sqlSbfB).append(")");

			//执行更新
			dbq.executeUpdate(sqlSbfA.toString(),paraList);
		}
		dbq.getDbs().refreshTableState(tableName,dbq.getSourceName()); //刷新表状态
		return pkValue;
	}

	/**
	 * 构造查询字段语句段
	 * @param fHead 字段前缀  最后生成的语句  fHead.字段名
	 * @param oHead 字段别名前缀  最后生成的别名为 oHead_字段名
	 * @param tableName 表名
	 * @return 查询字段语句段
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String getFieldInfo(String fHead, String oHead, String tableName) throws Exception {
		//构建返回值
		StringBuffer reSbf = new StringBuffer();
		//字段序列
		List<String> fieldList = getFieldList(tableName);
		String fieldName; //字段名
		for(int i=0;i<fieldList.size();i++) {
			if(i>0) {
				reSbf.append(",");
			}
			fieldName = fieldList.get(i);
			if(fHead!=null && fHead.length()>0) {
				reSbf.append(fHead).append(".").append(fieldName);
			}else {
				reSbf.append(fieldName);
			}
			if(oHead!=null && oHead.length()>0) {
				reSbf.append(" ").append(oHead).append("_").append(fieldName);
			}
		}
		return reSbf.toString();
	}




	/**
	 * 通过表主键值获取指定一条记录
	 * @param tableName 表名
	 * @param id 主键值
	 * @param dictBeans 数据字典类数组
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> queryByPK(String tableName, String id, Object[] dictBeans) throws Exception {
		if(id==null || id.length()<1) {
			//从会话线程中获取动作上下文，如果存在的话
			IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
			if(ac==null) {
				return new HashMap<String,String>();
			}
			id = str(ac.getParameter(getPKFieldName(tableName)));
		}
		//sql语句
		StringBuffer sqlSbf = new StringBuffer();
		//字段序列
		List<String> fieldList = getFieldList(tableName);
		sqlSbf.append("select ");
		for(int i=0;i<fieldList.size();i++) {
			if(i>0) {
				sqlSbf.append(",");
			}
			sqlSbf.append(fieldList.get(i));
		}
		sqlSbf.append(" from ").append(tableName).append(" where ").append(getPKFieldName(tableName)).append("=?");
		return dbq.queryOne(sqlSbf.toString(),new String[] {id},dictBeans);
	}

	/**
	 * 删除指定主键值记录  全称：deleteByPK
	 * 如果表中存在dr字段，则为标记删除，否则是真删
	 * @param tableName 表名
	 * @param id 主键值
	 * @return 影响记录数
	 * @throws Exception 异常
	 * 2014年10月16日
	 * @author 马宝刚
	 */
	@Override
    public int dpk(String tableName, String id) throws Exception {
		//sql语句
		StringBuffer sqlSbf = new StringBuffer();
		if(getFieldList(tableName).contains("dr")) {
			sqlSbf.append("update ").append(tableName).append(" set dr='1' where ").append(getPKFieldName(tableName)).append("=?") ;
		}else {
			sqlSbf.append("delete ").append(tableName).append(" where ").append(getPKFieldName(tableName)).append("=?");
		}
		return dbq.executeUpdate(sqlSbf.toString(),new String[] {id});
	}


	/**
	 * 删除指定主键值记录 简称：dpk
	 * 如果表中存在dr字段，则为标记删除，否则是真删
	 * @param tableName 表名
	 * @param id 主键值
	 * @return 影响记录数
	 * @throws Exception 异常
	 * 2014年10月16日
	 * @author 马宝刚
	 */
	@Override
    public int deleteByPK(String tableName, String id) throws Exception {
		return dpk(tableName,id);
	}



	/**
	 * 根据主键值删除表记录 简称：dpk
	 * 如果表中存在dr字段，则为标记删除，否则是真删
	 * @param tableName   表名
	 * @return 影响记录数
	 * @throws Exception  异常
	 * 2016年10月7日
	 * @author MBG
	 */
	@Override
    public int deleteByPK(String tableName) throws Exception {
		return dpk(tableName);
	}

	/**
	 * 根据主键值删除表记录 全称：deleteByPK
	 * 如果表中存在dr字段，则为标记删除，否则是真删
	 * @param tableName   表名
	 * @return 影响记录数
	 * @throws Exception  异常
	 * 2016年10月7日
	 * @author MBG
	 */
	@Override
    public int dpk(String tableName) throws Exception {
		//从会话线程中获取动作上下文，如果存在的话
		IActionContext ac = (IActionContext)ThreadSession.get("_action_context");
		if(ac==null) {
			return 0;
		}
		//主键名
		String pkName = getPKFieldName(tableName);
		//主键值
		String pkValue = ac.getParameter(pkName);
		//sql语句
		StringBuffer sqlSbf = new StringBuffer();
		if(getFieldList(tableName).contains("dr")) {
			sqlSbf.append("update ").append(tableName).append(" set dr='1' where ").append(pkName).append("=?") ;
		}else {
			if("oracle".equalsIgnoreCase(getSourceType())) {
				sqlSbf.append("delete ").append(tableName).append(" where ").append(pkName).append("=?");
			}else {
				sqlSbf.append("delete from ").append(tableName).append(" where ").append(pkName).append("=?");
			}
		}
		return dbq.executeUpdate(sqlSbf.toString(),new String[] {pkValue});
	}



	/**
	 * 如果提交上来主键值，则对指定记录执行更新，否则执行新增
	 * @param tableName 表名
	 * @param ac 动作上下文
	 * @return 返回主键值
	 * @throws Exception 异常
	 * 2014年10月16日
	 * @author 马宝刚
	 */
	@Override
    public String updateByPK(String tableName, IActionContext ac) throws Exception {
		return updateByPK(tableName,ac.getSingleParameterMap());
	}

	/**
	 * 通过表主键值获取指定一条记录
	 * @param tableName 表名
	 * @param id 主键值
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> queryByPK(String tableName, String id) throws Exception {
		return queryByPK(tableName,id,null);
	}

	/**
	 * 通过表主键值获取指定一条记录 简称：qpk
	 * @param tableName 表名
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2016年10月9日
	 * @author MBG
	 */
	@Override
    public Map<String,String> queryByPK(String tableName) throws Exception {
		return queryByPK(tableName,null,null);
	}

	/**
	 * 通过表主键值获取指定一条记录 简称：qpk
	 * @param tableName 表名
	 * @param dicts     字典对象数组
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2016年10月9日
	 * @author MBG
	 */
	@Override
    public Map<String,String> queryByPK(String tableName, Object[] dicts) throws Exception {
		return queryByPK(tableName,null,dicts);
	}


	/**
	 * 通过表主键值获取指定一条记录 全称：queryByPK
	 * @param tableName 表名
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2016年10月9日
	 * @author MBG
	 */
	@Override
    public Map<String,String> qpk(String tableName) throws Exception {
		return queryByPK(tableName,null,null);
	}

	/**
	 * 通过表主键值获取指定一条记录 全称：queryByPK
	 * @param tableName 表名
	 * @param idValue   主键值
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2016年10月9日
	 * @author MBG
	 */
	@Override
    public Map<String,String> qpk(String tableName, String idValue) throws Exception {
		return queryByPK(tableName,idValue,null);
	}

	/**
	 * 通过表主键值获取指定一条记录 全称：queryByPK
	 * @param tableName 表名
	 * @param dicts     字典对象数组
	 * @return 一条记录集
	 * @throws Exception 异常
	 * 2016年10月9日
	 * @author MBG
	 */
	@Override
    public Map<String,String> qpk(String tableName, Object[] dicts) throws Exception {
		return queryByPK(tableName,null,dicts);
	}




	/**
	 * 查询指定主键值的记录集
	 * @param pk    主键值
	 * @param tableName 表名
	 * @return 行记录
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> getRowDataByPK(String pk, String tableName) throws Exception {
		//sql语句
		StringBuffer sqlSbf = new StringBuffer();
		//主键名
		String pkFieldName = getPKFieldName(tableName);
		//字段序列
		List<String> fieldList = getFieldList(tableName);

		sqlSbf.append("select ");
		for(int i=0;i<fieldList.size();i++) {
			if(i>0) {
				sqlSbf.append(",");
			}
			sqlSbf.append(fieldList.get(i));
		}
		sqlSbf.append(" from ").append(tableName).append(" where ").append(pkFieldName).append("=?");

		return dbq.queryOne(sqlSbf.toString(),new String[] {SString.valueOf(pk)});
	}

	/**
	 * 获取指定表的主键字段名
	 * @param tableName 表名
	 * @return  主键字段名
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public String getPKFieldName(String tableName) throws Exception {
		if(tableName==null) {
			return "";
		}
		//主键字段名
		String pkFieldName = dbq.getDbs().getPKFieldName(tableName,dbq.getSourceName());
		if(pkFieldName.length()<1) {
			String dsType = dbq.getSourceType().toLowerCase(); //数据源类型
			if("oracle".equals(dsType)) {
				//构造查询字段信息语句
				String sql = "select col.column_name from user_constraints con,  user_cons_columns col where con.constraint_name = col.constraint_name and con.constraint_type='P' and col.table_name = ?";
				//查询记录集
				Map<String,String> rsm = dbq.queryOne(sql,new String[] {tableName.toUpperCase()});
				pkFieldName = SString.valueOf(rsm.get("column_name")).toLowerCase();
			}else if("h2".equals(dsType)) {
				//构造查询字段信息语句
				String sql = "SELECT column_name  FROM INFORMATION_SCHEMA.INDEXES where table_name=?";
				//查询记录集
				Map<String,String> rsm = dbq.queryOne(sql,new String[] {tableName.toUpperCase()});
				pkFieldName = SString.valueOf(rsm.get("column_name")).toLowerCase();
			}else if("mysql".equals(dsType)) {
				//构造查询字段信息语句
				String sql = "SELECT column_name FROM information_schema.columns WHERE table_name=? AND column_key='PRI'";
				//查询记录集
				Map<String,String> rsm = dbq.queryOne(sql,new String[] {tableName.toUpperCase()});
				pkFieldName = SString.valueOf(rsm.get("column_name")).toLowerCase();
			}else if("mssql".equals(dsType)) {
				//构造查询字段信息语句
				String sql = "select a.name as column_name from syscolumns a inner join (select max(id) as id ,name from sysobjects where xtype='u' group by name) b on a.id=b.id where b.name=? and  (SELECT count(0) as ct FROM sysobjects WHERE (name in (SELECT name FROM sysindexes  WHERE (id = a.id)  AND (indid in (SELECT indid FROM sysindexkeys  WHERE (id = a.id) AND (colid in (SELECT colid FROM syscolumns  WHERE (id = a.id) AND (name = a.name))))))) AND (xtype = 'PK'))>0";
				//查询记录集
				Map<String,String> rsm = dbq.queryOne(sql,new String[] {tableName.toUpperCase()});
				pkFieldName = SString.valueOf(rsm.get("column_name")).toLowerCase();
			}
			dbq.getDbs().setPkFieldName(pkFieldName,tableName,dbq.getSourceName());
		}
		return pkFieldName;
	}

	/**
	 * 获取指定表的字段序列
	 * @param tableName 表名
	 * @return 字段序列
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public List<String> getFieldList(String tableName) throws Exception {
		if(tableName==null) {
			return new ArrayList<String>();
		}
		//从缓存中获取字段信息序列
		List<String> fList = dbq.getDbs().getFieldList(tableName,dbq.getSourceName());
		if(fList.size()<1) {
			String dsType = dbq.getSourceType().toLowerCase(); //数据源类型
			if("oracle".equals(dsType)) {
				//构造查询字段信息语句
				String sql = "select column_name from user_tab_columns where table_name=?";
				//查询记录集
				List<Map<String,String>> rs = dbq.queryList(sql,new String[] {tableName.toUpperCase()});
				for(int i=0;i<rs.size();i++) {
					fList.add(SString.valueOf(rs.get(i).get("column_name")).toLowerCase());
				}
			}else if("h2".equals(dsType)) {
				//h2数据库
				//构造查询字段信息语句
				String sql = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME =? order by ordinal_position asc";
				//查询记录集
				List<Map<String,String>> rs = dbq.queryList(sql,new String[] {tableName.toUpperCase()});
				for(int i=0;i<rs.size();i++) {
					fList.add(SString.valueOf(rs.get(i).get("column_name")).toLowerCase());
				}
			}else if("mysql".equals(dsType)) {
				//mysql数据库
				//构造查询字段信息语句
				String sql = "SELECT column_name FROM information_schema.columns WHERE table_name=? ORDER BY ordinal_position";
				//查询记录集
				List<Map<String,String>> rs = dbq.queryList(sql,new String[] {tableName.toUpperCase()});
				for(int i=0;i<rs.size();i++) {
					fList.add(SString.valueOf(rs.get(i).get("column_name")).toLowerCase());
				}
			}else if("mssql".equals(dsType)) {
				//mssql数据库
				String sql = "select a.name as column_name from syscolumns a inner join (select max(id) as id ,name from sysobjects where xtype='u' group by name) b on a.id=b.id where b.name=?";
				//查询记录集
				List<Map<String,String>> rs = dbq.queryList(sql,new String[] {tableName.toUpperCase()});
				for(int i=0;i<rs.size();i++) {
					fList.add(SString.valueOf(rs.get(i).get("column_name")).toLowerCase());
				}
			}
		}
		//构建一个新的序列，避免使用的程序修改序列中的内容，导致其它程序调用时受影响
		List<String> reList = new ArrayList<String>();
		reList.addAll(fList);
		return reList;
	}

	/**
	 * 获取当前日期时间
	 * 刘虻
	 * 2011-6-2 下午03:22:23
	 * @return 当前日期时间
	 */
	@Override
	public String getDateTime() {
		return dbq.getDateTime();
	}

	/**
	 * 获取当前时间
	 * 刘虻
	 * 2011-6-2 下午03:22:30
	 * @return 当前时间
	 */
	@Override
	public String getTime() {
		return dbq.getTime();
	}

	/**
	 * 获取当前日期时间
	 * 刘虻
	 * 2011-6-2 下午03:22:23
	 * @return 当前日期时间
	 */
	@Override
	public String getDate() {
		return dbq.getDate();
	}

	/**
	 * 获取新的主键值
	 * 刘虻
	 * 2011-6-2 下午03:17:28
	 * @return 新的主键值
	 */
	@Override
	public String getSID() {
		return dbq.getSID();
	}


	/**
	 * 获取查询记录数
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	@Override
	public void queryAllCount(QueryPageVO qp) throws Exception {
		dbq.queryAllCount(qp);
	}

	/**
	 * 获取指定行记录集中指定字段值
	 * 刘虻
	 * 2011-6-2 下午05:06:11
	 * @param fieldPK   字段主键
	 * @param row       行号
	 * @param rs        记录集
	 * @return          字段值
	 */
	@Override
	public String rss(String fieldPK, int row, List<Map<String, String>> rs) {
		return dbq.rss(fieldPK,row,rs);
	}

	/**
	 * 获取首行记录指定字段值
	 * 刘虻
	 * 2011-6-2 下午05:06:50
	 * @param fieldPK   字段主键
	 * @param rs        记录集
	 * @return          字段值
	 */
	@Override
	public String frss(String fieldPK, List<Map<String, String>> rs) {
		return dbq.frss(fieldPK,rs);
	}

	/**
	 * 更新CLob字段
	 * 
	 * 再此之前需要在insert语句中将CLob字段设置为 empty_clob()函数值
	 * 
	 * @author 刘虻
	 * 2009-9-21下午07:48:59
	 * @param clobSql 更新语句 select content from table_name where id=? for update
	 * @param uList  语句中对应的条件值
	 * @param fieldName CLob字段名
	 * @param fieldValue CLob 字段值
	 * @throws Exception 执行发生异常
	 */
	@Deprecated
	@Override
	public void updateClob(String clobSql, List<String> uList, String fieldName, String fieldValue)throws Exception {
		dbq.updateClob(clobSql,uList,fieldName,fieldValue);
	}

	/**
	 * 执行更新
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值数组
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public int executeUpdate(String sql, String[] uValueArrl) throws Exception {
		return dbq.executeUpdate(sql,uValueArrl);
	}

	/**
	 * 执行更新
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值序列
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public int executeUpdate(String sql, List<String> uValueArrl)throws Exception {
		return dbq.executeUpdate(sql,uValueArrl);
	}

	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值数组
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public List<Map<String, String>> queryList(String sql, String[] uValueArrl) throws Exception {
		return dbq.queryList(sql,uValueArrl);
	}


	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public List<Map<String, String>> queryList(String sql) throws Exception {
		return dbq.queryList(sql);
	}

	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值序列
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public List<Map<String, String>> queryList(String sql,List<String> uValueArrl) throws Exception {
		return dbq.queryList(sql,uValueArrl);
	}

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值素组
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public Map<String, String> queryOne(String sql, String[] uValueArrl) throws Exception {
		return dbq.queryOne(sql,uValueArrl);
	}

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public Map<String, String> queryOne(String sql) throws Exception {
		return dbq.queryOne(sql);
	}

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public Map<String, String> queryOne(String sql, List<String> uValueArrl) throws Exception {
		return dbq.queryOne(sql,uValueArrl);
	}

	/**
	 * 设置数据源主键
	 * @param dsName 数据源主键
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public void setDataSource(String dsName) {
		dbq.setDataSource(dsName);
	}

	/**
	 * 更新Blob字段值
	 * @param blobSql 更新语句  slect content from table_name where id=? for update
	 * @param uList 设置Clob条件值对象
	 * @param fieldName BLob字段名
	 * @param blobIs BLob 字段值读入流
	 * @throws Exception 执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public void updateBlob(String blobSql, List<String> uList,
			String fieldName, InputStream blobIs) throws Exception {
		dbq.updateBlob(blobSql,uList,fieldName,blobIs);
	}

	/**
	 * 更新Blob字段值
	 * @param blobSql 更新语句  slect content from table_name where id=? for update
	 * @param pk 主键值
	 * @param fieldName BLob字段名
	 * @param blobIs BLob 字段值读入流
	 * @throws Exception 执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public void updateBlobByPK(String blobSql, String pk, String fieldName,InputStream blobIs) throws Exception {
		dbq.updateBlobByPK(blobSql,pk,fieldName,blobIs);
	}

	/**
	 * 获取Blob字段值 读入流
	 * @param sql            取数语句   
	 * 语句中只能有一个blob字段，并且用主键定位 比如  select blobContent from table where id=?
	 * @param pkValue    主键值
	 * @param blobOuter blob字段输出处理类
	 * @throws Exception 异常
	 * 2014年4月11日
	 * @author 马宝刚
	 */
	@Deprecated
	@Override
	public void queryBlob(String sql, String pkValue,IBlobOuter blobOuter) throws Exception {
		dbq.queryBlob(sql,pkValue,blobOuter);
	}


	/**
	 * 调用存储过程
	 * @param sql              sql语句
	 * @param params       传入参数数组
	 * @param types          对应的参数类型数组
	 * @param isOuts        是否为传出参数
	 * @return                   传出参数序列
	 * @throws Exception  异常
	 * 2014年5月23日
	 * @author 马宝刚
	 */
	@Override
	public List<Object> call(String sql, String[] params, int[] types,boolean[] isOuts) throws Exception {
		return dbq.call(sql,params,types,isOuts);
	}

	/**
	 * 获取数据源类型字符串
	 * @return 数据源类型字符串
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
	public String getSourceType() {
		return dbq.getSourceType();
	}

	/**
	 * 获取数据源主键
	 * @return 数据源主键
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
	public String getSourceName() {
		return dbq.getSourceName();
	}

	/**
	 * 查询记录集
	 * @param sql        查询语句
	 * @param uList      查询提交值序列
	 * @param dicts      数据字典类序列
	 * @return           查询记录集
	 * @throws Exception 异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	@Override
	public List<Map<String, String>> queryList(String sql, List<String> uList,Object[] dicts) throws Exception {
		return dbq.queryList(sql,uList,dicts,null);
	}
	
	/**
	 * 查询记录集
	 * @param sql             查询语句
	 * @param uList           查询提交值序列
	 * @param dicts           数据字典类序列
	 * @param rs              待插入数据的序列（如果存在该值，返回值也是该值）
	 * @return                查询记录集
	 * @throws Exception      异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	@Override
	public List<Map<String, String>> queryList(String sql, List<String> uList,Object[] dicts,List<Map<String,String>> rs) throws Exception {
		return dbq.queryList(sql,uList,dicts,rs);
	}

	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值数组
	 * @param dicts              数据字典序列
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
	public List<Map<String, String>> queryList(String sql, String[] uValueArrl,Object[] dicts) throws Exception {
		return dbq.queryList(sql,uValueArrl,dicts);
	}

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uList             提交值序列
	 * @param dicts             数据字典类序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
	public Map<String, String> queryOne(String sql, List<String> uList, Object[] dicts) throws Exception {
		return dbq.queryOne(sql,uList,dicts);
	}


	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值素组
	 * @param dicts               数据字典类序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
	public Map<String, String> queryOne(String sql, String[] uValueArrl,Object[] dicts) throws Exception {
		return dbq.queryOne(sql,uValueArrl,dicts);
	}

	/**
	 * 获取数据库链接
	 * @return 数据库链接对象
	 * @throws Exception 异常
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	@Override
    public Connection getConn() throws Exception {
		return dbq.getConn();
	}

	/**
	 * 获取数据库链接
	 * @param sessionKey        会话主键（事务处理）
	 * @return 数据库链接对象
	 * @throws Exception 异常
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	@Override
    public Connection getConn(String sessionKey) throws Exception {
		return dbq.getConn(sessionKey);
	}



	/**
	 * 调用存储过程  全称 call
	 * @param sql              sql语句
	 * @param params       传入参数数组
	 * @param types          对应的参数类型数组
	 * @param isOuts        是否为传出参数
	 * @return                   传出参数序列
	 * @throws Exception  异常
	 * 2014年5月23日
	 * @author 马宝刚
	 */
	@Override
    public List<Object> c(String sql, String[] params, int[] types, boolean[] isOuts) throws Exception {
		return dbq.c(sql,params,types,isOuts);
	}

	/**
	 * 执行更新 全称 executeUpdate
	 * @param sql                       更新语句
	 * @param uList                  更新值序列
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public int e(String sql, List<String> uList) throws Exception {
		return dbq.e(sql,uList);
	}


	/**
	 * 执行更新 全称 executeUpdate
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值数组
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public int e(String sql, String[] uValueArrl) throws Exception {
		return dbq.e(sql,uValueArrl);
	}


	/**
	 * 更新Blob字段值 全称 updateBlobByPK
	 * @param blobSql 更新语句  slect content from table_name where id=? for update
	 * @param pk 主键值
	 * @param fieldName BLob字段名
	 * @param blobIs BLob 字段值读入流
	 * @throws Exception 执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	@Override
    public void u(
			String blobSql
			,String pk
			,String fieldName
			,InputStream blobIs) throws Exception {
		dbq.u(blobSql,pk,fieldName,blobIs);
	}


	/**
	 * 获取数据库连接池管理类
	 * @return 数据库连接池管理类
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	@Override
    public IDataManager dm() {
		return  dbq.dm();
	}


	/**
	 * 获取查询记录数 全称 queryAllCount
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	@Override
    public void qc(QueryPageVO qp) throws Exception {
		dbq.qc(qp);
	}


	/**
	 * 获取Blob字段值 读入流  全称 queryList
	 * @param sql            取数语句   
	 * 语句中只能有一个blob字段，并且用主键定位 比如  select blobContent from table where id=?
	 * @param pkValue    主键值
	 * @param blobOuter blob字段输出处理类
	 * @throws Exception 异常
	 * 2014年4月11日
	 * @author 马宝刚
	 */
	@Override
    public void q(String sql, String pkValue, IBlobOuter blobOuter) throws Exception {
		dbq.q(sql,pkValue,blobOuter);
	}


	/**
	 * 查询并返回记录集序列  全称 queryList
	 * @param sql                   查询语句
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(String sql) throws Exception {
		return dbq.q(sql);
	}

	/**
	 * 查询并返回记录集序列 全称 queryList
	 * @param sql                   查询语句
	 * @param uList        更新值序列
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(String sql, List<String> uList) throws Exception {
		return dbq.q(sql,uList);
	}


	/**
	 * 查询记录集 全称 queryList
	 * @param sql              查询语句
	 * @param uList           查询提交值序列
	 * @param dicts           数据字典类序列
	 * @return                  查询记录集
	 * @throws Exception 异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(String sql, List<String> uList, Object[] dicts) throws Exception {
		return dbq.q(sql,uList,dicts);
	}


	/**
	 * 查询并返回记录集序列 全称  queryList
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值数组
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(String sql, String[] uValueArrl) throws Exception {
		return dbq.q(sql,uValueArrl);
	}


	/**
	 * 查询并返回记录集序列 全称  queryList
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值数组
	 * @param dicts              数据字典序列
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(String sql, String[] uValueArrl, Object[] dicts) throws Exception {
		return dbq.q(sql,uValueArrl,dicts);
	}


	/**
	 * 查询结果并返回第一条记录 全称  queryOne
	 * @param sql                   查询语句
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(String sql) throws Exception {
		return dbq.o(sql);
	}


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(String sql, List<String> uValueArrl) throws Exception {
		return dbq.o(sql,uValueArrl);
	}

	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql                   查询语句
	 * @param uList             提交值序列
	 * @param dicts             数据字典类序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(String sql, List<String> uList, Object[] dicts) throws Exception {
		return dbq.o(sql,uList,dicts);
	}


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值素组
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(String sql, String[] uValueArrl) throws Exception {
		return dbq.o(sql,uValueArrl);
	}


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值素组
	 * @param dicts               数据字典类序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(String sql, String[] uValueArrl, Object[] dicts) throws Exception{
		return dbq.o(sql,uValueArrl,dicts);
	}

	/**
	 * 获取查询记录数  全称 queryPage
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	@Override
    public void q(QueryPageVO qp) throws Exception {
		dbq.q(qp);
	}


	/**
	 * 执行更新
	 * @param sql 更新语句
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2015年11月28日
	 * @author 马宝刚
	 */
	@Override
    public int executeUpdate(String sql) throws Exception {
		return dbq.executeUpdate(sql,(List<String>)null);
	}

	/**
	 * 执行更新 全称：executeUpdate
	 * @param sql 更新语句
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2015年11月28日
	 * @author 马宝刚
	 */
	@Override
    public int e(String sql) throws Exception {
		return dbq.executeUpdate(sql,(List<String>)null);
	}


	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(String sql, String[] updates) throws Exception {
		return dbq.qp(sql,updates);
	}

	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO queryPage(String sql, String[] updates) throws Exception {
		return dbq.queryPage(sql,updates);
	}


	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @param dicts 字典主键对象
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(
			String sql,String[] updates,Object[] dicts) throws Exception {
		return dbq.qp(sql,updates,dicts);
	}

	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @param dicts 字典主键对象
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO queryPage(
			String sql,String[] updates,Object[] dicts) throws Exception {
		return dbq.queryPage(sql,updates,dicts);
	}


	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(String sql) throws Exception {
		return dbq.qp(sql);
	}

	/**
	 * 执行分页查询
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO queryPage(String sql) throws Exception {
		return dbq.queryPage(sql);
	}


	/**
	 * 更新数据
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * 刘虻
	 * 2011-6-2 下午03:07:50
	 * @param sql			更新语句
	 * @param params		提交值对象序列
	 * @return				更新记录数
	 * @throws Exception	异常
	 */
	@Override
    public int executeUpdate(String sql, Object[] params) throws Exception {
		return dbq.executeUpdate(sql,params);
	}


	/**
	 * 执行更新 全称 executeUpdate
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql                       更新语句
	 * @param params            		更新值数组
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public int e(String sql, Object[] params) throws Exception {
		return dbq.e(sql,params);
	}


	/**
	 * 查询记录集
	 * 
	 * 根据不同的提交值对象类型，设置不同的类型
	 * 
	 * 
	 * @param sql              查询语句
	 * @param params           查询提交值对象数组
	 * @param dicts           数据字典类序列
	 * @return                  查询记录集
	 * @throws Exception 异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String, String>> queryList(String sql, Object[] params, Object[] dicts)throws Exception {
		return dbq.queryList(sql,params,dicts);
	}


	/**
	 * 查询结果并返回第一条记录
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql					查询语句
	 * @param params				提交值对象数组
	 * @return						记录集行对象
	 * @throws Exception		异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String, String> o(String sql, Object[] params) throws Exception {
		return dbq.o(sql,params);
	}


	/**
	 * 查询结果并返回第一条记录
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql					查询语句
	 * @param paras			提交值对象数组
	 * @param dict               数据字典类序列
	 * @return						记录集行对象
	 * @throws Exception		异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String, String> o(String sql, Object[] paras, Object[] dict) throws Exception {
		return dbq.o(sql,paras,dict);
	}


	/**
	 * 查询并返回记录集序列
	 * 
	 * 通过传入不同类型的提交值，设置不同的传入值类型
	 * 
	 * 
	 * @param sql					查询语句
	 * @param params		更新值数组
	 * @return						记录集序列
	 * @throws Exception		异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String, String>> q(String sql, Object[] params) throws Exception {
		return dbq.q(sql,params);
	}

	/**
	 * 查询并返回记录集序列
	 * 
	 * 用于不同类型的提交值，设置不同类型
	 * 
	 * 
	 * @param sql               查询语句
	 * @param params        更新值数组
	 * @param dicts             数据字典序列
	 * @return                  记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String, String>> q(String sql, Object[] params, Object[] dicts) throws Exception {
		return dbq.q(sql,params,dicts);
	}


	/**
	 * 执行分页查询
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值对象数组
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(String sql, Object[] updates) throws Exception {
		return dbq.qp(sql,updates);
	}


	/**
	 * 执行分页查询
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @param dicts 字典主键对象
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(String sql, Object[] updates, Object[] dicts) throws Exception {
		return dbq.qp(sql,updates,dicts);
	}


	/**
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param params      提交值数组
	 * @param onlyOne     是否只查询头一条记录
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	@Override
    public String queryString(String sql, Object[] params, boolean onlyOne) throws Exception {
		return dbq.queryString(sql,params,onlyOne);
	}


	/**
	 * 简称：qs
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	@Override
    public String queryString(String sql, String[] uValueArrl) throws Exception {
		return dbq.queryString(sql,uValueArrl,false,null);
	}

	/**
	 * 全称：queryString
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	@Override
    public String qs(String sql, String[] uValueArrl) throws Exception {
		return dbq.queryString(sql,uValueArrl,false,null);
	}

	/**
	 * 获取第一条记录，将第一条记录作为字符串输出
	 * col1|col2|col3|col4
	 * @param sql 查询语句
	 * @param uValueArrl 提交值数组
	 * @return 记录字符串
	 * @throws Exception 异常
	 * 2016年10月17日
	 * @author MBG
	 */
	@Override
    public String os(String sql, String[] uValueArrl) throws Exception {
		return dbq.os(sql,uValueArrl);
	}

	/**
	 * 获取第一条记录，将第一条记录作为字符串输出
	 * col1|col2|col3|col4
	 * @param sql 查询语句
	 * @return 记录字符串
	 * @throws Exception 异常
	 * 2016年10月17日
	 * @author MBG
	 */
	@Override
    public String os(String sql) throws Exception {
		return dbq.os(sql);
	}

	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param        infos       提交值信息
	 * @return              记录集序列
	 * @throws Exception	异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(Object[] infos) throws Exception {
		return dbq.q(infos);
	}

	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param infos         提交值信息
	 * @param dicts         字典对象实例数组
	 * @return              记录集序列
	 * @throws Exception	异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(Object[] infos, Object[] dicts) throws Exception {
		return dbq.q(infos,dicts);
	}
	
	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param infos         提交值信息
	 * @param dicts         字典对象实例数组
	 * @param rs            待插入数据的序列（如果存在该值，返回值也是该值）
	 * @return              记录集序列
	 * @throws Exception	异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	@Override
    public List<Map<String,String>> q(Object[] infos, Object[] dicts,List<Map<String,String>> rs) throws Exception {
		return dbq.q(infos,dicts,rs);
	}
	
	
	/**
	 * 执行无分页查询记录集，如果从上次查询到当前，该表的数据没有发生变化，则不做查询，直接返回true
	 * 
	 * 注意：切记！！！！ 该方法禁止查询频繁增长，或者数据记录过多的表，会导致内存溢出！！！ 切记！切记
	 * 
	 * 该方法通常用来缓存数据字典，等固定数据记录的表
	 * 
	 * @param caller     调用者（通常为服务类实例，一个服务类只允许一个类实例操作同一个数据源的一张表，不允许同一个类的多个类实例操作同一个数据源的同一张表）
	 * @param tableName  指定表名，多个表用逗号分隔
	 * @param rs         空记录集序列或者上次查询好的记录集（如果该表数据没发生变化，则不再更新该记录集序列）
	 * @param infos      提交语句与插入值信息数组
	 * @return           是否重新从数据库读取了数据
	 * @throws Exception 异常
	 * 2020年9月5日
	 * @author MBG
	 */
	public boolean qb(
			Object caller,
			String tableName,
			List<Map<String,String>> rs,
			Object[] infos) throws Exception {
		if(rs==null) {
			return false;
		}
		//记录集为空时强制更新表状态，并且返回真
		if(!dbq.getDbs().dataChange(caller,dbq.getSourceName(),tableName,rs.size()<1)) {
			//传入参数不合法，或者数据没有发生变化
			return false;
		}
		rs.clear(); //清空历史记录
		
		dbq.q(infos,null,rs); //执行查询
		
		return true;
	}
	
	/**
	 * 执行无分页查询记录集，如果从上次查询到当前，该表的数据没有发生变化，则不做查询，直接返回true
	 * 
	 * 注意：切记！！！！ 该方法禁止查询频繁增长，或者数据记录过多的表，会导致内存溢出！！！ 切记！切记
	 * 
	 * 该方法通常用来缓存数据字典，等固定数据记录的表
	 * 
	 * @param caller     调用者（通常为服务类实例，一个服务类只允许一个类实例操作同一个数据源的一张表，不允许同一个类的多个类实例操作同一个数据源的同一张表）
	 * @param rs         空记录集序列或者上次查询好的记录集（如果该表数据没发生变化，则不再更新该记录集序列）
	 * @param tableName  指定表名，多个表用逗号分隔
	 * @param infos      提交语句与插入值信息数组
	 * @param dicts      需要处理记录集的字典实例数组
	 * @return           是否重新从数据库读取了数据
	 * @throws Exception 异常
	 * 2020年9月5日
	 * @author MBG
	 */
	public boolean qb(
			Object caller,
			String tableName,
			List<Map<String,String>> rs,
			Object[] infos,
			Object[] dicts) throws Exception {
		if(rs==null) {
			return false;
		}
		//记录集为空时强制更新表状态，并且返回真
		if(!dbq.getDbs().dataChange(caller,dbq.getSourceName(),tableName,rs.size()<1)) {
			//传入参数不合法，或者数据没有发生变化
			return false;
		}
		rs.clear(); //清空历史记录
		
		dbq.q(infos,dicts,rs); //执行查询
		
		return true;
	}


	/**
	 * 执行分页查询 （用于复杂语句拼装接入）
	 * @param infos 提交值信息
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(Object[] infos) throws Exception {
		return dbq.qp(infos);
	}

	/**
	 * 执行分页查询 （用于复杂语句拼装接入）
	 * @param infos 提交值信息
	 * @param dicts         字典对象实例数组
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */
	@Override
    public QueryPageVO qp(Object[] infos, Object[] dicts) throws Exception {
		return dbq.qp(infos,dicts);
	}

	/**
	 * 将查询出的记录集采用这种方式输出为字符串  （用于复杂语句拼装接入）
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param infos 提交值信息
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */
	@Override
    public String qs(Object[] infos) throws Exception {
		return dbq.qs(infos);
	}

	/**
	 * 获取第一条记录，将第一条记录作为字符串输出 （用于复杂语句拼装接入）
	 * col1|col2|col3|col4
	 * @param infos 提交值信息
	 * @return 记录字符串
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */
	@Override
    public String os(Object[] infos) throws Exception {
		return dbq.os(infos);
	}

	/**
	 * 查询结果并返回第一条记录 （用于复杂语句拼装接入）
	 * @param infos             提交值信息
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(Object[] infos) throws Exception {
		return dbq.o(infos);
	}

	/**
	 * 查询结果并返回第一条记录 （用于复杂语句拼装接入）
	 * @param infos             提交值信息
	 * @param dicts             数据字典类序列
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public Map<String,String> o(Object[] infos, Object[] dicts) throws Exception {
		return dbq.o(infos,dicts);
	}


	/**
	 * 执行更新 （用于复杂语句拼装接入）
	 * @param infos             提交值信息
	 * @return                  返回更新状态
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	@Override
    public int e(Object[] infos) throws Exception {
		return dbq.e(infos);
	}

	/**
	 * 获取数据库连接池管理类
	 * @return 数据库连接池管理类
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	@Override
    public IDataManager getDataManager() {
		return dbq.getDataManager();
	}

	/**
	 * 更新BLob字段
	 * @author 刘虻
	 * 2009-9-21下午07:48:59
	 * @param blobSql 更新语句 update content from table_name where id=?
	 * @param pkValue 主键值
	 * @param blobIs  Blob值流
	 * @throws Exception 执行发生异常
	 */
	@Override
    public void updateBlob(String blobSql, String pkValue, InputStream blobIs) throws Exception {
		dbq.updateBlob(blobSql,pkValue,blobIs);
	}

	/**
	 * 查询带Blob字段的记录集（只查询符合条件的第一条记录）
	 * 注意：有个特殊的字段值，即 blob字段名__length  标记输出内容大小
	 * @param               提交值信息
	 * @return              记录集 key 主键  value：字段值或者为读入流
	 * @throws Exception	异常
	 * 2017年4月7日
	 * @author 马宝刚
	 */
	@Override
    @SuppressWarnings("rawtypes")
	public Map queryBlob(Object[] infos) throws Exception {
		return dbq.queryBlob(infos);
	}

	/**
	 * 查询带Blob字段的记录集（只查询符合条件的第一条记录）
	 * 注意：有个特殊的字段值，即 blob字段名__length  标记输出内容大小
	 * @param sql        查询语句
	 * @param params 提交值
	 * @return 记录集 key 主键  value：字段值或者为读入流
	 * @throws Exception 异常
	 * 2017年4月7日
	 * @author MBG
	 */
	@Override
    @SuppressWarnings("rawtypes")
	public Map queryBlob(String sql,Object[] params) throws Exception {
		return dbq.queryBlob(sql,params);
	}

	/**
	 * 执行插入语句，并获取插入后的自增主键值
	 * @param infos 脚本中采用 <%SQL$  语句     $SQL%> 拼装的信息
	 * @return 自增主键值
	 * @throws Exception 异常
	 * 2017年4月18日
	 * @author MBG
	 */
	@Override
    public int insert(Object[] infos) throws Exception {
		return dbq.insert(infos);
	}

	/**
	 * 构造分页查询对象
	 * @param infos 提交值信息
	 * @return 分页查询对象（未执行查询）
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */
	@Override
	public QueryPageVO qpvo(Object[] infos) {
		return dbq.qpvo(infos);
	}

	/**
	 * 执行分页查询 （用于复杂语句拼装接入）
	 * 注意：该方法并不会返回记录集总数，也不会拼装和执行获取记录集总数的语句
	 *       该方法通常用于执行复杂SQL语句，避免拼装获取记录集总数导致执行时间过长
	 * @param pageSize     如果从会话线程中没有获取到每页记录数，则设置默认的每页记录数（避免一次性查询出大量记录导致系统崩溃）
	 * @param infos        提交值信息
	 * @return             带记录集的分页对象
	 * @throws Exception   异常
	 * 2017年9月8日
	 * @author MBG
	 */
	@Override
	public QueryPageVO qp(int pageSize,Object[] infos) throws Exception {
		return dbq.qp(pageSize,infos);
	}

	/**
	 * 简称 qm
	 * 仅允许查两个字段值的记录，第一个字段值作为Map的key，第二个断指作为Map的Value
	 * @param infos 查询传入信息
	 * @return      返回数据 key第一个字段值  value第二个字段值
	 * @throws Exception 异常
	 * 2017年9月11日
	 * @author MBG
	 */
	@Override
	public Map<String, String> queryMap(Object[] infos) throws Exception {
		return dbq.queryMap(infos);
	}

	/**
	 * 全称 queryMap
	 * 仅允许查两个字段值的记录，第一个字段值作为Map的key，第二个断指作为Map的Value
	 * @param infos 查询传入信息
	 * @return      返回数据 key第一个字段值  value第二个字段值
	 * @throws Exception 异常
	 * 2017年9月11日
	 * @author MBG
	 */
	@Override
	public Map<String, String> qm(Object[] infos) throws Exception {
		return dbq.qm(infos);
	}

	/**
	 * 简称：ql
	 * 仅允许查一个字段值的记录，作为序列元素返回
	 * @param infos 查询传入信息
	 * @return      返回数据 ele元素：第一个字段值
	 * @throws Exception 异常
	 * 2017年9月11日
	 * @author MBG
	 */
	@Override
	public List<String> queryList(Object[] infos) throws Exception {
		return dbq.queryList(infos);
	}

	/**
	 * 全称：queryList
	 * 仅允许查一个字段值的记录，作为序列元素返回
	 * @param infos 查询传入信息
	 * @return      返回数据 ele元素：第一个字段值
	 * @throws Exception 异常
	 * 2017年9月11日
	 * @author MBG
	 */
	@Override
	public List<String> ql(Object[] infos) throws Exception {
		return dbq.ql(infos);
	}

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	@Override
	public Map<String, Map<String, String>> queryIndex(String indexKey, Object[] infos) throws Exception {
		return dbq.queryIndex(indexKey,infos);
	}

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @param dicts        字典类对象数组
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	@Override
	public Map<String, Map<String, String>> queryIndex(String indexKey, Object[] infos, Object[] dicts) throws Exception {
		return dbq.queryIndex(indexKey,infos,dicts);
	}

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	@Override
	public Map<String, Map<String, String>> qi(String indexKey, Object[] infos) throws Exception {
		return dbq.qi(indexKey,infos);
	}

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @param dicts        字典类对象数组
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	@Override
	public Map<String, Map<String, String>> qi(String indexKey, Object[] infos, Object[] dicts) throws Exception {
		return dbq.qi(indexKey,infos,dicts);
	}
	
	 /**
	  * 执行分页查询（用于复杂语句拼装接入）
	  * @param pageNo      指定页号
	  * @param pageSize    每页记录数
	  * @param infos       提交值信息
	  * @return            查询结果
	  * @throws Exception  异常
	  * 2018年8月1日
	  * @author MBG
	  */
	@Override
	 public QueryPageVO qp(int pageNo,int pageSize,Object[] infos) throws Exception {
		 return dbq.qp(pageNo,pageSize,infos);
	 }
	 
	 
	 /**
	  * 执行分页查询 （用于复杂语句拼装接入）
	  * @param pageNo        页号
	  * @param pageSize      每页记录数
	  * @param infos         提交值信息
	  * @param dicts         字典对象实例数组
	  * @return              查询结果
	  * @throws Exception    异常
	  * 2017年1月22日
	  * @author MBG
	  */
	@Override
	 public QueryPageVO qp(int pageNo,int pageSize,Object[] infos,Object[] dicts) throws Exception {
		return dbq.qp(pageNo,pageSize,infos,dicts);
	}

	/**
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param params      提交值数组
	 * @param onlyOne       是否只取一条记录
	 * @param splitStr    如果返回多条记录，分隔符值（为空时为半角逗号）
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2019年02月14日
	 * @author MBG
	 */
	@Override
	public String queryString(String sql, Object[] params, boolean onlyOne, String splitStr) throws Exception {
		return dbq.queryString(sql,params,onlyOne,splitStr);
	}

	/**
	 * 全称：queryString
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @param splitStr    分隔符，默认为半角逗号
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	@Override
	public String qs(String sql, String[] uValueArrl, String splitStr) throws Exception {
		return dbq.qs(sql,uValueArrl,splitStr);
	}

	 /**
	  * 将查询出的记录集采用这种方式输出为字符串  （用于复杂语句拼装接入）
	  *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	  *  即字段值之间用|分割，行数据之间用半角逗号分割
	  * @param infos 提交值信息
	  * @param splitStr 分隔符，为空时为半角逗号
	  * @return 记录集字符串
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	@Override
	public String qs(Object[] infos, String splitStr) throws Exception {
		return dbq.qs(infos,splitStr);
	}
	
	/**
	 * 获取数据操作服务
	 * @return 数据操作服务
	 * @throws Exception 异常
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	@Override
    public DBQuerySvc getDbs() {
		return dbq.getDbs();
	}
	
  /**
   * 以exec方式执行存储过程
   * @param sql         调用存储过程的语句
   * @param paras       提交参数数组
   * @return            返回记录及 
   * @throws Exception  异常
   * 2019年8月13日
   * @author MBG
   */
  @Override
  public List<Map<String,String>> exec(String sql, String[] paras) throws Exception {
	  return dbq.exec(sql,paras);
  }

  /**
   * 将值放入事务中
   * @author 刘虻
   * @param stmt 事务
   * @param params 值序列
   * @return 插入值日志信息
   * 2006-3-30 19:44:40
   * @throws SQLException 设置值时发生异常
   */
	@Override
	public String fixPreparedStatement(PreparedStatement stmt, Object[] params) throws Exception {
		return dbq.fixPreparedStatement(stmt,params);
	}
}
