/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.service.db.common.interfaceclass;

import java.util.HashMap;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 数据字典服务接口
 * @author 刘虻
 * 2012-6-22 下午5:26:17
 */
@ClassInfo({"2014-06-05 10:21","数据字典服务接口"})
@BeanInfo({"dictservice"})
public interface IDataDictonaryService {
	
	/**
	 * 通过数据字典主键获取对应的数据字典容器
	 * 刘虻
	 * 2012-6-22 下午5:21:48
	 * @param dictonaryKey 数据字典主键
	 * @return 数据字典容器
	 */
	HashMap<String,String> getDictonaryByKey(String dictonaryKey);
}
