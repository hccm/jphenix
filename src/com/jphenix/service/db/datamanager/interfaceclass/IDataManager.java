/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.db.datamanager.interfaceclass;

import java.sql.Connection;
import java.util.Map;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 数据库连接管理类
 * 
 * 2018-12-24 将声明的变量由类型改为接口类型
 * 2019-01-08 在建立连接时需要执行的脚本
 * 2019-03-06 增加了获取指定数据源主键的配置信息包 Map<String,String> getElementInfoMap(String dataSourceName)
 * 2019-11-06 删除了接口中只用在一个地方的常量
 * 
 * @author 刘虻
 * 2006-4-1  17:01:12
 */
@ClassInfo({"2019-11-06 19:56","数据库连接管理类"})
@BeanInfo({"dbpool"})
public interface IDataManager {

	/**
	 * 数据库类型 Mysql
	 */
	String DATA_BASE_TYPE_MYSQL = "MySql";
	
	/**
	 * 数据库类型 Oracle
	 */
	String DATA_BASE_TYPE_ORACLE = "Oracle";
	
	
	/**
	 * 数据库类型 MsSql
	 */
	String DATA_BASE_TYPE_MSSQL = "MSSql";
	
	
	   /**
     * 数据库类型 H2
     */
    String DATA_BASE_TYPE_H2 = "H2";
	
	/**
	 * 数据库类型 普通
	 */
	String DATA_BASE_TYPE_NORMAL = "Normal";
	
    /**
     * 获得指定数据源的数据库连接对象
     * @author 刘虻
     * @param dataSourceName 数据源名称
     * @exception 获得连接对象时发生异常
     * @return 指定数据源的数据库连接对象
     * 2006-4-1  17:58:39
     */
    Connection getConnection(
            String dataSourceName) throws Exception;
    
    
    /**
     * 获得指定数据源，并设置对象占用超时时间
     * @author 刘虻
     * @param dataSourceName 数据源名称
     * @param timeOut 设置对象占用超时时间（毫秒）
     * @exception 获得连接对象时发生异常
     * @return 连接对象
     * 2006-4-1  17:59:29
     */
    Connection getConnection(
            String dataSourceName
            ,long timeOut) throws Exception;
    
    
    
    /**
     * 获得指定数据源的数据库连接对象
     * @author 刘虻
     * @param dataSourceName 数据源名称
     * @param pk 数据对象主键 设置了主键后，只能获取一个数据库连接对像
     * 为了集中控制数据库对象提交，回滚，关闭等操作
     * @throws Exception 获得连接对象时发生异常
     * @return 指定数据源的数据库连接对象
     * 2006-4-1  17:58:39
     */
    Connection getConnection(
            String dataSourceName,String pk) throws Exception;
    
    
    /**
     * 获得指定数据源，并设置对象占用超时时间
     * @author 刘虻
     * @param dataSourceName 数据源名称
     * @param pk 数据对象主键 设置了主键后，只能获取一个数据库连接对像
     * 为了集中控制数据库对象提交，回滚，关闭等操作
     * @param timeOut 设置对象占用超时时间（毫秒）
     * @exception 获得连接对象时发生异常
     * @return 连接对象
     * 2006-4-1  17:59:29
     */
    Connection getConnection(
            String dataSourceName
            ,String pk
            ,long timeOut) throws Exception;
    
    
    
    /**
     * 获得指定数据源的类型
     * @author 刘虻
     * @param dataSourceName 数据源名称
     * @return 指定数据源的类型
     * 2006-4-1  18:05:40
     */
    String getDataSourceType(String dataSourceName);
    


    /**
     * 提交数据
     * 原本还需要传入数据源主键，
     * 但通常会话只存在于其中一个数据源中，没必要再传入数据源主键
     * @author 刘虻
     * @param sessionKey 会话主键
     * @throws Exception 提交数据时发生异常
     * 2006-7-26  22:03:47
     */
    void commit(String sessionKey) throws Exception;
    
    

    /**
     * 回滚数据时发生异常
     * 原本还需要传入数据源主键，
     * 但通常会话只存在于其中一个数据源中，没必要再传入数据源主键
     * @author 刘虻
     * @param sessionKey 会话主键
     * @throws Exception 回滚数据时发生异常
     * 2006-7-26  22:04:27
     */
    void rollback(String sessionKey) throws Exception;
    

    /**
     * 关闭连接
     * 原本还需要传入数据源主键，
     * 但通常会话只存在于其中一个数据源中，没必要再传入数据源主键
     * @author 刘虻
     * @param sessionKey 会话主键
     * @throws Exception 关闭连接时发生异常
     * 2006-7-26  22:05:02
     */
    void close(String sessionKey) throws Exception;
    
    /**
     * 执行初始化
     * @author 刘虻
     * 2007-3-15上午10:22:25
     */
    void init();
    
    /**
     * 执行类终止
     * @author 刘虻
     * 2007-3-15上午10:22:34
     */
    void destory();
    
    
    /**
     * 获取数据库连接
     * @author 刘虻
     * 2009-9-21下午08:41:23
     * @param dataSourceName 数据源主键
     * @param pk 会话主键
     * @param autoCommit 是否自动提交
     * @return 数据库连接
     * @throws Exception 执行发生异常
     */
    Connection getConnection(
    		String dataSourceName
    		,String pk
    		,boolean autoCommit) throws Exception;
    
    
	/**
	 * 获取配置信息包
	 * @author 刘虻
	 * 2007-3-15上午10:20:01
	 * @return 配置信息包
	 */
	Map<String,Map<String,String>> getElementInfoMap();
	
	/**
	 * 获取指定数据源的配置信息包
	 * @param dataSourceName 数据源主键
	 * @return 指定数据源的配置信息包
	 * 2019年3月6日
	 * @author MBG
	 */
	Map<String,String> getElementInfoMap(String dataSourceName);
}
