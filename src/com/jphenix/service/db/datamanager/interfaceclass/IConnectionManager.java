/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.db.datamanager.interfaceclass;

import java.sql.SQLException;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 连接对象管理类
 * @author 刘虻
 * 2006-4-3  14:54:11
 */
@ClassInfo({"2014-06-05 13:41","连接对象管理类"})
public interface IConnectionManager {

	/**
	 * 检测连接是否正常
	 * 刘虻
	 * 2011-5-13 下午02:54:06
	 * @return true正常
	 */
	boolean checkConnection();
	
    /**
     * 关闭连接
     * @author 刘虻
     * @throws SQLException 关闭连接时发生异常
     * 2006-4-3  14:55:32
     */
    void doClose() throws SQLException;
    
    
    /**
     * 提交数据
     * @author 刘虻
     * @throws SQLException 提交数据时发生异常
     * 2006-4-3  14:55:22
     */
    void doCommit() throws SQLException;
    
    
    
    /**
     * 回滚数据
     * @author 刘虻
     * @throws SQLException 回滚数据 回滚数据时发生异常
     * 2006-4-3  14:57:22
     */
    void doRollback() throws SQLException;
    
    
    /**
     * 设置是否自动提交
     * @author 刘虻
     * 2006-8-26下午11:10:00
     * @param autoCommit true 是
     * @throws SQLException 设置是否自动提交时发生异常
     */
    void doSetAutoCommit(boolean autoCommit) throws SQLException;
}
