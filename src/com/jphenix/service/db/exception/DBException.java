/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.db.exception;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.exceptions.MsgException;

/**
 * 数据库异常
 * @author 刘虻
 * 2010-5-3 上午11:49:17
 */
@ClassInfo({"2014-06-05 16:09","数据库异常"})
public class DBException extends MsgException {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -6894244112300936950L;

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param message
	 */
	public DBException(Object boss,String message) {
		super(boss,message);
	}
}
