/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2021-03-15
 * V4.0
 */
package com.jphenix.service.nodeloader;

import com.jphenix.driver.nodehandler.instancea.NodeHandler;
import com.jphenix.service.nodeloader.instancea.NodeLoader;
import com.jphenix.servlet.parent.ServiceBeanParent;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.viewhandler.INodeHandler;
import com.jphenix.standard.viewhandler.INodeLoader;

/**
 * HTML/XML页面对象服务
 * com.jphenix.service.nodeloader.NodeService
 * 
 * 2021-03-15 创建
 * 
 * @author 刘虻
 * 2021-03-15 16:42
 */
@Running({"95"})
@BeanInfo({"nodeservice"})
@ClassInfo({"2021-03-15 16:42","HTML/XML页面对象服务"})
public class NodeService extends ServiceBeanParent {
  
  private INodeLoader viewLoader = null; //视图对象加载器
  
  /**
   * 构造函数
   */
  public NodeService(){
    super();
  }

  /**
   * 获取视图加载类实例 
   * 刘虻 2010-6-8 下午04:59:11
   * @param ac 动作上下文
   * @return 视图加载类实例
   * @throws Exception 执行发生异常
   */
  public INodeLoader getNodeLoader() throws Exception {
    if (viewLoader == null) {
      viewLoader = new NodeLoader(this);
    }
    return viewLoader;
  }

  /**
   * 获取一个新的视图 
   * 刘虻 2010-6-17 上午10:58:12
   * @return 新的视图
   */
  public INodeHandler getNewViewHandler() {
    INodeHandler reVh = null; // 构建返回值
    try {
      reVh = getNodeLoader().getNewNode();
    } catch (Exception e) {
    }
    if (reVh == null) {
      reVh = new NodeHandler();
    }
    return reVh;
  }

  /**
   * 加载页面模板
   * @param filePath 页面模板相对路径（相对网站根路径）
   * @param ac       动作上下文
   * @return 页面模板对象
   * @throws Exception 异常 2015年4月8日
   * @author 刘虻
   */
  public INodeHandler loadNode(String subPath, IActionContext ac) throws Exception {
    return getNodeLoader().getNode(subPath, ac);
  }

  /**
   * 获取视图类
   * 刘虻 2010-6-8 下午05:00:02
   * @param ac   动作上下文
   * @param path 视图路径
   * @return 视图类
   * @throws Exception 执行发生异常
   */
  public INodeHandler getNodeHandlerByPath(IActionContext ac, String path) throws Exception {
    return getNodeLoader().getNode(path, ac);
  }
}
