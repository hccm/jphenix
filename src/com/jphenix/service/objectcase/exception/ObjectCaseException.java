/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.exception;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 信息池异常管理类抛出的异常
 * @author 刘虻
 * 2006-3-22  15:13:46
 */
@ClassInfo({"2014-06-05 16:21","信息池异常管理类"})
public class ObjectCaseException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = 6844758898300036276L;
	
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-4-8 12:58:08
     */
    public ObjectCaseException(Exception e) {
        super(e);
    }

    /**
     * 构造函数 
     * @author 刘虻
     * 2006-4-8 12:58:14
     */
    public ObjectCaseException(String errorMsgStr,Exception e) {
        super(errorMsgStr, e);
    }


    /**
     * 构造函数 
     * @author 刘虻
     * 2006-4-8 12:58:24
     */
    public ObjectCaseException(String msgStr) {
        super(msgStr);
    }
}
