/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 加入对象池的元素接口
 * @author 刘虻
 * 2008-1-13下午08:10:08
 */
@ClassInfo({"2014-06-05 16:29","加入对象池的元素接口"})
public interface ICaseElement {

    /**
	 * 执行关闭方法
	 * @author 刘虻
	 * 2008-1-13下午08:11:23
	 */
	void closeCaseElement();
}
