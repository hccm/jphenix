/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.interfaceclass;

import com.jphenix.kernel.objectpool.interfaceclass.IObjectElement;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 对象元素接口
 * @author 刘虻
 * 2007-5-14下午01:39:10
 */
@ClassInfo({"2014-06-05 16:30","对象元素接口"})
public interface IObjectCaseElement extends IObjectElement {

	/**
	 * 设置核心类
	 * @author 刘虻
	 * 2007-5-14下午01:40:20
	 * @param value 核心类
	 * @return 当前类实例
	 */
	void setValue(Object value);
	
	/**
	 * 获取核心类
	 * @author 刘虻
	 * 2007-5-14下午01:40:33
	 * @return 核心类
	 */
	Object getValue();
	
	
	/**
	 * 设置元素状态 0未读取 1已读取
	 * 刘虻
	 * 2010-5-6 下午12:46:40
	 * @param state 状态
	 */
	void setState(int state);
	
	/**
	 * 获取元素状态 0未读取 1已读取
	 * 刘虻
	 * 2010-5-6 下午12:47:08
	 * @return 状态
	 */
	int getState();
}
