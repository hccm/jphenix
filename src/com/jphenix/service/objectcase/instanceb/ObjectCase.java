/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.instanceb;

import com.jphenix.kernel.objectpool.instancea.ObjectPool;
import com.jphenix.kernel.objectpool.interfaceclass.IObjectElement;
import com.jphenix.kernel.objectpool.interfaceclass.IObjectPool;
import com.jphenix.service.objectcase.interfaceclass.IObjectCaseElement;
import com.jphenix.share.lang.SLong;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.objectcase.IObjectCase;
import com.jphenix.standard.objectcase.IObjectCaseManager;

import java.util.HashMap;
import java.util.Map;

/**
 * 对象容器 对象空闲超时时，自动释放资源
 * com.jphenix.service.objectcase.instanceb.ObjectCase
 * @author 刘虻
 * 2007-05-14 14:01:00
 */
@ClassInfo({"2014-06-05 16:26","对象持久化容器"})
public class ObjectCase extends ObjectPool implements IObjectCase {

    protected IObjectCaseManager objectManager = null; //信息池管理类
    protected Map<String,HashMap<String,String>> parameterMap = null; //参数容器
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-11 16:01:11
     */
    public ObjectCase() {
    	super();
    }
    

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:11
     */
    @Override
    public Object getObject(String blogName, String key) {
    	try {
    		//获取池元素
    		IObjectCaseElement oce =  
    			(IObjectCaseElement)getObjectInstance(blogName,key);
    		if (oce!=null) {
    			return oce.getValue();
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }


    /**
     * 获取新的对象元素实例
     * @author 刘虻
     * @param type 块名
     * @param pk 主键
     * 2007-5-14下午12:59:19
     * @return 新的对象元素实例
     */
    protected ObjectCaseElement getNewElement(String type,String pk) {
    	//构建返回值
    	ObjectCaseElement element = new ObjectCaseElement();
    	element.setType(type);
    	element.setPK(pk);
    	element.setPool(this);
    	//设置元素
    	setObjectInstance(element);
    	return element;
    }
    
    
    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:11
     */
    @Override
    public void setObject(String blogName, String key, Object value) {
    	//构建新的元素类
    	ObjectCaseElement element = getNewElement(blogName,key);
    	element.setPool(this);
    	element.setValue(value);
    	element.setFreeTimeOut(
    			SLong.valueOf(
    					getPropMapByBlogName(blogName)
    						.get(IObjectPool.TAG_OBJECT_POOL_NOT_IN_USE_TILE_OUT)));
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:38
     * @deprecated
     */
    @Override
    public void terminat() {
    	super.terminat();
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 23:15:18
     */
    @Override
    public void removeObject(String blogName, String key) {
        Object obj = getObject(blogName,key);
        //调用管理类,报告该主键信息已经移出
        if (getObjectManager() != null) {
            getObjectManager().beforeDropElement(blogName,key,obj);
        }
        try {
        	closeElement(blogName,key);
        }catch(Exception e) {}
    }


    /**
     * 覆盖方法  无用
     * @author  刘虻
     * 2006-4-1 11:49:42
     */
    public void start() {
    	super.startPool();
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-1 11:49:42
     * @deprecated
     */
    public void stop() {
    	super.terminat();
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-1 11:49:42
     */
    @Override
    public boolean isHaveNew(String blogName, String key) {
    	if(!hasInstance(blogName,key)) {
    		return false;
    	}
    	//获取元素
    	IObjectCaseElement element = null;
    	try {
    		element = (IObjectCaseElement)getObjectInstance(blogName,key);
    	}catch(Exception e) {
    		return false;
    	}
    	if(element==null) {
    		return false;
    	}
    	return element.getState() == 0;
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-14 14:50:26
     */
    @Override
    public void setObjectManager(
    		IObjectCaseManager objectManager) {
    	this.objectManager = objectManager;
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-14 15:19:25
     */
    @Override
    public IObjectCaseManager getObjectManager() {
        return objectManager;
    }



    /**
     * 覆盖方法
     * @author 刘虻
     * 2007-1-4下午03:23:45
     */
	@Override
    public void clearAll() {
		clearAllObject();
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14上午11:33:08
	 */
	@Override
    public void clear(String blogName) {
		clearByType(blogName);
	}

	/**
	 * @deprecated 无用
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午12:38:44
	 */
	@Override
    protected boolean checkElementError(IObjectElement element) {
		return false;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午12:39:40
	 */
	@Override
    protected IObjectElement initObjectElement(
			String type, String pk) throws Exception {
		//构建新实例
		IObjectElement reElement = getNewElement(type,pk);
		reElement.setPK(pk);
		return reElement;
	}

	

	/**
	 * 设置参数信息容器
	 * @author 刘虻
	 * 2007-5-14下午12:42:58
	 * @param parameterMap 参数信息容器
	 */
	public void setParameterMap(Map<String,HashMap<String,String>> parameterMap) {
		this.parameterMap = parameterMap;
	}
	
	
	/**
	 * 根据块名获取对应的参数信息容器
	 * 刘虻
	 * 2010-5-6 下午12:53:29
	 * @param blogName 块名
	 * @return 对应的参数信息容器
	 */
	protected Map<String,String> getPropMapByBlogName(String blogName) {
		if(parameterMap==null) {
			parameterMap = new HashMap<String, HashMap<String, String>>();
		}
		HashMap<String,String> reMap = parameterMap.get(blogName);
		if(reMap==null) {
			reMap = new HashMap<String,String>();
			parameterMap.put(blogName,reMap);
		}
		return reMap;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午03:17:18
	 */
	@Override
    public boolean refreshObject(String blogName, String key) {
		try {
            return getObjectInstance(blogName, key) != null;
        }catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 获取类名
	 * @author 刘虻
	 * 2007-7-5下午12:24:31
	 * @return 类名
	 */
	@Override
    public String getClassName() {
		return "ObjectCase";
	}
}
