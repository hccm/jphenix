/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.instanceb;

import com.jphenix.kernel.objectpool.instancea.ObjectElement;
import com.jphenix.service.objectcase.interfaceclass.ICaseElement;
import com.jphenix.service.objectcase.interfaceclass.IObjectCaseElement;
import com.jphenix.standard.docs.ClassInfo;


/**
 * 对象元素类
 * com.jphenix.service.objectcase.instanceb.ObjectCaseElement
 * @author 刘虻
 * 2007-5-14下午12:43:57
 */
@ClassInfo({"2014-06-05 16:28","对象元素类"})
public class ObjectCaseElement 
				extends ObjectElement 
						implements IObjectCaseElement {

	protected Object value = null; //核心类
	protected int state = 0; //0未读取 1已读取
	
	/**
	 * 构造函数
	 * 2007-5-14下午12:43:57
	 */
	public ObjectCaseElement() {
		super();
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午01:41:44
	 */
	@Override
    public Object getValue() {
		freeElement(); //重新更新空闲超时时间
		return value;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午01:41:49
	 */
	@Override
    public void setValue(Object value) {
		this.value = value;
    	setState(0); //未读取
    	setUseTimeOut(-1);
    	freeElement(); //因为对象容器不存在占用超时，所以初始化时就设置为自由
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-6 下午12:34:35
	 */
	@Override
    public void terminatElement() {
		if (value!=null && value instanceof ICaseElement) {
			((ICaseElement)value).closeCaseElement();
		}
		value = null;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-6 下午12:47:47
	 */
	@Override
    public int getState() {
		return state;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-6 下午12:47:50
	 */
	@Override
    public void setState(int state) {
		this.state = state;
	}
}
