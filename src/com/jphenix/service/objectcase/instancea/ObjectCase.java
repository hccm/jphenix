/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.instancea;

import com.jphenix.share.lang.SBoolean;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.objectcase.IObjectCase;
import com.jphenix.standard.objectcase.IObjectCaseManager;

import java.util.HashMap;

/**
 * 对象持久化容器 (简单容器,没有实现池管理)
 * 该类没有继承总的父类，通常用在低层类中
 * 
 * 该对象容器具备块分类管理
 * 
 * com.jphenix.service.objectcase.instancea.ObjectCase
 * 
 * @author 刘虻
 * 2006-1-29  22:40:11
 */
@ClassInfo({"2014-06-05 16:22","对象持久化容器"})
public class ObjectCase implements IObjectCase {

	//对象状态容器 key：块名  value：对象状态容器 key:对象主键 value:对象状态
    protected HashMap<String,HashMap<String,String>> blogStateHasMap = null;
    protected IObjectCaseManager objectManager = null; //信息池管理类
    
	//块容器 key:块名  value：HashMap对象容器 key:对象主键 value：对象值
    protected HashMap<String,HashMap<String,Object>> blogHashMap = null;
    protected String blogName = null; //块名

    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-11 16:01:11
     */
    public ObjectCase() {
    	super();
    }
    
    /**
     * 获取块内容状态容器
     * @author 刘虻
     * 2007-5-14上午11:15:24
     * @return
     */
    protected HashMap<String,HashMap<String,String>> getBlogStateHashMap() {
    	if (blogStateHasMap==null) {
    		blogStateHasMap = new HashMap<String,HashMap<String,String>>();
    	}
    	return blogStateHasMap;
    }
    
    /**
     * 通过块名获取指定的对象状态容器
     * @author 刘虻
     * 2007-5-14上午11:17:24
     * @param blogName 块名
     * @return 指定的对象状态容器
     */
    protected HashMap<String,String> getObjectStateHashMapByBlogName(String blogName) {
    	//获取指定的对象状态容器
    	HashMap<String,String> objectStateHasm = getBlogStateHashMap().get(blogName);
    	if (objectStateHasm==null) {
    		objectStateHasm = new HashMap<String,String>();
    		getBlogStateHashMap().put(blogName,objectStateHasm);
    	}
    	return objectStateHasm;
    }
    
    
    /**
     * 获取块容器
     * @author 刘虻
     * 2007-5-14上午11:11:03
     * @return 块容器
     */
    protected HashMap<String,HashMap<String,Object>> getBlogHashMap() {
    	if (blogHashMap==null) {
    		blogHashMap = new HashMap<String,HashMap<String,Object>>();
    	}
    	return blogHashMap;
    }

    /**
     * 获取指定块的对象容器
     * @author 刘虻
     * 2007-5-14上午11:13:29
     * @param blogName 块名
     * @return 指定块的对象容器
     */
    protected HashMap<String,Object> getObjectHashMapByBlogName(String blogName) {
    	//获取指定块的类容器
    	HashMap<String,Object> objectHasm = getBlogHashMap().get(blogName);
    	if (objectHasm==null) {
    		objectHasm = new HashMap<String,Object>();
    		getBlogHashMap().put(blogName,objectHasm);
    	}
    	return objectHasm;
    }
    

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:11
     */
    @Override
    public Object getObject(String blogName, String key) {
    	getObjectStateHashMapByBlogName(blogName).put(
                key,SBoolean.TRUE_VALUE); //设置状态为已读取
        return getObjectHashMapByBlogName(blogName).get(key);
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:11
     */
    @Override
    public void setObject(String blogName, String key, Object value) {
    	getObjectStateHashMapByBlogName(blogName).put(key,SBoolean.FALSE_VALUE); //设置状态为未读取
        getObjectHashMapByBlogName(blogName).put(key,value);
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 22:40:38
     */
    public void terminat() {
    	blogHashMap = null;
    	blogStateHasMap = null;
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-1-29 23:15:18
     */
    @Override
    public void removeObject(String blogName, String key) {
    	//获取指定对象值
        Object obj = getObject(blogName,key);
        //调用管理类,报告该主键信息已经移出
        if (getObjectManager() != null) {
            getObjectManager().beforeDropElement(blogName,key,obj);
        }
        try {
	        getObjectHashMapByBlogName(blogName).remove(key);
	        getObjectStateHashMapByBlogName(blogName).remove(key);
        }catch(Exception e) {}
    }


    /**
     * 覆盖方法  无用
     * @author  刘虻
     * 2006-4-1 11:49:42
     */
    public IObjectCase start() {
    	return this;
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-1 11:49:42
     */
    public IObjectCase stop() {
    	return this;
    }


    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-1 11:49:42
     */
    @Override
    public boolean isHaveNew(String blogName, String key) {
        return SBoolean.valueOf(
        		getObjectStateHashMapByBlogName(blogName).get(key));
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-14 14:50:26
     */
    @Override
    public void setObjectManager(
    		IObjectCaseManager objectManager) {
    	this.objectManager = objectManager;
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-14 15:19:25
     */
    @Override
    public IObjectCaseManager getObjectManager() {
        return objectManager;
    }

    /**
     * 覆盖方法 （无用）
     * @author  刘虻
     * 2006-4-15 17:13:39
     */
    public IObjectCase cancelSetObject(String blogName,String key) {
    	return this;
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-17 0:28:32
     */
    public IObjectCase setBlogName(String blogName) {
    	this.blogName = blogName;
    	return this;
    }

    /**
     * 覆盖方法 
     * @author  刘虻
     * 2006-4-17 0:28:32
     */
    public String getBlogName() {
        return blogName;
    }


    /**
     * 覆盖方法
     * @author 刘虻
     * 2007-1-4下午03:23:45
     */
	@Override
    public void clearAll() {
		blogHashMap = null;
		blogStateHasMap = null;
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14上午11:33:08
	 */
	@Override
    public void clear(String blogName) {
		getBlogHashMap().put(blogName,null);
		getBlogStateHashMap().put(blogName,null);
	}
	
	/**
	 * @deprecated 无用
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-14下午03:19:05
	 */
	@Override
    public boolean refreshObject(String blogName, String key) {
        return getObject(blogName, key) != null;
    }
}
