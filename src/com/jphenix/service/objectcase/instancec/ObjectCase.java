/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.service.objectcase.instancec;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.objectcase.IObjectCase;
import com.jphenix.standard.objectcase.IObjectCaseManager;

import java.util.HashMap;

/**
 * 实例池
 * 
 * 该池为单独块,全部驻留内存池,通常用在需要高效率存储,并且已知最大容量
 * 
 * 
 * com.jphenix.service.objectcase.instancea.ObjectCase
 * 
 * @author 刘虻
 * 2007-9-11上午11:23:18
 */
@ClassInfo({"2014-06-05 16:28","实例池"})
public class ObjectCase implements IObjectCase {

	protected HashMap<String,Object> objectCaseMap = null; //实例池容器
	protected String blogName = null; //块名
	protected IObjectCaseManager objectCaseManager = null; //对象池管理器
	
	/**
	 * 构造函数
	 * 2007-9-11上午11:23:19
	 */
	public ObjectCase() {
		super();
	}
	
	
	/**
	 * 获取有效的实例池容器
	 * @author 刘虻
	 * 2007-9-11下午12:02:54
	 * @return 有效的实例池容器
	 */
	public HashMap<String,Object> getObjectCaseMap() {
		if (objectCaseMap==null) {
			objectCaseMap = new HashMap<String,Object>();
		}
		return objectCaseMap;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	public IObjectCase cancelSetObject(String blogName, String key) {
		return this;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public void clear(String blogName) {
		objectCaseMap = null;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public void clearAll() {
		objectCaseMap = null;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	public String getBlogName() {
		return blogName;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public Object getObject(String blogName, String key) throws Exception {
		return getObjectCaseMap().get(key);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public IObjectCaseManager getObjectManager() {
		return objectCaseManager;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public boolean isHaveNew(String blogName, String key) {
		return true;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public boolean refreshObject(String blogName, String key) {
		return true;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public void removeObject(String blogName, String key) {
		getObjectCaseMap().remove(key);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	public IObjectCase setBlogName(String nameStr) {
		blogName = nameStr;
		return this;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public void setObject(
			String blogName, String key, Object value) throws Exception {
		getObjectCaseMap().put(key,value);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-11上午11:23:19
	 */
	@Override
    public void setObjectManager(IObjectCaseManager objectCaseManager) {
		this.objectCaseManager = objectCaseManager;
	}
}
