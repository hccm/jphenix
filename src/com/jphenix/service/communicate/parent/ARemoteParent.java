/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月18日
 * V4.0
 */
package com.jphenix.service.communicate.parent;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegisterChild;
import com.jphenix.service.communicate.exception.RemoteException;
import com.jphenix.service.communicate.interfaceclass.IRoute;
import com.jphenix.service.communicate.util.DataUtil;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Register;
import com.jphenix.standard.exceptions.MsgException;

import java.util.Map;

/**
 * 远程数据处理父类
 * 
  * 第三层
  * 负责业务数据处理
  * 
  * 
  * 调用目标功能
  * 
  * 设计思路：
  * 1. 当前层为通信最高层，不设计网络信息，服务器信息，只设计调用与被调用信息
  * 2. 一个系统内的功能代码不能重复
  * 3. 如果需要实现同一个功能代码调用不同的目标服务器，需要在上一层做路由，而不是在这一层做处理
  * 4. 这一层只处理业务逻辑相关的数据，与发送目的地无关，与网络无关。 
  * @author 马宝刚
  * 2015年9月18日
  */
@ClassInfo({"2015-09-14 10:48","远程调用与被调用父类"})
@BeanInfo({"aremoteparent","1","","远程数据处理父类"})
@Register({"routemanager"})
public abstract class ARemoteParent extends ABase implements IBeanRegisterChild {

    protected IRoute route = null; //父类 
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ARemoteParent() {
        super();
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 需要子类实现的方法
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /**
     * 获取当前功能类的代码
     * @return 当前功能类的代码
     * 2015年9月18日
     * @author 马宝刚
     */
    protected abstract String getFuncCode();

    
    /**
     * 注意：如果需要做事件处理，需要重写这个方法
     * @param func 来源功能号
     * @param dataMap 数据对象
     * @throws RemoteException 异常
     * 2015年9月18日
     * @author 马宝刚
     */
    @SuppressWarnings("rawtypes")
    protected void event(String func,Map dataMap) throws MsgException {
        return;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 父类已经完成的功能
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * 第三层
     * 负责业务数据处理
     * 
     * 
     * 调用目标功能
     * 
     * 设计思路：
     * 1. 当前层为通信最高层，不设计网络信息，服务器信息，只设计调用与被调用信息
     * 2. 一个系统内的功能代码不能重复
     * 3. 如果需要实现同一个功能代码调用不同的目标服务器，需要在上一层做路由，而不是在这一层做处理
     * 4. 这一层只处理业务逻辑相关的数据，与发送目的地无关，与网络无关。
     * 
     * @param func 调用目标功能号
     * @param data 传入的数据包
     * @throws MsgException 执行发生异常
     * 2015年9月14日
     * @author 马宝刚
     */
    @SuppressWarnings("rawtypes")
    protected void call(String func,Map data) throws MsgException{
        route.call(getFuncCode(),func,DataUtil.dataToBytes(data)); //发送数据
    }
    
    
    /**
     * 被调用的事件
     * @param func 调用者功能号
     * @param dataBytes 传过来的数据包
     * @throws MsgException 执行发生异常
     * 2015年9月15日
     * @author 马宝刚
     */
    protected void event(String func,byte[] dataBytes) throws MsgException{
        event(func,DataUtil.bytesToMap(dataBytes));
    }
    
    
    /**
     * 设置注册管理类实例
     * @param register 注册管理类实例
     * 2015年9月18日
     * @author 马宝刚
     */
    @Override
    public void setRegister(Object register) {
        route = (IRoute)register;
    }
    
    
    
    
    
    
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 不必介意以下方法
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * 注册后执行的方法
     * 2015年9月18日
     * @author 马宝刚
     */
    @Override
    public void afterRegister() {}
    
    
    /**
     * 取消注册前执行的方法
     * 2015年9月18日
     * @author 马宝刚
     */
    @Override
    public void beforeUnRegister() {}
}
