/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.service.communicate.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 前台调用服务接口
 * @author 刘虻
 * 2007-2-26下午03:28:24
 */
@ClassInfo({"2014-06-04 20:21","前台调用服务接口ICallObject"})
public interface ICallObject {
	
	
	/**
	 * 调用后台服务方法
	 * @author 刘虻
	 * 2007-2-26下午03:30:01
	 * @param socketSource 服务器源
	 * @param sessionID 会话主键
	 * @param classID 调用服务类主键
	 * @param methodMame 调用服务方法名
	 * @param paraClss 传入参数类型数组
	 * @param paraObjs 传入参数值数组
	 * @return 返回值
	 * @throws Exception 执行发生异常
	 */
	Object call(
			String socketSource
			,String sessionID
			,String classID
			,String methodMame
			,Class<?>[] paraClss
			,Object[] paraObjs) throws Exception;
}
