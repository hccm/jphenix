/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月15日
 * V4.0
 */
package com.jphenix.service.communicate.interfaceclass;

import com.jphenix.service.communicate.exception.RouteException;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 第二层（总管理层）
 * 负责物理网络与逻辑网络交互
 * 
 * 该层承上启下，第一层可由不通方式实现，也可连接多个第一层和多个第三层
 * 
 * 该类只能由一个实例
 * 
 * @author 马宝刚
 * 2015年9月15日
 */
@ClassInfo({"2015-09-17 10:48","负责物理网络与逻辑网络交互"})
public interface IRoute {

    
    /**
     * 发送数据到目标方法中
     * @param srcFunc 源方法主键
     * @param objFunc 目标方法主键
     * @param datas 发送数据
     * @throws RouteException 异常
     * 2015年9月18日
     * @author 马宝刚
     */
    void call(String srcFunc,String objFunc,byte[] datas) throws RouteException;
}
