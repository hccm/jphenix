/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.service.communicate.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 集群广播处理接口
 * @author 刘虻
 * 2007-10-27上午10:29:36
 */
@ClassInfo({"2014-06-04 20:21","集群广播处理接口"})
public interface IGatherBroadcast {

	/**
	 * 执行集群广播
	 * @author 刘虻
	 * 2007-10-27上午10:28:42
	 * @param beanPK 调用类主键
	 * @param methodName 方法名
	 * @param paraClss 参数类型数组
	 * @param paraObjs 参数值数组
	 */
	void doBroadcast(
			String beanPK
			,String methodName
			,Class<?>[] paraClss
			,Object[] paraObjs);
	
	
	/**
	 * 获取集群服务器主键字符串序列(用逗号分割)
	 * @author 刘虻
	 * 2007-10-27上午09:53:05
	 * @return 集群服务器主键字符串序列
	 */
	String getGatherServerKeys();
	
	/**
	 * 设置集群服务器主键字符串序列(用逗号分割)
	 * @author 刘虻
	 * 2007-10-27上午09:53:13
	 * @param gatherServerKeys 集群服务器主键字符串序列
	 */
	void setGatherServerKeys(String gatherServerKeys);

	/**
	 * 获取远程调用处理类
	 * @author 刘虻
	 * 2007-10-27上午09:56:33
	 * @return 远程调用处理类
	 */
	ICallObject getCallObject();

	/**
	 * 设置远程调用处理类
	 * @author 刘虻
	 * 2007-10-27上午09:56:39
	 * @param callObject 远程调用处理类
	 */
	void setCallObject(ICallObject callObject);

	/**
	 * 获取执行远程方法调用超时时间
	 * @author 刘虻
	 * 2007-10-27上午10:12:53
	 * @return 执行远程方法调用超时时间
	 */
	long getExecuteTimeOut();

	/**
	 * 设置执行远程方法调用超时时间
	 * @author 刘虻
	 * 2007-10-27上午10:13:01
	 * @param executeTimeOut 执行远程方法调用超时时间
	 */
	void setExecuteTimeOut(long executeTimeOut);


	/**
	 * 获取当前服务器主键 设置此值后,不广播自己
	 * @author 刘虻
	 * 2007-10-27上午10:54:53
	 * @return 当前服务器主键
	 */
	String getSelfServerKey();


	/**
	 * 设置当前服务器主键
	 * @author 刘虻
	 * 2007-10-27上午10:55:10
	 * @param selfServerKey 当前服务器主键
	 */
	void setSelfServerKey(String selfServerKey);
	
	
	/**
	 * 获取是否允许广播
	 * @author 刘虻
	 * 2007-10-27上午11:15:19
	 * @return 是否允许广播 yes no
	 */
	String getEnabledBroadcast();

	/**
	 * 设置是否允许广播
	 * @author 刘虻
	 * 2007-10-27上午11:15:29
	 * @param enabledBroadcast 是否允许广播 yes no
	 */
	void setEnabledBroadcast(String enabledBroadcast);
}
