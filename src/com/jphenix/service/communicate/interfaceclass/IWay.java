/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月15日
 * V4.0
 */
package com.jphenix.service.communicate.interfaceclass;

import com.jphenix.service.communicate.exception.WayException;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 第一层
 * 负责网络连接、数据传输
 * 
 * 有不同的实现方式，比如TCP、UDP、进程管道等等
 * 该层只负责数据传输
 * 
 * @author 马宝刚
 * 2015年9月15日
 */
@ClassInfo({"2015-09-17 10:48","负责网络连接、数据传输"})
public interface IWay {

    /**
     * 发送数据
     * @param data 数据
     * @throws WayException 异常
     * 2015年9月17日
     * @author 马宝刚
     */
    void send(byte[] data) throws WayException;
    
    
    /**
     * 接收数据
     * @param data 数据
     * @throws WayException 异常
     * 2015年9月17日
     * @author 马宝刚
     */
    void receive(byte[] data) throws WayException;
 }
