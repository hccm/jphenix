/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月17日
 * V4.0
 */
package com.jphenix.service.communicate.exception;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.exceptions.MsgException;

/**
 * 第三层处理异常
 * @author 马宝刚
 * 2015年9月17日
 */
@ClassInfo({"2014-09-17 11:34","第三层处理异常"})
public class RemoteException extends MsgException {

    /**
     * 串行标识
     */
    private static final long serialVersionUID = -3427821571312860670L;

    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Class<?> bossCls, String msg) {
        super(bossCls, msg);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Class<?> bossCls, String msg,String code) {
        super(bossCls, msg,code);
    }
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Object boss, String msg) {
        super(boss, msg);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Object boss, Exception e) {
        super(boss, e);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Object boss, String msg, Exception e) {
        super(boss, msg, e);
    }
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public RemoteException(Object boss,String msg,String code) {
        super(boss,msg,code);
    }
}
