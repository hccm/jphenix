/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.service.communicate.exception;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 通信异常管理类
 * @author 刘虻
 * 2007-01-12  20:05:00
 */
@ClassInfo({"2014-06-04 20:20","通信异常管理类"})
public class CommunicateException extends Exception {

	private static final long serialVersionUID = 779885214157234191L; //序列号
	
    /**
     * 构造函数 
     * @author 刘虻
     * 2007-01-12  20:05:00
     */
    public CommunicateException(Exception e) {
        super(e);
    }

    /**
     * 构造函数 
     * @author 刘虻
     * 2007-01-12  20:05:00
     */
    public CommunicateException(
    		String errorMsg
    		, Exception e) {
        super(errorMsg,e);
    }

    /**
     * 构造函数 
     * @author 刘虻
     * 2007-01-12  20:05:00
     */
    public CommunicateException(String errorMsg) {
        super(errorMsg);
    }
}
