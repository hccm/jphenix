/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月17日
 * V4.0
 */
package com.jphenix.service.communicate.exception;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.exceptions.MsgException;

/**
 * 第一层通信异常
 * @author 马宝刚
 * 2015年9月17日
 */
@ClassInfo({"2014-09-17 11:34","第一层通信异常"})
public class WayException extends MsgException {

    /**
     * 串行标识
     */
    private static final long serialVersionUID = 818461598674327199L;

    /**
     * 构造函数
     * @author 马宝刚
     */
    public WayException(Class<?> bossCls, String msg) {
        super(bossCls, msg);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public WayException(Object boss, String msg) {
        super(boss, msg);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public WayException(Object boss, Exception e) {
        super(boss, e);
    }

    /**
     * 构造函数
     * @author 马宝刚
     */
    public WayException(Object boss, String msg, Exception e) {
        super(boss, msg, e);
    }
}
