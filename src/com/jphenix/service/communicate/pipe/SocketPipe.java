/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年12月15日
 * V4.0
 */
package com.jphenix.service.communicate.pipe;

import java.net.Socket;

/**
 * 通信端口对接类
 * @author 马宝刚
 * 2014年12月15日
 */
public class SocketPipe {

    private Socket fromSocket = null; //来源端口
    private Socket toSocket = null; //目标端口
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public SocketPipe() {
        super();
    }
    
    /**
     * 构造函数
     * @params fromSocket 来源端口
     * @params toSocket 目标端口
     * @author 马宝刚
     */
    public SocketPipe(Socket fromSocket,Socket toSocket) {
        super();
        this.fromSocket = fromSocket;
        this.toSocket = toSocket;
    }
    
    
    /**
     * 启动传输
     * 2014年12月16日
     * @author 马宝刚
     */
    public void start() throws Exception {
        /*
        try {
            transfer = new DataTransfer(fromSocket.getInputStream(),toSocket.getOutputStream());
            transfer.start();
        }catch(Exception e) {
            e.printStackTrace();
            throw new MsgException(this,"SocketPipe Create Send Data Stream Exception",e);
        }
        try {
            unTransfer = new DataTransfer(toSocket.getInputStream(),fromSocket.getOutputStream());
            unTransfer.start();
        }catch(Exception e) {
            e.printStackTrace();
            throw new MsgException(this,"SocketPipe Create Get Data Stream Exception",e);
        }finally {
            
        }
        */
    }
    
    /**
     * 获取目标端口
     * @return 目标端口
     * 2014年12月15日
     * @author 马宝刚
     */
    public Socket getToSocket() {
        return toSocket;
    }

    /**
     * 获取目标端口
     * @param socket 获取
     * 2014年12月15日
     * @author 马宝刚
     */
    public void setToSocket(Socket socket) {
        toSocket = socket;
    }
    
    /**
     * 设置来源端口
     * @param socket 来源端口
     * 2014年12月15日
     * @author 马宝刚
     */
    public void setFromSocket(Socket socket) {
        fromSocket = socket;
    }
    
    /**
     * 获取来源端口
     * @return 来源端口
     * 2014年12月15日
     * @author 马宝刚
     */
    public Socket getFromSocket() {
        return fromSocket;
    }
}
