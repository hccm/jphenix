/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月15日
 * V4.0
 */
package com.jphenix.service.communicate.vo;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 包数据类
 * 
 * 2020-09-12 去掉了提醒注释
 * 
 * @author 马宝刚 2015年9月15日
 */
@ClassInfo({"2020-09-12 16:30","包数据类"})
public class PackageVO {

    /**
     * 构造函数
     * @author 马宝刚
     */
    public PackageVO() {
        super();
    }

}
