/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.parent;

import com.jphenix.servlet.common.BaseParent;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;


/**
 * 服务父类
 * 该类主要标记了服务类都是常驻内存的
 * 
 * import com.jphenix.servlet.parent.ServiceBeanParent;
 * 
 * 2018-11-12 将脚本中常用的方法提取到了BaseParent类中，作为当前类的父类，动作类跟服务类的父类，最终归为一个父类
 * 2018-11-13 修改了类主键，这值原本是脚本父类主键，已经简化掉了这个父类脚本。
 * 2019-01-24 将该类迁移到单独包中
 * 2019-01-25 修改了类注释
 * 
 * @author 刘虻
 * 2010-6-2 下午06:02:48
 */
@ClassInfo({"2019-01-25 10:16","服务父类"})
@Running({"1"})
@BeanInfo({"servicebeanparent","1","","服务类的父类，提供数据库操作方法，继承该父类，可以常驻内存","1"})
public class ServiceBeanParent extends BaseParent {
    

	/**
	 * 构造函数
	 * @author MBG
	 */
	public ServiceBeanParent() {
		super();
	}
}
