/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月16日
 * V4.0
 */
package com.jphenix.servlet.parent;

import com.jphenix.driver.cluster.ClusterFilter;
import com.jphenix.servlet.service.SignService;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 安全接口动作类父类
 * 
 * import com.jphenix.servlet.parent.ApiBeanParent;
 * 
 * 请求参数 _res_format
 *    _res_format=xml 返回XML格式报文  _res_format=json 返回Json格式报文（默认）
 *    _res_format参数也参与签名验签
 *    
 * 2019-01-24 将该类迁移到单独包中
 * 2019-01-25 修改了类注释
 * 2021-03-17 除去了注册类主键，采用父类中的注册类主键
 * 
 * @author 马宝刚
 */
@ClassInfo({"2021-03-17 21:11","安全接口动作类父类"})
@BeanInfo({"apibeanparent","true","","安全接口动作类父类"})
public class ApiBeanParent extends ActionBeanParent {

	  private SignService ss = null; //签名类

	  /**
	   * 构造函数
	   */
	  public ApiBeanParent() {
	    super();
	  }

	  /**
	   * 获取有效的基本服务类
	   * @return 有效的基本服务类
	   */
	  private synchronized SignService getSs() {
	    if(ss==null) {
	      try {
	        //获取返回值
	        Object res = getBean(SignService.class);
	        ss = (SignService)res;
	      }catch(Exception e) {
	        e.printStackTrace();
	      }
	    }
	    return ss;
	  }

	  /**
	   * 注意：仅针对提交数据做签名验签，返回数据不做签名延签
	   *       目前返回数据可能是XML，可能是Json，可能是别的，签名思路不太成熟，暂不做
	   * 在调用动作后执行的方法 （由系统调用，需要时覆盖该方法）
	   * 刘虻
	   * 2010-6-28 下午06:27:56
	   * @return 返回false时，不会执行后续的将数据输出到界面。通常
	   *               重写这个方法时，已经将数据输出到界面，故返回false
	   * @throws Exception 异常
	   */
	  //public boolean afterRunAction() throws Exception {
	  //  if(isNoReturnInfo()) {
	  //    //不需要输出信息
	  //    return false;
	  //  }
	  //  String encoding; //输出编码
	  //  switch(getOutContentType()) {
	  //    case 0: //html
	  //      IViewHandler vh = getReVh();
	  //      encoding = vh.getDealEncode();
	  //      if(encoding==null || encoding.length()<1) {
	  //        encoding = "UTF-8";
	  //      }
	  //      $$().setHeader("Content-Type","text/html; charset="+encoding);
	  //      ss.signOut($(),$$(),vh); //输出数据
	  //      return false;
	  //    case 1: //xml
	  //      vh = getReXml();
	  //      if(vh!=null) {
	  //        encoding = vh.getDealEncode();
	  //        if(encoding==null || encoding.length()<1) {
	  //          encoding = "UTF-8";
	  //        }
	  //      }else {
	  //        encoding = "UTF-8";
	  //      }
	  //      $$().setHeader("Content-Type","text/xml; charset="+encoding);
	  //      ss.signOut($(),$$(),vh); //输出数据
	  //      return false;
	  //    default: //json
	  //      reJson = getReJson();
	  //      $$().setHeader("Content-Type","application/json; charset=UTF-8");
	  //      ss.signOut($(),$$(),reJson); //输出数据
	  //      return false;
	  //  }
	  //}
	    
	  
	  /**
	   * 覆盖方法
	   * 马宝刚
	   * 2010-6-29 下午01:46:19
	   */
	  @Override
      public boolean beforeRunAction() throws Exception {
	    //获取集群过滤器类实例
	    ClusterFilter cf = bean(ClusterFilter.class);
	    if(cf!=null && cf.callFromCluster($())){
	      //该请求是由集群中调用，属于内部调用，直接放行，无需校验。
	      return true;
	    }
	    @SuppressWarnings("deprecation")
		String format = $("_res_format"); //返回报文格式
	    if(format.length()<1 || "json".equals(format.toLowerCase())) {
	      setOutJson(); //返回json格式数据
	    }else if("xml".equals(format)) {
	      setOutXml(); ////返回xml格式数据
	    }
	    //else  返回html格式数据
	    
	    //执行验签
	    String verfyRes = getSs().verfySign($(),this);
	    if(verfyRes!=null) {
	      out(-90,verfyRes);
	      return false;
	    }
	    return true;
	  }
}
