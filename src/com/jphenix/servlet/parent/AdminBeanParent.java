/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月16日
 * V4.0
 */
package com.jphenix.servlet.parent;

import com.jphenix.servlet.filter.ConsoleFilter;
import com.jphenix.share.lang.SInteger;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;

/**
 * 管理员动作类父类
 * 
 * import com.jphenix.servlet.parent.AdminBeanParent;
 * 
 * 2019-01-24 将该类迁移到单独包中
 * 2019-01-25 修改了类注释
 * 2020-08-02 修改了权限控制方法
 * 2020-08-31 增加了取消当前授权码验证状态方法
 * 2021-03-17 除去了注册类主键，采用父类中的注册类主键
 * 
 * @author 马宝刚
 */
@ClassInfo({"2021-03-17 21:11","管理员动作类父类"})
@BeanInfo({"adminbeanparent","true","","管理员动作类父类"})
public class AdminBeanParent extends ActionBeanParent {

	/**
	 * 构造函数
	 */
	public AdminBeanParent() {
		super();
	}
	
	/**
	 * 取消当前授权码验证后状态
	 * 2020年8月31日
	 * @author MBG
	 */
	public void cancelSign() {
		ConsoleFilter cf = bean(ConsoleFilter.class); //控制台过滤器
		if(cf==null) {
			return;
		}
		cf.cancelSign(getActionContext());
	}

	/**
	 * 覆盖方法
	 * 马宝刚
	 * 2010-6-29 下午01:46:19
	 */
	@Override
    public boolean beforeRunAction() throws Exception {
		if(!super.beforeRunAction()) {
			return true;
		}
		IActionContext ac = getActionContext(); //获取动作上下文
		ConsoleFilter cf = bean(ConsoleFilter.class); //控制台过滤器
		if(cf==null || !cf.verify(ac,this)) {
			return false;
		}
		//动作路径
		String action = ac.getAction();
		if("/N100034".equals(action) || "/systemaction_checkhealth".equals(action)) {
			return true;
		}
		//会话超时时间
		int managerSessionTimeOut = SInteger.valueOf(p("debug_session_timeout"));
		if(managerSessionTimeOut==0) {
			managerSessionTimeOut = 1800;
		}
		ac.getRequest().getSession().setMaxInactiveInterval(managerSessionTimeOut);
		return true;
    }
}
