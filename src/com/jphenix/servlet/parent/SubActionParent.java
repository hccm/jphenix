/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月16日
 * V4.0
 */
package com.jphenix.servlet.parent;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 动作过程脚本父类
 * 该动作过程脚本父类无需注册类主键
 * 
 * import com.jphenix.servlet.parent.SubActionParent;
 * 
 * 2019-01-24 将该类迁移到单独包中
 * 2019-01-25 修改了类注释
 * 
 * @author 马宝刚
 */
@ClassInfo({"2019-01-25 10:17","动作过程脚本父类"})
@BeanInfo({"subactionparent","true","","该动作过程脚本父类无需注册类主键"})
public class SubActionParent extends ActionBeanParent {

    /**
     * 构造函数
     */
    public SubActionParent() {
        super();
    }
}
