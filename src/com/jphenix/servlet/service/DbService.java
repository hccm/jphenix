/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年4月22日
 * V4.0
 */
package com.jphenix.servlet.service;

import com.jphenix.servlet.common.BaseParent;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;

/**
 * 数据库资源服务
 * 
 * com.jphenix.servlet.service.DbService
 * 
 * 所有功能都在父类中
 * 
 * @author MBG
 * 2019年4月22日
 */
@ClassInfo({"2019-04-22 12:25","数据库资源服务"})
@BeanInfo({"singledbservice"})
@Running({"1"})
public class DbService extends BaseParent {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public DbService() {
		super();
	}
}
