/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.service;

import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegister;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegisterChild;
import com.jphenix.servlet.interfaceclass.IRootMenuService;
import com.jphenix.servlet.parent.ServiceBeanParent;
import com.jphenix.share.lang.SortVector;
import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 根菜单数据服务
 * com.jphenix.servlet.service.RootMenuService
 * 
 * 2019-01-24 修改了父类的类路径
 * 
 * @author 刘虻
 * 2010-8-11 下午03:00:17
 */
@ClassInfo({"2019-01-24 16:42","根菜单数据服务"})
public class RootMenuService 
				extends ServiceBeanParent 
						implements IBeanRegister,IRootMenuService {

	protected SortVector<RootMenuElement> sortVector = new SortVector<RootMenuElement>(); //排序容器
	protected List<RootMenuElement> elementList = null; //菜单元素序列
	protected HashMap<String,RootMenuElement> elementMap = 
	                            new HashMap<String,RootMenuElement>(); //菜单元素容器
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public RootMenuService() {
		super();
	}
	
	/**
	 * 获取菜单信息元素序列
	 * 刘虻
	 * 2010-8-11 下午03:36:31
	 * @return 菜单信息元素序列
	 */
	@Override
    public List<RootMenuElement> getElementList() {
		if(elementList==null) {
			elementList = new ArrayList<RootMenuElement>();
			sortVector.asc();
			while(sortVector.hasNext()) {
				elementList.add(sortVector.nextValue());
			}
		}
		return elementList;
	}
	
	/**
	 * 设置菜单元素信息序列
	 * 刘虻
	 * 2010-8-11 下午03:37:36
	 * @param elementList 菜单元素信息序列
	 */
	@Override
    public void setElementList(List<RootMenuElement> elementList) {
		this.elementList = elementList;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-11 下午03:00:17
	 */
	@Override
    public void unRegist(Object bean) {
		if(bean==null || !(bean instanceof RootMenuElement)) {
			return;
		}
        if(bean instanceof IBeanRegisterChild) {
            ((IBeanRegisterChild)bean).beforeUnRegister();
        }
		//获取菜单主键
		String id = ((RootMenuElement)bean).getID();
		elementMap.remove(id);
		if(elementList!=null) {
			elementList.remove(bean);
		}
		SortVector<RootMenuElement> sv = new SortVector<RootMenuElement>();
		sortVector.asc();
		RootMenuElement re; //菜单项
		while(sortVector.hasNext()) {
			re = sortVector.nextValue();
			if(re!=bean) {
				sv.add((RootMenuElement)bean,((RootMenuElement)bean).getIndex());
			}
		}
		sortVector = sv;
	}
	

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-11 下午03:00:17
	 */
	@Override
    public boolean regist(Object bean) {
		if(bean==null || !(bean instanceof RootMenuElement)) {
			return false;
		}
		//获取菜单主键
		String id = ((RootMenuElement)bean).getID();
		if(id.length()<1) {
			log.warning("The RootMenuElement ID Is Null:"+bean,null);
			return false;
		}
		if(elementMap.containsKey(id)) {
			log.warning("The RootMenuElement ID Is Repeat: The Bean:["+bean
					+"] Repeat Bean:["+elementMap.get(id)+"]",null);
			return false;
		}
		elementMap.put(id,(RootMenuElement)bean);
		sortVector.add((RootMenuElement)bean,((RootMenuElement)bean).getIndex());
        if(bean instanceof IBeanRegisterChild) {
            ((IBeanRegisterChild)bean).setRegister(this);
            ((IBeanRegisterChild)bean).afterRegister();
        }
		return true;
	}

}
