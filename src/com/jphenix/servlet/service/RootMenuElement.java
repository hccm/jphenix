/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.service;

import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * com.jphenix.servlet.service.RootMenuElement
 * 根菜单选项元素信息类
 * @author 刘虻
 * 2010-8-11 下午02:26:33
 */
@ClassInfo({"2014-06-12 19:20","根菜单选项元素信息类"})
public class RootMenuElement {

	protected String 
					id = null				//菜单主键
					,name = null			//菜单名
					,childAction = null		//子菜单动作路径
					,target = null			//动作目标主键（frame的id）
					,iconPath = null		//菜单图标路径
					,script = null			//点击菜单调用脚本
					,defAction = null;		//点击菜单调用动作
					
					
	protected boolean 
					refresh = false			//是否每次点击菜单都执行动作
					,hiddenRoot = false		//超级用户访问时隐藏该功能
					,def = false;			//是否为默认打开菜单
					
	protected int index = 0;				//菜单索引
	
	protected ArrayList<String> roleCodeList = null; //角色编码序列
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public RootMenuElement() {
		super();
	}
	
	/**
	 * 设置超级用户访问时是否隐藏该功能
	 * 刘虻
	 * 2011-5-20 下午01:50:22
	 * @param hiddenRoot true是
	 */
	public void setHiddenRoot(boolean hiddenRoot) {
		this.hiddenRoot = hiddenRoot;
	}
	
	/**
	 * 获取超级用户访问时是否隐藏该功能
	 * 刘虻
	 * 2011-5-20 下午01:50:39
	 * @return true是
	 */
	public boolean isHiddenRoot() {
		return hiddenRoot;
	}
	
	/**
	 * 获取角色代码序列
	 * 刘虻
	 * 2011-5-9 下午01:05:47
	 * @return 角色代码序列
	 */
	public List<String> getRoleCodeList() {
		if(roleCodeList==null) {
			roleCodeList = new ArrayList<String>();
		}
		return roleCodeList;
	}
	
	/**
	 * 设置角色代码
	 * 刘虻
	 * 2011-5-9 下午01:01:56
	 * @param roleCode 角色代码  （多个用半角逗号分割）
	 */
	public void setRoleCode(String roleCode) {
		if(roleCode==null) {
			return;
		}
		//角色代码数组
		String[] roleCodes = roleCode.split(",");
		roleCodeList = new ArrayList<String>();
		for(int i=0;i<roleCodes.length;i++) {
			roleCodes[i] = roleCodes[i].toLowerCase().trim();
			if(roleCodes[i].length()<1) {
				continue;
			}
			if(!roleCodeList.contains(roleCodes[i])) {
				roleCodeList.add(roleCodes[i]);
			}
		}
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-8-12 上午09:04:26
	 */
	@Override
    public String toString() {
		return "<menu id=\""+getID()+"\" name=\""+getName()+"\" childaction=\""
		+getChildAction()+"\" refresh=\""+(isRefresh()?"yes":"no")
		+"\" target=\""+getTarget()+"\" default=\""+(isDef()?"yes":"no")
		+"\" icon=\""+getIconPath()+"\" script=\""+getScript()+"\"></menu>";
	}
	
	/**
	 * 获取菜单主键
	 * 刘虻
	 * 2010-8-11 下午02:57:46
	 * @return 菜单主键
	 */
	public String getID() {
		if(id==null) {
			id = "";
		}
		return id;
	}
	
	/**
	 * 设置菜单主键
	 * 刘虻
	 * 2010-8-11 下午02:57:17
	 * @param id 菜单主键
	 */
	public void setID(String id) {
		this.id = id;
	}
	
	/**
	 * 获取菜单名
	 * 刘虻
	 * 2010-8-11 下午02:56:59
	 * @return 菜单名
	 */
	public String getName() {
		if(name==null) {
			name = "";
		}
		return name;
	}
	
	/**
	 * 设置菜单名
	 * 刘虻
	 * 2010-8-11 下午02:56:30
	 * @param name 菜单名
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 获取子菜单动作路径
	 * 刘虻
	 * 2010-8-11 下午02:56:14
	 * @return 子菜单动作路径
	 */
	public String getChildAction() {
		if(childAction==null) {
			childAction = "";
		}
		return childAction;
	}
	
	
	/**
	 * 设置子菜单动作路径
	 * 刘虻
	 * 2010-8-11 下午02:55:25
	 * @param childAction 子菜单动作路径
	 */
	public void setChildAction(String childAction) {
		this.childAction = childAction;
	}
	
	/**
	 * 获取动作目标主键（frame的id）
	 * 刘虻
	 * 2010-8-11 下午02:54:50
	 * @return 动作目标主键
	 */
	public String getTarget() {
		if(target==null) {
			target = "";
		}
		return target;
	}
	
	/**
	 * 设置动作目标主键（frame的id）
	 * 刘虻
	 * 2010-8-11 下午02:54:23
	 * @param target 动作目标主键
	 */
	public void setTarget(String target) {
		this.target = target;
	}
	
	/**
	 * 获取菜单图标路径
	 * 刘虻
	 * 2010-8-11 下午02:54:00
	 * @return 菜单图标路径
	 */
	public String getIconPath() {
		if(iconPath==null) {
			iconPath = "";
		}
		return iconPath;
	}
	
	/**
	 * 设置菜单图标路径
	 * 刘虻
	 * 2010-8-11 下午02:53:29
	 * @param iconPath 菜单图标路径
	 */
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	
	/**
	 * 获取点击菜单调用脚本
	 * 刘虻
	 * 2010-8-11 下午02:52:57
	 * @return 点击菜单调用脚本
	 */
	public String getScript() {
		if(script==null) {
			script = "";
		}
		return script;
	}
	
	
	/**
	 * 设置点击菜单调用脚本
	 * 刘虻
	 * 2010-8-11 下午02:52:30
	 * @param script 点击菜单调用脚本
	 */
	public void setScript(String script) {
		this.script = script;
	}
	
	
	/**
	 * 获取点击菜单调用动作
	 * 刘虻
	 * 2010-8-11 下午02:52:06
	 * @return 点击菜单调用动作
	 */
	public String getDefAction() {
		if(defAction==null) {
			defAction = "";
		}
		return defAction;
	}
	
	/**
	 * 设置点击菜单调用动作
	 * 刘虻
	 * 2010-8-11 下午02:51:35
	 * @param defAction 点击菜单调用动作
	 */
	public void setDefAction(String defAction) {
		this.defAction = defAction;
	}
	
	/**
	 * 是否为默认打开菜单
	 * 刘虻
	 * 2010-8-11 下午02:51:12
	 * @return 是否为默认打开菜单
	 */
	public boolean isDef() {
		return def;
	}
	
	/**
	 * 设置是否为默认打开菜单
	 * 刘虻
	 * 2010-8-11 下午02:50:53
	 * @param def 是否为默认打开菜单
	 */
	public void setDef(boolean def) {
		this.def = def;
	}
	
	/**
	 * 是否每次点击菜单都执行动作
	 * 刘虻
	 * 2010-8-11 下午02:50:28
	 * @return 是否每次点击菜单都执行动作
	 */
	public boolean isRefresh() {
		return refresh;
	}
	
	/**
	 * 设置是否每次点击菜单都执行动作
	 * 刘虻
	 * 2010-8-11 下午02:50:08
	 * @param refresh 是否每次点击菜单都执行动作
	 */
	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	
	/**
	 * 获取菜单索引
	 * 刘虻
	 * 2010-8-11 下午02:49:39
	 * @return 菜单索引
	 */
	public int getIndex() {
		return index;
	}
	
	
	/**
	 * 设置菜单索引
	 * 刘虻
	 * 2010-8-11 下午02:49:14
	 * @param index 菜单索引
	 */
	public void setIndex(int index) {
		this.index = index;
	}

}
