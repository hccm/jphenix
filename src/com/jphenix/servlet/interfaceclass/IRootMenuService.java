/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import java.util.List;

import com.jphenix.servlet.service.RootMenuElement;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 根菜单数据服务接口
 * @author 刘虻
 * 2010-8-11 下午03:05:55
 */
@ClassInfo({"2014-06-06 18:44","根菜单数据服务接口"})
@BeanInfo({"rootmenuservice"})
public interface IRootMenuService {
	
	/**
	 * 获取菜单信息元素序列
	 * 刘虻
	 * 2010-8-11 下午03:36:31
	 * @return 菜单信息元素序列
	 */
	List<RootMenuElement> getElementList();
	
	/**
	 * 设置菜单元素信息序列
	 * 刘虻
	 * 2010-8-11 下午03:37:36
	 * @param elementList 菜单元素信息序列
	 */
	void setElementList(List<RootMenuElement> elementList);
}
