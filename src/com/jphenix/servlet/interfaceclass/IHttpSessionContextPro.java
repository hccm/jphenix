/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import javax.servlet.http.HttpSessionContext;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

/**
 * @deprecated 接口就不赞成用
 * 封装的会话上下文
 * @author 刘虻
 * 2007-5-21下午06:12:17
 */
@ClassInfo({"2014-06-06 18:43","封装的会话上下文"})
public interface IHttpSessionContextPro 
				extends HttpSessionContext,ICloneable {}
