/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import javax.servlet.ServletConfig;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

/**
 * Servlet配置信息增强包
 * @author 刘虻
 * 2007-1-24下午08:16:35
 */
@ClassInfo({"2014-06-06 18:46","Servlet配置信息增强包"})
public interface IServletConfigPro extends ServletConfig,ICloneable{

	/**
	 * 设置初始化参数
	 * @author 刘虻
	 * 2007-1-24下午08:24:15
	 * @param key 参数主键
	 * @param value 参数值
	 */
	void setInitParameter(String key,String value);
	
	/**
	 * 获取核心配置信息包
	 * @author 刘虻
	 * 2007-1-24下午08:26:43
	 * @return 核心配置信息包
	 */
	ServletConfig getServletConfig();
}
