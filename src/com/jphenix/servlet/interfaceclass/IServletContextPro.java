/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import javax.servlet.ServletContext;

/**
 * 封装的Servlet上下文
 * @author 刘虻
 * 2007-5-21下午06:10:28
 */
@ClassInfo({"2014-06-06 18:47","封装的Servlet上下文"})
public interface IServletContextPro 
				extends ServletContext,ICloneable {
	
	/**
	 * 设置初始化参数
	 * @author 刘虻
	 * 2007-5-21下午07:10:34
	 * @param key 主键
	 * @param value 参数值
	 */
    @Override
    boolean setInitParameter(String key,String value);
}
