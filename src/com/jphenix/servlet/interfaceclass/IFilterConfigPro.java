/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import javax.servlet.FilterConfig;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

/**
 * 配置信息类配置信息增强包
 * @author 刘虻
 * 2010-5-25 下午05:12:20
 */
@ClassInfo({"2014-06-06 18:42","配置信息类配置信息增强包"})
public interface IFilterConfigPro 
				extends FilterConfig,ICloneable {

	/**
	 * 设置初始化参数
	 * @author 刘虻
	 * 2010-5-25 下午05:12:20
	 * @param key 参数主键
	 * @param value 参数值
	 */
	void setInitParameter(String key,String value);
	
	
	/**
	 * 获取核心配置信息包
	 * @author 刘虻
	 * 2010-5-25 下午05:12:20
	 * @return 核心配置信息包
	 */
	FilterConfig getFilterConfig();
}
