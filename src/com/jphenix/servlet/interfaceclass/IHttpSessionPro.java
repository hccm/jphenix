/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

/**
 * HttpSession 扩展方法接口
 * @author 刘虻
 * 2007-3-25下午07:43:01
 */
@ClassInfo({"2014-06-06 18:44","HttpSession 扩展方法接口"})
public interface IHttpSessionPro extends HttpSession,ICloneable {

	/**
	 * 获取属性容器
	 * @author 刘虻
	 * 2008-7-22下午06:07:01
	 * @return 属性容器
	 */
	HashMap<String,Object> getAttributeMap();
}
