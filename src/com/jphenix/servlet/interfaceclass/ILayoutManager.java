/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 页面布局处理类
 * 
 * 2021-03-15 去掉了设置已经废弃的ServletManager方法
 * 
 * 
 * @author 刘虻
 * 2012-11-6 下午2:28:01
 */
@ClassInfo({"2021-03-15 17:46","页面布局处理类"})
@BeanInfo({"layoutmanager"})
public interface ILayoutManager {

	/**
	 * 处理页面布局
	 * 
	 * 如果在页面内容中出现
	 * 
	 * <!--*  layout="页面布局信息主键" -->
	 * 
	 * 在当前页面解析处理后，再进入这个环节，
	 * 将这个页面作为一个板块放入布局模板页面中，
	 * 并解析设置其它的板块，比如页头页尾等等。
	 * 最后输出整个页面。
	 * 
	 * 刘虻
	 * 2012-11-6 下午4:34:41
	 * @param vh 当前页面
	 * @param ac 动作上下文
	 * @return 处理后的页面
	 * @throws Exception 异常
	 */
	IViewHandler fixLayout(IViewHandler vh,IActionContext ac) throws Exception;
}
