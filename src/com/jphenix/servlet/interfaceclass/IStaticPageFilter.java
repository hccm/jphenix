/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年12月10日
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

/**
 * 静态页面过滤器
 * @author 马宝刚
 * 2014年12月10日
 */
public interface IStaticPageFilter {

    /**
     * 搜索指定根路径中的静态页面
     * 2014年12月10日
     * @author 马宝刚
     */
    void searchPath();
}
