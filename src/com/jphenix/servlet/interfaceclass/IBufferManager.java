/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 缓存管理类接口
 * @author 刘虻
 * 2010-6-3 上午10:43:32
 */
@ClassInfo({"2014-06-06 18:41","缓存管理类接口"})
@BeanInfo({"buffermanager"})
public interface IBufferManager {
	
	/**
	 * 是否启用缓存
	 * 刘虻
	 * 2010-6-3 下午06:00:35
	 * @return 是否启用缓存
	 */
	boolean isEnabledBuffer();
	
	
	/**
	 * 缓存关
	 * 
	 * 分为两步，在解析视图前执行一次，如果允许缓存，则直接 返回缓存。
	 * 如果无效缓存，则继续解析视图，解析结束后再调用一次，用来刷新缓存
	 * 
	 * vh 可以为空，这样用来在第一步检查是否整个页面缓存是否有效。
	 * 
	 * 刘虻
	 * 2010-6-3 下午09:30:34
	 * @param bufferKey 缓存主键 
	 * @param subAction 过程动作主键
	 * @param actionContext 动作上下文
	 * @param nh 视图对象
	 * @param outTime 超时时间（毫秒）
	 * @param isLast 是否为最后一步调用
	 * @param isOut 是否直接输出视图
	 * @return 0缓存是否有效("0"无效 "1"有效)  1缓存主键(这两个值都是在第一步返回的，缓存主键主要针对
	 * 			缓存无效时，在最后一步刷新缓存时做缓存索引文件名用)
	 * @throws Exception 执行发生异常
	 */
	String[] bufferGate(
			String bufferKey
			,String subAction
			,IActionContext actionContext
			,IViewHandler nh
			,long outTime
			,boolean isLast
			,boolean isOut) throws Exception;
}
