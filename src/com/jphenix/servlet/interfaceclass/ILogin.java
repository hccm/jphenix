/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2020年7月21日
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 登录服务接口
 * com.jphenix.servlet.interfaceclass.ILogin
 * 
 * 2020-07-21 修改了传入参数，使登录验证方法处理更加灵活
 * 
 * 
 * @author MBG
 * 2020年7月21日
 */
@ClassInfo({"2020-07-21 10:29","登录服务接口"})
public interface ILogin {

	/**
	 * 执行登录验证
	 * @param abp          响应请求的动作类父类
	 * @return             true 不再做后续处理，动作结束  false 处理并没结束，继续调用后续登录处理类
	 * @throws Exception   异常
	 * 2020年7月21日
	 * @author MBG
	 */
	boolean login(ActionBeanParent abp) throws Exception;
	
	
	/**
	 * 处理优先序号  由小到大
	 * @return       处理优先序号
	 * 2020年7月21日
	 * @author MBG
	 */
	int index();
}
