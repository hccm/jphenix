/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.interfaceclass;

import java.util.Map;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;

/**
 * 应用层会话处理接口
 * @author 刘虻
 * 2013-1-8 上午10:33:31
 */
@ClassInfo({"2014-06-06 18:40","应用层会话处理接口"})
@BeanInfo({"appsession"})
@Running({"1"})
public interface IAppSession {

	
	
	/**
	 * 建立会话信息
	 * 刘虻
	 * 2013-1-8 上午10:35:07
	 * @param outTime 会话超时时间（毫秒）
	 * @return 会话主键
	 */
	String createSession(long outTime);
	
	/**
	 * 注销会话信息
	 * 刘虻
	 * 2013-1-8 下午1:52:40
	 * @param key 会话主键
	 */
	void destorySession(String key);
	
	/**
	 * 检测会话信息是否超时
	 * 刘虻
	 * 2013-1-8 上午10:35:37
	 * @param key 会话主键
	 * @return true超时
	 */
	boolean checkSession(String key);
	
	/**
	 * 获取指定会话信息容器
	 * 刘虻
	 * 2013-1-8 上午10:42:02
	 * @param key 会话主键
	 * @return 会话信息容器
	 */
	Map<String,?> getSessionMap(String key);
	
	
	/**
	 * 获取指定属性值
	 * 刘虻
	 * 2013-1-8 下午3:27:15
	 * @param sessionKey 会话主键
	 * @param key 属性主键
	 * @return 属性值
	 */
	Object getAttribute(String sessionKey,String key);
}
