/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.action;

import com.jphenix.servlet.interfaceclass.IRootMenuService;
import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.servlet.service.RootMenuElement;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.share.lang.SString;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.IViewHandler;

import java.util.List;

/**
 * 根菜单动作类
 * com.jphenix.servlet.action.RootMenuAction
 * 
 * 2019-01-24 修改了父类类路径
 * 
 * @author 刘虻
 * 2010-8-11 下午01:34:04
 */
@ClassInfo({"2019-01-24 16:26","根菜单动作类"})
public class RootMenuAction extends ActionBeanParent {

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public RootMenuAction() {
		super();
	}
	
	/**
	 * 获取当前用户是否具备该功能块的权限
	 * 会话中的主键是 rolelist 元素是 List  序列中的元素是角色代码字符串
	 * 刘虻
	 * 2011-5-9 下午01:10:42
	 * @param rme 功能块元素
	 * @return true具备权限
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
    protected boolean hasRole(RootMenuElement rme) {
		//是否过滤掉公共菜单项
		boolean disabledPubMenu = SBoolean.valueOf($$$("disabledpubmenu"));
		if(rme==null || rme.getRoleCodeList().size()<1) {
			//没有设置角色代码，默认拥有权限
            return !disabledPubMenu;
        }
		//从会话中获取该用户的角色信息
		List<String> sessionRoleList = (List<String>)$$$("rolelist");
		if(sessionRoleList==null) {
			return false;
		}
		if(sessionRoleList.contains("_root") && !rme.isHiddenRoot()) {
			//如果角色中包含这个角色，无论什么功能点都能访问
			//并且该功能点没有设置超级用户访问时，隐藏该功能点
			return true;
		}
		for(int i=0;i<rme.getRoleCodeList().size();i++) {
			if(sessionRoleList.contains(rme.getRoleCodeList().get(i))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 子菜单列表
	 * 刘虻
	 * 2011-5-9 下午03:11:27
	 * @throws Exception 执行发生异常
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
    public void childList() throws Exception {
		//从会话中获取该用户的角色信息
		List<String> sessionRoleList = (List<String>)$$$("rolelist");
		if(sessionRoleList==null) {
			return;
		}
		if(sessionRoleList.contains("_root")) {
			//如果角色中包含这个角色，无论什么功能点都能访问
			return;
		}
		IViewHandler vh = getReVh(); //获取主界面
		//获取需要权限控制的节点
		List<IViewHandler> cVhList = vh.getChildNodesByAttribute("role","yes");
		for(IViewHandler cvh:cVhList) {
			if(cvh==null) {
				continue;
			}
			String roleCode = cvh.a("rolecode"); //角色信息
			if(roleCode.length()<1) {
				continue;
			}
			//分割成序列
			List<String> rCodeList = BaseUtil.splitToList(roleCode,",");
			boolean noRole = true; //没有权限
			String ele; //获取角色代码元素
			for(int j=0;j<rCodeList.size();j++) {
				ele = SString.valueOf(rCodeList.get(j)).toLowerCase().trim();
				if(ele.length()<1) {
					continue;
				}
				if(sessionRoleList.contains(ele)) {
					noRole = false;
					break;
				}
			}
			if(noRole) {
				cvh.oa(false);
			}
		}
	}
	
	/**
	 * 主菜单列表
	 * 会话中的主键是 rolelist 元素是 List  序列中的元素是角色代码字符串
	 * 刘虻
	 * 2010-8-12 上午09:35:50
	 * @throws Exception 执行发生异常
	 */
	public void list() throws Exception {
		//构建主菜单数据服务
		IRootMenuService rms =
                getBean(IRootMenuService.class);
		//获取菜单元素序列
		List<RootMenuElement> menuElementList = rms.getElementList();
		//构建新的视图对象
		IViewHandler vh = newVh();
		setReVh(vh); //设置为主视图
		vh.addChildNode(
				newVh()
				.setNodeText("<?xml version=\"1.0\" encoding=\""
						+getNodeLoader().getDealEncoding()+"\"?>\n"));
		//构建菜单根对象
		IViewHandler menuVh = 
			vh.addNewChildNode("menus").setAttribute("id","root");
		for(RootMenuElement rme:menuElementList) {
			if(hasRole(rme)) {
				menuVh
					.addNewChildNode("menu")
						.setOutSelf(false)
						.setNodeText(rme.toString());
			}
		}
	}
}
