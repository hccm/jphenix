/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.action;

import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.servlet.IErrorOutAction;

/**
 * 通用信息输出动作
 * 
 * 2019-01-24 修改了父类类路径
 * 
 * @author 刘虻
 * 2010-6-2 下午01:36:12
 */
@ClassInfo({"2019-01-24 16:26","通用信息输出动作"})
public class ErrorOutAction 
				extends ActionBeanParent 
						implements IErrorOutAction {

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ErrorOutAction() {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-6-10 下午01:07:12
	 */
	@Override
    public void out(IActionContext actionContext) throws Exception {}
}
