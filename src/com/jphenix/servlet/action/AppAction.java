/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.action;

import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegister;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegisterChild;
import com.jphenix.servlet.common.ActionContext;
import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.share.lang.SortVector;
import com.jphenix.standard.app.IAppInfo;
import com.jphenix.standard.app.IAppPurview;
import com.jphenix.standard.app.IModulePurview;
import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 菜单信息处理动作
 * 
 * 2019-01-24 修改了父类类路径
 * 2020-03-13 修改了从URL中获取请求参数
 * 
 * com.jphenix.servlet.action.AppAction
 * @author 刘虻
 * 2010-6-22 下午05:49:03
 */
@ClassInfo({"2020-03-13 17:54","菜单信息处理动作"})
public class AppAction extends ActionBeanParent implements IBeanRegister {

    //模块功能点信息序列 元素：每个模块的功能点信息类
	protected ArrayList<IAppInfo> appInfoList = new ArrayList<IAppInfo>();
				
    //排序后的模块功能点信息
	protected ArrayList<IAppInfo> appInfoFixList = null;
	
	//模块功能点信息对照容器
	protected HashMap<String,IAppInfo> appInfoMap = null;
	
	//功能点权限处理类容器  key:模块主键  value:功能点权限处理类
	protected HashMap<String,IAppPurview> appPurviewMap = new HashMap<String,IAppPurview>();
	
	protected IModulePurview modulePurview = null; //模块权限控制类
	
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public AppAction() {
		super();
	}
	
	
	/**
	 * 显示指定模块功能点信息
	 * 默认显示第一个模块功能点信息
	 * 刘虻
	 * 2010-6-22 下午06:29:49
	 * @param ac 动作上下文
	 * @throws Exception 执行发生异常
	 */
	public void appItemShow(ActionContext ac) throws Exception {
		//菜单主键
		String moduleID = ac.getUrlParameter("moduleid");
		IAppInfo appInfo = null; //菜单信息类
		if(moduleID.length()<1) {
			if(getMenuFixList().size()>0) {
				appInfo = getMenuFixList().get(0);
			}
		}else {
			appInfo = getAppInfoMap().get(moduleID);
		}
		if(appInfo==null) {
			return;
		}
		List<IAppPurview> itemList = null; //功能点信息表
		//获取对应的功能点权限处理类
		IAppPurview appPurview = appPurviewMap.get(appInfo.getModuleID());
		if(appPurview!=null) {
			itemList = appPurview.purview(appInfo.getAppInfoItemList());
		}
		if(itemList==null) {
			itemList = new ArrayList<IAppPurview>();
		}
	}
	
	
	/**
	 * 获取模块功能点信息对照容器
	 * 刘虻
	 * 2010-6-22 下午06:40:57
	 * @return 菜单信息对照容器
	 */
	protected synchronized HashMap<String,IAppInfo> getAppInfoMap() {
		if(appInfoMap==null) {
			appInfoMap = new HashMap<String,IAppInfo>();
			for(IAppInfo appInfo:appInfoList) {
				if(appInfo==null) {
					continue;
				}
				appInfoMap.put(appInfo.getModuleID(),appInfo);
			}
		}
		return appInfoMap;
	}
	
	/**
	 * 获取排序后的菜单信息序列
	 * 刘虻
	 * 2010-6-22 下午06:38:48
	 * @return 排序后的菜单信息序列
	 */
	protected synchronized ArrayList<IAppInfo> getMenuFixList() {
		if(appInfoFixList==null) {
			//构建排序容器
			SortVector<IAppInfo> appInfoSv = new SortVector<IAppInfo>();
			for(IAppInfo appInfo:appInfoList) {
				if(appInfo==null) {
					continue;
				}
				appInfoSv.add(appInfo,appInfo.getModuleIndex());
			}
			appInfoSv.asc(); //由小到大排序
			appInfoFixList = new ArrayList<IAppInfo>();
			while(appInfoSv.hasNext()) {
				appInfoFixList.add(appInfoSv.nextValue());
			}
		}
		return appInfoFixList;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-6-22 下午05:49:44
	 */
	@Override
    public void unRegist(Object bean) {
		if(bean==null) {
			return;
		}
		if(bean instanceof IAppInfo) {
	        if(bean instanceof IBeanRegisterChild) {
	            ((IBeanRegisterChild)bean).beforeUnRegister();
	        }
			appInfoList.remove(bean);
			return;
		}else if(bean instanceof IModulePurview) {
	        if(bean instanceof IBeanRegisterChild) {
	            ((IBeanRegisterChild)bean).beforeUnRegister();
	        }
			return;
		}else if(bean instanceof IAppPurview) {
	        if(bean instanceof IBeanRegisterChild) {
	            ((IBeanRegisterChild)bean).beforeUnRegister();
	        }
			appPurviewMap.remove(((IAppPurview)bean).getModuleID());
			return;
		}
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-6-22 下午05:49:44
	 */
	@Override
    public boolean regist(Object bean) {
		if(bean==null) {
			return false;
		}
		if(bean instanceof IAppInfo) {
			if(!appInfoList.contains(bean)) {
				appInfoList.add((IAppInfo)bean);
			}
            if(bean instanceof IBeanRegisterChild) {
                ((IBeanRegisterChild)bean).setRegister(this);
                ((IBeanRegisterChild)bean).afterRegister();
            }
			return true;
		}else if(bean instanceof IModulePurview) {
			this.modulePurview = (IModulePurview)bean;
            if(bean instanceof IBeanRegisterChild) {
                ((IBeanRegisterChild)bean).setRegister(this);
                ((IBeanRegisterChild)bean).afterRegister();
            }
			return true;
		}else if(bean instanceof IAppPurview) {
			appPurviewMap.put(((IAppPurview)bean).getModuleID(),(IAppPurview)bean);
            if(bean instanceof IBeanRegisterChild) {
                ((IBeanRegisterChild)bean).setRegister(this);
                ((IBeanRegisterChild)bean).afterRegister();
            }
			return true;
		}
		return false;
	}

}
