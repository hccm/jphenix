/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.action;

import com.jphenix.kernel.script.ScriptVO;
import com.jphenix.servlet.common.ClassicScriptActionParent;
import com.jphenix.share.tools.DynamicImageBean;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.docs.ScriptInfo;
import com.jphenix.standard.script.IScriptLoader;

import java.util.Map;

/**
 * 验证码图片动作类
 * 
 * com.jphenix.servlet.action.RandImageAction
 * 
 * 2019-01-24 修改了父类类路径
 * 2020-09-09 增加了需要实现接口的无用方法
 * 
 * @author 刘虻
 * 2011-8-22 上午09:33:01
 */
@ClassInfo({"2020-09-09 15:00","验证码图片动作类"})
@BeanInfo({"RANDIMAGE","false","jaractionbeanparent"})
@ScriptInfo({"","RANDIMAGE","RandImageAction","","","","","0","0","0","","","","com.jphenix.servlet.action.RandImageAction","验证码图片动作类","0","0","0","jaractionbeanparent","","0","0","0","0","0","0"})
@Running({"0","","",""})
public class RandImageAction extends ClassicScriptActionParent {
	
   /**
    * 构造函数
    * @param sl 脚本加载器
    * 2016-12-14 11:24:11
    */
   public RandImageAction(IScriptLoader sl,ScriptVO sVO) {
        super(sl,sVO);
   }

	/**
	 * 覆盖方法
	 */
	@Override
	public String getScriptId() {
		return "RANDIMAGE";
	}

	/**
	 * 脚本执行入口
	 * 2016-12-14 10:13:07
	 */
	@Override
    public void _executeN(Map<String,?> _in) throws Exception {
		//执行生成图片,并且将图片放入会话中(主键randimage)
		String code = (new DynamicImageBean()).createRandImageAction($(),$$());
		
		log("SessionID:["+$().getSession().getId()+"] ImageCode:["+code+"]");
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public Object _execute(Map<String,?> para) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean isClassVar() {
		return false;
	}
}
