/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月18日
 * V4.0
 */
package com.jphenix.servlet.multipart.instancea;

import com.jphenix.share.tools.FileCopyTools;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;

import java.io.File;

/**
 * 上传后的文件对象
 * 
 * 2018-07-05 移动文件后，可以通过getFile()方法获取到移动后的文件
 * 2018-09-10 增加了通过File对象构造上传文件的静态方法
 * 2019-01-03 增加了返回原文件扩展名
 * 2019-12-21 增加了静态方法，构造文件上传类，并设置指定文件对象以及上传前的文件名
 * 2020-01-13 在toString方法中增加了保存文件的绝对路径信息
 * 
 * @author 马宝刚
 * 2014年6月18日
 */
@ClassInfo({"2020-01-13 15:47","上传后的文件对象"})
public class UploadFile {

    public String tempPath       = null; //临时文件夹（相对路径）
    public String name           = null; //上传文件对象名
    public String srcFileName    = null; //上传前文件名
    public String objFileName    = null; //上传后的文件名
    public String filePath;              //上传后的文件路径(绝对路径)
    public String uploadBasePath = null; //文件上传根路径（绝对路径）
    
    //是否通过setFile设置的服务器上的文件
    //如果是通过setFile设置的文件，则获取文件时不加安全扩展名.save
    //并且执行delete方法无效
    private boolean serverFile = false;
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public UploadFile() {
        super();
    }
    
    /**
     * 获取上传的文件对象
     * @return 上传的文件对象
     * 2014年6月18日
     * @author 马宝刚
     */
    public File getFile() {
    	if(serverFile) {
    		return new File(filePath);
    	}
    	return new File(filePath+MultipartServletRequest.SAVED_EXT_NAME);
    }
    
    
    /**
     * 设置已经存在的文件
     * @param file 指定文件对象
     * @return 当前类实例
     * 2018年3月15日
     * @author MBG
     */
    public UploadFile setFile(File file) {
    	if(file==null) {
    		return this;
    	}
    	serverFile = true;
    	filePath = file.getPath();
    	srcFileName = SFilesUtil.getFileName(filePath);
    	objFileName = srcFileName;
    	return this;
    }
    
    /**
     * 设置已经存在的文件
     * @param file 指定文件对象
     * @return 构造的类实例
     * 2018年9月10日
     * @author MBG
     */
    public static UploadFile file(File file) {
    	return new UploadFile().setFile(file);
    }
    
    /**
     * 设置已经存在的文件
     * @param file         指定文件对象
     * @param srcFileName  文件上传之前的名称
     * @return             构造的类实例
     * 2019年12月21日
     * @author MBG
     */
    public static UploadFile file(File file,String srcFileName) {
    	//构建返回值
    	UploadFile res = new UploadFile().setFile(file);
    	if(srcFileName!=null && srcFileName.length()>0) {
    		//设置文件上传之前的名称
    		res.srcFileName = srcFileName;
    	}
    	return res;
    }
    
    /**
     * 返回文件扩展名
     * @return 文件扩展名
     * 2019年1月3日
     * @author MBG
     */
    public String extName() {
    	//文件名
    	String fileName = srcFileName;
    	if(fileName==null || fileName.length()<1) {
    		fileName = objFileName;
    	}
    	if(fileName==null || fileName.length()<1) {
    		return "";
    	}
    	//扩展名分隔符
    	int point = fileName.lastIndexOf(".");
    	if(point<0) {
    		return "";
    	}
    	return fileName.substring(point+1);
    }
    
    /**
     * 覆盖函数
     */
    @Override
    public String toString() {
    	return "UploadFile name:["+name+"] Source:["+srcFileName+"] Object:["+objFileName+"] File:["+getFile()+"]";
    }
    
    /**
     * 将当前文件移动到目标路径中（相对于类根路径）
     * @param objPath 目标路径
     * @return 是否移动成功
     * 2014年6月18日
     * @author 马宝刚
     */
    public boolean moveFile(String objPath) {
        if (objPath==null || objPath.length()<1) {
            return false;
        }   
        //上传后的文件
        File srcFile = getFile();
        if(!srcFile.exists()) {
            return false;
        }
        //获取目标全路径
        objPath = SFilesUtil.getAllFilePath(objPath,uploadBasePath);
        //目标文件对象
        File objFile = SFilesUtil.createFile(objPath);
        if(objFile.isDirectory()) {
            objFile =  SFilesUtil.createFile(objPath+"/"+objFileName);
        }
        if (objFile.exists()) {
            //删除目标文件
            objFile.delete();
        }
        try {
            //执行复制文件
            FileCopyTools.copy(srcFile,objFile);
        }catch(Exception e) {
            e.printStackTrace();
        }
        //删除源文件
        srcFile.delete();
        filePath = objPath+"/"+objFileName;
        serverFile = true;
        return true;
    }
    
    /**
     * 重新设置上传文件根路径  简称：basePath();
     * @param uploadBasePath 上传文件根路径
     * 2016年11月14日
     * @author MBG
     */
    public void setUploadBasePath(String uploadBasePath) {
    	if(uploadBasePath==null || uploadBasePath.trim().length()<1) {
    		return;
    	}
    	this.uploadBasePath = uploadBasePath;
    }
    
    /**
     * 重新设置上传文件路径 全称：setUploadBasePath
     * @param uploadBasePath 上传文件根路径
     * 2016年11月14日
     * @author MBG
     */
    public void basePath(String uploadBasePath) {
    	if(uploadBasePath==null || uploadBasePath.trim().length()<1) {
    		return;
    	}
    	this.uploadBasePath = uploadBasePath;
    }
    
    /**
     * 执行删除临时文件 简称：d();
     * 2016年11月14日
     * @author MBG
     */
    public void delete() {
    	if(serverFile) {
    		//这个文件是通过setFile设置进来的。
    		//在自动处理（框架中按照上传文件处理时）时，自动删除功能无效
    		return;
    	}
    	getFile().delete();
    }
    
    /**
     * 执行删除临时文件 全称：delete();
     * 2016年11月14日
     * @author MBG
     */
    public void d() {
    	if(serverFile) {
    		//这个文件是通过setFile设置进来的。
    		//在自动处理（框架中按照上传文件处理时）时，自动删除功能无效
    		return;
    	}
    	getFile().delete();
    }
}
