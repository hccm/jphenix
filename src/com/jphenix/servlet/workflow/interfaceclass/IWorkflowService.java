/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.workflow.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 工作流处理服务接口
 * @author 刘虻
 * 2012-11-8 上午11:51:42
 */
@ClassInfo({"2014-06-12 19:24","工作流处理服务接口"})
public interface IWorkflowService {}
