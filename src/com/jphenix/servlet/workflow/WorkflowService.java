/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.workflow;

import com.jphenix.servlet.parent.ServiceBeanParent;
import com.jphenix.servlet.workflow.interfaceclass.IWorkflowService;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 工作流处理服务
 * 
 * 2019-01-24 修改了父类的类路径
 * 
 * @author 刘虻
 * 2012-11-8 下午1:55:31
 */
@ClassInfo({"2019-01-24 16:44","工作流处理服务"})
public class WorkflowService extends ServiceBeanParent implements
		IWorkflowService {
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public WorkflowService() {
		super();
	}
}
