/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.workflow.script;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Java 脚本管理类
 * @author 刘虻
 * 2012-11-7 下午3:26:23
 */
@ClassInfo({"2014-06-12 19:26","Java 脚本管理类"})
public class JaScriptManager {
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public JaScriptManager() {
		super();
	}

}
