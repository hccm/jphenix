/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.servlet.workflow;

import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 工作流动作处理类
 * com.jphenix.servlet.workflow.WorkflowAction
 * 
 * 2019-01-24 修改了父类的类路径
 * 
 * 
 * @author 刘虻
 * 2012-11-8 上午10:40:13
 */
@ClassInfo({"2019-01-24 16:44","工作流动作处理类"})
public class WorkflowAction extends ActionBeanParent {

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public WorkflowAction() {
		super();
	}
}
