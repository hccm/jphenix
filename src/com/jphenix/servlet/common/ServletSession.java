/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月11日
 * V4.0
 */
package com.jphenix.servlet.common;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;

/**
 * Servlet内部会话（用来做内部调用不用再走Socket）
 * @author 马宝刚
 * 2014年9月11日
 */
@ClassInfo({"2014-09-11 21:21","Servlet内部会话（用来做内部调用不用再走Socket）"})
public class ServletSession implements HttpSession {

    //用来保存会话属性信息
    private Hashtable<String,Object> attributeMap = new Hashtable<String,Object>();
    //用来保存会话值信息
    private Hashtable<String,Object> valueMap = new Hashtable<String,Object>();
    private String id = null; //会话主键
    private long sessionCreateTime = 0; //会话建立时间
    private long sessionTime = 0; //会话时间
    private int liveTime = 0; //会话存活时间
    
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    @SuppressWarnings("deprecation")
	public ServletSession(HttpSession session) {
        super();
        if(session!=null) {
	        this.id = session.getId();
	        this.sessionCreateTime = session.getCreationTime();
	        this.sessionTime = session.getLastAccessedTime();
	        this.liveTime = session.getMaxInactiveInterval();
	        //获取会话信息主键枚举
	        Enumeration<String> names = session.getAttributeNames();
	        Object value; //会话属性值
	        if(names!=null) {
	        	String key; //会话属性主键
	        	while(names.hasMoreElements()) {
	        		key = names.nextElement();
	        		if(key==null) {
	        			continue;
	        		}
	        		value = session.getAttribute(key);
	        		if(value==null) {
	        			continue;
	        		}
	        		attributeMap.put(key,value);
	        	}
	        }
			String[] nameStrs = session.getValueNames();
	        if(nameStrs!=null) {
	        	for(int i=0;i<nameStrs.length;i++) {
	        		value = session.getValue(nameStrs[i]);
	        		if(value==null) {
	        			continue;
	        		}
	        		valueMap.put(nameStrs[i],value);
	        	}
	        }
        }
    }

    
    /**
     * 获取属性值
     */
    @Override
    public Object getAttribute(String arg0) {
        return  attributeMap.get(arg0);
    }

    /**
     * 获取属性主键枚举
     */
    @Override
    public Enumeration<String> getAttributeNames() {
        return attributeMap.keys();
    }

    /**
     * 获取会话建立时间
     */
    @Override
    public long getCreationTime() {
        return sessionCreateTime;
    }

    /**
     * 获取会话主键
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * 获取会话最后活动时间
     */
    @Override
    public long getLastAccessedTime() {
        return sessionTime;
    }

    /**
     * 获取会话超时时间（毫秒）
     */
    @Override
    public int getMaxInactiveInterval() {
        return liveTime;
    }

    /**
     * 获取Servlet上下文
     */
    @Override
    public ServletContext getServletContext() {
    	//不提供复杂方法
        return null;
    }

    /**
     * 获取会话上下文
     */
    @Deprecated
    @Override
    public javax.servlet.http.HttpSessionContext getSessionContext() {
    	//不提供复杂方法
        return null;
    }


    /**
     * 获取对应的值
     */
    @Override
    public Object getValue(String arg0) {
        return valueMap.get(arg0);
    }

    /**
     * 获取值主键数组
     */
    @Override
    public String[] getValueNames() {
        return BaseUtil.getMapKeys(valueMap);
    }

    /**
     * 注销当前会话
     */
    @Override
    public void invalidate() {
        attributeMap.clear();
        valueMap.clear();
    }

    /**
     * 是否为新生成的会话
     */
    @Override
    public boolean isNew() {
        return sessionTime==sessionCreateTime;
    }

    /**
     * 设置值
     */
    @Override
    public void putValue(String arg0, Object arg1) {
       if(arg0==null) {
           return;
       }
       if(arg1==null) {
           valueMap.remove(arg0);
           return;
       }
       valueMap.put(arg0,arg1);
    }

    /**
     * 移出属性值
     */
    @Override
    public void removeAttribute(String arg0) {
        attributeMap.remove(arg0);
    }

    /**
     * 移出指定值
     */
    @Override
    public void removeValue(String arg0) {
        valueMap.remove(arg0);
    }

    /**
     * 设置属性值
     */
    @Override
    public void setAttribute(String arg0, Object arg1) {
        if(arg0==null) {
            return;
        }
        if(arg1==null) {
            attributeMap.remove(arg0);
            return;
        }
        attributeMap.put(arg0,arg1);
    }

    /**
     * 设置会话超时时间（秒）
     */
    @Override
    public void setMaxInactiveInterval(int arg0) {
        liveTime = arg0;
    }

}
