/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.common;

import com.jphenix.servlet.parent.ServiceBeanParent;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;

/**
 * 任务服务父类
 * 
 * 注意：这个父类不是给脚本用的，所以没放在parent包中
 * 
 * 2019-01-24 修改了父类路径
 * 
 * 
 * @author 马宝刚
 * 2014年4月24日
 */
@ClassInfo({"2019-01-24 16:30","任务服务父类"})
@Running({"true","start"})
public abstract class JobBeanParent extends ServiceBeanParent {

    protected int aroundTime = 0; //调用任务间隔时间
    protected boolean jobRunning = false; //是否正在执行任务
    protected JobThread jobThread = null; //任务线程
    
    /**
     * 执行任务
     * @author 马宝刚
     * 2014年4月24日
     */
    protected class JobThread extends Thread {
        
        /**
         * 构造函数
         * @author 马宝刚
         */
        public JobThread(Class<?> cls) {
            super(cls.getName()+"_JobThread");
        }
        
        /**
         * 覆盖方法
         */
        @Override
        public void run() {
            //轮训间隔时间
            int aroundTime = getAroundTime();
            if(aroundTime<1) {
                aroundTime = 300000;
            }
            try {
                while(true) {
                    beforeRun();
                    jobRunning = true;
                    job(); //执行任务
                    jobRunning = false;
                    afterRun();
                    Thread.sleep(aroundTime);
                }
            }catch(Exception e) {
                e.printStackTrace();
                onJobException(e);
            }
        }
    }
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public JobBeanParent() {
        super();
    }

    /**
     * 执行指定任务
     * 2014年4月24日
     * @author 马宝刚
     * @throws Exception 异常
     */
    public abstract void job() throws Exception;
    
    /**
     * 执行任务前调用的方法
     * 2014年4月25日
     * @author 马宝刚
     */
    public void beforeRun() {}

    /**
     * 执行任务后调用的方法
     * 2014年4月25日
     * @author 马宝刚
     */
    public void afterRun() {}

    /**
     * 当执行任务时发生异常时，会调用该方法
     * 2014年4月24日
     * @param e 异常
     * @author 马宝刚
     */
    public  void onJobException(Exception e) {}

    /**
     * 启动服务
     * @throws Exception 异常
     * 2014年4月24日
     * @author 马宝刚
     */
    public void start() throws Exception {
        stop(); //先尝试停止
        jobThread = new JobThread(this.getClass());
        jobThread.start();
    }
    
    
    /**
     * 停止任务
     * 2014年4月24日
     * @author 马宝刚
     * @deprecated
     */
    public void stop() {
        if(jobThread!=null) {
            try {
                jobThread.stop();
            }catch(Exception e) {}
            jobThread = null;
        }
    }
    
    /**
     * 判断任务是否在执行中
     * @return
     * 2014年4月24日
     * @author 马宝刚
     */
    public boolean isJobRunning() {
        return jobRunning;
    }
    
    /**
     * 获取执行任务间隔时间
     * @return 执行任务间隔时间
     * 2014年4月24日
     * @author 马宝刚
     */
    public int getAroundTime() {
        return aroundTime;
    }
    
    /**
     * 设置执行任务间隔时间
     * @param arountTime 执行任务间隔时间
     * 2014年4月24日
     * @author 马宝刚
     */
    public void setAroundTime(int arountTime) {
        this.aroundTime = arountTime;
    }
}
