/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月28日
 * V4.0
 */
package com.jphenix.servlet.common;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;

/**
 * 会话信息类父类
 * @author 马宝刚
 * 2014年6月28日
 */
@ClassInfo({"2014-06-28 19:24","会话信息类父类"})
@BeanInfo({"","1"})
public abstract class SessionPara extends ABase {
    
    protected IActionContext ac = null; //动作上下文

    /**
     * 构造函数
     * @author 马宝刚
     */
    public SessionPara() {
        super();
    }
    
    /**
     * 获取存放在会话中的主键
     * @return 存放在会话中的主键
     * 2014年9月10日
     * @author 马宝刚
     */
    public abstract String getSessionKey();

    /**
     * 设置动作上下文
     * @param ac 动作上下文
     * @throws Exception 异常
     * 2014年6月28日
     * @author 马宝刚
     */
    public void setActionContext(IActionContext ac) {
        this.ac = ac;
    }
    
    /**
     * 注销当前会话信息
     * 2014年9月22日
     * @author 马宝刚
     */
    public void invalidate() {
        ac.getRequest().getSession().invalidate();
    }
    
    /**
     * 将值放入会话中
     * @param key   会话主键
     * @param obj   会话值
     * 2014年6月28日
     * @author 马宝刚
     */
    protected void putValue(String key,Object obj) {
        if(ac==null) {
            return;
        }
        ac.setSessionAttribute(key,obj);
    }
    
    /**
     * 从会话中获取指定值
     * @param key 参数主键
     * @return 参数值
     * 2014年8月1日
     * @author 马宝刚
     */
    public Object getValue(String key) {
    	return ac.getSessionAttribute(key);
    }
}
