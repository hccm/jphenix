/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.common;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 会话监听服务
 * com.jphenix.service.servlet.instancea.SessionListener
 * @author 刘虻 
 * 2007-6-15  上午10:32:56
 */
@ClassInfo({"2014-06-05 17:22","会话监听服务"})
public class SessionListener implements HttpSessionListener {

	protected long sessionCount = 0; //会话总数
	
	/**
	 * 构造函数 
	 * @author 刘虻
	 * 2007-6-15 上午10:33:00
	 */
	public SessionListener() {
		super();
	}

	/**
	 * 覆盖方法 
	 * @author  刘虻
	 * 2007-6-15 上午10:32:59
	 */
	@Override
    public void sessionCreated(HttpSessionEvent se) {
		sessionCount++;
		System.out.println("\nAfter Add Session Count:>"+sessionCount);
	}

	/**
	 * 覆盖方法 
	 * @author  刘虻
	 * 2007-6-15 上午10:32:59
	 */
	@Override
    public void sessionDestroyed(HttpSessionEvent se) {
		sessionCount--;
		System.out.println("\nAfter Destroy Session Count:>"+sessionCount);
	}
}
