/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.common;

import com.jphenix.driver.threadpool.ThreadSession;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.servlet.IServletBean;
import com.jphenix.standard.servlet.IServletConst;

/**
 * Servlet父类
 * 
 * 通常继承该类的都是不驻留内存的
 * 
 * 2018-11-12 将脚本中常用的方法提取到了BaseParent类中，作为当前类的父类，动作类跟服务类的父类，最终归为一个父类
 * 
 * @author 刘虻
 * 2013-1-8 下午2:36:23
 */
@ClassInfo({"2018-11-12 19:56","Servlet父类"})
public class ServletBeanParent extends BaseParent implements IServletBean {
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ServletBeanParent() {
		super();
	}
	
	/**
	 * 获取动作上下文
	 * 马宝刚
	 * 2010-6-30 下午09:03:35
	 * @return 动作上下文
	 */
	public IActionContext getActionContext() {
		return (IActionContext)ThreadSession.get(IServletConst.KEY_ACTION_CONTEXT);
	}
}
