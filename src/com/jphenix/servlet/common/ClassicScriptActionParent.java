/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年12月23日
 * V4.0
 */
package com.jphenix.servlet.common;

import com.jphenix.kernel.script.ScriptFieldVO;
import com.jphenix.kernel.script.ScriptVO;
import com.jphenix.servlet.parent.ActionBeanParent;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.docs.ScriptInfo;
import com.jphenix.standard.script.IScriptBean;
import com.jphenix.standard.script.IScriptLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * 纯java类编写的脚本动作类
 * 
 * 注意：这个父类是给传统开发模式中的动作类使用的，并不在脚本中使用
 * 
 * 废弃，没地方用，不赞成这么用
 * 
 * 2019-01-24 修改了父类类路径
 * 2019-04-09 增加了获取脚本对应的ScriptVO类实例
 * 
 * @author MBG
 * 2016年12月23日
 */
@ClassInfo({"2019-04-09 11:19","纯java类编写的脚本动作类"})
@BeanInfo({"CLASSICSCRIPTACTIONPARENT","true","jaractionbeanparent"})
@ScriptInfo({"","","","","","","","0","0","0","","","","com.jphenix.servlet.action.RandImageAction","","0","0","0","jaractionbeanparent","","0","0","0","0","0","0"})
@Running({"0","","",""})
public abstract class ClassicScriptActionParent extends ActionBeanParent implements IScriptBean {

	protected IScriptLoader _scriptLoader = null;	//ScriptLoader
	protected ScriptVO      _scriptVO     = null;	//ScriptVO
	
	/**
	 * 构造函数
	 * @param sl 脚本加载器
	 * 2016-12-14 11:24:11
	 */
	public ClassicScriptActionParent(IScriptLoader sl,ScriptVO sVO) {
		super();
		_scriptLoader = sl;
		_scriptVO = sVO;
	}

	

	@Override
	public List<ScriptFieldVO> getParameterList() {
		return new ArrayList<ScriptFieldVO>();
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public abstract String getScriptId();

	/**
	 * 覆盖方法
	 */
	@Override
	public List<String> getForScriptIds() {
		return new ArrayList<String>();
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptLoader getScriptLoader() {
		return _scriptLoader;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean noOutLog() {
		return true;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean isDbTransaction() {
		return false;
	}
	
	/**
	 * 是否允许动作类同一个用户并行执行
	 * 使用这个功能的具体原因请看ScriptVO类中，该变量的注释
	 * @return 是否允许动作类同一个用户并行执行
	 * 2016年12月23日
	 * @author MBG
	 */
	@Override
    public boolean isEnabledActionCharge() {
		return false;
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO getScriptVO() {
		return _scriptVO;
	}
}
