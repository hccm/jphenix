/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.servlet.exception;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Servlet 异常
 * @author 刘虻
 * 2010-5-21 下午09:03:50
 */
@ClassInfo({"2014-06-05 17:26","Servlet 异常"})
public class ServletException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -2896080902401799204L;

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ServletException() {
		super();
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param message
	 */
	public ServletException(String message) {
		super(message);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param cause
	 */
	public ServletException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param message
	 * @param cause
	 */
	public ServletException(String message, Throwable cause) {
		super(message, cause);
	}

}
