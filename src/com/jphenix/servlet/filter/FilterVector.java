/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.filter;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegister;
import com.jphenix.share.lang.SortVector;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IFilter;

import javax.servlet.FilterConfig;
import java.util.ArrayList;
import java.util.List;

/**
 * 过滤器类容器
 * com.jphenix.servlet.filter.FilterVector
 * 
 * 2019-06-15 在注册过滤器时，执行初始化方法
 * 
 * @author 刘虻
 * 2010-5-25 下午02:50:01
 */
@ClassInfo({"2019-06-15 19:34","过滤器类容器"})
@BeanInfo({"filtervector"})
public class FilterVector extends ABase implements IBeanRegister {

	protected FilterExplorer filterExplorer = null;  //总过滤器
	protected FilterConfig config = null; //配置信息类
	protected List<IFilter> filterList = new ArrayList<IFilter>(); //过滤器序列
	protected List<List<String>> filterExtList = new ArrayList<List<String>>(); //对应的过滤器响应扩展名序列
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public FilterVector(FilterExplorer filterExplorer,FilterConfig config) {
		super();
		setBase(filterExplorer);
		this.config = config;
		this.filterExplorer = filterExplorer;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午02:50:01
	 */
	public IFilter getFilter(int index) {
		try {
			return getFilterList().get(index);
		}catch(Exception e) {}
		return new NullFilter();
	}
	

	
	/**
	 * 获取过滤器触发的动作扩展名序列
	 * @param index 过滤器索引
	 * @return 扩展名序列
	 * 2014年9月12日
	 * @author 马宝刚
	 */
	public List<String> getFilterExtNameList(int index){
	    return filterExtList.get(index);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午02:50:01
	 */
	public List<IFilter> getFilterList() {
		return filterList;
	}
	
	/**
	 * 获取触发过滤器的动作扩展名序列
	 * @return 触发过滤器的动作扩展名序列
	 * 2014年9月12日
	 * @author 马宝刚
	 */
	public List<List<String>> getFilterExtNameList(){
	    return filterExtList;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午03:16:55
	 */
	@Override
    public void unRegist(Object bean) {
		if(bean==null 
				|| !(bean instanceof IFilter) 
				|| !filterList.contains(bean)) {
			return;
		}
		filterExtList.remove(filterList.indexOf(bean));
		filterList.remove(bean);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午03:16:55
	 */
	@Override
    public boolean regist(Object bean) {
		if(bean==null || !(bean instanceof IFilter)) {
			return false;
		}
		try {
			//执行初始化
			((IFilter)bean).init(filterExplorer,config);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		SortVector<IFilter> sv = new SortVector<IFilter>(); //排序容器
		for(IFilter ele:filterList) {
			sv.add(ele,ele.getIndex());
		}
		sv.add((IFilter)bean,((IFilter)bean).getIndex());
		sv.asc();
		
		//重新初始化
		filterList = new ArrayList<IFilter>();
		filterExtList = new ArrayList<List<String>>();
		String extName; //扩展名
		IFilter ele; //过滤器元素
		while(sv.hasNext()) {
		    ele = sv.nextValue();
			filterList.add(ele);
			
			extName = ele.getFilterActionExtName(); 
			if(extName==null || extName.length()<1) {
			    filterExtList.add(new ArrayList<String>());
			}else {
			    filterExtList.add(BaseUtil.splitToList(extName,","));
			}
		}
		return true;
	}
	
	

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午03:19:52
	 */
	public int getFilterCount() {
		return getFilterList().size();
	}
}
