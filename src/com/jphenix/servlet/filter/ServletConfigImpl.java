/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.filter;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Enumeration;

/**
 * 封装后的配置信息类
 * @author 刘虻
 * 2010-5-25 下午04:41:24
 */
@ClassInfo({"2014-06-06 18:39","封装后的配置信息类"})
public class ServletConfigImpl implements ServletConfig {

	protected FilterConfig filterConfig = null; //过滤器配置类
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ServletConfigImpl(FilterConfig filterConfig) {
		super();
		this.filterConfig = filterConfig;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:41:24
	 */
	@Override
    public String getInitParameter(String arg0) {
		return filterConfig.getInitParameter(arg0);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:41:24
	 */
	@Override
    public Enumeration<String> getInitParameterNames() {
		return filterConfig.getInitParameterNames();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:41:24
	 */
	@Override
    public ServletContext getServletContext() {
		return filterConfig.getServletContext();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:41:24
	 */
	@Override
    public String getServletName() {
		return filterConfig.getFilterName();
	}

}
