/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年12月10日
 * V4.0
 */
package com.jphenix.servlet.filter;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.share.util.StringUtil;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Register;
import com.jphenix.standard.docs.Running;
import com.jphenix.standard.servlet.IFilter;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.standard.servlet.IResponse;

import javax.servlet.FilterConfig;
import java.util.ArrayList;
import java.util.List;

/**
 * 静态页面自动切换过滤器
 * 
 * 扫描指定静态页面文件夹，如果跟请求的路径相同，
 * 则自动转换为输出静态页面，而不是动态页面
 * com.jphenix.servlet.filter.StaticPageFilter
 * @author 马宝刚
 * 2014年12月10日
 */
@ClassInfo({"2014-06-03 18:02","静态页面自动切换过滤器"})
@BeanInfo({"staticpagefilter"})
@Running({"91","init"})
@Register({"filtervector"})
public class StaticPageFilter extends ABase implements IFilter {

  //静态页面文件根路径  可以在配置文件中重新指定
    protected String         staticPageBasePath  = "/static_pages";
    private   List<String>   pagePathList        = new ArrayList<String>(); //静态页面文件路径序列 A
    private   FilterExplorer fe                  = null;                    //过滤器管理类
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public StaticPageFilter() {
        super();
    }
    
	/**
	 * 执行初始化
	 * @param fe         过滤器管理类
	 * @param config     Servlet配置信息类
	 * @throws Exception 异常（如果初始化发生异常，则放弃不再使用）
	 * 2019年6月15日
	 * @author MBG
	 */
	@Override
	public void init(FilterExplorer fe, FilterConfig config) throws Exception {
		this.fe = fe;
		searchPath();
	}
	
    /**
     * 重新搜索静态页面
     * 2014年12月10日
     * @author 马宝刚
     */
    public void searchPath() {
        //整理根路径
        if(staticPageBasePath!=null) {
            if(!staticPageBasePath.endsWith("/")) {
                staticPageBasePath += "/";
            }
        }
        pagePathList.clear();
        try {
            filesUtil.setFilePath(pagePathList,staticPageBasePath,"html",true,true);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 覆盖方法
     */
    @Override
    public int getIndex() {
        return 0; //优先级最高，一但存在静态页面，啥也不处理，直接返回静态页面
    }

    /**
     * 覆盖方法
     */
    @Override
    public String getFilterActionExtName() {
        return "htm,ha";
    }

    /**
     * 覆盖方法
     */
    @Override
    public boolean doFilter(IRequest req, IResponse resp) throws Exception {
        //对应的静态文件路径
        String pagePath = StringUtil.toStaticPageName(req.getServletPath(),req.getQueryString(),true);
        if(pagePath==null) {
            //如果参数中增加了__sp__=__sp__ 说明是获取动态页面中的内容，所以不能直接返回静态内容
            return false;
        }
        if(pagePath.startsWith("/")) {
            pagePath = pagePath.substring(1);
        }
        pagePath+=".html";
        if(pagePathList.contains(pagePath)) {
            //读取指定的静态文件
        	fe.serveFile(filesUtil.getAllFilePath(staticPageBasePath+pagePath),req,resp);
            return true;
        }
        return false;
    }
}
