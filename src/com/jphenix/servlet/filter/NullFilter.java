/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-06
 * V4.0
 */
package com.jphenix.servlet.filter;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IFilter;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.standard.servlet.IResponse;

import javax.servlet.FilterConfig;

/**
 * 空的过滤类(通常在发生异常时使用)
 * 
 * 2019-06-15 按照IFilter增加了过滤器初始化方法
 * 
 * @author 刘虻
 * 2010-5-25 下午04:12:02
 */
@ClassInfo({"2019-06-15 18:17","空的过滤类"})
public class NullFilter implements IFilter {
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public NullFilter() {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:12:02
	 */
	@Override
    public boolean doFilter(IRequest req, IResponse resp) throws Exception {
		return false;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-25 下午04:12:02
	 */
	@Override
    public int getIndex() {
		return 0;
	}

    /**
     * 获取需要过滤的动作路径扩展名
     * 
     * 多个扩展名用逗号分割
     * 
     * 多个扩展名用半角逗号分割
     * 
     * 扩展名前不用加半角聚号（.)
     * 
     * 如果返回空，或者空字符串，说明需要过滤全部动作路径（不建议这么做）
     * 
     * 如果需要过滤无扩展名的动作，用半角减号（-）标记
     * 
     * @return
     * 2014年9月12日
     * @author 马宝刚
     */
    @Override
    public String getFilterActionExtName() {
        return null;
    }
    
	/**
	 * 执行初始化
	 * @param fe         过滤器管理类
	 * @param config     Servlet配置信息类
	 * @throws Exception 异常（如果初始化发生异常，则放弃不再使用）
	 * 2019年6月15日
	 * @author MBG
	 */
	@Override
	public void init(FilterExplorer fe, FilterConfig config) throws Exception {}
}
