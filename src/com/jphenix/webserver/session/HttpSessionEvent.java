/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.session;

import javax.servlet.http.HttpSession;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 会话事件类
 * @author 刘虻
 * 2008-6-29下午08:56:41
 */
@ClassInfo({"2014-06-13 19:00","会话事件类"})
public class HttpSessionEvent extends javax.servlet.http.HttpSessionEvent {

	/**
	 * 串行表示
	 */
	private static final long serialVersionUID = -2639963956848752317L;

	/**
	 * 构造函数
	 * 2008-6-29下午08:56:45
	 */
	public HttpSessionEvent(HttpSession arg0) {
		super(arg0);
	}
}
