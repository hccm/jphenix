/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.filter;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

/**
 * 过滤器配置信息类
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-6-29下午08:27:28
 */
@ClassInfo({"2019-09-17 10:41","过滤器配置信息类"})
public class FilterConfigImpl implements FilterConfig {

	private String                   filterName     = null; //过滤器名
	private Hashtable<String,Object> paraMap        = null; //参数容器
	private ServletContext           servletContext = null; //Servlet 上下文
	
	/**
	 * 构造函数
	 * 2008-6-29下午08:27:28
	 */
	public FilterConfigImpl() {
		super();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-29下午08:27:28
	 */
	@Override
    public String getFilterName() {
		return filterName;
	}
	
	/**
	 * 获取参数容器
	 * @author 刘虻
	 * 2008-6-29下午08:30:04
	 * @return 参数容器
	 */
	public Map<String,Object> getParameterMap() {
		if (paraMap==null) {
			paraMap = new Hashtable<String,Object>();
		}
		return paraMap;
	}
	
	/**
	 * 设置参数容器
	 * @author 刘虻
	 * 2008-6-29下午08:30:30
	 * @param paraMap 参数容器
	 */
    public void setParameterMap(Map<String,Object> paraMap) {
		if (paraMap!=null) {
			if (paraMap instanceof Hashtable) {
				this.paraMap = (Hashtable<String,Object>)paraMap;
			}else {
				this.paraMap = new Hashtable<String,Object>();
				try {
					this.paraMap.putAll(paraMap);
				}catch(Exception e) {}
			}
		}
	}
	
	/**
	 * 设置过滤器名
	 * @author 刘虻
	 * 2008-6-29下午08:29:00
	 * @param filterName 过滤器名
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-29下午08:27:28
	 */
	@Override
    public String getInitParameter(String arg0) {
		return (String)getParameterMap().get(arg0);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-29下午08:27:28
	 */
	@Override
    public Enumeration<String> getInitParameterNames() {
		return ((Hashtable<String,Object>)getParameterMap()).keys();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-29下午08:27:28
	 */
	@Override
    public ServletContext getServletContext() {
		return servletContext;
	}
	
	
	/**
	 * 设置Servlet上下文
	 * @author 刘虻
	 * 2008-6-29下午08:33:47
	 * @param servletContext Servlet上下文
	 */
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
