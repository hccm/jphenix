/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.webserver.servlet.ServletInfoBean;

/**
 * 服务参数处理接口
 * @author 刘虻
 * 2007-9-29下午04:49:39
 */
@ClassInfo({"2014-06-13 18:31","服务参数处理接口"})
public interface IServeParameter {

	/**
	 * 获取网站根路径
	 * @author 刘虻
	 * 2007-9-29下午04:48:38
	 * @return 网站根路径
	 */
	String getWebBasePath();
	
	/**
	 * 获取服务类根路径
	 * @author 刘虻
	 * 2008-6-28下午03:25:12
	 * @return 服务类根路径
	 */
	String getServletBasePath();
	
	/**
	 * 获取物理路径
	 * @author 刘虻
	 * 2008-6-28下午03:24:17
	 * @param path 相对路径
	 * @return 物理路径
	 */
	String getRealPath(String path);
	
	/**
	 * 整理启动服务参数
	 * @author 刘虻
	 * 2006-9-15下午12:21:24
	 * @param args 导入参数
	 * @throws Exception 整理参数时发生异常
	 */
	void initProp(String[] args) throws Exception;
	
	
	/**
	 * 获取参数容器
	 * @author 刘虻
	 * 2007-9-30下午03:45:53
	 * @return 参数容器
	 */
	Map<String,Object> getArgumentMap();
	
	
	/**
	 * 获取指定参数值
	 * @author 刘虻
	 * 2007-9-30下午03:48:06
	 * @param key 指定参数主键
	 * @return 指定参数值
	 */
	String getArgumentString(String key);
	
	
	/**
	 * 获取指定参数值
	 * 注意:保存时就要保存Boolean值
	 * @author 刘虻
	 * 2007-9-30下午03:59:07
	 * @param key 指定参数主键
	 * @return 指定参数值
	 */
	boolean getArgumentBoolean(String key);
	
	
	/**
	 * 获取指定参数值
	 * @author 刘虻
	 * 2007-9-30下午03:59:07
	 * @param key 指定参数主键
	 * @return 指定参数值
	 */
	int getArgumentInt(String key);
	
	
	/**
	 * 获取Mime类型
	 * @author 刘虻
	 * 2008-6-28下午07:44:23
	 * @param key 主键
	 * @return 值
	 */
	String getMimeType(String key);
	
	
	/**
	 * 获取会话监听信息序列
	 * @author 刘虻
	 * 2008-6-28下午08:49:56
	 * @return 会话监听信息序列
	 */
	List<String> getSessionListenerClassList();
	
	
	/**
	 * 获取过滤器信息序列
	 * @author 刘虻
	 * 2008-6-28下午08:50:28
	 * @return 过滤器信息序列
	 */
	List<ServletInfoBean> getFilterInfoList();
	
	
	/**
	 * 获取Servlet信息序列
	 * @author 刘虻
	 * 2008-6-28下午08:49:22
	 * @return Servlet信息序列
	 */
	List<ServletInfoBean> getServletInfoList();
	
	
	/**
	 * 获取默认访问文件序列
	 * @author 刘虻
	 * 2009-7-22下午03:18:08
	 * @return 默认访问文件序列
	 */
	List<String> getWelcomeFileList();
	
	/**
	 * 获取配置文件路径
	 * 刘虻
	 * 2012-9-13 下午10:03:21
	 * @return 配置文件路径
	 */
	String getConfigFilePath();
}
