/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;


/**
 * web服务全局变量
 * 
 * 2019-08-13 修改了报文头为标准格式（原来都是小写的）
 * 
 * 
 * @author 刘虻
 * 2006-8-20下午11:24:02
 */
@ClassInfo({"2019-08-13 14:54","IGlobalVar"})
public interface IGlobalVar {
	
	/**
	 * 系统名称
	 */
	String SERVER_NAME = "Phenix";

	/**
	 * 系统版本号
	 */
	String SERVER_VERSION = "1.2";

	/**
	 * 文本配置文件
	 */
	String CONFIG_TEXT_FILE_NAME = "phserver.xml";
	
	/**
	 * 相关链接地址
	 */
	String SERVER_URL = "http://www.jphenix.org";
	
	/**
	 * UTF-8编码
	 */
	String UTF82 = "UTF-8";
	
	/**
	 * ISO-8859-1编码
	 */
	String ISO88591 = "ISO-8859-1";
	
	/**
	 * 默认页面背景颜色
	 */
	String BGCOLOR = "BGCOLOR=\"#D1E9FE\"";
	
	/**
	 * 默认服务器名
	 */
	String progName = "Serve";

	/**
	 * 监听端口
	 */
	String ARG_PORT = "port";
	
	
	/**
	 * 安全会话监听端口
	 */
	String ART_SSL_PORT = "sslport";

	/**
	 * 私有Servlet配置文件路径
	 */
	String ARG_THROTTLES = "throttles";
		
	/**
	 * 服务器绑定地址
	 */
	String ARG_BINDADDRESS = "bind-address";

	/**
	 * 是否输出服务器普通日志
	 */
	String ARG_OUT_SERVLOG = "out-serv-log";

	/**
	 * 默认并发连接数参数
	 */
	String ARG_BACKLOG = "backlog";

	/**
	 * 网站启动后执行的命令行
	 */
	String ARG_AFTER_START_EXECUTE_COMMAND = "after-start-execute-command";
	
	/**
	 * 会话超时时间(秒)
	 */
	String ARG_SESSION_TIMEOUT = "session-timeout";

	/**
	 * 是否在控制窗口控制网站 yes(可以在控制台终止服务) no不支持
	 */
	String ARG_NOTNOHUP = "notnohup";

	/**
	 * 是否浏览器与服务器建立持久连接
	 */
	String ARG_KEEPALIVE = "keep-alive";

	/**
	 * 默认日志编码
	 */
	String DEF_LOGENCODING = "UTF-8";

	/**
	 * 浏览器与服务器保持连接超时时间
	 */
	String ARG_KEEPALIVE_TIMEOUT = "timeout-keep-alive";

	/**
	 * 服务器与浏览器最大连接数
	 */
	String ARG_MAX_CONN_USE = "max-alive-conn-use";

	/**
	 * 服务器最大线程数
	 */
	String ARG_MAX_EXECUTE_THREAD_COUNT = "max-execute_thread_count";
	
	/**
	 * Servlet传输编码格式
	 */
	String ARG_SERVLET_TRANSFER_ENCODING = "transfer-encoding";
	
	/**
	 * 虚拟路径
	 */
	String ARG_CONTEXT_PATH = "context-path";
	
	/**
	 * 默认会话超时时间(默认30分钟)
	 */
	int DEF_SESSION_TIMEOUT = 1800;

	/**
	 * 默认端口
	 */
	int DEF_PORT = 80;
	
	/**
	 * 默认安全端口
	 */
	int DEF_SSLPORT = 443;
	
	/**
	 * 默认HTML格式
	 */
	String WWWFORMURLENCODE = "application/x-www-form-urlencoded";

	/**
	 * 传输编码格式
	 */
	String TRANSFERENCODING = "Transfer-Encoding";

	/**
	 * 是否与浏览器保持连接
	 */
	String KEEPALIVE = "Keep-Alive";

	/**
	 * 连接关键字
	 */
	String CONNECTION = "Connection";

	/**
	 * HTML头参数
	 */
	String CHUNKED = "chunked";

	/**
	 * HTML内容长度
	 */
	String CONTENTLENGTH = "Content-Length";

	/**
	 * HTML内容类型
	 */
	String CONTENTTYPE = "Content-Type";

	/**
	 * HMTL头信息
	 */
	String SETCOOKIE = "Set-Cookie";

	/**
	 * 主机地址
	 */
	String HOST = "Host";

	/**
	 * Cookie
	 */
	String COOKIE = "Cookie";

	/**
	 * HTML Head 信息
	 */
	String ACCEPT_LANGUAGE = "Accept-Language";

	/**
	 * 是否启用ssl连接
	 */
	String ART_ENABLED_SSL = "enabled-ssl";
	
	/**
	 * 会话主键名
	 */
	String ARG_SESSION_COOKIE_NAME = "SessionCookieName";
	
	/**
	 * 默认会话主键名
	 */
	String DEF_SESSION_COOKIE_NAME = "JSESSIONID";

	/**
	 * jsessionid=
	 */
	String SESSION_URL_NAME = ";$sessionid$";
	
	/**
	 * SUNX509
	 */
    String ARG_ALGORITHM = "algorithm";

    /**
     * false
     */
    String ARG_CLIENTAUTH = "clientAuth";

    /**
     * System.getProperty("user.home") + File.separator + ".keystore";
     */
    String ARG_KEYSTOREFILE = "keystoreFile";

    /**
     * KEYSTOREPASS
     */
    String ARG_KEYSTOREPASS = "keystorePass";

    /**
     * KEYSTORETYPE
     */
    String ARG_KEYSTORETYPE = "keystoreType";

    /**
     * keyPass
     */
    String ARG_KEYPASS = "keyPass";

    /**
     * TLS
     */
    String ARG_PROTOCOL = "protocol";

    /**
     * ifAddress
     */
    String ARG_IFADDRESS = "ifAddress";

    /**
     * com.sun.net.ssl.internal.www.protocol
     */
    String PROTOCOL_HANDLER = "com.sun.net.ssl.internal.www.protocol";

    /**
     * The name of the system property containing a "|" delimited list of
     * protocol handler packages.
     */
    String PROTOCOL_PACKAGES = "java.protocol.handler.pkgs";

    /**
     * Certificate encoding algorithm to be used.
     */
    String SUNX509 = "SunX509";

    /**
     *  default backlog
     */
    int BACKLOG = 1000;

    /**
     * Storeage type of the key store file to be used.
     */
    String KEYSTORETYPE = "JKS";

    /**
     * SSL protocol variant to use.
     */
    String TLS = "TLS";

    /**
     * SSL protocol variant to use.
     */
    String protocol = TLS;
    
    /**
     * 默认线程池最大线程数
     */
	int DEF_MAX_POOLED_THREAD = 20;

	/**
	 * 最大线程数
	 */
	String ARG_THREAD_POOL_SIZE = "Max Threead:"+DEF_MAX_POOLED_THREAD;
	
	/**
	 * 缓存
	 */
	int COPY_BUF_SIZE = 4096 * 2;
	
	/**
	 * 默认连接数
	 */
	int DEF_MAX_CONN_USE = 100;
	
	/**
	 * 在没选中文件时，是否显示文件列表
	 */
	String ARG_LIST_FILE = "listings";
	
	/**
	 * 网站根路径
	 */
	String WEB_BASE_PATH = "WebBasePath";
	
	/**
	 * Servlet根路径
	 */
	String SERVLET_BASE_PATH = "ServletBasePath";
	
	/**
	 * Servlet配置文件根路径
	 */
	String SERVLET_ROOT_XML_PATH = "ServletRootXmlPath";
	
}
