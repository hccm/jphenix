/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import java.io.PrintStream;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 服务日志类接口
 * @author 刘虻
 * 2007-9-30下午02:50:54
 */
@ClassInfo({"2014-06-13 18:44","服务日志类接口"})
public interface ILog {

	/**
	 * 输入异常日志
	 * @author 刘虻
	 * 2006-9-15上午09:44:51
	 * @param exception 异常对象
	 * @param message 日志信息
	 */
	void log(Exception exception, String message);
	
	/**
	 * 获取日志流
	 * @author 刘虻
	 * 2007-9-30下午02:47:41
	 * @return 日志流
	 */
	PrintStream getLogStream();

	/**
	 * 设置日志流
	 * @author 刘虻
	 * 2007-9-30下午02:47:54
	 * @param logStream 日志流
	 */
	void setLogStream(PrintStream logStream);


	/**
	 * 获取日志编码
	 * @author 刘虻
	 * 2007-9-30下午02:48:02
	 * @return 日志编码
	 */
	String getLogEncoding();
	
	
	/**
	 * 设置日志编码
	 * @author 刘虻
	 * 2007-9-30下午02:48:18
	 * @param logEncoding 日志编码
	 */
	void setLogEncoding(String logEncoding);


	/**
	 * 获取普通日志文件头名
	 * @author 刘虻
	 * 2007-9-30下午02:48:54
	 * @return 普通日志文件头名
	 */
	String getLogFileHeadName();

	/**
	 * 设置普通日志文件头名
	 * @author 刘虻
	 * 2007-9-30下午02:49:12
	 * @param logFileHeadName 普通日志文件头名
	 */
	void setLogFileHeadName(String logFileHeadName);



	/**
	 * 获取错误日志类名
	 * @author 刘虻
	 * 2007-9-30下午02:49:58
	 * @return 错误日志类名
	 */
	String getErrorLogClassName();


	/**
	 * 设置错误日志类名
	 * @author 刘虻
	 * 2007-9-30下午02:50:10
	 * @param errorLogClassName 错误日志类名
	 */
	void setErrorLogClassName(String errorLogClassName);

	/**
	 * 获取错误日志流
	 * @author 刘虻
	 * 2007-9-30下午02:50:17
	 * @return 错误日志流
	 */
	PrintStream getErrorPrintStream();


	/**
	 * @deprecated 不赞成使用，如果设置进来的流对象被别的类方法关闭了，或者为空
	 * 就会导致失败
	 * @param errorPrintStream 要设置的 errorPrintStream
	 */
	void setErrorPrintStream(PrintStream errorPrintStream);
	
	

	/**
	 * 获取工作路径
	 * @author 刘虻
	 * 2007-9-30下午02:48:27
	 * @return 工作路径
	 */
	String getWorkPath();



	/**
	 * 设置工作路径
	 * @author 刘虻
	 * 2007-9-30下午02:48:47
	 * @param workPath 工作路径
	 */
	void setWorkPath(String workPath);
	
	
	/**
	 * 获取系统日志流
	 * @author 刘虻
	 * 2006-9-15上午11:22:18
	 * @param arguments 导入参数容器 from=givenpath;dir=realpath
	 * @return 系统日志流
	 */
	PrintStream getPrintStream();

	
	
	/**
	 * 输出日志
	 * @author 刘虻
	 * 2006-9-15上午09:25:47
	 * @param message 日志信息
	 */
	void log(String message);

	/**
	 * 输出日志
	 * @author 刘虻
	 * 2006-9-15上午09:39:17
	 * @param message 日志信息
	 * @param throwable 断点对象
	 */
	void log(String message, Throwable throwable);
	
	/**
	 * 是否输出普通日志
	 * @author 刘虻
	 * 2008-6-29下午09:47:18
	 * @param outLog 是否输出普通日志
	 */
	void setOutLog(boolean isOutLog);
	
}
