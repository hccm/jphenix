/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import java.io.IOException;
import java.net.ServerSocket;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Socket类工厂
 * @author 刘虻
 * 2006-9-15上午12:03:29
 */
@ClassInfo({"2014-06-13 18:44","Socket类工厂"})
public interface SocketFactory {
	
	/**
	 * 建立Socket
	 * @author 刘虻
	 * 2008-7-9上午08:14:26
	 * @param serveParameter 配置信息包
	 * @return Socket
	 * @throws IOException 输入输出异常
	 * @throws IllegalArgumentException 类异常
	 */
	ServerSocket createSocket(IServeParameter serveParameter) throws IOException,IllegalArgumentException;
}