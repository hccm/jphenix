/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.webserver.instancea.Serve;


/**
 * War文件部署类
 * @author 刘虻
 * 2008-7-9上午08:16:30
 */
@ClassInfo({"2014-06-13 18:44","War文件部署类"})
public interface WarDeployer {
	
	/**
	 * 执行附属
	 * @author 刘虻
	 * 2008-7-9上午08:16:51
	 * @param server 服务器
	 */
    void deploy(Serve server);
}
