/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 线程工厂
 * @author 刘虻
 * 2006-8-25下午09:27:15
 */
@ClassInfo({"2014-06-13 18:44","线程工厂"})
public interface ThreadFactory {
	
	/**
	 * 建立线程
	 * @author 刘虻
	 * 2008-7-9上午08:15:44
	 * @param runnable 运行时
	 * @return 线程
	 */
	Thread create(Runnable runnable);
}
