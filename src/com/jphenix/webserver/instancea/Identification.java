/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.webserver.interfaceclass.IGlobalVar;

/**
 * 系统信息 
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2006-9-15上午12:17:08
 */
@ClassInfo({"2019-09-17 10:36","系统信息Identification"})
public class Identification {
	
	/**
	 * 输出基本HTML格式的系统信息
	 * @author 刘虻
	 * 2006-9-15上午09:00:11
	 * @param o 输出流对象
	 * @throws IOException 输出时发生异常
	 */
	public static void writeAddress(OutputStream o) throws IOException {
		//构建输出对象
		PrintStream p = new PrintStream(o);
		p.println(
				"<ADDRESS><A HREF=\"" + IGlobalVar.SERVER_NAME + "\">" 
				+ IGlobalVar.SERVER_NAME + " " + IGlobalVar.SERVER_VERSION + "</A></ADDRESS>");
	}

	/**
	 * 输出基本HTML格式的系统信息
	 * @author 刘虻
	 * 2006-9-15上午09:00:11
	 * @param sbf 字符串缓存
	 * @throws IOException 输出时发生异常
	 */
	public static void writeAddress(StringBuffer sbf) throws IOException {
		sbf.append("<ADDRESS><A HREF=\"" + IGlobalVar.SERVER_URL + "\">" 
				+ IGlobalVar.SERVER_NAME + " " + IGlobalVar.SERVER_VERSION + "</A></ADDRESS>");
	}
}
