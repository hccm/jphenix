/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * WildcardDictionary
 * 
 * 2019-09-17 整理了代码格式
 * 2020-09-12 去掉了提醒注释
 * 
 * @author 刘虻
 * 2007-3-6下午01:38:17
 */ 
@ClassInfo({"2020-09-12 16:33","WildcardDictionary"})
public class WildcardDictionary extends Dictionary<String,Object> {

	private Vector<String> keys;
	private Vector<Object> elements;

	/**
	 * 构造函数
	 * 2008-7-9上午08:12:55
	 */
	public WildcardDictionary() {
		keys     = new Vector<String>();
		elements = new Vector<Object>();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:02
	 */
	@Override
    public int size() {
		return elements.size();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:07
	 */
	@Override
    public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:11
	 */
	@Override
    public Enumeration<String> keys() {
		return keys.elements();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:15
	 */
	@Override
    public Enumeration<Object> elements() {
		return elements.elements();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:19
	 */
	@Override
    public synchronized Object get(Object key) {
		String sKey = (String) key;
		int matching_len = 0, found = -1;
		// to optimize speed, keys should be sorted by length
		String thisKey;
		for (int i = keys.size() - 1; i > -1; i--) {
			thisKey = keys.elementAt(i);
			int current = matchSpan(thisKey, sKey);
			if (current > matching_len) {
				found = i;
				matching_len = current;
			}
		}
		if (found > -1) {
            return elements.elementAt(found);
        }
		return null;
	}

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午08:13:24
	 * @param src 母鸡
	 * @return 母鸡
	 */
	public static String trimPathSeparators(String src) {
		StringBuffer result = new StringBuffer(src.length());
		boolean ms = false;
		for (int i = 0; i < src.length(); i++) {
			char c = src.charAt(i);
			if (c == '/' || c == '\\') {
				if (!ms) {
					result.append(c);
					ms = true;
				}
			} else {
				result.append(c);
				ms = false;
			}
		}
		return result.toString();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:44
	 */
	@Override
    public synchronized Object put(String key, Object element) {
		int i = keys.indexOf(key);
		if (i != -1) {
			Object oldElement = elements.elementAt(i);
			elements.setElementAt(element, i);
			return oldElement;
		} else {
			keys.addElement(key);
			elements.addElement(element);
			return null;
		}
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-9上午08:13:51
	 */
	@Override
    public synchronized Object remove(Object key) {
		int i = keys.indexOf(key);
		if (i != -1) {
			Object oldElement = elements.elementAt(i);
			keys.removeElementAt(i);
			elements.removeElementAt(i);
			return oldElement;
		} else {
            return null;
        }
	}
	
	
	


    /**
     * 母鸡
     * @author 刘虻
     * 2008-7-9上午07:45:36
     * @param pattern 母鸡
     * @param string 母鸡
     * @return 母鸡
     */
	protected boolean match(String pattern, String string) {
		for (int p = 0;; ++p) {
			for (int s = 0;; ++p, ++s) {
				boolean sEnd = (s >= string.length());
				boolean pEnd = (p >= pattern.length() || pattern.charAt(p) == '|');
				if (sEnd && pEnd) {
                    return true;
                }
				if (sEnd || pEnd) {
                    break;
                }
				if (pattern.charAt(p) == '?') {
                    continue;
                }
				if (pattern.charAt(p) == '*') {
					int i;
					++p;
					for (i = string.length(); i >= s; --i) {
                        if (match(pattern.substring(p), string.substring(i))) /*
                                                                                 * not quite right
                                                                                 */ {
                            return true;
                        }
                    }
					break;
				}
				if (pattern.charAt(p) != string.charAt(s)) {
                    break;
                }
			}
			p = pattern.indexOf('|', p);
			if (p == -1) {
                return false;
            }
		}
	}

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:45:46
	 * @param pattern 母鸡
	 * @param string 母鸡
	 * @return 母鸡
	 */
	protected int matchSpan(String pattern, String string) {
		int result = 0;
		StringTokenizer st = new StringTokenizer(pattern, "|");

		while (st.hasMoreTokens()) {
			int len = matchSpan1(st.nextToken(), string);
			if (len > result) {
                result = len;
            }
		}
		return result;
	}

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:46:04
	 * @param pattern 母鸡
	 * @param string 母鸡
	 * @return 母鸡
	 */
	protected int matchSpan1(String pattern, String string) {
		int p = 0;
		for (; p < string.length() && p < pattern.length(); p++) {
			if (pattern.charAt(p) == string.charAt(p)) {
                continue;
            }
			if (pattern.charAt(p) == '*') {
                return p - 1;
            }
			return 0;
		}
		return p < (pattern.length() - 1) ? -1 : p;
	}
}
