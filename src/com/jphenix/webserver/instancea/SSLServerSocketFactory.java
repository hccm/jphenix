/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.webserver.interfaceclass.IGlobalVar;
import com.jphenix.webserver.interfaceclass.IServeParameter;
import com.jphenix.webserver.interfaceclass.SocketFactory;

import javax.net.ssl.SSLServerSocket;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;


/**
 * SSLServerSocketFactory 无用
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2007-3-6下午01:35:04
 */
@ClassInfo({"2019-09-17 10:40","SSLServerSocketFactory"})
public class SSLServerSocketFactory implements SocketFactory {
	
    private Serve serve = null; //服务类
    
    static {
        initHandler();
    }
    
    /**
     * 构造函数
     * 2009-11-11上午10:37:42
     */
    public SSLServerSocketFactory(Serve serve) {
    	super();
    	this.serve = serve;
    }
    
    /**
     * 覆盖方法
     * @author 刘虻
     * 2009-12-29上午11:17:56
     */
    @Override
    public ServerSocket createSocket(IServeParameter serveParameter) throws IOException {
    	//文件路径
        String keystoreFile = serveParameter.getArgumentString(IGlobalVar.ARG_KEYSTOREFILE);
        if ("".equals(keystoreFile)) {
            keystoreFile = serveParameter.getServletBasePath()+"/conf/"+".keystore";
        }else {
        	keystoreFile = serve.getFilesUtil().getAllFilePath(keystoreFile);
        }
        System.setProperty("javax.net.ssl.keyStore", keystoreFile);
        //证书库密码
        String keystorePass = serveParameter.getArgumentString(IGlobalVar.ARG_KEYSTOREPASS);
        if (keystorePass.length()>0) {
        	System.setProperty("javax.net.ssl.keyStorePassword", keystorePass);
        }
        //获取监听端口
        int port = 
        	serveParameter.getArgumentInt(IGlobalVar.ART_SSL_PORT);
        if (port<1) {
        	port = IGlobalVar.DEF_SSLPORT;
        }
        //Socket backlog
        int backlog = 
        	serveParameter.getArgumentInt(IGlobalVar.ARG_BACKLOG);
        //绑定地址
        String ifAddress = 
        	serveParameter.getArgumentString(IGlobalVar.ARG_IFADDRESS);
        
        //获取默认端口工厂
        javax.net.ServerSocketFactory serverSocketFactory = javax.net.ssl.SSLServerSocketFactory.getDefault();
        SSLServerSocket secureSocket = null;
        try {
        	if(ifAddress.length()>0) {
            	secureSocket = 
            		(SSLServerSocket)serverSocketFactory
            			.createServerSocket(
            					port,backlog,InetAddress.getByName(ifAddress));
        	}else {
            	secureSocket = 
            		(SSLServerSocket)serverSocketFactory
            			.createServerSocket(port,backlog);
        	}
        } catch (Exception e) {
            e.printStackTrace();
			//构建错误信息
			StringBuffer errorMsg = new StringBuffer();
			
			errorMsg
				.append("\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx\n")
				.append("Create SSLSocket Port:[")
				.append(port)
				.append("] Backlog:[")
				.append(backlog)
				.append("] BindAddress:[")
				.append(ifAddress)
				.append("] Exception:[")
				.append(e)
				.append("]\n")
				.append("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n\n\n");
			System.err.print(errorMsg);
            throw new IOException(e.toString());
        }
        //设置匿名访问
        String[] cipherSuites = secureSocket.getSupportedCipherSuites();
        secureSocket.setEnabledCipherSuites(cipherSuites);
        // Set client authentication if necessary
        secureSocket.setNeedClientAuth(
        		serveParameter.getArgumentBoolean(IGlobalVar.ARG_CLIENTAUTH));
        return secureSocket;
    }

    /**
     * 母鸡
     * @author 刘虻
     * 2008-7-9上午07:37:58
     */
    protected static void initHandler() {
        String packages = System.getProperty(IGlobalVar.PROTOCOL_PACKAGES);
        if (packages == null) {
            packages = IGlobalVar.PROTOCOL_HANDLER;
        } else if (packages.indexOf(IGlobalVar.PROTOCOL_HANDLER) < 0) {
            packages += "|" + IGlobalVar.PROTOCOL_HANDLER;
        }
        System.setProperty(IGlobalVar.PROTOCOL_PACKAGES, packages);
    }
}
