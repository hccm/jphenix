/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 线程工厂 （内置WebServer专用）
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-8-13下午02:37:55
 */
@ClassInfo({"2019-09-17 10:40","线程工厂"})
public class ThreadFactory {

	private ThreadGroup threadGroup = null; //线程组
	
	/**
	 * 构造函数
	 * 2008-8-13下午02:37:55
	 */
	public ThreadFactory() {
		super();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-8-13下午02:37:55
	 */
	public Thread create(Runnable runnable) {
		//构建返回值
		Thread result = new Thread(getThreadGroup(), runnable);
		result.setDaemon(true);
		return result;
	}

	
	/**
	 * 获取线程组
	 * @author 刘虻
	 * 2008-8-13下午02:43:15
	 * @return 线程组
	 */
	public ThreadGroup getThreadGroup() {
		if (threadGroup==null) {
			threadGroup = new ThreadGroup("Phenix WebServer threads");
		}
		return threadGroup;
	}
	
	/**
	 * 设置线程组
	 * @author 刘虻
	 * 2008-8-13下午02:43:33
	 * @param threadGroup 线程组
	 */
	public void setThreadGroup(ThreadGroup threadGroup) {
		this.threadGroup = threadGroup;
	}
	
	/**
	 * 设置线程组
	 * @author 刘虻
	 * 2008-8-13下午02:44:11
	 * @param groupName 组名
	 */
	public void setThreadGroup(String groupName) {
		threadGroup = new ThreadGroup(groupName);
	}
}
