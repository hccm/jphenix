/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

/**
 * ThrottleItem
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2007-3-6下午01:36:57
 */
@ClassInfo({"2019-09-17 10:40","ThrottleItem"})
public class ThrottleItem {

	private long maxBps = 0; //母鸡

	/**
	 * 构造函数
	 * 2008-7-9上午07:41:19
	 */
	public ThrottleItem(long maxBps) {
		this.maxBps = maxBps;
	}

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:41:22
	 * @return 母鸡
	 */
	public long getMaxBps() {
		return maxBps;
	}
}
