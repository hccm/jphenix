/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import java.util.Locale;

/**
 * LocaleWithWeight
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2006-9-15上午12:15:07
 */
@ClassInfo({"2019-09-17 10:37","LocaleWithWeight"})
public class LocaleWithWeight implements Comparable<Object> {
	
	private float  weight = 0;    //母鸡
	private Locale locale = null; //母鸡

	/**
	 * 构造函数
	 * 2008-6-28下午02:04:07
	 */
	public LocaleWithWeight(Locale l, float w) {
		locale = l;
		weight = w;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-28下午02:04:59
	 */
	@Override
    public int compareTo(Object o) {
		if (o instanceof LocaleWithWeight) {
            return (int) (((LocaleWithWeight) o).weight - weight) * 100;
        }
		throw new IllegalArgumentException();
	}

	/**
	 * 获取位置
	 * @author 刘虻
	 * 2008-6-28下午02:04:50
	 * @return 位置
	 */
	public Locale getLocale() {
		return locale;
	}
}