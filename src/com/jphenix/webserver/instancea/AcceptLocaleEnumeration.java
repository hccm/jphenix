/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * 封装后的迭代器
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2006-9-15上午12:05:38
 */
@ClassInfo({"2019-09-17 10:35","封装后的迭代器"})
@SuppressWarnings("rawtypes")
public class AcceptLocaleEnumeration implements Enumeration {
	//迭代器
	private Iterator<LocaleWithWeight> iterator = null;

	/**
	 * 构造函数
	 * @param ts LocaleWithWeight
	 * 2006-9-15上午09:04:42
	 */
	public AcceptLocaleEnumeration(TreeSet<LocaleWithWeight> ts) {
		iterator = ts.iterator();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-9-15上午09:05:14
	 */
	@Override
    public boolean hasMoreElements() {
		return iterator.hasNext();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-9-15上午09:05:19
	 */
	@Override
    public Object nextElement() {
		return iterator.next().getLocale();
	}
}
