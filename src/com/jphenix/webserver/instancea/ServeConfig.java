/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * ServeConfig
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2006-9-15上午12:01:27
 */
@ClassInfo({"2019-09-17 10:38","ServeConfig"})
public class ServeConfig implements ServletConfig {

	private ServletContext           context;     //Servlet上下文
	private Hashtable<String,Object> initParams;  //初始化参数容器
	private String                   servletName; //Servlet名

	/**
	 * 构造函数
	 * 2008-7-8下午07:25:25
	 */
	public ServeConfig(ServletContext context) {
		this(context, null, "undefined");
	}

	/**
	 * 构造函数
	 * 2008-7-8下午07:25:33
	 */
	public ServeConfig(
			ServletContext context
			, Hashtable<String,Object> initParams
			, String servletName) {
		this.context = context;
		this.initParams = initParams;
		this.servletName = servletName;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-8下午07:25:39
	 */
	@Override
    public ServletContext getServletContext() {
		return context;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-8下午07:25:54
	 */
	@Override
    public String getInitParameter(String name) {
		if (initParams != null) {
            return (String) initParams.get(name);
        }
		return null;
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-8下午07:27:05
	 */
	@Override
    public Enumeration<String> getInitParameterNames() {
		if (initParams != null) {
			return initParams.keys();
		}
		return new Vector<String>().elements();
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-7-8下午11:02:05
	 */
	@Override
    public String getServletName() {
		return servletName;
	}
}