/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 池中的线程
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-8-13下午02:59:14
 */
@ClassInfo({"2019-09-17 10:38","池中的线程"})
public class PooledThread implements Runnable {

	private Thread        delegateThread;
	private Runnable      runner        = null;  //核心类
	private boolean       quit          = false; //是否退出
	private ThreadManager threadManager = null;  //线程管理类
	
	/**
	 * 构造函数
	 * 2008-8-13下午02:59:14
	 */
	public PooledThread() {
		super();
	}
	
	/**
	 * 获取线程管理类
	 * @author 刘虻
	 * 2008-8-13下午03:03:13
	 * @return 线程管理类
	 */
	public ThreadManager getThreadManager() {
		return threadManager;
	}
	
	/**
	 * 设置线程管理类
	 * @author 刘虻
	 * 2008-8-13下午03:02:44
	 * @param threadManager 线程管理类
	 */
	public void setThreadManager(ThreadManager threadManager) {
		this.threadManager = threadManager;
	}
	
	/**
	 * 执行初始化
	 * @author 刘虻
	 * 2008-8-13下午03:26:55
	 */
	public void init() {
		delegateThread = getThreadManager().getThreadFactory().create(this);
		delegateThread.setName(ThreadManager.ID + "-PooledThread: CREATED");
		delegateThread.start();
	}

	/**
	 * 设置名称
	 * @author 刘虻
	 * 2008-8-13下午03:03:36
	 * @param name 名称
	 */
	public void setName(String name) {
		delegateThread.setName(name);
	}

	/**
	 * 是否存活中
	 * @author 刘虻
	 * 2008-8-13下午03:03:44
	 * @return 是否存活中
	 */
	public boolean isAlive() {
		return delegateThread.isAlive();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-8-13下午03:10:02
	 */
	@Override
    public synchronized void run() {
		do {
			if (runner == null) {
                try {
                    this.wait();
                } catch (InterruptedException ie) {

                }
            }
			if (runner != null) {
				try {
					runner.run();
					runner = null;
				} catch (Throwable t) {
					t.printStackTrace();
				}
				int activeThreads = 0;
				synchronized (getThreadManager().getBusyThreadMap()) {
					getThreadManager().getBusyThreadMap().remove(this);
					activeThreads = getThreadManager().getBusyThreadMap().size();
				}
				synchronized (getThreadManager().getFreeThreadList()) {
					if (getThreadManager()
								.getFreeThreadList().size()+activeThreads>getThreadManager().getMaxThreadCount()) {
						break; // discard this thread
					}
					getThreadManager().getFreeThreadList().add(this);
					delegateThread.setName(ThreadManager.ID + "-PooledThread: FREE");
					getThreadManager().getFreeThreadList().notify();
				}
			}
		} while (!quit);
	}

	/**
	 * 中断执行
	 * @author 刘虻
	 * 2008-8-13下午03:10:11
	 */
	public synchronized void interrupt() {
		quit = true;
		delegateThread.interrupt();
	}

	/**
	 * 设置核心类
	 * @author 刘虻
	 * 2008-8-13下午03:10:45
	 * @param runnable 核心类
	 */
	protected synchronized void setRunner(Runnable runnable) {
		if (runner != null) {
			//虽然抛了异常，但在前台页面显示却是正常，所以取消该异常
			//throw new RuntimeException("Invalid worker thread state, current runner not null.");
			return;
		}
		runner = runnable;
		this.notifyAll();
	}

}
