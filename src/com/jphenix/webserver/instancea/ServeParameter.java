/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.driver.nodehandler.FNodeHandler;
import com.jphenix.driver.propertie.FConfProp;
import com.jphenix.driver.propertie.instanceb.XmlConfProp;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.share.lang.SInteger;
import com.jphenix.share.lang.SString;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.exceptions.EmptyException;
import com.jphenix.standard.propertie.IConfigSetter;
import com.jphenix.standard.viewhandler.INodeHandler;
import com.jphenix.standard.viewhandler.IViewHandler;
import com.jphenix.webserver.interfaceclass.IGlobalVar;
import com.jphenix.webserver.interfaceclass.IServeParameter;
import com.jphenix.webserver.servlet.ServletInfoBean;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务参数处理类
 * 
 * 2018-11-08 在加载配置文件前，先判断了文件是否存在
 * 2019-09-17 整理了代码格式
 * 2020-08-06 去掉了配置文件接口，改为直接使用实体类
 * 2021-09-12 增加了配置文件格式说明
 * 
 * 
 * phserver.xml 内容：
 * 
 * <?xml version="1.0" encoding='UTF-8'?>
 * <root>
 *   <!-- Web Server For Phenix v3.0 -->
 *   <enabled-ssl></enabled-ssl>
 *   <sslport></sslport>                  <!-- SSL监听端口 -->
 *   <keystorePass>chenxin</keystorePass> <!-- 证书密码，如果需要的话 -->
 *   <port>80</port>                      <!-- 常规监听端口 -->
 *   <context-path>/</context-path>       <!-- 虚拟路径 -->
 *   <backlog>0</backlog>                 <!-- Socket Backlog -->
 *   <session-timeout></session-timeout>  <!-- 会话超时时间(分钟) -->
 *   <notnohup>yes</notnohup>             <!-- 是否在控制窗口控制网站 yes(可以在控制台终止服务) no不支持 -->
 *   <out-serv-log>false</out-serv-log>   <!-- 是否输入普通服务器日志 -->
 *   <keep-alive>true</keep-alive>        <!-- 浏览器与服务器是否进行持久连接 -->
 *   <WebBasePath>../..</WebBasePath>     <!-- 网站根路径 -->
 *   <max-execute_thread_count>50</max-execute_thread_count>     <!-- Servlet最大处理线程数 -->
 *   <after-start-execute-command></after-start-execute-command> <!-- 网站启动后执行命令行 -->
 * </root>
 * 
 * 
 * @author 刘虻
 * 2007-9-29下午03:57:14
 */
@ClassInfo({"2021-09-12 18:51","服务参数处理类"})
public class ServeParameter implements IServeParameter,IConfigSetter {

    private String webBasePath     = null; //网站根路径
    private String servletBasePath = null; //服务程序根路径
                        
    private List<String>           mimeKeyList              = null; //mime主键序列
    private List<ServletInfoBean>  servletInfoList          = null; //Servlet信息序列
    private List<String>           servletNameList          = null; //Servlet名称序列
    private List<String>           sessionListenerClassList = null; //会话监听信息类路径序列
    private List<ServletInfoBean>  filterInfoList           = null; //过滤器信息序列
    private List<String>           filterNameList           = null; //过滤器名称序列
    private List<String>           welcomeFileList          = null; //默认访问文件序列
    
    private Serve                  serve                    = null; //服务类
    private XmlConfProp            confProp                 = null; //配置文件处理类
    private Map<String,String>     mimeMap                  = null; //mime容器
    private Map<String,String>     confDataMap              = new HashMap<>(); //需要设置到配置文件中的参数容器
    
    /**
     * 构造函数
     * 2007-9-29下午03:57:14
     */
    public ServeParameter(Serve serve) {
        super();
        this.serve = serve;
    }
    
    
    /**
     * 获取有效的配置文件处理类
     * @author 刘虻
     * 2008-6-28下午02:31:43
     * @return 配置文件处理类
     */
    public XmlConfProp getConfProp() {
        return confProp;
    }
    

    /**
     * 获取参数容器
     * @author 刘虻
     * 2007-9-30下午03:45:53
     * @return 参数容器
     */
    @Override
    public Map<String,Object> getArgumentMap() {
        return getConfProp().getParameterMap();
    }
    
    
    /**
     * 覆盖方法
     * 刘虻
     * 2012-9-13 下午10:05:09
     */
    @Override
    public String getConfigFilePath() {
    	//获取加载的配置文件序列
    	List<String> pathList = confProp.getConfigPathList();
    	StringBuffer reSbf = new StringBuffer();
    	for(String path:pathList) {
    		reSbf.append(path).append(File.separator);
    	}
        return reSbf.toString();
    }


    /**
     * 显示帮助信息
     * 2017年1月31日
     * @author MBG
     */
    protected void showHelp() {
    	System.out.println("Nobody ever saw this. See the source code Please.");
    }
    
    
    
    /**
     * 整理启动服务参数
     * 
     * 当第一个参数为 1 时，为直传参数模式
     * 在该模式下，第二个参数为监听端口
     * 第三个传入参数为虚拟路径
     * 直传参数模式只支持者两个参数
     * 
     * @author 刘虻
     * 2006-9-15下午12:21:24
     * @param args 导入参数
     * @return 整理后的参数包容器
     * @throws Exception 整理参数时发生异常
     */
    @Override
    public void initProp(
            String[] args) throws Exception  {
        confProp = FConfProp.newInstance(); //构建配置文件处理类
        //获取当前包所在的文件夹
        String localPath = PathUtil.getThisPath(null);
        //构建配置文件对象
        File confFile = new File(localPath+IGlobalVar.CONFIG_TEXT_FILE_NAME);
        if(confFile.exists()) {
            //先加载包同级下的基础配置文件
            try {
                System.out.println("Begin Try Load Base Config File:["+localPath+IGlobalVar.CONFIG_TEXT_FILE_NAME+"]");
                //基础配置文件
                XmlConfProp baseProp = FConfProp.newInstance();
                baseProp.setFilePath(confFile.getPath());
                confProp.addChild("base",baseProp);
            }catch(Exception e) {
                System.out.println("The Base Config File Not Loaded. Maybe Not Found");
            }
        }
        if(args!=null && args.length==1 && "?".equals(args[0])) {
        	//显示帮助信息
        	showHelp();
        	throw new EmptyException();
        }
        if(args!=null && args.length>0 && "1".equals(args[0])) {
        	/*
        	 * 当第一个参数为 1 时，为直传参数模式
        	 * 在该模式下，第二个参数为监听端口
        	 * 第三个传入参数为虚拟路径
        	 * 直传参数模式只支持者两个参数
        	 */
        	String port = "";
        	if(args.length>1) {
        		port = args[1].trim();
        	}
        	if(port.length()<1) {
        		port = "80";
        	}
        	String contextPath = ""; //设置默认虚拟路径
        	if(args.length>2) {
        		contextPath = args[2].trim();
        	}
        	if(contextPath.length()<1) {
        		contextPath = "/";
        	}
        	System.out.println("\n==============================================================================================================\n Load Parameters[Port:"+port+" ContextPath:"+contextPath+"]\n==============================================================================================================");
        	confDataMap.put("port",port);
        	confDataMap.put("context-path",contextPath);
        	confDataMap.put("backlog","0");
        	confDataMap.put("notnohup","yes");
        	confDataMap.put("keep-alive","true");
        	confDataMap.put("max-execute_thread_count","50");
        	confDataMap.put("WebBasePath","../..");
        	confProp.registerSetter(this);
        }else {
            //目标配置文件路径
            String configPath = null;
            if (args!=null && args.length>0 && args[0]!=null && args[0].length()>0) {
                configPath = args[0];
            }else {
                //目标配置文件路径
                configPath = PathUtil.fixUpPath(localPath+"../resfiles/phserver.xml");
            }
            //配置文件对象
            File checkFile = new File(configPath); 
            if(checkFile.exists() && checkFile.isFile()) {
            	System.out.println("Begin Try Load Config File:["+configPath+"]");
            	confProp.setFilePath(configPath); //设置配置文件路径
            }else{
            	System.out.println("\n==============================================================================================================\nThe Config File Not Loaded. Maybe Not Found [WEB-INF/resfiles/phserver.xml] LoadDefault[Port:80 ContextPath:/]\n==============================================================================================================");
            	confDataMap.put("port","80");
            	confDataMap.put("context-path","/");
            	confDataMap.put("backlog","0");
            	confDataMap.put("notnohup","yes");
            	confDataMap.put("keep-alive","true");
            	confDataMap.put("max-execute_thread_count","50");
            	confDataMap.put("WebBasePath","../..");
            	confProp.registerSetter(this);
            }
        }
        //获取网站根路径
        webBasePath = 
            SString.valueOf(
                    confProp.getParameter(IGlobalVar.WEB_BASE_PATH));
        if(webBasePath.length()<1) {
            webBasePath = "/../../..";
        }
        webBasePath = SFilesUtil.getAllFilePath(webBasePath,SFilesUtil.getBaseClassPath((Class<?>)null));
        //获取服务类根路径
        servletBasePath = webBasePath+"/WEB-INF/classes";
        servletBasePath = serve.getFilesUtil().getAllFilePath(servletBasePath);

        //设置类根路径
        serve.getFilesUtil().setWebInfPath(webBasePath+"/WEB-INF"); 
        
        //初始化网站
        
        //解析WEB-INF/web.xml
        
        INodeHandler xml = FNodeHandler.newNodeHandler();
        xml.setXmlStyle(true);
        
        File cFile; //检测文件是否存在
        
        sessionListenerClassList = new ArrayList<String>(); //监听类路径序列
        filterInfoList = new ArrayList<ServletInfoBean>(); //过滤器信息序列
        filterNameList = new ArrayList<String>(); //过滤器名称序列
        servletInfoList = new ArrayList<ServletInfoBean>(); //Servlet信息序列
        servletNameList = new ArrayList<String>(); //Servlet名称序列
        
        /*
         * 先在包根路径中查找基本配置文件
         */

        String baseWebFilePath = PathUtil.fixPath(localPath+"/web.xml");
        cFile = new File(baseWebFilePath);
        if(cFile.exists()) {
            //设置基础配置文件路径
            xml.setStream(new FileInputStream(new File(baseWebFilePath)),baseWebFilePath);
            fixSessionListenerInfo(xml); //处理监听器序列
            fixFilterInfo(xml); //处理过滤器信息
            fixServletInfo(xml); //处理Servlet信息
            fixWelcomeFileList(xml);//处理默认访问文件信息
            fixMimeType(xml); //处理类型信息
        }
        
        /*
         * 再加载指定项目下的配置文件
         */
        //web.xml文件路径
        String xmlFilePath = 
            PathUtil.fixUpPath(servletBasePath+"/../web.xml");
        cFile = new File(xmlFilePath);
        if (cFile.exists()) {
            xml = FNodeHandler.newNodeHandler();
            xml.setXmlStyle(true);
            //设置基础配置文件路径
            xml.setStream(new FileInputStream(new File(xmlFilePath)),xmlFilePath);
            fixSessionListenerInfo(xml); //处理监听器序列
            fixFilterInfo(xml); //处理过滤器信息
            fixServletInfo(xml); //处理Servlet信息
            fixWelcomeFileList(xml);//处理默认访问文件信息
            fixMimeType(xml); //处理类型信息
        }
        
        addDefaultServlet(); //加载默认Servlet
    }
    
    

    /**
     * 覆盖方法
     * @author 刘虻
     * 2007-9-30下午03:48:38
     */
    @Override
    public String getArgumentString(String key) {
        return SString.valueOf(getConfProp().getParameter(key));
    }


    /**
     * 覆盖方法
     * @author 刘虻
     * 2007-9-30下午04:00:00
     */
    @Override
    public boolean getArgumentBoolean(String key) {
        return SBoolean.valueOf(getArgumentString(key));
    }


    /**
     * 覆盖方法
     * @author 刘虻
     * 2007-9-30下午04:02:18
     */
    @Override
    public int getArgumentInt(String key) {
        return SInteger.valueOf(getArgumentString(key));
    }


    /**
     * 覆盖方法
     * @author 刘虻
     * 2008-6-28下午03:27:30
     */
    @Override
    public String getRealPath(String path) {
        if (path==null) {
            return getWebBasePath();
        }
        if (!path.startsWith("/")) {
            path = "/"+path;
        }
        return serve.getFilesUtil().getAllFilePath(getWebBasePath()+path);
    }

    /**
     * 覆盖方法
     * @author 刘虻
     * 2008-6-28下午03:27:27
     */
    @Override
    public String getServletBasePath() {
        return servletBasePath;
    }

    /**
     * 覆盖方法
     * @author 刘虻
     * 2008-6-28下午03:27:10
     */
    @Override
    public String getWebBasePath() {
        return webBasePath;
    }
    
    /**
     * 获取Mime主键序列
     * @author 刘虻
     * 2008-6-28下午07:49:40
     * @return Mime主键序列
     */
    protected List<String> getMimeKeyList() {
        if (mimeKeyList==null) {
            getMimeMap();
        }
        return mimeKeyList;
    }
    
    
    /**
     * 处理配置文件中的类型信息
     * @author 刘虻
     * 2009-12-11下午02:55:12
     * @param xml 配置文件对象
     */
    protected void fixMimeType(IViewHandler xml) {
        //获取类型映射
        List<IViewHandler> xmls = xml.getChildNodesByNodeName("mime-mapping");
        //获取类型序列
        List<String> mKeyList = getMimeKeyList();
        //获取类型对照容器
        Map<String,String> mMap = getMimeMap();
        String extension; //类型主键
        String mimeType; //类型值
        IViewHandler node; //获取类型主键信息节点
        for(IViewHandler cXml:xmls) {
            extension = null; //类型主键
            mimeType = null; //类型值
            node = cXml.getFirstChildNodeByNodeName("extension");
            if(!node.isEmpty()) {
                extension = node.nt();
            }
            node = cXml.getFirstChildNodeByNodeName("mime-type");
            if(!node.isEmpty()) {
                mimeType = node.nt();
            }
            if(extension==null 
                    || extension.length()<1 
                    || mimeType==null 
                    || mimeType.length()<1) {
                continue;
            }
            extension = "."+extension.toUpperCase();
            mMap.put(extension,mimeType);
            if(!mKeyList.contains(extension)) {
                mKeyList.add(extension);
            }
        }
    
    }
    
    /**
     * 获取有效的类型对照容器
     * @author 刘虻
     * 2009-12-11下午02:47:26
     * @return 有效的类型对照容器
     */
    protected Map<String,String> getMimeMap() {
        if (mimeMap==null) {
            mimeKeyList = new ArrayList<String>();
            mimeMap = new HashMap<String,String>();
            
            mimeKeyList.add(".HTML");
            mimeMap.put(".HTML","text/html");
            
            mimeKeyList.add(".HTM");
            mimeMap.put(".HTM","text/html");
            
            mimeKeyList.add(".TXT");
            mimeMap.put(".TXT","text/plain");
            
            mimeKeyList.add(".XML");
            mimeMap.put(".XML","text/xml");
            
            mimeKeyList.add(".CSS");
            mimeMap.put(".CSS","text/css");
            
            mimeKeyList.add(".SGML");
            mimeMap.put(".SGML","text/x-sgml");
            
            mimeKeyList.add(".SGM");
            mimeMap.put(".SGM","text/x-sgml");
            
            mimeKeyList.add(".GIF");
            mimeMap.put(".GIF","image/gif");
            
            mimeKeyList.add(".JPG");
            mimeMap.put(".JPG","image/jpeg");
            
            mimeKeyList.add(".JPEG");
            mimeMap.put(".JPEG","image/jpeg");
            
            mimeKeyList.add(".JPE");
            mimeMap.put(".JPE","image/jpeg");
            
            mimeKeyList.add(".PNG");
            mimeMap.put(".PNG","image/png");
            
            mimeKeyList.add(".BMP");
            mimeMap.put(".BMP","image/bmp");
            
            mimeKeyList.add(".TIF");
            mimeMap.put(".TIF","image/bmp");
            
            mimeKeyList.add(".TIFF");
            mimeMap.put(".TIFF","image/bmp");
            
            mimeKeyList.add(".RGB");
            mimeMap.put(".RGB","image/x-rgb");
            
            mimeKeyList.add(".XPM");
            mimeMap.put(".XPM","image/x-xpixmap");
            
            mimeKeyList.add(".XBM");
            mimeMap.put(".XBM","image/x-xbitmap");
            
            mimeKeyList.add(".SVG");
            mimeMap.put(".SVG","image/svg-xml ");
            
            mimeKeyList.add(".SVGZ");
            mimeMap.put(".SVGZ","image/svg-xml ");
            
            mimeKeyList.add(".AU");
            mimeMap.put(".AU","audio/basic");
            
            mimeKeyList.add(".SND");
            mimeMap.put(".SND","audio/basic");
            
            mimeKeyList.add(".MID");
            mimeMap.put(".MID","audio/mid");
            
            mimeKeyList.add(".MIDI");
            mimeMap.put(".MIDI","audio/mid");
            
            mimeKeyList.add(".RMI");
            mimeMap.put(".RMI","audio/mid");
            
            mimeKeyList.add(".KAR");
            mimeMap.put(".KAR","audio/mid");
            
            mimeKeyList.add(".MPGA");
            mimeMap.put(".MPGA","audio/mpeg");
            
            mimeKeyList.add(".MP2");
            mimeMap.put(".MP2","audio/mpeg");
            
            mimeKeyList.add(".MP3");
            mimeMap.put(".MP3","audio/mpeg");
            
            mimeKeyList.add(".WAV");
            mimeMap.put(".WAV","audio/wav");
            
            mimeKeyList.add(".AIFF");
            mimeMap.put(".WAV","audio/aiff");
            
            mimeKeyList.add(".AIFC");
            mimeMap.put(".AIFC","audio/aiff");
            
            mimeKeyList.add(".AIF");
            mimeMap.put(".AIF","audio/x-aiff");
            
            mimeKeyList.add(".RA");
            mimeMap.put(".RA","audio/x-realaudio");
            
            mimeKeyList.add(".RPM");
            mimeMap.put(".RPM","audio/x-pn-realaudio-plugin");
            
            mimeKeyList.add(".RAM");
            mimeMap.put(".RAM","audio/x-pn-realaudio");
            
            mimeKeyList.add(".SD2");
            mimeMap.put(".SD2","audio/x-sd2");
            
            mimeKeyList.add(".BIN");
            mimeMap.put(".BIN","application/octet-stream");
            
            mimeKeyList.add(".DMS");
            mimeMap.put(".DMS","application/octet-stream");
            
            mimeKeyList.add(".LHA");
            mimeMap.put(".LHA","application/octet-stream");
            
            mimeKeyList.add(".LZH");
            mimeMap.put(".LZH","application/octet-stream");
            
            mimeKeyList.add(".EXE");
            mimeMap.put(".EXE","application/octet-stream");
            
            mimeKeyList.add(".DLL");
            mimeMap.put(".DLL","application/octet-stream");
            
            mimeKeyList.add(".CLASS");
            mimeMap.put(".CLASS","application/octet-stream");
            
            mimeKeyList.add(".HQX");
            mimeMap.put(".HQX","application/mac-binhex40");
            
            mimeKeyList.add(".PS");
            mimeMap.put(".PS","application/postscript");
            
            mimeKeyList.add(".AI");
            mimeMap.put(".AI","application/postscript");
            
            mimeKeyList.add(".EPS");
            mimeMap.put(".EPS","application/postscript");
            
            mimeKeyList.add(".PDF");
            mimeMap.put(".PDF","application/pdf");
            
            mimeKeyList.add(".RTF");
            mimeMap.put(".RTF","application/rtf");
            
            mimeKeyList.add(".DOC");
            mimeMap.put(".DOC","application/msword");
            
            mimeKeyList.add(".PPT");
            mimeMap.put(".PPT","application/powerpoint");
            
            mimeKeyList.add(".FIF");
            mimeMap.put(".FIF","application/fractals");
            
            mimeKeyList.add(".P7C");
            mimeMap.put(".P7C","application/pkcs7-mime");
            
            mimeKeyList.add(".JS");
            mimeMap.put(".JS","application/x-javascript");
            
            mimeKeyList.add(".Z");
            mimeMap.put(".Z","application/x-compress");
            
            mimeKeyList.add(".GZ");
            mimeMap.put(".GZ","application/x-gzip");
            
            mimeKeyList.add(".TAR");
            mimeMap.put(".TAR","application/x-tar");
            
            mimeKeyList.add(".TGZ");
            mimeMap.put(".TGZ","application/x-compressed");
            
            mimeKeyList.add(".ZIP");
            mimeMap.put(".ZIP","application/x-zip-compressed");
            
            mimeKeyList.add(".DIR");
            mimeMap.put(".DIR","application/x-director");
            
            mimeKeyList.add(".DCR");
            mimeMap.put(".DIR","application/x-director");
            
            mimeKeyList.add(".DXR");
            mimeMap.put(".DXR","application/x-director");
            
            mimeKeyList.add(".DVI");
            mimeMap.put(".DVI","application/x-dvi");
            
            mimeKeyList.add(".TEX");
            mimeMap.put(".TEX","application/x-tex");
            
            mimeKeyList.add(".LATEX");
            mimeMap.put(".LATEX","application/x-latex");
            
            mimeKeyList.add(".TCL");
            mimeMap.put(".TCL","application/x-tcl");
            
            mimeKeyList.add(".CER");
            mimeMap.put(".CER", "application/x-x509-ca-cert");
            
            mimeKeyList.add(".CRT");
            mimeMap.put(".CRT", "application/x-x509-ca-cert");
            
            mimeKeyList.add(".DER");
            mimeMap.put(".DER", "application/x-x509-ca-cert");
            
            mimeKeyList.add(".MPG");
            mimeMap.put(".MPG", "video/mpeg");
            
            mimeKeyList.add(".MPE");
            mimeMap.put(".MPE", "video/mpeg");
            
            mimeKeyList.add(".MPEG");
            mimeMap.put(".MPEG", "video/mpeg");
            
            mimeKeyList.add(".QT");
            mimeMap.put(".QT", "video/quicktime");
            
            mimeKeyList.add(".MOV");
            mimeMap.put(".MOV", "video/quicktime");
            
            mimeKeyList.add(".AVI");
            mimeMap.put(".AVI", "video/x-msvideo");
            
            mimeKeyList.add(".MOVIE");
            mimeMap.put(".MOVIE", "video/x-sgi-movie");
            
            mimeKeyList.add(".PDB");
            mimeMap.put(".PDB", "chemical/x-pdb");
            
            mimeKeyList.add(".XYZ");
            mimeMap.put(".XYZ", "chemical/x-pdb");
            
            mimeKeyList.add(".ICE");
            mimeMap.put(".ICE", "x-conference/x-cooltalk");
            
            mimeKeyList.add(".JNLP");
            mimeMap.put(".JNLP", "application/x-java-jnlp-file");
            
            mimeKeyList.add(".WRL");
            mimeMap.put(".WRL", "x-world/x-vrml");
            
            mimeKeyList.add(".VRML");
            mimeMap.put(".VRML", "x-world/x-vrml");
            
            mimeKeyList.add(".WML");
            mimeMap.put(".WML", "text/vnd.wap.wml");
            
            mimeKeyList.add(".WMLC");
            mimeMap.put(".WMLC", "application/vnd.wap.wmlc");
            
            mimeKeyList.add(".WMLS");
            mimeMap.put(".WMLS", "text/vnd.wap.wmlscript");
            
            mimeKeyList.add(".WMLSC");
            mimeMap.put(".WMLSC", "application/vnd.wap.wmlscriptc");
            
            mimeKeyList.add(".WBMP");
            mimeMap.put(".WBMP", "image/vnd.wap.wbmp");
        }
        return mimeMap;
    }
    
    
    /**
     * 获取Mime类型
     * @author 刘虻
     * 2008-6-28下午07:44:23
     * @param file 文件路径
     * @return 值
     */
    @Override
    public String getMimeType(String file) {
        if (file==null) {
            return null;
        }
        file = file.toUpperCase(); //转换为大写
        int size = getMimeKeyList().size(); //获取类型总数
        for(int i=0;i<size;i++) {
            //获取元素
            String element = SString.valueOf(getMimeKeyList().get(i));
            if (file.endsWith(element)) {
                return SString.valueOf(getMimeMap().get(element));
            }
        }
        return null;
    }
    
    
    /**
     * 获取Servlet信息序列
     * @author 刘虻
     * 2008-6-28下午08:49:22
     * @return Servlet信息序列
     */
    @Override
    public List<ServletInfoBean> getServletInfoList() {
        if (servletInfoList==null) {
            servletInfoList = new ArrayList<ServletInfoBean>();
        }
        return servletInfoList;
    }
    
    /**
     * 获取默认访问文件序列
     * @author 刘虻
     * 2009-7-22下午03:18:08
     * @return 默认访问文件序列
     */
    @Override
    public List<String> getWelcomeFileList() {
        if (welcomeFileList==null) {
            welcomeFileList = new ArrayList<String>();
        }
        return welcomeFileList;
    }
    
    
    /**
     * 处理会话监听器信息
     * @author 刘虻
     * 2008-6-29下午04:02:36
     * @param xml 配置文件
     */
    protected void fixSessionListenerInfo(IViewHandler xml) {
        //获取会话监听器段
        IViewHandler xmlSub = 
            xml.getFirstChildNodeByNodeName("listener");
        if (xmlSub!=null) {
            //获取会话监听器类路径序列段
            List<IViewHandler> xmlSubs = 
                xmlSub.getChildNodesByNodeName("listener-class");
            for(IViewHandler ele:xmlSubs) {
                //获取类路径 
                String value = ele.nt();
                if (value==null 
                        || value.length()<1 
                        || sessionListenerClassList.contains(value)) {
                    continue;
                }
                sessionListenerClassList.add(value);
            }
        }
    }
    
    
    /**
     * 从配置文件中获取过滤器信息
     * @author 刘虻
     * 2008-6-29下午04:00:49
     * @param xml 过滤器信息
     */
    protected void fixFilterInfo(IViewHandler xml) {
        //过滤器信息容器
        HashMap<String,ServletInfoBean> filterInfoMap = new HashMap<String,ServletInfoBean>();
        //获取Servlet信息段数组
        List<IViewHandler> serverXmls = xml.getChildNodesByNodeName("filter");
        IViewHandler xmlSub = null; //xml段
        //参数段数组
        List<IViewHandler> paraXmls = null;
        //处理映射
        List<IViewHandler> mappingXmls = null;
        for(IViewHandler serverXml:serverXmls) {
            //构建新的Servlet信息类
            ServletInfoBean sib = new ServletInfoBean();
            //获取filter-name段
            xmlSub = serverXml.getFirstChildNodeByNodeName("filter-name");
            if(!xmlSub.isEmpty()) {
                sib.setName(xmlSub.nt());
            }
            //获取filter-class段
            xmlSub = serverXml.getFirstChildNodeByNodeName("filter-class");
            if(!xmlSub.isEmpty()) {
                sib.setClassPath(xmlSub.nt());
            }
            //获取load-on-startup段
            xmlSub = serverXml.getFirstChildNodeByNodeName("load-on-startup");
            if(!xmlSub.isEmpty()) {
                sib.setLoadOnStartUp(SInteger.valueOf(xmlSub.nt()));
            }
            //参数段数组
            paraXmls = serverXml.getChildNodesByNodeName("init-param");
            for(IViewHandler paraXml:paraXmls) {
                //获取参数主键段
                xmlSub = paraXml.getFirstChildNodeByNodeName("param-name");
                if (xmlSub.isEmpty()) {
                    continue;
                }
                String key = xmlSub.nt(); //获取参数主键
                if(key.length()<1) {
                    continue;
                }
                //获取参数值段
                xmlSub = paraXml.getFirstChildNodeByNodeName("param-value");
                if (xmlSub.isEmpty()) {
                    continue;
                }
                String value = xmlSub.nt(); //参数值
                if (value.length()<1) {
                    continue;
                }
                sib.putParaInfo(key,value);
            }
            if(filterNameList.contains(sib.getName())) {
                //覆盖值
                filterInfoList.set(filterNameList.indexOf(sib.getName()),sib);
                filterInfoMap.put(sib.getName(),sib); //放入容器
            }else {
                filterNameList.add(sib.getName());
                filterInfoList.add(sib); //放入序列
                filterInfoMap.put(sib.getName(),sib); //放入容器
            }
        }
        //处理映射
        mappingXmls = xml.getChildNodesByNodeName("filter-mapping");
        List<IViewHandler> xmlSubs = null; //获取映射数组
        for(IViewHandler mappingXml:mappingXmls) {
            //获取过滤去名字段
            xmlSub = mappingXml.getFirstChildNodeByNodeName("filter-name");
            if (xmlSub.isEmpty()) {
                continue;
            }
            //通过名字获取指定的过滤器
            ServletInfoBean sib =
                    filterInfoMap.get(xmlSub.nt());
            if (sib==null) {
                continue;
            }
            //获取映射数组
            xmlSubs = mappingXml.getChildNodesByNodeName("url-pattern");
            for(IViewHandler ele:xmlSubs) {
                //获取映射值
                String value = ele.nt();
                if (value.length()<1) {
                    continue;
                }
                sib.getMappingList().add(value);
            }
        }
    }
    
    /**
     * 处理默认文件配置序列 
     * welcome-file-list
     * @author 刘虻
     * 2009-7-22下午03:13:25
     * @param xml 配置文件对象
     */
    protected void fixWelcomeFileList(IViewHandler xml) {
        welcomeFileList = new ArrayList<String>();
        //获取默认文件序列XML信息
        List<IViewHandler> cXmls = 
            xml.getFirstChildNodeByNodeName("welcome-file-list")
                            .getChildNodesByNodeName("welcome-file");
        String fileName; //获取文件元素
        for(IViewHandler cXml:cXmls) {
            fileName = cXml.nt();
            if(fileName.length()<1) {
                continue;
            }
            welcomeFileList.add(fileName);
        }
    }
    
    /**
     * 处理Servlet信息
     * @author 刘虻
     * 2008-6-28下午08:51:11
     * @param xml xml对象
     */
    protected void fixServletInfo(IViewHandler xml) {
        //Servlet信息容器
        HashMap<String,ServletInfoBean> servletInfoMap = new HashMap<String,ServletInfoBean>();
        //获取Servlet信息段数组
        List<IViewHandler> serverXmls = xml.getChildNodesByNodeName("servlet");
        ServletInfoBean sib; //Servlet 信息类
        IViewHandler xmlSub; //xml段
        List<IViewHandler> paraXmls; //参数段数组
        List<IViewHandler> mappingXmls; //处理映射
        List<IViewHandler> xmlSubs; //获取映射数组
        for(IViewHandler serverXml:serverXmls) {
            //构建新的Servlet信息类
            sib = new ServletInfoBean();
            //获取servlet-name段
            xmlSub = serverXml.getFirstChildNodeByNodeName("servlet-name");
            if (!xmlSub.isEmpty()) {
                sib.setName(xmlSub.nt());
            }
            //获取servlet-class段
            xmlSub = serverXml.getFirstChildNodeByNodeName("servlet-class");
            if (!xmlSub.isEmpty()) {
                sib.setClassPath(xmlSub.nt());
            }
            //获取load-on-startup段
            xmlSub = serverXml.getFirstChildNodeByNodeName("load-on-startup");
            if (!xmlSub.isEmpty()) {
                sib.setLoadOnStartUp(SInteger.valueOf(xmlSub.nt()));
            }
            //处理加载参数
            
            //参数段数组
            paraXmls = serverXml.getChildNodesByNodeName("init-param");
            String key; //参数主键
            String value; //参数值
            for(IViewHandler paraXml:paraXmls) {
                //获取参数主键段
                xmlSub = paraXml.getFirstChildNodeByNodeName("param-name");
                if (xmlSub.isEmpty()) {
                    continue;
                }
                key = xmlSub.nt(); //获取参数主键
                if (key.length()<1) {
                    continue;
                }
                //获取参数值段
                xmlSub = paraXml.getFirstChildNodeByNodeName("param-value");
                if (xmlSub.isEmpty()) {
                    continue;
                }
                value = xmlSub.nt(); //参数值
                if (value.length()<1) {
                    continue;
                }
                sib.putParaInfo(key,value);
            }
            if(servletNameList.contains(sib.getName())) {
                servletInfoList.set(servletNameList.indexOf(sib.getName()),sib);
                servletInfoMap.put(sib.getName(),sib); //放入容器
            }else {
                servletNameList.add(sib.getName());
                servletInfoList.add(sib); //放入序列
                servletInfoMap.put(sib.getName(),sib); //放入容器
            }
        }
    
        //处理映射
        mappingXmls = xml.getChildNodesByNodeName("servlet-mapping");
        for(IViewHandler mappingXml:mappingXmls) {
            //获取过滤去名字段
            xmlSub = mappingXml.getFirstChildNodeByNodeName("servlet-name");
            if (xmlSub.isEmpty()) {
                continue;
            }
            sib = servletInfoMap.get(xmlSub.nt());
            if (sib==null) {
                continue;
            }
            //获取映射数组
            xmlSubs = mappingXml.getChildNodesByNodeName("url-pattern");
            String value;
            for(IViewHandler ele:xmlSubs) {
                //获取映射值
                value = ele.nt();
                if (value.length()<1) {
                    continue;
                }
                sib.getMappingList().add(value);
            }
        }
    
    }
    
    
    /**
     * 加载默认Servlet
     * 刘虻
     * 2010-10-29 下午03:06:05
     */
    @SuppressWarnings("unlikely-arg-type")
	protected void addDefaultServlet() {
        if(!getServletInfoList().contains("jsp")) {
            //添加默认解析jsp
            //构建Servlet信息类
            ServletInfoBean sib = new ServletInfoBean();
            sib.setClassPath("com.jphenix.webserver.servlet.jsp.JSPServlet");
            sib.setLoadOnStartUp(1);
            sib.setName("jsp");
            sib.getMappingList().add("jsp");
            getServletInfoList().add(sib);
        }
    }
    
    
    /**
     * 获取会话监听信息序列
     * @author 刘虻
     * 2008-6-28下午08:49:56
     * @return 会话监听信息序列
     */
    @Override
    public List<String> getSessionListenerClassList() {
        if (sessionListenerClassList==null) {
            sessionListenerClassList = new ArrayList<String>();
        }
        return sessionListenerClassList;
    }
    
    
    /**
     * 获取过滤器信息序列
     * @author 刘虻
     * 2008-6-28下午08:50:28
     * @return 过滤器信息序列
     */
    @Override
    public List<ServletInfoBean> getFilterInfoList() {
        if (filterInfoList==null) {
            filterInfoList = new ArrayList<ServletInfoBean>();
        }
        return filterInfoList;
    }


	/**
	 * 需要设置到配置信息类中的数据容器
	 * @return 需要设置到配置信息类中的数据容器
	 * 2018年11月8日
	 * @author MBG
	 */
	@Override
	public Map<String, String> getPropertyMap() {
		return confDataMap;
	}
}
