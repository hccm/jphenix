/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import java.util.Hashtable;

import com.jphenix.standard.docs.ClassInfo;

/**
 * BasicAuthRealm
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2006-9-15上午12:16:02
 */
@ClassInfo({"2019-09-17 10:36","BasicAuthRealm"})
public class BasicAuthRealm extends Hashtable<String,String> {
	
	/**
	 * 版本标识
	 */
	private static final long serialVersionUID = 6883285992191022947L;
	
	private String name; //母鸡name

	/**
	 * 构造函数
	 * 2006-9-15上午09:11:02
	 */
	public BasicAuthRealm(String name) {
		this.name = name;
	}

	/**
	 * 获取Name
	 * @author 刘虻
	 * 2008-7-8下午06:19:29
	 * @return 母鸡
	 */
	protected String getName() {
		return name;
	}
}