/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.*;
import java.io.IOException;
import java.util.List;

/**
 * 过滤器链
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2010-6-18 上午11:25:52
 */
@ClassInfo({"2019-09-17 10:36","过滤器链"})
public class FilterChainImpl implements FilterChain {

	private List<Filter> filterList = null; //过滤器序列 
	private int filterIndex = 0; //过滤器索引
	private Servlet servlet = null; //servlet
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public FilterChainImpl(List<Filter> filterList,Servlet servlet) {
		super();
		this.filterList = filterList;
		this.servlet = servlet;
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-6-18 上午11:25:53
	 */
	@Override
    public void doFilter(ServletRequest req, ServletResponse resp)
			throws IOException, ServletException {
		if(filterList==null || filterIndex>=filterList.size()) {
			if(servlet==null) {
				return;
			}
			servlet.service(req,resp);
			return;
		}
		//执行过滤
		filterList.get(filterIndex++).doFilter(req,resp,this);
	}
}
