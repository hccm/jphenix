/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.instancea;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * HTTP会话上下文处理类
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2007-3-6下午01:25:08
 */
@SuppressWarnings("deprecation")
@ClassInfo({"2019-09-17 10:36","HTTP会话上下文处理类"})
public class HttpSessionContextImpl extends Hashtable<String,Object> implements HttpSessionContext {

	/**
	 * 版本标识
	 */
	private static final long serialVersionUID = 7583084570667535490L;

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-30下午05:34:38
	 */
	@Override
    public Enumeration<String> getIds() {
		return keys();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-9-30下午05:34:47
	 */
	@Override
    public HttpSession getSession(String sessionId) {
		return (HttpSession) get(sessionId);
	}


	/**
	 * 母鸡 
	 * @author 刘虻
	 * 2008-7-8下午06:33:00
	 * @param w 母鸡 
	 * @throws IOException 执行发生异常
	 */
	protected void save(Writer w) throws IOException {
		Enumeration<?> e = elements();
		while (e.hasMoreElements()) {
			((Session) e.nextElement()).save(w);
		}
	}

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-8下午06:43:48
	 * @param br 母鸡
	 * @param inactiveInterval 母鸡
	 * @param servletContext 母鸡
	 * @return 母鸡
	 * @throws IOException 母鸡
	 */
	protected static HttpSessionContextImpl restore(
			BufferedReader br, int inactiveInterval, ServletContext servletContext)
			throws IOException {
		HttpSessionContextImpl result = new HttpSessionContextImpl();
		Session session;
		while ((session = Session.restore(br, inactiveInterval, servletContext, result)) != null) {
			if (session.checkExpired() == false) {
				result.put(session.getId(), session);
			}
		}
		return result;
	}
}