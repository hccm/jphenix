/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.servlet;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

/**
 * Servlet过滤器
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-6-30下午03:13:05
 */
@ClassInfo({"2019-09-17 10:44","Servlet过滤器"})
public class ServletFilterChain implements FilterChain {

	private HttpServlet servlet = null; //当前Servlet
	
	/**
	 * 构造函数
	 * 2008-6-30下午03:13:06
	 */
	public ServletFilterChain() {
		super();
	}
	
	/**
	 * 构造函数
	 * 2008-6-30下午03:14:46
	 */
	public ServletFilterChain(HttpServlet servlet) {
		super();
		this.servlet = servlet;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-30下午03:13:06
	 */
	@Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1)
			throws IOException, ServletException {
		servlet.service(arg0,arg1);
	}

}
