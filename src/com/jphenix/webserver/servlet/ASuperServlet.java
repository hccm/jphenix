/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.servlet;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.webserver.instancea.Serve;

import javax.servlet.http.HttpServlet;

/**
 * 超级Servlet父类
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-6-28下午04:48:44
 */
@ClassInfo({"2019-09-17 10:43","超级Servlet父类"})
public abstract class ASuperServlet extends HttpServlet {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -992180994104897883L;

	private Serve serv = null; //服务器
	
	/**
	 * 构造函数
	 * 2008-6-28下午04:48:44
	 */
	public ASuperServlet() {
		super();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-28下午04:48:44
	 */
	public Serve getServ() {
		return serv;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-28下午04:48:44
	 */
	public boolean hasSuperServlet() {
        return serv != null;
    }

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2008-6-28下午04:48:44
	 */
	public void setServ(Serve serv) {
		this.serv = serv;
	}

}
