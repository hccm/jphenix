/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.webserver.servlet;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Servlet信息类
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2008-6-28下午08:33:55
 */
@ClassInfo({"2019-09-17 10:44","Servlet信息类"})
public class ServletInfoBean {

	private String                   classPath     = null; //类路径
	private String                   name          = null; //类名称
	private Hashtable<String,Object> paraMap       = null; //参数容器
	private List<String>             mappingList   = null; //映射序列
	private int                      loadOnStartup = 0;    //启动顺序
	
	/**
	 * 构造函数
	 * 2008-6-28下午08:33:55
	 */
	public ServletInfoBean() {
		super();
	}
	
	/**
	 * 设置启动顺序
	 * @author 刘虻
	 * 2008-6-29下午03:49:15
	 * @param loadOnStartup 启动顺序
	 */
	public void setLoadOnStartUp(int loadOnStartup) {
		this.loadOnStartup = loadOnStartup;
	}
	
	/**
	 * 获取启动顺序
	 * @author 刘虻
	 * @return 启动顺序
	 * 2008-6-29下午03:49:58
	 */
	public int getLoadOnStartUp() {
		return loadOnStartup;
	}
	
	/**
	 * 获取映射序列
	 * @author 刘虻
	 * 2008-6-28下午08:55:33
	 * @return 映射序列
	 */
	public List<String> getMappingList() {
		if (mappingList==null) {
			mappingList = new ArrayList<String>();
		}
		return mappingList;
	}
	
	/**
	 * 设置类名称
	 * @author 刘虻
	 * 2008-6-28下午08:54:50
	 * @param name 类名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 获取类名称
	 * @author 刘虻
	 * 2008-6-28下午08:54:26
	 * @return 类名称
	 */
	public String getName() {
		if (name==null) {
			name = "";
		}
		return name;
	}
	
	/**
	 * 设置类路径
	 * @author 刘虻
	 * 2008-6-28下午08:36:35
	 * @param classPath 类路径
	 */
	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}
	
	/**
	 * 获取类路径
	 * @author 刘虻
	 * 2008-6-28下午08:36:43
	 * @return 类路径
	 */
	public String getClassPath() {
		return classPath;
	}
	
	/**
	 * 设置参数
	 * @author 刘虻
	 * 2008-6-28下午08:37:05
	 * @param key 参数主键
	 * @param value 参数值
	 */
	public void putParaInfo(String key,String value) {
		if (key==null || value==null) {
			return;
		}
		getParaMap().put(key,value);
	}
	
	/**
	 * 获取参数容器
	 * @author 刘虻
	 * 2008-6-28下午08:37:50
	 * @return 参数容器
	 */
	public Hashtable<String,Object> getParaMap() {
		if (paraMap==null) {
			paraMap = new Hashtable<String,Object>();
		}
		return paraMap;
	}
}
