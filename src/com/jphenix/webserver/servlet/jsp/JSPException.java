/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package	com.jphenix.webserver.servlet.jsp;
import javax.servlet.http.HttpServletResponse;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解析JSP时发生异常
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2009-11-18下午03:08:48
 */
@ClassInfo({"2019-09-17 10:42","解析JSP时发生异常"})
public class JSPException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -1439390193259881160L;
	
	private int httpErrorCode; //错误代码

	/**
	 * 构造函数
	 * 2009-11-18下午03:10:13
	 */
	public JSPException(int httpErrorCode, String msg) {
		super(msg);
		this.httpErrorCode = httpErrorCode;
	}

	/**
	 * 构造函数
	 * 2009-11-18下午03:10:18
	 */
	public JSPException(String msg) {
		this(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, msg);
	}

	/**
	 * 获取错误代码
	 * @author 刘虻
	 * 2009-11-18下午03:10:22
	 * @return 错误代码
	 */
	public int getHttpErrorCode() {
		return httpErrorCode;
	}
}
