/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package	com.jphenix.webserver.servlet.jsp;

import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.ServletConfig;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * JSP类加载器
 * 
 * 2019-09-17 整理了代码格式
 * 
 * @author 刘虻
 * 2009-11-18下午02:10:53
 */
@ClassInfo({"2019-09-17 10:42","JSP类加载器"})
public class JSPClassLoader extends ClassLoader {
	
	private final boolean debug; //是否为调试状态
	private final File repository; //生成的class类根路径
	private final ServletConfig config; //Servlet配置信息类
	private final Hashtable<String,Class<?>> classCache; //类缓存

	/**
	 * 构造函数
	 * 2009-11-18下午02:19:05
	 */
	public JSPClassLoader(
			File repository, ServletConfig config, boolean debug) {
		this.repository = repository;
		this.config = config;
		this.debug = debug;
		this.classCache = new Hashtable<String,Class<?>>();
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2009-11-18下午02:20:15
	 */
	@Override
    protected Class<?> loadClass(
			String className, boolean resolve) throws ClassNotFoundException {
		//从缓存中尝试获取
		Class<?> jspCls = classCache.get(className);
		if(jspCls == null) {
			//获取类数据
			byte[] classData = loadClassData(className);
			if(classData != null) {
				jspCls = defineClass(className, classData, 0, classData.length);
				if(debug) {
					config.getServletContext().log(className + " loaded by JSP class loader");
				}
			} else {
				//获取类加载器
				ClassLoader cl = JSPServlet.class.getClassLoader(); 
				if (cl == null) { 
					jspCls = findSystemClass(className); 
					if(debug) {
						config.getServletContext().log(className + " loaded by system class loader");
					}
				} else { 
					jspCls = cl.loadClass(className);
					if(debug) {
						if(jspCls.getClassLoader() == null) {
							config.getServletContext().log(className + " loaded by system class loader (through servlet class loader)");
						} else {
							config.getServletContext().log(className + " loaded by servlet class loader");
						}
					}
				}
				if(jspCls == null) {
					if(debug) {
						config.getServletContext().log("failed to load " + className);
					}
					return null;
				}
			}
			classCache.put(className, jspCls);
		}
		if(resolve) {
			resolveClass(jspCls);
		}
		return jspCls;
	}

	/**
	 * 获取JSP类数据
	 * @author 刘虻
	 * 2009-11-18下午02:21:44
	 * @param className JSP类名
	 * @return JSP类数据
	 */
	protected byte[] loadClassData(String className) {
		DataInputStream	in; //文件读入流
		byte[] classData; //返回值
		if(!className.startsWith("_jsp.")) {
			return null;
		}
		//获取目标类文件对象
		File classFile = 
			JSPCompiler.getFileForClass(repository, className, ".class");
		try {
			in = new DataInputStream(new FileInputStream(classFile));
			try {
				classData = new byte[(int) classFile.length()];
				in.readFully(classData, 0, classData.length);
				return classData;
			} finally {
				in.close();
			}
		} catch(IOException ioexc) {}
		return null;
	}
}
