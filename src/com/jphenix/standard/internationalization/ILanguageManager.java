/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.standard.internationalization;

import java.util.HashMap;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;


/**
 * 语言管理类
 * 
 * 规则：语言配置文件需要放到使用类的同级文件夹中。
 * 文件名为：类名_语言代码.properties
 * 文件内容采用UTF-8格式
 * 
 * @author 马宝刚
 * 2009-12-3 下午02:32:16
 */
@ClassInfo({"2014-06-03 17:38","语言管理类接口"})
@BeanInfo({"languagemanager"})
public interface ILanguageManager {

	
	/**
	 * 获取指定类的语言信息类
	 * 马宝刚
	 * 2009-12-3 下午02:33:41
	 * @param obj 指定类
	 * @return 语言信息类
	 */
	ILanguage getLanguge(Object obj);
	
	/**
	 * 通过使用国际化的类，获取国际化处理类实例
	 * 刘虻
	 * 2013-4-9 下午5:13:43
	 * @param cls 使用国际化的类
	 * @return 国际化处理类实例
	 */
	ILanguage getLanguageByClass(Class<?> cls);
	
	/**
	 * 获取语言编码
	 * 刘虻
	 * 2010-2-2 下午01:14:02
	 * @return 语言编码
	 */
	String getDefaultLanguageCode();
	
	/**
	 * 获取语言代码对照容器  比如 key:zh_cn  value:chs   key小写
	 * 刘虻
	 * 2013-4-12 下午2:22:48
	 * @return 语言代码对照容器
	 */
	HashMap<String,String> getLanguageCodeMap();
}
