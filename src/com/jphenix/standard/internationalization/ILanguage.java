/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-03
 * V4.0
 */
package com.jphenix.standard.internationalization;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;



/**
 * 语言处理类
 * 
 * 规则：语言配置文件需要放到使用类的同级文件夹中。
 * 文件名为：类名_语言代码.properties
 * 文件内容采用UTF-8格式
 * 
 * @author 马宝刚
 * 2009-12-3 下午02:31:41
 */
@ClassInfo({"2014-06-03 17:37","语言处理类接口"})
@BeanInfo({"language"})
public interface ILanguage {

	/**
	 * 输出指定的语言
	 * 刘虻
	 * 2010-2-2 下午01:03:36
	 * @param languageCode 语言代码
	 * @param contentKey 内容主键
	 * @param defaultContent 默认输出内容
	 * @param paraValues 参数值数组，用来替换信息中的<?> 
	 * @return 对应的内容
	 */
	String i(String languageCode,String contentKey,String defaultContent,String[] paraValues);
	
	
	/**
	 * 输出指定的语言 （先从线程会话中获取语言代码）
	 * 刘虻
	 * 2010-2-2 下午01:03:36
	 * @param contentKey 内容主键
	 * @param defaultContent 默认输出内容
	 * @param paraValues 参数值数组，用来替换信息中的<?> 
	 * @return 对应的内容
	 */
	String i(String contentKey,String defaultContent,String[] paraValues);
}
