/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年10月6日
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 已经做过处理的异常
 * 遇到这个异常，直接做返回即可，因为抛这个异常之前，已经做好处理
 * @author MBG
 * 2016年10月6日
 */
@ClassInfo({"2016-10-06 21:37","已经做过处理的异常"})
public class FixException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -2203668614542081087L;

	/**
	 * 构造函数
	 * @author MBG
	 */
	public FixException() {
		super();
	}
}
