/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年5月21日
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Http调用异常
 * 
 * 2019-09-10 增加了无提交参数的构造函数
 * 
 * 
 * @author MBG
 * 2018年5月21日
 */
@ClassInfo({"2019-09-10 18:19","Http调用异常"})
public class HttpException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -3664245257140753253L;

	private String    url        = null;   //调用目标URL
	private Object    data       = null;   //提交数据对象
	private int       statusCode = 0;      //返回状态码
	private String    statusMsg  = null;   //返回状态码对应的信息
	private Exception kernel     = null;   //上一级异常对象实例
	
	/**
	 * 构造函数
	 * @param url        调用目标URL
	 * @param data       提交数据对象
	 * @param statusCode 返回状态码
	 * @param statusMsg  返回状态码对应的信息
	 * @param e          上一级异常对象实例
	 * @author MBG
	 */
	public HttpException(
			String url
			,Object data
			,int statusCode
			,String statusMsg
			,Exception e) {
		super();
		this.url        = url;
		this.data       = data;
		this.statusCode = statusCode;
		this.statusMsg  = statusMsg;
		this.kernel     = e;
	}
	
	/**
	 * 构造函数
	 * @param url        调用目标URL
	 * @param statusCode 返回状态码
	 * @param statusMsg  返回状态码对应的信息
	 * @param e          上一级异常对象实例
	 * @author MBG
	 */
	public HttpException(
			String url
			,int statusCode
			,String statusMsg
			,Exception e) {
		super();
		this.url        = url;
		this.statusCode = statusCode;
		this.statusMsg  = statusMsg;
		this.kernel     = e;
	}
	
	/**
	 * 构造函数
	 * @param url        调用目标URL
	 * @param statusCode 返回状态码
	 * @param statusMsg  返回状态码对应的信息
	 * @author MBG
	 */
	public HttpException(
			String url
			,int statusCode
			,String statusMsg) {
		super();
		this.url        = url;
		this.statusCode = statusCode;
		this.statusMsg  = statusMsg;
	}
	
	/**
	 * 返回调用目标URL
	 * @return 调用目标URL
	 * 2018年5月21日
	 * @author MBG
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * 返回提交数据对象
	 * @return 提交数据对象
	 * 2018年5月21日
	 * @author MBG
	 */
	public Object getData() {
		return data;
	}
	
	/**
	 * 返回返回状态码
	 * @return 返回状态码
	 * 2018年5月21日
	 * @author MBG
	 */
	public int getStatusCode() {
		return statusCode;
	}
	
	/**
	 * 返回返回状态码对应的信息
	 * @return 返回状态码对应的信息
	 * 2018年5月21日
	 * @author MBG
	 */
	public String getStatusMsg() {
		return statusMsg;
	}
	
	/**
	 * 返回上一级异常对象实例
	 * @return 上一级异常对象实例
	 * 2018年5月21日
	 * @author MBG
	 */
	public Exception getKernel() {
		return kernel;
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
    public String toString() {
		return "HttpException URL:["+url+"] Params:["+data+"] e:["+
				kernel+"] HTTP Status:["+statusCode+"] Msg:["+statusMsg+"]";
	}
}
