/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年4月22日
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 超时异常
 * @author MBG
 * 2017年4月22日
 */
@ClassInfo({"2017-04-22 15:58","超时异常"})
public class TimeOutException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = 5494671462782809084L;

	/**
	 * 构造函数
	 * @author MBG
	 */
	public TimeOutException() {
		super();
	}

	/**
	 * 构造函数
	 * @author MBG
	 */
	public TimeOutException(String message) {
		super(message);
	}
}
