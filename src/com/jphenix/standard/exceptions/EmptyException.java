/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年1月31日
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 空的异常
 * 遇到该异常，不做任何提示，直接退出
 * @author MBG
 * 2017年1月31日
 */
@ClassInfo({"2017-01-31 00:38","空的异常"})
public class EmptyException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -6414148117953661445L;

	/**
	 * 构造函数
	 * @author MBG
	 */
	public EmptyException() {
		super();
	}
}
