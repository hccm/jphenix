/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 执行动作发生逻辑异常
 * @author 刘虻
 * 2010-10-27 下午10:05:36
 */
@ClassInfo({"2014-06-13 14:44","执行动作发生逻辑异常"})
public class ActionException extends MsgException {

	/**
	 * 串行标识
	 */
	protected final static long serialVersionUID = 6883896345578941653L;

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param bossCls 抛异常类
	 * @param msg 异常信息
	 */
	public ActionException(Class<?> bossCls, String msg) {
		super(bossCls, msg);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param boss 抛异常类实例
	 * @param msg 异常信息
	 */
	public ActionException(Object boss, String msg) {
		super(boss, msg);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param boss 抛异常类实例
	 * @param e 异常
	 */
	public ActionException(Object boss, Exception e) {
		super(boss, e);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param boss 抛异常类实例
	 * @param msg 异常信息
	 * @param e 异常
	 */
	public ActionException(Object boss, String msg, Exception e) {
		super(boss, msg, e);
	}

}
