/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.exceptions;

import com.jphenix.standard.docs.ClassInfo;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 信息输出异常类
 * 
 * 通常抛这种异常，都是输出错误提示信息，而不是为了排查错误位置
 * 
 * 2020-03-28 增加了只传字符串消息的构造函数
 * 
 * @author 刘虻
 * 2010-9-1 下午04:10:19
 */
@ClassInfo({"2020-03-28 14:26","信息输出异常类"})
public class MsgException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -4653868846816952253L;

	protected String head = null; //异常头
	protected String code = null; //异常代码
	protected String msg = null; //异常信息
	
	/**
	 * 构造函数
	 * @param msg  异常信息 
	 * @author MBG
	 */
	public MsgException(String msg) {
		super(msg);
		if(msg==null || msg.length()<1) {
			msg = "MsgException";
		}
		this.msg = msg;
	}
	
	/**
	 * 构造函数
	 * @param head 异常头信息
	 * @param msg  异常信息 
	 * @param code 错误代码
	 * @author MBG
	 */
	public MsgException(String head,String msg,String code) {
		super(msg);
		if(msg==null || msg.length()<1) {
			msg = "MsgException";
		}
		this.msg = msg;
		this.head = head;
		this.code = code;
	}
	
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param bossCls 抛异常类
	 * @param msg 异常信息
	 */
	public MsgException(Class<?> bossCls,String msg) {
		super(msg);
		if(msg==null || msg.length()<1) {
			msg = "MsgException";
		}
		this.msg = msg;
		if(bossCls!=null) {
			this.head = 
				" ["+bossCls.getName()+"] ["
				+(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
					.format(new Date())+"]";
		}
	}
	
	/**
	 * 构造函数
	 * @author 马宝刚
     * @param bossCls 抛异常类
     * @param msg 异常信息
     * @param code 错误代码
	 */
	public MsgException(Class<?> bossCls,String msg,String code) {
        super(msg);
        if(msg==null || msg.length()<1) {
            msg = "MsgException";
        }
        if(code==null || code.length()<1) {
            code = "-98";
        }
        this.code = code;
        this.msg = msg;
        if(bossCls!=null) {
            this.head = 
                " ["+bossCls.getName()+"] ["
                +(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
                    .format(new Date())+"]";
        }
    }
	
	/**
	 * 构造函数
	 * @param boss 抛异常类
	 * @param msg 异常信息
	 * @author 刘虻
	 */
	public MsgException(Object boss,String msg) {
		super(msg);
		if(msg==null || msg.length()<1) {
			msg = "MsgException";
		}
		this.msg = msg;
		if(boss!=null) {
			this.head = 
				" ["+boss.getClass().getName()+"] ["
				+(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
					.format(new Date())+"]";
		}
	}
	
	
	/**
	 * 构造函数
     * @param boss 抛异常类
     * @param msg 异常信息
     * @param code 错误代码
	 * @author 马宝刚
	 */
	public MsgException(Object boss,String msg,String code) {
	        super(msg);
	        if(msg==null || msg.length()<1) {
	            msg = "MsgException";
	        }
	        if(code==null || code.length()<1) {
	            code = "-98";
	        }
	        this.msg = msg;
	        this.code = code;
	        if(boss!=null) {
	            this.head = 
	                " ["+boss.getClass().getName()+"] ["
	                +(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
	                    .format(new Date())+"]";
	        }
	    }
	
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-1 下午04:29:11
	 */
	@Override
    public String getMessage() {
		return msg==null?"":msg;
	}
	
	/**
	 * 返回错误代码
	 * @return
	 * 2015年9月23日
	 * @author 马宝刚
	 */
	public String getCode() {
	    return code==null?"":code;
	}
	
	/**
	 * 返回异常头信息
	 * @return 异常头信息
	 * 2020年3月28日
	 * @author MBG
	 */
	public String getHead() {
		return head==null?"":head;
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-1 下午04:27:48
	 */
	@Override
    public void printStackTrace() {
		if(msg==null) {
			return;
		}
		System.err.println("\nXXXXXXXXXXXXXXXXXXXXXXX\n"+head+msg+"\nXXXXXXXXXXXXXXXXXXXXXXX");
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-1 下午04:28:41
	 */
	@Override
    public String toString() {
		if(head==null) {
			head = "";
		}
		return msg==null?"":head+msg;
	}
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param boss 抛异常类
	 * @param e 其它异常
	 */
	public MsgException(Object boss,Exception e) {
		super(e);
		if(e==null) {
			msg = "MsgException";
		}else {
			msg = e.toString();
		}
		if(e instanceof MsgException) {
			return;
		}
		if(boss!=null) {
			head = " ["+boss.getClass().getName()+"]";
		}
		head += " ["+(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
						.format(new Date())+"]";
	}
	
	/**
	 * 构造函数
	 * @author 刘虻
	 * @param boss 抛异常类
	 * @param msg 异常信息
	 * @param e 其它异常
	 */
	public MsgException(Object boss,String msg,Exception e) {
		super(msg,e);
		if(msg==null && e!=null) {
			this.msg = e.toString();
		}
		if(e instanceof MsgException) {
			return;
		}
		if(boss!=null) {
			this.head += " ["+boss.getClass().getName()+"]";
		}
		this.head += " ["+(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S"))
						.format(new Date())+"]";
	}
}
