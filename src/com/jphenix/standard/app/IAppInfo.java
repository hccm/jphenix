/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.standard.app;

import java.util.List;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 模块功能点信息接口
 * @author 刘虻
 * 2010-6-22 下午05:52:23
 */
@ClassInfo({"2014-06-05 16:38","模块功能点信息接口"})
@BeanInfo({"appinfo"})
public interface IAppInfo {
	
	/**
	 * 获取模块功能点信息序列
	 * 刘虻
	 * 2010-6-22 下午06:17:02
	 * @return 模块功能点信息序列
	 */
	List<IAppPurview> getAppInfoItemList();
	
	
	/**
	 * 获取模块主键
	 * 刘虻
	 * 2010-6-22 下午06:17:59
	 * @return 模块主键
	 */
	String getModuleID();
	
	
	/**
	 * 获取模块排序索引
	 * 刘虻
	 * 2010-6-22 下午06:18:40
	 * @return 模块排序索引
	 */
	int getModuleIndex();
}
