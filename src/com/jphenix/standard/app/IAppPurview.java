/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.app;

import java.util.List;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;


/**
 * 功能点权限接口
 * @author 刘虻
 * 2010-6-23 上午11:18:13
 */
@ClassInfo({"2014-06-13 14:13","功能点权限接口"})
@BeanInfo({"apppurview"})
public interface IAppPurview {

	/**
	 * 处理功能点信息序列（过滤掉没有权限的功能点）
	 * 刘虻
	 * 2010-6-23 上午11:22:50
	 * @param appInfoList 待处理的功能点信息序列
	 * @return 处理后的模块信息序列
	 */
	List<IAppPurview> purview(List<IAppPurview> appInfoList);
	
	/**
	 * 获取模块主键
	 * 刘虻
	 * 2010-6-23 上午11:29:36
	 * @return 模块主键
	 */
	String getModuleID();
}
