/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.app;

import java.util.List;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 模块权限接口
 * @author 刘虻
 * 2010-6-23 上午11:19:58
 */
@ClassInfo({"2014-06-13 14:13","模块权限接口"})
public interface IModulePurview {

	/**
	 * 处理模块信息序列（过滤掉没有权限的模块）
	 * 刘虻
	 * 2010-6-23 上午11:21:39
	 * @param moduleInfoList 待处理的模块信息序列
	 * @return 处理后的模块信息序列
	 */
	List<IAppPurview> purview(List<IAppPurview> moduleInfoList);
}
