/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import java.util.Map;

import com.jphenix.driver.json.Json;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 动作类接口
 * 
 * 2018-12-12 增加了直接设置输出类型值的方法
 * 2019-01-24 去掉了调用动作还带返回值的方法，调用动作不需要返回值。具体请看invokeAction方法说明
 * 2021-03-15 去掉了不常用的 sendRedirectByID 方法，增加了两个父类方法 beforeOutView 和 beforeOutJson
 * 
 * @author 刘虻
 * 2010-5-27 下午02:41:53
 */
@ClassInfo({"2021-03-15 17:44","动作类接口"})
public interface IActionBean {

	/**
	 * 获取动作方法对照容器
	 * 刘虻
	 * 2010-5-27 下午03:23:02
	 * @return 动作方法对照容器
	 */
	Map<String,String> getActionMap();
	
	/**
	 * 设置模板中的参数信息容器
	 * 刘虻
	 * 2010-8-5 下午02:16:07
	 * @param parameterMap 模板中的参数信息容器
	 */
	void setModelParameterMap(Map<String,String> parameterMap);
	
	/**
	 * 获取返回主界面 原方法名：getReturnViewHandler
	 * 刘虻
	 * 2010-6-17 上午10:34:56
	 * @return 返回主界面
	 */
	IViewHandler getReVh();
	
	/**
	 * 设置返回主界面 原方法名：setReturnViewHandler
	 * 刘虻
	 * 2010-6-17 上午10:35:37
	 * @param vh 返回主界面
	 * @return 返回主界面（与传入参数相同，方便写程序用）
	 */
	IViewHandler setReVh(IViewHandler vh);
	
	/**
	 * 通过视图主键获取页面对象，并设置为返回主页面 源方法名
	 * 刘虻
	 * 2012-12-4 下午2:18:33
	 * @param viewID 页面主键  setPathMap 中的key
	 * @return 设置为返回主页面后的页面对象
	 * @throws Exception 异常
	 */
	IViewHandler setReVh(String viewID) throws Exception;
	
	/**
	 * 获取一个新的主界面 原方法名 getNewViewHandler
	 * 刘虻
	 * 2010-6-17 上午10:35:59
	 * @return 一个新的主界面
	 */
	IViewHandler newVh();
	
	/**
	 * 根据视图主键获取视图类实例 原方法名：getViewHandlerByID
	 * 刘虻
	 * 2010-6-17 上午10:36:39
	 * @param viewID 视图主键
	 * @return 视图类实例
	 * @throws Exception 执行发生异常
	 */
	IViewHandler getVh(String nodeID) throws Exception;
	
	/**
	 * 在调用动作前执行的方法（由系统调用，需要时覆盖该方法）
	 * 刘虻
	 * 2010-6-28 下午06:26:27
	 * @return 如果返回假，则不执行当前动作，直接返回
	 * @throws Exception 执行发生异常
	 */
	boolean beforeRunAction() throws Exception;
	
	/**
	 * 在调用动作后执行的方法 （由系统调用，需要时覆盖该方法）
	 * 刘虻
	 * 2010-6-28 下午06:27:56
	 * @return 如果返回假，则不继续输出数据（通常在这个方法中已经输出数据，顾返回假）
	 * @throws Exception 执行发生异常
	 */
	boolean afterRunAction() throws Exception;
	
	/**
	 * 设置动作上下文
	 * 刘虻
	 * 2010-6-30 上午11:28:54
	 * @param actionContext 动作上下文
	 */
	void setActionContext(IActionContext actionContext);
	
	/**
	 *  一个动作的每个环节异常处理方法
	 * 刘虻
	 * 2010-6-8 下午02:15:24
	 * @param e 异常
	 * @throws Exception 异常
	 */
	void errorAction(Exception e) throws Exception;
	
	/**
   * 获取动作上下文
   */
  IActionContext getActionContext();
  
  /**
   * 获取即将返回的Json数据对象
   * @return
   * 2014年4月17日
   * @author 马宝刚
   */
  Json getReJson();

  /**
   * 获取即将返回的XML数据对象
   * @return 即将返回的XML数据对象
   * 2014年6月21日
   * @author 马宝刚
   */
  IViewHandler getReXml();
  
  /**
   * 获取返回内容类型  0html 1xml 2json  
   * 
   * 如果指定模板，就不判断内容类型了，直接往模板中插入数据返回
   * 如果没指定模板，通过报文头中的内容类型来判断返回什么格式
   * 如果内容类型既不是json也不是xml，从提交参数中获取返回内容类型（_oct）
   * 如果如果提交参数中也没有设置，则设定为配置文件中默认输出(DEFAULT_OUT_CONTENT_TYPE)内容格式
   * 
   * 如果没设置默认输出格式，默认输出json格式
   * @return
   * 2014年6月21日
   * @author 马宝刚
   */
  int getOutContentType();
  
  
  /**
   * 执行动作
   * 
   * 注意：之前是带返回值的，通常返回布尔值，然后用来判断是否继续往下执行。
   *       实际上返回值可以放到request.setAttribute 即：$$(key,value) $$(key)
   * 
   * 
   * @throws Exception 异常
   * 2014年8月1日
   * @author 马宝刚
   */
  void invokeAction() throws Exception;
  
  /**
   * 是否不处理返回信息对象
   * @return 是否不处理返回信息对象
   * 2014年8月16日
   * @author 马宝刚
   */
  boolean isNoReturnInfo();
  
  
  /**
   * 是否处理完当前动作后，就不再处理页面中后续的页面块动作
   * @return 如题
   * 2016年11月23日
   * @author MBG
   */
  boolean isOver();
  
  
  /**
   * 设置是否处理完当前动作后，就不再处理页面中后续的页面块动作
   * @param isOver 如题
   * 2016年11月23日
   * @author MBG
   */
  void over(boolean isOver);
  
  /**
   * 是否在当前动作块中加载了整个页面
   * @return 如题
   * 2016年11月23日
   * @author MBG
   */
  boolean loadAllPage();
    
  /**
   * 整理调用动作的参数
   * 这么做主要是为了在调用beforeRunAction时，能取到即将传入的值，有些值只能获取一次
   * ，比如提交的XML对象或者Json对象
   * @param paraMap 指定设置的参数值容器 key，参数名  value参数值 （优先使用该容器中的值）
   * @throws Exception 异常
   * 2014年8月1日
   * @author 马宝刚
   */
	void buildActionParameter(Map<String,?> paraMap) throws Exception;
	
  /**
   * 设置输出类型 
   * @param contentType  0html  1xml  2json
   * 2018年12月12日
   * @author MBG
   */
  void setContentType(int contentType);

  /**
   * 在输出页面之前调用该方法
   * 该方法可以在继承该类的父类中覆盖该方法，用来统一整理即将输出的页面
   * @param vh 即将输出的页面对象
   * @return   处理后的页面对象
   */
  IViewHandler beforeOutView(IViewHandler vh);

  /**
   * 在输出页面之前调用该方法
   * 该方法可以在继承该类的父类中覆盖该方法，用来统一整理即将输出的Json数据
   * @param json 即将输出的json数据对象
   * @return   处理后的json数据对象
   */
  Json beforeOutJson(Json json);
}
