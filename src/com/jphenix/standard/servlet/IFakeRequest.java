/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年11月24日
 * V4.0
 */
package com.jphenix.standard.servlet;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 仿造的页面请求对象接口
 * @author MBG
 * 2016年11月24日
 */
@ClassInfo({"2016-11-24 10:03","仿造的页面请求对象接口"})
public interface IFakeRequest extends IRequest {

	
	/**
	 * 设置路径信息
	 * @param webBasePath  网站根路径（文件路径）
	 * @param contextPath  上下文路径  /网站虚拟路径
	 * @param servletPath  纯动作路径  /文件夹/文件     weblogic返回空
	 * @param queryString  url中提交的参数
	 * 2016年11月24日
	 * @author MBG
	 */
	void setUrlInfo(String webBasePath,String contextPath,String servletPath,String queryString);
	
	
	/**
	 * 设置请求方式
	 * @param method 请求方式
	 * 2016年11月24日
	 * @author MBG
	 */
	void setMethod(String method);
	
	
	/**
	 * 设置提交的数据
	 * @param data 提交的数据
	 * 2016年11月24日
	 * @author MBG
	 */
	void setPostData(Object data);
	
	/**
	 * 设置内容类型
	 * @param contentType 内容类型
	 * 2016年11月24日
	 * @author MBG
	 */
	void setContentType(String contentType);
	
	/**
	 * 设置方案 http 还是 https
	 * @param scheme 方案
	 * 2016年11月24日
	 * @author MBG
	 */
	void setScheme(String scheme);
	
	/**
	 * 设置头信息
	 * @param key   头主键
	 * @param value 对应值
	 * 2016年12月11日
	 * @author MBG
	 */
	void setHeader(String key,String value);
}
