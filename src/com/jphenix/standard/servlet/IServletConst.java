/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import com.jphenix.standard.docs.ClassInfo;

/**
 * Servlet 常量信息
 * 
 * 2018-12-14 增加了用户会话主键值的键名
 * 2019-01-31 增加了请求方IP地址主键，请求路径主键
 * 2020-08-03 去掉了缓存相关的常亮，没用过缓存，已经取消缓存
 * 
 * 
 * @author 刘虻
 * 2010-5-24 下午03:49:35
 */
@ClassInfo({"2020-08-06 15:11","Servlet 常量信息"})
public interface IServletConst {
    
    /**
     * 初始化参数主键：工厂主键
     */
    String SERVLET_CONFIG_KEY_FACTORY_ID = "factoryid";
    
    /**
     * 初始化参数主键：根配置文件路径
     */
    String SERVLET_CONFIG_KEY_CONFIG_PATH = "configpath";
    
    /**
     * 初始化参数主键：类根路径
     */
    String SERVLET_CONFIG_KEY_BASE_CLASS_PATH = "base_class_path";
    
    /**
     * 是否为调试模式
     */
    String SERVLET_CONFIG_KEY_DEBUG = "debug";
    
    /**
     * Servlet 扩展名
     */
    String SERVLET_CONFIG_KEY_EXT_NAME = "extname";
    
    /**
     * 模板扩展名，程序会屏蔽直接显示模板到浏览器中
     */
    String SERVLET_CONFIG_KEY_MODEL_EXT_NAME = "modelextname";

    /**
     * 动作上下文主键
     */
    String KEY_ACTION_CONTEXT = "_action_context";
    
    /**
     * 动作请求IP
     */
    String KEY_ACTION_IP = "_action_ip";
    
    /**
     * 动作请求路径
     */
    String KEY_ACTION_NAME = "_action_name";
    
    /**
     * 会话主键
     */
    String KEY_SESSION_ID = "_session_id";
    
    /**
     * 网站根路径
     */
    String PARAMETER_WEB_BASE_PATH = "webBasePath";
    
    /**
     * 视图扩展名
     */
    String PARAMETER_VIEW_EXT_NAME = "viewExtName";
    
    /**
     * 视图绑定动作父类主键   绑定后在解析视图模板前后会调用对应父类的 beforeRunAction 和 afterRunAction 方法
     */
    String VIEW_PARAMETER_ACTION_BEAN_PARENT = "actionbeanparent";
}
