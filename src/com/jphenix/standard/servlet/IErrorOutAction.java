/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 错误信息输出动作接口
 * 
 * 将错误信息用特殊标签方式输出到页面上
 * 静态页面发布程序会检查将要发布的页面中是否包含该错误标签
 * 来判断页面是否出现错误
 * 
 * 
 * @author 刘虻
 * 2010-6-2 下午04:43:04
 */
@ClassInfo({"2014-06-13 14:55","错误信息输出动作接口"})
@BeanInfo({"erroroutaction"})
public interface IErrorOutAction extends IActionBean {

	/**
	 * 输出错误信息
	 * 刘虻
	 * 2010-6-2 下午04:45:00
	 * @param actionContext 动作上下文
	 * 
	 * 	参数用getAttribute方法获取到
	 * 
	 *  输出信息 主键：_out_msg  (String)
	 *  
	 *  输出信息类型 主键： _out_type (String)  
	 *  
	 *  	ERROR 系统错误  ACTION业务逻辑错误
	 * 
	 * 	为业务逻辑错误时，getReturnViewHandler()获取到的值已经是处理好的界面
	 * 
	 * 
	 * 
	 *  输出异常类 主键：_out_exception (Exception)
	 *  
	 * @throws Exception 执行发生异常
	 */
	void out(IActionContext actionContext) throws Exception;
}
