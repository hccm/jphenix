/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月11日
 * V4.0
 */
package com.jphenix.standard.servlet;

import javax.servlet.http.HttpSession;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 会话管理类
 * @author 马宝刚
 * 2014年9月11日
 */
@ClassInfo({"2014-09-11 21:22","会话管理类"})
public interface ISessionManager {

    /**
     * 终止当前会话
     * @param session 会话对象
     * 2014年9月11日
     * @author 马宝刚
     */
    void invalidate(HttpSession session);
}
