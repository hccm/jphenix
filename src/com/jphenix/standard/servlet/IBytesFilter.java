/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月30日
 * V4.0
 */
package com.jphenix.standard.servlet;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 字节数组过滤器
 * @author 马宝刚
 * 2014年6月30日
 */
@ClassInfo({"2014-06-30 14:05","字节数组过滤器"})
public interface IBytesFilter {

    /**
     * 获取排序索引，数值越小越先执行
     * 刘虻
     * 2010-5-25 下午03:15:01
     * @return 排序索引
     */
    int getIndex();
    
    /**
     * 执行过滤
     * @param bytes 过滤前的字节数组
     * @param req 页面请求对象 
     * @param resp 页面反馈
     * @return 过滤后的字节数组
     * @throws Exception 异常
     * 2014年6月30日
     * @author 马宝刚
     */
    byte[] doBytesFilter(byte[] bytes,IRequest req,IResponse resp) throws Exception;
    
    
    /**
     * 读取指定文档内容并做内容处理
     * @param filePath     文档全路径
     * @param req          页面请求
     * @param resp         页面反馈
     * @return             处理后的文件内容
     * @throws Exception   异常
     * 2017年9月11日
     * @author MBG
     */
    byte[] doFileFilter(String filePath,IRequest req,IResponse resp) throws Exception;
}
