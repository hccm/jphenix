/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import com.jphenix.servlet.multipart.instancea.DownloadFile;
import com.jphenix.servlet.multipart.instancea.MultipartServletRequest;
import com.jphenix.standard.docs.ClassInfo;

import javax.servlet.ServletOutputStream;
import java.util.Map;

/**
 * 动作上下文
 * 
 * 2020-03-13 增加了从URL中获取参数值的方法 getUrlParameter
 * 
 * 
 * @author 刘虻
 * 2010-5-21 下午06:34:45
 */
@ClassInfo({"2020-03-13 16:53","动作上下文"})
public interface IActionContext {

	/**
	 * 页面请求对象
	 * 刘虻
	 * 2010-5-27 下午04:41:44
	 * @return 请求对象
	 */
	IRequest getRequest();
	
	/**
	 * 获取页面反馈对象
	 * 刘虻
	 * 2010-5-27 下午04:41:09
	 * @return 页面反馈对象
	 */
	IResponse getResponse();
	
	/**
	 * 设置属性
	 * 刘虻
	 * 2010-6-2 下午04:55:10
	 * @param key 属性主键
	 * @param value 属性值
	 */
	void setAttribute(String key,Object value);
	
	/**
	 * 获取指定属性值
	 * 刘虻
	 * 2010-6-2 下午04:55:50
	 * @param key 属性主键
	 * @return 属性值
	 */
	Object getAttribute(String key);
	
	/**
	 * 获取参数值(参数值永不等于空)
	 * 刘虻
	 * 2010-6-9 下午05:39:24
	 * @param key 参数主键
	 * @return 参数值
	 */
	String getParameter(String key);
	
	/**
	 * 获取会话属性信息
	 * 刘虻
	 * 2010-6-9 下午05:38:03
	 * @param key 会话属性主键
	 * @return 会话属性值
	 */
	Object getSessionAttribute(String key);
	
	/**
	 * 设置会话属性信息
	 * 刘虻
	 * 2010-6-9 下午05:38:25
	 * @param key 会话属性信息主键
	 * @param value 会话属性信息值
	 */
	void setSessionAttribute(String key,Object value);
	
	/**
	 * 获取会话主键
	 * 刘虻
	 * 2010-6-9 下午05:38:44
	 * @return 会话主键
	 */
	String getSessionID();
	
	/**
	 * 获取动作路径
	 * 刘虻
	 * 2010-6-9 下午08:49:55
	 * @return 动作路径
	 */
	String getAction();
	
	/**
	 * 获取错误信息
	 * 刘虻
	 * 2010-6-17 下午02:56:22
	 * @return 错误信息
	 */
	String getErrorMessage();
	
	/**
	 * 累加错误信息
	 * 刘虻
	 * 2010-6-17 下午02:55:55
	 * @param content 错误信息
	 */
	void addErrorMessage(String content);
	
	/**
	 * 获取参数容器
	 * 原getParameterMap中的参数值为字符串数组，用起来不方便
	 * 刘虻
	 * 2010-7-6 下午03:38:43
	 * @return 参数容器
	 */
	Map<String,String> getSingleParameterMap();
	
	/**
	 * 是否为非文本信息提交
	 * 刘虻
	 * 2011-5-4 下午04:29:44
	 * @return true是
	 */
	boolean isMultipartRequest();
	
	/**
	 * 获取非文本信息请求对象
	 * 刘虻
	 * 2011-5-4 下午04:33:12
	 * @return 非文本信息请求对象
	 */
	MultipartServletRequest getMultipartServletRequest();
	
	/**
	 * 获取网站根路径
	 * 刘虻
	 * 2012-7-29 下午10:07:42
	 * @return 网站根路径
	 */
	String getWebBasePath();
	
	/**
	 * 获取当前访问过来的域名
	 * 刘虻
	 * 2012-7-30 上午8:52:46
	 * @return 当前访问过来的域名
	 */
	String getDomain();
	
	   /**
     * 获取Cookie值
     * @param key Cookie主键
     * @return Cookie值
     * 2014年5月28日
     * @author 马宝刚
     */
    String getCookie(String key);
    
    /**
     * 设置Cookie信息（默认最长时间，全局域）
     * @param key      主键
     * @param value    值
     * 2014年5月28日
     * @author 马宝刚
     */
    void setCookie(String key,String value);
    
	/**
	 * 设置Cookie信息（默认最长时间1年，全局域）
	 * @param key          主键
	 * @param value       值
	 * @param path        域（相对路径）
	 * @param outTime  过期时间（秒）
	 * 2017年8月21日
	 * @author MBG
	 */
	void setCookie(String key,String value,String path,int outTime);
	
	/**
	 * 删除指定Cookie
	 * @param key 主键
	 * 2017年9月11日
	 * @author MBG
	 */
    void delCookie(String key);
	
	/**
	 * 删除指定Cookie
	 * @param key  主键
	 * @param path 域（相对路径）
	 * 2017年9月11日
	 * @author MBG
	 */
	void delCookie(String key,String path);
    
    /**
     * 是否存在该参数
     * @param key 参数主键
     * @return 是否存在该参数
     * 2014年6月12日
     * @author 马宝刚
     */
    boolean hasParameter(String key);
    
    /**
     * 设置参数值
     * @param key          参数主键
     * @param values      参数值
     * 2014年6月12日
     * @author 马宝刚
     */
    void setParameter(String key,String[] values);
    
    
	/**
	 * 获取下载处理对象
	 * @return 下载处理对象
	 * 2014年8月17日
	 * @author 马宝刚
	 */
	DownloadFile getDownloadFile();
	
	/**
	 * 获取下载处理对象
	 * @param uploadBasePath 重新设置的文件根路径
	 * @return 下载处理对象
	 * 2016年11月14日
	 * @author MBG
	 */
	DownloadFile getDownloadFile(String uploadBasePath);
	
	/**
	 * 获取客户端IP地址
	 * @return 客户端IP地址
	 * 2016年9月28日
	 * @author MBG
	 */
	String getRemoteAddress();
	
	/**
	 * 获取客户端域名
	 * @return 客户端域名
	 * 2016年9月28日
	 * @author MBG
	 */
	String getRemoteHost();
	
	/**
	 * 获取发起请求的服务器的IP地址（避免取到代理服务器地址）
	 * @return 发起请求的服务器的IP地址（返回信息绝对不为null）
	 * 2016年10月17日
	 * @author MBG
	 */
	String cip();
	
	/**
	 * 上传文件根路径（绝对路径）
	 * @return 上传文件根路径
	 * 2016年11月3日
	 * @author MBG
	 */
	String uploadPath();
	
	/**
	 * 清除缓存
	 * 2017年4月27日
	 * @author MBG
	 */
	void clear();
	
    /**
     * 刷新缓存
     * @throws Exception 异常
     * 2014年8月28日
     * @author 马宝刚
     */
    void flush() throws Exception;
    
    /**
     * 获取输出流
     * 2014年8月28日
     * @author 马宝刚
     */
    ServletOutputStream getStream() throws Exception;
    
    /**
     * 直接将信息输出到界面
     * @param contentBytes 信息
     * @throws Exception 异常
     * 2014年8月28日
     * @author 马宝刚
     */
    void write(byte[] contentBytes) throws Exception;
    
    /**
     * 直接将内容输出到页面中
     * @param content 信息对象
     * @return 当前类实例
     * @throws Exception 异常
     * 2014年8月16日
     * @author 马宝刚
     */
    void outContent(Object content) throws Exception;
    
    /**
     * 设置返回报文头信息
     * @param key 信息主键
     * @param value 信息值
     * 2014年8月28日
     * @author 马宝刚
     */
    void setHeader(String key,String value);
    
    /**
     * 增加报文头信息
     * @param key    信息主键
     * @param value 信息值
     * 2017年4月28日
     * @author MBG
     */
    void addHeader(String key,String value);
    
	/**
	 * 请求方式是否为POST
	 * @return 请求方式是否为POST
	 * 2017年5月1日
	 * @author MBG
	 */
	boolean isPost();
	
    /**
     * 从URL中获取参数值
     * @param key  参数主键
     * @return     对应的参数值
     * 2020年3月13日
     * @author MBG
     */
    String getUrlParameter(String key);
}
