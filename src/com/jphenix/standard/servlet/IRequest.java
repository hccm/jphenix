/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 页面请求扩展接口
 * @author 刘虻
 * 2011-5-4 下午01:48:32
 */
@ClassInfo({"2014-06-13 14:55","页面请求扩展接口"})
public interface IRequest extends HttpServletRequest {

    /**
     * 设置传入参数
     * @param key      参数主键
     * @param value    参数值数组
     * 2014年6月12日
     * @author 马宝刚
     */
    void setParameter(String key,String[] value);
    
    /**
     * 设置传入参数
     * @param key      参数主键
     * @param value    参数值
     * 2014年6月12日
     * @author 马宝刚
     */
    void setParameter(String key,String value);
    
	
	/**
	 * 设置是否禁止使用页面请求外壳（主要用于过滤）
	 * 刘虻
	 * 2010-8-2 下午03:25:07
	 * @param disabled 是否禁止使用页面请求外壳
	 */
	void setDisabledWrapper(boolean disabled);
	
	   /**
     * 获取客户端IP地址信息 简称：cip
     * @return 客户端IP地址信息
     * 2014年4月15日
     * @author 马宝刚
     */
    String getClientIP();
    
    
	/**
	 * 获取客户端IP地址信息 全称：getClientIP
	 * @return 客户端IP地址信息
	 * 2014年4月15日
	 * @author 马宝刚
	 */
    String cip();
    
    /**
     * 获取头信息容器
     * @return 头信息容器
     * 2014年9月5日
     * @author 马宝刚
     */
    Map<String,String> getHeaderMap();
    
    /**
     * 设置缓存提交值
     * 2014年9月5日
     * @author 马宝刚
     */
    void setBuffered();
    
    /**
     * 是否缓存提交值
     * @return 是否缓存提交值
     * 2014年9月5日
     * @author 马宝刚
     */
    boolean isBuffered();
    
    /**
     * 获取提交数据字节数组
     * @return 提交数据字节数组
     * 2014年9月5日
     * @author 马宝刚
     */
    byte[] getPostData();
    
    /**
     * 获取网站根路径
     * @return 网站根路径
     * 2016年3月11日
     * @author 马宝刚
     */
    String getWebBasePath();
    
    
	/**
	 * 获取自定义头主键序列
	 * @return 自定义头主键序列
	 * 2016年7月13日
	 * @author MBG
	 */
	List<String> getCustomHeaderNameList();
	
	
	/**
	 * 获取自定义头信息容器
	 * @return 自定义头信息容器
	 * 2016年7月13日
	 * @author MBG
	 */
	Map<String,String> getCustomHeaderMap();
	
	/**
	 * 克隆一个同样值的对象（主要用于内部调用其它动作）
	 * @return 克隆一个同样值的对象
	 * 2016年11月21日
	 * @author MBG
	 */
	IRequest clone();
	
	/**
	 * 获取指定的Cookie值
	 * @param key 主键
	 * @return 值
	 * 2017年3月1日
	 * @author MBG
	 */
	String getCookieValue(String key);
	
	/**
	 * 请求方式是否为get
	 * @return 请求方式是否为get
	 * 2017年5月1日
	 * @author MBG
	 */
	boolean isGet();
	
	/**
	 * 请求方式是否为POST
	 * @return 请求方式是否为POST
	 * 2017年5月1日
	 * @author MBG
	 */
	boolean isPost();
	
	/**
	 * 是否为内置服务器响应的请求
	 * @return 是否为内置服务器响应的请求
	 * 2017年8月31日
	 * @author MBG
	 */
	boolean isNativeServer();
	
	/**
	 * 获取请求URL中提交的参数容器
	 * @return 请求URL中提交的参数容器
	 * 2018年3月2日
	 * @author MBG
	 */
	Map<String,String> getUrlParameterMap();
	
	/**
	 * 从请求URL中获取指定的参数值
	 * @param key 参数主键
	 * @return    参数值
	 * 2018年3月2日
	 * @author MBG
	 */
	String getUrlParameter(String key);
}
