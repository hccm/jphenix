/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 页面反馈扩展接口
 * @author 刘虻
 * 2012-7-30 上午9:08:46
 */
@ClassInfo({"2014-06-13 14:55","页面反馈扩展接口"})
public interface IResponse extends HttpServletResponse {
	
	/**
	 * 将信息头放入返回对象
	 * @param headerMap 信息头容器
	 * 2016年7月13日
	 * @author MBG
	 */
	void putHeaderMap(Map<String,String> headerMap);
	
	
	/**
	 * 克隆一个同样值的对象（主要用于内部动作调用）
	 * @return 克隆一个同样值的对象
	 * 2016年11月21日
	 * @author MBG
	 */
	IResponse clone();
	
	
    /**
     * 返回错误信息，如果有的话
     * @return 错误信息
     * 2017年3月1日
     * @author MBG
     */
    String getErrorMsg();
    
    
	/**
	 * 获取核心类
	 * @return 核心类
	 * 2014年12月3日
	 * @author 马宝刚
	 */
	HttpServletResponse getKernel();
	
	/**
	 * 清除缓存
	 * 2017年4月28日
	 * @author MBG
	 */
	void clear();
	
	
    /**
     * 向浏览器输出错误信息，并跳转到指定的错误页面
     * 程序会解析错误页面中的动态内容
     * @param status          错误代码
     * @param req             页面请求（用于解析错误页面中的动态内容）
     * @throws IOException    异常
     * 2017年11月24日
     * @author MBG
     */
	void sendError(int status,IRequest req) throws IOException;

    /**
     * 向浏览器输出错误信息，并跳转到指定的错误页面
     * 程序会解析错误页面中的动态内容
     * @param status          错误代码
     * @param errorMsg        错误信息
     * @param req             页面请求（用于解析错误页面中的动态内容）
     * @throws IOException    异常
     * 2017年11月24日
     * @author MBG
     */
	void sendError(int status, String errorMsg,IRequest req) throws IOException;
}
