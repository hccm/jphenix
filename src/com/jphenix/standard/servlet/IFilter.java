/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.servlet;

import javax.servlet.FilterConfig;

import com.jphenix.servlet.filter.FilterExplorer;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 过滤器接口
 * 
 * 2019-06-15 增加了过滤器初始化方法
 * 
 * 
 * @author 刘虻
 * 2010-5-24 下午04:08:06
 */
@ClassInfo({"2019-06-15 17:24","过滤器接口"})
public interface IFilter {

	/**
	 * 获取排序索引，数值越小越先执行
	 * 刘虻
	 * 2010-5-25 下午03:15:01
	 * @return 排序索引
	 */
	int getIndex();
	
	/**
	 * 获取需要过滤的动作路径扩展名
	 * 多个扩展名用逗号分割
	 * 多个扩展名用半角逗号分割
	 * 扩展名前不用加半角聚号（.)
	 * 如果返回空，或者空字符串，说明需要过滤全部动作路径（不建议这么做）
	 * 如果需要过滤无扩展名的动作，用半角减号（-）标记
	 * @return 需要过滤的动作路径扩展名
	 * 2014年9月12日
	 * @author 马宝刚
	 */
	String getFilterActionExtName();
	
	/**
	 * 执行过滤
	 * 刘虻
	 * 2010-5-25 下午04:09:25
	 * @param req 页面请求
	 * @param resp 页面反馈
	 * @return 如果返回真，则不继续往下进行，直接跳过结束
	 * @throws Exception 执行发生异常
	 */
	boolean doFilter(IRequest req,IResponse resp)throws Exception;
	
	
	/**
	 * 执行初始化
	 * @param fe         过滤器管理类
	 * @param config     Servlet配置信息类
	 * @throws Exception 异常（如果初始化发生异常，则放弃不再使用）
	 * 2019年6月15日
	 * @author MBG
	 */
	void init(FilterExplorer fe,FilterConfig config) throws Exception;
}
