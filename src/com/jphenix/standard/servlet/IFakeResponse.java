/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年11月24日
 * V4.0
 */
package com.jphenix.standard.servlet;

import java.io.ByteArrayOutputStream;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 仿造的页面反馈对象接口
 * @author MBG
 * 2016年11月24日
 */
@ClassInfo({"2016-11-24 10:03","仿造的页面反馈对象接口"})
public interface IFakeResponse extends IResponse {

	
	/**
	 * 获取返回的信息数据流
	 * @return 返回的信息数据流
	 * 2016年11月25日
	 * @author MBG
	 */
	ByteArrayOutputStream getData();
}
