/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月9日
 * V4.0
 */
package com.jphenix.standard.servlet;

import java.io.InputStream;
import java.util.List;

import com.jphenix.servlet.common.HttpServletRequestImpl;
import com.jphenix.share.lang.SListMap;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 广播过滤器接口
 * 
 * 主要用在动作类中，调用过滤器中的方法
 * 
 * @author 马宝刚
 * 2014年9月9日
 */
@ClassInfo({"2014-09-09 16:33","广播过滤器接口"})
@BeanInfo({"broadcastfilter"})
public interface IBroadcastFilter {

    
    /**
     * 执行广播
     * 
     * 在动作入口，为获取提交数据前，一定要执行以下语句
     * request.setBuffered(); //设置缓存提交值
     * 
     * @param req
     * 2014年9月9日
     * @author 马宝刚
     */
    void broadcast(final HttpServletRequestImpl request);
    
    /**
     * 将当前的动作广播到指定服务器中
     * @param serverName 服务器名
     * @param request 页面请求
     * @return 返回读入流
     * @throws Exception 异常
     * 2014年9月16日
     * @author 马宝刚
     */
    InputStream broadcastOneReStream(String serverName,IRequest request) throws Exception;
    
    /**
     * 将当前的动作广播到指定服务器中
     * @param serverName 指定服务器
     * @param request 页面请求
     * @return 调用返回信息
     * 2014年9月16日
     * @author 马宝刚
     */
    byte[] broadcastOneReBytes(String serverName,IRequest request) throws Exception;
    
    
    /**
     * 广播一台服务器，并返回信息流
     * （可以用在获取指定服务器日志信息流）
     * @param serverName 服务器主键
     * @param action 动作路径
     * @param data 发送数据
     * @param contentType 内容类型
     * @param sendEnc 发送数据编码
     * @param request 页面请求对象
     * @return 服务器返回信息流
     * @throws Exception 异常
     * 2014年9月15日
     * @author 马宝刚
     */
    InputStream broadcastOneReStream(String serverName,String action,byte[] datas,IRequest request) throws Exception;
    
    /**
     * 广播到一台服务器中
     * @param serverName 服务器名
     * @param action 动作路径
     * @param data 发送数据
     * @param contentType 内容类型 0html  1xml  2json
     * @param sendEnc 发送内容编码
     * @param resEnc 返回内容编码
     * @param request 页面请求
     * @return 返回值
     * @throws Exception 异常
     * 2014年9月15日
     * @author 马宝刚
     */
    byte[] broadcastOneReBytes(String serverName,String action,byte[] datas,IRequest request) throws Exception;
    
    /**
     * 获取集群中其它服务器名序列（不包含本机服务器）
     * @param includeMe 是否包含本机服务器名
     * @return 其它服务器名序列
     * 2014年9月15日
     * @author 马宝刚
     */
    List<String> getServerNameList(boolean includeMe);
    
    /**
     * 获取本机服务器名
     * @return 本机服务器名
     * 2014年9月16日
     * @author 马宝刚
     */
    String getSelfServerName();
    
    /**
     * 获取本机根URL
     * @return 本机根URL
     * 2014年9月16日
     * @author 马宝刚
     */
    String getSelfBaseUtl();
    
    /**
     * 执行广播（执行前请确保不会出现循环调用）
     * @param action                动作路径
     * @param datas                 传入数据
     * @param contentType      内容类型
     * @param includeMe         广播范围中是否包含本机服务器
     * 2014年9月16日
     * @author 马宝刚
     */
    void broadcast(String action,byte[] datas,String contentType,boolean includeMe);
    
    
    /**
     * 获取指定服务器所在的组名
     * @param serverName 服务器名
     * @return 组名
     * 2015年10月26日
     * @author 马宝刚
     */
    String getServerGroup(String serverName);
    
    /**
     * 判断服务器名是否存在于集群服务器信息中
     * @param serverName 需要判断的服务器名
     * @return 是否存在
     * 2015年10月23日
     * @author 马宝刚
     */
    boolean checkServerName(String serverName);
    
    /**
     * 获取全部组名
     * @return
     * 2015年10月26日
     * @author 马宝刚
     */
    List<String> getGroupNameList();
    
    /**
     * 获取本机所属的组名
     * @return 本机所属的组名
     * 2015年10月26日
     * @author 马宝刚
     */
    String getSelfGroupName();
    
    /**
     * 获取本机接口url地址
     * @return 本机接口url地址
     * 2015年10月26日
     * @author 马宝刚
     */
    String getSelfServerUrl();
    
    
    /**
     * 通过组名返回该组服务器信息序列
     * @param groupName 组名
     * @return 该组服务器信息序列
     * 2015年10月26日
     * @author 马宝刚
     */
    SListMap<String> getServerGroups(String groupName);
    
    
    /**
     * 通过组名，获取该组中所有的服务器名序列
     * @param groupName 组名
     * @return 该组中所有的服务器名序列
     * 2015年10月26日
     * @author 马宝刚
     */
    List<String> getServerNameListByGroupName(String groupName);
    
    /**
     * 获取服务器对应的调用地址
     * @param serverName 服务名
     * @return 服务器调用地址
     * 2015年10月19日
     * @author 马宝刚
     */
    String getServerURL(String serverName);
}
