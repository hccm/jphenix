/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年11月8日
 * V4.0
 */
package com.jphenix.standard.propertie;

import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 需要设置参数到配置文件类中的类实例，需要实现该接口
 * 
 * 去掉了直接设置参数到配置文件类中，因为配置文件类发现配置文件发生变化，会重新初始化配置数据，那么
 * 后设置的数据就找不到了。采用这种方式可以避免这种问题
 * 
 * 
 * @author MBG
 * 2018年11月8日
 */
@ClassInfo({"2018-11-08 12:33","需要设置参数到配置文件类中的类实例，需要实现该接口"})
public interface IConfigSetter {

	/**
	 * 需要设置到配置信息类中的数据容器
	 * @return 需要设置到配置信息类中的数据容器
	 * 2018年11月8日
	 * @author MBG
	 */
	Map<String,String> getPropertyMap();
}
