/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.serialno;

import java.util.List;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 获得唯一序列号接口
 * 
 * 2019-06-26 重构了该序列号生成器
 * 
 * 
 * @author 刘虻
 * 2006-1-28  0:20:52
 */
@ClassInfo({"2019-06-26 10:04","获得唯一序列号接口"})
@BeanInfo({"sn"})
public interface ISN {
	
	/**
	 * 默认序列号头
	 */
	String DEFAULT_HEAD = "PX";
	
    /**
     * 获得当前序列号生成类编号
     * @author 刘虻
     * @return 当前列号生成类编号
     * 2006-1-28  0:23:09
     */
    String head();
    
    /**
     * 序列号生成器索引值
     * @return 当前服务器在集群中的位置
     * 2019年6月26日
     * @author MBG
     */
    String index(); 
    
    /**
     * 设置序列号生成器索引值
     * @param index 当前服务器在集群中的位置
     * 2019年6月26日
     * @author MBG
     */
    void index(String index);

    /**
     * 获取主键长度
     * @author 刘虻
     * @return 主键长度
     * 2006-7-20  11:34:59
     */
    int size();
    
    /**
     * 批量生成序列号
     * @author 刘虻
     * @param countInt 生成序列号数量
     * @return 返回唯一序列号数组
     * 2006-1-28  0:26:15
     */
    String[] getSIDs(int countInt);
    
    /**
     * 获取一组序列号
     * @author 刘虻
     * 2007-8-31下午08:23:58
     * @param countInt 序列号
     * @return 一组序列号
     */
    List<String> getSIDList(int countInt);
    
    /**
     * 获取一个新的主键
     * @return 一个新的主键
     * 2015年5月14日
     * @author 马宝刚
     */
    String getSID();
    
    /**
     * 获取一个新的主键
     * @return 一个新的主键
     * 2015年5月14日
     * @author 马宝刚
     */
    String sid();
}
