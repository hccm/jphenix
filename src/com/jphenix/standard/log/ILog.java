/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.log;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 日志输出接口
 * com.jphenix.share.log.interfaceclass.ILog
 * 
 * 2018-07-18 修改了日志输出相关方法
 * 2018-07-30 增加了写入自定义日志方法
 * 2019-03-06 增加了输出sql语句的方法（带是否输出日志，是否写日志参数）
 * 2019-03-15 增加了输出sql语句到自定义日志文件中的方法
 * 2020-08-20 原本整套程序是跑在默认编码格式中的，比如在windows下跑gbk格式的，后来升级tomcat后，需要强制跑在utf-8下，写日志文件的编码格式就错了，这回拆成控制台和写文件两个编码
 * 
 * @author 刘虻
 * 2009-12-3 下午03:45:06
 */
@ClassInfo({"2020-08-20 10:10","日志输出接口"})
@BeanInfo({"log"})
public interface ILog {
	
	/**
	 * 输出普通日志
	 * 刘虻
	 * 2009-12-3 下午03:45:34
	 * @param content 内容
	 */
	void log(Object content);
	
	/**
	 * 输出普通日志
	 * 刘虻
	 * 2009-12-3 下午03:45:34
   * @param title   日志提示标题
	 * @param content 内容
	 */
	void log(String title,Object content);

	/**
	 * 输出启动日志
	 * 刘虻
	 * 2009-12-3 下午03:45:55
	 * @param content 内容
	 */
	void startLog(Object content);

	/**
	 * 输出警告日志
	 * 刘虻
	 * 2009-12-3 下午04:43:25
	 * @param content 日志内容
	 * @param e 异常，或者放入调用类用来输出堆栈信息
	 */
	void warning(Object content,Object e);

	/**
	 * 输出数据库语句日志
	 * 刘虻
	 * 2009-12-3 下午04:43:41
	 * @param sql 数据库语句
	 */
	void sqlLog(Object sql);

	/**
	 * 输出错误日志
	 * 刘虻
	 * 2009-12-3 下午04:44:04
	 * @param content 日志内容
	 * @param e 异常，或者放入调用类用来输出堆栈信息
	 */
	void error(Object content,Object e);
	
	/**
	 * 设置方法调用前的时间
	 * 刘虻
	 * 2009-12-3 下午04:44:29
	 * @return 调用前的时间
	 */
	Object runBefore();
	
	/**
	 * 将程序运行时间写入日志
	 * 刘虻
	 * 2009-12-3 下午04:48:30
	 * @param executeTimeVo 时间对象
	 * @param title 标题
	 */
	void writeRuntime(Object executeTimeVo,String title);
	
	/**
	 * 获取输出到控制台日志编码格式
	 * 刘虻
	 * 2009-12-3 下午04:48:13
	 * @return 日志编码格式
	 */
	String getConsoleEncoding();
	
	/**
	 * 获取写入文件日志编码格式
	 * @return 日志编码格式
	 * 2020年8月20日
	 * @author MBG
	 */
	String getFileEncoding();
	
	/**
	 * 终止日志类
	 * 刘虻
	 * 2010-9-10 下午04:49:47
	 */
	void destroy();

  /**
   * 将所有日志屏蔽后，还需要在控制台显示一些量不大，又关键的日志信息
   * @param content 日志内容
   * 2014年6月17日
   * @author 马宝刚
   */
  void info(Object content);

  /**
   * 将所有日志屏蔽后，还需要在控制台显示一些量不大，又关键的日志信息
   * @param title   日志提示标题
   * @param content 日志内容
   * 2014年6月17日
   * @author 马宝刚
   */
  void info(String title,Object content);
    
  /**
    * 输出内部日志，该日志不会输出到控制台，也不会写入文件
    * 只会在设置了日志事件处理类，将日志信息输出到到日志事件
    * 处理类中
    * @param content 日志内容
    * 2014年4月17日
    * @author 马宝刚
    */
  void debug(Object content);

  /**
    * 输出内部日志，该日志不会输出到控制台，也不会写入文件
    * 只会在设置了日志事件处理类，将日志信息输出到到日志事件
    * 处理类中
    * @param title   日志提示标题
    * @param content 日志内容
    * 2014年4月17日
    * @author 马宝刚
    */
    void debug(String title,Object content);
    
  /**
   * 输出交易数据日志
   * @param content 交易数据
   * 2016年6月7日
   * @author MBG
   */
  void dataLog(Object content);

  /**
 	 * 临时输出日志（测试后需要删除）
 	 * @param content 日志内容
 	 * 2018年7月12日
 	 * @author MBG
 	 */
  void tlog(Object content);

  /**
 	 * 临时输出日志（测试后需要删除）
   * @param title   日志提示标题
 	 * @param content 日志内容
 	 * 2018年7月12日
 	 * @author MBG
 	 */
    void tlog(String title,Object content);

	/**
	 * 设置故障码
	 * @param code   故障码
	 * 2018年7月17日
	 * @author MBG
	 */
	void setAlertCode(String code);
	
	/**
	 * 写入自定义日志
	 * 需要在影子类中设置允许输出的文件头信息，否则日志不会输出
	 * 另外，自定义文件头名字不允许使用  log  或者以 data_ 开头的名字
	 * @param content  日志内容
	 * @param fileKey  自定义日志文件头
	 * 2018年7月30日
	 * @author MBG
	 */
	void olog(Object content,String fileKey);

	/**
	 * 写入自定义日志
	 * 需要在影子类中设置允许输出的文件头信息，否则日志不会输出
	 * 另外，自定义文件头名字不允许使用  log  或者以 data_ 开头的名字
   * @param title    日志提示标题
	 * @param content  日志内容
	 * @param fileKey  自定义日志文件头
	 * 2018年7月30日
	 * @author MBG
	 */
	void olog(String title,Object content,String fileKey);
	
	/**
	 * 输出sql语句日志
	 * @param sql       sql语句
	 * @param outSql    是否输出日志到控制台
	 * @param writeSql  是否将日志写入到文件
	 * 2019年3月6日
	 * @author MBG
	 */
	void sqlLog(Object sql,boolean outSql,boolean writeSql);
	
	/**
	 * 输出sql语句日志
	 * @param sql       sql语句
	 * @param outSql    是否输出日志到控制台
	 * @param fileKey   写入文件的文件名（不带扩展名）
	 * 2019年3月15日
	 * @author MBG
	 */
	void sqlLog(Object sql,boolean outSql,String fileKey);
}