/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.log;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;


/**
 * 日志影子类接口
 * 
 * 该类需要常驻内存，主要用来保存日志配置信息
 * 
 * 2018-07-18 修改了日志输出相关方法
 * 2018-07-30 增加了写入自定义日志方法
 * 2019-03-15 修改了输出日志到自定日志的传入参数顺序和业务逻辑
 * 2020-08-20 原本整套程序是跑在默认编码格式中的，比如在windows下跑gbk格式的，后来升级tomcat后，需要强制跑在utf-8下，写日志文件的编码格式就错了，这回拆成控制台和写文件两个编码
 * 
 * @author 刘虻
 * 2010-2-1 下午03:49:23
 */
@ClassInfo({"2020-08-20 10:10","日志影子类接口"})
@BeanInfo({"logshadow"})
public interface ILogShadow {

	/**
	 * 初始化
	 * 刘虻
	 * 2009-12-7 上午10:55:53
	 */
	void init();
	
	/**
	 * 终止
	 * 刘虻
	 * 2010-9-10 下午04:50:33
	 */
	void destroy();
	
	/**
	 * 获取当前时间
	 * 刘虻
	 * 2009-12-4 下午04:40:52
	 * @return 当前时间
	 */
	String now();
	
	/**
	 * 是否输出错误日志
	 * 刘虻
	 * 2010-2-1 下午04:14:52
	 * @return 是否输出错误日志
	 */
	boolean isOutError();
	
	/**
	 * 是否写入错误日志
	 * 刘虻
	 * 2010-2-1 下午04:16:15
	 * @return 是否写入错误日志
	 */
	boolean isWriteError();
	
	/**
	 * 是否输出sql语句
	 * 刘虻
	 * 2010-2-1 下午04:33:23
	 * @return 是否输出sql语句
	 */
	boolean isOutSql();
	
	/**
	 * 是否写入sql语句
	 * 刘虻
	 * 2010-2-1 下午04:33:46
	 * @return 是否写入sql语句
	 */
	boolean isWriteSql();
	
	/**
	 * 写日志
	 * 刘虻
	 * 2009-12-7 上午10:31:01
	 * @param content 日志内容
	 * @param showLog 是否显示日志
	 * @param writeLog 是否写入日志
	 * @param nativeLog 内部日志
	 */
	void write(String content,boolean showLog,boolean writeLog,boolean nativeLog);
	
	/**
	 * 写日志
	 * 刘虻
	 * 2010-8-24 上午11:09:33
	 * @param bytes 日志内容
	 */
	void write(byte[] bytes);
	
	/**
	 * 写日志
	 * 刘虻
	 * 2010-8-24 上午11:13:31
	 * @param buf 缓存
	 * @param off 起始位置
	 * @param len 长度
	 */
	void write(byte[] buf,int off,int len);
	
	/**
	 * 获取输出到控制台日志编码格式
	 * 刘虻
	 * 2009-12-4 上午11:26:35
	 * @return 编码格式
	 */
	String getConsoleEncoding();
	
	/**
	 * 获取写文件日志编码格式
	 * @return 编码格式
	 * 2020年8月20日
	 * @author MBG
	 */
	String getFileEncoding();
	
	/**
	 * 是否输出普通日志
	 * 刘虻
	 * 2010-2-1 下午04:17:51
	 * @return 是否输出普通日志
	 */
	boolean isOutLog();
	
	/**
	 * 是否写入普通日志
	 * 刘虻
	 * 2010-2-1 下午04:18:42
	 * @return 是否写入普通日志
	 */
	boolean isWriteLog();
	
	/**
	 * 是否输出启动日志
	 * 刘虻
	 * 2010-2-1 下午04:20:43
	 * @return 是否输出启动日志
	 */
	boolean isOutStart();
	
	/**
	 * 是否写入启动日志
	 * 刘虻
	 * 2010-2-1 下午04:21:36
	 * @return 是否写入启动日志
	 */
	boolean isWriteStart();
	
	/**
	 * 写入启动日志
	 * 刘虻
	 * 2009-12-7 上午11:18:05
	 * @param content 日志内容
	 * @param showLog 是否显示日志
	 * @param writeLog 是否写入日志
	 */
	void writeStart(
			String content,boolean showLog,boolean writeLog);
	
	/**
	 * 是否输出警告日志
	 * 刘虻
	 * 2010-2-1 下午04:22:46
	 * @return 是否输出警告日志
	 */
	boolean isOutWarning();
	
	/**
	 * 是否写入警告日志
	 * 刘虻
	 * 2010-2-1 下午04:23:26
	 * @return 是否写入警告日志
	 */
	boolean isWriteWarning();

    /**
     * 是否写入info日志
     * @return 是否写入info日志
     * 2014年6月17日
     * @author 马宝刚
     */
    boolean isWriteInfo();
    
	/**
     * 是否输出info日志
     * @return 是否输出info日志
     * 2014年6月17日
     * @author 马宝刚
     */
    boolean isOutInfo();
    
	/**
	 * 是否输出耗费时间日志
	 * 刘虻
	 * 2010-2-1 下午04:24:17
	 * @return 是否输出耗费时间日志
	 */
	boolean isOutRuntime();
	
	/**
	 * 是否写入运行时间
	 * 刘虻
	 * 2010-2-1 下午04:25:11
	 * @return 是否写入运行时间
	 */
	boolean isWriteRuntime();
    
    /**
     * 如果设置了日志事件处理类，只会输出到这里
     * @param content 日志内容
     * 2015年4月13日
     * @author 刘虻
     */
    void writeLogEvent(String content);
    
    
    /**
     * 手动执行备份日志
     * 2015年5月22日
     * @author 马宝刚
     */
    void backupLogFile();
    
    /**
     * 是否将内部调试信息写入日志文件
     * （不赞成写入日志文件，因为信息量巨大）
     * 注意，debug 设计的初衷是临时查看显示信息
     * 并非后期打开日志查找之前的信息。 如果设置成
     * 写入文件，实际上跟随log方法，如果log方法没
     * 设置成写入文件，那么debug即使设置成写入文件，
     * 实际上也不会写入文件
     * @return 是否写入
     * 2015年6月18日
     * @author 马宝刚
     */
    boolean isWriteDebug();
    
    
	/**
	 * 是否输出交易数据日志
	 * @return 是否输出交易数据日志
	 * 2016年6月7日
	 * @author MBG
	 */
	boolean isOutData();
	
	
	/**
	 * 是否写入交易数据日志
	 * @return 是否写入交易数据日志
	 * 2016年6月7日
	 * @author MBG
	 */
	boolean isWriteData();
	
	/**
	 * 设置是否输出交易数据日志
	 * @param outData 是否输出交易数据日志
	 * 2016年6月7日
	 * @author MBG
	 */
	void setOutData(boolean outData);
	
    /**
     * 设置是否将交易数据写入独立的日志文件
     * @param writeData 是否将交易数据写入独立的日志文件
     * 2016年6月7日
     * @author MBG
     */
    void setWriteData(boolean writeData);
    
	/**
	 * 输出（写入）交易数据日志
	 * @param content 日志内容
	 * @param showLog 是否显示日志到控制台
	 * @param writeLog 是否写入日志到独立文件
	 * 2016年6月7日
	 * @author MBG
	 */
	void writeData(String content,boolean showLog,boolean writeLog);
	
	/**
	 * 是否输出临时日志（测试后需要删除的日志）
	 * @return 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	boolean isOutTLog();
	
	/**
	 * 设置是否输出临时日志 （测试后需要删除的日志）
	 * @param outTLog 是否输出临时日志
	 * 2018年7月17日
	 * @author MBG
	 */
	void setOutTLog(boolean outTLog);
	
	/**
	 * 是否需要写入临时日志到文件（测试后需要删除的日志）
	 * @return 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	boolean isWriteTLog();
	
	/**
	 * 设置是否需要写入临时日志到文件 （测试后需要删除的日志）
	 * @param writeTLog 是否需要写入临时日志到文件
	 * 2018年7月17日
	 * @author MBG
	 */
	void setWriteTLog(boolean writeTLog);
	
	/**
	 * 设置故障码
	 * @param code  故障码
	 * 2018年7月17日
	 * @author MBG
	 */
	void setAlertCode(String code);
	
	/**
	 * 请求查看实施日志
	 * @param token 之前用的token，如果已经过期，则分配一个新的值，如果没过期，还用这个值
	 * @return 查看日志的token
	 * 2018年7月12日
	 * @author MBG
	 */
	String getToken(String token);
	
    /**
     * 写日志到指定文件
     * @param content 日志内容
     * @param showLog 是否输出到控制台
     * @param fileKey 文件名（不带扩展名）
     * 2018年7月27日
     * @author MBG
     */
    void write(String content,boolean showLog,String fileKey);
}
