/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月4日
 * V4.0
 */
package com.jphenix.standard.threadpool;

import java.util.List;

import com.jphenix.driver.threadpool.ThreadEle;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Running;

/**
 * 线程管理类
 * @author 马宝刚
 * 2014年7月4日
 */
@ClassInfo({"2014-07-04 12:47","线程管理类"})
@BeanInfo({"threadmanager"})
@Running({"1"})
public interface IThreadManager {

    /**
     * 将线程元素放入管理类
     * @param ele 线程元素
     * 2014年7月4日
     * @author 马宝刚
     */
    void addThread(ThreadEle ele);
    
    /**
     * 获取线程序列
     * @return 线程序列
     * 2014年7月4日
     * @author 马宝刚
     */
    List<ThreadEle> getThreadList();
    
    
    /**
     * 终止全部线程
     * 2014年7月4日
     * @author 马宝刚
     */
    void stop();
}
