/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月5日
 * V4.0
 */
package com.jphenix.standard.threadpool;

import java.util.List;

import com.jphenix.driver.threadpool.CrontabThread;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 定时任务服务控制接口
 * 
 * 2018-07-04 增加了获取指定定时任务的下一次执行时间
 * 
 * @author 马宝刚
 * 2014年7月5日
 */
@ClassInfo({"2018-07-04 16:12","定时任务服务控制接口"})
@BeanInfo({"crontab_service"})
public interface ICrontabService {

    /**
     * 获取定时任务线程序列
     * @return 定时任务线程序列
     * 2014年7月5日
     * @author 马宝刚
     */
    List<CrontabThread> getCrontabElement();
    
    /**
     * 设置全部暂停执行或者恢复继续执行
     * @param disabled true暂停  false继续执行
     * 2014年7月5日
     * @author 马宝刚
     */
    void setDisabled(boolean disabled);
    
    /**
     * 停止全部任务
     * 2014年7月5日
     * @author 马宝刚
     */
    void stop();

    
    /**
     * 启动服务
     * @throws Exception 异常
     * 2014年7月4日
     * @author 马宝刚
     */
    void start() throws Exception;
    
    
    /**
     * 获取指定定时任务类上次执行时间
     * @param beanId 定时任务类主键
     * @return 上次执行时间
     * 2016年5月9日
     * @author MBG
     */
    String getLastExecuteDate(String beanId);
    
    /**
     * 获取指定定时任务的下次执行时间
     * @param beanId 定时任务类主键
     * @return 下次执行时间
     * 2018年7月4日
     * @author MBG
     */
    String getNextExecuteDate(String beanId);
    
    /**
     * 获取受控的定时任务类主键序列
     * @return 受控的定时任务类主键序列
     * 2016年5月9日
     * @author MBG
     */
    List<String> getCrontabIdList();
    
    
    
    /**
     * 指定任务是否正在运行中
     * @param beanId 任务类主键
     * @return 是否在运行中
     * 2016年5月26日
     * @author MBG
     */
    boolean isRunning(String beanId);
}
