/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年10月20日
 * V4.0
 */
package com.jphenix.standard.threadpool;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.docs.Register;

/**
 * 定时任务元素接口
 * 
 * 2020-01-07 增加了是否允许执行该定时任务的开关
 * 
 * 
 * @author 马宝刚
 * 2014年10月20日
 */
@ClassInfo({"2020-01-07 09:38","定时任务元素接口"})
@Register({"icrontabbean"})
public interface ICrontabBean {

    
    /**
     * 获取定时间隔时间信息
     * @return 定时间隔时间信息
     * 2014年10月20日
     * @author 马宝刚
     */
    String getTimeInfo();
    
    /**
     * 是否允许执行该定时任务
     * @return 是否允许执行该定时任务
     * 2020年1月7日
     * @author MBG
     */
    boolean enabled();
    
    /**
     * 设置是否允许指定该定时任务
     * @param isEnabled 是否允许指定该定时任务
     * 2020年1月7日
     * @author MBG
     */
    void enabled(boolean isEnabled);
    
    /**
     * 需要执行的方法
     * 2014年10月20日
     * @throws Exception 异常
     * @author 马宝刚
     */
    void run() throws Exception;
}
