/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.objectcase;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 信息池元素
 * @author 刘虻
 * 2006-4-14  14:35:11
 */
@ClassInfo({"2014-06-13 14:51","信息池元素"})
@BeanInfo({"objectcasemanager"})
public interface IObjectCaseManager {
	
    /**
     * 信息池在删除一个消息元素之前调用的方法
     * @author 刘虻
     * @param blogName 块名
     * @param key 对象主键
     * @param kernel 核心类实例
     * 2006-4-14  14:37:10
     */
    void beforeDropElement(String blogName,Object key,Object kernel);
    
    /**
     * 取消正在处理并且将要设置结果值的动作
     * @author 刘虻
     * @param blogName 块名
     * @param key 指定取消的对象主键
     * @param kernel 核心类实例
     * 2006-4-15  17:14:22
     */
    void onCancelSetObject(String blogName,Object key,Object kernel);
}
