/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.objectcase;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 对象持久化容器
 * @author 刘虻
 * 2006-1-29  22:33:08
 * 目前正在测试中，感觉没什么用处
 */
@ClassInfo({"2014-06-13 14:51","对象持久化容器"})
@BeanInfo({"objectcase"})
public interface IObjectCase {

    /**
     * 根据主键获得持久化对象
     * @author 刘虻
     * @param blogName 块名
     * @param key 对应的主键
     * @return 对应的对象
     * @throws Exception 获得对象发生异常
     * 2006-1-29  22:35:11
     */
    Object getObject(String blogName,String key) throws Exception;
    
    /**
     * 清空容器内容
     * @author 刘虻
     * 2007-1-4下午03:22:42
     */
    void clearAll();
    
    /**
     * 清空指定块中的内容
     * @author 刘虻
     * 2007-5-14上午11:32:18
     * @param blogName 块名
     */
    void clear(String blogName);
    
    /**
     * 设置对象
     * @author 刘虻
     * @param blogName 块名
     * @param key 对象主键
     * @param value 对象值
     * @throws Exception 执行发生异常
     * 2006-1-29  22:36:00
     */
    void setObject(
    		String blogName
    		,String key
    		,Object value) throws Exception;
    
    /**
     * 从持久化对象容器中移出一个对象
     * @author 刘虻
     * @param blogName 块名
     * @param key 对应对象的主键 
     * @return 返回当前类实例
     * 2006-1-29  23:14:46
     */
    void removeObject(String blogName,String key);
    
    /**
     * 是否读取过指定值
     * @author 刘虻
     * @param blogName 块名
     * @param key 主键
     * @return true读取过  false未读取过
     * 2006-4-1  11:49:07
     */
    boolean isHaveNew(String blogName,String key);
    
    /**
     * 设置信息池管理类
     * @author 刘虻
     * @param objectCaseManager 信息池管理类
     * 2006-4-14  14:49:24
     */
    void setObjectManager(IObjectCaseManager objectCaseManager);
    
    /**
     * 获取信息池管理类
     * @author 刘虻
     * @return 信息池管理类
     * 2006-4-14  14:52:27
     */
    IObjectCaseManager getObjectManager();
    
    /**
     * 刷新指定对象值，以免超时释放
     * @author 刘虻
     * 2007-5-14下午03:16:31
     * @param blogName 块名
     * @param key 对象主键
     * @return true刷新成功  false不存在此对象
     */
    boolean refreshObject(String blogName,String key);
}
