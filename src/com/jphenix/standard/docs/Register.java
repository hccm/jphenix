/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注册目标声明注释                                         <br />
 *                                                                      <br />
 * 返回信息数组：                                             <br />
 *                                                                       <br />
 *                              0: 注册目标类主键            <br />
 *                                                                       <br />
 * @author 马宝刚
 * 2014年6月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Register {

    /**
     * 获取注释值数组
     * @return   0: 注册目标类主键
     * 
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
