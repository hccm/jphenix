/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 脚本类注释信息
 * 
 * 2018-08-06 整理了代码格式
 * 2018-11-19 增加了创建时间
 * 2019-01-11 增加了扩展库路径 21
 * 2019-01-16 又他妈取消了扩展库路径21，改成用ExtLib注释类保存
 * 2019-01-21 去掉了isStatic 索引3  属性，从来就没这么用过
 * 2020-09-09 增加了声明变量是否都是类变量27
 * 
 * @author 马宝刚
 * 2015年11月25日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface ScriptInfo {
    
	/**
     * 获取注释值数组
     * @return
     * 返回信息数组：                                  <br />
     *                                                 <br />
     *                  0:  脚本类加载器版本号         <br />
     *                  1:  id                         <br />
     *                  2:  className                  <br />
     *                  3:  initMethodName             <br />
     *                  4:  afterStartMethodName       <br />
     *                  5:  destoryMethodName          <br />
     *                  
     *                  6:  registerID                 <br />
     *                  7:  isShadow                   <br />
     *                  8:  disabled                   <br />
     *                  9:  singletonLevel             <br />
     *                 10:  classFilePath              <br />
     *                 
     *                 11:  classFileVer               <br />
     *                 12:  subPath                    <br />
     *                 13:  classPath                  <br />
     *                 14:  title                      <br />
     *                 15:  actionSpecialCode          <br />
     *                 
     *                 16:  noOutLog                   <br />
     *                 17:  isParent                   <br />
     *                 18:  parentBeanID               <br />
     *                 19:  interfaceClasspath         <br />
     *                 20:  isDbTransaction            <br />
     *                 
     *                 21:  independentClass           <br />
     *                 22:  isSync                     <br />
     *                 23:  clusterMasterTaskRun       <br />
     *                 24:  clusterCallOnly            <br />
     *                 25:  enabledActionCharge        <br />
     *                 
     *                 26:  createTime                 <br />
     *                 27:  isClassVar                 <br />
     *                                                                                                                                                                                                   
     * 2015年11月25日
     * @author 马宝刚
     */
    String[] value();
    
    /**
     * 获取元素数量
     */
    int length = 28;
}
