/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 通用类属性解析
 *
 * 返回信息数组：
 *                      0: 版本信息（YYYY-MM-DD HH:mm） 不足位数前面补零
 *                      1: 类文字说明
 *                      
 * 2019-01-24 完善了代码格式
 * 
 * @author 马宝刚
 * 2014年6月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface ClassInfo {

    
    /**
     * 获取注释值数组
     * @return   0: 版本信息（YYYY-MM-DD HH:mm） 不足位数前面补零
     *           1: 类文字说明
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
