/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类加载器管理的类注释信息
 *
 * 返回信息数组：
 *
 *                  0:  类主键
 *                  1:  是否为父类  true是父类  false不是父类
 *                  2:  父类主键，在配置文件中有一段父类配置，在此写入对应的父类主键，可以使用相同的那段配置
 *                  3:  父类介绍，说明父类的作用
 *                  4：   是否允许在jar内部加载  1允许在jar内部加载
 *                  5：   父类类名（不包含包路径，即不带扩展名的类文件名。因为包路径都是固定的）
 *                  
 * 2019-01-24 整理了一下代码格式
 *                                                                                                                                                                                                  
 * @author 马宝刚
 * 2014年6月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface BeanInfo {

    
    /**
     * 获取注释值数组
     * @return
     *
     *                  0:  类主键
     *                  1:  是否为父类  true是父类  false不是父类
     *                  2:  父类主键，在配置文件中有一段父类配置，在此写入对应的父类主键，可以使用相同的那段配置
     *                  3:  父类介绍，说明父类的作用            
     *                  4:  是否允许在jar内部加载  1允许在jar内部加载
     *                  5： 父类类名（不包含包路径，即不带扩展名的类文件名。因为包路径都是固定的）
     *
     *                                                                                                                                                                        
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
