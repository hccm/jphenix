/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类运行时注释信息                                                                                                                             <br />
 *                                                                                                                                                           <br />
 *      返回的信息数组：                                                                                                                        <br />
 *                                                                                                                                                            <br />
 *                  0: 是否常驻内存，以及优先初始化顺序   0不常驻内存     1~无限大     优先级由低到高   	  <br />
 *                  		通常  1功能级（其他常驻内存服务在初始化时不会引用该服务）也称最终级					<br />
 *                  				 2外围服务级（在开发项目时，确定其它常驻内存服务会调用该服务）				<br />
 *                  			     3内核服务级 （开发框架内部的常驻内存服务，并确定其它常驻内存服务要调用该服务  <br />
 *                  				 99内核级 （框架内部类加载器核心常驻内存类，并在初始化时不会调用其它常驻内存服务)                        <br />
 *                  1: 类加载器全部加载完后，调用的方法                                                                                <br />
 *                  2:  类加载器加载完当前信息后，调用的方法（此时类加载器并未加载完全部的类）             <br />
 *                  3: 类加载器终止前，调用的方法                                                                                            <br />
 *                                                                                                                                                               <br />
 * @author 马宝刚
 * 2014年6月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Running {

    
    /**
     * 获取注释值数组
     * @return   <br />
     *                  0: 是否常驻内存                                                                                                                               <br />
     *                  1: 类加载器全部加载完后，调用的方法                                                                                             <br />
     *                  2:  类加载器加载完当前信息后，调用的方法（此时类加载器并未加载完全部的类）                         <br />
     *                  3: 类加载器终止前，调用的方法                                                                                                        <br />
     * 
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
