/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月3日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * 影子类注释                                                                            <br />
 *                                                                                               <br />
 *  返回值数组：                                                                        <br />
 *                                                                                               <br />
 *                  0:  是否为影子实体类  true 是  false 否                  <br />
 *                                                                                               <br />
 * @author 马宝刚
 * 2014年6月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Shadow {

    
    /**
     * 获取注释值数组
     * @return   0: 是否为影子实体类  true 是  false 否
     * 
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
