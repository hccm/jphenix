/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月16日
 * V4.0
 */
package com.jphenix.standard.docs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加载扩展类相对路径（相对于 WEB-INF/ext_lib）
 * 
 * 返回信息数组：相对路径元素
 * @author MBG
 * 2019年1月16日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface ExtLib {
	
    /**
     * 获取注释值数组
     * @return 相对路径元素（路径开头可以不用路径分隔符）
     *         相对于WEB-INF/ext_lib文件夹
     *         例如：["exchange_1.2","jmail_1.6.0"]
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
