/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年3月3日
 * V4.0
 */
package com.jphenix.standard.docs;

/**
 * 在当前脚本中，在编译当前脚本时
 * 需要关联其它脚本类。数组为其它脚本
 * 类的类主键
 * @author 马宝刚
 * 2016年3月3日
 */
public @interface ScriptBuild {

    
    /**
     * 获取注释值数组
     * @return 类主键数组                                                                                                                                                          
     * 2014年6月3日
     * @author 马宝刚
     */
    String[] value();
}
