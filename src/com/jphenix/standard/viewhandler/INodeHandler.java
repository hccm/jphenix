/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.viewhandler;

import java.io.File;
import java.io.InputStream;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 格式化文本方法接口
 * @author 刘虻
 * 2006-9-15下午02:26:55
 */
@ClassInfo({"2017-11-03 16:03","格式化文本方法接口"})
public interface INodeHandler extends IViewHandler {

	/**
	 * 设置文件路径
	 * @author 刘虻
	 * 2006-10-22下午05:48:47
	 * @param filePath 文件路径
	 * @return 返回当前类实例
	 * @exception 解析内容时发生异常
	 */
	INodeHandler setFilePath(
			String filePath) throws Exception;
	
	/**
	 * 设置节点文件对象
	 * 刘虻
	 * 2010-6-8 下午06:13:02
	 * @param file 文件对象
	 * @return 节点对象
	 * @throws Exception 执行发生异常
	 */
	INodeHandler setFile(File file) throws Exception;
	
	/**
	 * 获取节点文件路径
	 * @author 刘虻
	 * 2006-10-22下午05:49:12
	 * @return 文件路径
	 */
	String getFilePath();
	
	
	/**
	 * 将节点内容信息写入文件
	 * @author 刘虻
	 * 2007-3-16下午05:14:37
	 * @param filePath 文件路径（包含文件名）
	 * @param outEncoding 输出文件编码
	 * @return 返回当前类实例
	 * @throws Exception 执行发生异常
	 */
	INodeHandler writeFile(String filePath,String outEncoding) throws Exception;
	
	
	/**
	 * 将节点内容信息写入当前文件中
	 * @author 刘虻
	 * 2007-3-16下午05:15:25
	 * @return 返回当前类实例
	 * @throws Exception 执行发生异常
	 */
	INodeHandler writeFile() throws Exception;
	
	
	/**
	 * 设置节点内容
	 * @author 刘虻
	 * 2006-10-22下午05:51:39
	 * @param nodeBody 节点内容
	 * @return 返回当前类实例
	 * @exception 解析内容时发生异常
	 */
	INodeHandler setNodeBody(String nodeBody) throws Exception;
	
	
	/**
	 * 将HTML段作为子节点对象插入当前节点中
	 * @param nodeBody HTML内容
	 * @return 当前类实例
	 * @throws Exception 异常
	 * 2017年11月3日
	 * @author MBG
	 */
	INodeHandler addNodeBody(String nodeBody) throws Exception;
	
	
	/**
	 * 将指定子页面作为对象插入到当前节点对象中
	 * @param filePath 子页面路径（绝对路径）
	 * @return 子页面对象
	 * @throws Exception 异常
	 * 2017年11月8日
	 * @author MBG
	 */
	INodeHandler addPath(String filePath) throws Exception;
	
	
	/**
	 * 将指定子页面作为对象插入到当前节点对象中
	 * @param filePath 子页面文件对象
	 * @return 子页面对象
	 * @throws Exception 异常
	 * 2017年11月8日
	 * @author MBG
	 */
	INodeHandler addFile(File file) throws Exception;
	
	/**
	 * 设置时间戳
	 * 刘虻
	 * 2010-8-5 下午03:03:52
	 * @param ts 时间戳
	 */
	void setTS(long ts);
	
	/**
	 * 获取时间戳
	 * 刘虻
	 * 2010-8-5 下午03:04:03
	 * @return 时间戳
	 */
	long getTS();
	
	/**
	 * 设置读入流
	 * @param is 读入流
	 * @param filePath 路径信息（用来记录来源，输出到日志用）
	 * @return			当前类实例
	 * @throws Exception 异常
	 * 2014-3-14
	 * @author 马宝刚
	 */
	INodeHandler setStream(InputStream is,String filePath) throws Exception;
	
	
	/**
	 * 设置HTML内容
	 * @param bytes				字节数组
	 * @return						当前类实例
	 * @throws Exception		异常
	 * 2014-3-14
	 * @author 马宝刚
	 */
	INodeHandler setBytes(byte[] bytes) throws Exception;
	
	
	/**
	 * 如果解析的文件，可以返回这个文件的MD5值
	 * @return MD5值
	 * 2017年10月16日
	 * @author MBG
	 */
	String getMd5();
	
	/**
	 * 将指定HTML内容设置到当前节点中 同：setNodeBody
	 * @param nodeBody html内容
	 * @return 当前类实例
	 * @throws Exception 异常
	 * 2017年11月8日
	 * @author MBG
	 */
	INodeHandler html(String nodeBody) throws Exception;
	
	/**
	 * 将指定HTML内容设置到当前节点中 同：setNodeBody
	 * @param nodeBody html内容
	 * @return 当前类实例
	 * @throws Exception 异常
	 * 2017年11月8日
	 * @author MBG
	 */
	INodeHandler xml(String nodeBody) throws Exception;
}
