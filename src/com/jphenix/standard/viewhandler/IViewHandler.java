/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.viewhandler;

import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;
import com.jphenix.standard.lang.IPrintStream;

/**
 * 视图数据包操作类接口
 * 
 * 注意：属性名都为小写
 * 
 * 抽象节点：节点为无效节点（无意义节点）
 * 
 * 抽象节点意义：通常会出现无主节点的多同级节点，需要用一个抽象节点作为它们的主节点
 * 
 * 2018-12-05 增加了在其子节点中（不包含子节点的子节点，只在一级子节点中查找）获取指定节点名的第一个子节点
 * 2019-07-02 增加了fnnc方法，获取第一个符合条件的子节点，如果不存在，则创建该子节点
 * 2019-08-02 增加了ac(节点名,节点文本) 方法，通常用于拼装XML报文
 * 
 * @author 刘虻
 * 2006-9-15下午02:26:55
 */
@ClassInfo({"2019-08-02 11:30","视图数据包操作类接口"})
public interface IViewHandler extends ICloneable {

	/**
	 * 可操作节点
	 */
	int NODE_TYPE_DEAL = 1;
	
	/**
	 * 文本节点
	 */
	int NODE_TYPE_TEXT = 0;
	
	/**
	 * 节点主键属性名
	 */
	String NODE_ATTRIB_ID_KEY = "id";
	
	/**
	 * 节点插入数据主键属性名
	 */
	String NODE_ATTRIB_PK_KEY = "pk";
		
	/**
	 * 节点名称属性名
	 */
	String NODE_ATTRIB_NAME_KEY = "name";
	
	/**
	 * 获取节点名 nn()
	 * @author 刘虻
	 * 2006-9-15下午02:55:47
	 * @return 节点名
	 */
	String getNodeName();
	
	/**
	 * 获取节点名 getNodeName()
	 * 刘虻
	 * 2010-8-1 上午10:18:20
	 * @return 节点名
	 */
	String nn();
	
	/**
	 * 设置节点名 nn(String nodeName)
	 * @author 刘虻
	 * 2006-9-15下午02:56:21
	 * @param nodeName 节点名
	 * @return 返回当前类实例
	 */
	IViewHandler setNodeName(String nodeName);
	
	/**
	 * 设置节点名 setNodeName(String nodeName)
	 * 刘虻
	 * 2010-8-1 上午10:20:08
	 * @param name 节点名
	 * @return 返回当前类实例
	 */
	IViewHandler nn(String name);
	
	/**
	 * 获取属性值 a(String key)
	 * @author 刘虻
	 * 2006-9-15下午02:57:22
	 * @param key 属性主键 均为小写
	 * @return 属性值
	 */
	String getAttribute(String key);
	
	/**
	 * 获取属性值  getAttribute(String key)
	 * 刘虻
	 * 2010-8-1 上午10:14:29
	 * @param key 属性主键 均为小写
	 * @return 属性值
	 */
	String a(String key);
	
	/**
	 * 增加属性值 a(String key,String value)
	 * @author 刘虻
	 * 2006-9-15下午02:57:45
	 * @param key 属性主键 均为小写
	 * @param value 属性值
	 * @return 返回当前类实例
	 */
	IViewHandler setAttribute(String key,String value);
	
	/**
	 * 增加属性值 setAttribute(String key,String value)
	 * 刘虻
	 * 2010-8-1 上午10:14:55
	 * @param key 属性主键 均为小写
	 * @param value 属性值
	 * @return 返回当前类实例
	 */
	IViewHandler a(String key,String value);
	
	/**
	 * 在原有属性值中增加新的值 全称：addAttribute(key,value);
	 * 注意：需要新增加值的属性主键为class，并且该属性已经存在值
	 *           新增值时，会自动在前面增加一个空格，如果是style会自动
	 *           增加个分号
	 * @param key    指定属性值
	 * @param value 需要添加的属性值
	 * @return  当前节点属性
	 * 2017年5月9日
	 * @author MBG
	 */
	IViewHandler aa(String key,String value);
	
	/**
	 * 移出指定属性 ra(String key)
	 * @author 刘虻
	 * 2006-9-15下午02:58:15
	 * @param key 属性主键 均为小写
	 * @return 移出后的属性值
	 */
	String removeAttribute(String key);
	
	/**
	 * 移出指定属性 removeAttribute(String key)
	 * 刘虻
	 * 2010-8-5 下午03:20:03
	 * @param key 属性主键 均为小写
	 * @return 移出后的属性值
	 */
	String ra(String key);

	/**
	 * 移除指定属性值中指定的部分 全称：removeAttribute
	 * 注意：属性值不分大小写进行匹配
	 * @param key            指定属性主键
	 * @param value         指定属性值中的部分
	 * @return                  移除后的值
	 * 2017年5月9日
	 * @author MBG
	 */
	String ra(String key,String value);
	
	/**
	 * @deprecated 比较危险，一旦加错输出结果可能不对
	 * 并且是否加错也不太好判断
	 * 
	 * 将指定属性的属性值加入分割符号
	 * @author 刘虻
	 * 2006-10-31下午06:29:52
	 * @param key 属性名 均为小写
	 * @param sign 属性值
	 * @return 返回当前类实例
	 */
	IViewHandler addAttributeSign(String key,String sign);
	
	/**
	 * 通过属性名称获取属性值分割符号
	 * @author 刘虻
	 * 2006-10-31下午06:26:03
	 * @param key 属性名 均为小写
	 * @return 分割符号
	 */
	String getAttributeSign(String key);
	
	/**
	 * 是否存在此参数
	 * @author 刘虻
	 * 2006-9-15下午02:59:53
	 * @param property 参数名
	 * @return true存在参数
	 */
	boolean hasProperty(String property);
	
	/**
	 * 是否存在此属性
	 * @author 刘虻
	 * 2006-10-25下午05:15:21
	 * @param key 属性主键 均为小写
	 * @return true存在
	 */
	boolean hasAttributeKey(String key);
	
	/**
	 * 是否存在此值的属性
	 * @author 刘虻
	 * 2006-10-25下午05:15:58
	 * @param value 属性值
	 * @return true存在
	 */
	boolean hasAttributeValue(String value);
	
	/**
	 * 获得属性名容器
	 * @author 刘虻
	 * 2006-11-3下午04:49:35
	 * @return 属性名容器 此容器中的属性名称是区分大小写的
	 */
	List<String> getAttributeKeyList();
	
	
	/**
	 * 增加指定参数 p(String property)
	 * @author 刘虻
	 * 2006-9-15下午03:00:57
	 * @param property 参数名
	 * @return 返回当前类实例
	 */
	IViewHandler addProperty(String property);
	
	
	/**
	 * 增加指定参数 addProperty(String property)
	 * 刘虻
	 * 2010-8-1 上午10:15:34
	 * @param property 参数名
	 * @return 返回当前类实例
	 */
	IViewHandler p(String property);
	
	/**
	 * 移出指定参数 rp(String property)
	 * @author 刘虻
	 * 2006-9-15下午03:01:29
	 * @param property 参数名
	 * @return 返回当前类实例
	 */
	IViewHandler removeProperty(String property);
	
	/**
	 * 移出指定参数  removeProperty(String property)
	 * 刘虻
	 * 2010-8-1 上午10:16:02
	 * @param property 参数名
	 * @return 返回当前类实例
	 */
	IViewHandler rp(String property);
	
	/**
	 * 获取参数容器
	 * @author 刘虻
	 * 2006-9-15下午03:02:08
	 * @return 参数容器
	 */
	List<String> getPropertyList();
	
	/**
	 * 获取当前节点下的所有子节点序列（包含文本节点） c()
	 * @author 刘虻
	 * 2006-10-21下午12:57:32
	 * @return 当前节点下的所有子节点序列
	 */
	List<IViewHandler> getChildNodes();
	
	/**
	 * 获取当前节点下的所有子节点序列（包含文本节点） getChildNodes()
	 * 刘虻
	 * 2010-8-1 上午10:16:35
	 * @return 当前节点下的所有子节点序列
	 */
	List<IViewHandler> c();
	
	/**
	 * 获取第一个子节点
	 * @author 刘虻
	 * 2007-3-18下午04:01:47
	 * @return 第一个子节点
	 * @return 返回当前类实例
	 */
	IViewHandler getFirstChildNode();
	
	/**
	 * 获取第一个非文本节点 缩写：fc();
	 * @author 刘虻
	 * 2007-3-18下午09:36:40
	 * @return 第一个非文本节点
	 * @return 返回当前类实例
	 */
	IViewHandler getFirstChildDealNode();
	
	/**
	 * 获取第一个非文本节点 全称：getFirstChildDealNode();
	 * @author 刘虻
	 * 2007-3-18下午09:36:40
	 * @return 第一个非文本节点
	 * @return 返回当前类实例
	 */
	IViewHandler fc();
	
	/**
	 * 获取最后一个子节点
	 * @author 刘虻
	 * 2007-3-18下午04:02:21
	 * @return 最后一个子节点
	 * @return 返回当前类实例
	 */
	IViewHandler getLastChildNode();
	
	/**
	 * 获取最后一个可操作子节点
	 * @author 刘虻
	 * 2007-3-18下午09:37:14
	 * @return 最后一个可操作子节点
	 */
	IViewHandler getLastChildDealNode();
	
	
	/**
	 * 获取指定位置的子节点
	 * @author 刘虻
	 * 2007-3-18下午04:02:33
	 * @param index 节点序号
	 * @return 指定位置的子节点
	 */
	IViewHandler getChildNode(int index);
	
	/**
	 * 获取当前节点下所有可操作的节点序列（省略文本节点）
	 * @author 刘虻
	 * 2006-10-21下午01:15:51
	 * @return 当前节点下所有可操作的节点序列
	 */
	List<IViewHandler> getChildDealNodes();
	
	/**
	 * 获取一个刚新增的节点
	 * 刘虻
	 * 2013-1-5 下午12:07:25
	 * @param nodeName 节点名
	 * @return 刚新增的节点
	 */
	IViewHandler getNewChild(String nodeName);
	
	/**
	 * 取当前节点下的符合id值的节点序列 cid(String idValue)
	 * @author 刘虻
	 * 2006-10-21下午01:17:05
	 * @param idValue 节点主键值
	 * @return 符合条件的子节点序列
	 */
	List<IViewHandler> getChildNodesByID(String idValue);
	
	/**
	 * 取当前节点下的符合id值的节点序列 getChildNodesByID(String idValue)
	 * 刘虻
	 * 2010-9-17 下午07:01:12
	 * @param idValue 节点主键值
	 * @return 符合条件的子节点序列
	 */
	List<IViewHandler> cid(String idValue);
	
	/**
	 * 获取当前节点下符合pk值的节点序列  cpk
	 * @author 刘虻
	 * 2008-12-10上午11:39:45
	 * @param pkValue 节点主键值
	 * @return 符合条件的子节点序列
	 */
	List<IViewHandler> getChildNodesByPK(String pkValue);
	
	/**
	 * 获取当前节点下符合pk值的节点序列  getChildNodesByPK
	 * 刘虻
	 * 2012-6-21 下午3:43:55
	 * @param pkValue
	 * @return
	 */
	List<IViewHandler> cpk(String pkValue);
	
	/**
	 * 获取当前节点下的符合name属性值的节点序列
	 * @author 刘虻
	 * 2007-4-18下午01:35:59
	 * @param nameValue  name属性值
	 * @return 符合条件的子节点序列
	 */
	List<IViewHandler> getChildNodesByName(String nameValue);
	
	/**
	 * 获取当前节点下的符合节点名称的节点序列
	 *  缩写名：cnn
	 * @author 刘虻
	 * 2007-4-18下午02:01:58
	 * @param nodeName 节点名称
	 * @return 当前节点下的符合节点名称的节点序列
	 */
	List<IViewHandler> getChildNodesByNodeName(String nodeName);
	
	/**
	 * 获取当前节点下所有符合指定属性值的节点序列
	 * @author 刘虻
	 * 2007-3-19上午10:34:50
	 * @param attributeKey 属性主键
	 * @param attributeValue 属性值
	 * @return 当前节点下所有符合指定属性值的节点序列
	 */
	List<IViewHandler> getChildNodesByAttribute(
			String attributeKey,String attributeValue);
	
	/**
	 * 获取当前节点下的第一个符合id值的节点类 fcid(String idValue)
	 * @author 刘虻
	 * 2006-10-21下午01:18:19
	 * @param idValue 节点主键
	 * @return 当前节点下的第一个符合id值的节点类
	 */
	IViewHandler getFirstChildNodeByID(String idValue);
	
	/**
	 * 获取当前节点下的第一个符合id值的节点类 getFirstChildNodeByID(String idValue)
	 * 刘虻
	 * 2010-8-1 上午10:17:48
	 * @param idValue 节点主键
	 * @return 当前节点下的第一个符合id值的节点类
	 */
	IViewHandler fcid(String idValue);
	
	
	/**
	 * 将id为指定值的节点克隆后，将源节点移除
	 * 刘虻
	 * 2012-6-21 下午4:40:52
	 * @param idValue 节点主键
	 * @return 克隆后的值
	 */
	IViewHandler mcid(String idValue);
	
	
	/**
	 * 获取当前节点下的第一个符合pk值的节点类
	 * @author 刘虻
	 * 2008-12-10上午11:40:32
	 * @param pkValue 节点主键值
	 * @return 当前节点下的第一个符合pk值的节点类
	 */
	IViewHandler getFirstChildNodeByPK(String pkValue);
	
	/**
	 * 获取当前节点下第一个符合name值的节点类
	 * @author 刘虻
	 * 2007-4-18下午02:00:00
	 * @param nameValue name属性值
	 * @return 当前节点下的第一个符合id值的节点类
	 */
	IViewHandler getFirstChildNodeByName(String nameValue);
	
	/**
	 * 获取当前节点下第一个符合节点名称的节点类
	 * 缩写名方法  fnn
	 * @author 刘虻
	 * 2007-4-18下午02:01:01
	 * @param nodeName 节点名
	 * @return 前节点下第一个符合节点名称的节点类
	 */
	IViewHandler getFirstChildNodeByNodeName(String nodeName);
	
	/**
	 * 获取当前节点下第一个符合指定属性值的节点 简称：fcna
	 * @author 刘虻
	 * 2007-3-19上午10:33:07
	 * @param attributeKey 属性主键
	 * @param attributeValue 属性值
	 * @return 第一个符合指定属性值的节点
	 */
	IViewHandler getFirstChildNodeByAttribute(
				String attributeKey,String attributeValue);
	
	/**
	 * 获取当前节点下第一个符合指定属性值的节点 全称：getFirstChildNodeByAttribute
	 * @param attributeKey
	 * @param attributeValue
	 * @return
	 * 2016年12月14日
	 * @author MBG
	 */
	IViewHandler fcna(String attributeKey,String attributeValue);
	
	/**
	 * 向指定位置插入子节点
	 * @author 刘虻
	 * 2006-11-1上午12:06:36
	 * @param index 索引位置
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler addChildNode(int index,IViewHandler childNode);
	
	/**
	 * 插入子节点序列
	 * @author 刘虻
	 * 2007-7-3下午01:31:51
	 * @param childNodeList 子节点序列
	 * @return 当前类实例
	 */
	IViewHandler addChildNode(List<IViewHandler> childNodeList);
	
	
	/**
	 * 插入子节点 ac(IViewHandler childNode)
	 * @author 刘虻
	 * 2007-2-9下午12:45:55
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler addChildNode(IViewHandler childNode);
	
	
	/**
	 * 插入子节点 addChildNode(IViewHandler childNode)
	 * 刘虻
	 * 2010-8-1 上午10:13:29
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler ac(IViewHandler childNode);
	
	/**
	 * 插入子节点到头部 addChildNode(IViewHandler childNode)
	 * @author 刘虻
	 * 2007-3-22下午08:20:03
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler addFirstChildNode(IViewHandler childNode);
	
	
	/**
	 * 插入子节点到头部 addFirstChildNode(IViewHandler childNode)
	 * 刘虻
	 * 2010-8-1 上午10:14:02
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler afc(IViewHandler childNode);
	
	/**
	 * 移出当前节点下所有子节点
	 * @author 刘虻
	 * @return 返回当前类实例
	 * 2007-3-18下午03:51:27
	 */
	List<IViewHandler> removeChildNodes();
	
	/**
	 * 移出子节点
	 * @author 刘虻
	 * 2007-2-9下午01:55:25
	 * @param index 节点序号
	 * @return 移出的节点
	 */
	IViewHandler removeChildNode(int index);
	
	/**
	 * 移出子节点 rcid(String id)
	 * @author 刘虻
	 * 2007-2-9下午01:55:40
	 * @param idValue 节点id属性
	 * @return 移出的节点序列
	 */
	IViewHandler removeChildNodeByID(String idValue);
	
	/**
	 * 移出子节点 removeChildsNodeByID(String idValue)
	 * 刘虻
	 * 2010-8-1 上午10:19:22
	 * @param id 移出的节点序列
	 */
	List<IViewHandler> rcsid(String id);
	
	/**
	 * 移出子节点 rcsid(String id)
	 * @author 刘虻
	 * 2007-2-9下午01:55:40
	 * @param idValue 节点id属性
	 * @return 移出的节点序列
	 */
	List<IViewHandler> removeChildNodesByID(String idValue);
	
	/**
	 * 移出子节点 removeChildNodeByID(String idValue)
	 * 刘虻
	 * 2010-8-1 上午10:19:22
	 * @param id 移出的节点序列
	 */
	IViewHandler rcid(String id);
	
	/**
	 * 根据pk属性值移除子节点
	 * @author 刘虻
	 * 2008-12-10上午11:42:18
	 * @param pkValue pk属性值
	 * @return 移出的节点序列
	 */
	List<IViewHandler> removeChildNodeByPK(String pkValue);
	
	/**
	 * 移出子节点
	 * @author 刘虻
	 * 2007-2-9下午01:55:55
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler removeChildNode(IViewHandler childNode);
	
	/**
	 * 设置当前节点的子节点
	 * @author 刘虻
	 * 2006-10-21下午01:24:44
	 * @param childNode 子节点
	 * @return 返回当前类实例
	 */
	IViewHandler setChildNode(IViewHandler childNode);
	
	/**
	 * 设置当前节点的子节点序列
	 * @author 刘虻
	 * 2006-10-21下午01:25:28
	 * @param childNodes 子节点序列
	 * @return 返回当前类实例
	 */
	IViewHandler setChildNodes(List<IViewHandler> childNodes);
	
	/**
	 * 获取当前节点内容
	 * @author 刘虻
	 * 2006-10-21下午01:20:33
	 * @return 当前节点内容
	 */
	String getNodeBody();

	/**
	 * 获取节点内容并且输出到流中
	 * @author 刘虻
	 * 2007-2-8下午03:37:58
	 * @param printStream 输出流
	 * @return 返回当前类实例
	 */
	IViewHandler getNodeBody(IPrintStream printStream);
	
	/**
	 * 获得节点类型
	 * @author 刘虻
	 * 2006-10-21下午01:01:53
	 * @return 节点类型
	 */
	int getNodeType();
	
	
	/**
	 * 设置节点类型
	 * @author 刘虻
	 * 2006-10-21下午01:04:37
	 * @param nodeType 节点类型
	 * @return 返回当前类实例
	 */
	IViewHandler setNodeType(int nodeType);

	/**
	 * 是否输出当前标签（不控制当前标签的子标签） os()
	 * @author 刘虻
	 * 2006-12-6下午10:19:14
	 * @return true输出
	 */
	boolean isOutSelf();

	/**
	 * 是否输出当前标签（不控制当前标签的子标签） isOutSelf()
	 * @author 刘虻
	 * 2006-12-6下午10:19:14
	 * @return true输出
	 */
	boolean os();
	
	/**
	 * 设置是否输出当前标签（不控制当前标签的子标签） os(boolean outSelf)
	 * @author 刘虻
	 * 2006-12-6下午10:20:04
	 * @param outSelf true输出
	 * @return 返回当前类实例
	 */
	IViewHandler setOutSelf(boolean outSelf);
	
	/**
	 * 设置是否输出当前标签（不控制当前标签的子标签） setOutSelf(boolean outSelf)
	 * 刘虻
	 * 2010-8-1 上午10:23:55
	 * @param outSelf true输出
	 * @return 返回当前类实例
	 */
	IViewHandler os(boolean outSelf);
	
	/**
	 * 获取是否输出当前节点全部内容 oa()
	 * @author 刘虻
	 * 2006-10-21下午01:08:49
	 * @return true输出全部内容
	 */
	boolean isOutAll();
	
	/**
	 * 获取是否输出当前节点全部内容 isOutAll()
	 * 刘虻
	 * 2010-8-1 上午10:22:35
	 * @return true输出全部内容
	 */
	boolean oa();
	
	/**
	 * 是否为空节点
	 * @author 刘虻
	 * 2007-3-26上午10:57:09
	 * @return true是
	 */
	boolean isEmpty();
	
	/**
	 * 设置是否输出当前节点全部内容 oa(boolean outAll)
	 * @author 刘虻
	 * 2006-10-21下午01:09:31
	 * @param outAll true输出当前节点全部内容
	 * @return 返回当前类实例
	 */
	IViewHandler setOutAll(boolean outAll);
	
	
	/**
	 * 设置是否输出当前节点全部内容 setOutAll(boolean outAll)
	 * 刘虻
	 * 2010-8-1 上午10:22:05
	 * @param outAll true输出当前节点全部内容
	 * @return 返回当前类实例
	 */
	IViewHandler oa(boolean outAll);
	

	/**
	 * 获取是否输出当前节点的子节点内容 oc()
	 * @author 刘虻
	 * 2006-10-21下午01:11:59
	 * @return true输出当前节点的子节点内容
	 */
	boolean isOutChild();
	
	
	/**
	 * 获取是否输出当前节点的子节点内容 isOutChild()
	 * 刘虻
	 * 2010-8-1 上午10:22:55
	 * @return true输出当前节点的子节点内容
	 */
	boolean oc();
	
	/**
	 * 设置是否输出当前节点的子节点内容 oc(boolean ooutChild)
	 * @author 刘虻
	 * 2006-10-21下午01:12:51
	 * @param outChild true输出当前节点的子节点内容
	 * @return 返回当前类实例
	 */
	IViewHandler setOutChild(boolean outChild);
	
	/**
	 * 设置是否输出当前节点的子节点内容 setOutChild(boolean outChild)
	 * 刘虻
	 * 2010-8-1 上午10:23:21
	 * @param ooutChild true输出当前节点的子节点内容
	 * @return 返回当前类实例
	 */
	IViewHandler oc(boolean ooutChild);
	
	/**
	 * 清除内部数据
	 * @author 刘虻
	 * @return 返回当前类实例
	 * 2006-11-1上午12:30:03
	 */
	IViewHandler clear();
	
	/**
	 * 获取当前节点的父节点 等同于 pn();
	 * @author 刘虻
	 * 2006-11-3下午03:28:59
	 * @return 当前节点的父节点
	 */
	IViewHandler getParentNode();
	
	/**
     * 当前节点的父节点 等同于 getParentNode();
     * @return 节点的父节点
     * 2016年1月29日
     * @author 马宝刚
     */
    IViewHandler pn();
    
	/**
	 * 获取当前节点的根节点
	 * 刘虻
	 * 2010-8-6 上午10:54:16
	 * @return 当前节点的根节点
	 */
	IViewHandler getBaseNode();
	
	/**
	 * 设置当前节点的父节点
	 * @author 刘虻
	 * 2006-11-3下午03:30:02
	 * @param parentNode 当前节点的父节点
	 * @return 返回当前类实例
	 */
	IViewHandler setParentNode(IViewHandler parentNode);
	
	
	/**
	 * 设置当前节点中的内容 nt(String nodeText)
	 * @author 刘虻
	 * 2006-11-3下午04:06:20
	 * @param nodeText 当前节点中的内容
	 * @return 返回当前类实例
	 */
	IViewHandler setNodeText(String nodeText);
	
	/**
	 * 设置当前节点中的内容 setNodeText(String nodeText)
	 * 刘虻
	 * 2010-8-1 上午10:21:35
	 * @param nodeText 当前节点中的内容
	 * @return 返回当前类实例
	 */
	IViewHandler nt(String nodeText);
	
	/**
	 * 获取当前节点中的内容 nt()
	 * @author 刘虻
	 * 2006-11-3下午04:06:38
	 * @return 当前节点中的内容
	 */
	String getNodeText();
	
	/**
	 * 获取当前节点中的内容 getNodeText()
	 * 刘虻
	 * 2010-8-1 上午10:18:36
	 * @return 当前节点中的内容
	 */
	String nt();
	
	/**
	 * 设置程序操作编码
	 * @author 刘虻
	 * 2007-4-5下午07:47:45
	 * @param dealEncode 程序操作编码
	 * @return 程序操作编码
	 */
	IViewHandler setDealEncode(String dealEncode);
	
	
	/**
	 * 获取程序操作编码
	 * @author 刘虻
	 * 2007-4-5下午07:48:08
	 * @return 程序操作编码
	 */
	String getDealEncode();
	
	/**
	 * 获取一个和当前实例相同的新实例 n()
	 * 返回的实例是当前节点的子节点
	 * @author 刘虻
	 * 2007-2-7下午07:43:47
	 * @return 当前实例相同的新实例
	 */
	IViewHandler newInstance();
	
	/**
	 * 获取一个和当前实例相同的新实例 newInstance()
	 * @author 刘虻
	 * 2007-2-7下午07:43:47
	 * @return 当前实例相同的新实例
	 */
	IViewHandler n();
	
	
	/**
	 * 获取新实例 n(boolean alone)
	 * 刘虻
	 * 2010-9-13 上午10:08:31
	 * @param alone true是否作为单独节点， false返回值是当前节点的子节点 
	 * @returnd 新节点对象
	 */
	IViewHandler newInstance(boolean alone);
	
	/**
	 * 获取新实例 newInstance(boolean alone)
	 * 刘虻
	 * 2010-9-13 上午10:08:31
	 * @param alone true是否作为单独节点， false返回值是当前节点的子节点 
	 * @returnd 新节点对象
	 */
	IViewHandler n(boolean alone);
	
	/**
	 * 获取属性容器 简称 am()
	 * @author 刘虻
	 * 2007-1-1上午10:39:19
	 * @return 属性容器
	 */
	Map<String,String> getAttributeMap();
	
	/**
	 * 获取属性容器 全称：getAttributeMap()
	 * @return
	 * 2017年11月20日
	 * @author MBG
	 */
	Map<String,String> am();
	
	/**
	 * 获取属性临界符号容器
	 * @author 刘虻
	 * 2007-1-1上午10:40:06
	 * @return 属性临界符号容器
	 */
	Map<String,String> getAttributeSignMap();

	
	/**
	 * 该节点是否为文本内容
	 * @author 刘虻
	 * 2007-4-20下午01:43:11
	 * @return true是
	 */
	boolean isTextNode();
	
	
	/**
	 * 设置是否输出警告日志
	 * @author 刘虻
	 * 2007-4-24下午05:53:34
	 * @param noWarning true输出警告日志
	 */
	IViewHandler setWarning(boolean isWarning);
	
	/**
	 * 获取是否输出警告日志
	 * @author 刘虻
	 * 2007-4-24下午05:53:46
	 * @return 不输出警告日志
	 */
	boolean isWarning();
	
	/**
	 * 向当前节点中放入文本节点
	 * @author 刘虻
	 * 2007-7-3下午01:26:38
	 * @param textString 文本内容
	 * @return 当前类实例
	 */
	IViewHandler innerText(String textString);
	
	
	/**
	 * 是否存在属性名为name的子板块
	 * @author 刘虻
	 * 2007-8-29下午04:07:46
	 * @param nameValue 属性值
	 * @return true存在
	 */
	boolean hasChildNodeByName(String nameValue);
	
	
	/**
	 * 是否存在属性名为id的子板块
	 * @author 刘虻
	 * 2007-8-29下午04:10:25
	 * @param idValue 属性值
	 * @return true存在
	 */
	boolean hasChildNodeByID(String idValue);

	/**
	 * 是否存在属性名为pk的子板块
	 * @author 刘虻
	 * 2008-12-10上午11:41:28
	 * @param pkValue 属性值
	 * @return true存在
	 */
	boolean hasChildNodeByPK(String pkValue);
	
	/**
	 * 通过属性名判断是否存在指定的子标签
	 * @author 刘虻
	 * 2007-8-29下午04:06:46
	 * @param attributeKey 属性名
	 * @param attributeValue 属性值
	 * @return true存在
	 */
	boolean hasChildNodeByAttribute(
			String attributeKey, String attributeValue);
	
	
	/**
	 * 是否存在指定标签名的子标签
	 * @author 刘虻
	 * 2007-8-29下午04:12:55
	 * @param nodeName 标签名
	 * @return true存在
	 */
	boolean hasChildNodeByNodeName(String nodeName);
	
	/**
	 * 指定节点是否为当前节点的子节点
	 * @author 刘虻
	 * 2008-5-13上午09:51:12
	 * @param childNode 指定节点
	 * @return true是当前节点的子节点
	 */
	boolean isChildNode(Object childNode);
	
	
	/**
	 * 根据节点名称移除子节点
	 * @author 刘虻
	 * 2008-11-13下午12:55:07
	 * @param nodeName 节点名称
	 * @return 移出的节点序列
	 */
	List<IViewHandler> removeChildNodesByNodeName(String nodeName);
	
	
	/**
	 * 移除指定属性值的节点 简称：rcsa
	 * @param attributeName  属性名
	 * @param attributeValue 属性值
	 * @return 被移除的节点序列
	 * 2016年11月23日
	 * @author MBG
	 */
	List<IViewHandler> removeChildNodesByAttribute(String attributeName,String attributeValue);
	
	
	/**
	 * 移除指定属性值的节点 全称：removeChildNodesByAttribute
	 * @param attributeName  属性名
	 * @param attributeValue 属性值
	 * @return 被移除的节点序列
	 * 2016年11月23日
	 * @author MBG
	 */
	List<IViewHandler> rcsa(String attributeName,String attributeValue);
	
	
	/**
	 * 移除第一个指定节点名的子节点
	 * @author 刘虻
	 * 2008-11-13下午12:57:00
	 * @param nodeName 节点名
	 * @return 移出的节点
	 */
	IViewHandler removeFirstChildNodeByNodeName(String nodeName);
	
	
	/**
	 * 移除最后一个指定节点名的子节点
	 * @author 刘虻
	 * 2008-11-13下午12:57:47
	 * @param nodeName 节点名
	 * @return 移出的节点
	 */
	IViewHandler removeLastChildNodeByNodeName(String nodeName);
	
	
	/**
	 * 获取符合属性信息的最后一个子节点
	 * @author 刘虻
	 * 2008-11-13下午01:03:24
	 * @param attributeKey 属性名
	 * @param attributeValue 属性值
	 * @return 当前类实例
	 */
	IViewHandler getLastChildNodeByAttribute(
					String attributeKey,String attributeValue);
	
	
	/**
	 * 设置节点CDATA数据 cdata(String nodeText)
	 * @author 刘虻
	 * 2008-11-14下午02:28:34
	 * @param nodeText CDATA数据
	 * @return 当前类实例
	 */
	IViewHandler setNodeCdata(String nodeText);
	
	/**
	 * 设置节点CDATA数据 setNodeCdata(String nodeText)
	 * 刘虻
	 * 2010-8-1 上午10:21:06
	 * @param nodeText CDATA数据
	 * @return 当前类实例
	 */
	IViewHandler cdata(String nodeText);
	
	/**
	 * 是否为顶级节点
	 * @author 刘虻
	 * 2008-12-10下午02:54:31
	 * @return 顶级节点
	 */
	boolean isTopNode();
	
	/**
	 * 是否存在子节点
	 * @author 刘虻
	 * 2008-12-11下午12:22:30
	 * @return 是否存在子节点
	 */
	boolean hasChildNodes();
	
	
	/**
	 * 获取有效的参数容器
	 * @author 刘虻
	 * 2009-7-20下午05:25:55
	 * @return 参数容器
	 */
	Map<String,String> getParameterMap();
	
	
	/**
	 * 获取指定参数值
	 * <!--*   key1="value1" key2="value2" -->
	 * @author 刘虻
	 * 2009-7-20下午05:26:31
	 * @param key 参数主键
	 * @return 参数值
	 */
	String getParameter(String key);
	
	/**
	 * 判断是否存在该参数 简称 isp()
	 * @author 刘虻
	 * 2009-7-20下午05:27:44
	 * @param key 参数主键
	 * @return true 存在该参数
	 */
	boolean hasParameter(String key);
	
	
	/**
	 * 设置是否为无结束标签头
	 * @author 刘虻
	 * 2008-12-3下午03:38:04
	 * @param singleTag 是否为无结束标签头
	 * @return 返回当前类实例
	 */
	IViewHandler setSingleTag(boolean singleTag);
	
	/**
	 * 设置是否为无结束标签头
	 * @author 刘虻
	 * 2008-12-3下午03:38:04
	 * @param singleTag 是否为无结束标签头
	 * @return 返回当前类实例
	 */
	IViewHandler st(boolean singleTag);
	
	/**
	 * 获取是否为无结束标签头 st()
	 * @author 刘虻
	 * 2008-12-3下午03:38:15
	 * @return 是否为无结束标签头
	 */
	boolean isSingleTag();
	
	/**
	 * 获取是否为无结束标签头 isSingleTag()
	 * @author 刘虻
	 * 2008-12-3下午03:38:15
	 * @return 是否为无结束标签头
	 */
	boolean st();
	
	/**
	 * 设置是内容否为XML格式
	 * 
	 * 因为HTML中存在无结束标识的标签，比如 <input> <br> 在XML中这是不允许的
	 * 
	 * @author 刘虻
	 * 2009-11-5下午03:39:53
	 * @param xmlStyle 内容否为XML格式
	 */
	IViewHandler setXmlStyle(boolean xmlStyle);
	
	
	/**
	 * 获取内容否为XML格式
	 * @author 刘虻
	 * 2009-11-5下午03:40:08
	 * @return 内容否为XML格式
	 */
	boolean isXmlStyle();
	
	
	/**
	 * 添加并返回一个新的子节点 nc(String nodeName)
	 * 刘虻
	 * 2010-7-7 下午05:39:03
	 * @param nodeName 新的子节点名称
	 * @return 新的子节点
	 */
	IViewHandler addNewChildNode(String nodeName);
	
	/**
	 * 添加并返回一个新的子节点 addNewChildNode(String nodeName)
	 * 刘虻
	 * 2010-7-7 下午05:39:03
	 * @param nodeName 新的子节点名称
	 * @return 新的子节点
	 */
	IViewHandler nc(String nodeName);
	
	
	/**
	 * 克隆
	 * 刘虻
	 * 2010-10-28 下午02:41:50
	 * @return 相同类实例
	 */
	IViewHandler cn();
	
	/**
	 * 重命名属性名
	 * @param srcAttributeName     源属性名
	 * @param objAttributeName     目标属性名
	 * @return             当前类实例
	 * 2014年4月19日
	 * @author 马宝刚
	 */
	IViewHandler rna(String srcAttributeName,String objAttributeName);
	
	/**
	 * 复制属性值
	 * @param srcAttributeName 源属性名
	 * @param objAttributeName 目标属性名
	 * @return 当前类实例
	 * 2014年4月19日
	 * @author 马宝刚
	 */
	IViewHandler cpa(String srcAttributeName,String objAttributeName);
	
	
	/**
	 * 获取来源信息
	 * @return 来源信息
	 * 2014年7月30日
	 * @author 马宝刚
	 */
	String getLocationInfo();
	
	   /**
     * 获取存在指定属性名的节点序列
     * @param attribName 属性名
     * @return 节点序列
     * 2014年12月31日
     * @author 刘虻
     */
    List<IViewHandler> getAttribNodes(String attribName);
    
    
    /**
     * 根据节点名获取第一个符合条件的节点对象
     * 是  getFirstChildNodeByNodeName 方法的缩写
     * @param nodeName 节点名
     * @return 第一个符合条件的节点对象
     * 2015年4月17日
     * @author 马宝刚
     */
    IViewHandler fnn(String nodeName);
    
    
    /**
     * 根据节点名获取第一个符合条件的节点对象
     * 如果不存在符合条件的节点，则在当前节点中创建一个新的子节点
     * @param nodeName 节点名
     * @return         子节点
     * 2019年7月2日
     * @author MBG
     */
    IViewHandler fnnc(String nodeName);
    
    
    /**
     * 根据节点名获取符合条件的节点序列（包含子节点的子节点，以及当前节点）
     * 
     * 如果父节点跟子节点的节点名同名，用这个方法遍历同名子节点，就出问题了，需要用 ocnn
     * 
     * 全名：getChildNodesByNodeName
     * @param nodeName 节点名
     * @return 符合条件的节点序列
     * 2015年4月17日
     * @author 马宝刚
     */
    List<IViewHandler> cnn(String nodeName);
    
    /**
     * 返回符合节点名的子节点序列（不包含子节点的子节点）
     * @param nodeName 节点名
     */
    List<IViewHandler> ocnn(String nodeName);
    
    /**
     * 构建一个名字为childNodeName的子节点
     * @param childNodeName 子节点的名字
     * @return 子节点
     * 2016年1月29日
     * @author 马宝刚
     */
    IViewHandler ac(String childNodeName);
    
	/**
	 * 获取包含指定属性名的子节点（包含子节点的子节点）
	 * @param attributeKey 属性名
	 * @return 子节点序列
	 * 2017年3月31日
	 * @author MBG
	 */
	List<IViewHandler> getChildNodesByAttribute(String attributeKey);
	
	
	/**
	 * 返回首个节点名为nodeName,并且id值为attrValue的节点 （返回值不为空）
	 * @param nodeName   节点名
	 * @param idValue    id值
	 * @return           首个符合条件的子节点 （返回值不为空）
	 * 2017年10月16日
	 * @author MBG
	 */
	IViewHandler fnn(String nodeName,String idValue);
	
	/**
	 * 返回当前节点中，节点名为nodeName,并且属性名为attrName,属性值为attrValue的节点 （返回值不为空）
	 * @param nodeName   当前节点或其子节点
	 * @param attrName   属性名
	 * @param attrValue  属性值
	 * @param node       当前节点或其子节点
	 * @return           首个符合条件的节点 （返回值不为空）
	 * 2017年10月16日
	 * @author MBG
	 */
	IViewHandler fnn(String nodeName,String attrName,String attrValue);
	
	/**
	 * 设置节点中的文本字符串（非HTML或XML）
	 * @param nodeText  文本内容
	 * @return          当前类实例
	 * 2017年11月8日
	 * @author MBG
	 */
	IViewHandler text(String nodeText);
	
	/**
	 * 输出节点内容 同getNodeBody
	 * @return
	 * 2017年11月8日
	 * @author MBG
	 */
	String html();
	
	/**
	 * 输出节点内容 同getNodeBody
	 * @return
	 * 2017年11月8日
	 * @author MBG
	 */
	String xml();
	
	/**
	 * 将容器中的信息作为属性信息放入节点中
	 * @param data 数据容器
	 * @return     当前类实例
	 * 2017年11月13日
	 * @author MBG
	 */
	IViewHandler a(Map<String,String> data);
	
	/**
	 * 返回第一个子节点
	 * @return 第一个子节点
	 * 2017年11月13日
	 * @author MBG
	 */
	IViewHandler f();
	
	/**
	 * 判断当前节点是否存在指定参数值 全称：hasProperty()
	 * @param property 指定参数值
	 * @return 是否存在指定参数值
	 * 2017年11月14日
	 * @author MBG
	 */
	boolean isp(String property);
	
	/**
	 * 将子节点名作为key，子节点的text作为值拼装成Map
	 * @return 拼装后的Map
	 * 2018年3月2日
	 * @author MBG
	 */
	Map<String,String> cnm();
	
    /**
     * 返回符合节点名的子节点序列（不包含子节点的子节点）
     * @param nodeName 节点名
     */
    IViewHandler fonn(String nodeName);
    
	/**
	 * 创建指定节点名的子节点并且设置节点文本（该方法通常用在拼装xml报文）
	 * @param childNodeName 节点名
	 * @param childNodeText 节点文本
	 * @return              注意：返回的是当前节点类实例（不是新增的子节点类实例）
	 * 2019年8月2日
	 * @author MBG
	 */
	IViewHandler ac(String childNodeName,String childNodeText);
}
