/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.viewhandler;

import java.util.List;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 按照HTML模板操作指定HTML处理接口
 * @author 刘虻
 * 2010-9-10 下午05:53:55
 */
@ClassInfo({"2014-06-04 11:04","按照HTML模板操作指定HTML处理接口"})
@BeanInfo({"nodemodelhandler"})
public interface INodeModel {
	
	/**
	 * 获取指定HTML处理类
	 * @author 刘虻
	 * 2008-12-11上午11:53:36
	 * @param filePath HTML文件绝对路径
	 * @param encoding 编码
	 * @return 指定HTML处理类
	 * @throws Exception 执行发生异常
	 */
	INodeHandler getHtmlNode(
			String filePath,String encoding) throws Exception;
	
	
	/**
	 * 通过指定模板区域获取目标HTML对象中的区域(递归)
	 * @author 刘虻
	 * 2008-12-10下午06:25:56
	 * @param mVdh模板HTML对象
	 * @param mCVdh 指定模板区域块对象
	 * @param oVdh 目标HTML对象
	 * @return 指定的目标区域块
	 * @throws Exception 执行发生异常
	 */
	IViewHandler getTargetViewDataHandler(
			IViewHandler mVdh,
			IViewHandler mCVdh) throws Exception;
	
	
	/**
	 * 通过模板获取指定位置的节点序列
	 * 
	 * 目标对象从模板页面对象中获取信息
	 * <!TARGET URL="" />
	 * 
	 * @author 刘虻
	 * 2008-12-10下午07:05:43
	 * @param mVdh 模板HTML对象
	 * @param mCVdh 模板指定位置的循环元素
	 * @param isVirtual 指定循环元素是否为虚拟元素(目标HTML中不存在的元素)
	 * @return 指定位置的节点序列
	 * @throws Exception 执行发生异常
	 */
	List<IViewHandler> getTargetViewDataHandlerList(
			IViewHandler mVdh
			,IViewHandler mCVdh
			,boolean isVirtual) throws Exception;
}
