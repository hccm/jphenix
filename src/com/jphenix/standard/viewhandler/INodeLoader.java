/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.viewhandler;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;


/**
 * 模板加载管理类接口
 * @author 刘虻
 * 2007-1-1下午09:58:03
 */
@ClassInfo({"2014-06-04 11:04","模板加载管理类接口"})
@BeanInfo({"nodeloader"})
public interface INodeLoader {
	
	/**
	 * 类容器主键
	 */
	String CASE_ID = "nodeloader";
	
	/**
	 * 获取新的视图实例
	 * 刘虻
	 * 2010-6-8 下午04:47:02
	 * @return 新的视图实例
	 */
	INodeHandler getNewNode();
	
	
	/**
	 * 获取程序编码
	 * @author 刘虻
	 * 2010-6-8 下午04:47:02
	 * @return 程序编码
	 */
	String getDealEncoding();

	
	/**
	 * 设置页面根路径
	 * 刘虻
	 * 2010-5-18 下午12:48:15
	 * @return 页面根路径
	 */
	String getBasePath();
	
	/**
	 * 设置视图根路径
	 * 刘虻
	 * 2010-6-8 下午04:47:59
	 * @param basePath 视图根路径
	 */
	void setBasePath(String basePath);
	
	/**
	 * 加载指定文件
	 * 刘虻
	 * 2010-6-8 下午05:31:08
	 * @param subPath 视图文件网站相对路径
	 * @param ac      动作上下文
	 * @return 视图类实例
	 * @throws Exception 执行发生异常
	 */
	INodeHandler getNode(String subPath,IActionContext ac) throws Exception;
}
