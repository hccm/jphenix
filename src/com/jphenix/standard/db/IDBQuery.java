/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.db;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.jphenix.service.db.common.instancea.DBQuerySvc;
import com.jphenix.service.db.datamanager.interfaceclass.IDataManager;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 数据库操作接口
 * 
 * 2018-08-01 增加了分页查询方法，直接传入页号和每页记录数
 * 2019-02-14 增加了查询并返回字符串的方法，增加了自定义分隔符
 * 2019-03-06 简化了接口中无用的方法
 * 2019-06-18 新增了获取表信息服务
 * 2019-08-13 增加了exec方法调用存储过程返回记录集
 * 2019-11-06 提取了拼装语句值的方法到接口中
 * 2020-09-04 不分页查询记录集方法，增加了传入记录集序列，将查询后的记录集直接放入传入参数中
 * 
 * @author 刘虻
 * 2011-6-2 下午02:56:41
 */
@ClassInfo({"2020-09-05 00:49","数据库操作接口"})
@BeanInfo({"dbquery"})
public interface IDBQuery {

	/**
	 * 获取当前日期时间
	 * 刘虻
	 * 2011-6-2 下午03:22:23
	 * @return 当前日期时间
	 */
	String getDateTime();

	/**
	 * 获取当前时间
	 * 刘虻
	 * 2011-6-2 下午03:22:30
	 * @return 当前时间
	 */
	String getTime();

	/**
	 * 获取当前日期
	 * 刘虻
	 * 2011-6-2 下午03:22:41
	 * @return 当前日期
	 */
	String getDate();

	/**
	 * 获取新的主键值
	 * 刘虻
	 * 2011-6-2 下午03:17:28
	 * @return 新的主键值
	 */
	String getSID();

	/**
	 * 获取查询记录数
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp      分页信息容器
	 * @throws Exception  异常
	 */
	void queryAllCount(QueryPageVO qp) throws Exception;

	/**
	 * 分页查询记录
	 * 刘虻
	 * 2011-6-2 下午03:12:49
	 * @param qp      分页信息容器
	 * @throws Exception  异常
	 */
	void queryPage(QueryPageVO qp) throws Exception;

	/**
	 * 获取指定行记录集中指定字段值
	 * 刘虻
	 * 2011-6-2 下午05:06:11
	 * @param fieldPK   字段主键
	 * @param row    行号
	 * @param rs    记录集
	 * @return      字段值
	 */
	String rss(String fieldPK,int row,List<Map<String,String>> rs);

	/**
	 * 获取首行记录指定字段值
	 * 刘虻
	 * 2011-6-2 下午05:06:50
	 * @param fieldPK  字段主键
	 * @param rs    记录集
	 * @return      字段值
	 */
	String frss(String fieldPK,List<Map<String,String>> rs);

	/**
	 * 更新CLob字段
	 * 
	 * 再此之前需要在insert语句中将CLob字段设置为 empty_clob()函数值
	 * 
	 * @author 刘虻
	 * 2009-9-21下午07:48:59
	 * @param clobSql    更新语句 select content from table_name where id=? for update
	 * @param uList      语句中对应的条件值
	 * @param fieldName  CLob字段名
	 * @param fieldValue CLob 字段值
	 * @throws Exception 执行发生异常
	 */
	void updateClob(
			String clobSql
			,List<String> uList
			,String fieldName
			,String fieldValue) throws Exception;

	/**
	 * 执行更新
	 * @param sql            更新语句
	 * @param uValueArrl      更新值数组
	 * @return              返回更新状态
	 * @throws Exception      异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	int executeUpdate(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 执行更新
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值序列
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	int executeUpdate(String sql,List<String> uValueArrl) throws Exception;

	/**
	 * 查询并返回记录集序列
	 * @param sql          查询语句
	 * @param uValueArrl    更新值数组
	 * @return            记录集序列
	 * @throws Exception    异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(String sql) throws Exception;

	/**
	 * 查询并返回记录集序列
	 * @param sql                   查询语句
	 * @param uValueArrl        更新值序列
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(String sql,List<String> uValueArrl) throws Exception;


	/**
	 * 查询结果并返回第一条记录
	 * @param sql          查询语句
	 * @param uValueArrl    提交值素组
	 * @return            记录集行对象
	 * @throws Exception    异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> queryOne(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> queryOne(String sql) throws Exception;

	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> queryOne(String sql,List<String> uValueArrl) throws Exception;

	/**
	 * 设置数据源主键
	 * @param dsName 数据源主键
	 * 2014-3-8
	 * @author 马宝刚
	 */
	void setDataSource(String dsName);

	/**
	 * 更新Blob字段值
	 * @param blobSql 更新语句  slect content from table_name where id=? for update
	 * @param uList       设置Clob条件值对象
	 * @param fieldName   BLob字段名
	 * @param blobIs      BLob 字段值读入流
	 * @throws Exception  执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	void updateBlob(
			String blobSql
			,List<String> uList
			,String fieldName
			,InputStream blobIs) throws Exception;

	/**
	 * 更新Blob字段值
	 * @param blobSql 更新语句  slect content from table_name where id=? for update
	 * @param pk 主键值
	 * @param fieldName BLob字段名
	 * @param blobIs BLob 字段值读入流
	 * @throws Exception 执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	void updateBlobByPK(
			String blobSql
			,String pk
			,String fieldName
			,InputStream blobIs) throws Exception;

	/**
	 * 获取Blob字段值 读入流
	 * 语句中只能有一个blob字段，并且用主键定位 比如  select blobContent from table where id=?
	 * 
	 * @param sql        取数语句   
	 * @param pkValue    主键值
	 * @param blobOuter  blob字段输出处理类
	 * @throws Exception 异常
	 * 2014年4月11日
	 * @author 马宝刚
	 */
	void queryBlob(String sql,String pkValue,IBlobOuter blobOuter) throws Exception;

	/**
	 * 调用存储过程
	 * @param sql          sql语句
	 * @param params       传入参数数组
	 * @param types        对应的参数类型数组
	 * @param isOuts       是否为传出参数
	 * @return             传出参数序列
	 * @throws Exception   异常
	 * 2014年5月23日
	 * @author 马宝刚
	 */
	List<Object> call(String sql,String[] params,int[] types,boolean[] isOuts) throws Exception;

	/**
	 * 获取当前数据源类型
	 * @return 当前数据源类型
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	String getSourceType();

	/**
	 * 获取数据源主键
	 * @return 数据源主键
	 * 2014年9月24日
	 * @author 马宝刚
	 */
	String getSourceName();

	/**
	 * 查询记录集
	 * @param sql        查询语句
	 * @param uList      查询提交值序列
	 * @param dicts      数据字典类序列
	 * @return           查询记录集
	 * @throws Exception 异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(String sql, List<String> uList,Object[] dicts) throws Exception;


	/**
	 * 查询并返回记录集序列
	 * @param sql         查询语句
	 * @param uValueArrl  更新值数组
	 * @param dicts       数据字典序列
	 * @return            记录集序列
	 * @throws Exception  异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(String sql,String[] uValueArrl,Object[] dicts) throws Exception;

	/**
	 * 查询结果并返回第一条记录
	 * @param sql               查询语句
	 * @param uList             提交值序列
	 * @param dicts             数据字典类序列
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> queryOne(String sql,List<String> uList,Object[] dicts) throws Exception;


	/**
	 * 查询结果并返回第一条记录
	 * @param sql                   查询语句
	 * @param uValueArrl        提交值素组
	 * @param dicts               数据字典类序列
	 * @return                      记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> queryOne(String sql,String[] uValueArrl,Object[] dicts) throws Exception;


	/**
	 * 获取数据库链接
	 * @param sessionKey     会话主键（事务处理）
	 * @return               数据库链接对象
	 * @throws Exception     异常
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	Connection getConn(String sessionKey) throws Exception;

	/**
	 * 获取数据库链接
	 * @return 数据库链接对象
	 * @throws Exception 异常
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	Connection getConn() throws Exception;


	/**
	 * 调用存储过程 全称 call
	 * @param sql          sql语句
	 * @param params       传入参数数组
	 * @param types        对应的参数类型数组
	 * @param isOuts       是否为传出参数
	 * @return             传出参数序列
	 * @throws Exception   异常
	 * 2014年5月23日
	 * @author 马宝刚
	 */
	List<Object> c(String sql,String[] params,int[] types,boolean[] isOuts) throws Exception;


	/**
	 * 执行更新 全称 executeUpdate
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值序列
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	int e(String sql,List<String> uValueArrl) throws Exception;


	/**
	 * 执行更新 全称 executeUpdate
	 * @param sql                       更新语句
	 * @param uValueArrl            更新值数组
	 * @return                          返回更新状态
	 * @throws Exception            异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	int e(String sql,String[] uValueArrl) throws Exception;


	/**
	 * 更新Blob字段值 全称 updateBlobByPK
	 * @param blobSql     更新语句  slect content from table_name where id=? for update
	 * @param pk          主键值
	 * @param fieldName   BLob字段名
	 * @param blobIs      BLob 字段值读入流
	 * @throws Exception  执行发生异常
	 * 2014年4月10日
	 * @author 马宝刚
	 */
	void u(
			String blobSql
			,String pk
			,String fieldName
			,InputStream blobIs) throws Exception;


	/**
	 * 获取数据库连接池管理类
	 * @return 数据库连接池管理类
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	IDataManager dm();


	/**
	 * 获取查询记录数 全称 queryAllCount
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	void qc(QueryPageVO qp) throws Exception;


	/**
	 * 获取Blob字段值 读入流  全称 queryList
	 * 语句中只能有一个blob字段，并且用主键定位 比如  select blobContent from table where id=?
	 * 
	 * @param sql        取数语句   
	 * @param pkValue    主键值
	 * @param blobOuter  blob字段输出处理类
	 * @throws Exception 异常
	 * 2014年4月11日
	 * @author 马宝刚
	 */
	void q(String sql,String pkValue,IBlobOuter blobOuter) throws Exception;


	/**
	 * 查询并返回记录集序列  全称 queryList
	 * @param sql                   查询语句
	 * @return                      记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql) throws Exception;


	/**
	 * 查询并返回记录集序列  全称 queryList
	 * @param sql               查询语句
	 * @param uValueArrl        更新值序列
	 * @return                  记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql,List<String> uList) throws Exception;


	/**
	 * 查询记录集 全称 queryList
	 * @param sql             查询语句
	 * @param uList           查询提交值序列
	 * @param dicts           数据字典类数组
	 * @return                查询记录集
	 * @throws Exception      异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql, List<String> uList,Object[] dicts) throws Exception;
	

	/**
	 * 查询记录集
	 * 
	 * 根据不同的提交值对象类型，设置不同的类型
	 * 
	 * 
	 * @param sql              查询语句
	 * @param uList            查询提交值序列
	 * @param dicts            数据字典类数组
	 * @param rs               待插入数据的序列（如果存在该值，返回值也是该值）
	 * @return                 查询记录集
	 * @throws Exception       异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(
			String sql, List<String> uList,Object[] dicts,List<Map<String,String>> rs) throws Exception;


	/**
	 * 查询并返回记录集序列 全称  queryList
	 * @param sql               查询语句
	 * @param uValueArrl        更新值数组
	 * @return                  记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql,String[] uValueArrl) throws Exception;


	/**
	 * 查询并返回记录集序列 全称  queryList
	 * @param sql               查询语句
	 * @param uValueArrl        更新值数组
	 * @param dicts             数据字典数组
	 * @return                  记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql,String[] uValueArrl,Object[] dicts) throws Exception;


	/**
	 * 查询结果并返回第一条记录 全称  queryOne
	 * @param sql               查询语句
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql) throws Exception;


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql               查询语句
	 * @param uValueArrl        提交值序列
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,List<String> uValueArrl) throws Exception;


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql               查询语句
	 * @param uList             提交值序列
	 * @param dicts             数据字典类数组
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,List<String> uList,Object[] dicts) throws Exception;


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql               查询语句
	 * @param uValueArrl        提交值素组
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,String[] uValueArrl) throws Exception;


	/**
	 * 查询结果并返回第一条记录 全称 queryOne
	 * @param sql               查询语句
	 * @param uValueArrl        提交值素组
	 * @param dicts             数据字典类序列
	 * @return                  记录集行对象
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,String[] uValueArrl,Object[] dicts) throws Exception;

	/**
	 * 获取查询记录数  全称 queryPage
	 * 刘虻
	 * 2011-6-2 下午03:10:36
	 * @param qp            分页信息容器
	 * @throws Exception    异常
	 */
	void q(QueryPageVO qp) throws Exception;


	/**
	 * 执行更新
	 * @param sql 更新语句
	 * @return 更新记录数
	 * @throws Exception 异常
	 * 2015年11月28日
	 * @author 马宝刚
	 */
	int executeUpdate(String sql) throws Exception;

	/**
	 * 执行更新 全称：executeUpdate
	 * @param sql        更新语句
	 * @return           更新记录数
	 * @throws Exception 异常
	 * 2015年11月28日
	 * @author 马宝刚
	 */
	int e(String sql) throws Exception;


	/**
	 * 执行分页查询
	 * @param sql        查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates    提交值数组
	 * @return           查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO qp(String sql,String[] updates) throws Exception;

	/**
	 * 执行分页查询
	 * @param sql           查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates       提交值数组
	 * @return              查询结果
	 * @throws Exception    异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO queryPage(String sql,String[] updates) throws Exception;


	/**
	 * 执行分页查询
	 * @param sql        查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates    提交值数组
	 * @param dicts      字典类实例数组
	 * @return           查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO qp(String sql,String[] updates,Object[] dicts) throws Exception;

	/**
	 * 执行分页查询
	 * @param sql        查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates    提交值数组
	 * @param dicts      字典主键对象
	 * @return           查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO queryPage(String sql,String[] updates,Object[] dicts) throws Exception;


	/**
	 * 执行分页查询
	 * @param sql        查询语句（获取总数语句通过查询语句拼装而来）
	 * @return           查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO qp(String sql) throws Exception;

	/**
	 * 执行分页查询
	 * @param sql        查询语句（获取总数语句通过查询语句拼装而来）
	 * @return           查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO queryPage(String sql) throws Exception;


	/**
	 * 更新数据
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * 刘虻
	 * 2011-6-2 下午03:07:50
	 * @param sql         更新语句
	 * @param params      提交值对象数组
	 * @return            更新记录数
	 * @throws Exception  异常
	 */
	int executeUpdate(String sql, Object[] params) throws Exception;


	/**
	 * 执行更新 全称 executeUpdate
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql        更新语句
	 * @param params     更新值数组
	 * @return           返回更新状态
	 * @throws Exception 异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	int e(String sql,Object[] params) throws Exception;

	/**
	 * 查询记录集
	 * 
	 * 根据不同的提交值对象类型，设置不同的类型
	 * 
	 * 
	 * @param sql              查询语句
	 * @param params           查询提交值对象数组
	 * @param dicts            数据字典类数组
	 * @return                 查询记录集
	 * @throws Exception       异常
	 * 2014年9月25日
	 * @author 马宝刚
	 */
	List<Map<String,String>> queryList(
			String sql, Object[] params,Object[] dicts) throws Exception;


	/**
	 * 查询结果并返回第一条记录
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql         查询语句
	 * @param params      提交值对象数组
	 * @return            记录集行对象
	 * @throws Exception  异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,Object[] params) throws Exception;


	/**
	 * 查询结果并返回第一条记录
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql          查询语句
	 * @param uVlaueArrl      提交值对象数组
	 * @param dicts               数据字典类序列
	 * @return            记录集行对象
	 * @throws Exception    异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	Map<String,String> o(String sql,Object[] paras,Object[] dicts) throws Exception;


	/**
	 * 查询并返回记录集序列
	 * 
	 * 通过传入不同类型的提交值，设置不同的传入值类型
	 * 
	 * 
	 * @param sql          查询语句
	 * @param params    更新值数组
	 * @return            记录集序列
	 * @throws Exception    异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql,Object[] params) throws Exception;


	/**
	 * 查询并返回记录集序列
	 * 
	 * 用于不同类型的提交值，设置不同类型
	 * 
	 * 
	 * @param sql               查询语句
	 * @param params        更新值数组
	 * @param dicts             数据字典序列
	 * @return                  记录集序列
	 * @throws Exception        异常
	 * 2014-3-8
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(String sql,Object[] params,Object[] dicts) throws Exception;


	/**
	 * 执行分页查询
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值对象数组
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO qp(String sql,Object[] updates) throws Exception;


	/**
	 * 执行分页查询
	 * 
	 * 根据不同类型的提交值设置不同值
	 * 
	 * @param sql 查询语句（获取总数语句通过查询语句拼装而来）
	 * @param updates 提交值
	 * @param dicts 字典主键对象
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2016年5月19日
	 * @author MBG
	 */
	QueryPageVO qp(String sql,Object[] updates,Object[] dicts) throws Exception;


	/**
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param params      提交值数组
	 * @param onlyOne     是否只取头一条记录
	 * @return            记录集字符串（用逗号分割）
	 * @throws Exception  异常
	 * 2016年10月2日
	 * @author MBG
	 */
	String queryString(String sql, Object[] params,boolean onlyOne) throws Exception;


	/**
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param params      提交值数组
	 * @param value       是否只取一条记录
	 * @param splitStr    如果返回多条记录，分隔符值（为空时为半角逗号）
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2019年02月14日
	 * @author MBG
	 */
	String queryString(String sql, Object[] params,boolean onlyOne,String splitStr) throws Exception;


	/**
	 * 简称：qs
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	String queryString(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 全称：queryString
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	String qs(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 全称：queryString
	 * 将查询出的记录集采用这种方式输出为字符串 
	 *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	 *  即字段值之间用|分割，行数据之间用半角逗号分割
	 * @param sql         查询语句
	 * @param uValueArrl   提交值数组
	 * @param splitStr    分隔符，默认为半角逗号
	 * @return 记录集字符串（用逗号分割）
	 * @throws Exception 异常
	 * 2016年10月2日
	 * @author MBG
	 */
	String qs(String sql,String[] uValueArrl,String splitStr) throws Exception;

	/**
	 * 获取第一条记录，将第一条记录作为字符串输出
	 * col1|col2|col3|col4
	 * @param sql 查询语句
	 * @param uValueArrl 提交值数组
	 * @return 记录字符串
	 * @throws Exception 异常
	 * 2016年10月17日
	 * @author MBG
	 */
	String os(String sql,String[] uValueArrl) throws Exception;

	/**
	 * 获取第一条记录，将第一条记录作为字符串输出
	 * col1|col2|col3|col4
	 * @param sql 查询语句
	 * @return 记录字符串
	 * @throws Exception 异常
	 * 2016年10月17日
	 * @author MBG
	 */
	String os(String sql) throws Exception;



	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param               提交值信息
	 * @return              记录集序列
	 * @throws Exception  异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(Object[] infos) throws Exception;

	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param infos         提交值信息
	 * @param dicts         字典对象实例数组
	 * @return              记录集序列
	 * @throws Exception  异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(Object[] infos,Object[] dicts) throws Exception;

	/**
	 * 查询并返回记录集序列 （用于复杂语句拼装接入）
	 * 
	 * @param infos         提交值信息
	 * @param dicts         字典对象实例数组
	 * @param rs            待插入数据的序列（如果存在该值，返回值也是该值）
	 * @return              记录集序列
	 * @throws Exception    异常
	 * 2017年1月22日
	 * @author 马宝刚
	 */
	List<Map<String,String>> q(Object[] infos,Object[] dicts,List<Map<String,String>> rs) throws Exception;


	/**
	 * 执行分页查询 （用于复杂语句拼装接入）
	 * @param infos 提交值信息
	 * @return 查询结果
	 * @throws Exception 异常
	 * 2017年1月22日
	 * @author MBG
	 */QueryPageVO qp(Object[] infos) throws Exception;

	 /**
	  * 执行分页查询 （用于复杂语句拼装接入）
	  * @param infos 提交值信息
	  * @param dicts         字典对象实例数组
	  * @return 查询结果
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 QueryPageVO qp(Object[] infos,Object[] dicts) throws Exception;

	 /**
	  * 将查询出的记录集采用这种方式输出为字符串  （用于复杂语句拼装接入）
	  *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	  *  即字段值之间用|分割，行数据之间用半角逗号分割
	  * @param infos 提交值信息
	  * @return 记录集字符串（用逗号分割）
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 String qs(Object[] infos) throws Exception;

	 /**
	  * 将查询出的记录集采用这种方式输出为字符串  （用于复杂语句拼装接入）
	  *  row1_col1|row1_col2|row1_col3,row2_col1|row2_col2|row2_col3
	  *  即字段值之间用|分割，行数据之间用半角逗号分割
	  * @param infos 提交值信息
	  * @param splitStr 分隔符，为空时为半角逗号
	  * @return 记录集字符串
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 String qs(Object[] infos,String splitStr) throws Exception;

	 /**
	  * 获取第一条记录，将第一条记录作为字符串输出 （用于复杂语句拼装接入）
	  * col1|col2|col3|col4
	  * @param infos 提交值信息
	  * @return 记录字符串
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 String os(Object[] infos) throws Exception;

	 /**
	  * 查询结果并返回第一条记录 （用于复杂语句拼装接入）
	  * @param infos             提交值信息
	  * @return                  记录集行对象
	  * @throws Exception        异常
	  * 2017年1月22日
	  * @author 马宝刚
	  */
	 Map<String,String> o(Object[] infos) throws Exception;

	 /**
	  * 查询结果并返回第一条记录 （用于复杂语句拼装接入）
	  * @param infos             提交值信息
	  * @param dicts             数据字典类序列
	  * @return                  记录集行对象
	  * @throws Exception        异常
	  * 2014-3-8
	  * @author 马宝刚
	  */
	 Map<String,String> o(Object[] infos,Object[] dicts) throws Exception;


	 /**
	  * 执行更新 （用于复杂语句拼装接入）
	  * @param infos             提交值信息
	  * @return                  返回更新状态
	  * @throws Exception        异常
	  * 2014-3-8
	  * @author 马宝刚
	  */
	 int e(Object[] infos) throws Exception;


	 /**
	  * 获取数据库连接池管理类
	  * @return 数据库连接池管理类
	  * 2015年11月23日
	  * @author 马宝刚
	  */
	 IDataManager getDataManager();

	 /**
	  * 更新BLob字段
	  * @author 刘虻
	  * 2009-9-21下午07:48:59
	  * @param clobSql 更新语句 update content from table_name where id=?
	  * @param pkValue 主键值
	  * @param blobIs  Blob值流
	  * @throws Exception 执行发生异常
	  */
	 void updateBlob(
			 String blobSql
			 ,String pkValue
			 ,InputStream blobIs) throws Exception;

	 /**
	  * 查询带Blob字段的记录集（只查询符合条件的第一条记录）
	  * 注意：有个特殊的字段值，即 blob字段名__length  标记输出内容大小
	  * @param               提交值信息
	  * @return              记录集 key 主键  value：字段值或者为读入流
	  * @throws Exception  异常
	  * 2017年4月7日
	  * @author 马宝刚
	  */
	 @SuppressWarnings("rawtypes")
	 Map queryBlob(Object[] infos) throws Exception;

	 /**
	  * 查询带Blob字段的记录集（只查询符合条件的第一条记录）
	  * 注意：有个特殊的字段值，即 blob字段名__length  标记输出内容大小
	  * @param sql        查询语句
	  * @param uValueArrl 提交值
	  * @return 记录集 key 主键  value：字段值或者为读入流
	  * @throws Exception 异常
	  * 2017年4月7日
	  * @author MBG
	  */
	 @SuppressWarnings("rawtypes")
	 Map queryBlob(String sql,Object[] params) throws Exception;

	 /**
	  * 执行插入语句，并获取插入后的自增主键值
	  * @param infos 脚本中采用 <%SQL$  语句     $SQL%> 拼装的信息
	  * @return 自增主键值
	  * @throws Exception 异常
	  * 2017年4月18日
	  * @author MBG
	  */
	 int insert(Object[] infos) throws Exception;

	 /**
	  * 构造分页查询对象
	  * @param infos 提交值信息
	  * @return 分页查询对象（未执行查询）
	  * @throws Exception 异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 QueryPageVO qpvo(Object[] infos);

	 /**
	  * 执行分页查询 （用于复杂语句拼装接入）
	  * 注意：该方法并不会返回记录集总数，也不会拼装和执行获取记录集总数的语句
	  *       该方法通常用于执行复杂SQL语句，避免拼装获取记录集总数导致执行时间过长
	  * @param pageSize     如果从会话线程中没有获取到每页记录数，则设置默认的每页记录数（避免一次性查询出大量记录导致系统崩溃）
	  * @param infos        提交值信息
	  * @return             带记录集的分页对象
	  * @throws Exception   异常
	  * 2017年9月8日
	  * @author MBG
	  */
	 QueryPageVO qp(int pageSize,Object[] infos) throws Exception;

	 /**
	  * 简称 qm
	  * 仅允许查两个字段值的记录，第一个字段值作为Map的key，第二个断指作为Map的Value
	  * @param infos 查询传入信息
	  * @return      返回数据 key第一个字段值  value第二个字段值
	  * @throws Exception 异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,String> queryMap(Object[] infos) throws Exception;

	 /**
	  * 全称 queryMap
	  * 仅允许查两个字段值的记录，第一个字段值作为Map的key，第二个断指作为Map的Value
	  * @param infos 查询传入信息
	  * @return      返回数据 key第一个字段值  value第二个字段值
	  * @throws Exception 异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,String> qm(Object[] infos) throws Exception;

	 /**
	  * 简称：ql
	  * 仅允许查一个字段值的记录，作为序列元素返回
	  * @param infos 查询传入信息
	  * @return      返回数据 ele元素：第一个字段值
	  * @throws Exception 异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 List<String> queryList(Object[] infos) throws Exception;

	 /**
	  * 全称：queryList
	  * 仅允许查一个字段值的记录，作为序列元素返回
	  * @param infos 查询传入信息
	  * @return      返回数据 ele元素：第一个字段值
	  * @throws Exception 异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 List<String> ql(Object[] infos) throws Exception;

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,Map<String,String>> queryIndex(String indexKey,Object[] infos) throws Exception;

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @param dicts        字典类对象数组
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,Map<String,String>> queryIndex(String indexKey,Object[] infos,Object[] dicts) throws Exception;

	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,Map<String,String>> qi(String indexKey,Object[] infos) throws Exception;


	 /**
	  * 全称 queryIndex
	  * 查询记录集，并按照指定字段值作为返回的key值放入返回容器中
	  * @param indexKey     key值字段名
	  * @param infos        查询提交值
	  * @param dicts        字典类对象数组
	  * @return             key 指定字段值，value，行记录集
	  * @throws Exception   异常
	  * 2017年9月11日
	  * @author MBG
	  */
	 Map<String,Map<String,String>> qi(String indexKey,Object[] infos,Object[] dicts) throws Exception;


	 /**
	  * 执行分页查询（用于复杂语句拼装接入）
	  * @param pageNo      指定页号
	  * @param pageSize    每页记录数
	  * @param infos       提交值信息
	  * @return            查询结果
	  * @throws Exception  异常
	  * 2018年8月1日
	  * @author MBG
	  */
	 QueryPageVO qp(int pageNo,int pageSize,Object[] infos) throws Exception;


	 /**
	  * 执行分页查询 （用于复杂语句拼装接入）
	  * @param pageNo        页号
	  * @param pageSize      每页记录数
	  * @param infos         提交值信息
	  * @param dicts         字典对象实例数组
	  * @return              查询结果
	  * @throws Exception    异常
	  * 2017年1月22日
	  * @author MBG
	  */
	 QueryPageVO qp(int pageNo,int pageSize,Object[] infos,Object[] dicts) throws Exception;


	 /**
	  * 获取数据操作服务
	  * @return 数据操作服务
	  * @throws Exception 异常
	  * 2014年9月24日
	  * @author 马宝刚
	  */
	 DBQuerySvc getDbs();

	 /**
	  * 以exec方式执行存储过程
	  * @param sql         调用存储过程的语句
	  * @param paras       提交参数数组
	  * @return            返回记录及
	  * @throws Exception  异常
	  * 2019年8月13日
	  * @author MBG
	  */
	 List<Map<String,String>> exec(String sql,String[] paras) throws Exception;


	 /**
	  * 将值放入事务中
	  * @author 刘虻
	  * @param stmt 事务
	  * @param fieldObjectArrl 值序列 
	  * @return 插入值日志信息
	  * 2006-3-30 19:44:40
	  * @throws SQLException 设置值时发生异常
	  */
	 String fixPreparedStatement(PreparedStatement stmt,Object[] params) throws Exception;
}
