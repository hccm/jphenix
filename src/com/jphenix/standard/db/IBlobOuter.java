/*
 * 代号：凤凰
 * 2014年9月3日
 * V3.0
 */
package com.jphenix.standard.db;

import java.io.InputStream;

/**
 * Blob字段输出处理类（内部类接口）
 * @author 马宝刚
 * 2014年9月3日
 */
public interface IBlobOuter {

    /**
     * 版本控制用
     */
    String VER = "2014-09-03 16:22";
    
    /**
     * 类标题
     */
    String CLASS_TITLE = "Blob字段输出处理类";
    
    /**
     * 执行输出
     * @param is  数据读入流
     * 2014年9月3日
     * @author 马宝刚
     */
    void outer(InputStream is);
}
