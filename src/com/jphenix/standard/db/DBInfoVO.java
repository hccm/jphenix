/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-05
 * V4.0
 */
package com.jphenix.standard.db;

import java.util.HashMap;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 数据库连接信息VO
 * com.jphenix.service.db.vo.DBInfoVO
 * @author 刘虻
 * 2010-6-22 上午10:22:42
 */
@ClassInfo({"2014-06-05 16:10","数据库连接信息VO"})
public class DBInfoVO {

	protected HashMap<String,String> dbInfoMap = null;		//数据库连接信息容器
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public DBInfoVO() {
		super();
	}
	
	/**
	 * 设置数据库连接信息容器
	 * 刘虻
	 * 2010-6-22 下午12:40:18
	 * @param dbInfoMap 数据库连接信息容器
	 */
	public void setDBInfoMap(HashMap<String,String> dbInfoMap) {
		this.dbInfoMap = dbInfoMap;
	}
	
	/**
	 * 获取数据库连接信息容器
	 * 刘虻
	 * 2010-6-22 下午12:40:11
	 * @return 数据库连接信息容器
	 */
	public HashMap<String,String> getDBInfoMap() {
		return dbInfoMap;
	}
}
