/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年6月24日
 * V4.0
 */
package com.jphenix.standard.db;

import com.jphenix.share.lang.SString;
import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主子记录集
 * @author 马宝刚
 * 2015年6月24日
 */
@ClassInfo({"2015-06-24 14:58","主子记录集"})
public class ParentChildRecord {

    private int thisIndex = -1; //当前记录所在序列位置
    private String thisIdValue = null; //当前记录主键值
    private Map<String,Map<String,String>> indexMap = null; //主键索引容器
    private Map<String,List<String>> pcr = null; //主子记录集
    private List<String> thisRsIdList = new ArrayList<String>(); //当前层级记录集主键序列
    
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ParentChildRecord(
            List<String> idList
            ,Map<String,Map<String,String>> indexMap
            ,Map<String,List<String>> pcr) {
        thisIndex = -1;
        this.thisRsIdList = idList;
        this.indexMap = indexMap;
        this.pcr = pcr;
    }
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ParentChildRecord(String id,String pid,List<Map<String,String>> rs) {
        super();
        pcr = new HashMap<String,List<String>>();
        if(rs==null || rs.size()<1 || id==null || id.length()<1 || pid==null || pid.length()<1) {
            return;
        }
        indexMap = new HashMap<String,Map<String,String>>();
        String idValue;     //主键值
        String pidValue;    //父主键值
        List<String> idList;         //主键序列
        thisIndex = -1;
        thisIdValue = null;
        for(Map<String,String> ele:rs) {
            idValue = SString.valueOf(ele.get(id));
            pidValue = SString.valueOf(ele.get(pid));
            indexMap.put(idValue,ele);
            if(pidValue.length()<1) {
                thisRsIdList.add(idValue);
                continue;
            }
            idList = pcr.get(pidValue);
            if(idList==null) {
                idList = new ArrayList<String>();
                pcr.put(pidValue,idList);
            }
            idList.add(idValue);
        }
    }
    
    /**
     * 将记录集指针指向BOF位置，并返回当前层级第一条记录
     * @return 当前层级第一条记录
     * 2015年6月24日
     * @author 马宝刚
     */
    public Map<String,String> first(){
        thisIndex = -1;
        return indexMap.get(thisRsIdList.get(0));
    }
    
    /**
     * 讲记录集指针指向EOF位置，并返回当前层级最后一条记录
     * @return 当前层级最后一条记录
     * 2015年6月24日
     * @author 马宝刚
     */
    public Map<String,String> last(){
        thisIndex = thisRsIdList.size();
        if(thisIndex>0) {
            return indexMap.get(thisRsIdList.get(thisIndex-1));
        }
        return new HashMap<String,String>();
    }
    
    /**
     * 返回下一条行记录容器
     * @return 下一条行记录容器
     * 2015年6月24日
     * @author 马宝刚
     */
    public Map<String,String> next(){
        thisIndex++;
        if(thisIndex<0 || thisIndex>=thisRsIdList.size()) {
            return null;
        }
        thisIdValue = thisRsIdList.get(thisIndex);
        return indexMap.get(thisIdValue);
    }
    
    /**
     * 是否讲记录集指针移动到了末尾
     * @return 是否讲记录集指针移动到了末尾
     * 2015年6月24日
     * @author 马宝刚
     */
    public boolean eof() {
        if(thisRsIdList==null || thisRsIdList.size()<1) {
            return true;
        }
        return thisIndex + 1 >= thisRsIdList.size();
    }
    
    /**
     * 指针是否移动到了记录集开头位置
     * @return 指针是否移动到了记录集开头位置
     * 2015年6月24日
     * @author 马宝刚
     */
    public boolean bof() {
        return thisIndex - 1 < 0;
    }
    
    /**
     * 是否存在子记录集
     * @return 是否存在子记录集
     * 2015年6月24日
     * @author 马宝刚
     */
    public boolean hasChild() {
        List<String> idList = pcr.get(thisIdValue);
        return idList != null && idList.size() >= 1;
    }
    
    /**
     * 获取子记录集
     * @return 子记录集
     * 2015年6月24日
     * @author 马宝刚
     */
    public ParentChildRecord getChild() {
        //子记录主键序列
        List<String> idList = pcr.get(thisIdValue);
        if(idList==null || idList.size()<1) {
            return null;
        }
        return new ParentChildRecord(idList,indexMap,pcr);
    }
    
    
    /**
     * 当前层级记录集数量
     * @return 当前层级记录集数量
     * 2015年6月24日
     * @author 马宝刚
     */
    public int size() {
        return thisRsIdList.size();
    }
}
