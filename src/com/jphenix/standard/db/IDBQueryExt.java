/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月24日
 * V4.0
 */
package com.jphenix.standard.db;

import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;

/**
 * 扩展数据库操作类
 * 
 * 2020-09-09 增加了查询记录并缓存表状态值方法
 * 
 * 
 * @author 马宝刚
 * 2014年9月24日
 */
@ClassInfo({"2020-09-09 19:37","扩展数据库操作类"})
public interface IDBQueryExt extends IDBQuery {

    
    /**
     * 指定表数据是否发生变化
     * @param tableName 表名
     * @param tableState 表状态
     * @return true表数据发生变化
     * @throws Exception 异常
     * 2014年9月25日
     * @author 马宝刚
     */
    boolean dataChange(String tableName,String tableState) throws Exception;
    
    /**
     * 获取最新的表状态
     * @param tableName 表名
     * @return 表状态值
     * 2014年9月26日
     * @author 马宝刚
     */
    String getTableState(String tableName);
    
    
	/**
	 * 设置最新的表状态
	 * @param tableName  表名
	 * @return           新的表状态值
	 * 2018年4月23日
	 * @author MBG
	 */
	String setTableState(String tableName);
    
    /**
     * 通过主键值更新指定记录，如果不存在主键值，则新增记录
     * 
     * 数据值从提交参数中获取
     * 
     * @param tableName 表名
     * @return 返回更新主键值
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String updateByPK(String tableName) throws Exception;
    
    /**
     * 通过主键值更新指定记录，如果不存在主键值，则新增记录
     * @param tableName 表名
     * @param dataMap 数据容器
     * @return 主键值
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String updateByPK(String tableName,Map<String,String> dataMap) throws Exception;
    
    /**
     * 构造查询字段语句段
     * @param fHead 字段前缀  最后生成的语句  fHead.字段名
     * @param oHead 字段别名前缀  最后生成的别名为 oHead_字段名
     * @param tableName 表名
     * @return 查询字段语句段
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String getFieldInfo(String fHead,String oHead,String tableName) throws Exception;
    

    /**
     * 查询指定主键值的记录集
     * @param pk    主键值
     * @param tableName 表名
     * @return 行记录
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    Map<String,String> getRowDataByPK(String pk,String tableName) throws Exception;
    
    /**
     * 获取指定表的主键字段名
     * @param tableName 表名
     * @return  主键字段名
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String getPKFieldName(String tableName) throws Exception;
    
    /**
     * 获取指定表的字段序列
     * @param tableName 表名
     * @param dsName 数据源主键
     * @return 字段序列
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    List<String> getFieldList(String tableName) throws Exception;
    
    /**
     * 从页面请求中获取数据更新到表中
     * @param tableName 表名
     * @param where 更新条件
     * @param uList 条件中对应的参数数组
     * @return 更新记录数
     * @throws Exception 异常
     * 2014年9月26日
     * @author 马宝刚
     */
    int updateTable(String tableName,String where,String[] uList) throws Exception;
    
    /**
     * 更新表数据
     * @param tableName 表名
     * @param dataMap 提交数据容器
     * @param where 更新条件（不包含where关键字）
     * @param uList 条件中对应的参数数组
     * @return 更新记录数
     * @throws Exception 异常
     * 2014年9月26日
     * @author 马宝刚
     */
    int updateTable(String tableName,Map<String,String> dataMap,String where,String[] uList) throws Exception;
    
    
    /**
     * 通过表主键值获取指定一条记录
     * @param tableName 表名
     * @param id 主键值
     * @param dictBeans 数据字典类数组
     * @return 一条记录集
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    Map<String,String> queryByPK(String tableName,String id,Object[] dictBeans) throws Exception;
    
    /**
     * 通过表主键值获取指定一条记录
     * @param tableName 表名
     * @param id 主键值
     * @return 一条记录集
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    Map<String,String> queryByPK(String tableName,String id) throws Exception;
    
    
    /**
     * 通过表主键值获取指定一条记录
     * @param tableName 表名
     * @return 一条记录集
     * @throws Exception 异常
     * 2016年10月9日
     * @author MBG
     */
    Map<String,String> queryByPK(String tableName) throws Exception;
    
    /**
     * 通过表主键值获取指定一条记录
     * @param tableName 表名
     * @param dicts     字典对象数组
     * @return 一条记录集
     * @throws Exception 异常
     * 2016年10月9日
     * @author MBG
     */
    Map<String,String> queryByPK(String tableName,Object[] dicts) throws Exception;
    
    
    /**
     * 通过表主键值获取指定一条记录 全称：queryByPK
     * @param tableName 表名
     * @return 一条记录集
     * @throws Exception 异常
     * 2016年10月9日
     * @author MBG
     */
    Map<String,String> qpk(String tableName) throws Exception;
    
    
    /**
     * 通过表主键值获取指定一条记录 全称：queryByPK
     * @param tableName 表名
     * @param idValue   主键值
     * @return 一条记录集
     * @throws Exception 异常
     * 2016年10月9日
     * @author MBG
     */
    Map<String,String> qpk(String tableName,String idValue) throws Exception;
    
    /**
     * 通过表主键值获取指定一条记录 全称：queryByPK
     * @param tableName 表名
     * @param dicts     字典对象数组
     * @return 一条记录集
     * @throws Exception 异常
     * 2016年10月9日
     * @author MBG
     */
    Map<String,String> qpk(String tableName,Object[] dicts) throws Exception;
    
    
    /**
     * 如果提交上来主键值，则对指定记录执行更新，否则执行新增
     * @param tableName 表名
     * @param ac 动作上下文
     * @return 返回主键值
     * @throws Exception 异常
     * 2014年10月16日
     * @author 马宝刚
     */
    String updateByPK(String tableName,IActionContext ac) throws Exception;
    
    /**
     * 通过主键值更新指定记录，如果不存在主键值，则新增记录
     * 
     * 数据值从提交参数中获取  全称： updateByPK
     * 
     * @param tableName 表名
     * @return 返回更新主键值
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String upk(String tableName) throws Exception;
    
    /**
     * 从页面请求中获取数据更新到表中  全称：updateTable
     * @param tableName 表名
     * @param where 更新条件
     * @param uList 条件中对应的参数数组
     * @return 更新记录数
     * @throws Exception 异常
     * 2014年9月26日
     * @author 马宝刚
     */
    int ut(String tableName,String where,String[] uList) throws Exception;
    
    
    /**
     * 通过主键值更新指定记录，如果不存在主键值，则新增记录 全称：updateByPK
     * @param tableName 表名
     * @param dataMap 数据容器
     * @return 主键值
     * @throws Exception 异常
     * 2014年9月24日
     * @author 马宝刚
     */
    String upk(String tableName,Map<String,String> dataMap) throws Exception;
    
    
    /**
     * 删除指定主键值记录  全称：deleteByPK
     * 如果表中存在dr字段，则为标记删除，否则是真删
     * @param tableName 表名
     * @param id 主键值
     * @return 影响记录数
     * @throws Exception 异常
     * 2014年10月16日
     * @author 马宝刚
     */
    int dpk(String tableName,String id) throws Exception;
    
    
    /**
     * 删除指定主键值记录 简称：dpk
     * 如果表中存在dr字段，则为标记删除，否则是真删
     * @param tableName 表名
     * @param id 主键值
     * @return 影响记录数
     * @throws Exception 异常
     * 2014年10月16日
     * @author 马宝刚
     */
    int deleteByPK(String tableName,String id) throws Exception;
    
    
    /**
     * 根据主键值删除表记录 简称：dpk
     * 如果表中存在dr字段，则为标记删除，否则是真删
     * @param tableName   表名
     * @return 影响记录数
     * @throws Exception  异常
     * 2016年10月7日
     * @author MBG
     */
    int deleteByPK(String tableName) throws Exception;
    
    /**
     * 根据主键值删除表记录 全称：deleteByPK
     * 如果表中存在dr字段，则为标记删除，否则是真删
     * @param tableName   表名
     * @return 影响记录数
     * @throws Exception  异常
     * 2016年10月7日
     * @author MBG
     */
    int dpk(String tableName) throws Exception;
    
    /**
     * 通过主键更新指定记录，如果不存在主键值，则新增记录   用defDataMap覆盖页面请求值
     * @param tableName 表名
     * @param defDataMap 覆盖页面提交值的容器
     * @return 主键值
     * @throws Exception 异常
     * 2016年10月26日
     * @author MBG
     */
    String upkd(String tableName,Map<String,String> defDataMap) throws Exception;
    
    
	
	/**
	 * 执行无分页查询记录集，如果从上次查询到当前，该表的数据没有发生变化，则不做查询，直接返回true
	 * 
	 * 注意：切记！！！！ 该方法禁止查询频繁增长，或者数据记录过多的表，会导致内存溢出！！！ 切记！切记
	 * 
	 * 该方法通常用来缓存数据字典，等固定数据记录的表
	 * 
	 * @param caller     调用者（通常为服务类实例，一个服务类只允许一个类实例操作同一个数据源的一张表，不允许同一个类的多个类实例操作同一个数据源的同一张表）
	 * @param tableName  指定表名，多个表用逗号分隔
	 * @param rs         空记录集序列或者上次查询好的记录集（如果该表数据没发生变化，则不再更新该记录集序列）
	 * @param infos      提交语句与插入值信息数组
	 * @return           是否重新从数据库读取了数据
	 * @throws Exception 异常
	 * 2020年9月5日
	 * @author MBG
	 */
	boolean qb(
			Object caller,
			String tableName,
			List<Map<String,String>> rs,
			Object[] infos) throws Exception;
	
	/**
	 * 执行无分页查询记录集，如果从上次查询到当前，该表的数据没有发生变化，则不做查询，直接返回true
	 * 
	 * 注意：切记！！！！ 该方法禁止查询频繁增长，或者数据记录过多的表，会导致内存溢出！！！ 切记！切记
	 * 
	 * 该方法通常用来缓存数据字典，等固定数据记录的表
	 * 
	 * @param caller     调用者（通常为服务类实例，一个服务类只允许一个类实例操作同一个数据源的一张表，不允许同一个类的多个类实例操作同一个数据源的同一张表）
	 * @param rs         空记录集序列或者上次查询好的记录集（如果该表数据没发生变化，则不再更新该记录集序列）
	 * @param tableName  指定表名，多个表用逗号分隔
	 * @param infos      提交语句与插入值信息数组
	 * @param dicts      需要处理记录集的字典实例数组
	 * @return           是否重新从数据库读取了数据
	 * @throws Exception 异常
	 * 2020年9月5日
	 * @author MBG
	 */
	boolean qb(
			Object caller,
			String tableName,
			List<Map<String,String>> rs,
			Object[] infos,
			Object[] dicts) throws Exception;
}
