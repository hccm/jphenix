/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年9月25日
 * V4.0
 */
package com.jphenix.standard.db;

import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 数据字典接口
 * 
 * 之前试过，不能将QueryPageVO传到接口里，来解决分页查询后的记录集被字典处理后，QueryPageVO.fields 中没有
 * 被字典新增的字段信息，导致遍历字段时，没有获取到被字典新增的字段值。
 * 
 * 只能修改遍历字段的方法，改用从记录集中获取字段名序列，而不能从QueryPageVO.fields中获取
 * 
 * 之前曾做过在fields中用字典增加字段值，但执行效率比较低，遍历每行记录时都要判断fields中是否存在新的字段名。
 * 一旦加到fields中，在遍历下一行记录时，又从数据库中获取这个由字典新增的字段名，导致操作数据库错误。
 * 
 * 2020-09-09 增加了初始化方法，在执行查询调用字典前，会先调用该初始化方法
 * 
 * @author 马宝刚
 * 2014年9月25日
 */
@ClassInfo({"2020-09-09 17:08","数据字典接口"})
public interface IDataDict {
    
    /**
     * 通过查询数据的字段值获取对应的字典值
     * @param colDataMap 当前插入的行数据 （完整行数据）
     * 2014年9月25日
     * @author 马宝刚
     */
    void data(Map<String,String> colDataMap);
    
    /**
     * 在查询方法使用字典之前，会调用该方法
     * 
     * 通常在这个方法中，刷新动态数据缓存，或者做一些字典数据准备工作
     * 
     * @throws 异常
     * 2020年9月9日
     * @author MBG
     */
    void init() throws Exception;
}
