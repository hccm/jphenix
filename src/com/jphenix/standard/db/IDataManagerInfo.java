/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.db;

import java.util.List;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 数据库管理信息类接口
 * @author 刘虻
 * 2010-10-27 上午09:56:49
 */
@ClassInfo({"2014-06-05 10:50","数据库管理信息类接口"})
public interface IDataManagerInfo {

	/**
	 * 获取数据源主键序列
	 * 刘虻
	 * 2010-10-27 上午09:58:00
	 * @return 数据源主键序列
	 */
	List<String> getDataSourceKeyList();
}
