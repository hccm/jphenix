/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.db;

import java.util.Collection;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 缓存接口
 * @author 刘虻
 * 2012-11-16 下午6:25:35
 */
@ClassInfo({"2014-06-05 10:25","缓存接口"})
public interface IBuffer {
	
	/**
	 * 加入缓存
	 * @author 刘虻
	 * 2007-11-9上午11:25:54
	 * @param obj 对象
	 */
	void add(Object obj);
	
	/**
	 * 加入缓存开头位置
	 * 刘虻
	 * 2013-1-22 下午1:21:23
	 * @param obj 对象
	 */
	void addFirst(Object obj);
	
	/**
	 * 清空缓存
	 * @author 刘虻
	 * 2007-11-9上午11:33:04
	 */
	void clear();
	
	
	/**
	 * 获取获取第一个元素,获取后移出
	 * @author 刘虻
	 * 2007-11-9上午11:35:06
	 */
	Object get();
	
	
	/**
	 * 获取堆栈中堆数
	 * @author 刘虻
	 * 2008-1-16下午05:03:39
	 * @return 堆栈中堆数
	 */
	int size();
	
	/**
	 * 将一个序列中的全部元素放入缓存
	 * 刘虻
	 * 2012-11-18 下午2:33:19
	 * @param c 序列
	 */
	void addAll(Collection<?> c);
	
	
	/**
	 * 检测是否存在
	 * 刘虻
	 * 2012-11-18 下午5:44:40
	 * @param obj 检测对象
	 * @return 是否存在
	 */
	boolean contains(Object obj);
}
