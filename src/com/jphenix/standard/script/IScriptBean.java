/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月31日
 * V4.0
 */
package com.jphenix.standard.script;

import java.util.List;
import java.util.Map;

import com.jphenix.kernel.script.ScriptFieldVO;
import com.jphenix.kernel.script.ScriptVO;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本类接口
 * 
 * 2019-01-24 增加了直接调用脚本的方法，避免反射调用降低效率
 * 2019-04-09 增加了获取当前脚本对应的ScriptVO类实例
 * 2020-09-09 脚本类中增加了方法，返回是否将传入参数变量作为类变量
 * 
 * @author 马宝刚
 * 2014年7月31日
 */
@ClassInfo({"2020-09-09 15:00","脚本类接口"})
public interface IScriptBean {
	

	/**
	 * 获取传入参数序列
	 * @return 传入参数序列
	 * 2016年10月2日
	 * @author MBG
	 */
	List<ScriptFieldVO> getParameterList();
	
	/**
	 * 获取当前脚本的主键
	 * @return 当前脚本的主键
	 * 2015年4月2日
	 * @author 马宝刚
	 */
	String getScriptId();
	
   /**
    * 获取当前脚本的被调用脚本主键序列 
    * 2015年8月4日
    * @return 脚本主键序列
    */
	List<String> getForScriptIds();
	
	/**
	 * 获取脚本加载器
	 * @return 脚本加载器
	 * 2015年11月23日
	 * @author 马宝刚
	 */
	IScriptLoader getScriptLoader();
	
	
	/**
	 * 是否输出日志
	 * @return 是否输出日志
	 * 2016年7月5日
	 * @author MBG
	 */
	boolean noOutLog();
	
	
	/**
	 * 是否启用数据库事务处理
	 * @return 是否启用数据库事务处理
	 * 2016年7月5日
	 * @author MBG
	 */
	boolean isDbTransaction();
	
	/**
	 * 是否允许动作类同一个用户并行执行
	 * 使用这个功能的具体原因请看ScriptVO类中，该变量的注释
	 * @return 是否允许动作类同一个用户并行执行
	 * 2016年12月23日
	 * @author MBG
	 */
	boolean isEnabledActionCharge();
	
	/**
	 * 调用脚本（无返回值）
	 * @param para        传入参数
	 * @throws Exception  异常
	 * 2019年1月21日
	 * @author MBG
	 */
	void _executeN(Map<String,?> para) throws Exception;
	
	/**
	 * 调用脚本（带返回值）
	 * @param para       传入参数
	 * @return           返回值
	 * @throws Exception 异常
	 * 2019年1月21日
	 * @author MBG
	 */
	Object _execute(Map<String,?> para) throws Exception;
	
	
	/**
	 * 获取当前脚本对应的ScriptVO类实例
	 * @return 当前脚本对应的ScriptVO类实例
	 * 2019年4月9日
	 * @author MBG
	 */
	ScriptVO getScriptVO();
	
	/**
	 * 声明的变量是否为类变量
	 * @return 变量是否为类变量
	 * 2020年9月9日
	 * @author MBG
	 */
	boolean isClassVar();
}
