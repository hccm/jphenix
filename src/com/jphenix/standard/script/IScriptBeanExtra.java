/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年12月6日
 * V4.0
 */
package com.jphenix.standard.script;

import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本类扩展方法接口
 * 
 * com.jphenix.standard.script.IScriptBeanExtra
 * 
 * 这个接口不用刻意实现，因为所有脚本最终都继承了父类 BaseParent ，在这个父类中已经实现了这个接口
 * 
 * 2019-12-09 为doException和doFinally增加了传入参数
 * 
 * @author MBG
 * 2019年12月6日
 */
@ClassInfo({"2019-12-12 11:31","脚本类扩展方法接口"})
public interface IScriptBeanExtra {

	/**
	 * 在脚本执行发生异常时执行该方法
	 * 
	 * paras参数，在脚本中固定为 Object[] _SUB_PATAS，
	 * 在脚本过程中可以构造这个参数的实例，
	 * 比如 _SUB_PATAS = new Object[]{"1","2"};
	 * 
	 * 注意：如果脚本（这个脚本带返回值）执行发生异常，并且用该方法拦截到了异常，希望
	 *      该脚本并不抛出异常，而是在这个方法中做了异常处理，希望能返回一个处理后的返回值，
	 *      这个返回值需要放到传入参数 in 中，key为 _RETURN_VALUE_
	 * 
	 * @param e          异常类实例
	 * @param in         当前脚本传入参数容器
	 * @parma paras      当前脚本执行过程中声明的参数
	 * @return           返回true，不再往上抛异常 返回false，继续往上抛异常
	 * @throws Exception 执行发生异常
	 * 2019年12月6日
	 * @author MBG
	 */
	boolean doException(Exception e,Map<String,?> in,Object[] paras) throws Exception;
	
	/**
	 * 在脚本执行结束后执行该方法
	 * 
	 * paras参数，在脚本中固定为 Object[] _SUB_PATAS，
	 * 在脚本过程中可以构造这个参数的实例，
	 * 比如 _SUB_PATAS = new Object[]{"1","2"};
	 * 
	 * @param in         当前脚本传入参数容器
	 * @parma paras      当前脚本执行过程中声明的参数
	 * @throws Exception 异常
	 * 2019年12月6日
	 * @author MBG
	 */
	void doFinally(Map<String,?> in,Object[] paras) throws Exception;
}
