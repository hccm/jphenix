/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年3月14日
 * V4.0
 */
package com.jphenix.standard.script;

import com.jphenix.kernel.script.ScriptVO;
import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

import java.util.Map;

/**
 * 脚本编码管理类接口
 * @author 马宝刚
 * 2016年3月14日
 */
@ClassInfo({"2015-03-14 13:29","脚本编码管理类接口"})
@BeanInfo({"scriptnomanager"})
public interface IScriptNoManager {

    
    /**
     * 获取新的编号
     * @param header 脚本头代码
     * @return 新的脚本编号
     * 2016年3月15日
     * @author 马宝刚
     */
    String create(String header);
    
    
    /**
     * 删除脚本编号
     * @param no 脚本编号
     * 2016年3月15日
     * @author 马宝刚
     */
    void delete(String no);
    
    /**
     * 启动后通知服务端，本地最大的编号
     * @param scriptIdMap  key脚本头，value最大编号
     * 2016年3月15日
     * @author 马宝刚
     */
    void setMaxId(Map<String,String> scriptIdMap);
    
    
    /**
     * 设置本地最大的编号
     * @param header 脚本头
     * @param maxId  最大编号
     * 2016年3月15日
     * @author 马宝刚
     */
    void setMaxId(String header,String maxId);
    
    /**
     * 设置本地最大的编号
     * @param scriptId 脚本编号
     * 2016年3月15日
     * @author 马宝刚
     */
    void setMaxid(String scriptId);
	
	/**
	 * 统计脚本最大主键值
	 * @param scriptMap 脚本容器
	 * 2017年1月12日
	 * @author MBG
	 */
	void setMaxIds(Map<String,ScriptVO> scriptMap);
    
    /**
     * 获取统一的版本编号
     * 不同的服务器的时间一致，会导致新旧版本判断的失误
     * @return
     * 2016年6月16日
     * @author MBG
     */
    String newVer();
}
