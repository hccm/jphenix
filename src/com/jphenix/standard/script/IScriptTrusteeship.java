/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月22日
 * V4.0
 */
package com.jphenix.standard.script;

import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 
 * com.jphenix.standard.script.IScriptTrusteeship
 * 
 * 脚本托管父类接口
 * 
 * 
 * 如果一组脚本（动作类和服务类都可以）需要先做某些判断再做执行，可以先编写个父类
 * 这个父类实现这个接口，并且这个父类加上注解： @ScriptTrusteeship
 * 这样框架会先调用这个接口中的方法，在这个方法中，做判断是否继续调用脚本中的方法
 * 
 * 
 * @author MBG
 * 2019年1月22日
 */
@ClassInfo({"2019-01-22 13:08","脚本托管父类接口"})
public interface IScriptTrusteeship {

	/**
	 * 框架会先调用这个方法，需要在这个方法中自行判断是否 （不带返回值）
	 * 继续调用脚本的 IScriptBean._executeN(Map<String,?> _in)方法
	 * @param _in          传入参数
	 * @throws Exception   异常
	 * 2019年1月22日
	 * @author MBG
	 */
	void _executeXN(Map<String,?> _in) throws Exception;
	
	
	/**
	 * 框架会先调用这个方法，需要在这个方法中自行判断是否 （带返回值）
	 * 继续调用脚本的 IScriptBean._executeN(Map<String,?> _in)方法
	 * @param _in          传入参数
	 * @throws Exception   异常
	 * 2019年1月22日
	 * @author MBG
	 */
	Object _executeX(Map<String,?> _in) throws Exception;
}
