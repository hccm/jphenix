/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年3月15日
 * V4.0
 */
package com.jphenix.standard.script;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本被调用事件接口
 * 
 * com.jphenix.standard.script.IScriptInvokeEvent
 * 
 * 通常可以在脚本父类中实现这个接口，实现统一处理某个功能
 * 
 * 2019-01-24 在执行后触发事件中，增加了调用脚本返回值传入参数
 * 
 * @author 马宝刚
 * 2016年3月15日
 */
@ClassInfo({"2019-01-24 16:51","脚本被调用事件接口"})
@BeanInfo({"scriptinvokeevent"})
public interface IScriptInvokeEvent {

    /**
     * 在调用该脚本前执行的方法
     * @param invoker 调用者（当前脚本是被调用者）
     * @param paraObj 传入参数（多数为Map，少数为List或其他类型），还可能为空
     * @return 如果返回空，继续执行该脚本的方法。 如果不为空，则直接返回这个值
     * 2016年3月15日
     * @author 马宝刚
     */
    <T> T beforeInvoke(Object invoker,Object paraObj);
    
    /**
     * 在脚本执行之后调用的方法
     * @param invoker 调用者 （当前脚本是被调用者），还可能为空
     * @param paraObj 传入参数（多数为Map，少数为List或其他类型）
     * @param res     调用脚本获取的返回值
     * 2016年3月15日
     * @author 马宝刚
     */
    void afterInvoke(Object invoker,Object paraObj,Object res);
}
