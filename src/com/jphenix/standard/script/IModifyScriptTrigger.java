/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年10月30日
 * V4.0
 */
package com.jphenix.standard.script;

import com.jphenix.kernel.script.ScriptVO;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本内容发生变化时触发事件接口
 * 
 * com.jphenix.standard.script.IModifyScriptTrigger
 * 
 * 实现该接口的脚本，需要注册到ScriptLoader脚本加载器中
 * 
 * @author MBG
 * 2019年10月30日
 */
@ClassInfo({"2019-10-30 10:45","脚本内容发生变化时触发事件接口"})
public interface IModifyScriptTrigger {

	/**
	 * 脚本保存后的事件
	 * (可以通过sVO.sourceFileStatus 来区分新增1或更新 2）
	 * @param sVO 脚本信息类
	 * 2019年10月30日
	 * @author MBG
	 */
	void scriptSaved(ScriptVO sVO);
	
	
	/**
	 * 脚本删除后的事件
	 * @param sVO 脚本信息类
	 * 2019年10月30日
	 * @author MBG
	 */
	void scriptDeleted(ScriptVO sVO);
	
	/**
	 * 是否在集群中，只触发一次
	 * 
	 * 如果返回true，集群中的多台服务器中，只有发起方服务器触发一次事件。
	 *     返回false，不光发起方触发事件，同一集群组中，每台服务器都触发一次。
	 *     
	 * 通常返回true时，为类似将脚本更新到Git库中处理服务，只需要更新一次即可
	 * 
	 * @return
	 * 2019年10月30日
	 * @author MBG
	 */
	boolean isClusterOnce();
}
