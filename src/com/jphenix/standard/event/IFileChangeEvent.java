/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2020年3月9日
 * V4.0
 */
package com.jphenix.standard.event;

import com.jphenix.standard.docs.ClassInfo;

/**
 * com.jphenix.standard.event.IFileChangeEvent
 * 
 * 文件变化事件接口
 * 
 * 实现该接口的脚本，在注册类主键输入框中写上N100143,
 * 
 * 在脚本中实现接口中的方法。 在脚本和其它文件新增，修改删除，时
 * 
 * 都会调用该接口方法
 * 
 * 2020-03-10 增加了批次主键值，比如批量上传文件，使用同一个批次值，一次性提交一批文件
 * 
 * 
 * @author MBG
 * 2020年3月9日
 */
@ClassInfo({"2020-03-10 16:23","文件变化事件接口"})
public interface IFileChangeEvent {

	
	/**
	 * 在更新脚本，更新文件之后，调用的方法
	 * @param batchId    批次主键（在commit为true时，提交当前传入的批次主键值相同的全部文件变动信息）
	 * @param path       变动文件路径
	 * @param scriptId   脚本主键（如果不是脚本，该值为空）
	 * @param msg        变动原因信息
	 * @param isDelete   是否为删除
	 * @param commit     是否提交（在修改一批文件时，除了最后一个文件该值为true
	 *                   ，其它文件该值为false。通常用在提交GIT功能中）
	 * @throws           抛出异常，在调用类中捕获异常
	 *                   
	 * 实现该方法，在注册类主键输入框中写上N100143,
	 *                   
	 * 实现该方法用在提交GIT时，可以先判断msg是否为空，如果为空，先不做任何提交，先将信息记录到数据库
	 * 中，最后在一个功能点中做勾选，然后一起提交，提交的时候再确保msg有值
	 * 
	 * 如果commit为true，但msg为空，也不做真正提交，而是先记录下该文件变动
	 *                   
	 * 2020年3月9日
	 * @author MBG
	 */
	public void change(String batchId,String path,String scriptId,String msg,boolean isDelete,boolean commit) throws Exception;
}
