/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年3月29日
 * V4.0
 */
package com.jphenix.standard.beans;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 触发初始化接口
 * 
 * 实现该接口，然后注册到指定类后。在这个类满足条件后回调该方法
 * 
 * 比如一个需要获取到当前服务器名的程序，注册到集群管理类中。
 * 集群管理类在整个网站启动好以后才开始查找当前服务器名（集群配置中声明的名字）
 * 找到服务器名后，再执行这个程序的初始化方法
 * 
 * @author MBG
 * 2017年3月29日
 */
@ClassInfo({"2014-06-13 14:16","触发初始化接口"})
public interface ITriggerInit {

	/**
	 * 注册主类满足条件后调用的初始化方法
	 * @param caller 注册主类
	 * @throws Exception 执行发生异常
	 * 2017年3月29日
	 * @author MBG
	 */
	void init(Object caller) throws Exception;
}
