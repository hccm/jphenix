/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.beans;

import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.standard.docs.ClassInfo;


/**
 * 基类接口
 * @author 刘虻
 * 2007-1-16下午04:13:57
 */
@ClassInfo({"2014-06-13 14:16","基类接口"})
public interface IBase extends IBean {

	/**
	 * 设置基本类信息从目标类
	 * @author 刘虻
	 * 2007-11-7下午05:21:42
	 * @param obj 目标类
	 */
	void setBase(Object obj);
}