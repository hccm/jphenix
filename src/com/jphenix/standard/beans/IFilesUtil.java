/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.standard.beans;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.zip.ZipFile;

import com.jphenix.standard.docs.BeanInfo;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 文件工具类接口
 * 
 * 2018-05-28 增加了写入文件内容的方法 addContent putContent
 * 
 * @author 刘虻
 * 2007-7-16上午11:36:02
 */
@ClassInfo({"2018-05-28 12:40","文件工具类接口"})
@BeanInfo({"filesutil"})
public interface IFilesUtil {
	
	/**
	 * 获得指定路径下，符合文件类型的所有文件路径
	 * @author 刘虻
	 * @param filePathList 存放搜索后的文件路径
	 * @param basePath 指定根文件夹路径（末尾不加/)
	 * @param extNames 文件扩展名 为空或者*则搜索所有文件  为/则只搜索文件夹名，多个用逗号分割
	 * @param incChild 是否包含子路径
	 * @param returnSubPath 是否返回相对路径
	 * Exception 获得文件路径失败
	 * 2006-4-1  11:26:00
	 */
    void setFilePath (
            List<String> filePathList
            ,String basePath
            ,final String extNames
            ,boolean incChild
            ,boolean returnSubPath) throws Exception;
    
    /**
     * 通过URL方式获得文件对象
     * @author 刘虻
     * @param filePathStr 文件相对路径
     * @return 对应的文件对象
     * @exception 获取文件时发生异常
     * 2006-4-8  14:01:57
     */
    File getFileByName(String filePathStr) throws Exception;
    
    
    /**
     * 设置类的根路径
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param baseClassPath 类的根路径
     */
    void setWebInfPath(String baseClassPath);

    /**
     * 获取WEB-INF路径
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @return 类的根路径
     */
    String getWebInfPath();

    
    /**
     * 获取完整路径
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param dummyPath 相对路径
     * @return 完整路径
     */
    String getAllFilePath(String dummyPath);
    
    
    /**
     * 从文件全路径中获取文件路径
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param filePathStr 文件全路径
     * @param 如果是压缩包内部路径，是否返回压缩包内部路径。 false，返回压缩包文件所在路径
     * @return 文件路径
     */
    String getFilePath(String filePathStr,boolean inZipPath);
    
    /**
     * 从文件全路径中获取文件路径（不能处理压缩包内路径）
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param filePathStr 文件全路径
     * @return 文件路径
     */
    String getFilePath(String filePathStr);
	
	/**
	 * 通过文件路径获取文件内容
	 * @author 刘虻
	 * @param filePath 文件路径
	 * @return 文件内容
	 * @throws Exception 执行发生异常
	 * 2007-07-16上午11:38:00
	 */
	String getFileContentByPath(
					String filePath,String encode) throws Exception;
	
    /**
     * 从文件路径中获得文件名
     * @author 刘虻
     * @param filePathStr 文件路径
     * @return 文件名
     * 2007-07-16上午11:38:00
     */
    String getFileName(String filePathStr);
    
    
    /**
     * 获取文件路径中,文件的扩展名
     * @author 刘虻
     * 2008-1-18下午12:32:38
     * @param filePathStr 文件路径
     * @return 文件的扩展名
     */
    String getFileExtName(String filePathStr);
    
    
    /**
     * 获取路径中的最后一个名称
     * 
     * 比如：  c:/a/b/c/   返回c
     * 		  c:/a/b/c    返回c
     * 		  C:/a/b/c/d.txt 返回d.txt
     * 
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param path 路径
     * @return 最后一个名称
     */
    String getPathLastName(String path);
    
    /**
     * 从文件路径中获取文件前名(不包括扩展名)
     * @author 刘虻
     * @param filePathStr 文件路径
     * @return 文件前名(不包括扩展名)
     * 2007-07-16上午11:38:00
     */
    String getFileBeforeName(String filePathStr);
    
    /**
     * 根据文件路径建立文件 如果目标路径不存在，则自动建立
     * @author 刘虻
     * @param filePath 文家路径
     * @return 建立好的文件对象
     * 2007-07-16上午11:38:00
     */
    File createFile(String filePath);
    
    
    /**
     * 删除指定路径
     * @author 刘虻
     * 2007-07-16上午11:38:00
     * @param path 指定路径
     * @exception 执行发生异常
     */
    void deletePath(String path) throws Exception;
    
    
    /**
     * 获取临时文件保存路径
     * @author 刘虻
     * 2007-8-23下午06:54:23
     * @return 临时文件保存路径
     */
	String getTempPath();
	
	/**
	 * 设置临时文件保存路径
	 * @author 刘虻
	 * 2007-8-23下午06:54:33
	 * @param tempPath 临时文件保存路径
	 */
	void setTempPath(String tempPath);
    
    /**
     * 设置上传文件根路径
     * 刘虻
     * 2011-5-5 下午05:49:39
     * @param uploadBasePath 上传文件根路径
     */
	void setUploadBasePath(String uploadBasePath);
	
	
	/**
	 * 获取上传文件根路径
	 * 刘虻
	 * 2011-5-5 下午05:49:58
	 * @return 上传文件根路径
	 */
	String getUploadBasePath();
	
	
	/**
	 * 获取完整的临时文件保存路径
	 * @author 刘虻
	 * 2007-8-23下午06:55:07
	 * @return 完整的临时文件保存路径
	 */
    String getAllTempPath();
    
    
	/**
	 * 设置虚拟临时路径
	 * @author 刘虻
	 * 2007-8-24下午03:20:56
	 * @param tempVirtualPath 虚拟临时路径
	 */
	void setTempVirtualPath(String tempVirtualPath);
	
	
    /**
     * 获取虚拟临时路径
     * @author 刘虻
     * 2007-8-24下午03:20:33
     * @return 虚拟临时路径
     */
	String getTempVirtualPath();
	
	
    /**
     * 通过URL路径获取压缩文件对象
     * @author 刘虻
     * 2008-7-26下午03:11:06
     * @param zipFileUrl URL路径
     * @return 压缩文件对象
     */
    ZipFile getZipFileByUrlString(String zipFileUrl);
    
    
    /**
     * 通过URL获取压缩文件对象
     * @author 刘虻
     * 2008-7-26下午03:12:13
     * @param zipFileUrl URL路径
     * @return 压缩文件对象
     */
    ZipFile getZipFileByUrl(URL zipFileUrl);
    
    
    /**
     * 通过压缩文件URL获取压缩文件物理路径
     * 
     * 	如：zip:d:/zipfile/one.war!/com/Test.class
     * 
     *  返回 d:/zipfile/one.war
     * 
     * @author 刘虻
     * 2008-7-26下午03:13:49
     * @param zipFileUrl 压缩文件URL
     * @return 压缩文件物理路径
     */
    String getZipFilePathByUrlString(String zipFileUrl);
    
    
    /**
     * 搜索压缩文件中的文件元素列表
     * @author 刘虻
     * 2008-7-26下午03:25:00
     * @param filePathList 文件元素列表序列
     * @param basePath 压缩文件url
     * @param extNameList 符合条件的扩展名序列 支持*
     * @param returnSubPath 是否返回相对路径
     */
    void searchZipFilePath (
            List<String> filePathList
            ,String basePath
            ,List<String> extNameList
            ,boolean returnSubPath);
    
    
    /**
     * 是否为压缩文件路径
     * @author 刘虻
     * 2008-7-26下午04:08:14
     * @param urlStr 测试路径
     * @return 是否为压缩文件路径
     */
    boolean isZipUrl(String urlStr);
    
    
    /**
     * 获取压缩文件内部相对路径
     * @author 刘虻
     * 2008-7-26下午05:19:07
     * @param fileUrl 压缩文件全路径
     * @return 压缩文件内部相对路径
     */
    String getZipFileChildPath(String fileUrl);
    
    
    /**
     * 获取压缩文件中指定文件读入流
     * @author 刘虻
     * 2008-7-26下午05:32:45
     * @param fileUrl 压缩文件中指定文件全路径
     * @param zipFileObj 压缩文件对象ZipFile
     *                      通过 getZipFileByUrl 活 getZipFileByUrlString 获取
     * @return 压缩文件中指定文件读入流
     * @throws Exception 执行发生异常
     */
    InputStream getZipFileInputStreamByUrl(String fileUrl,Object zipFileObj) throws Exception;
    
    
    /**
     * 通过文件路径获取文件读入流
     * @author 刘虻
     * 2008-7-26下午05:34:35
     * @param fileUrl 文件路径获取文件读入流
     * @return 文件读入流
     * @throws Exception 执行发生异常
     */
    InputStream getFileInputStreamByUrl(String fileUrl) throws Exception;
    
    
    /**
     * 判断压缩包中是否存在该文件
     * @author 刘虻
     * 2008-7-27下午07:30:11
     * @param filePath 文件URL  zip:/c:/a.zip!/com/a.class
     * @return 是否存在
     */
    boolean isZipFileExist(String filePath);
    
    
    /**
     * 判断文件是否存在
     * @author 刘虻
     * 2008-7-27下午07:31:02
     * @param filePath 文件路径
     * @return 文件是否存在
     */
    boolean isFileExist(String filePath);
    
    /**
     * 获取压缩文件中指定文件元素的时间
     * @author 刘虻
     * 2008-7-27下午08:46:25
     * @param filePath 文件元素全路径 zip:c:/jar/a.class
     * @return 文件时间
     */
    long getZipFileTime(String filePath);
    
    
    /**
     * 获取文件最后更新时间
     * @author 刘虻
     * 2008-7-27下午08:47:36
     * @param filePath 文件路径
     * @return 文件最后更新时间
     */
    long getFileLastModified(String filePath);
    
    
    /**
     * 通过当前类加载器确定文件的绝对路径
     * @author 刘虻
     * 2009-7-17上午10:58:38
     * @param filePath 文件相对路径
     * @return 文件绝对路径
     * @throws Exception 执行发生异常
     */
    String getContextClassLoaderResource(String filePath) throws Exception;
    
    /**
     * 关闭压缩文件对象
     * @param zipFileObj 压缩文件对象
     * 2015年7月16日
     * @author 马宝刚
     */
    void closeZipFile(Object zipFileObj);
    
	/**
	 * 将网站文件相对路径，转换成绝对路径
	 * @param webSubPath 文件的相对路径（相对网站根路径）
	 * @return 文件的绝对路径
	 * 2017年9月11日
	 * @author MBG
	 */
	String webPath(String webSubPath);
	
	
    /**
     * 将指定内容追加写入文件
     * @param filePath   文件绝对路径（如果文件不存在，会自动建立）
     * @param content    写入内容字符串 (UTF-8编码）
     * @return           是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    boolean addContent(String filePath,String content);
    
    
    /**
     * 将指定内容写入文件
     * @param filePath   文件绝对路径（如果文件不存在，会自动建立）
     * @param content    写入内容字符串
     * @param encoding   写入内容编码格式
     * @param append     是否为追加写入
     * @return           是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    boolean putContent(String filePath,String content,String encoding,boolean append);
    
    /**
     * 将指定内容写入文件
     * @param filePath  文件相对路径（如果文件不存在，会自动建立。相对于WEB-INF文件夹）
     * @param contents  写入内容字节数组
     * @param append    是否为追加写入
     * @return          是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    boolean putContent(String filePath,byte[] contents,boolean append);
}
