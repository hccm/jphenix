/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.beans;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 自动设置父类接口
 * 
 * 在子类加载器中加载一个类，如果这个类实现了这个接口
 * 当前子类加载器会检查是否存在父类同样主键的类实例，如果存在，则调用接口中的方法
 * 设置到当前类实例中
 * 
 * 注意：父类与当前类不能在同一个类加载器中，
 * 		因为两个类的主键相同，后一个类信息会覆盖前一个类信息
 * 
 * @author 刘虻
 * 2010-6-22 下午02:55:38
 */
@ClassInfo({"2014-06-13 14:17","自动设置父类接口"})
public interface IParent {

	/**
	 * 设置父类
	 * 刘虻
	 * 2010-6-22 下午03:33:36
	 * @param parent 父类
	 */
	void setParent(Object parent);
}
