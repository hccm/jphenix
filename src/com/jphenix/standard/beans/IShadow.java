/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.beans;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 影子类接口
 * 
 * 三种获取类实例模式
 * 
 * 	1.每次获取新的类实例
 * 	2.每次获取同一个类实例
 * 	3.每次获取到的类实例中，一部分是常驻内存的，一部分是新构造的
 * 
 * 	我们称一部分常驻内存的类为影子类
 *
 * 
 * @author 刘虻
 * 2009-12-4 上午11:01:01
 */
@ClassInfo({"2014-06-13 14:17","影子类接口"})
public interface IShadow {
	
	/**
	 * 获取影子类实例
	 * 刘虻
	 * 2009-12-4 上午11:03:49
	 * @return 影子类实例
	 */
	<T> T shadowInstance();
}
