/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.lang;

import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 记录集行接口
 * @author 马宝刚
 * 2014年6月12日
 */
@ClassInfo({"2014-06-13 14:47","记录集行接口"})
public interface IResultRow {
    
    /**
     * 行记录容器   key:主键  value:值
     * @return 容器
     */
    Map<String,String>  _getMap();
}
