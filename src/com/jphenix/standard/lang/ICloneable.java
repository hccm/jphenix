/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.lang;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 实现克隆接口
 * @author 刘虻
 * 2007-4-19下午04:43:23
 */
@ClassInfo({"2014-06-04 12:43","实现克隆接口"})
public interface ICloneable {
	
	/**
	 * 克隆方法
	 * @author 刘虻
	 * 2007-4-19下午04:45:14
	 * @return 克隆后的类实例
	 * @exception 克隆发生异常
	 */
	Object clone() throws CloneNotSupportedException;
}
