/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-13
 * V4.0
 */
package com.jphenix.standard.lang;

import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 记录集对象接口
 * @author 马宝刚
 * 2014年6月12日
 */
@ClassInfo({"2014-06-13 14:47","记录集对象接口"})
public interface IResult {
    
    /**
     * 获取记录集
     * @return 记录集序列  element:HashMap:key:主键 value:值
     * 2014年6月12日
     * @author 马宝刚
     */
    List<Map<String,String>> _getResult();
}
