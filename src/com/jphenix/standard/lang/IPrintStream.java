/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.lang;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 打印输出流
 * @author 刘虻
 * 2006-11-27上午11:50:31
 */
@ClassInfo({"2014-06-04 12:44","打印输出流IPrintStream"})
public interface IPrintStream {

	/**
	 * 设置核心类实例（输出流类实例）
	 * @author 刘虻
	 * 2006-11-27下午12:53:05
	 * @param obj 核心类实例
	 */
	void setKernel(Object obj);
	
	
	/**
	 * 获取核心类实例 （输出流类实例）
	 * @author 刘虻
	 * 2006-11-27下午12:54:07
	 * @return 核心类实例
	 */
	Object getKernel();
	
	/**
	 * 添加内容
	 * @author 刘虻
	 * 2006-11-27上午12:07:04
	 * @param value 内容
	 */
	IPrintStream append(Object value);
	
	/**
	 * 是否为空
	 * @author 刘虻
	 * 2007-3-26下午11:30:12
	 * @return true空
	 */
	boolean isEmpty();
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27上午12:06:35
	 */
    @Override
    String toString();
	
	/**
	 * 设置输出编码格式
	 * @author 刘虻
	 * 2007-7-24下午01:42:31
	 * @param outEncoding 输出编码格式
	 */
	void setOutEncoding(String outEncoding);
	
	
	/**
	 * 获取输出编码格式
	 * @author 刘虻
	 * 2007-7-24下午01:42:42
	 * @return 输出编码格式
	 */
	String getOutEncoding();
	
	/**
	 * 设置输入编码格式
	 * @author 刘虻
	 * 2007-7-24下午01:46:51
	 * @param inEncoding 输入编码格式
	 */
	void setInEncoding(String inEncoding);
	
	/**
	 * 获取输入编码格式
	 * @author 刘虻
	 * 2007-7-24下午01:47:04
	 * @return 输入编码格式
	 */
	String getInEncoding();
	
	/**
	 * 输出数据（末尾带换行符）
	 * @param value 数据
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	IPrintStream println(Object value);
	
	/**
	 * 输出换行符
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	IPrintStream println();
	
	/**
	 * 输出数据
	 * @param value 数据
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	IPrintStream print(Object value);
}
