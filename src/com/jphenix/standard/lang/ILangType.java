/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.standard.lang;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 标准类型名常量
 * @author 刘虻
 * 2007-2-26上午10:16:41
 */
@ClassInfo({"2014-06-04 12:43","标准类型名常量"})
public interface ILangType {
	
	/**
	 * 类型标识 String
	 */
	String TYPE_String = "String";
	
	/**
	 * 类型标识 String数组
	 */
	String TYPE_Strings = "Strings"; //字符串数组类型标识
	
	/**
	 * 类型标识 int
	 */
	String TYPE_int = "int";
	
	/**
	 * 类型标识 int数组
	 */
	String TYPE_ints = "ints"; 
	
	/**
	 * 类型标识 Integer
	 */
	String TYPE_Integer = "Integer";
	
	/**
	 * 类型标识 Integer数组
	 */
	String TYPE_Integers = "Integers";
	
	/**
	 * 类型标识 long
	 */
	String TYPE_long = "long";
	
	/**
	 * 类型标识 long数组
	 */
	String TYPE_longs = "longs";
	
	/**
	 * 类型标识 Long
	 */
	String TYPE_Long = "Long";
	
	/**
	 * 类型标识 Long数组
	 */
	String TYPE_Longs = "Longs";
	
	/**
	 * 类型标识 boolean
	 */
	String TYPE_boolean = "boolean";
	
	/**
	 * 类型标识 boolean数组
	 */
	String TYPE_booleans = "booleans";
	
	/**
	 * 类型标识 Boolean
	 */
	String TYPE_Boolean = "Boolean"; 
	
	/**
	 * 类型标识 Boolean数组
	 */
	String TYPE_Booleans = "Booleans";

	/**
	 * 类型标识Map
	 */
	String TYPE_Map = "Map";
	
	/**
	 * 类型标识XML
	 */
	String TYPE_XML = "XML";
	
	/**
	 * 类型标识Map数组
	 */
	String TYPE_Maps = "Maps";
	
	/**
	 * 类型标识 HashMap
	 */
	String TYPE_HashMap = "HashMap";
	
	/**
	 * 类型标识 HashMap数组
	 */
	String TYPE_HashMaps = "HashMaps";

	/**
	 * 类型标识 List
	 */
	String TYPE_List = "List";
	
	/**
	 * 类型标识 List数组
	 */
	String TYPE_Lists = "Lists";
	
	/**
	 * 类型标识 ArrayList
	 */
	String TYPE_ArrayList = "ArrayList";
	
	/**
	 * 类型标识 ArrayList数组
	 */
	String TYPE_ArrayLists = "ArrayLists";
	
	/**
	 * 类型标识 数组
	 */
	String TYPE_Array = "Array";
	
	/**
	 * 路径  设置值如果是相对路径，会自动转换为绝对路径
	 */
	String TYPE_PATH = "Path";
	
	/**
	 * 类型标识 byte
	 */
	String TYPE_byte = "byte";
	
	/**
	 * 类型标识 byte数组
	 */
	String TYPE_bytes = "bytes";
	
	/**
	 * 类型标识 Byte
	 */
	String TYPE_Byte = "Byte";
	
	/**
	 * 类型标识 Byte数组
	 */
	String TYPE_Bytes = "Bytes";
	
	/**
	 * 类型标识 float
	 */
	String TYPE_float = "float";
	
	/**
	 * 类型标识 float数组
	 */
	String TYPE_floats = "floats";
	
	/**
	 * 类型标识 Float
	 */
	String TYPE_Float = "Float";
	
	/**
	 * 类型标识 Float数组
	 */
	String TYPE_Floats = "Floats";
	
	/**
	 * 类型标识 double
	 */
	String TYPE_double = "double";
	
	/**
	 * 类型标识 double数组
	 */
	String TYPE_doubles = "doubles";
	
	/**
	 * 类型标识 Double
	 */
	String TYPE_Double = "Double";
	
	/**
	 * 类型标识 Double数组
	 */
	String TYPE_Doubles = "Doubles";
	
	/**
	 * 类型标识 Hashtable
	 */
	String TYPE_Hashtable = "Hashtable";
	
	/**
	 * 类型标识 Hashtable数组
	 */
	String TYPE_Hashtables = "Hashtables";
	
	/**
	 * 类型标识  Class类
	 */
	String TYPE_Class = "Class";
	
	/**
	 * 类型标识  Class类数组
	 */
	String TYPE_Classes = "classes";
	
	/**
	 * 类型标识 Vector
	 */
	String TYPE_Vector = "Vector";
	
	/**
	 * 类型标识 Vector数组
	 */
	String TYPE_Vectors = "Vectors";
	
	/**
	 * 类型标识 Properties
	 */
	String TYPE_Properties = "Properties";
	
	/**
	 * 类型标识 Properties数组
	 */
	String TYPE_Propertiess = "Propertiess";
	
	/**
	 * 类型标识 Date
	 */
	String TYPE_Date = "Date";
	
	/**
	 * 类型标识 Date数组
	 */
	String TYPE_Dates = "Dates";
	
	/**
	 * 类型标识 char
	 */
	String TYPE_char = "char";
	
	/**
	 * 类型标识 char数组
	 */
	String TYPE_chars = "chars";
	
	/**
	 * 类型标识 StringBuffer
	 */
	String TYPE_StringBuffer = "StringBuffer";
	
	/**
	 * 类型标识 StringBuffer数组
	 */
	String TYPE_StringBuffers = "StringBuffers";
	
	/**
	 *  类型标识 Object
	 */
	String TYPE_Object = "Object";
	
	/**
	 * 类型标识 Object数组
	 */
	String TYPE_Objects = "Objects";
	
	/**
	 * 类型标识 Exception
	 */
	String TYPE_Exception = "Exception";
	
	/**
	 * 类型标识 Exception数组
	 */
	String TYPE_Exceptions = "Exceptions";
	
	/**
	 * ref类型标识
	 */
	String TYPE_ref = "ref";
	
	
	/**
	 * ref类型标识数组
	 */
	String TYPE_refs = "refs";
	
	/**
	 * 空参数标识
	 */
	String TYPE_NULL = "null";
	
	/**
	 * 空参数数组标识
	 */
	String TYPE_NULLs = "nulls";
}
