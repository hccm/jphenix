/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.printstream;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.IPrintStream;

/**
 * 将类中的内容输出到字符串工具
 * @author 刘虻
 * 2006-11-26下午11:52:48
 */
@ClassInfo({"2014-06-12 20:01","将类中的内容输出到字符串工具"})
public class StringPrintStreamTool implements IPrintStream {

	protected StringBuffer stringBuffer = new StringBuffer(); //字符串缓存
	protected boolean isEmpty = true; //是否为空
	protected String inEncoding = null; //输入编码格式
	protected String outEncoding = null; //输出编码格式
	
	/**
	 * 构造函数
	 * 2006-11-27下午01:01:22
	 */
	public StringPrintStreamTool() {
		super();
	}
	
	/**
	 * 获取一个新的StringBuffer核心的实例
	 * @author 刘虻
	 * 2006-11-27上午11:33:09
	 * @return 新的StringBuffer核心的实例
	 */
	public static StringPrintStreamTool getStringBufferInstance() {
		return new StringPrintStreamTool(new StringBuffer());
	}
	
	/**
	 * 构造函数
	 * 2006-11-26下午11:52:48
	 */
	public StringPrintStreamTool(StringBuffer stringBuffer) {
		this.stringBuffer = stringBuffer;
	}
	
	
	/**
	 * 添加内容
	 * @author 刘虻
	 * 2006-11-27上午12:07:04
	 * @param value 内容
	 */
	@Override
    public IPrintStream append(Object value) {
		return print(value);
	}
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream println(Object value) {
		if (value!=null && stringBuffer!=null) {
			if (outEncoding==null) {
				stringBuffer.append(value).append("\n");
				isEmpty = false;
			}else {
				try {
					if (inEncoding!=null) {
						stringBuffer.append(new String(value.toString().getBytes(inEncoding),outEncoding)).append("\n");
						isEmpty = false;
					}else {
						stringBuffer.append(new String(value.toString().getBytes(),outEncoding)).append("\n");
						isEmpty = false;
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return this;
	}
	
	/**
	 * 输出换行符
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream println() {
		if(stringBuffer!=null) {
			stringBuffer.append("\n");
		}
		return this;
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream print(Object value) {
		if (value!=null && stringBuffer!=null) {
			if (outEncoding==null) {
				stringBuffer.append(value);
				isEmpty = false;
			}else {
				try {
					if (inEncoding!=null) {
						stringBuffer.append(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						stringBuffer.append(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return this;
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27上午12:06:35
	 */
	@Override
    public String toString() {
		if (stringBuffer!=null) {
			return stringBuffer.toString();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:57:53
	 */
	@Override
    public Object getKernel() {
		return stringBuffer;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:58:22
	 */
	@Override
    public void setKernel(Object obj) {
		if (obj!=null && obj instanceof StringBuffer) {
			stringBuffer = (StringBuffer)obj;
		}
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-3-26下午11:35:11
	 */
	@Override
    public boolean isEmpty() {
		return isEmpty;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:55:58
	 */
	@Override
    public String getInEncoding() {
		return inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:56:11
	 */
	@Override
    public String getOutEncoding() {
		return outEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:56:27
	 */
	@Override
    public void setInEncoding(String inEncoding) {
		this.inEncoding = inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:56:34
	 */
	@Override
    public void setOutEncoding(String outEncoding) {
		this.outEncoding = outEncoding;
	}
}
