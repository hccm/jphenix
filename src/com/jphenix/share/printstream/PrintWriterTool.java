/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.printstream;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.IPrintStream;

import java.io.PrintWriter;

/**
 * 服务输出流工具类
 * @author 刘虻
 * 2006-11-27下午12:43:55
 */
@ClassInfo({"2014-06-12 19:59","服务输出流工具类"})
public class PrintWriterTool implements IPrintStream {

	protected PrintWriter printWriter = null; //服务输出流
	protected boolean isEmpty = true; //是否为空
	protected String inEncoding = null; //输入编码
	protected String outEncoding = null; //输出编码
	protected boolean outInfoToConsole = false; //是否将输出内容也输出到控制台中
	
	/**
	 * 构造函数
	 * 2006-11-27下午01:00:44
	 */
	public PrintWriterTool() {
		super();
	}
	
	/**
	 * 构造函数
	 * 2006-11-27下午12:43:55
	 */
	public PrintWriterTool(
			PrintWriter printWriter) {
		super();
		this.printWriter = printWriter;
	}

	/**
	 * 构造函数
	 * 2007-7-24下午02:01:02
	 */
	public PrintWriterTool(
			PrintWriter printWriter
			,String outEncoding) {
		super();
		this.printWriter = printWriter;
		setOutEncoding(outEncoding);
	}
	
	/**
	 * 构造函数
	 * 2007-7-24下午02:01:21
	 */
	public PrintWriterTool(
			PrintWriter printWriter
			,String inEncoding
			,String outEncoding) {
		super();
		this.printWriter = printWriter;
		setInEncoding(inEncoding);
		setOutEncoding(outEncoding);
	} 
	
	/**
	 * 构造函数
	 * @param outInfoToConsole 是否将输出内容也输出到控制台中
	 * 2007-7-24下午02:01:21
	 */
	public PrintWriterTool(
			PrintWriter printWriter
			,String inEncoding
			,String outEncoding
			,boolean outInfoToConsole) {
		super();
		this.outInfoToConsole = outInfoToConsole;
		this.printWriter = printWriter;
		setInEncoding(inEncoding);
		setOutEncoding(outEncoding);
	} 
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream append(Object value) {
		if(outInfoToConsole) {
			System.out.print(value);
		}
		return print(value);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream println(Object value) {
		if (value!=null && printWriter!=null) {
			if (outEncoding==null) {
				try {
					printWriter.write(value.toString());
					printWriter.write('\n');
					isEmpty = false;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else {
				try {
					if (inEncoding!=null) {
						printWriter.write(new String(value.toString().getBytes(inEncoding),outEncoding));
						printWriter.write('\n');
						isEmpty = false;
					}else {
						printWriter.write(new String(value.toString().getBytes(),outEncoding));
						printWriter.write('\n');
						isEmpty = false;
					}
				}catch(Exception e) {
					System.err.println("ServletOutputStream Print Error:"+e);
				}
			}
		}
		return this;
	}
	
	/**
	 * 输出换行符
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream println() {
		if(printWriter!=null) {
			try {
				printWriter.write('\n');
			}catch(Exception e) {
				System.err.println("ServletOutputStream Print Error:"+e);
			}
		}
		return this;
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream print(Object value) {
		if (value!=null && printWriter!=null) {
			if (outEncoding==null) {
				try {
					printWriter.write(value.toString());
					isEmpty = false;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else {
				try {
					if (inEncoding!=null) {
						printWriter.write(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						printWriter.write(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					System.err.println("ServletOutputStream Print Error:"+e);
				}
			}
		}
		return this;
	}
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:57:35
	 */
	@Override
    public Object getKernel() {
		return printWriter;
	}

	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-1-4下午01:45:40
	 */
	@Override
    public String toString() {
		return "";
	}
	
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:57:29
	 */
	@Override
    public void setKernel(Object obj) {
		if (obj!=null && obj instanceof PrintWriter) {
			printWriter = (PrintWriter)obj;
		}
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-3-26下午11:32:15
	 */
	@Override
    public boolean isEmpty() {
		return isEmpty;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:37
	 */
	@Override
    public String getInEncoding() {
		return inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:43
	 */
	@Override
    public String getOutEncoding() {
		return outEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:52
	 */
	@Override
    public void setInEncoding(String inEncoding) {
		this.inEncoding = inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:51:08
	 */
	@Override
    public void setOutEncoding(String outEncoding) {
		this.outEncoding = outEncoding;
	}

}
