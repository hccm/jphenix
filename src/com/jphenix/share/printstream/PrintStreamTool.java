/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.printstream;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.IPrintStream;

import java.io.PrintStream;

/**
 * 输出流工具
 * @author 刘虻
 * 2006-11-27下午12:39:15
 */
@ClassInfo({"2014-06-12 19:59","输出流工具"})
public class PrintStreamTool implements IPrintStream {

	protected PrintStream printStream = null; //输出流
	protected boolean isEmpty = true; //是否为空
	protected String outEncoding = null; //输出编码格式
	protected String inEncoding = null; //输入编码格式
	
	/**
	 * 构造函数
	 * 2006-11-27下午12:59:40
	 */
	public PrintStreamTool() {
		super();
	}
	
	/**
	 * 构造函数
	 * 2006-11-27下午12:39:15
	 */
	public PrintStreamTool(PrintStream printStream) {
		super();
		this.printStream = printStream;
	}
	
	/**
	 * 构造函数
	 * 2007-7-24下午02:00:25
	 */
	public PrintStreamTool(PrintStream printStream,String outEncoding) {
		super();
		this.printStream = printStream;
		setOutEncoding(outEncoding);
	}
	
	/**
	 * 构造函数
	 * 2007-7-24下午02:00:04
	 */
	public PrintStreamTool(PrintStream printStream,String inEncoding,String outEncoding) {
		super();
		this.printStream = printStream;
		setInEncoding(inEncoding);
		setOutEncoding(outEncoding);
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:39:15
	 */
	@Override
    public IPrintStream append(Object value) {
		return print(value);
	}
	
	/**
	 * 输出换行符
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream println() {
		if(printStream!=null) {
			try {
				printStream.println();
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		return this;
	}
	
	/**
	 * 输出数据（末尾带换行符）
	 * @param value 数据
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream println(Object value) {
		if (value!=null && printStream!=null) {
			if (outEncoding==null) {
				printStream.println(value);
				isEmpty = false;
			}else {
				try {
					if (inEncoding!=null) {
						printStream.println(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						printStream.println(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return this;
	}
	
	/**
	 * 输出数据
	 * @param value 数据
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream print(Object value) {
		if (value!=null && printStream!=null) {
			if (outEncoding==null) {
				printStream.print(value);
				isEmpty = false;
			}else {
				try {
					if (inEncoding!=null) {
						printStream.print(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						printStream.print(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return this;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:55:54
	 */
	@Override
    public Object getKernel() {
		return printStream;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:55:59
	 */
	@Override
    public void setKernel(Object obj) {
		if (obj != null && obj instanceof PrintStream) {
			printStream = (PrintStream)obj;
		}
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-3-26下午11:31:05
	 */
	@Override
    public boolean isEmpty() {
		return isEmpty;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:47:58
	 */
	@Override
    public String getInEncoding() {
		return inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:48:08
	 */
	@Override
    public String getOutEncoding() {
		return outEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:48:17
	 */
	@Override
    public void setInEncoding(String inEncoding) {
		this.inEncoding = inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:48:31
	 */
	@Override
    public void setOutEncoding(String outEncoding) {
		this.outEncoding = outEncoding;
	}

}
