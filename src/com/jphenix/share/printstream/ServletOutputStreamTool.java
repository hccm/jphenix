/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.printstream;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.IPrintStream;

import javax.servlet.ServletOutputStream;

/**
 * 服务输出流工具类
 * @author 刘虻
 * 2006-11-27下午12:43:55
 */
@ClassInfo({"2014-06-12 20:02","服务输出流工具类"})
public class ServletOutputStreamTool implements IPrintStream {

	protected ServletOutputStream servletOutputStream = null; //服务输出流
	protected boolean isEmpty = true; //是否为空
	protected String inEncoding = null; //输入编码
	protected String outEncoding = null; //输出编码
	
	/**
	 * 构造函数
	 * 2006-11-27下午01:00:44
	 */
	public ServletOutputStreamTool() {
		super();
	}
	
	/**
	 * 构造函数
	 * 2006-11-27下午12:43:55
	 */
	public ServletOutputStreamTool(
			ServletOutputStream servletOutputStream) {
		super();
		this.servletOutputStream = servletOutputStream;
	}

	/**
	 * 构造函数
	 * 2007-7-24下午02:01:02
	 */
	public ServletOutputStreamTool(
			ServletOutputStream servletOutputStream
			,String outEncoding) {
		super();
		this.servletOutputStream = servletOutputStream;
		setOutEncoding(outEncoding);
	}
	
	/**
	 * 构造函数
	 * 2007-7-24下午02:01:21
	 */
	public ServletOutputStreamTool(
			ServletOutputStream servletOutputStream
			,String inenString
			,String outEncoding) {
		super();
		this.servletOutputStream = servletOutputStream;
		setInEncoding(inenString);
		setOutEncoding(outEncoding);
	} 
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream append(Object value) {
		return print(value);
	}
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream println(Object value) {
		if (value!=null && servletOutputStream!=null) {
			if (outEncoding==null) {
				try {
					servletOutputStream.println(value.toString());
					isEmpty = false;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else {
				try {
					if (inEncoding!=null) {
						servletOutputStream.println(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						servletOutputStream.println(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					System.err.println("ServletOutputStream Print Error:"+e);
				}
			}
		}
		return this;
	}
	
	/**
	 * 输出换行符
	 * @return 当前类实例
	 * 2014年8月15日
	 * @author 马宝刚
	 */
	@Override
    public IPrintStream println() {
		if(servletOutputStream!=null) {
			try {
				servletOutputStream.println();
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		return this;
	}
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:43:55
	 */
	@Override
    public IPrintStream print(Object value) {
		if (value!=null && servletOutputStream!=null) {
			if (outEncoding==null) {
				try {
					servletOutputStream.print(value.toString());
					isEmpty = false;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else {
				try {
					if (inEncoding!=null) {
						servletOutputStream.print(new String(value.toString().getBytes(inEncoding),outEncoding));
						isEmpty = false;
					}else {
						servletOutputStream.print(new String(value.toString().getBytes(),outEncoding));
						isEmpty = false;
					}
				}catch(Exception e) {
					System.err.println("ServletOutputStream Print Error:"+e);
				}
			}
		}
		return this;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:57:35
	 */
	@Override
    public Object getKernel() {
		return servletOutputStream;
	}

	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-1-4下午01:45:40
	 */
	@Override
    public String toString() {
		return "";
	}
	
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2006-11-27下午12:57:29
	 */
	@Override
    public void setKernel(Object obj) {
		if (obj!=null && obj instanceof ServletOutputStream) {
			servletOutputStream = (ServletOutputStream)obj;
		}
	}


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-3-26下午11:32:15
	 */
	@Override
    public boolean isEmpty() {
		return isEmpty;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:37
	 */
	@Override
    public String getInEncoding() {
		return inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:43
	 */
	@Override
    public String getOutEncoding() {
		return outEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:50:52
	 */
	@Override
    public void setInEncoding(String inEncoding) {
		this.inEncoding = inEncoding;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-7-24下午01:51:08
	 */
	@Override
    public void setOutEncoding(String outEncoding) {
		this.outEncoding = outEncoding;
	}

}
