/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 * 双精度处理类
 * 
 * com.jphenix.share.lang.SDouble
 * 
 * 2007-07-11 支持以下功能
 * 	数字长 6  小数位数2 传入参数 012345  整理后： 123.45 可用与数据导入
 * 
 * 2018-04-02 去掉了总长度参数，不限总长度
 * 2018-06-11 修改了传入数字，并且传入小数点位数，返回的数字并没有按照小数点位数做截取
 *            增加了设置是否对小数做四舍五入，还是只做小数位数截取
 * 2018-08-06 增加了强制输出非科学记数法的数值字符串
 * 2019-03-21 修改了处理小数点位数时，四舍五入后数值错误的问题
 * 2019-04-08 增加了处理科学计数法字符串的方法
 * 2019-04-11 增加了处理3.03E-1方根处理代码
 * 2019-04-12 修改了四舍五入时，数字负号丢失问题
 * 2019-04-18 修改了四舍五入错误
 * 2019-05-14 重构了内部部分代码，处理了四舍五入后补零问题
 * 2019-06-11 解决了科学计数法处理错误
 * 
 * @author 刘虻
 * 2006-8-16  13:33:14
 */
@ClassInfo({"2019-06-11 13:01","双精度处理类"})
public class SDouble implements ICloneable {
	
    protected String  doubleString = null;   //浮点字符串
    protected int     pointDigit   = -1;     //小数点位数
    protected boolean isDoCut      = false;  //是否不做四舍五入，只做截取
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-8-16 13:33:14
     */
    public SDouble() {
        super();
    }
    
    /**
     * 构造函数
     * 2008-6-5上午11:41:41
     */
    public SDouble(double doubleValue,String format) {
    	setFormat(format);
    	setDoubleValue(doubleValue);
    }
    
    /**
     * 构造函数
     * @author 刘虻
     * @param doubleValue 待处理的浮点数
     * @param pointDigit 小数点保留位数
     */
    public SDouble(double doubleValue,int pointDigit) {
    	setPointDigit(pointDigit);
    	setDoubleValue(doubleValue);
    }
    
    /**
     * 构造函数
     * 2007-8-24下午01:15:36
     */
    public SDouble(Object doubleObj,String format){
    	super();
    	setFormat(format);
    	setDoubleObject(doubleObj);
    }
    
    /**
     * 构造函数
     * @author 刘虻
     * @param doubleObj 待处理的数字
     * @param pointDigit 小数点保留位数
     */
    public SDouble(Object doubleObj,int pointDigit){
    	super();
        setPointDigit(pointDigit);
    	setDoubleObject(doubleObj);
    }
    
    /**
     * 构造函数
     * 2007-8-24下午01:16:09
     */
    public SDouble(Object doubleObj){
    	super();
    	setDoubleObject(doubleObj);
    }
    
    /**
     * 整理要输出的浮点字符串，小数位数不足的补0
     * @author 刘虻
     * 2006-9-11上午10:34:05
     * @param source 原字符串
     * @return 整理后的字符串
     */
    protected String fixOutDoubleString(String source) {
    	if(source==null || source.length()<1) {
    		source = "0";
    	}
    	if(pointDigit<0) {
    		return source;
    	}
    	
    	if(isDoCut) {
    		//不做四舍五入
    		String zeroStr  = "";
    		for(int i=0;i<pointDigit;i++) {
    			zeroStr += "0";
    		}
    		return (new DecimalFormat("#."+zeroStr)).format((new BigDecimal(source)).doubleValue());
    	}
    	return String.format("%."+pointDigit+"f",(new BigDecimal(source)).doubleValue());
    }
    

    /**
     * 获得0字符串
     * @author 刘虻
     * 2006-9-11上午10:35:54
     * @param num 多少个0
     * @return 指定0数量的字符串 
     */
    protected String getZero(int num) {
    	String reStr = "";
    	for (int i=0;i<num;i++) {
    		reStr += "0";
    	}
    	return reStr;
    }
    
    /**
     * 执行解析
     * @author 刘虻
     * 2006-8-16  13:49:02
     */
    protected String doExecute(String doubleStr) {
    	if ("infinity".equalsIgnoreCase(doubleStr)) {
    		//判断如果字符串值是 英菲尼迪，就返回0，爱咋咋地，要啥自行车啊
    		doubleStr = "0";
    	}
        //参数合法化
        if (doubleStr==null 
                || doubleStr.length()<1) {
            doubleStr = "0";
            return "0";
        }
        //去掉头尾无用的字符
        doubleStr = cutStart(doubleStr,new String[] {" ","	","0"});
        doubleStr = cutEnd(doubleStr,new String[] {" ","	"});
        
        if (doubleStr.length()<1 || doubleStr.startsWith(".")) {
        	doubleStr = "0" + doubleStr;
        }
        if (doubleStr.endsWith(".")) {
        	//去掉末尾的点儿
        	doubleStr = doubleStr.substring(0,doubleStr.length()-1);
        }
        try {
        	(new Double(doubleStr)).doubleValue();
        }catch(Exception e) {
        	return "0";
        }
        //之前用的是按照.分隔字符串，实际上不存在多个.否则在上面的try就因为异常返回0了
        int point = doubleStr.indexOf(".");
        //处理科学计数法
        int ePoint = doubleStr.toLowerCase().indexOf("e");
        if(ePoint>0) {
        	try {
        		//幂或方根
	        	int mi = Integer.valueOf(doubleStr.substring(ePoint+1));
	        	//之前的数
	        	doubleStr = doubleStr.substring(0,ePoint);
	        	if(mi>0) {
	        		//10的mi次幂
		        	if(point>0) {
		        		//当前小数位数
		        		int p = doubleStr.length()-point-1;
		        		//去掉小数点
		        		doubleStr = doubleStr.substring(0,point)+doubleStr.substring(point+1);
		        		if(p<mi) {
		        			mi -= p;
		        		}else {
		        			p = doubleStr.length()-(p-mi);
		        			mi = 0;
		        			doubleStr = doubleStr.substring(0,p)+"."+doubleStr.substring(p);
		        		}
		        	}
	    			for(int i=0;i<mi;i++) {
	    				doubleStr += 0;
	    			}
	        	}else {
	        		//10的mi次方根
	        		mi = Math.abs(mi);
	        		String beforeVal; //小数点前面的数值
	        		String afterVal;  //小数点后面的数值
	        		if(point>0) {
	        			//之前存在小数点
	        			beforeVal = doubleStr.substring(0,point);
	        			afterVal  = doubleStr.substring(point+1);
	        			if(beforeVal.length()>mi) {
	        				afterVal = beforeVal.substring(beforeVal.length()-mi)+afterVal;
	        				beforeVal = beforeVal.substring(0,beforeVal.length()-mi);
	        			}else {
	        				mi        = mi-beforeVal.length();
	        				afterVal  = beforeVal+afterVal;
	        				beforeVal = "0";
	        				for(int i=0;i<mi;i++) {
	        					afterVal = "0"+afterVal;
	        				}
	        			}
	        		}else {
	        			beforeVal = doubleStr;
	        			afterVal  = "";
	        			if(beforeVal.length()>mi) {
	        				afterVal = beforeVal.substring(beforeVal.length()-mi);
	        				beforeVal = beforeVal.substring(0,beforeVal.length()-mi);
	        			}else {
	        				mi        = mi-beforeVal.length();
	        				afterVal  = beforeVal;
	        				beforeVal = "0";
	        				for(int i=0;i<mi;i++) {
	        					afterVal = "0"+afterVal;
	        				}
	        			}
	        		}
	        		doubleStr = beforeVal+"."+afterVal;
	        	}
        	}catch(Exception e) {
        		return "0";
        	}
        	point = doubleStr.indexOf(".");
        }
        if(point<0) {
            return doubleStr;
        }
        //处理四舍五入
        return fixOutDoubleString(doubleStr);
    }
   
    /**
     * 将字符串按指定字符串分割成数组
     * @author 刘虻
     * @param str 被分割的字符串
     * @param splitStr 指定的字符串
     * @return 分割后的数组
     * 2006-3-15  15:37:36
     */
    protected String[] splitString(String str,String splitStr) {
        if (str==null) {
            return null;
        }
        if (splitStr==null || splitStr.length()==0) {
            return new String[] {str,null};
        }
        //放入分割后的段容器
        Vector<String> vect = new Vector<String>();
        //临时段
        String strSub = null;
        while(str.indexOf(splitStr) > -1) {
            strSub = str.substring(0,str.indexOf(splitStr));
            vect.add(strSub);
            str = str.substring(strSub.length());

            if (str.length() > splitStr.length()) {
                str = str.substring(splitStr.length());
            }else if (strSub.length()==0) {
                str = "";
                break;
            }
        }
        if (str != null && str.length()>0) {
            vect.add(str);
        }
        //构建返回值
        String[] strs = new String[vect.size()];
        vect.copyInto(strs);
        return strs;
    }
    
	/**
	 * 将字符串的结尾去掉指定字符
	 * @author 刘虻
	 * 2006-10-27下午01:51:49
	 * @param source 字符串
	 * @param subStr 指定字符
	 * @return 整理后的字符串
	 */
	protected String cutEnd(String source,String[] subStr) {
		if (source==null 
				|| source.length()==0 
				|| subStr==null) {
			return source;
		}
		while(true) {
			boolean isBreak = true; //是否结束
			for (int i=0;i<subStr.length;i++) {
				if (subStr[i]==null) {
					continue;
				}
				if (source.endsWith(subStr[i])) {
					source = 
						source.substring(0,source.length()-subStr[i].length());
					isBreak = false;
				}
			}
			if (isBreak) {
				break;
			}
		}
		return source;
		
	}
	
	/**
	 * 将字符串的开头去掉指定字符
	 * 注意:只限于无大小写或者严格区分大小写的字符串
	 * @author 刘虻
	 * 2006-10-27下午01:34:03
	 * @param source 字符串
	 * @param subStr 指定字符
	 * @return 整理后的字符串
	 */
	protected String cutStart(String source,String[] subStr) {
		
		if (source==null 
				|| source.length()==0 
				|| subStr==null) {
			return source;
		}
		
		while(true) {
			//是否截取到
			boolean isBreak = true;
			
			for (int i=0;i<subStr.length;i++) {
				if (subStr[i]==null) {
					continue;
				}
				if (source.startsWith(subStr[i])) {
					source = source.substring(subStr[i].length());
					isBreak = false;
				}
			}
			if (isBreak) {
				break;
			}
		}
		return source;
	}
	
    /**
     * 将浮点字符串转换为浮点数
     * 刘虻
     * 2011-7-13 下午04:58:39
     * @param doubleString 浮点值字符串
     * @param pointDigit 小数点后位数
     * @return 浮点数
     */
    public static double valueOf(Object doubleString,int pointDigit) {
    	return (new SDouble(doubleString,pointDigit)).getDoubleValue();
    }
    
    /**
     * 获取double值
     * @author 刘虻
     * 2007-8-24下午01:25:45
     * @param doubleString double值
     * @param format 格式  整个数字长度,小数点长度
     * @return 整理后的double值
     */
    public static double valueOf(Object doubleString,String format) {
    	return (new SDouble(doubleString,format)).getDoubleValue();
    }
    
    /**
     * 将字符串转换为浮点类型
     * @author 刘虻
     * 2006-9-20下午05:09:29
     * @param doubleString 浮点字符串
     * @return 浮点值
     */
    public static double valueOf(Object doubleString) {
    	return (new SDouble(doubleString)).getDoubleValue();
    }
    
    /**
     * 将值转换为Double
     * @author 刘虻
     * 2007-5-21下午12:32:21
     * @param doubleString 值
     * @return Double值
     */
    public static Double doubleOf(Object doubleString) {
    	return new Double(valueOf(doubleString));
    }
    
    /**
     * 整理浮点数字符串
     * 刘虻
     * 2011-7-13 下午05:02:23
     * @param doubleString 原浮点字符串
     * @param pointDigit 小数点后位数
     * @return 整理后的浮点数字字符串
     */
    public static String stringValueOf(Object doubleString,int pointDigit){
    	return (new SDouble(doubleString,pointDigit)).getDoubleString();
    }
    
    /**
     * 整理浮点数字
     * @param doubleVal 浮点值
     * @param pointDigit 小数点保留位数
     * @return 整理后的字符串
     * 2016年11月15日
     * @author MBG
     */
    public static String stringValueOf(double doubleVal,int pointDigit) {
    	return (new SDouble(doubleVal,pointDigit)).getDoubleString();
    }
    
    /**
     * 整理浮点数字
     * @param doubleVal   浮点值
     * @param pointDigit  小数点保留位数
     * @param isDoCut     是否不做四舍五入处理，只截取小数点位数
     * @return            处理后的值
     * 2019年5月14日
     * @author MBG
     */
    public static String stringValueOf(double doubleVal,int pointDigit,boolean isDoCut) {
    	return (new SDouble(doubleVal,pointDigit)).setCut(isDoCut).getDoubleString();
    }
    
    /**
     * 获取浮点字符串
     * @author 刘虻
     * 2007-12-27下午10:54:02
     * @param doubleVal 浮点值
     * @param format 浮点数格式
     * @return 浮点字符串
     */
    public static String stringValueOf(double doubleVal,String format) {
    	return (new SDouble()).setFormat(format).setDoubleValue(doubleVal).getDoubleString();
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param douObj 源数据
     * @param format 输出格式
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object douObj,String format) {
    	return (new SDouble(douObj,format)).getShowString();
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param douObj 源数据
     * @param format 输出格式
     * @param size 分割点
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object douObj,String format,int size) {
    	return (new SDouble(douObj,format)).getShowString(size);
    }
    
    /**
     * 获取浮点数字字符串
     * @author 刘虻
     * 2007-8-24下午01:27:47
     * @param doubleString 原浮点字符串
     * @param format 数字总位数,小数点后位数
     * @return 整理后的浮点数字字符串
     */
    public static String stringValueOf(Object doubleString,String format) {
    	return (new SDouble(doubleString,format)).getDoubleString();
    }
    
    /**
     * 获取浮点数字字符串
     * @author 刘虻
     * 2007-8-24下午01:27:47
     * @param doubleString 原浮点字符串
     * @return 整理后的浮点数字字符串
     */
    public static String stringValueOf(Object doubleString) {
    	return (new SDouble(doubleString)).getDoubleString();
    }
    
    /**
     * 设置数据格式  数据总长,小数长度
     * @author 刘虻
     * 2007-8-24上午11:29:42
     * @param format 数据格式
     * @return 当前类实例
     */
    public SDouble setFormat(String format) {
    	if (format!=null) {
    		//分割点 很早以前的格式为 20,3 即总长度为20，小数点保留3位
    		//这是按照数据库存储格式设计的。但是总长度参数时无用的，而且遇到超出长度时
    		//返回错误的值
    		int point = format.indexOf(",");
    		if (point>-1) {
        		try {
        			pointDigit = (new Integer(format.substring(point+1))).intValue();
        		}catch(Exception e) {
        			e.printStackTrace();
        		}
    		}else {
        		try {
        			pointDigit = (new Integer(format)).intValue();
        		}catch(Exception e) {
        			e.printStackTrace();
        		}
    		}
    	}
    	return this;
    }
    
    /**
     * 获取double字符串
     * @author 刘虻
     * 2007-7-11下午06:33:27
     * @return double字符串
     */
    public String getDoubleString() {
        return fixOutDoubleString(doubleString);
    }
    
    /**
     * 获取double字符串
     * @author 刘虻
     * 2008-6-5上午11:58:30
     * @param format 输出格式 所有位数,小数位数
     * @return double字符串
     */
    public String getDoubleFormatString(String format) {
    	setFormat(format);
    	return fixOutDoubleString(doubleString);
    }
    
    /**
     * 获取整理后的浮点字符串
     * @author 刘虻
     * @param doubleString 源浮点字符串
     * @return 整理后的浮点字符串
     * 2006-8-18  11:27:16
     */
    public String getDoubleString(String doubleString) {
    	setDoubleString(doubleString);
    	return getDoubleString();
    }
    
    /**
     * 获得整理后的浮点字符串
     * @author 刘虻
     * @param doubleValue 浮点值
     * @return 返回整理后的浮点字符串
     * 2006-8-18  11:35:05
     */
    public String getDoubleString(double doubleValue){
    	setDoubleValue(doubleValue);
    	return getDoubleString();
    }
    
    /**
     * 设置double字符串
     * @author 刘虻
     * 2007-7-11下午06:33:42
     * @param doubleString double字符串
     */
    public SDouble setDoubleString(String doubleString){
        this.doubleString = doExecute(doubleString); //执行解析
        return this;
    }
    
    /**
     * 将double值转换成字符串(主要处理科学计数法)
     * @author 刘虻
     * 2008-6-5上午11:30:07
     * @param doubleValue double值
     * @return 对应的字符串值
     */
    protected String getFixDoubleString(double doubleValue) {
    	
    	//获取浮点数字符串
    	String doubleStr = String.valueOf(doubleValue);
    	
    	int ePoint = doubleStr.indexOf("E"); //判断是否为科学计数法
    	if (ePoint<0) {
    		return doubleStr;
    	}
    	//获取位数
    	int zeroCount = 
    		(new Integer(doubleStr.substring(ePoint+1))).intValue();
    	doubleStr = doubleStr.substring(0,ePoint); //头数
    	boolean isFu = false; //是否为负数
    	if (doubleStr.startsWith("-")) {
    		isFu = true;
    		doubleStr = doubleStr.substring(1);
    	}
    	int point = doubleStr.indexOf("."); //小数点位置
    	if (point>-1) {
    		//去掉小数点
    		doubleStr = doubleStr.substring(0,point)+doubleStr.substring(point+1);
    	}
    	if(zeroCount!=0) {
    		//x*10的n次幂
    		int roundCount = zeroCount; //循环处理次数
        	if (roundCount<0) {
        		roundCount = 0-roundCount;
        	}
        	for(int i=0;i<roundCount;i++) {
        		if (zeroCount>0) {
        			point++;
        			if (point>doubleStr.length()) {
        				doubleStr = doubleStr+"0";
        			}
        		}else {
        			point--;
        			if (point<0) {
        				doubleStr = "0"+doubleStr;
        			}
        		}
        	}
        	if (point>0 && point<doubleStr.length()) {
        		doubleStr = doubleStr.substring(0,point)+"."+doubleStr.substring(point);
        	}else if (point<0) {
        		doubleStr = "0."+doubleStr;
        	}
        	if (doubleStr.indexOf(".")>-1) {
        		while(doubleStr.endsWith("0")) {
        			doubleStr = doubleStr.substring(0,doubleStr.length()-1);
        		}
        	}
    	}
    	if (isFu) {
    		return "-"+doubleStr;
    	}
    	return doubleStr;
    }
    
    /**
     * 设置浮点数
     * @author 刘虻
     * 2007-8-24下午01:07:57
     * @param doubleObject 浮点数
     * @return 当前类实例
     */
    public SDouble setDoubleObject(Object doubleObject) {
    	
    	if (doubleObject ==null) {
    		return this;
    	}
    	if (doubleObject instanceof Double) {
    		setDoubleValue(((Double)doubleObject).doubleValue());
    	}else {
    		setDoubleString(doubleObject.toString());
    	}
    	return this;
    }

    /**
     * 获取double值
     * @author 刘虻
     * 2007-7-11下午06:33:55
     * @return double值
     */
    public double getDoubleValue() {
		if(pointDigit>0) {
			doubleString = fixOutDoubleString(doubleString);
		}
		return (new Double(doubleString)).doubleValue();
    }
    
    /**
     * 获取字符串的浮点值
     * @author 刘虻
     * @param doubleString 浮点字符串
     * @return 浮点值
     * 2006-8-18  11:16:48
     */
    public double getDoubleValue(String doubleString){
    	setDoubleString(doubleString); //设置浮点字符串
    	return getDoubleValue();
    }
    
    /**
     * 获取整理后的浮点值
     * @author 刘虻
     * @param doubleValue 源浮点值
     * @return 整理后的浮点值
     * 2006-8-18  11:25:16
     */
    public double getDoubleValue(double doubleValue){
    	setDoubleValue(doubleValue); //设置浮点字符串
    	return getDoubleValue();
    }

    /**
     * 设置double值
     * @author 刘虻
     * 2007-7-11下午06:34:12
     * @param doubleValue double值
     */
    public SDouble setDoubleValue(double doubleValue) {
    	doubleString = getFixDoubleString(doubleValue);
        return this;
    }

    /**
     * 获取小数点位数
     * @author 刘虻
     * 2007-7-11下午06:34:34
     * @return 小数点位数
     */
    public int getPointDigit() {
        return pointDigit;
    }

    /**
     * 设置小数点位数
     * @author 刘虻
     * 2007-7-11下午06:36:04
     * @param pointDigit 小数点位数
     */
    public SDouble setPointDigit(int pointDigit) {
        this.pointDigit = pointDigit;
        return this;
    }
    
    /**
     * 测试入口
     * @author 刘虻
     * 2006-9-11上午10:39:06
     * @param arg0 导入参数
     */
    public static void main(String[] arg0) {
    	try {
    		String a = "-0.000001";
    		SDouble sd = new SDouble(a,"20,3");
    		System.out.println(sd.getDoubleString());
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @author 刘虻
     * 2008-12-5下午01:11:14
     * @return 显示类型的整型数据
     */
    public String getShowString() {
    	return getShowString(3);
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @author 刘虻
     * 2008-12-5下午01:11:14
     * @param size 分割点
     * @return 显示类型的整型数据
     */
    public String getShowString(int size) {
    	String douStr = getDoubleString(); //获取返回值
    	boolean isFu = false; //是否为负数
    	if (douStr.startsWith("-")) {
    		isFu = true;
    		douStr = douStr.substring(1);
    	}
    	String xStr = ""; //小数点区
    	int xPoint = douStr.indexOf("."); //截取小数点
    	if (xPoint>-1) {
    		xStr = douStr.substring(xPoint);
    		douStr = douStr.substring(0,xPoint);
    	}
    	int point = douStr.length(); //字符串指针
    	if(size<0) {
    		size = 3;
    	}
    	while(point>size) {
    		point-=size;
    		douStr = douStr.substring(0,point)+","+douStr.substring(point);
    	}
    	if (isFu) {
    		return "-"+douStr+xStr;
    	}
    	return douStr+xStr;
    }
    
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-22上午10:31:42
	 */
    @Override
    public Object clone() {
    	//构建返回值
    	SDouble sf = new SDouble();
    	sf.doubleString = doubleString;
    	sf.pointDigit = pointDigit;
    	return sf;
    }
    
    /**
     * 截取小数点位数（并非四舍五入）
     * @param doubleObj 带处理数字对象
     * @param point 截取小数点位数
     * @return 截取后的字符串信息
     * 2016年1月14日
     * @author 马宝刚
     */
    public static String cut(Object doubleObj,int point) {
        //构建返回值
        StringBuffer reSbf = new StringBuffer();
        if(doubleObj==null) {
            if(point>0) {
                reSbf.append("0.");
                for(int i=0;i<point;i++) {
                    reSbf.append("0");
                }
                return reSbf.toString();
            }else {
                return "0";
            }
        }
        if(doubleObj instanceof Double) {
            doubleObj = String.valueOf(((Double)doubleObj).doubleValue());
        }else if(doubleObj instanceof Float) {
            doubleObj = String.valueOf(((Float)doubleObj).floatValue());
        }else {
            doubleObj = doubleObj.toString();
        }
        //转换为字符串
        String doubleStr = ((String)doubleObj).trim();
        String regex = "^(-?[1-9]\\d*\\.?\\d*)|(-?0\\.\\d*[1-9])|(-?[0])|(-?[0]\\.\\d*)$";  
        if(!doubleStr.matches(regex)) {
            if(point>0) {
                reSbf.append("0.");
                for(int i=0;i<point;i++) {
                    reSbf.append("0");
                }
                return reSbf.toString();
            }else {
                return "0";
            }
        }
       //小数点分隔符
       int p = doubleStr.indexOf(".");
        if(p<1) {
            if(point>0) {
                reSbf.append(doubleStr).append(".");
                for(int i=0;i<point;i++) {
                    reSbf.append("0");
                }
                return reSbf.toString();
            }else {
                return doubleStr;
            }
        }
        reSbf.append(doubleStr, 0, p);
        if(point>0) {
            //剩下的小数点后面的数字
            doubleStr = doubleStr.substring(p+1);
            
            if(doubleStr.length()>point) {
                reSbf.append(".").append(doubleStr, 0, point);
            }else {
                reSbf.append(".").append(doubleStr);
                point = point-doubleStr.length();
                for(int i=0;i<point;i++) {
                    reSbf.append("0");
                }
            }
        }
        return reSbf.toString();
    }
    
    /**
     * 返回小数点是否只做截取不做四舍五入
     * @return 小数点是否只做截取不做四舍五入
     * 2018年6月11日
     * @author MBG
     */
    public boolean isCut() {
    	return isDoCut;
    }
    
    /**
     * 设置小数点是否只做截取不做四舍五入
     * @param isCut 小数点是否只做截取不做四舍五入
     * @return 当前类实例
     * 2018年6月11日
     * @author MBG
     */
    public SDouble setCut(boolean isCut) {
    	this.isDoCut = isCut;
    	return this;
    }
    
    
    public static String toNumberStr(Object val) {
    	return new SDouble(val).toNumberString();
    }
    
    /**
     * 转换成非科学记数法的数值字符串
     * @return 非科学记数法的数值字符串
     * 2018年8月6日
     * @author MBG
     */
    public String toNumberString() {
    	//转换为字符串
    	String douStr = String.valueOf(getDoubleValue());
    	int point = douStr.indexOf("E"); //分隔符
    	if(point<0) {
    		return douStr;
    	}
    	//小数点位数
    	int zCount = Integer.parseInt(douStr.substring(point+1));
    	douStr = douStr.substring(0,point); //去掉末尾的科学计数
    	boolean isNegative = false; //是否为负数 
    	if(douStr.startsWith("-")) {
    		isNegative = true;
    		douStr = douStr.substring(1);
    	}
    	point = douStr.indexOf("."); //是否存在小数点
    	if(point>0) {
			//去掉小数点
    		douStr = douStr.substring(0,point)+douStr.substring(point+1);
    		if(zCount>0) {
    			if(douStr.length()-point>zCount+1) {
    				zCount++;
    				douStr = douStr.substring(0,zCount)+"."+douStr.substring(zCount);
    			}else if(douStr.length()-point<zCount) {
    				zCount = zCount-douStr.length()+point;
    				for(int i=0;i<zCount;i++) {
    					douStr += "0";
    				}
    			}
    		}else {
    			zCount = Math.abs(zCount);
        		if(point>zCount) {
        			point -= zCount;
        			douStr = douStr.substring(0,point)+"."+douStr.substring(point);
        		}else {
        			zCount -= point;
        			for(int i=0;i<zCount;i++) {
        				douStr = "0"+douStr;
        			}
        			douStr = "0."+douStr;
        		}
    		}
    	}else {
    		if(zCount>0) {
    			for(int i=0;i<zCount;i++) {
    				douStr += "0";
    			}
    		}else {
    			zCount = Math.abs(zCount);
    			for(int i=0;i<zCount;i++) {
    				douStr = "0"+douStr;
    			}
    			douStr = "0."+douStr;
    		}
    	}
    	return (isNegative?"-":"")+douStr;
    }
}
