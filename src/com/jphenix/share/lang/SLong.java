/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;


/**
 * 封装好的long型
 * 
 * com.jphenix.share.lang.SLong
 * 
 * 通常有字符串转换成long或者有long转换成字符串
 * 
 * 2018-07-06 增加计算文件大小，传入Object对象
 * 
 * 
 * @author 刘虻
 * 2006-7-22  22:00:50
 */
@ClassInfo({"2014-06-12 19:53","封装好的long型"})
public class SLong implements ICloneable {

    protected Object longString = null; //字符串值
    protected long longValue = 0; //long值
    
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-22 22:00:50
     */
    public SLong() {
        super();
    }
    
    
    /**
     * 获取字符串值
     * @author 刘虻
     * 2007-3-16下午03:59:35
     * @return 字符串值
     */
    public String getLongString() {
    	if(longString==null) {
    		return "0";
    	}
        return longString.toString();
    }

    /**
     * 设置字符串值
     * @author 刘虻
     * 2007-3-16下午03:59:42
     * @param longString 字符串值
     */
    public void setLongString(Object longString) {
        this.longString = longString;
        doExecute(); //执行解析
    }

    /**
     * 获取long值
     * @author 刘虻
     * 2007-3-16下午03:59:56
     * @return long值
     */
    public long getlongValue() {
        return longValue;
    }
    
    /**
     * 获取Long值
     * @author 刘虻
     * 2007-5-21下午12:29:38
     * @return Long值
     */
    public Long getLongValue() {
    	return new Long(longValue);
    }

    /**
     * 设置long值
     * @author 刘虻
     * 2007-3-16下午04:00:08
     * @param langValue long值
     */
    public void setlongValue(long langValue) {
        this.longValue = langValue;
        doOut(); //执行反向解析
    }
    
    /**
     * 构造函数 
     * @param longString 字符串值 "1234"
     * @author 刘虻
     * 2006-7-22 22:02:51
     */
    public SLong(Object longString) {
        super();
        setLongString(longString); //设置字符串值
    }
    
    
    /**
     * 构造函数 
     * @param langValue long值
     * @author 刘虻
     * 2006-7-22 22:03:42
     */
    public SLong(long langValue) {
        super();
        setlongValue(langValue); //设置long值
    }

    
    /**
     * 直接将字符串转为数字
     * @author 刘虻
     * @param longString 数字字符串 "123"
     * @return long数字
     * 2006-7-22  22:08:41
     */
    public static long valueOf(Object longString) {
    	if (longString==null) {
    		return 0;
    	}else if (longString instanceof Long) {
    		return ((Long)longString).longValue();
    	}
        return (new SLong(longString)).getlongValue();
    }
    
    /**
     * 返回转换后的数字字符串
     * @author 刘虻
     * 2007-6-18下午02:00:19
     * @param longObj 转换前的对象
     * @return 转换后的字符串
     */
    public static String stringValueOf(Object longObj) {
    	return String.valueOf(valueOf(longObj));
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param longObj 源数据
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object longObj) {
    	return (new SLong(longObj)).getShowString();
    }
    
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param longObj 源数据
     * @param size 分割点
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object longObj,int size) {
    	return (new SLong(longObj)).getShowString(size);
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2015-08-6下午02:18:21
     * @param longValue 源数据
     * @param size 分割点
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(long longValue,int size) {
        return (new SLong(longValue)).getShowString(size);
    }
    
    /**
     * 将值转换为Long
     * @author 刘虻
     * 2007-5-21下午12:29:52
     * @param longString 值
     * @return Long值
     */
    public static Long longOf(Object longString) {
    	return (new SLong(longString)).getLongValue();
    }
    
    /**
     * 直接将long转为字符串数字
     * @author 刘虻
     * @param langValue long值
     * @return 字符串数字  "123"
     * 2006-7-22  22:09:16
     */
    public static String valueOf(long langValue) {
        return (new SLong(langValue)).getLongString();
    }
    
    
    /**
     * 执行解析
     * @author 刘虻
     * 2006-7-22  22:10:31
     */
    protected void doExecute() {
        
        longValue = 0; //初始化
        
        
        if (longString == null) {
            return;
        }
        longString = longString.toString(); //获取对象的字符串
        if (((String)longString).trim().length()==0) {
            return;
        }
        
        try {
            //强制转换
            longValue = (new Long((String)longString)).longValue();
        }catch(Exception e) {}
        longString = String.valueOf(longValue);
    }
    
    
    /**
     * 执行反向解析
     * @author 刘虻
     * 2006-7-22  22:12:44
     */
    protected void doOut() {
        longString = String.valueOf(longValue);
    }
    
    
    /**
     * 输出指定位数的整数字符串
     * 通常用在数字开头用0补齐,如果当前数字长度大于指定的长度
     * 将不操作,返回原数字
     * @author 刘虻
     * @param size 指定位数
     * @return 指定为数的字符串
     * 2006-7-22  22:16:37
     */
    public String getlongStringBySize(long size) {
        
        //获取返回值
        String relongStr = getLongString();
        long nowSize = relongStr.length(); //当前数字位数
        if (size > nowSize) {
            for (long i=0;i<size-nowSize;i++) {
                relongStr = "0"+relongStr;
            }
        }
        return relongStr;
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @author 刘虻
     * 2008-12-5下午01:11:14
     * @return 显示类型的整型数据
     */
    public String getShowString() {
    	return getShowString(3);
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @author 刘虻
     * @param size 分割点
     * 2008-12-5下午01:11:14
     * @return 显示类型的整型数据
     */
    public String getShowString(int size) {
    	String longStr = getLongString(); //获取返回值
    	boolean isFu = false; //是否为负数
    	if (longStr.startsWith("-")) {
    		isFu = true;
    		longStr = longStr.substring(1);
    	}
    	int point = longStr.length(); //字符串指针
    	if(size<0) {
    		size = 3;
    	}
    	while(point>size) {
    		point-=size;
    		longStr = longStr.substring(0,point)+","+longStr.substring(point);
    	}
    	if (isFu) {
    		return "-"+longStr;
    	}
    	return longStr;
    }
    
    
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-22上午10:33:36
	 */
	@Override
    public Object clone() {
		//构建返回值
		SLong sl = new SLong();
		sl.longString = longString;
		sl.longValue = longValue;
		return sl;
	}
    
	/**
	 * 将长整型数值转换为 MB GB KB 等说明信息
	 * @param size 数值
	 * @return 显示信息
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String convertFileSize(Object size) {
		return convertFileSize(valueOf(size));
	}
	
	/**
	 * 将长整型数值转换为 MB GB KB 等说明信息
	 * @param size 数值
	 * @return 显示信息
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String convertFileSize(String size) {
		return convertFileSize(valueOf(size));
	}
	
	/**
	 * 将长整型数值转换为 MB GB KB 等说明信息
	 * @param size 数值
	 * @return 显示信息
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String convertFileSize(long size) {
		long kb = 1024;
		long mb = kb * 1024;
		long gb = mb * 1024;
		if (size >= gb) {
			return String.format("%.1f GB", (float) size / gb);
		} else if (size >= mb) {
			float f = (float) size / mb;
			return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
		} else if (size >= kb) {
			float f = (float) size / kb;
			return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
		} else {
			return String.format("%d B", size);
		}
	}
}
