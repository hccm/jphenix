/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import java.util.regex.Pattern;


/**
 * 封装好的int型
 * 
 * com.jphenix.share.lang.SInteger
 * 
 * 通常有字符串转换成int或者有int转换成字符串
 * @author 刘虻
 * 2006-7-22  22:00:50
 */
@ClassInfo({"2014-06-12 =19:52","封装好的int型"})
public class SInteger implements ICloneable {

    protected Object intString = null; //字符串值
    protected int intValue = 0; //int值
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-22 22:00:50
     */
    public SInteger() {
        super();
    }

    
    /**
     * 返回字符串值
     * @author 刘虻
     * 2007-3-16下午03:57:53
     * @return 字符串值
     */
    public String getIntString() {
    	if(intString==null) {
    		return "0";
    	}
        return intString.toString();
    }

    /**
     * 设置字符串值
     * @author 刘虻
     * 2007-3-16下午03:58:07
     * @param intString 字符串值
     */
    public void setIntString(Object intString,int defValue) {
        this.intString = intString;
        doExecute(defValue); //执行解析
    }


    /**
     * 返回int值
     * @author 刘虻
     * 2007-3-16下午03:58:13
     * @return int值
     */
    public int getIntValue() {
        return intValue;
    }
    
    /**
     * 返回Integer值
     * @author 刘虻
     * 2007-5-21下午12:27:20
     * @return Integer值
     */
    public Integer getIntegerValue() {
    	return new Integer(intValue);
    }

    
    /**
     * 设置int值
     * @author 刘虻
     * 2007-3-16下午03:58:35
     * @param intValue int值
     */
    public void setIntValue(int intValue) {
        this.intValue = intValue;
        doOut(); //执行反向解析
    }
    
    
    /**
     * 构造函数 
     * @param intString 字符串值 "1234"
     * @author 刘虻
     * 2006-7-22 22:02:51
     */
    public SInteger(Object intString) {
        super();
        setIntString(intString,0); //设置字符串值
    }
    
    /**
     * 构造函数 
     * @param intString 字符串值 "1234"
     * @author 刘虻
     * 2006-7-22 22:02:51
     */
    public SInteger(Object intString,int defValue) {
        super();
        setIntString(intString,defValue); //设置字符串值
    }
    
    /**
     * 构造函数 
     * @param intValue int值
     * @author 刘虻
     * 2006-7-22 22:03:42
     */
    public SInteger(int intValue) {
        super();
        setIntValue(intValue); //设置int值
    }

    
    /**
     * 直接将字符串转为数字
     * @author 刘虻
     * @param intString 数字字符串 "123"
     * @return int数字
     * 2006-7-22  22:08:41
     */
    public static int valueOf(Object intString) {
    	if (intString==null) {
    		return 0;
    	}else if (intString instanceof Integer) {
    		return ((Integer)intString).intValue();
    	}
        return (new SInteger(intString)).getIntValue();
    }
    
    /**
     * 直接将字符串转为数字
     * @author 刘虻
     * @param intString   数字字符串 "123"
     * @param defValue  默认值
     * @return int数字
     * 2006-7-22  22:08:41
     */
    public static int valueOf(Object intString,int defValue) {
    	if (intString==null) {
    		return defValue;
    	}else if (intString instanceof Integer) {
    		return ((Integer)intString).intValue();
    	}
        return (new SInteger(intString,defValue)).getIntValue();
    }
    
    /**
     * 直接将int转为字符串数字
     * @author 刘虻
     * @param intValue int值
     * @return 字符串数字  "123"
     * 2006-7-22  22:09:16
     */
    public static String valueOf(int intValue) {
        return (new SInteger(intValue)).getIntString();
    }
    
    /**
     * 将对象转换为Integer字符串
     * @author 刘虻
     * 2007-6-18下午01:54:49
     * @param intObj 数字对象
     * @return Integer字符串
     */
    public static String stringValueOf(Object intObj) {
    	return String.valueOf(valueOf(intObj));
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param intObj 源数据
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object intObj) {
    	return (new SInteger(intObj)).getShowString();
    }
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2008-12-5下午02:18:21
     * @param intObj 源数据
     * @param size 分割点
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(Object intObj,int size) {
    	return (new SInteger(intObj)).getShowString(size);
    }
    
    
    /**
     * 获取显示形式的字符串
     * @author 刘虻
     * 2015-08-6下午02:18:21
     * @param intValue 源数据
     * @param size 分割点
     * @return 显示形式的字符串
     */
    public static String showStringValueOf(long intValue,int size) {
        return (new SInteger(intValue)).getShowString(size);
    }
    
    
    /**
     * 直接将值转换为Integer类型
     * @author 刘虻
     * 2007-5-21下午12:27:42
     * @param intString 值
     * @return Integer类型值
     */
    public static Integer integerOf(Object intString) {
    	return (new SInteger(intString)).getIntegerValue();
    }
    
    
    /**
     * 执行解析
     * @author 刘虻
     * 2006-7-22  22:10:31
     */
    protected void doExecute(int defValue) {
        intValue = defValue; //初始化
        if (intString==null) {
            return;
        }
        String iStr = intString.toString().trim(); //获取字符串
        if (iStr.length()<1) {
            return;
        }
        //判断是否有小数点
        int point = iStr.indexOf(".");
        if(point>-1) {
        	iStr = iStr.substring(0,point);
        }
        try {
            //强制转换
            intValue = (new Integer(iStr)).intValue();
        }catch(Exception e) {}
    }
    
    
    /**
     * 执行反向解析
     * @author 刘虻
     * 2006-7-22  22:12:44
     */
    protected void doOut() {
        intString = String.valueOf(intValue);
    }
    
    
    /**
     * 输出指定位数的整数字符串
     * 通常用在数字开头用0补齐,如果当前数字长度大于指定的长度
     * 将不操作,返回原数字
     * @author 刘虻
     * @param size 指定位数
     * @return 指定为数的字符串
     * 2006-7-22  22:16:37
     */
    public String getIntStringBySize(int size) {
        
        //获取返回值
        String reIntStr = getIntString();
        int nowSize = reIntStr.length(); //当前数字位数
        if (size > nowSize) {
            for (int i=0;i<size-nowSize;i++) {
                reIntStr = "0"+reIntStr;
            }
        }
        return reIntStr;
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @author 刘虻
     * 2008-12-5下午01:11:14
     * @return 显示类型的整型数据
     */
    public String getShowString() {
    	return getShowString(3);
    }
    
    /**
     * 获取显示类型的整型数据  3,123,333
     * @param size 分割点 
     * @author 刘虻
     * 2008-12-5下午01:11:14
     * @return 显示类型的整型数据
     */
    public String getShowString(int size) {
    	String intStr = getIntString(); //获取返回值
    	boolean isFu = false; //是否为负数
    	if (intStr.startsWith("-")) {
    		isFu = true;
    		intStr = intStr.substring(1);
    	}
    	int point = intStr.length(); //字符串指针
    	if(size<0) {
    		size = 3;
    	}
    	while(point>size) {
    		point-=size;
    		intStr = intStr.substring(0,point)+","+intStr.substring(point);
    	}
    	if (isFu) {
    		return "-"+intStr;
    	}
    	return intStr;
    }
    
    
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-22上午10:32:44
	 */
	@Override
    public Object clone() {
		//构建返回值
		SInteger si = new SInteger();
		si.intString = intString;
		si.intValue = intValue;
		return si;
	}

    private static Pattern integerPattern = Pattern.compile("^[-\\+]?[\\d]*$");
	/**
	 * 判断对象是否为整数
	 * @param str 待判断值
	 * @return 是否为整数
	 * 2015年11月27日
	 * @author 马宝刚
	 */
	public static boolean isInteger(Object str) {    
	    if(str==null) {
	        return false;
	    }
	    return integerPattern.matcher(str.toString()).matches();
	}  
}
