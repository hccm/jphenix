/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年4月12日
 * V4.0
 */
package com.jphenix.share.lang;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 主子信息容器
 * @author MBG
 * 2017年4月12日
 */
@ClassInfo({"2017-04-12 12:17","主子信息容器"})
public class ParentChildVO {

	private ParentChildVO parent                = null; // 父节点
	private String key                          = null; // 当前节点主键
	private List<ParentChildVO> childList       = null; // 当前子元素序列
	private Map<String, ParentChildVO> childMap = null; // 子节点容器 只有根节点会构造该对象

	/**
	 * 构造函数
	 * @author MBG
	 */
	public ParentChildVO() {
		super();
		childList = new ArrayList<ParentChildVO>();
		childMap = new HashMap<String, ParentChildVO>();
	}

	/**
	 * 构造函数 (子节点构造函数）
	 * @author MBG
	 */
	private ParentChildVO(boolean isChild) {
		super();
		childList = new ArrayList<ParentChildVO>();
	}

	/**
	 * 添加元素
	 * @param key       元素主键
	 * @param parentKey 父节点主键
	 * 2016年12月1日
	 * @author MBG
	 */
	public void add(String key, String parentKey) {
		if (parent == null || parent.equals(this)) {
			if (parentKey == null || parentKey.length() < 1) {
				add(key);
				return;
			}
			// 获取根节点
			ParentChildVO base = base();
			// 从根节点操作
			ParentChildVO pNode = childMap.get(parentKey);
			if (pNode == null) {
				// 获取根节点
				pNode = new ParentChildVO(true);
				pNode.parent = base;
				pNode.key = parentKey;
				base.childList.add(pNode);
				base.childMap.put(parentKey, pNode);
			}
			// 放入元素作为其子元素
			pNode.add(key);
			return;
		}
		parent.add(key, parentKey);
	}

	/**
	 * 往当前节点对象中增加子元素
	 * @param key 子元素主键 2016年12月1日
	 * @author MBG
	 */
	public void add(String key) {
		// 获取根节点
		ParentChildVO base = base();
		// 构建子元素
		ParentChildVO child = base.childMap.get(key);
		if (child == null) {
			child = new ParentChildVO(true);
			child.parent = this;
			child.key = key;
			childList.add(child);
			base.childMap.put(key, child);
			return;
		}
		if (child.parent == null || child.parent.equals(this)) {
			// 该元素已经存在，按理说这是不可能的
			child.parent = this;
			return;
		}
		// 这个子元素以前有个爹，但那不是它亲爹，这回找到亲爹了
		child.parent.childList.remove(child);
		child.parent = this;
	}

	/**
	 * 获取根节点
	 * @return 根节点 2016年12月1日
	 * @author MBG
	 */
	public ParentChildVO base() {
		if (parent == null || parent.equals(this)) {
			return this;
		}
		return parent.base();
	}

	/**
	 * 获取指定父节点主键值
	 * @param key 指定节点主键
	 * @return    其父节点的主键值 2016年12月1日
	 * @author MBG
	 */
	public String parentKey(String key) {
		ParentChildVO parent = parent(key);
		if (parent == null) {
			return "";
		}
		return parent.key();
	}

	/**
	 * 获取父节点主键
	 * @return 父节点主键 2016年12月1日
	 * @author MBG
	 */
	public String parentKey() {
		if (parent == null || parent.equals(this)) {
			return "";
		}
		return SString.valueOf(parent.key);
	}

	/**
	 * 当前节点的父节点
	 * @return 当前节点的父节点 2016年12月1日
	 * @author MBG
	 */
	public ParentChildVO parent() {
		if (parent == null || parent.equals(this)) {
			return null;
		}
		return parent;
	}

	/**
	 * 获取指定节点的父节点对象
	 * @param key 指定节点主键
	 * @return    父节点对象 2016年12月1日
	 * @author MBG
	 */
	public ParentChildVO parent(String key) {
		ParentChildVO node = get(key);
		if (node == null) {
			return null;
		}
		return node.parent();
	}

	/**
	 * 获取当前节点主键名
	 * @return 当前节点主键名 2016年12月1日
	 * @author MBG
	 */
	public String key() {
		return SString.valueOf(key);
	}

	/**
	 * 获取指定节点对象
	 * @param key 节点主键
	 * @return 节点对象 2016年12月1日
	 * @author MBG
	 */
	public ParentChildVO get(String key) {
		return base().childMap.get(key);
	}
}
