/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 字符串信息堆栈  堆栈中跳过插入空的字符串（禁止add空字符串）
 * @author 刘虻
 * 2011-10-21 下午01:49:51
 */
@ClassInfo({"2014-06-12 19:53","字符串信息堆栈"})
public class StringsBuffer {

	protected int 
					size = 0 			//缓存大小
					,wpoint = 0			//写入索引
					,rpoint = 0;		//读取索引
	protected String[] buffer = null;	//缓存体
	
	
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public StringsBuffer(int size) {
		super();
		buffer = new String[size];
	}
	
	/**
	 * 获取缓存元素
	 * 刘虻
	 * 2011-10-21 下午02:33:58
	 * @return 缓存元素
	 */
	public String get() {
		if(wpoint==rpoint) {
			//没有缓存元素
			return null;
		}else {
			//获取返回值
			String reStr = buffer[rpoint];
			buffer[rpoint] = null;
			rpoint++;
			if(rpoint==size) {
				//已经到头
				rpoint = 0;
			}
			return reStr;
		}
	}
	
	/**
	 * 清空缓存
	 * 刘虻
	 * 2011-10-21 下午02:33:03
	 */
	public void clear() {
		wpoint = 0;
		rpoint = 0;
		for(int i=0;i<size;i++) {
			buffer[i] = null;
		}
	}

	
	/**
	 * 加入缓存
	 * 刘虻
	 * 2011-10-21 下午02:32:42
	 * @param str 字符串信息
	 */
	public void add(String str) {
		buffer[wpoint] = str;
		wpoint++;
		if(wpoint>=size) {
			wpoint = 0;
		}else if(wpoint==rpoint) {
			//写入缓存从头写入后，又赶上了读取缓存指针，则需要将读取指针向前移动
			rpoint++;
			if(rpoint>=size) {
				rpoint = 0;
			}
		}
	}
}
