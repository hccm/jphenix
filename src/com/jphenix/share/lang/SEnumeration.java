/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import java.util.*;

/**
 * 枚举类
 * com.jphenix.share.lang.SEnumeration
 * @author 刘虻
 * 2007-5-21下午03:49:33
 */
@SuppressWarnings("rawtypes")
@ClassInfo({"2016-11-24 14:24","枚举类"})
public class SEnumeration<E> implements Enumeration,ICloneable {

	protected ArrayList<E> valueArrl = new ArrayList<E>(); //值序列
	protected int index = -1; //指针
	protected int size = 0; //总数
	
	/**
	 * 构造函数
	 * 2007-5-21下午03:49:33
	 */
	public SEnumeration() {
		super();
	}
	
	/**
	 * 构造函数
	 * 2007-9-11下午01:40:33
	 */
	public SEnumeration(Enumeration<E> enumer) {
		super();
		setEnumeration(enumer);
	}

	/*
	 * 构造函数
	 * @params map 将容器主键作为枚举内容
	 * @author MBG
	 */
	public SEnumeration(Map<E,?> map) {
		super();
		if(map!=null) {
			//获取主键枚举
			Iterator<E> keyIter = map.keySet().iterator();
			if(keyIter!=null) {
				while(keyIter.hasNext()) {
					valueArrl.add(keyIter.next());
				}
			}
		}
	}
	
	/*
	 * 构造函数
	 * @params list 序列容器作为枚举内容
	 * @author MBG
	 */
	public SEnumeration(List<E> list) {
		super();
		if(list!=null) {
			valueArrl.addAll(list);
		}
	}
	
	
	/**
	 * 构造函数
	 * @params vector 序列容器作为枚举内容
	 * @author MBG
	 */
	public SEnumeration(Vector<E> vector) {
		super();
		if(vector!=null) {
			valueArrl.addAll(vector);
		}
	}
	
	/*
	 * 构造函数
	 * @params infos 用逗号分隔的字符串
	 * @author MBG
	 */
	@SuppressWarnings("unchecked")
	public SEnumeration(String infos) {
		super();
		if(infos!=null) {
			//用逗号分隔为数组
			String[] infoArr = infos.split(",");
			for(int i=0;i<infoArr.length;i++) {
				valueArrl.add((E)infoArr[i]);
			}
		}
	}
	
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-21下午03:49:33
	 */
	@Override
    public boolean hasMoreElements() {
		return size > index + 1;
	}

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-21下午03:49:33
	 */
	@Override
    public E nextElement() {
		return valueArrl.get(++index);
	}
	
	/**
	 *  获取下一个字符串元素
	 * @author 刘虻
	 * 2007-11-22下午11:33:14
	 * @return 下一个字符串元素
	 */
	public String nextElementString() {
		return SString.valueOf(nextElement());
	}

	/**
	 * 指针移到开始位置
	 * @author 刘虻
	 * 2007-5-21下午03:52:17
	 */
	public void first() {
		index = -1;
	}
	
	/**
	 * 清除数据
	 * @author 刘虻
	 * 2007-5-21下午03:52:48
	 */
	public void clear() {
		valueArrl = new ArrayList<E>();
		index = -1;
		size = 0;
	}
	
	/**
	 * 放入枚举器
	 * @author 刘虻
	 * 2007-5-21下午03:53:40
	 * @param value 元素值
	 * @return 返回当前类实例
	 */
	public SEnumeration addElement(E value) {
		valueArrl.add(value);
		index++;
		size++;
		return this;
	}
	
	
	/**
	 * 累加全部元素
	 * @author 刘虻
	 * 2007-5-21下午03:57:07
	 * @param elementArrayList 全部元素序列
	 * @return 当前类实例
	 */
	public SEnumeration addAllElement(List<E> elementArrayList) {
		if (elementArrayList!=null) {
			valueArrl.addAll(elementArrayList);
			size = valueArrl.size();
		}
		return this;
	}
	
	/**
	 * 加入全部元素
	 * @author 刘虻
	 * 2007-5-21下午05:22:33
	 * @param elementArrayList 全部元素序列
	 * @return 当前类实例
	 */
	public SEnumeration setAllElement(List<E> elementArrayList) {
		clear();
		addAllElement(elementArrayList);
		return this;
	}
	
	/**
	 * 加入全部元素
	 * @author 刘虻
	 * 2007-5-21下午05:07:28
	 * @param elementMap 全部元素容器
	 * @return 当前类实例
	 */
	public SEnumeration setAllElement(Map<E,?> elementMap) {
		clear();
		addAllElement(elementMap);
		return this;
	}
	
	
	/**
	 * 累加全部元素
	 * @author 刘虻
	 * 2007-5-21下午05:07:28
	 * @param elementMap 全部元素容器
	 * @return 当前类实例
	 */
	public SEnumeration addAllElement(Map<E,?> elementMap) {
		if (elementMap!=null) {
			//获取全部容器中的主键序列
			//获取主键枚举
			Iterator<E> keyIter = elementMap.keySet().iterator();
			if(keyIter!=null) {
				while(keyIter.hasNext()) {
					valueArrl.add(keyIter.next());
				}
			}
		}
		return this;
	}
	
	
	/**
	 * 获取枚举内容序列
	 * @author 刘虻
	 * 2007-5-21下午05:21:21
	 * @return 枚举内容序列
	 */
	public ArrayList getEnumerationArrayList() {
		return valueArrl;
	}
	
	
	/**
	 * 累加枚举器
	 * @author 刘虻
	 * 2007-5-21下午05:23:37
	 * @param enumer 枚举器
	 * @return 当前类实例
	 */
	public SEnumeration addEnumeration(Enumeration<E> enumer) {
		
		if (enumer!=null) {
			while(enumer.hasMoreElements()) {
				valueArrl.add(enumer.nextElement());
			}
		}
		size = valueArrl.size();
		return this;
	}
	
	
	/**
	 * 加入枚举器
	 * @author 刘虻
	 * 2007-5-21下午05:23:37
	 * @param enumer 枚举器
	 * @return 当前类实例
	 */
	public SEnumeration setEnumeration(Enumeration<E> enumer) {
		clear();
		addEnumeration(enumer);
		return this;
	}
	

	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-21下午04:02:28
	 */
	@Override
    public Object clone() {
		//构造返回值
		SEnumeration<E> reObj = new SEnumeration<E>();
		reObj.setAllElement(valueArrl);
		reObj.index = index;
		return reObj;
	}
	
}
