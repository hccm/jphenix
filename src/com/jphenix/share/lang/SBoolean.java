/*
 * 代号：凤凰
 * 创建日期 2006-7-22
 * V2.0
 */
package com.jphenix.share.lang;


import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import java.util.Vector;


/**
 * 处理布尔类
 * com.jphenix.share.lang.SBoolean
 * @author 刘虻
 * 2006-7-22  18:11:42
 */
@ClassInfo({"2014-06-12 19:42","处理布尔类"})
public class SBoolean implements ICloneable {
	
    public static String TRUE_VALUE = "1"; //标准的真值字符串
    public static String FALSE_VALUE = "0"; //标准的假值字符串
    protected boolean booleanValue = false; //boolean值
    protected Object booleanString = null; //boolean字符串值
    protected Vector<String> trueVector = null; //保存真值字符串
    public final static String TITLE_TRUE = "是"; //标题真
    public final static String TITLE_FALSE = "否"; //标题假
    
    
    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-22 18:11:42
     */
    public SBoolean() {
        super();
    }

    
    /**
     * 构造函数 
     * @param booleanString boolean字符串
     * @author 刘虻
     * 2006-7-22 18:34:27
     */
    public SBoolean(Object booleanString) {
        super();
        setBooleanString(booleanString); //设置boolean字符串
    }
    
    /**
     * 构造函数 
     * @param booleanValue boolean值
     * @author 刘虻
     * 2006-7-22 18:35:22
     */
    public SBoolean(boolean booleanValue) {
        super();
        setBooleanValue(booleanValue); //设置boolean值
    }
    
    
    
    /**
     * 获取合法的保存真值字符串容器
     * @author 刘虻
     * @return 合法的保存真值字符串容器
     * 2006-7-22  18:15:51
     */
    protected Vector<String> getTrueVector() {
        if (trueVector == null) {
            trueVector = new Vector<String>();
            trueVector.add("yes");
            trueVector.add("true");
            trueVector.add("y");
            trueVector.add("t");
            trueVector.add("1");
            trueVector.add("on");
            trueVector.add("是");
            trueVector.add("真");
            trueVector.add("男");
            trueVector.add("有");
            trueVector.add("好");
        }
        return trueVector;
    }
    
    /**
     * 反向处理字符串
     * @author 刘虻
     * 2006-7-22  18:26:37
     */
    protected void doOut() {
        if (booleanValue) {
            booleanString = TRUE_VALUE; 
        }else {
            booleanString = FALSE_VALUE;
        }
    }
    
    /**
     * 处理字符串
     * @author 刘虻
     * 2006-7-22  18:13:39
     */
    protected void doExecute() {
        
        booleanValue = false;
        
        //空为假值
        if (booleanString == null) {
            return;
        }
        booleanString = booleanString.toString(); //强制转换为字符串
        if (((String)booleanString).length()==0) {
            return;
        }
        
        //去掉空格，转成小写
        booleanString =
            ((String)booleanString).trim().toLowerCase();
        
        //如果字符串与真值人容器匹配，则输出真值
        booleanValue = getTrueVector().contains(booleanString);
        return;
    }
    
    

    /**
     * 设置boolean值
     * @author 刘虻
     * @param booleanValue boolean值
     * 2006-7-22  18:33:02
     */
    public void setBooleanValue(boolean booleanValue) {
        this.booleanValue = booleanValue;
        doOut(); //执行反向解析
    }


    /**
     * 获得boolean值
     * @author 刘虻
     * @return boolean值
     * 2006-7-22  18:32:50
     */
    public boolean getBooleanValue() {
        return booleanValue;
    }
    
    /**
     * 获取Boolean值
     * @author 刘虻
     * 2007-5-21下午12:38:43
     * @return Boolean值
     */
    public Boolean getBoolean() {
    	return new Boolean(booleanValue);
    }


    /**
     * 设置boolean字符
     * @author 刘虻
     * @param booleanString boolean字符
     * 2006-7-22  18:32:42
     */
    public void setBooleanString(Object booleanString) {
        this.booleanString = booleanString;
        doExecute(); //执行解析
    }


    /**
     * 获得boolean字符
     * @author 刘虻
     * @return boolean字符
     * 2006-7-22  18:32:28
     */
    public String getBooleanString() {
        return (String)booleanString;
    }
    
    
    /**
     * 返回标题值
     * @author 刘虻
     * @return 标题值
     * 2006-7-29  11:21:07
     */
    public String getTitleValue() {
        if (getBooleanValue()) {
            return TITLE_TRUE;
        }
        return TITLE_FALSE;
    }
    
    /**
     * 直接将字符串转为boolean
     * @author 刘虻
     * @param booleanObj boolean字符串
     * @return boolean值
     * 2006-7-22  22:05:33
     */
    public static boolean valueOf(Object booleanObj) {
    	if (booleanObj==null) {
    		return false;
    	}else if (booleanObj instanceof Boolean) {
    		return ((Boolean)booleanObj).booleanValue();
    	}
        return (new SBoolean(booleanObj)).getBooleanValue();
    }
    
    /**
     * 返回真假值字符串
     * @author 刘虻
     * 2007-6-4下午12:53:10
     * @param booleanObj 真假源值
     * @return 真假值字符串
     */
    public static String stringBooleanOf(Object booleanObj) {
    	return valueOf(booleanObj)?SBoolean.TRUE_VALUE:SBoolean.FALSE_VALUE;
    }
    
    /**
     * 将值转换为Booelan
     * @author 刘虻
     * 2007-5-21下午12:39:04
     * @param booleanString 值
     * @return Boolean值
     */
    public static Boolean booleanOf(Object booleanString) {
    	return (new SBoolean(booleanString)).getBoolean();
    }
    
    /**
     * 直接将boolean转为字符串
     * @author 刘虻
     * @param booleanValue boolean值
     * @return 字符串值
     * 2006-7-22  22: 06:25
     */
    public static String valueOf(boolean booleanValue) {
        return (new SBoolean(booleanValue)).getBooleanString();
    }

    
    /**
     * 直接将boolean转换为页面显示信息
     * @author 刘虻
     * @param booleanValue boolean值
     * @return 页面显示信息
     * 2006-7-29  11:20:08
     */
    public static String titleOf(boolean booleanValue) {
        return (new SBoolean(booleanValue)).getTitleValue();
    }
    
    
    /**
     * 直接将boolean转换为页面显示信息
     * @author 刘虻
     * 2007-8-30下午12:51:41
     * @param booleanObj boolean值
     * @return 页面显示信息
     */
    public static String titleOf(Object booleanObj) {
    	return (new SBoolean(booleanObj)).getTitleValue();
    }
    
    
    /**
     * 直接将字符串boolean转换为页面显示信息
     * @author 刘虻
     * @param booleanString 字符串boolean
     * @return 页面显示信息
     * 2006-7-29  11:22:26
     */
    public static String titleOf(String booleanString) {
        return (new SBoolean(booleanString)).getTitleValue();
    }


	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-22上午10:26:37
	 */
	@Override
    public Object clone() {
		
		//构建返回值
		SBoolean sboolean = new SBoolean();
		sboolean.booleanValue = booleanValue;
		sboolean.booleanString = booleanString;
		
		return sboolean;
	}
    
    
}
