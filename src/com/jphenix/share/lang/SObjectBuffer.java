/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.db.IBuffer;
import com.jphenix.standard.docs.ClassInfo;

import java.util.Collection;
import java.util.List;
import java.util.Vector;


/**
 * 对象缓存序列
 * 
 * com.jphenix.share.lang.SObjectBuffer
 * 
 * 不断调用add方法,将对象放入序列尾部
 * 不断调用get方法,将头部方法从缓存中取出
 * 
 * 初级:无算法
 * 
 * @author 刘虻
 * 2007-11-9上午11:21:03
 */
@ClassInfo({"2014-06-12 19:53","对象缓存序列"})
public class SObjectBuffer implements IBuffer {
	
	protected Vector<Object> bufferList = new Vector<Object>(); //缓存序列
	
	
	/**
	 * 构造函数
	 * 2007-11-9上午11:21:03
	 */
	public SObjectBuffer() {
		super();
	}
	
	/**
	 * 加入缓存
	 * @author 刘虻
	 * 2007-11-9上午11:25:54
	 * @param obj 对象
	 */
	@Override
    public void add(Object obj) {
		if (obj==null) {
			return;
		}
		bufferList.add(obj);
	}
	
	/**
	 * 放入首位
	 * 刘虻
	 * 2013-1-22 下午1:20:49
	 * @param obj 对象
	 */
	@Override
    public void addFirst(Object obj) {
		if (obj==null) {
			return;
		}
		bufferList.add(0,obj);
	}
	
	/**
	 * 将一个序列中的全部元素放入缓存
	 * 刘虻
	 * 2012-11-18 下午2:33:19
	 * @param c 序列
	 */
	@Override
    public void addAll(Collection<?> c) {
		if(c==null) {
			return;
		}
		bufferList.addAll(c);
	}
	
	/**
	 * 清空缓存
	 * @author 刘虻
	 * 2007-11-9上午11:33:04
	 */
	@Override
    public void clear() {
		bufferList = new Vector<Object>();
	}
	
	/**
	 * 判断对象是否存在于缓存中
	 * @author 刘虻
	 * 2007-11-9下午12:31:30
	 * @param element 比较对象
	 * @return 对象是否存在于缓存中
	 */
	@Override
    public boolean contains(Object element) {
		return bufferList.contains(element);
	}
	
	/**
	 * 获取获取第一个元素,获取后移出
	 * @author 刘虻
	 * 2007-11-9上午11:35:06
	 */
	@Override
    public Object get() {
		//构造返回值
		Object reObj = null;
		if (bufferList.size()>0) {
			//获取第一个元素
			reObj = bufferList.get(0);
			bufferList.remove(0);
		}
		return reObj;
	}
	
	/**
	 * 获取第一个元素
	 * @author 刘虻
	 * 2007-11-9下午12:51:11
	 * @return 第一个元素
	 */
	public Object first() {
		//构造返回值
		Object reObj = null;
		if (bufferList.size()>0) {
			//获取第一个元素
			reObj = bufferList.get(0);
		}
		return reObj;
	}
	
	
	/**
	 * 获取堆栈中堆数
	 * @author 刘虻
	 * 2008-1-16下午05:03:39
	 * @return 堆栈中堆数
	 */
	@Override
    public int size() {
		return bufferList.size();
	}
	
	
	/**
	 * 获取有效的缓存序列
	 * @author 刘虻
	 * 2007-11-9上午11:34:00
	 * @return 有效的缓存序列
	 */
	public List<Object> getBufferList() {
		return bufferList;
	}
}
