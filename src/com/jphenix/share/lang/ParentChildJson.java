/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年11月29日
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.driver.json.Json;
import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 主子关系Json序列容器
 * 
 * 根记录集没有数据，其子记录集序列为一级记录
 * 
 * com.jphenix.share.ParentChildJson
 * @author MBG
 * 2016年12月1日
 */
@ClassInfo({"2017-11-29 10:51","主子关系Json序列容器"})
public class ParentChildJson {

	private ParentChildJson parent = null; //父节点
	
	private Json data = null; //当前行记录集
	
	private String key  = null; //当前节点主键 
	private String pKey = null; //父节点主键
	
	//当前子元素序列
	private List<ParentChildJson> childList = new ArrayList<ParentChildJson>();
	
	//记录集索引容器（全部记录集）只有根主子记录集存在该值
	private SListMap<ParentChildJson> indexMap = null;
	
	/**
	 * 构造函数（根节点构造函数）
	 * @author MBG
	 */
	public ParentChildJson() {
		super();
	}
	
	/**
	 * 构建主子记录集对象
	 * @param rs      数据记录集
	 * @param idKey   主键字段名
	 * @param pIdKey  父主键字段名
	 * @return        主子记录集对象
	 * 2017年4月11日
	 * @author MBG
	 */
	public static ParentChildJson build(List<Json> rs,String idKey,String pIdKey) {
		//根记录集
		ParentChildJson pcl = new ParentChildJson();
		pcl.parent = pcl;
		pcl.indexMap = new SListMap<ParentChildJson>();
		
		ParentChildJson child; //子记录集
		//循环放入全部记录集
		for(Json ele:rs) {
			child = new ParentChildJson();
			child.data = ele;
			child.key  = ele.getValue(idKey);
			child.pKey = ele.getValue(pIdKey);
			pcl.indexMap.put(child.key,child);
		}
		ParentChildJson pEle; //父节点
		for(ParentChildJson ele:pcl.indexMap) {
			if(ele.pKey.length()<1) {
				ele.parent = pcl;
				pcl.childList.add(ele);
			}else {
				pEle = pcl.indexMap.get(ele.pKey);
				if(pEle==null) {
					ele.parent = pcl;
					pcl.childList.add(ele);
				}else {
					ele.parent = pEle;
					pEle.childList.add(ele);
				}
			}
		}
		return pcl;
	}
	
	/**
	 * 获取当前行字段值
	 * @param fieldKey 字段主键
	 * @return 字段值（不为空）
	 * 2017年4月11日
	 * @author MBG
	 */
	public String get(String fieldKey) {
		return data.getValue(fieldKey);
	}
	
	/**
	 * 获取当前节点主键名
	 * @return 当前节点主键名
	 * 2016年12月1日
	 * @author MBG
	 */
	public String key() {
		return key;
	}
	
	/**
	 * 返回整个记录集数量
	 * @return 整个记录集数量
	 * 2016年12月1日
	 * @author MBG
	 */
	public int allSize() {
		return base().allSize();
	}
	
	/**
	 * 返回当前子节点序列元素数量
	 * @return 当前子节点序列元素数量
	 * 2016年12月1日
	 * @author MBG
	 */
	public int size() {
		return childList.size();
	}
	
	/**
	 * 当前节点的父节点
	 * @return 当前节点的父节点
	 * 2016年12月1日
	 * @author MBG
	 */
	public ParentChildJson parent() {
		return parent;
	}
	
	/**
	 * 获取父节点主键
	 * @return 父节点主键
	 * 2016年12月1日
	 * @author MBG
	 */
	public String parentKey() {
		return pKey;
	}
	
	/**
	 * 获取当前节点的父节点主键序列 （包含父节点的主键，父节点的父节点的主键。。）
	 * @return 当前节点的父节点主键序列
	 * 2017年12月14日
	 * @author MBG
	 */
	public List<String> parentKeyList() {
		//构建返回值
		List<String> reList = new ArrayList<String>();
		getParentKeyList(this,reList); //递归获取其父主键序列
		return reList;
	}
	
	/**
	 * 获取父主键序列（递归函数）（包含父节点的主键，父节点的父节点的主键。。）
	 * @param pcj     指定节点
	 * @param idList  父主键序列
	 * 2017年12月14日
	 * @author MBG
	 */
	private void getParentKeyList(ParentChildJson pcj,List<String> idList) {
		if(pcj==null) {
			return;
		}
		String key = pcj.key(); //当前主键
		if(key==null || key.length()<1) {
			return;
		}
		if(!idList.contains(key)) {
			idList.add(key);
		}
		getParentKeyList(indexMap.get(pcj.parentKey()),idList);
	}
	
	/**
	 * 获取指定子元素的所有父类主键序列（父类主键，父类的父类主键）
	 * @param idKey   主键值
	 * @return        所有父类主键序列
	 * 2017年12月14日
	 * @author MBG
	 */
	public List<String> parentKeyList(String idKey) {
		//获取指定子元素
		ParentChildJson ele = getObject(idKey);
		if(ele==null) {
			return new ArrayList<String>();
		}
		return ele.parentKeyList();
	}
	
	
	/**
	 * 获取指定元素
	 * @param idKey  主键值
	 * @return       对应指定元素
	 * 2017年12月14日
	 * @author MBG
	 */
	public ParentChildJson getObject(String idKey) {
		return indexMap.get(idKey);
	}
	
	/**
	 * 获取根节点
	 * @return 根节点
	 * 2016年12月1日
	 * @author MBG
	 */
	public ParentChildJson base() {
		if(parent.equals(this)) {
			return this;
		}
		return parent.base();
	}

	/**
	 * 获取当前子记录集序列
	 * @return 当前子记录集序列
	 * 2016年12月1日
	 * @author MBG
	 */
	public List<ParentChildJson> childs() {
		return childList;
	}
	
	/**
	 * 返回整个主键序列
	 * @return 整个主键序列
	 * 2016年12月1日
	 * @author MBG
	 */
	public SListMap<ParentChildJson> all(){
		if(parent.equals(this)) {
			return indexMap;
		}
		return base().all();
	}
	
	/**
	 * 当前行记录字段数量
	 * @return 当前行记录字段数量
	 * 2017年10月16日
	 * @author MBG
	 */
	public int dataSize() {
		return data==null?0:data.size();
	}
	
	/**
	 * 当前行记录是否为空
	 * @return 当前行记录是否为空
	 * 2017年10月16日
	 * @author MBG
	 */
	public boolean dataEmpty() {
		return data == null || data.isEmpty();
	}
	
	/**
	 * 是否为根（根没有行记录数据）
	 * @return 是否为根（根没有行记录数据）
	 * 2017年10月16日
	 * @author MBG
	 */
	public boolean isRoot() {
        return parent == null || parent.equals(this);
    }
	
	/**
	 * 返回当前行记录容器
	 * @return 当前行记录容器
	 * 2017年10月16日
	 * @author MBG
	 */
	public Json data(){
		if(data==null) {
			return new Json();
		}
		return data;
	}
	
	/**
	 * 添加子元素
	 * @param id   子元素主键
	 * @param ele  子元素数据
	 * @return     子元素对象
	 * 2017年12月14日
	 * @author MBG
	 */
	public ParentChildJson add(String id,Json ele) {
		//子元素
		ParentChildJson child = new ParentChildJson();
		child.parent = this;
		child.key = id;
		child.pKey = this.key();
		child.data = ele;
		base().indexMap.put(id,child);
		return child;
	}
}
