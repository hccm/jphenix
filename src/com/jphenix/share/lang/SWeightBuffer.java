/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年9月25日
 * V4.0
 */
package com.jphenix.share.lang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 带权重值的队列缓存处理类
 * @author MBG
 * 2018年9月25日
 */
@ClassInfo({"2018-09-25 16:04","带权重值的队列缓存处理类"})
public class SWeightBuffer {

	private int           maxWeight     = 0;                        //最大权重值
	private List<Object>  valueList     = new ArrayList<Object>();  //元素序列
	private List<Integer> weightList    = new ArrayList<Integer>(); //对应元素的权重值序列
	private List<Integer> weightElement = new ArrayList<Integer>(); //权重值枚举序列
	
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public SWeightBuffer() {
		super();
	}


	/**
	 * 增加元素
	 * @param obj     指定元素
	 * @param weight  权重（由大到小）
	 * 2018年9月25日
	 * @author MBG
	 */
	public void add(Object obj,int weight) {
		if(!weightElement.contains(weight)) {
			weightElement.add(weight);
			Collections.sort(weightElement);
			Collections.reverse(weightElement);
		}
		if(maxWeight<weight) {
			maxWeight = weight;
		}
		valueList.add(obj);
		weightList.add(weight);
	}


	/**
	 * 清空缓存中的内容
	 * 2018年9月25日
	 * @author MBG
	 */
	public void clear() {
		valueList.clear();
		valueList.clear();
		weightElement.clear();
	}

	
	/**
	 * 从缓存中获取一个元素
	 * @return 获取到的元素
	 * 2018年9月25日
	 * @author MBG
	 */
	public Object get() {
		int weight; //权重元素
		while(weightElement.size()>0) {
			for(int i=0;i<weightList.size();i++) {
				weight = weightList.get(i);
				if(weight==maxWeight) {
					weightList.remove(i);
					return valueList.remove(i);
				}
			}
			weightElement.remove(new Integer(maxWeight));
			maxWeight = 0;
			for(int weightEle:weightElement) {
				if(weightEle>=maxWeight) {
					maxWeight = weightEle;
				}
			}
		}
		return null;
	}

	/**
	 * 缓存中的元素数量
	 * @return 缓存中的元素数量
	 * 2018年9月25日
	 * @author MBG
	 */
	public int size() {
		return valueList.size();
	}

	
	/**
	 * 判断指定对象是否存在于缓存中
	 * @param obj 指定对象
	 * @return    是否存在于对象中
	 * 2018年9月25日
	 * @author MBG
	 */
	public boolean contains(Object obj) {
		return valueList.contains(obj);
	}
	
	
	/**
	 * 测试入口
	 * @param args 传入参数（无用）
	 * 2018年9月25日
	 * @author MBG
	 */
	public static void main(String[] args) {
		//构建测试类
		SWeightBuffer swb = new SWeightBuffer();
		swb.add("aaa",1);
		swb.add("bbb",2);
		swb.add("ccc",2);
		swb.add("ddd",3);
		swb.add("eee",2);
		swb.add("fff",1);
		
		Object ele; //测试元素
		while((ele=swb.get())!=null) {
			System.out.println(ele); //输出测试值
		}
	}
}
