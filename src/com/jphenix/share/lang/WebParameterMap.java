/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月18日
 * V4.0
 */
package com.jphenix.share.lang;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 忽略大小写去掉下划线主键的对照容器
 * 
 * 兼容原主键，也就是说，使用原主键也能获取到指定值
 * 
 * 页面提交参数，在程序中获取到的参数，这两种参数的格式是不同的
 * 该程序就是为了将页面提交的参数主键转换为程序中风格的参数名
 * 
 * 页面提交参数字母都是小写，每个单词用下划线分割。
 * 
 * 程序中的参数每个单词都是连在一起的（没有下划线），第二个单词首个字母大写
 * 
 * 比如：页面中提交的参数名为  id_str  到程序中变为 idStr
 * 
 * @author 马宝刚
 * 2014年6月18日
 */
@ClassInfo({"2014-06-12 19:42","忽略大小写去掉下划线主键的对照容器"})
public class WebParameterMap<K,V> extends  HashMap<K, V> {

    /**
     * 串行标识
     */
    private static final long serialVersionUID = 7544416095856227130L;

    protected HashMap<String,Object> keyMap = new HashMap<String,Object>(); //主键对照容器 key:小写主键，value 原主键
    
    
    @Override
    public boolean containsKey(Object key) {
       if(key==null) {
            return false;
        }
        return keyMap.containsKey(fixKey(key)) || super.containsKey(key) ;
    }

    @Override
    public V get(Object key) {
        if(key==null) {
            return null;
        }
        V reObj = super.get(key);
        if(reObj==null) {
            reObj = super.get(keyMap.get(fixKey(key)));
        }
        return reObj;
    }

    @Override
    public V put(K key, V value) {
        if(key==null) {
            return value;
        }
        keyMap.put(fixKey(key),key);
        return super.put(key,value);
    }

    @Override
    public V remove(Object key) {
        if(key==null) {
            return null;
        }
        if(super.containsKey(key)) {
            keyMap.remove(fixKey(key));
            return super.remove(key);
        }
        //获取源主键
        Object srcKey = keyMap.remove(fixKey(key));
        return super.remove(srcKey);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if(m==null) {
            return;
        }
        //获取参数主键迭代
         Iterator<? extends K> keyIterator = m.keySet().iterator();
         Object key; //参数主键元素
        while(keyIterator.hasNext()) {
            key = keyIterator.next();
            if(key==null) {
                continue;
            }
            keyMap.put(fixKey(key),key);
        }
        super.putAll(m);
    }

    @Override
    public void clear() {
        keyMap.clear();
        super.clear();
    }
    
    /**
     * 将参数主键除去下划线，并且全部转换为小写
     * @param key 处理前的参数主键
     * @return 处理后的参数主键
     * 2014年6月18日
     * @author 马宝刚
     */
    protected String fixKey(Object key) {
        return key.toString().replaceAll("_","").toLowerCase();
    }
}
