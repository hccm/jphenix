/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年6月18日
 * V4.0
 */
package com.jphenix.share.lang;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 忽略大小写主键的对照容器
 * @author 马宝刚
 * 2014年6月18日
 */
@ClassInfo({"2014-06-12 19:42","忽略大小写主键的对照容器"})
public class CaseMap<K,V> extends  HashMap<K, V> {

    /**
     * 串行标识
     */
    private static final long serialVersionUID = 7544416095856227130L;

  //主键对照容器 key:小写主键，value 原主键
    protected HashMap<String,Object> keyMap = new HashMap<String,Object>(); 
    
    
    @Override
    public boolean containsKey(Object key) {
       if(key==null) {
            return false;
        }
        return keyMap.containsKey(key.toString().toLowerCase());
    }

    @Override
    public V get(Object key) {
        if(key==null) {
            return null;
        }
        return super.get(keyMap.get(key.toString().toLowerCase()));
    }

    @Override
    public V put(K key, V value) {
        if(key==null) {
            return value;
        }
        keyMap.put(key.toString().toLowerCase(),key);
        return super.put(key,value);
    }

    @Override
    public V remove(Object key) {
        if(key==null) {
            return null;
        }
        //获取源主键
        Object srcKey = keyMap.remove(key.toString().toLowerCase());
        return super.remove(srcKey);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if(m==null) {
            return;
        }
        //获取参数主键迭代
         Iterator<? extends K> keyIterator = m.keySet().iterator();
         Object key; //参数主键元素
        while(keyIterator.hasNext()) {
            key = keyIterator.next();
            if(key==null) {
                continue;
            }
            keyMap.put(key.toString().toLowerCase(),key);
        }
        super.putAll(m);
    }

    @Override
    public void clear() {
        keyMap.clear();
        super.clear();
    }
}
