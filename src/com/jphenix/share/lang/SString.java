/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ICloneable;

import java.text.SimpleDateFormat;


/**
 * 字符串处理工具
 * 
 * com.jphenix.share.lang.SString
 * 
 * 2018-07-05 增加了去掉左右空格处理
 * 
 * @author 刘虻
 * 2006-7-23  13:47:46
 */
@ClassInfo({"2018-07-05 17:01","字符串处理工具"})
public class SString implements ICloneable {

    /**
     * 构造函数 
     * @author 刘虻
     * 2006-7-23 13:47:46
     */
    public SString() {
        super();
    }
    
    /**
     * 将对象强制转换为字符串
     * @param obj          待处理的对象
     * @param defValue     默认值
     * @return             转换后的字符串
     * 2017年7月26日
     * @author MBG
     */
    public static String valueOf(Object obj,String defValue) {
    	return valueOf(obj,defValue,false);
    }
    
    /**
     * 将对象强制转换为去掉左右空格的字符串
     * @param obj          待处理的对象
     * @param defValue     默认值
     * @return             去掉左右空格的字符串
     * 2017年7月26日
     * @author MBG
     */
    public static String trimValueOf(Object obj,String defValue) {
    	return valueOf(obj,defValue,true);
    }
    
    /**
     * 将对象强制转换为字符串
     * @param obj          待处理的对象
     * @param defValue     默认值
     * @param needTrim     是否需要去掉左右空格
     * @return             转换后的字符串
     * 2017年7月26日
     * @author MBG
     */
    public static String valueOf(Object obj,String defValue,boolean needTrim) {
        if(obj==null){
            return defValue;
        }
        if (obj instanceof java.sql.Timestamp) {
        	return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(((java.sql.Timestamp)obj));
        }
        //构造返回值
        String reStr =  String.valueOf(obj);
        if(reStr.length()<1) {
        	if(defValue==null) {
        		return "";
        	}
        	return defValue;
        }
        return reStr;
    }
    
    /**
     * 将对象强制转换为字符串
     * @author 刘虻
     * @param obj      待处理的对象
     * @param needTrim 是否需要去掉左右空格
     * @return         对应的字符串值
     * 2006-7-23  13:48:16
     */
    public static String valueOf(Object obj,boolean needTrim) {
        if(obj==null){
            return "";
        }
        if (obj instanceof java.sql.Timestamp) {
        	return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(((java.sql.Timestamp)obj));
        }
        return String.valueOf(obj);
    }
    
    /**
     * 将对象强制转换为字符串
     * @author 刘虻
     * @param obj      待处理的对象
     * @return         对应的字符串值
     * 2006-7-23  13:48:16
     */
    public static String valueOf(Object obj) {
    	return valueOf(obj,false);
    }
    
    
    /**
     * 将对象强制转换为字符串
     * @author 刘虻
     * @param obj      待处理的对象
     * @return         对应的字符串值
     * 2006-7-23  13:48:16
     */
    public static String trimValueOf(Object obj) {
    	return valueOf(obj,true);
    }
    
    
    /**
     * 对象是否为空
     * 刘虻
     * 2010-11-8 下午01:25:56
     * @param obj 指定对象
     * @return 为空true
     */
    public static boolean isEmpty(Object obj) {
        return obj == null || obj.toString().length() < 1;
    }
    
    /**
     * 对象是否为空（去掉左右空格后再判断）
     * 刘虻
     * 2010-11-8 下午01:25:56
     * @param obj 指定对象
     * @return 为空true
     */
    public static boolean isTrimEmpty(Object obj) {
        return obj == null || obj.toString().trim().length() < 1;
    }
    
    /**
     * 是否不为空
     * 刘虻
     * 2010-11-8 下午01:35:10
     * @param obj 指定对象
     * @return 不为空true
     */
    public static boolean isNotEmpty(Object obj) {
        return obj != null && obj.toString().length() >= 1;
    }
    
    
    /**
     * 是否不为空（去掉左右空格后再判断）
     * 刘虻
     * 2010-11-8 下午01:35:10
     * @param obj 指定对象
     * @return 不为空true
     */
    public static boolean trimIsNotEmpty(Object obj) {
        return obj != null && obj.toString().trim().length() >= 1;
    }
    
    
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2007-5-22上午10:34:24
	 */
	@Override
    public Object clone() {
		return new SString();
	}

}
