/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 字符串排序类
 * @author 刘虻
 * 2010-8-20 下午02:58:34
 */
@ClassInfo({"2014-06-12 19:59","字符串排序类"})
public class StringSorter {

	
	/**
	 * 顺序排序
	 * 刘虻
	 * 2010-8-20 下午03:00:42
	 * @param strList 待排序的字符串序列
	 * @return 排序后的字符串序列
	 */
	public static List<String> asc(List<String> strList) {
	    if(strList==null || strList.size()<1) {
	        return new ArrayList<String>();
	    }
		//获取待排序的数组
		String[] arrs = new String[strList.size()];
		strList.toArray(arrs);
		Arrays.sort(arrs);
		strList = new ArrayList<String>();
		Collections.addAll(strList,arrs);
		return strList;
	}

	
	/**
	 * 倒序排序
	 * 刘虻
	 * 2010-8-20 下午03:00:59
	 * @param strList 待排序的字符串序列
	 * @return 排序后的字符串序列
	 */
	public static List<String> desc(List<String> strList) {
        if(strList==null || strList.size()<1) {
            return new ArrayList<String>();
        }
        //获取待排序的数组
        String[] arrs = new String[strList.size()];
        strList.toArray(arrs);
        Arrays.sort(arrs);
        strList = new ArrayList<String>();
        Collections.addAll(strList,arrs);
        Collections.reverse(strList); //倒序
        return strList;
	}
}
