/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.util;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 数学工具类
 * @author 刘虻
 * 2010-8-18 下午02:52:49
 */
@ClassInfo({"2014-06-12 20:02","数学工具类"})
public class MathUtil {
	
	/**
	 * 两个字符串中的数字相加
	 * 注意：数字字符串中如果存在非数字字符，则按0来处理
	 * 
	 * 目前不支持小数点和负数
	 * 
	 * 刘虻
	 * 2010-8-18 下午03:21:36
	 * @param strNumber		数字字符串1
	 * @param addStrNumber	数字字符串2
	 * @return 两个数字字符串和
	 */
	public static String stringAdd(String strNumber,String addStrNumber) {
		if(strNumber==null || strNumber.length()<1) {
			strNumber = "0";
		}
		if(addStrNumber==null || addStrNumber.length()<1) {
			addStrNumber = "0";
		}
		int 
			size1 = strNumber.length()				//数字1的长度
			,size2 = addStrNumber.length()			//数字2的长度
			,maxSize = size1>size2?size1:size2	 	//最大长度
			,num1 = 0								//处理的数字1位
			,num2 = 0								//处理的数字2位
			,sum = 0;								//处理数字和
		boolean jw = false; //进位标识
		String reStr = ""; //构建返回值
		for(int i=0;i<maxSize;i++) {
			if(i<size1) {
				num1 = intValueOf(strNumber.substring(size1-i-1,size1-i));
			}else {
				num1 = 0;
			}
			if(i<size2) {
				num2 = intValueOf(addStrNumber.substring(size2-i-1,size2-i));
			}else {
				num2 = 0;
			}
			sum = num1+num2+(jw?1:0);
			boolean tjw= false; //本次进位标识
			if(sum>9) {
				tjw = true;
				sum -= 10;
			}
			if(i>0) {
				reStr = sum+reStr;
			}else {
				reStr = String.valueOf(sum);
			}
			jw = tjw;
		}
		if(jw) {
			reStr = "1"+reStr;
		}
		return reStr;
	}
	
	
	/**
	 * 将字符串转换为数字
	 * 如果字符串中包含非数字字符，则返回0
	 * 刘虻
	 * 2010-8-18 下午03:13:38
	 * @param str 待转换的数字字符串
	 * @return 转换后的数字
	 */
	protected static int intValueOf(String str) {
		try {
			return Integer.parseInt(str);
		}catch(Exception e) {} 
		return 0;
	}
	
	
	/**
	 * 测试入口
	 * 刘虻
	 * 2010-8-18 下午03:23:10
	 * @param args 传入参数 
	 */
	public static void main(String[] args) {
		System.out.println(stringAdd("133913019391","3181313"));
	}
}
