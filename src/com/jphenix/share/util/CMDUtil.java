/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年11月4日
 * V4.0
 */
package com.jphenix.share.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import com.jphenix.standard.docs.ClassInfo;

/**
 * CMD命令执行工具类
 * 
 * @author zhangcf 2019年11月4日
 */

@ClassInfo({ "2019-11-04 10:25", "CMD命令执行工具" })
public class CMDUtil {

	/**
	 * 构造函数 2007-3-13下午12:39:14
	 */
	public CMDUtil() {
		super();
	}

	public static String runCmd(String cmd) {
		return runCmd(cmd, null);
	}

	public static String runCmd(String cmd, String cmdpath) {
		String result = "";
		if (null == cmdpath) {
			// 默认在用户目录下执行
			cmdpath = System.getProperty("user.home");// windows下取用户目录的方法，linux下还未测试
		}
		File dir = new File(cmdpath);
		try {
			// 调用Runtime执行CMD命令
			Process ps = Runtime.getRuntime().exec(cmd, null, dir);
			BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream(), Charset.forName("GBK")));
			String line = null;
			while ((line = br.readLine()) != null) {
				// 输出执行过程中CMD的返回
				System.out.println(line);
				result += line + "\n";
			}

			br.close();
			ps.waitFor();
			ps.destroy();

			return result;
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

}
