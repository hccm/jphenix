/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.util;

import com.jphenix.standard.docs.ClassInfo;

import java.io.*;

/**
 * 流工具类
 * @author 刘虻
 * 2008-8-6上午11:52:36
 */
@ClassInfo({"2014-06-12 20:02","流工具类"})
public class StreamUtil {

	protected static final int COPY_BUF_SIZE = 4096 * 2;

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:47:20
	 * @param in 母鸡
	 * @param out 母鸡
	 * @throws IOException 执行发生异常
	 */
	public static void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[COPY_BUF_SIZE];
		int len;
		while ((len = in.read(buf)) != -1) {
			out.write(buf, 0, len);
		}
	}



	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:47:33
	 * @param in 母鸡
	 * @param out 母鸡
	 * @throws IOException 母鸡
	 */
	public static void copyStream(Reader in, Writer out) throws IOException {
		char[] buf = new char[COPY_BUF_SIZE];
		int len;
		while ((len = in.read(buf)) != -1) {
			out.write(buf, 0, len);
		}
	}
	
	
	

	/**
	 * 母鸡
	 * @author 刘虻
	 * 2008-7-9上午07:47:44
	 * @param in 母鸡
	 * @param out 母鸡
	 * @param charSet 母鸡
	 * @throws IOException 执行发生异常
	 */
	public static void copyStream(Reader in, OutputStream out, String charSet) throws IOException {
		char[] buf = new char[4096];
		int len;
		if (charSet == null) {
            while ((len = in.read(buf)) != -1) {
                out.write(new String(buf, 0, len).getBytes());
            }
        } else {
            while ((len = in.read(buf)) != -1) {
                out.write(new String(buf, 0, len).getBytes(charSet));
            }
        }
	}

}
