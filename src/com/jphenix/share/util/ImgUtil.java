/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年7月27日
 * V4.0
 */
package com.jphenix.share.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;

import javax.imageio.ImageIO;

import com.jphenix.share.tools.Base64;
import com.jphenix.share.tools.FileCopyTools;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 图片工具
 * 
 * 2019-07-27 为证通接口创建
 * 2019-08-13 修改了压缩图片策略，如果图片文件大小没达到要求最小值，不必执行压缩
 * 2019-08-19 换了一种压缩方法，避免压缩过头
 * 2019-09-10 修改了如果图片原本就没有超过最大值，则不做压缩处理
 * 
 * 
 * @author MBG
 * 2019年7月27日
 */
@ClassInfo({"2019-09-10 10:07","图片工具"})
public class ImgUtil {

	/**
	 * 返回压缩后的图片的Base64值
	 * @param srcFile   源文件对象
	 * @param minSize   压缩到最小大小（参考比率）
	 * @return          文件的base64值
	 * 2019年7月27日
	 * @author MBG
	 */
	public static String reduceImgB64(File srcFile,long minSize) {
		//构建返回值
		byte[] res = reduceImg(srcFile,minSize);
		return Base64.base64Encode(res);
	}
	

    /**
     * 指定图片宽度和高度和压缩比例对图片进行压缩
     * @param srcFile 源图片文件对象
     * @param maxSize 目标图片地址
     * @return        压缩后的图片字节数组
     */
    public static byte[] reduceImg(File srcFile,long maxSize) {
        try {
        	if(srcFile==null || !srcFile.exists()) {
        		return null;
        	}
        	//将原图片读取到内存
        	byte[] srcBytes = FileCopyTools.copyToByteArray(srcFile);
        	if(srcFile.length()<maxSize) {
        		return srcBytes;
        	}
        	return compressUnderSize(srcBytes,maxSize);
        } catch (Exception ef) {
            ef.printStackTrace();
        }
        return null;
    }
    
	
    /**
	 * 将图片压缩到指定大小以内
	 * 
	 * @param srcImgData 源图片数据
	 * @param maxSize    目的图片大小
	 * @return           压缩后的图片数据
	 */
	public static byte[] compressUnderSize(byte[] srcImgData, long maxSize) {
		double scale   = 0.9;
		byte[] imgData = Arrays.copyOf(srcImgData, srcImgData.length);
		if(imgData.length>maxSize) {
			do {
				imgData = compress(imgData, scale);
			} while (imgData.length > maxSize);
		}
		return imgData;
	}

	

	/**
	 * 按照 宽高 比例压缩
	 * @param imgIs 待压缩图片输入流
	 * @param scale 压缩刻度
	 * @return      压缩后图片数据
	 */
	public static byte[] compress(byte[] srcImgData, double scale){
		try {
			BufferedImage bi = ImageIO.read(new ByteArrayInputStream(srcImgData));
			int width = (int) (bi.getWidth() * scale); // 源图宽度
			int height = (int) (bi.getHeight() * scale); // 源图高度
	 
			Image image = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	 
			Graphics g = tag.getGraphics();
			g.setColor(Color.RED);
			g.drawImage(image, 0, 0, null); // 绘制处理后的图
			g.dispose();
			ByteArrayOutputStream bOut = new ByteArrayOutputStream();
			ImageIO.write(tag, "JPEG", bOut);
			return bOut.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 测试入口
	 * @param args
	 * 2019年7月27日
	 * @author MBG
	 */
	public static void main(String[] args) {
		File file = new File("d:/a.jpg");
		
		byte[] cnt = reduceImg(file,0);
		try {
			FileCopyTools.copy(cnt,new File("d:/b.jpg"));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
