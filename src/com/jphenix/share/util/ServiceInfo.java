/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年12月22日
 * V4.0
 */
package com.jphenix.share.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.kernel.objectloader.FBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.share.lang.SDate;
import com.jphenix.share.lang.SDouble;
import com.jphenix.share.lang.SLong;
import com.jphenix.share.tools.SysUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IRequest;
import com.jphenix.ver.PhenixVer;

/**
 * 服务相关信息类
 * 
 * 2018-07-04 增加了服务器当前时间
 * 2018-08-20 增加了是否为开发模式
 * 2019-04-02 增加了本机IP地址信息
 * 
 * @author MBG
 * 2016年12月22日
 */
@ClassInfo({"2018-08-20 10:49","服务相关信息类"})
public class ServiceInfo {
	
	/**
	 * 是否已经设置了信息
	 */
	public static boolean hasInfo = false;

	/**
	 * 服务启动时间（毫秒）
	 */
	public static long startTime = 0;
	
	/**
	 * 操作系统名称
	 */
	public static String osName = null;
	
	/**
	 * 操作系统版本号
	 */
	public static String osVersion = null;
	
	/**
	 * 编译类型
	 */
	public static String osArch = null;
	
	/**
	 * 处理器数量
	 */
	public static int processorsCount = 0;
	
	/**
	 * 服务器信息（综合信息）
	 */
	public static String serverInfo = null;
	
	/**
	 * 服务器用户
	 */
	public static String serverUser = null;
	
	/**
	 * 服务器用户文件夹路径
	 */
	public static String serverUserPath = null;
	
	/**
	 * 服务器用户信息（综合信息）
	 */
	public static String serverUserInfo = null;
	
	/**
	 * 程序根目录
	 */
	public static String serverBasePath = null;
	
	/**
	 * java版本号
	 */
	public static String javaVer = null;
	
	/**
	 * java安装目录
	 */
	public static String javaBasePath = null; 
	
	/**
	 * 系统临时文件夹
	 */
	public static String sysTempPath = null;
	
	/**
	 * Servlet容器信息
	 */
	public static String servletInfo = null;
	
	/**
	 * 内核版本
	 */
	public static String kernelVer = PhenixVer.getVER()+"("+PhenixVer.getUpdateCount()+")";
	
	/**
	 * 总共内存
	 */
	public static long totalMemory = 0;
	
	/**
	 * 最大内存
	 */
	public static long maxMemory = 0;
	
	/**
	 * 使用内存
	 */
	public static long usedMemory = 0;
	
	/**
	 * 剩余内存
	 */
	public static long freeMemory = 0;
	
	/**
	 * 内存占用百分比
	 */
	public static String usedMemoryPercent = "";
	
	/**
	 * 返回服务器信息容器
	 * @return 服务器信息容器
	 * 2017年4月27日
	 * @author MBG
	 */
	public static Map<String,String> info(){
		//构造返回值
		HashMap<String,String> reMap = new HashMap<String,String>();
		reMap.put("hasInfo",hasInfo?"1":"0");                                               //是否已经设置了信息
		reMap.put("serverTime",SDate.getNowTS());                                           //服务器当前时间
		reMap.put("startTime",String.valueOf(startTime));                                   //服务启动时间（毫秒）
		reMap.put("showStartTime",SDate.getTs(startTime));                                  //显示服务器启动时间戳
		reMap.put("startUsedTime",SDate.runTimeInfo(System.currentTimeMillis()-startTime)); //运行时间（消耗时间）
		reMap.put("osName",osName);                                                         //操作系统名称
		reMap.put("osVersion",osVersion);                                                   //操作系统版本号
		reMap.put("osArch",osArch);                                                         //编译类型
		reMap.put("processorsCount",String.valueOf(processorsCount));                       //处理器数量
		reMap.put("serverInfo",serverInfo);                                                 //服务器信息（综合信息）
		reMap.put("serverUser",serverUser);                                                 //服务器用户
		reMap.put("serverUserPath",serverUserPath);                                         //服务器用户文件夹路径
		reMap.put("serverUserInfo",serverUserInfo);                                         //服务器用户信息（综合信息）
		reMap.put("serverBasePath",serverBasePath);                                         //程序根目录
		reMap.put("javaVer",javaVer);                                                       //java版本号
		reMap.put("javaBasePath",javaBasePath);                                             //java安装目录
		reMap.put("sysTempPath",sysTempPath);                                               //系统临时文件夹
		reMap.put("servletInfo",servletInfo);                                               //Servlet容器信息
		reMap.put("kernelVer",kernelVer);                                                   //内核版本
		
		//获取默认类加载器
		IBeanFactory bf = FBeanFactory.getBeanFactory();
		reMap.put("devMode",(bf!=null && bf.isDevelopMode())?"1":"0");                      //是否为开发模式
		
		//当前运行时
		Runtime runtime   = Runtime.getRuntime();
		totalMemory       = runtime.totalMemory();
		freeMemory        = runtime.freeMemory();
		maxMemory         = runtime.maxMemory();
		usedMemory        = totalMemory-freeMemory;
		usedMemoryPercent = SDouble.stringValueOf((double)usedMemory/(double)totalMemory*100,2);
		
		reMap.put("totalMemory",memorySize(totalMemory));     //总共内存大小
		reMap.put("freeMemory",memorySize(freeMemory));       //剩余内存大小
		reMap.put("maxMemory",memorySize(maxMemory));         //最大内存大小
		reMap.put("usedMemory",memorySize(usedMemory));       //使用中内存大小
		reMap.put("usedMemoryPercent",usedMemoryPercent);     //内存占用百分比
		
		List<String> ips = SysUtil.getLocalNetAddress();                                    //本机IP地址
		StringBuffer sbf = new StringBuffer();                                              //输出结果
		for(int i=0;i<ips.size();i++) {
			if(i>0) {
				sbf.append(",");
			}
			sbf.append(ips.get(i));
		}
		reMap.put("ips",sbf.toString());                                                    //本机IP地址
		return reMap;
	}
	
	/**
	 * Java虚拟机内存总量
	 * @return Java虚拟机内存总量
	 * 2016年12月23日
	 * @author MBG
	 */
	public static long jvmMemorySize() {
		return Runtime.getRuntime().totalMemory();
	}
	
	/**
	 * Java虚拟机内存总量信息
	 * @return Java虚拟机内存总量信息
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String jvmMemoryInfo() {
		return SLong.convertFileSize(jvmMemorySize());
	}
	
	/**
	 * Java虚拟机剩余内存量
	 * @return Java虚拟机剩余内存量
	 * 2016年12月23日
	 * @author MBG
	 */
	public static long jvmFreeMemorySize() {
		return Runtime.getRuntime().freeMemory();
	}
	
	/**
	 * Java虚拟机剩余内存量信息
	 * @return Java虚拟机剩余内存量信息
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String jvmFreeMemoryInfo() {
		return SLong.convertFileSize(jvmFreeMemorySize());
	}
	
	/**
	 * Java虚拟机最大内存量
	 * @return Java虚拟机最大内存量
	 * 2016年12月23日
	 * @author MBG
	 */
	public static long jvmMaxMemorySize() {
		return Runtime.getRuntime().maxMemory();
	}
	
	/**
	 * Java虚拟机最大内存量信息
	 * @return Java虚拟机最大内存量
	 * 2016年12月23日
	 * @author MBG
	 */
	public static String jvmMaxMemoryInfo() {
		return SLong.convertFileSize(jvmMaxMemorySize());
	}
	
	/**
	 * 服务器端口
	 * @return 服务器端口
	 * 2016年12月23日
	 * @author MBG
	 */
	public static int serverPort(IRequest req) {
		if(req==null) {
			return 0;
		}
		return req.getServerPort();
	}
	
	/**
	 * 将内存数据转换为可读的内存数据
	 * @param size 内存数值
	 * @return 可读的内存数值
	 * 2018年1月16日
	 * @author MBG
	 */
	private static String memorySize(long size) {
		//将内存值转换为字符串 000(t) 000(g) 000(m) 000(k) 000(b)
		String sizeStr = String.valueOf(size);
		int length = sizeStr.length(); //字符串长度
		if(length>12) {
			return sizeStr.substring(0,length-12)+"."+sizeStr.substring(length-12,length-10)+"TB";
		}else if(length>9) {
			return sizeStr.substring(0,length-9)+"."+sizeStr.substring(length-9,length-7)+"GB";
		}else if(length>6) {
			return sizeStr.substring(0,length-6)+"."+sizeStr.substring(length-6,length-4)+"MB";
		}else if(length>3) {
			return sizeStr.substring(0,length-3)+"."+sizeStr.substring(length-3,length-1)+"KB";
		}
		return sizeStr+"Byte";
	}
}
