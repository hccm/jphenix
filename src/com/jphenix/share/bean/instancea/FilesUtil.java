/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.bean.instancea;

import com.jphenix.share.util.BaseUtil;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.beans.IFilesUtil;
import com.jphenix.standard.docs.ClassInfo;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.zip.ZipFile;

/**
 * 处理文件，路径工具
 * com.jphenix.share.bean.instance.FilesUtil
 * 
 * 2018-05-28 增加了写入文件内容的方法 addContent putContent
 * 
 * @author 刘虻
 * 2006-4-1  11:33:26
 */
@ClassInfo({"2018-05-28 11:22","处理文件/路径工具"})
public class FilesUtil implements IFilesUtil {

	protected String webInfPath  = null; //类根路径
	protected String uploadPath = "upload"; //上传文件路径
	protected String tempPath = "upload/temp"; //临时文件保存路径
	protected String tempVirtualPath = "/temp"; //虚拟路径
	

	/**
	 * 将网站文件相对路径，转换成绝对路径
	 * @param webSubPath 文件的相对路径（相对网站根路径）
	 * @return 文件的绝对路径
	 * 2017年9月11日
	 * @author MBG
	 */
	@Override
    public String webPath(String webSubPath) {
		return getAllFilePath(getWebInfPath()+"/.."+webSubPath);
	}
    
    /**
     * 设置类的根路径
     * @author 刘虻
     * 2007-7-16上午11:22:55
     * @param webInfPath 类的根路径
     */
    @Override
    public void setWebInfPath(String webInfPath) {
    	this.webInfPath = BaseUtil.swapString(webInfPath,"\\","/");
    }

    /**
     * 覆盖方法
     * 刘虻
     * 2010-2-2 上午09:50:15
     */
    @Override
    public String getWebInfPath() {
    	return webInfPath;
    }
    
    /**
     * 获取新的类实例
     * @author 刘虻
     * 2008-7-27下午03:05:29
     * @return 新的类实例
     */
    public static IFilesUtil newInstance() {
    	return new FilesUtil();
    }
    
    
    
    /**
     * 获取临时文件保存路径
     * @author 刘虻
     * 2007-8-23下午06:54:23
     * @return 临时文件保存路径
     */
	@Override
    public String getTempPath() {
		return tempPath;
	}


	/**
	 * 设置临时文件保存路径
	 * @author 刘虻
	 * 2007-8-23下午06:54:33
	 * @param tempPath 临时文件保存路径
	 */
	@Override
    public void setTempPath(String tempPath) {
		if(tempPath==null || tempPath.length()<1) {
			return;
		}
		this.tempPath = getAllFilePath(tempPath);
	}
    
    
	/**
	 * 获取完整的临时文件保存路径
	 * @author 刘虻
	 * 2007-8-23下午06:55:07
	 * @return 完整的临时文件保存路径
	 */
    @Override
    public String getAllTempPath() {
    	return getAllFilePath(getTempPath());
    }


    /**
     * 获取虚拟临时路径
     * @author 刘虻
     * 2007-8-24下午03:20:33
     * @return 虚拟临时路径
     */
	@Override
    public String getTempVirtualPath() {
		return tempVirtualPath;
	}

	/**
	 * 设置虚拟临时路径
	 * @author 刘虻
	 * 2007-8-24下午03:20:56
	 * @param tempVirtualPath 虚拟临时路径
	 */
	@Override
    public void setTempVirtualPath(String tempVirtualPath) {
		this.tempVirtualPath = tempVirtualPath;
	}
	
	
	/**
	 * 覆盖方法
	 * @author 刘虻
	 * 2009-8-18下午06:05:34
	 */
	@Override
    public Object clone() {
		//构建返回值
		FilesUtil fu = new FilesUtil();
		fu.webInfPath = webInfPath;
		fu.tempPath = tempPath;
		fu.tempVirtualPath = tempVirtualPath;
		return fu;
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午09:44:10
	 */
	@Override
    public File createFile(String filePath) {
		return SFilesUtil.createFile(getAllFilePath(filePath));
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:32:49
	 */
	@Override
    public void deletePath(String path) throws Exception {
		SFilesUtil.deletePath(getAllFilePath(path));
	}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午09:50:28
	 */
	@Override
    public String getAllFilePath(String dummyPath) {
		return SFilesUtil.getAllFilePath(dummyPath,getWebInfPath());
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:34:23
	 */
	@Override
    public String getContextClassLoaderResource(String filePath)
			throws Exception {
		return SFilesUtil.getContextClassLoaderResource(filePath);
	}


	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:34:29
	 */
	@Override
    public String getFileBeforeName(String filePathStr) {
		return SFilesUtil.getFileBeforeName(filePathStr);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:34:49
	 */
	@Override
    public File getFileByName(String filePathStr) throws Exception {
		return SFilesUtil.getFileByName(filePathStr,getWebInfPath());
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:35:21
	 */
	@Override
    public String getFileContentByPath(String filePath, String encode)
			throws Exception {
		return SFilesUtil.getFileContentByPath(getAllFilePath(filePath),encode);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:35:46
	 */
	@Override
    public String getFileExtName(String filePathStr) {
		return SFilesUtil.getFileExtName(filePathStr);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:36:02
	 */
	@Override
    public InputStream getFileInputStreamByUrl(String fileUrl) throws Exception {
		return SFilesUtil.getFileInputStreamByUrl(fileUrl,getWebInfPath());
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:36:22
	 */
	@Override
    public long getFileLastModified(String filePath) {
		return SFilesUtil.getFileLastModified(filePath);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:36:41
	 */
	@Override
    public String getFileName(String filePathStr) {
		return SFilesUtil.getFileName(getAllFilePath(filePathStr));
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:36:56
	 */
	@Override
    public String getFilePath(String filePathStr, boolean inZipPath) {
		return SFilesUtil.getFilePath(getAllFilePath(filePathStr),inZipPath);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:36:56
	 */
	@Override
    public String getFilePath(String filePathStr) {
		return SFilesUtil.getFilePath(getAllFilePath(filePathStr),false);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:42:18
	 */
	@Override
    public String getPathLastName(String path) {
		return SFilesUtil.getPathLastName(path);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:42:34
	 */
	@Override
    public ZipFile getZipFileByUrl(URL zipFileUrl) {
		return SFilesUtil.getZipFileByUrl(zipFileUrl);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:42:57
	 */
	@Override
    public ZipFile getZipFileByUrlString(String zipFileUrl) {
		return SFilesUtil.getZipFileByUrlString(zipFileUrl);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:43:13
	 */
	@Override
    public String getZipFileChildPath(String fileUrl) {
		return SFilesUtil.getZipFileChildPath(fileUrl);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:43:27
	 */
	@Override
    public InputStream getZipFileInputStreamByUrl(String fileUrl, Object zipFileObj)
			throws Exception {
		return SFilesUtil.getZipFileInputStreamByUrl(fileUrl,zipFileObj);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:43:41
	 */
	@Override
    public String getZipFilePathByUrlString(String zipFileUrl) {
		return SFilesUtil.getZipFilePathByUrlString(zipFileUrl);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:43:55
	 */
	@Override
    public long getZipFileTime(String filePath) {
		return SFilesUtil.getZipFileTime(filePath);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:44:10
	 */
	@Override
    public boolean isFileExist(String filePath) {
		return SFilesUtil.isFileExist(getAllFilePath(filePath),null);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:44:30
	 */
	@Override
    public boolean isZipFileExist(String filePath) {
		return SFilesUtil.isZipFileExist(filePath);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:45:18
	 */
	@Override
    public boolean isZipUrl(String urlStr) {
		return SFilesUtil.isZipUrl(urlStr);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:45:33
	 */
	@Override
    public void searchZipFilePath(List<String> filePathList, String basePath, List<String> extNameList, boolean returnSubPath) {
		SFilesUtil.searchZipFilePath(
				filePathList,getAllFilePath(basePath),null,extNameList,returnSubPath);
	}



	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-2 上午10:47:09
	 */
	@Override
    public void setFilePath(List<String> filePathList, String basePath,
                            String extName, boolean incChild, boolean returnSubPath) throws Exception {
		SFilesUtil.getFileList(
				filePathList,getAllFilePath(basePath),null,extName,incChild,returnSubPath);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2011-5-5 下午05:51:26
	 */
	@Override
    public void setUploadBasePath(String uploadBasePath) {
		if(uploadBasePath==null || uploadBasePath.length()<1) {
			return;
		}
		this.uploadPath = getAllFilePath(uploadBasePath);
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2011-5-5 下午05:51:55
	 */
	@Override
    public String getUploadBasePath() {
		return uploadPath;
	}
	
    /**
     * 关闭压缩文件对象
     * @param zipFileObj 压缩文件对象
     * 2015年7月16日
     * @author 马宝刚
     */
    @Override
    public void closeZipFile(Object zipFileObj) {
        SFilesUtil.closeZipFile(zipFileObj);
    }
    
    
    /**
     * 将指定内容追加写入文件
     * @param filePath   文件绝对路径（如果文件不存在，会自动建立）
     * @param content    写入内容字符串 (UTF-8编码）
     * @return           是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    @Override
    public boolean addContent(String filePath, String content) {
    	return SFilesUtil.addContent(getAllFilePath(filePath),content);
    }
    
    
    /**
     * 将指定内容写入文件
     * @param filePath   文件绝对路径（如果文件不存在，会自动建立）
     * @param content    写入内容字符串
     * @param encoding   写入内容编码格式
     * @param append     是否为追加写入
     * @return           是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    @Override
    public boolean putContent(String filePath, String content, String encoding, boolean append) {
    	return SFilesUtil.putContent(getAllFilePath(filePath),content,encoding,append);
    }
    
    /**
     * 将指定内容写入文件
     * @param filePath  文件相对路径（如果文件不存在，会自动建立。相对于WEB-INF文件夹）
     * @param contents  写入内容字节数组
     * @param append    是否为追加写入
     * @return          是否写入成功
     * 2018年5月28日
     * @author MBG
     */
    @Override
    public boolean putContent(String filePath, byte[] contents, boolean append) {
		return SFilesUtil.putContent(getAllFilePath(filePath),contents,append);
    }
}
