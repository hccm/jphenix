/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.test;

import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.kernel.objectloader.FBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.standard.docs.ClassInfo;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 测试类父类
 * 
 * 执行名字为 test的方法
 * 
 * 提供资源：
 * 			类工厂
 * 
 * 			数据库操作
 * 
 * 重要：如果测试类与其它类不在同一个文件夹，需要在住配置文件中重新定位类工厂根路径
 * 
 * 方法：
 * 			在当前类的启动入口函数main()中放入 main(当前类.class);
 * 
 * 			运行时，会自动调用当前类中的test方法
 * 
 * <baseClassPath>../webroot/WEB-INF/classes</baseClassPath>
 * 
 * @author 刘虻
 * 2010-9-10 下午01:15:19
 */
@ClassInfo({"2014-06-12 20:02","测试类父类"})
public abstract class TestBeanParent extends ABase {
	
	protected IBeanFactory beanFactory = null; //类加载器
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public TestBeanParent() {
		super();
	}
	
	/**
	 * 获取配置文件路径
	 * 
	 * 如果覆盖此方法，返回配置文件路径，则初始化类加载器
	 * 
	 * 刘虻
	 * 2010-9-10 下午02:39:40
	 * @return
	 */
	protected abstract String getConfigPath();
	
	/**
	 * 终止测试方法 需要时覆盖此方法
	 * 刘虻
	 * 2010-9-10 下午03:53:33
	 */
	protected void destroy() {}
	
	/**
	 * 测试入口
	 * 刘虻
	 * 2010-9-10 下午01:40:19
	 */
	protected static void main(Class<?> cls) {
		_executeTest(cls); //执行测试
	}
	
	/**
	 * 执行测试方法
	 * 刘虻
	 * 2010-9-10 下午01:54:43
	 * @param cls 测试类
	 */
	protected static void _executeTest(Class<?> cls) {
		//测试类
		Object obj = null;
		try {
			obj = cls.newInstance();
			//初始化类加载器
			_initBeanFactory(obj);
			//获取测试方法
			Method test = cls.getMethod("test");
			//执行开始时间
			long startTime = System.currentTimeMillis();
			System.out.println("\n***************************\n* Begin Start test Method *\n***************************");
			//执行测试
			test.invoke(obj);
			System.out.println("\n----------------------------");
			System.out.println("Execute Complete Used:"
					+(System.currentTimeMillis()-startTime)+" ms");
			System.out.println("\n***************************\n* Execute test Method Over*\n***************************");
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			_destroyTest(obj);
		}
	}
	
	
	/**
	 * 终止测试类
	 * 刘虻
	 * 2010-9-10 下午03:52:45
	 * @param obj
	 */
	protected static void _destroyTest(Object obj) {
		Method method = null; //终止方法
		try {
			method = obj.getClass().getDeclaredMethod("destroy");
		}catch(Exception e) {}
		if(method!=null) {
			try {
				method.setAccessible(true);
				method.invoke(obj);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		IBeanFactory bf = null; //类加载器
		try {
			//获取变量字段
			Field bfField = 
				_getSuperClass(obj.getClass()).getDeclaredField("beanFactory");
			if(bfField!=null) {
				bfField.setAccessible(true);
				bf = (IBeanFactory)bfField.get(obj);
			}
		}catch(Exception e) {}
		if(bf!=null) {
			try {
				bf.destroy(IBeanFactory.STATE_REST_AUTO);
			}catch(Exception e) {}
		}
		System.out.println("\n\n14-----------------------------------------------------------------------\nThe Application Has Bean Stoped\n--------------------------------------------------------------------------------------\n");
		System.exit(1);
	}
	
	
	/**
	 * 初始化类加载器
	 * 刘虻
	 * 2010-9-10 下午02:40:59
	 * @param obj 测试类 
	 */
	protected static void _initBeanFactory(Object obj) {
		String configPath = null; //类加载器配置文件路径
		try {
			//获取配置文件路径的方法
			Method method = 
				obj.getClass().getDeclaredMethod("getConfigPath");
			if(method==null) {
				return;
			}
			method.setAccessible(true);
			configPath = (String)method.invoke(obj,new Object[] {});
		}catch(Exception e) {}
		if(configPath==null || configPath.length()<1) {
			return;
		}
		Field bfField = null;
		try {
			bfField = _getSuperClass(obj.getClass()).getDeclaredField("beanFactory");
		}catch(Exception e) {}
		if(bfField==null) {
			return;
		}
		bfField.setAccessible(true);
		//注意：配置文件路径只能用相对路径，否则无法判断出类根路径
		//初始化类加载器
		IBeanFactory beanFactory = null;
		try {
			beanFactory = FBeanFactory.newInstance(configPath,null);
		}catch(Exception e) {
			e.printStackTrace();
		}
		if(beanFactory==null) {
			return;
		}
		try {
			//设置类加载器方法
            Method setBeanFactoryMethod = 
				_getABaseClass(obj.getClass())
					.getDeclaredMethod("setBeanFactory", IBeanFactory.class);
			setBeanFactoryMethod.setAccessible(true);
			setBeanFactoryMethod.invoke(obj, beanFactory);
		}catch(Exception e) {}
		try {
			bfField.set(obj,beanFactory);
		}catch(Exception e) {}
	}
	
	/**
	 * 获取基类
	 * 刘虻
	 * 2010-9-10 下午06:03:01
	 * @param cls 指定类
	 * @return 基类对象
	 */
	protected static Class<?> _getABaseClass(Class<?> cls) {
		if(cls==null || "java.lang.Object".equals(cls.getName())) {
			return null;
		}
		if("com.jphenix.kernel.baseobject.instanceb.ABase".equals(cls.getName())) {
			return cls;
		}
		return _getABaseClass(cls.getSuperclass());
	}
	
	/**
	 * 获取父类对象
	 * 刘虻
	 * 2010-9-10 下午03:42:36
	 * @param cls 指定类
	 * @return 父类对象
	 */
	protected static Class<?> _getSuperClass(Class<?> cls) {
		if(cls==null || "java.lang.Object".equals(cls.getName())) {
			return null;
		}
		if("com.jphenix.share.test.TestBeanParent".equals(cls.getName())) {
			return cls;
		}
		return _getSuperClass(cls.getSuperclass());
	}
	
	
	/**
	 * 输出日志到控制台
	 * 刘虻
	 * 2010-9-10 下午01:50:37
	 * @param content 日志内容
	 */
	public void out(Object content) {
		System.out.println(content);
	}
}
