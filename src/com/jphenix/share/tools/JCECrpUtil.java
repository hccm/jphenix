/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.standard.docs.ClassInfo;

import java.util.Random;

/**
 * 加密解密
 * @author 刘虻
 * 2007-1-24上午10:41:55
 */
@ClassInfo({"2014-06-12 20:02","加密解密JCECrpUtil"})
public class JCECrpUtil {
        
    protected static JCECrpUtil mvJCECrpUtilInstance;

    public static boolean equals(byte[] src, byte[] dest) {
        int lvLen = src.length;
        if (lvLen == dest.length) {
            for (int i = 0; i < lvLen; i++) {
                if (src[i] != dest[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static String encrypt(String src) {
        return encrypt(src.getBytes());
    }

    public static String encrypt(byte[] src) {
        String ret = "";
        byte[] dest = encryptByte(src);
        int lvLen = dest.length;
        for (int i = 0; i < lvLen; i++) {
            ret += (new Byte(dest[i])).intValue();
        }
        return ret;
    }

    protected static byte[] encryptByte(byte[] src) {
        int lvLen = src.length;
        byte[] dest = new byte[lvLen];
        char a1;
        char a2;
        for (int i = 0; i < lvLen; i++) {
            a1 = (char) ((src[i]) ^ 5);
            a1 = (char) (a1 << 4);
            a2 = (char) ((src[i]) ^ 0xaf);
            a2 = (char) (a2 >> 4);
            dest[i] = (byte) (a1 |  a2);
        }
        return dest;
    }

    /**
     * 网站应用密码加密 此方法中不能使用 <> " 因为避免将值放入输入框中发生错误
     * @author 刘虻
     * 2008-7-9上午01:25:25
     * @param src
     * @return
     */
    public static String webPWDEncrypt(String src) {
        String code = "120#$%()Z+4=-5XY/7[6]?abcdHefghijkGlmnopqrIs}tuvFwxyzA9@BCD{EJKLMN8OPQRSTUV3W`!^*~_|:;,&.\\";
        String dest = "";
        int codelen;
        int i;
        int ptr = -1;
        codelen = code.length();
        for (i = 0; i < src.length(); i++) {
            ptr = code.indexOf(src.substring(i, i + 1));
            if (ptr == -1) {
                ptr = ptr + 1;
            }
            dest = dest + code.substring(codelen - ptr - 1, codelen - ptr);
            
        }
        return dest;
    }
    
    /**
     * 判断字符串是否经过加密，如果经过加密，则返回解密字符串，否则返回源字符串
     * @param src 待检测字符串
     * @return 解密后的字符串
     * 2014年4月4日
     * @author 马宝刚
     */
    public static String checkDecode(String src) {
        if(src==null || src.length()<1) {
            return "";
        }
        if(!src.endsWith("==")) {
            return src;
        }
        return webPWDEncrypt(src.substring(0,src.length()-2));
    }
    
    
    public static String pWDEncrypt(String src) {
        String code = "120#$%()Z+4=-5XY/7[6]?abcdHefghijkGlmnopqrIs}tuvFwxyzA9@BCD{EJKLMN8OPQRSTUV3W`!^*~_|:;,&.\\<>'\"";
        String dest = "";
        int codelen;
        int i;
        int ptr = -1;
        codelen = code.length();
        for (i = 0; i < src.length(); i++) {
            ptr = code.indexOf(src.substring(i, i + 1));
            if (ptr == -1) {
                ptr = ptr + 1;
            }
            dest = dest + code.substring(codelen - ptr - 1, codelen - ptr);
        }
        return dest;
    }

    public static String ftpPWDEncrypt(String src) {
        String code = "120#$%()Z+4=-5XY/7[6]?abcdHefghijkGlmnopqrIs}tuvFwxyzA9@BCD{EJKLMN8OPQRSTUV3W`!^*~_|:;,&.\\<>'\"";
        String dest = "";
        int codelen;
        int i;
        int ptr = -1;
        codelen = code.length();
        for (i = 0; i < src.length(); i++) {
            ptr = code.indexOf(src.substring(i, i + 1));
            if (ptr == -1) {
                ptr = ptr + 1;
            }
            dest = dest + code.substring(codelen - ptr - 1, codelen - ptr);
        }
        return dest;
    }

    public static String getAtestationCode(int codelen) {
        String alpha = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer tmpstr = new StringBuffer(codelen);
        int iRandNum;
        Random rnd = new Random();
        for (int i = 0; i < codelen; i++) {
            iRandNum = Math.abs(rnd.nextInt()) % 62;
            tmpstr.append(alpha, iRandNum, iRandNum + 1);
        }
        return tmpstr.toString();
    }
    
    public static void main(String[] argv){
        System.out.println(JCECrpUtil.webPWDEncrypt("{Ab%@}"));
        System.out.println(JCECrpUtil.webPWDEncrypt("\"l\"\"<=#0iuTR2221\"\"\""));
    }
}