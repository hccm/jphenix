/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年2月17日
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.share.lang.SString;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * INI配置文件解析类
 * 
 * 2020-06-14 修改了读入流最后关闭
 * 
 * 注意：只支持UTF-8编码格式
 * @author MBG
 * 2017年2月17日
 */
@ClassInfo({"2020-06-14 09:49","INI配置文件解析类"})
public class IniConfProp {

	//配置信息容器
	private Map<String,Map<String,String>> pMap = new HashMap<String,Map<String,String>>();
	
	/**
	 * 构造函数
	 * @param filePath 配置文件全路径
	 * @author MBG
	 */
	public IniConfProp(String filePath) {
		super();
		parse(filePath); //执行解析
	}
	
	/**
	 * 构造函数
	 * @param is 配置文件读入流
	 * @author MBG
	 */
	public IniConfProp(InputStream is) {
		super();
		parse(is); //执行解析
	}
	
	
	/**
	 * 移除指定段中的指定主键
	 * @param title 指定段主键
	 * @param key 参数主键
	 * @return 被移除的参数值
	 * 2017年2月17日
	 * @author MBG
	 */
	public String remove(String title,String key) {
		if(title==null) {
			title = "";
		}
		//获取参数段对照容器
		Map<String,String> subMap = pMap.get(title);
		if(subMap==null) {
			return "";
		}
		return SString.valueOf(subMap.remove(key));
	}
	
	/**
	 * 移除默认段中指定参数值
	 * @param key 参数主键
	 * @return 对应参数值
	 * 2017年2月17日
	 * @author MBG
	 */
	public String remove(String key) {
		return remove("",key);
	}
	
	/**
	 * 获取默认段的指定参数值
	 * @param key 参数主键
	 * @return 参数值
	 * 2017年2月17日
	 * @author MBG
	 */
	public String value(String key) {
		return value("",key);
	}
	
	/**
	 * 获取指定段的指定参数值
	 * @param title 参数段主键
	 * @param key 参数主键
	 * @return 参数值
	 * 2017年2月17日
	 * @author MBG
	 */
	public String value(String title,String key) {
		if(title==null) {
			title = "";
		}
		//获取参数段对照容器
		Map<String,String> subMap = pMap.get(title);
		if(subMap==null) {
			return "";
		}
		return SString.valueOf(subMap.get(key));
	}
	
	/**
	 * 获取指定段的参数信息对照容器
	 * @param title 段主键
	 * @return 指定段的参数信息对照容器
	 * 2017年2月17日
	 * @author MBG
	 */
	public Map<String,String> valueMap(String title){
		if(title==null) {
			title = "";
		}
		//获取参数段对照容器
		Map<String,String> subMap = pMap.get(title);
		if(subMap==null) {
			return new HashMap<String,String>();
		}
		return subMap;
	}
	
	/**
	 * 返回整个配置信息对照容器
	 * @return 整个配置信息对照容器
	 * 2017年2月17日
	 * @author MBG
	 */
	public Map<String,Map<String,String>> valueMap(){
		return pMap;
	}
	
	/**
	 * 执行解析配置文件
	 * @param filePath 配置文件全路径
	 * 2017年2月17日
	 * @author MBG
	 */
	private void parse(String filePath) {
		try {
			parse(new FileInputStream(new File(filePath))); 
		}catch(Exception e) {
			e.printStackTrace();
			System.err.println("IniConfProp The Ini Config File Path:["+filePath+"] Exception:["+e+"]");
		}
	}
	
	/**
	 * 执行解析
	 * @param is 配置文件读入流（解析后自动关闭读入流）
	 * 2017年2月17日
	 * @author MBG
	 */
	private void parse(InputStream is) {
		if(is==null) {
			return;
		}
		BufferedReader br = null; //信息读入类
		try {
			//构建信息读入类
			br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
    		String line;
    		String title = ""; //配置信息段，即 [配置信息段]
    		Map<String,String> subMap = null; //段内信息对照容器
    		int point; //参数信息分割点
    		while((line=br.readLine())!=null) {
    			line =  BaseUtil.trim(line," ","\t");
    			if(line.length()<1) {
    				continue;
    			}
    			if(line.startsWith(";") || line.startsWith("#")) {
    				//注释
    				continue;
    			}
    			if(line.startsWith("[") && line.endsWith("]")) {
    				title = line.substring(1,line.length()-1);
    				continue;
    			}
    			point = line.indexOf("=");
    			if(point<0) {
    				//非法参数   不是标准的   key = value
    				continue;
    			}
    			subMap = pMap.get(title);
    			if(subMap==null) {
    				subMap = new HashMap<String,String>();
    				pMap.put(title,subMap);
    			}
    			//放入容器中
    			subMap.put(
    			   BaseUtil.trim(line.substring(0,point)," ","\t")
    			  ,BaseUtil.trim(line.substring(point+1)," ","\t"));
    		}
		}catch(Exception e) {
			e.printStackTrace();
			System.err.println("IniConfProp Parse Ini Config File Exception:["+e+"]");
		}finally {
			try {
				br.close();
			}catch(Exception e) {}
			try {
				is.close();
			}catch(Exception e) {}
			is = null;
		}
	}
}
