/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jphenix.driver.threadpool.ThreadSession;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.servlet.IServletConst;

/**
 * 生成动态图片类
 * 
 * 2018-10-31 增加了CentOS中的默认字体识别。否则验证码显示不出来。如果还是显示不出来，就从windows中的 c:\windows\fonts中复制出
 *            Times New Roman 字体，复制到linux的 usr/share/fonts/Times New Roman 文件夹中，然后用 fc-list 命令查看支持哪些字体
 *            
 * 2018-11-01 设置了windows跟linux都拥有的字体名。并且新增了返回系统拥有字体名信息方法 getFontNames
 *            妈的，CentOS下，怎么弄都是乱码，直接从windows中复制Arial字体过去
 *            
 * 2020-07-15 增加了输出图片大小
 * 
 * @author 刘虻
 * 2007-10-12上午11:22:15
 */
@ClassInfo({"2020-07-15 18:00","生成动态图片类"})
public class DynamicImageBean {

	//随机处理类
    protected Random random = new Random();
	
    protected int molestLineCount = 15; //干扰线数量
    
    protected int defaultStringLength = 4; //默认随机字符数量
    
    protected int defaultImageWidth = 60; //默认图片宽
    
    protected int defaultImageHeight = 20; //默认图片高
    
    protected String defaultSessionKey = "randimage"; //默认随机字符串放入会话中的主键
    
    protected String randStringSessioonKey = null; //随机号的会话主键
    
    protected int defaultFrontColor = 200; //默认前景色
    
    protected int defaultBackColor = 250; //默认背景色
    
    protected int imageWidth = 0; //图片宽
    
    protected int imageHeight = 0; //图片高
    
    protected int frontColor = 0; //图片前景色
    
    protected int backColor = 0; //图片背景色
    
    protected int randStringLength = 0; //随机字符串长度
    
	/**
	 * 构造函数
	 * 2007-10-12上午11:22:17
	 */
	public DynamicImageBean() {
		super();
	}
	
	/**
	 * 获取随机字符串长度
	 * @author 刘虻
	 * 2007-10-12下午12:33:20
	 * @return 随机字符串长度
	 */
	public int getRandStringLength() {
		if (randStringLength<1) {
			randStringLength = defaultStringLength;
		}
		return randStringLength;
	}
	
	/**
	 * 设置随机字符串长度
	 * @author 刘虻
	 * 2007-10-12下午12:33:30
	 * @param randStringLength 随机字符串长度
	 */
	public void setRandStringLength(int randStringLength) {
		this.randStringLength = randStringLength;
	}
	
	/**
	 * 获取背景色
	 * @author 刘虻
	 * 2007-10-12下午12:33:38
	 * @return 背景色
	 */
	public int getBackColor() {
		if (backColor<1) {
			backColor = defaultBackColor;
		}
		return backColor;
	}
	
	/**
	 * 设置背景色
	 * @author 刘虻
	 * 2007-10-12下午12:33:49
	 * @param backColor 背景色
	 */
	public void setBackColor(int backColor) {
		this.backColor = backColor;
	}
	
	/**
	 * 获取前景色
	 * @author 刘虻
	 * 2007-10-12下午12:33:57
	 * @return 前景色
	 */
	public int getFrontColor() {
		if (frontColor<1) {
			frontColor = defaultFrontColor;
		}
		return frontColor;
	}
	
	/**
	 * 设置前景色
	 * @author 刘虻
	 * 2007-10-12下午12:34:14
	 * @param frontColor 前景色
	 */
	public void setFrontColor(int frontColor) {
		this.frontColor = frontColor;
	}
	
	/**
	 * 设置图片高
	 * @author 刘虻
	 * 2007-10-12下午12:34:19
	 * @param imageHeight 图片高
	 */
	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}
	
	/**
	 * 获取图片高
	 * @author 刘虻
	 * 2007-10-12下午12:34:32
	 * @return 图片高
	 */
	public int getImageHeight() {
		if (imageHeight<1) {
			imageHeight = defaultImageHeight;
		}
		return imageHeight;
	}
	
	/**
	 * 设置图片宽
	 * @author 刘虻
	 * 2007-10-12下午12:34:39
	 * @param imageWidth 图片宽
	 */
	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}
	
	/**
	 * 获取图片宽
	 * @author 刘虻
	 * 2007-10-12下午12:34:52
	 * @return 图片宽
	 */
	public int getImageWidth() {
		if (imageWidth<1) {
			imageWidth = defaultImageWidth;
		}
		return imageWidth;
	}
	
	/**
	 * 设置随机号的会话主键
	 * @author 刘虻
	 * 2007-10-12下午12:19:18
	 * @param randStringSessioonKey 随机号的会话主键
	 */
	public void setRandStringSessioonKey(String randStringSessioonKey) {
		this.randStringSessioonKey = randStringSessioonKey;
	}
	
	/**
	 * 获取随机号的会话主键
	 * @author 刘虻
	 * 2007-10-12下午12:19:24
	 * @return 随机号的会话主键
	 */
	public String getRandStringSessioonKey() {
		if (randStringSessioonKey==null) {
			randStringSessioonKey = defaultSessionKey;
		}
		return randStringSessioonKey;
	}
	
	/**
	 * 构造随机颜色
	 * @author 刘虻
	 * 2007-10-12上午11:24:04
	 * @param fColor 前景颜色
	 * @param bColor 背景颜色
	 * @return 随机颜色
	 */
	protected Color getRandColor(int fColor,int bColor){

		if(fColor>255) {
        	fColor=255;
        }
        if(bColor>255) {
        	bColor=255;
        }
        //红
        int r=fColor+random.nextInt(bColor-fColor);
        //绿
        int g=fColor+random.nextInt(bColor-fColor);
        //蓝
        int b=fColor+random.nextInt(bColor-fColor);
        
        return new Color(r,g,b);
	}
	
	
	
	/**
	 * 获取随机图片
	 * @author 刘虻
	 * 2007-10-12下午12:08:41
	 * @param randStr 图片中的随机字符串
	 * @param width 图片宽
	 * @param height 图片高
	 * @return 随机图片
	 */
	public BufferedImage getRandImage(String randStr,int width,int height) {
		
		//构造返回值
		BufferedImage image = 
			new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		//获取图形上下文
		Graphics2D graphics = (Graphics2D)image.getGraphics();

		//设定背景色
		graphics.setColor(getRandColor(getFrontColor(),getBackColor()));
		graphics.fillRect(0, 0, width, height);
		
		//设定字体
		graphics.setFont(new Font("cmr10",Font.PLAIN,18));
		
		//随机产生干扰线，使图象中的认证码不易被其它程序探测到
		graphics.setStroke(new BasicStroke(2.0f)); //设置线条粗细
		for (int i=0;i<molestLineCount;i++){
			//干扰线坐标
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			graphics.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));
			graphics.drawLine(x,y,x+xl,y+yl);
		}
		
		if (randStr!=null) {
			int size = randStr.length();
			
			for (int i=0;i<size;i++) {
				//获取一个字符
				String str = randStr.substring(i,i+1);
			    // 将认证码显示到图象中
				//调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
				graphics
					.setColor(
							new Color(
									20+random.nextInt(110)
									,20+random.nextInt(110)
									,20+random.nextInt(110)));
				graphics.drawString(str,13*i+6,16);
			}
		}
		
		//图象生效
		graphics.dispose();
		
		return image;
	}
	
	
	/**
	 * 建立随机图片,并将随机号放入session中(主键为defaultSessionKey)
	 * @author 刘虻
	 * 2007-10-12下午12:04:42
	 * @param req 页面请求
	 * @param resp 页面反馈
	 * @throws Exception 执行发生异常
	 */
	public String createRandImageAction(
			HttpServletRequest req
			,HttpServletResponse resp) throws Exception {
		//执行生成图片
		return createRandImageAction(
				req
				,resp
				,getRandStringLength()
				,getImageWidth()
				,getImageHeight());
	}
			
	/**
	 * 建立随机图片,并将随机号放入session中(主键为defaultSessionKey)
	 * @author 刘虻
	 * 2007-10-12下午12:04:42
	 * @param req 页面请求
	 * @param resp 页面反馈
	 * @param strLength 随机字符串长度
	 * @param imageWidth 图片宽
	 * @param imageHeight 图片高
	 * @throws Exception 执行发生异常
	 */
	public String createRandImageAction(
			HttpServletRequest req
			,HttpServletResponse resp
			,int strLength
			,int imageWidth
			,int imageHeight) throws Exception {
		
		//获取随机字符串
		String randStr = getRandString(strLength);
		//获取随机图片
		BufferedImage image = getRandImage(randStr,imageWidth,imageHeight);
		
		//放入会话
		req.getSession().setAttribute(getRandStringSessioonKey(), randStr);
		
		//设置允许输出图片
		System.setProperty("java.awt.headless","true");
		resp.flushBuffer();
		resp.setContentType("image/jpeg");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setHeader("Pragma", "no-cache");
		resp.setDateHeader("Expires", 0); 
		//输出图象到页面内存中计算图片大小
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(image,"jpg",bos);
		
		resp.setContentLength(bos.size()); //设置长度
		
		//获取输出流
		OutputStream os = null;
		
		try {
			os = resp.getOutputStream();
			os.write(bos.toByteArray());
			os.flush();
		}catch(Exception e) {}
		finally {
			try {
				os.close();
			}catch(Exception e) {}
		}
		
		return randStr;
	}
	
	/**
	 * 显示随机验证码 （会话主键：randimage）
	 * @param req         页面请求
	 * @param resp        页面反馈
	 * @throws Exception  异常
	 * 2017年4月12日
	 * @author MBG
	 */
	public static void show(HttpServletRequest req,HttpServletResponse resp)throws Exception {
		show(null,req,resp);
	}
	
	/**
	 * 显示随机验证码
	 * @param sessionKey  图片值的会话主键
	 * @param req         页面请求
	 * @param resp        页面反馈
	 * @throws Exception  异常
	 * 2017年4月12日
	 * @author MBG
	 */
	public static void show(String sessionKey,HttpServletRequest req,HttpServletResponse resp)throws Exception {
		//构建绘图类实例
		DynamicImageBean dib = new DynamicImageBean();
		if(sessionKey!=null && sessionKey.length()>0) {
			dib.randStringSessioonKey = sessionKey;
		}
		dib.createRandImageAction(req,resp);
	}
	
	/**
	 * 显示随机验证码 （会话主键：randimage）
	 * 动作上下文对象需要提前放在线程会话中（常规脚本调用时，已经自动放入）
	 * @throws Exception 异常
	 * 2017年4月12日
	 * @author MBG
	 */
	public static void show() throws Exception {
		show(null);
	}
	
	/**
	 * 显示随机验证码
	 * 动作上下文对象需要提前放在线程会话中（常规脚本调用时，已经自动放入）
	 * @param sessionKey 图片值的会话主键
	 * @throws Exception 异常
	 * 2017年4月12日
	 * @author MBG
	 */
	public static void show(String sessionKey) throws Exception {
		//从线程会话中获取动作上下文
		IActionContext ac = 
				(IActionContext)ThreadSession.get(IServletConst.KEY_ACTION_CONTEXT);
		if(ac!=null) {
			//构建绘图类实例
			DynamicImageBean dib = new DynamicImageBean();
			if(sessionKey!=null && sessionKey.length()>0) {
				dib.randStringSessioonKey = sessionKey;
			}
			dib.createRandImageAction(ac.getRequest(),ac.getResponse());
		}
	}
	
	/**
	 * 获取随机字符串
	 * @author 刘虻
	 * 2007-10-12上午11:31:00
	 * @param length 字符串长度
	 * @return 随机字符串
	 */
	public String getRandString(int length) {
		//构造返回值
		StringBuffer reSbf = new StringBuffer();
		for (int i=0;i<length;i++){
			reSbf.append(random.nextInt(10));
		}
		return reSbf.toString();
	}
	
	
	/**
	 * 获取系统拥有字体名
	 * 
	 * 有些系统中可能无法显示验证码数字，是因为系统中没有预设的字体
	 * 可以用该方法获取系统拥有的字体名信息
	 * 
	 * @return 系统拥有的字体名信息
	 * 2018年11月1日
	 * @author MBG
	 */
	public static String getFontNames() {
		//构建返回值
		StringBuffer reSbf = new StringBuffer();
	    GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    String[] fontNames = e.getAvailableFontFamilyNames();
	    for (String fontName : fontNames) {
	    	reSbf.append(fontName).append(",");
	    }
	    return reSbf.toString();
	}
	
}
