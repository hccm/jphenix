/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.standard.docs.ClassInfo;

import java.util.HashMap;

/**
 * 通过邮编/区号获取省份
 * @author 刘虻
 * 2007-2-2下午06:14:27
 */
@ClassInfo({"2014-06-12 20:02","通过邮编/区号获取省份"})
public class YouBianQuHaoTool {

	protected final HashMap<String,String> youBianHashMap = new HashMap<String,String>(); //邮编地址容器
	protected final HashMap<String,String> quhaoHashMap = new HashMap<String,String>(); //区号地址容器
	
	/**
	 * 构造函数 
	 * @author 刘虻
	 * 2007-2-2 下午05:56:20
	 */
	public YouBianQuHaoTool() {
		super();
		init(); //执行初始化
	}


	/**
	 * 设置区号
	 * @author 刘虻
	 * 2007-2-12下午04:44:01
	 */
	protected void putQuHao() {
		
		quhaoHashMap.put("010","北京");

		quhaoHashMap.put("016","湖北");

		quhaoHashMap.put("020","广东");

		quhaoHashMap.put("021","上海");

		quhaoHashMap.put("022","天津");

		quhaoHashMap.put("023","重庆");

		quhaoHashMap.put("024","辽宁");

		quhaoHashMap.put("025","江苏");

		quhaoHashMap.put("027","湖北");

		quhaoHashMap.put("028","四川");

		quhaoHashMap.put("029","陕西");

		quhaoHashMap.put("0310","河北");

		quhaoHashMap.put("0311","河北");

		quhaoHashMap.put("0312","河北");

		quhaoHashMap.put("0313","河北");

		quhaoHashMap.put("0314","河北");

		quhaoHashMap.put("0315","河北");

		quhaoHashMap.put("0316","河北");

		quhaoHashMap.put("0317","河北");

		quhaoHashMap.put("0318","河北");

		quhaoHashMap.put("0319","河北");

		quhaoHashMap.put("03235","河北");

		quhaoHashMap.put("0335","河北");

		quhaoHashMap.put("0349","山西");

		quhaoHashMap.put("0350","山西");

		quhaoHashMap.put("0351","山西");

		quhaoHashMap.put("0352","山西");

		quhaoHashMap.put("0353","山西");

		quhaoHashMap.put("0354","山西");

		quhaoHashMap.put("0355","山西");

		quhaoHashMap.put("0356","山西");

		quhaoHashMap.put("0357","山西");

		quhaoHashMap.put("0358","山西");

		quhaoHashMap.put("0359","山西");

		quhaoHashMap.put("0370","河南");

		quhaoHashMap.put("0371","河南");

		quhaoHashMap.put("0372","河南");

		quhaoHashMap.put("0373","河南");

		quhaoHashMap.put("0374","河南");

		quhaoHashMap.put("0375","河南");

		quhaoHashMap.put("0376","河南");

		quhaoHashMap.put("0377","河南");

		quhaoHashMap.put("0378","河南");

		quhaoHashMap.put("0379","河南");

		quhaoHashMap.put("03862","河南");

		quhaoHashMap.put("03863","河南");

		quhaoHashMap.put("03864","河南");

		quhaoHashMap.put("03866","河南");

		quhaoHashMap.put("03867","河南");

		quhaoHashMap.put("03868","河南");

		quhaoHashMap.put("0391","河南");

		quhaoHashMap.put("0392","河南");

		quhaoHashMap.put("0393","河南");

		quhaoHashMap.put("0394","河南");

		quhaoHashMap.put("0395","河南");

		quhaoHashMap.put("0396","河南");

		quhaoHashMap.put("0398","河南");

		quhaoHashMap.put("0410","辽宁");

		quhaoHashMap.put("0411","辽宁");

		quhaoHashMap.put("0412","辽宁");

		quhaoHashMap.put("0413","辽宁");

		quhaoHashMap.put("0414","辽宁");

		quhaoHashMap.put("0415","辽宁");

		quhaoHashMap.put("0416","辽宁");

		quhaoHashMap.put("0417","辽宁");

		quhaoHashMap.put("0418","辽宁");

		quhaoHashMap.put("0419","辽宁");

		quhaoHashMap.put("0421","辽宁");

		quhaoHashMap.put("04266","辽宁");

		quhaoHashMap.put("0427","辽宁");

		quhaoHashMap.put("0429","辽宁");

		quhaoHashMap.put("0431","吉林");

		quhaoHashMap.put("0432","吉林");

		quhaoHashMap.put("0433","吉林");

		quhaoHashMap.put("0434","吉林");

		quhaoHashMap.put("0435","吉林");

		quhaoHashMap.put("0436","吉林");

		quhaoHashMap.put("0437","吉林");

		quhaoHashMap.put("0438","吉林");

		quhaoHashMap.put("0439","吉林");

		quhaoHashMap.put("0440","吉林");

		quhaoHashMap.put("0448","吉林");

		quhaoHashMap.put("0450","黑龙江");

		quhaoHashMap.put("0451","黑龙江");

		quhaoHashMap.put("0452","黑龙江");

		quhaoHashMap.put("0453","黑龙江");

		quhaoHashMap.put("0454","黑龙江");

		quhaoHashMap.put("0455","黑龙江");

		quhaoHashMap.put("0456","黑龙江");

		quhaoHashMap.put("0457","黑龙江");

		quhaoHashMap.put("0458","黑龙江");

		quhaoHashMap.put("0459","黑龙江");

		quhaoHashMap.put("0470","内蒙古");

		quhaoHashMap.put("0471","内蒙古");

		quhaoHashMap.put("0472","内蒙古");

		quhaoHashMap.put("0473","内蒙古");

		quhaoHashMap.put("0474","内蒙古");

		quhaoHashMap.put("0475","内蒙古");

		quhaoHashMap.put("0476","内蒙古");

		quhaoHashMap.put("0477","内蒙古");

		quhaoHashMap.put("0478","内蒙古");

		quhaoHashMap.put("0479","内蒙古");

		quhaoHashMap.put("0482","内蒙古");

		quhaoHashMap.put("04831","内蒙古");

		quhaoHashMap.put("04887","内蒙古");

		quhaoHashMap.put("04888","内蒙古");

		quhaoHashMap.put("0510","江苏");

		quhaoHashMap.put("0511","江苏");

		quhaoHashMap.put("0512","江苏");

		quhaoHashMap.put("0513","江苏");

		quhaoHashMap.put("0514","江苏");

		quhaoHashMap.put("0515","江苏");

		quhaoHashMap.put("0516","江苏");

		quhaoHashMap.put("0517","江苏");

		quhaoHashMap.put("0518","江苏");

		quhaoHashMap.put("0519","江苏");

		quhaoHashMap.put("0520","江苏");

		quhaoHashMap.put("0523","江苏");

		quhaoHashMap.put("0527","江苏");

		quhaoHashMap.put("0530","山东");

		quhaoHashMap.put("0531","山东");

		quhaoHashMap.put("0532","山东");

		quhaoHashMap.put("0533","山东");

		quhaoHashMap.put("0534","山东");

		quhaoHashMap.put("0535","山东");

		quhaoHashMap.put("0536","山东");

		quhaoHashMap.put("0537","山东");

		quhaoHashMap.put("0538","山东");

		quhaoHashMap.put("0539","山东");

		quhaoHashMap.put("0543","山东");

		quhaoHashMap.put("0546","山东");

		quhaoHashMap.put("0550","安徽");

		quhaoHashMap.put("0551","安徽");

		quhaoHashMap.put("0552","安徽");

		quhaoHashMap.put("0553","安徽");

		quhaoHashMap.put("0554","安徽");

		quhaoHashMap.put("0555","安徽");

		quhaoHashMap.put("0556","安徽");

		quhaoHashMap.put("0557","安徽");

		quhaoHashMap.put("0558","安徽");

		quhaoHashMap.put("0559","安徽");

		quhaoHashMap.put("0561","安徽");

		quhaoHashMap.put("0562","安徽");

		quhaoHashMap.put("0563","安徽");

		quhaoHashMap.put("05633","山东");

		quhaoHashMap.put("0564","安徽");

		quhaoHashMap.put("0565","安徽");

		quhaoHashMap.put("0566","安徽");

		quhaoHashMap.put("0570","浙江");

		quhaoHashMap.put("0571","浙江");

		quhaoHashMap.put("0572","浙江");

		quhaoHashMap.put("0573","浙江");

		quhaoHashMap.put("0574","浙江");

		quhaoHashMap.put("0575","浙江");

		quhaoHashMap.put("0576","浙江");

		quhaoHashMap.put("0577","浙江");

		quhaoHashMap.put("0578","浙江");

		quhaoHashMap.put("0579","浙江");

		quhaoHashMap.put("0580","浙江");

		quhaoHashMap.put("0591","福建");

		quhaoHashMap.put("0592","福建");

		quhaoHashMap.put("0593","福建");

		quhaoHashMap.put("0594","福建");

		quhaoHashMap.put("0595","福建");

		quhaoHashMap.put("0596","福建");

		quhaoHashMap.put("0597","福建");

		quhaoHashMap.put("0598","福建");

		quhaoHashMap.put("0599","福建");

		quhaoHashMap.put("0622","广东");

		quhaoHashMap.put("0631","山东");

		quhaoHashMap.put("0632","山东");

		quhaoHashMap.put("0633","山东");

		quhaoHashMap.put("0634","山东");

		quhaoHashMap.put("0635","山东");

		quhaoHashMap.put("0660","广东");

		quhaoHashMap.put("0661","广东");

		quhaoHashMap.put("0662","广东");

		quhaoHashMap.put("0663","广东");

		quhaoHashMap.put("0668","广东");

		quhaoHashMap.put("0688","广东");

		quhaoHashMap.put("0691","云南");

		quhaoHashMap.put("0692","云南");

		quhaoHashMap.put("0701","江西");

		quhaoHashMap.put("0710","湖北");

		quhaoHashMap.put("0711","湖北");

		quhaoHashMap.put("0712","湖北");

		quhaoHashMap.put("0713","湖北");

		quhaoHashMap.put("0714","湖北");

		quhaoHashMap.put("0715","湖北");

		quhaoHashMap.put("0716","湖北");

		quhaoHashMap.put("0717","湖北");

		quhaoHashMap.put("0718","湖北");

		quhaoHashMap.put("0719","湖北");

		quhaoHashMap.put("0722","湖北");

		quhaoHashMap.put("0724","湖北");

		quhaoHashMap.put("0728","湖北");

		quhaoHashMap.put("0730","湖南");

		quhaoHashMap.put("0731","湖南");

		quhaoHashMap.put("0732","湖南");

		quhaoHashMap.put("0733","湖南");

		quhaoHashMap.put("0734","湖南");

		quhaoHashMap.put("0735","湖南");

		quhaoHashMap.put("0736","湖南");

		quhaoHashMap.put("0737","湖南");

		quhaoHashMap.put("0738","湖南");

		quhaoHashMap.put("0739","湖南");

		quhaoHashMap.put("0743","湖南");

		quhaoHashMap.put("0744","湖南");

		quhaoHashMap.put("0745","湖南");

		quhaoHashMap.put("0746","湖南");

		quhaoHashMap.put("0750","广东");

		quhaoHashMap.put("0751","广东");

		quhaoHashMap.put("0752","广东");

		quhaoHashMap.put("0753","广东");

		quhaoHashMap.put("0754","广东");

		quhaoHashMap.put("0755","广东");

		quhaoHashMap.put("0756","广东");

		quhaoHashMap.put("0757","广东");

		quhaoHashMap.put("0758","广东");

		quhaoHashMap.put("0759","广东");

		quhaoHashMap.put("0760","广东");

		quhaoHashMap.put("0762","广东");

		quhaoHashMap.put("0763","广东");

		quhaoHashMap.put("0765","广东");

		quhaoHashMap.put("0766","广东");

		quhaoHashMap.put("0768","广东");

		quhaoHashMap.put("0769","广东");

		quhaoHashMap.put("0770","广西");

		quhaoHashMap.put("0771","广西");

		quhaoHashMap.put("0772","广西");

		quhaoHashMap.put("0773","广西");

		quhaoHashMap.put("0774","广西");

		quhaoHashMap.put("0775","广西");

		quhaoHashMap.put("0776","广西");

		quhaoHashMap.put("0777","广西");

		quhaoHashMap.put("0778","广西");

		quhaoHashMap.put("0779","广西");

		quhaoHashMap.put("0790","江西");

		quhaoHashMap.put("0791","江西");

		quhaoHashMap.put("0792","江西");

		quhaoHashMap.put("0793","江西");

		quhaoHashMap.put("0794","江西");

		quhaoHashMap.put("0795","江西");

		quhaoHashMap.put("0796","江西");

		quhaoHashMap.put("0797","江西");

		quhaoHashMap.put("0798","江西");

		quhaoHashMap.put("0799","江西");

		quhaoHashMap.put("08015","西藏");

		quhaoHashMap.put("08016","西藏");

		quhaoHashMap.put("08017","西藏");

		quhaoHashMap.put("08018","西藏");

		quhaoHashMap.put("08040","西藏");

		quhaoHashMap.put("08049","西藏");

		quhaoHashMap.put("08051","西藏");

		quhaoHashMap.put("08054","西藏");

		quhaoHashMap.put("08056","西藏");

		quhaoHashMap.put("08057","西藏");

		quhaoHashMap.put("08059","西藏");

		quhaoHashMap.put("08061","西藏");

		quhaoHashMap.put("08062","西藏");

		quhaoHashMap.put("08064","西藏");

		quhaoHashMap.put("08067","西藏");

		quhaoHashMap.put("08069","西藏");

		quhaoHashMap.put("08078","西藏");

		quhaoHashMap.put("08081","西藏");

		quhaoHashMap.put("0811","四川");

		quhaoHashMap.put("0812","四川");

		quhaoHashMap.put("0813","四川");

		quhaoHashMap.put("0814","四川");

		quhaoHashMap.put("0815","四川");

		quhaoHashMap.put("0816","四川");

		quhaoHashMap.put("0817","四川");

		quhaoHashMap.put("0818","四川");

		quhaoHashMap.put("08212","四川");

		quhaoHashMap.put("08213","四川");

		quhaoHashMap.put("08216","四川");

		quhaoHashMap.put("08217","四川");

		quhaoHashMap.put("08220","四川");

		quhaoHashMap.put("08221","四川");

		quhaoHashMap.put("08222","四川");

		quhaoHashMap.put("08223","四川");

		quhaoHashMap.put("08224","四川");

		quhaoHashMap.put("08225","四川");

		quhaoHashMap.put("08226","四川");

		quhaoHashMap.put("08227","四川");

		quhaoHashMap.put("08228","四川");

		quhaoHashMap.put("08229","四川");

		quhaoHashMap.put("08230","四川");

		quhaoHashMap.put("08231","四川");

		quhaoHashMap.put("08232","四川");

		quhaoHashMap.put("08235","四川");

		quhaoHashMap.put("08236","四川");

		quhaoHashMap.put("08237","四川");

		quhaoHashMap.put("08241","四川");

		quhaoHashMap.put("08247","四川");

		quhaoHashMap.put("0825","四川");

		quhaoHashMap.put("08255","四川");

		quhaoHashMap.put("08260","四川");

		quhaoHashMap.put("08267","四川");

		quhaoHashMap.put("08268","四川");

		quhaoHashMap.put("08269","四川");

		quhaoHashMap.put("08270","四川");

		quhaoHashMap.put("08276","四川");

		quhaoHashMap.put("08277","四川");

		quhaoHashMap.put("08278","四川");

		quhaoHashMap.put("08279","四川");

		quhaoHashMap.put("08282","四川");

		quhaoHashMap.put("08283","四川");

		quhaoHashMap.put("08284","四川");

		quhaoHashMap.put("08285","四川");

		quhaoHashMap.put("08286","四川");

		quhaoHashMap.put("08287","四川");

		quhaoHashMap.put("08288","四川");

		quhaoHashMap.put("08289","四川");

		quhaoHashMap.put("08291","四川");

		quhaoHashMap.put("08292","四川");

		quhaoHashMap.put("08293","四川");

		quhaoHashMap.put("08294","四川");

		quhaoHashMap.put("08297","四川");

		quhaoHashMap.put("0830","四川");

		quhaoHashMap.put("0831","四川");

		quhaoHashMap.put("0832","四川");

		quhaoHashMap.put("0833","四川");

		quhaoHashMap.put("0833","云南");

		quhaoHashMap.put("0834","四川");

		quhaoHashMap.put("0835","四川");

		quhaoHashMap.put("0836","四川");

		quhaoHashMap.put("0837","四川");

		quhaoHashMap.put("0838","四川");

		quhaoHashMap.put("0839","四川");

		quhaoHashMap.put("08403","四川");

		quhaoHashMap.put("08405","四川");

		quhaoHashMap.put("08406","四川");

		quhaoHashMap.put("08407","四川");

		quhaoHashMap.put("08408","四川");

		quhaoHashMap.put("08411","四川");

		quhaoHashMap.put("08412","四川");

		quhaoHashMap.put("08414","四川");

		quhaoHashMap.put("08415","四川");

		quhaoHashMap.put("08417","四川");

		quhaoHashMap.put("08418","四川");

		quhaoHashMap.put("08419","四川");

		quhaoHashMap.put("08420","四川");

		quhaoHashMap.put("08423","四川");

		quhaoHashMap.put("08430","四川");

		quhaoHashMap.put("08431","四川");

		quhaoHashMap.put("08434","四川");

		quhaoHashMap.put("08435","四川");

		quhaoHashMap.put("08436","四川");

		quhaoHashMap.put("08437","四川");

		quhaoHashMap.put("08438","四川");

		quhaoHashMap.put("08440","四川");

		quhaoHashMap.put("08444","四川");

		quhaoHashMap.put("08445","四川");

		quhaoHashMap.put("08446","四川");

		quhaoHashMap.put("08447","四川");

		quhaoHashMap.put("08453","四川");

		quhaoHashMap.put("08455","四川");

		quhaoHashMap.put("08456","四川");

		quhaoHashMap.put("08457","四川");

		quhaoHashMap.put("08458","四川");

		quhaoHashMap.put("08461","四川");

		quhaoHashMap.put("08462","四川");

		quhaoHashMap.put("08463","四川");

		quhaoHashMap.put("08465","四川");

		quhaoHashMap.put("08466","四川");

		quhaoHashMap.put("08489","四川");

		quhaoHashMap.put("08493","四川");

		quhaoHashMap.put("0851","贵州");

		quhaoHashMap.put("0852","贵州");

		quhaoHashMap.put("0853","贵州");

		quhaoHashMap.put("0854","贵州");

		quhaoHashMap.put("0855","贵州");

		quhaoHashMap.put("0856","贵州");

		quhaoHashMap.put("0857","贵州");

		quhaoHashMap.put("0858","贵州");

		quhaoHashMap.put("0859","贵州");

		quhaoHashMap.put("08610","贵州");

		quhaoHashMap.put("08619","贵州");

		quhaoHashMap.put("08620","贵州");

		quhaoHashMap.put("08621","贵州");

		quhaoHashMap.put("08622","贵州");

		quhaoHashMap.put("08623","贵州");

		quhaoHashMap.put("08624","贵州");

		quhaoHashMap.put("08625","贵州");

		quhaoHashMap.put("08626","贵州");

		quhaoHashMap.put("08627","贵州");

		quhaoHashMap.put("08628","贵州");

		quhaoHashMap.put("08629","贵州");

		quhaoHashMap.put("08631","贵州");

		quhaoHashMap.put("08632","贵州");

		quhaoHashMap.put("08633","贵州");

		quhaoHashMap.put("08634","贵州");

		quhaoHashMap.put("08635","贵州");

		quhaoHashMap.put("08640","贵州");

		quhaoHashMap.put("08641","贵州");

		quhaoHashMap.put("08642","贵州");

		quhaoHashMap.put("08643","贵州");

		quhaoHashMap.put("08644","贵州");

		quhaoHashMap.put("08645","贵州");

		quhaoHashMap.put("08646","贵州");

		quhaoHashMap.put("08647","贵州");

		quhaoHashMap.put("08648","贵州");

		quhaoHashMap.put("08649","贵州");

		quhaoHashMap.put("08650","贵州");

		quhaoHashMap.put("08651","贵州");

		quhaoHashMap.put("08652","贵州");

		quhaoHashMap.put("08653","贵州");

		quhaoHashMap.put("08654","贵州");

		quhaoHashMap.put("08655","贵州");

		quhaoHashMap.put("08656","贵州");

		quhaoHashMap.put("08657","贵州");

		quhaoHashMap.put("08658","贵州");

		quhaoHashMap.put("08659","贵州");

		quhaoHashMap.put("08662","贵州");

		quhaoHashMap.put("08663","贵州");

		quhaoHashMap.put("08664","贵州");

		quhaoHashMap.put("08665","贵州");

		quhaoHashMap.put("08666","贵州");

		quhaoHashMap.put("08667","贵州");

		quhaoHashMap.put("08669","贵州");

		quhaoHashMap.put("08670","贵州");

		quhaoHashMap.put("08671","贵州");

		quhaoHashMap.put("08672","贵州");

		quhaoHashMap.put("08673","贵州");

		quhaoHashMap.put("08674","贵州");

		quhaoHashMap.put("08675","贵州");

		quhaoHashMap.put("08676","贵州");

		quhaoHashMap.put("08677","贵州");

		quhaoHashMap.put("08680","贵州");

		quhaoHashMap.put("08681","贵州");

		quhaoHashMap.put("08682","贵州");

		quhaoHashMap.put("08686","贵州");

		quhaoHashMap.put("08687","贵州");

		quhaoHashMap.put("08688","贵州");

		quhaoHashMap.put("08689","贵州");

		quhaoHashMap.put("0870","云南");

		quhaoHashMap.put("0871","云南");

		quhaoHashMap.put("0872","云南");

		quhaoHashMap.put("0873","云南");

		quhaoHashMap.put("0874","云南");

		quhaoHashMap.put("0875","云南");

		quhaoHashMap.put("0876","云南");

		quhaoHashMap.put("0877","云南");

		quhaoHashMap.put("0878","云南");

		quhaoHashMap.put("0879","云南");

		quhaoHashMap.put("0881","云南");

		quhaoHashMap.put("0883","云南");

		quhaoHashMap.put("0886","云南");

		quhaoHashMap.put("0887","云南");

		quhaoHashMap.put("0888","云南");

		quhaoHashMap.put("0890","海南");

		quhaoHashMap.put("0891","西藏");

		quhaoHashMap.put("0892","西藏");

		quhaoHashMap.put("0893","西藏");

		quhaoHashMap.put("0894","西藏");

		quhaoHashMap.put("0895","西藏");

		quhaoHashMap.put("0898","海南");

		quhaoHashMap.put("0899","海南");

		quhaoHashMap.put("09003","新疆");

		quhaoHashMap.put("09009","新疆");

		quhaoHashMap.put("0902","新疆");

		quhaoHashMap.put("0903","新疆");

		quhaoHashMap.put("09081","新疆");

		quhaoHashMap.put("0909","新疆");

		quhaoHashMap.put("0910","陕西");

		quhaoHashMap.put("0911","陕西");

		quhaoHashMap.put("0912","陕西");

		quhaoHashMap.put("0913","陕西");

		quhaoHashMap.put("0914","陕西");

		quhaoHashMap.put("0915","陕西");

		quhaoHashMap.put("0916","陕西");

		quhaoHashMap.put("0917","陕西");

		quhaoHashMap.put("0919","陕西");

		quhaoHashMap.put("09220","陕西");

		quhaoHashMap.put("09221","陕西");

		quhaoHashMap.put("09222","陕西");

		quhaoHashMap.put("09223","陕西");

		quhaoHashMap.put("09224","陕西");

		quhaoHashMap.put("09225","陕西");

		quhaoHashMap.put("09227","陕西");

		quhaoHashMap.put("09228","陕西");

		quhaoHashMap.put("09229","陕西");

		quhaoHashMap.put("09240","陕西");

		quhaoHashMap.put("09242","陕西");

		quhaoHashMap.put("09243","陕西");

		quhaoHashMap.put("09244","陕西");

		quhaoHashMap.put("0931","甘肃");

		quhaoHashMap.put("0932","甘肃");

		quhaoHashMap.put("0933","甘肃");

		quhaoHashMap.put("0934","甘肃");

		quhaoHashMap.put("0935","甘肃");

		quhaoHashMap.put("0936","甘肃");

		quhaoHashMap.put("0937","甘肃");

		quhaoHashMap.put("0938","甘肃");

		quhaoHashMap.put("0939","甘肃");

		quhaoHashMap.put("09401","甘肃");

		quhaoHashMap.put("09402","甘肃");

		quhaoHashMap.put("09403","甘肃");

		quhaoHashMap.put("09404","甘肃");

		quhaoHashMap.put("09405","甘肃");

		quhaoHashMap.put("09406","甘肃");

		quhaoHashMap.put("09407","甘肃");

		quhaoHashMap.put("09411","甘肃");

		quhaoHashMap.put("09412","甘肃");

		quhaoHashMap.put("09413","甘肃");

		quhaoHashMap.put("09414","甘肃");

		quhaoHashMap.put("09415","甘肃");

		quhaoHashMap.put("09416","甘肃");

		quhaoHashMap.put("09417","甘肃");

		quhaoHashMap.put("09418","甘肃");

		quhaoHashMap.put("09421","甘肃");

		quhaoHashMap.put("09422","甘肃");

		quhaoHashMap.put("09423","甘肃");

		quhaoHashMap.put("09424","甘肃");

		quhaoHashMap.put("09425","甘肃");

		quhaoHashMap.put("09426","甘肃");

		quhaoHashMap.put("0943","甘肃");

		quhaoHashMap.put("09431","甘肃");

		quhaoHashMap.put("09432","甘肃");

		quhaoHashMap.put("09433","甘肃");

		quhaoHashMap.put("09434","甘肃");

		quhaoHashMap.put("09435","甘肃");

		quhaoHashMap.put("09436","甘肃");

		quhaoHashMap.put("09441","甘肃");

		quhaoHashMap.put("09442","甘肃");

		quhaoHashMap.put("09443","甘肃");

		quhaoHashMap.put("09444","甘肃");

		quhaoHashMap.put("09445","甘肃");

		quhaoHashMap.put("09446","甘肃");

		quhaoHashMap.put("09447","甘肃");

		quhaoHashMap.put("09463","甘肃");

		quhaoHashMap.put("09466","甘肃");

		quhaoHashMap.put("09467","甘肃");

		quhaoHashMap.put("09468","甘肃");

		quhaoHashMap.put("09469","甘肃");

		quhaoHashMap.put("09475","甘肃");

		quhaoHashMap.put("09482","甘肃");

		quhaoHashMap.put("09483","甘肃");

		quhaoHashMap.put("09484","甘肃");

		quhaoHashMap.put("09491","甘肃");

		quhaoHashMap.put("09492","甘肃");

		quhaoHashMap.put("09493","甘肃");

		quhaoHashMap.put("09494","甘肃");

		quhaoHashMap.put("09495","甘肃");

		quhaoHashMap.put("09496","甘肃");

		quhaoHashMap.put("09497","甘肃");

		quhaoHashMap.put("09498","甘肃");

		quhaoHashMap.put("0951","宁夏");

		quhaoHashMap.put("0952","宁夏");

		quhaoHashMap.put("0953","宁夏");

		quhaoHashMap.put("0954","宁夏");

		quhaoHashMap.put("0970","青海");

		quhaoHashMap.put("0971","青海");

		quhaoHashMap.put("0972","青海");

		quhaoHashMap.put("0973","青海");

		quhaoHashMap.put("0974","青海");

		quhaoHashMap.put("0975","青海");

		quhaoHashMap.put("0976","青海");

		quhaoHashMap.put("0977","青海");

		quhaoHashMap.put("0978","青海");

		quhaoHashMap.put("0979","青海");

		quhaoHashMap.put("09820","青海");

		quhaoHashMap.put("09828","青海");

		quhaoHashMap.put("09829","青海");

		quhaoHashMap.put("09831","青海");

		quhaoHashMap.put("09832","青海");

		quhaoHashMap.put("09833","青海");

		quhaoHashMap.put("09839","青海");

		quhaoHashMap.put("09840","青海");

		quhaoHashMap.put("09841","青海");

		quhaoHashMap.put("09842","青海");

		quhaoHashMap.put("09844","青海");

		quhaoHashMap.put("09846","青海");

		quhaoHashMap.put("09847","青海");

		quhaoHashMap.put("09848","青海");

		quhaoHashMap.put("09849","青海");

		quhaoHashMap.put("0990","新疆");

		quhaoHashMap.put("0991","新疆");

		quhaoHashMap.put("0992","新疆");

		quhaoHashMap.put("0993","新疆");

		quhaoHashMap.put("0994","新疆");

		quhaoHashMap.put("0995","新疆");

		quhaoHashMap.put("0996","新疆");

		quhaoHashMap.put("0997","新疆");

		quhaoHashMap.put("0998","新疆");

		quhaoHashMap.put("0999","新疆");
	}
	
	/**
	 * 设置邮编
	 * @author 刘虻
	 * 2007-2-12下午04:40:51
	 */
	protected void putYouBian() {
		
		youBianHashMap.put("00","福建");

		youBianHashMap.put("00","湖北");

		youBianHashMap.put("00","江苏");

		youBianHashMap.put("010000","内蒙古");

		youBianHashMap.put("010100","内蒙古");

		youBianHashMap.put("010200","内蒙古");

		youBianHashMap.put("011500","内蒙古");

		youBianHashMap.put("011600","内蒙古");

		youBianHashMap.put("011700","内蒙古");

		youBianHashMap.put("011800","内蒙古");

		youBianHashMap.put("011900","内蒙古");

		youBianHashMap.put("012000","内蒙古");

		youBianHashMap.put("012100","内蒙古");

		youBianHashMap.put("012200","内蒙古");

		youBianHashMap.put("012300","内蒙古");

		youBianHashMap.put("012500","内蒙古");

		youBianHashMap.put("012600","内蒙古");

		youBianHashMap.put("013150","内蒙古");

		youBianHashMap.put("013250","内蒙古");

		youBianHashMap.put("013350","内蒙古");

		youBianHashMap.put("013400","内蒙古");

		youBianHashMap.put("013650","内蒙古");

		youBianHashMap.put("013750","内蒙古");

		youBianHashMap.put("014000","内蒙古");

		youBianHashMap.put("014100","内蒙古");

		youBianHashMap.put("014200","内蒙古");

		youBianHashMap.put("014300","内蒙古");

		youBianHashMap.put("014400","内蒙古");

		youBianHashMap.put("015000","内蒙古");

		youBianHashMap.put("015100","内蒙古");

		youBianHashMap.put("015200","内蒙古");

		youBianHashMap.put("015300","内蒙古");

		youBianHashMap.put("015400","内蒙古");

		youBianHashMap.put("015500","内蒙古");

		youBianHashMap.put("016000","内蒙古");

		youBianHashMap.put("016100","内蒙古");

		youBianHashMap.put("016200","内蒙古");

		youBianHashMap.put("017000","内蒙古");

		youBianHashMap.put("017100","内蒙古");

		youBianHashMap.put("017200","内蒙古");

		youBianHashMap.put("017300","内蒙古");

		youBianHashMap.put("017400","内蒙古");

		youBianHashMap.put("021000","内蒙古");

		youBianHashMap.put("021100","内蒙古");

		youBianHashMap.put("021200","内蒙古");

		youBianHashMap.put("021400","内蒙古");

		youBianHashMap.put("021500","内蒙古");

		youBianHashMap.put("022150","内蒙古");

		youBianHashMap.put("022250","内蒙古");

		youBianHashMap.put("022450","内蒙古");

		youBianHashMap.put("024000","内蒙古");

		youBianHashMap.put("024200","内蒙古");

		youBianHashMap.put("024300","内蒙古");

		youBianHashMap.put("024400","内蒙古");

		youBianHashMap.put("024500","内蒙古");

		youBianHashMap.put("025100","内蒙古");

		youBianHashMap.put("025150","内蒙古");

		youBianHashMap.put("025250","内蒙古");

		youBianHashMap.put("025350","内蒙古");

		youBianHashMap.put("025550","内蒙古");

		youBianHashMap.put("026000","内蒙古");

		youBianHashMap.put("026100","内蒙古");

		youBianHashMap.put("026200","内蒙古");

		youBianHashMap.put("026300","内蒙古");

		youBianHashMap.put("027000","内蒙古");

		youBianHashMap.put("027100","内蒙古");

		youBianHashMap.put("027200","内蒙古");

		youBianHashMap.put("027300","内蒙古");

		youBianHashMap.put("028000","内蒙古");

		youBianHashMap.put("028100","内蒙古");

		youBianHashMap.put("028200","内蒙古");

		youBianHashMap.put("028300","内蒙古");

		youBianHashMap.put("028400","内蒙古");

		youBianHashMap.put("029100","内蒙古");

		youBianHashMap.put("029200","内蒙古");

		youBianHashMap.put("029400","内蒙古");

		youBianHashMap.put("030200","山西");

		youBianHashMap.put("030300","山西");

		youBianHashMap.put("030400","山西");

		youBianHashMap.put("030500","山西");

		youBianHashMap.put("030600","山西");

		youBianHashMap.put("030800","山西");

		youBianHashMap.put("030900","山西");

		youBianHashMap.put("03100","山西");

		youBianHashMap.put("031100","山西");

		youBianHashMap.put("031200","山西");

		youBianHashMap.put("031300","山西");

		youBianHashMap.put("031400","山西");

		youBianHashMap.put("031500","山西");

		youBianHashMap.put("031600","山西");

		youBianHashMap.put("031700","山西");

		youBianHashMap.put("031800","山西");

		youBianHashMap.put("032100","山西");

		youBianHashMap.put("032200","山西");

		youBianHashMap.put("032300","山西");

		youBianHashMap.put("032400","山西");

		youBianHashMap.put("032500","山西");

		youBianHashMap.put("032600","山西");

		youBianHashMap.put("032700","山西");

		youBianHashMap.put("033000","山西");

		youBianHashMap.put("033100","山西");

		youBianHashMap.put("033200","山西");

		youBianHashMap.put("033300","山西");

		youBianHashMap.put("033400","山西");

		youBianHashMap.put("034000","山西");

		youBianHashMap.put("034100","山西");

		youBianHashMap.put("034200","山西");

		youBianHashMap.put("034300","山西");

		youBianHashMap.put("034400","山西");

		youBianHashMap.put("035100","山西");

		youBianHashMap.put("035200","山西");

		youBianHashMap.put("035300","山西");

		youBianHashMap.put("035400","山西");

		youBianHashMap.put("035500","山西");

		youBianHashMap.put("036000","山西");

		youBianHashMap.put("036100","山西");

		youBianHashMap.put("036200","山西");

		youBianHashMap.put("036300","山西");

		youBianHashMap.put("036400","山西");

		youBianHashMap.put("036500","山西");

		youBianHashMap.put("036600","山西");

		youBianHashMap.put("037000","山西");

		youBianHashMap.put("037100","山西");

		youBianHashMap.put("037200","山西");

		youBianHashMap.put("037400","山西");

		youBianHashMap.put("037500","山西");

		youBianHashMap.put("037600","山西");

		youBianHashMap.put("038100","山西");

		youBianHashMap.put("038200","山西");

		youBianHashMap.put("038300","山西");

		youBianHashMap.put("038400","山西");

		youBianHashMap.put("038500","山西");

		youBianHashMap.put("041000","山西");

		youBianHashMap.put("041200","山西");

		youBianHashMap.put("041300","山西");

		youBianHashMap.put("041400","山西");

		youBianHashMap.put("041500","山西");

		youBianHashMap.put("042100","山西");

		youBianHashMap.put("042200","山西");

		youBianHashMap.put("042300","山西");

		youBianHashMap.put("042400","山西");

		youBianHashMap.put("042500","山西");

		youBianHashMap.put("042600","山西");

		youBianHashMap.put("043000","山西");

		youBianHashMap.put("043100","山西");

		youBianHashMap.put("043200","山西");

		youBianHashMap.put("043300","山西");

		youBianHashMap.put("043400","山西");

		youBianHashMap.put("043500","山西");

		youBianHashMap.put("043600","山西");

		youBianHashMap.put("043700","山西");

		youBianHashMap.put("043800","山西");

		youBianHashMap.put("044000","山西");

		youBianHashMap.put("044100","山西");

		youBianHashMap.put("044200","山西");

		youBianHashMap.put("044300","山西");

		youBianHashMap.put("044400","山西");

		youBianHashMap.put("044500","山西");

		youBianHashMap.put("044600","山西");

		youBianHashMap.put("045000","山西");

		youBianHashMap.put("045100","山西");

		youBianHashMap.put("045200","山西");

		youBianHashMap.put("045300","山西");

		youBianHashMap.put("046000","山西");

		youBianHashMap.put("046100","山西");

		youBianHashMap.put("046200","山西");

		youBianHashMap.put("046300","山西");

		youBianHashMap.put("046400","山西");

		youBianHashMap.put("046500","山西");

		youBianHashMap.put("046600","山西");

		youBianHashMap.put("046700","山西");

		youBianHashMap.put("047300","山西");

		youBianHashMap.put("047400","山西");

		youBianHashMap.put("047500","山西");

		youBianHashMap.put("047600","山西");

		youBianHashMap.put("048000","山西");

		youBianHashMap.put("048100","山西");

		youBianHashMap.put("048200","山西");

		youBianHashMap.put("048300","山西");

		youBianHashMap.put("050000","河北");

		youBianHashMap.put("050300","河北");

		youBianHashMap.put("050400","河北");

		youBianHashMap.put("050500","河北");

		youBianHashMap.put("050600","河北");

		youBianHashMap.put("050700","河北");

		youBianHashMap.put("050800","河北");

		youBianHashMap.put("051100","河北");

		youBianHashMap.put("051230","河北");

		youBianHashMap.put("051330","河北");

		youBianHashMap.put("051500","河北");

		youBianHashMap.put("051630","河北");

		youBianHashMap.put("051730","河北");

		youBianHashMap.put("051800","河北");

		youBianHashMap.put("052100","河北");

		youBianHashMap.put("052200","河北");

		youBianHashMap.put("052260","河北");

		youBianHashMap.put("052300","河北");

		youBianHashMap.put("052360","河北");

		youBianHashMap.put("052400","河北");

		youBianHashMap.put("052460","河北");

		youBianHashMap.put("052560","河北");

		youBianHashMap.put("052600","河北");

		youBianHashMap.put("052760","河北");

		youBianHashMap.put("052800","河北");

		youBianHashMap.put("052860","河北");

		youBianHashMap.put("053000","河北");

		youBianHashMap.put("053100","河北");

		youBianHashMap.put("053200","河北");

		youBianHashMap.put("053300","河北");

		youBianHashMap.put("053400","河北");

		youBianHashMap.put("053500","河北");

		youBianHashMap.put("053700","河北");

		youBianHashMap.put("054000","河北");

		youBianHashMap.put("054100","河北");

		youBianHashMap.put("054200","河北");

		youBianHashMap.put("054300","河北");

		youBianHashMap.put("054400","河北");

		youBianHashMap.put("054500","河北");

		youBianHashMap.put("054600","河北");

		youBianHashMap.put("054700","河北");

		youBianHashMap.put("054800","河北");

		youBianHashMap.put("054900","河北");

		youBianHashMap.put("055150","河北");

		youBianHashMap.put("055200","河北");

		youBianHashMap.put("055300","河北");

		youBianHashMap.put("055450","河北");

		youBianHashMap.put("056000","河北");

		youBianHashMap.put("056300","河北");

		youBianHashMap.put("056400","河北");

		youBianHashMap.put("056500","河北");

		youBianHashMap.put("056600","河北");

		youBianHashMap.put("056700","河北");

		youBianHashMap.put("056800","河北");

		youBianHashMap.put("056900","河北");

		youBianHashMap.put("057150","河北");

		youBianHashMap.put("057250","河北");

		youBianHashMap.put("057350","河北");

		youBianHashMap.put("057450","河北");

		youBianHashMap.put("057550","河北");

		youBianHashMap.put("057650","河北");

		youBianHashMap.put("057750","河北");

		youBianHashMap.put("061000","河北");

		youBianHashMap.put("061100","河北");

		youBianHashMap.put("061200","河北");

		youBianHashMap.put("061300","河北");

		youBianHashMap.put("061400","河北");

		youBianHashMap.put("061500","河北");

		youBianHashMap.put("061600","河北");

		youBianHashMap.put("061800","河北");

		youBianHashMap.put("062100","河北");

		youBianHashMap.put("062100","云南");

		youBianHashMap.put("062250","河北");

		youBianHashMap.put("062300","河北");

		youBianHashMap.put("062450","河北");

		youBianHashMap.put("062500","河北");

		youBianHashMap.put("062650","河北");

		youBianHashMap.put("063000","河北");

		youBianHashMap.put("063200","河北");

		youBianHashMap.put("063300","河北");

		youBianHashMap.put("063500","河北");

		youBianHashMap.put("063600","河北");

		youBianHashMap.put("063700","河北");

		youBianHashMap.put("064000","河北");

		youBianHashMap.put("064100","河北");

		youBianHashMap.put("064200","河北");

		youBianHashMap.put("064300","河北");

		youBianHashMap.put("064400","河北");

		youBianHashMap.put("066000","河北");

		youBianHashMap.put("066300","河北");

		youBianHashMap.put("066400","河北");

		youBianHashMap.put("066500","河北");

		youBianHashMap.put("066600","河北");

		youBianHashMap.put("067000","河北");

		youBianHashMap.put("067300","河北");

		youBianHashMap.put("067400","河北");

		youBianHashMap.put("067500","河北");

		youBianHashMap.put("067600","河北");

		youBianHashMap.put("068100","河北");

		youBianHashMap.put("068200","河北");

		youBianHashMap.put("068300","河北");

		youBianHashMap.put("068400","河北");

		youBianHashMap.put("071000","河北");

		youBianHashMap.put("071100","河北");

		youBianHashMap.put("071200","河北");

		youBianHashMap.put("071300","河北");

		youBianHashMap.put("071400","河北");

		youBianHashMap.put("071500","河北");

		youBianHashMap.put("071600","河北");

		youBianHashMap.put("071700","河北");

		youBianHashMap.put("071800","河北");

		youBianHashMap.put("072150","河北");

		youBianHashMap.put("072350","河北");

		youBianHashMap.put("072450","河北");

		youBianHashMap.put("072550","河北");

		youBianHashMap.put("072600","河北");

		youBianHashMap.put("072700","河北");

		youBianHashMap.put("073000","河北");

		youBianHashMap.put("073100","河北");

		youBianHashMap.put("073200","河北");

		youBianHashMap.put("074000","河北");

		youBianHashMap.put("074100","河北");

		youBianHashMap.put("074200","河北");

		youBianHashMap.put("075000","河北");

		youBianHashMap.put("075100","河北");

		youBianHashMap.put("075400","河北");

		youBianHashMap.put("075500","河北");

		youBianHashMap.put("075600","河北");

		youBianHashMap.put("075700","河北");

		youBianHashMap.put("075800","河北");

		youBianHashMap.put("076150","河北");

		youBianHashMap.put("076250","河北");

		youBianHashMap.put("076350","河北");

		youBianHashMap.put("076450","河北");

		youBianHashMap.put("076550","河北");

		youBianHashMap.put("076650","河北");

		youBianHashMap.put("076750","河北");

		youBianHashMap.put("100000","北京");

		youBianHashMap.put("101100","北京");

		youBianHashMap.put("101200","北京");

		youBianHashMap.put("101300","北京");

		youBianHashMap.put("101400","北京");

		youBianHashMap.put("101500","北京");

		youBianHashMap.put("101600","河北");

		youBianHashMap.put("101700","河北");

		youBianHashMap.put("102100","北京");

		youBianHashMap.put("102200","北京");

		youBianHashMap.put("102300","北京");

		youBianHashMap.put("102400","北京");

		youBianHashMap.put("102500","北京");

		youBianHashMap.put("102600","北京");

		youBianHashMap.put("102700","河北");

		youBianHashMap.put("102800","河北");

		youBianHashMap.put("102900","河北");

		youBianHashMap.put("110000","辽宁");

		youBianHashMap.put("110200","辽宁");

		youBianHashMap.put("110300","辽宁");

		youBianHashMap.put("111000","辽宁");

		youBianHashMap.put("111200","辽宁");

		youBianHashMap.put("111300","辽宁");

		youBianHashMap.put("112000","辽宁");

		youBianHashMap.put("112300","辽宁");

		youBianHashMap.put("112400","辽宁");

		youBianHashMap.put("112500","辽宁");

		youBianHashMap.put("113000","辽宁");

		youBianHashMap.put("113200","辽宁");

		youBianHashMap.put("113300","辽宁");

		youBianHashMap.put("114000","辽宁");

		youBianHashMap.put("114100","辽宁");

		youBianHashMap.put("114200","辽宁");

		youBianHashMap.put("115100","辽宁");

		youBianHashMap.put("115200","辽宁");

		youBianHashMap.put("116000","辽宁");

		youBianHashMap.put("116100","辽宁");

		youBianHashMap.put("116300","辽宁");

		youBianHashMap.put("116400","辽宁");

		youBianHashMap.put("116500","辽宁");

		youBianHashMap.put("117000","辽宁");

		youBianHashMap.put("117200","辽宁");

		youBianHashMap.put("118000","辽宁");

		youBianHashMap.put("118100","辽宁");

		youBianHashMap.put("118200","辽宁");

		youBianHashMap.put("118300","辽宁");

		youBianHashMap.put("118400","辽宁");

		youBianHashMap.put("121000","辽宁");

		youBianHashMap.put("121100","辽宁");

		youBianHashMap.put("121200","辽宁");

		youBianHashMap.put("121300","辽宁");

		youBianHashMap.put("121400","辽宁");

		youBianHashMap.put("121500","辽宁");

		youBianHashMap.put("121600","辽宁");

		youBianHashMap.put("121700","辽宁");

		youBianHashMap.put("122000","辽宁");

		youBianHashMap.put("122100","辽宁");

		youBianHashMap.put("122200","辽宁");

		youBianHashMap.put("122300","辽宁");

		youBianHashMap.put("122400","辽宁");

		youBianHashMap.put("122500","辽宁");

		youBianHashMap.put("123000","辽宁");

		youBianHashMap.put("123100","辽宁");

		youBianHashMap.put("123200","辽宁");

		youBianHashMap.put("124000","辽宁");

		youBianHashMap.put("124200","辽宁");

		youBianHashMap.put("130000","吉林");

		youBianHashMap.put("130200","吉林");

		youBianHashMap.put("130300","吉林");

		youBianHashMap.put("130400","吉林");

		youBianHashMap.put("130500","吉林");

		youBianHashMap.put("130600","吉林");

		youBianHashMap.put("130700","吉林");

		youBianHashMap.put("131200","吉林");

		youBianHashMap.put("131300","吉林");

		youBianHashMap.put("131400","吉林");

		youBianHashMap.put("131500","吉林");

		youBianHashMap.put("132000","吉林");

		youBianHashMap.put("132200","吉林");

		youBianHashMap.put("132300","吉林");

		youBianHashMap.put("132400","吉林");

		youBianHashMap.put("132500","吉林");

		youBianHashMap.put("132600","吉林");

		youBianHashMap.put("133000","吉林");

		youBianHashMap.put("133100","吉林");

		youBianHashMap.put("133200","吉林");

		youBianHashMap.put("133300","吉林");

		youBianHashMap.put("133400","吉林");

		youBianHashMap.put("133500","吉林");

		youBianHashMap.put("133600","吉林");

		youBianHashMap.put("133700","吉林");

		youBianHashMap.put("134000","吉林");

		youBianHashMap.put("134100","吉林");

		youBianHashMap.put("134200","吉林");

		youBianHashMap.put("134300","吉林");

		youBianHashMap.put("134400","吉林");

		youBianHashMap.put("134500","吉林");

		youBianHashMap.put("134600","吉林");

		youBianHashMap.put("135000","吉林");

		youBianHashMap.put("135100","吉林");

		youBianHashMap.put("135200","吉林");

		youBianHashMap.put("135300","吉林");

		youBianHashMap.put("136000","吉林");

		youBianHashMap.put("136100","吉林");

		youBianHashMap.put("136200","吉林");

		youBianHashMap.put("136300","吉林");

		youBianHashMap.put("136400","吉林");

		youBianHashMap.put("136500","吉林");

		youBianHashMap.put("137000","吉林");

		youBianHashMap.put("137100","吉林");

		youBianHashMap.put("137200","吉林");

		youBianHashMap.put("137300","吉林");

		youBianHashMap.put("137400","内蒙古");

		youBianHashMap.put("137500","内蒙古");

		youBianHashMap.put("137600","内蒙古");

		youBianHashMap.put("141100","吉林");

		youBianHashMap.put("150000","黑龙江");

		youBianHashMap.put("150100","黑龙江");

		youBianHashMap.put("150200","黑龙江");

		youBianHashMap.put("150300","黑龙江");

		youBianHashMap.put("150400","黑龙江");

		youBianHashMap.put("150500","黑龙江");

		youBianHashMap.put("150600","黑龙江");

		youBianHashMap.put("150700","黑龙江");

		youBianHashMap.put("150800","黑龙江");

		youBianHashMap.put("150900","黑龙江");

		youBianHashMap.put("151100","黑龙江");

		youBianHashMap.put("151300","黑龙江");

		youBianHashMap.put("151400","黑龙江");

		youBianHashMap.put("151500","黑龙江");

		youBianHashMap.put("151600","黑龙江");

		youBianHashMap.put("151700","黑龙江");

		youBianHashMap.put("151800","黑龙江");

		youBianHashMap.put("151900","黑龙江");

		youBianHashMap.put("152000","黑龙江");

		youBianHashMap.put("152100","黑龙江");

		youBianHashMap.put("152200","黑龙江");

		youBianHashMap.put("152300","黑龙江");

		youBianHashMap.put("152400","黑龙江");

		youBianHashMap.put("152500","黑龙江");

		youBianHashMap.put("153000","黑龙江");

		youBianHashMap.put("153200","黑龙江");

		youBianHashMap.put("154000","黑龙江");

		youBianHashMap.put("154100","黑龙江");

		youBianHashMap.put("154200","黑龙江");

		youBianHashMap.put("154300","黑龙江");

		youBianHashMap.put("154400","黑龙江");

		youBianHashMap.put("154500","黑龙江");

		youBianHashMap.put("154600","黑龙江");

		youBianHashMap.put("154700","黑龙江");

		youBianHashMap.put("154800","黑龙江");

		youBianHashMap.put("154900","黑龙江");

		youBianHashMap.put("155100","黑龙江");

		youBianHashMap.put("156100","黑龙江");

		youBianHashMap.put("156200","黑龙江");

		youBianHashMap.put("156400","黑龙江");

		youBianHashMap.put("156500","黑龙江");

		youBianHashMap.put("156600","黑龙江");

		youBianHashMap.put("156700","黑龙江");

		youBianHashMap.put("156900","黑龙江");

		youBianHashMap.put("157000","黑龙江");

		youBianHashMap.put("157100","黑龙江");

		youBianHashMap.put("157200","黑龙江");

		youBianHashMap.put("157300","黑龙江");

		youBianHashMap.put("157400","黑龙江");

		youBianHashMap.put("157500","黑龙江");

		youBianHashMap.put("157600","黑龙江");

		youBianHashMap.put("158100","黑龙江");

		youBianHashMap.put("158200","黑龙江");

		youBianHashMap.put("158300","黑龙江");

		youBianHashMap.put("158400","黑龙江");

		youBianHashMap.put("161000","黑龙江");

		youBianHashMap.put("161100","黑龙江");

		youBianHashMap.put("161200","黑龙江");

		youBianHashMap.put("161300","黑龙江");

		youBianHashMap.put("161400","黑龙江");

		youBianHashMap.put("161500","黑龙江");

		youBianHashMap.put("161600","黑龙江");

		youBianHashMap.put("161700","黑龙江");

		youBianHashMap.put("161800","黑龙江");

		youBianHashMap.put("162100","黑龙江");

		youBianHashMap.put("162200","黑龙江");

		youBianHashMap.put("162400","黑龙江");

		youBianHashMap.put("162650","内蒙古");

		youBianHashMap.put("162750","内蒙古");

		youBianHashMap.put("162850","内蒙古");

		youBianHashMap.put("163000","黑龙江");

		youBianHashMap.put("163300","黑龙江");

		youBianHashMap.put("163500","黑龙江");

		youBianHashMap.put("164000","黑龙江");

		youBianHashMap.put("164100","黑龙江");

		youBianHashMap.put("164200","黑龙江");

		youBianHashMap.put("164300","黑龙江");

		youBianHashMap.put("164400","黑龙江");

		youBianHashMap.put("164500","黑龙江");

		youBianHashMap.put("165000","黑龙江");

		youBianHashMap.put("165100","黑龙江");

		youBianHashMap.put("165200","黑龙江");

		youBianHashMap.put("165300","黑龙江");

		youBianHashMap.put("200000","上海");

		youBianHashMap.put("200100","上海");

		youBianHashMap.put("201100","上海");

		youBianHashMap.put("201200","上海");

		youBianHashMap.put("201300","上海");

		youBianHashMap.put("201400","上海");

		youBianHashMap.put("201500","上海");

		youBianHashMap.put("201600","上海");

		youBianHashMap.put("201700","上海");

		youBianHashMap.put("201800","上海");

		youBianHashMap.put("201900","上海");

		youBianHashMap.put("202100","上海");

		youBianHashMap.put("202450","浙江");

		youBianHashMap.put("210000","江苏");

		youBianHashMap.put("212000","江苏");

		youBianHashMap.put("212300","江苏");

		youBianHashMap.put("213000","江苏");

		youBianHashMap.put("213300","江苏");

		youBianHashMap.put("214000","江苏");

		youBianHashMap.put("214200","江苏");

		youBianHashMap.put("214400","江苏");

		youBianHashMap.put("215000","江苏");

		youBianHashMap.put("215200","江苏");

		youBianHashMap.put("215200","浙江");

		youBianHashMap.put("215300","江苏");

		youBianHashMap.put("215400","江苏");

		youBianHashMap.put("215500","江苏");

		youBianHashMap.put("215600","江苏");

		youBianHashMap.put("221000","江苏");

		youBianHashMap.put("221300","江苏");

		youBianHashMap.put("221400","江苏");

		youBianHashMap.put("222000","江苏");

		youBianHashMap.put("223000","江苏");

		youBianHashMap.put("223200","江苏");

		youBianHashMap.put("223800","江苏");

		youBianHashMap.put("224000","江苏");

		youBianHashMap.put("224200","江苏");

		youBianHashMap.put("224300","江苏");

		youBianHashMap.put("224400","江苏");

		youBianHashMap.put("225000","江苏");

		youBianHashMap.put("225200","江苏");

		youBianHashMap.put("225300","江苏");

		youBianHashMap.put("225400","江苏");

		youBianHashMap.put("225500","江苏");

		youBianHashMap.put("225600","江苏");

		youBianHashMap.put("225700","江苏");

		youBianHashMap.put("226000","江苏");

		youBianHashMap.put("226001","江苏");

		youBianHashMap.put("226100","江苏");

		youBianHashMap.put("226200","江苏");

		youBianHashMap.put("226300","江苏");

		youBianHashMap.put("226400","江苏");

		youBianHashMap.put("226500","江苏");

		youBianHashMap.put("226600","江苏");

		youBianHashMap.put("230000","安徽");

		youBianHashMap.put("231100","安徽");

		youBianHashMap.put("231200","安徽");

		youBianHashMap.put("231300","安徽");

		youBianHashMap.put("231400","安徽");

		youBianHashMap.put("231500","安徽");

		youBianHashMap.put("231600","安徽");

		youBianHashMap.put("232000","安徽");

		youBianHashMap.put("232100","安徽");

		youBianHashMap.put("232200","安徽");

		youBianHashMap.put("233000","安徽");

		youBianHashMap.put("233100","安徽");

		youBianHashMap.put("233200","安徽");

		youBianHashMap.put("233300","安徽");

		youBianHashMap.put("233400","安徽");

		youBianHashMap.put("233500","安徽");

		youBianHashMap.put("233600","安徽");

		youBianHashMap.put("233700","安徽");

		youBianHashMap.put("234000","安徽");

		youBianHashMap.put("234100","安徽");

		youBianHashMap.put("234200","安徽");

		youBianHashMap.put("234300","安徽");

		youBianHashMap.put("235000","安徽");

		youBianHashMap.put("235100","安徽");

		youBianHashMap.put("235200","安徽");

		youBianHashMap.put("235300","安徽");

		youBianHashMap.put("236000","安徽");

		youBianHashMap.put("236200","安徽");

		youBianHashMap.put("236300","安徽");

		youBianHashMap.put("236400","安徽");

		youBianHashMap.put("236500","安徽");

		youBianHashMap.put("236600","安徽");

		youBianHashMap.put("236700","安徽");

		youBianHashMap.put("236800","安徽");

		youBianHashMap.put("237000","安徽");

		youBianHashMap.put("237200","安徽");

		youBianHashMap.put("237300","安徽");

		youBianHashMap.put("237400","安徽");

		youBianHashMap.put("238000","安徽");

		youBianHashMap.put("238100","安徽");

		youBianHashMap.put("238200","安徽");

		youBianHashMap.put("238300","安徽");

		youBianHashMap.put("239200","安徽");

		youBianHashMap.put("239300","安徽");

		youBianHashMap.put("239400","安徽");

		youBianHashMap.put("239500","安徽");

		youBianHashMap.put("241000","安徽");

		youBianHashMap.put("241100","安徽");

		youBianHashMap.put("241200","安徽");

		youBianHashMap.put("242000","安徽");

		youBianHashMap.put("242100","安徽");

		youBianHashMap.put("242200","安徽");

		youBianHashMap.put("242300","安徽");

		youBianHashMap.put("242400","安徽");

		youBianHashMap.put("242500","安徽");

		youBianHashMap.put("242600","安徽");

		youBianHashMap.put("242700","安徽");

		youBianHashMap.put("242800","安徽");

		youBianHashMap.put("243000","安徽");

		youBianHashMap.put("243100","安徽");

		youBianHashMap.put("244000","安徽");

		youBianHashMap.put("244100","安徽");

		youBianHashMap.put("245100","安徽");

		youBianHashMap.put("245200","安徽");

		youBianHashMap.put("245300","安徽");

		youBianHashMap.put("245400","安徽");

		youBianHashMap.put("245500","安徽");

		youBianHashMap.put("245600","安徽");

		youBianHashMap.put("246000","安徽");

		youBianHashMap.put("246100","安徽");

		youBianHashMap.put("246200","安徽");

		youBianHashMap.put("246300","安徽");

		youBianHashMap.put("246400","安徽");

		youBianHashMap.put("246500","安徽");

		youBianHashMap.put("246600","安徽");

		youBianHashMap.put("246700","安徽");

		youBianHashMap.put("247100","安徽");

		youBianHashMap.put("247200","安徽");

		youBianHashMap.put("250000","山东");

		youBianHashMap.put("250200","山东");

		youBianHashMap.put("250300","山东");

		youBianHashMap.put("250400","山东");

		youBianHashMap.put("251100","山东");

		youBianHashMap.put("251300","山东");

		youBianHashMap.put("251400","山东");

		youBianHashMap.put("251500","山东");

		youBianHashMap.put("251600","山东");

		youBianHashMap.put("251700","山东");

		youBianHashMap.put("251800","山东");

		youBianHashMap.put("251900","山东");

		youBianHashMap.put("252000","山东");

		youBianHashMap.put("252100","山东");

		youBianHashMap.put("252300","山东");

		youBianHashMap.put("252400","山东");

		youBianHashMap.put("252500","山东");

		youBianHashMap.put("252600","山东");

		youBianHashMap.put("253000","山东");

		youBianHashMap.put("253121","山东");

		youBianHashMap.put("253200","山东");

		youBianHashMap.put("253300","山东");

		youBianHashMap.put("253400","山东");

		youBianHashMap.put("253500","山东");

		youBianHashMap.put("253600","山东");

		youBianHashMap.put("253700","山东");

		youBianHashMap.put("253800","河北");

		youBianHashMap.put("255000","山东");

		youBianHashMap.put("255800","山东");

		youBianHashMap.put("256100","山东");

		youBianHashMap.put("256300","山东");

		youBianHashMap.put("256400","山东");

		youBianHashMap.put("256500","山东");

		youBianHashMap.put("256800","山东");

		youBianHashMap.put("257000","山东");

		youBianHashMap.put("257300","山东");

		youBianHashMap.put("257400","山东");

		youBianHashMap.put("257500","山东");

		youBianHashMap.put("261000","山东");

		youBianHashMap.put("261300","山东");

		youBianHashMap.put("261400","山东");

		youBianHashMap.put("261500","山东");

		youBianHashMap.put("262100","山东");

		youBianHashMap.put("262200","山东");

		youBianHashMap.put("262300","山东");

		youBianHashMap.put("262400","山东");

		youBianHashMap.put("262500","山东");

		youBianHashMap.put("262600","山东");

		youBianHashMap.put("262700","山东");

		youBianHashMap.put("262800","山东");

		youBianHashMap.put("264000","山东");

		youBianHashMap.put("264100","山东");

		youBianHashMap.put("264200","山东");

		youBianHashMap.put("264300","山东");

		youBianHashMap.put("264400","山东");

		youBianHashMap.put("264500","山东");

		youBianHashMap.put("265100","山东");

		youBianHashMap.put("265200","山东");

		youBianHashMap.put("265300","山东");

		youBianHashMap.put("265400","山东");

		youBianHashMap.put("265500","山东");

		youBianHashMap.put("265700","山东");

		youBianHashMap.put("266000","山东");

		youBianHashMap.put("266200","山东");

		youBianHashMap.put("266300","山东");

		youBianHashMap.put("266400","山东");

		youBianHashMap.put("266600","山东");

		youBianHashMap.put("271000","山东");

		youBianHashMap.put("271100","山东");

		youBianHashMap.put("271200","山东");

		youBianHashMap.put("271400","山东");

		youBianHashMap.put("271600","山东");

		youBianHashMap.put("272000","山东");

		youBianHashMap.put("272100","山东");

		youBianHashMap.put("272200","山东");

		youBianHashMap.put("272300","山东");

		youBianHashMap.put("272400","山东");

		youBianHashMap.put("272500","山东");

		youBianHashMap.put("273100","山东");

		youBianHashMap.put("273200","山东");

		youBianHashMap.put("273300","山东");

		youBianHashMap.put("273400","山东");

		youBianHashMap.put("273500","山东");

		youBianHashMap.put("273600","山东");

		youBianHashMap.put("273700","山东");

		youBianHashMap.put("274000","山东");

		youBianHashMap.put("274100","山东");

		youBianHashMap.put("274400","山东");

		youBianHashMap.put("274500","山东");

		youBianHashMap.put("274600","山东");

		youBianHashMap.put("274700","山东");

		youBianHashMap.put("274800","山东");

		youBianHashMap.put("274900","山东");

		youBianHashMap.put("276000","山东");

		youBianHashMap.put("276100","山东");

		youBianHashMap.put("276200","山东");

		youBianHashMap.put("276300","山东");

		youBianHashMap.put("276400","山东");

		youBianHashMap.put("276500","山东");

		youBianHashMap.put("276600","山东");

		youBianHashMap.put("276700","山东");

		youBianHashMap.put("276800","山东");

		youBianHashMap.put("277100","山东");

		youBianHashMap.put("277500","山东");

		youBianHashMap.put("277600","山东");

		youBianHashMap.put("277700","山东");

		youBianHashMap.put("300000","天津");

		youBianHashMap.put("300270","天津");

		youBianHashMap.put("300450","天津");

		youBianHashMap.put("300480","天津");

		youBianHashMap.put("301500","天津");

		youBianHashMap.put("301600","天津");

		youBianHashMap.put("301700","天津");

		youBianHashMap.put("301800","天津");

		youBianHashMap.put("302550","河北");

		youBianHashMap.put("302650","河北");

		youBianHashMap.put("302750","河北");

		youBianHashMap.put("302850","河北");

		youBianHashMap.put("302950","河北");

		youBianHashMap.put("310000","浙江");

		youBianHashMap.put("311100","浙江");

		youBianHashMap.put("311200","浙江");

		youBianHashMap.put("311300","浙江");

		youBianHashMap.put("311400","浙江");

		youBianHashMap.put("311500","浙江");

		youBianHashMap.put("311600","浙江");

		youBianHashMap.put("311700","浙江");

		youBianHashMap.put("311800","浙江");

		youBianHashMap.put("312000","浙江");

		youBianHashMap.put("312300","浙江");

		youBianHashMap.put("312400","浙江");

		youBianHashMap.put("312500","浙江");

		youBianHashMap.put("313000","浙江");

		youBianHashMap.put("313100","浙江");

		youBianHashMap.put("313200","浙江");

		youBianHashMap.put("313300","浙江");

		youBianHashMap.put("314000","浙江");

		youBianHashMap.put("314100","浙江");

		youBianHashMap.put("314300","浙江");

		youBianHashMap.put("314400","浙江");

		youBianHashMap.put("314500","浙江");

		youBianHashMap.put("315000","浙江");

		youBianHashMap.put("315100","浙江");

		youBianHashMap.put("315300","浙江");

		youBianHashMap.put("315400","浙江");

		youBianHashMap.put("315500","浙江");

		youBianHashMap.put("315600","浙江");

		youBianHashMap.put("315700","浙江");

		youBianHashMap.put("316000","浙江");

		youBianHashMap.put("316100","浙江");

		youBianHashMap.put("316200","浙江");

		youBianHashMap.put("317000","四川");

		youBianHashMap.put("317000","浙江");

		youBianHashMap.put("317100","浙江");

		youBianHashMap.put("317200","浙江");

		youBianHashMap.put("317300","浙江");

		youBianHashMap.put("317400","浙江");

		youBianHashMap.put("317500","浙江");

		youBianHashMap.put("317600","浙江");

		youBianHashMap.put("317700","浙江");

		youBianHashMap.put("321000","四川");

		youBianHashMap.put("321000","浙江");

		youBianHashMap.put("321100","浙江");

		youBianHashMap.put("321200","浙江");

		youBianHashMap.put("321300","浙江");

		youBianHashMap.put("321400","浙江");

		youBianHashMap.put("322000","浙江");

		youBianHashMap.put("322100","浙江");

		youBianHashMap.put("322300","浙江");

		youBianHashMap.put("323000","浙江");

		youBianHashMap.put("323300","浙江");

		youBianHashMap.put("323400","浙江");

		youBianHashMap.put("323500","浙江");

		youBianHashMap.put("323600","浙江");

		youBianHashMap.put("323700","浙江");

		youBianHashMap.put("323800","浙江");

		youBianHashMap.put("323900","浙江");

		youBianHashMap.put("324000","浙江");

		youBianHashMap.put("324100","浙江");

		youBianHashMap.put("324200","浙江");

		youBianHashMap.put("324300","浙江");

		youBianHashMap.put("324400","浙江");

		youBianHashMap.put("325000","浙江");

		youBianHashMap.put("325100","浙江");

		youBianHashMap.put("325200","浙江");

		youBianHashMap.put("325300","浙江");

		youBianHashMap.put("325400","浙江");

		youBianHashMap.put("325500","浙江");

		youBianHashMap.put("325600","浙江");

		youBianHashMap.put("325700","浙江");

		youBianHashMap.put("325800","浙江");

		youBianHashMap.put("330000","江西");

		youBianHashMap.put("330100","江西");

		youBianHashMap.put("330200","江西");

		youBianHashMap.put("330300","江西");

		youBianHashMap.put("330400","江西");

		youBianHashMap.put("330500","江西");

		youBianHashMap.put("330600","江西");

		youBianHashMap.put("330700","江西");

		youBianHashMap.put("330800","江西");

		youBianHashMap.put("331100","江西");

		youBianHashMap.put("331200","江西");

		youBianHashMap.put("331300","江西");

		youBianHashMap.put("331400","江西");

		youBianHashMap.put("331500","江西");

		youBianHashMap.put("331600","江西");

		youBianHashMap.put("331700","江西");

		youBianHashMap.put("331800","江西");

		youBianHashMap.put("332000","江西");

		youBianHashMap.put("332100","江西");

		youBianHashMap.put("332200","江西");

		youBianHashMap.put("332300","江西");

		youBianHashMap.put("332400","江西");

		youBianHashMap.put("332500","江西");

		youBianHashMap.put("332600","江西");

		youBianHashMap.put("332700","江西");

		youBianHashMap.put("332800","江西");

		youBianHashMap.put("332900","江西");

		youBianHashMap.put("333000","江西");

		youBianHashMap.put("333100","江西");

		youBianHashMap.put("333200","江西");

		youBianHashMap.put("333300","江西");

		youBianHashMap.put("333400","江西");

		youBianHashMap.put("334000","江西");

		youBianHashMap.put("334100","江西");

		youBianHashMap.put("334200","江西");

		youBianHashMap.put("334300","江西");

		youBianHashMap.put("334400","江西");

		youBianHashMap.put("334406","江西");

		youBianHashMap.put("334500","江西");

		youBianHashMap.put("334600","江西");

		youBianHashMap.put("334700","江西");

		youBianHashMap.put("335000","江西");

		youBianHashMap.put("335100","江西");

		youBianHashMap.put("335200","江西");

		youBianHashMap.put("335300","江西");

		youBianHashMap.put("335400","江西");

		youBianHashMap.put("335500","江西");

		youBianHashMap.put("336000","江西");

		youBianHashMap.put("336100","江西");

		youBianHashMap.put("336200","江西");

		youBianHashMap.put("336300","江西");

		youBianHashMap.put("336400","江西");

		youBianHashMap.put("336500","江西");

		youBianHashMap.put("336600","江西");

		youBianHashMap.put("337100","江西");

		youBianHashMap.put("341000","江西");

		youBianHashMap.put("341100","江西");

		youBianHashMap.put("341200","江西");

		youBianHashMap.put("341300","江西");

		youBianHashMap.put("341400","江西");

		youBianHashMap.put("341500","江西");

		youBianHashMap.put("341600","江西");

		youBianHashMap.put("341700","江西");

		youBianHashMap.put("341800","江西");

		youBianHashMap.put("341900","江西");

		youBianHashMap.put("342100","江西");

		youBianHashMap.put("342200","江西");

		youBianHashMap.put("342300","江西");

		youBianHashMap.put("342400","江西");

		youBianHashMap.put("342500","江西");

		youBianHashMap.put("342600","江西");

		youBianHashMap.put("342700","江西");

		youBianHashMap.put("342800","江西");

		youBianHashMap.put("343000","江西");

		youBianHashMap.put("343100","江西");

		youBianHashMap.put("343200","江西");

		youBianHashMap.put("343400","江西");

		youBianHashMap.put("343500","江西");

		youBianHashMap.put("343600","江西");

		youBianHashMap.put("343700","江西");

		youBianHashMap.put("343800","江西");

		youBianHashMap.put("343900","江西");

		youBianHashMap.put("344200","江西");

		youBianHashMap.put("344300","江西");

		youBianHashMap.put("344400","江西");

		youBianHashMap.put("344500","江西");

		youBianHashMap.put("344600","江西");

		youBianHashMap.put("344700","江西");

		youBianHashMap.put("344800","江西");

		youBianHashMap.put("344900","江西");

		youBianHashMap.put("350000","福建");

		youBianHashMap.put("350100","福建");

		youBianHashMap.put("350200","福建");

		youBianHashMap.put("350300","福建");

		youBianHashMap.put("350500","福建");

		youBianHashMap.put("350600","福建");

		youBianHashMap.put("351100","福建");

		youBianHashMap.put("351200","福建");

		youBianHashMap.put("352100","福建");

		youBianHashMap.put("352200","福建");

		youBianHashMap.put("352300","福建");

		youBianHashMap.put("353000","福建");

		youBianHashMap.put("353100","福建");

		youBianHashMap.put("353200","福建");

		youBianHashMap.put("353400","福建");

		youBianHashMap.put("353500","福建");

		youBianHashMap.put("353600","福建");

		youBianHashMap.put("354000","福建");

		youBianHashMap.put("354100","福建");

		youBianHashMap.put("354200","福建");

		youBianHashMap.put("354400","福建");

		youBianHashMap.put("354500","福建");

		youBianHashMap.put("355000","福建");

		youBianHashMap.put("355200","福建");

		youBianHashMap.put("355300","福建");

		youBianHashMap.put("355400","福建");

		youBianHashMap.put("355500","福建");

		youBianHashMap.put("361000","福建");

		youBianHashMap.put("361100","福建");

		youBianHashMap.put("362000","福建");

		youBianHashMap.put("362100","福建");

		youBianHashMap.put("362200","福建");

		youBianHashMap.put("362300","福建");

		youBianHashMap.put("362400","福建");

		youBianHashMap.put("362500","福建");

		youBianHashMap.put("362600","福建");

		youBianHashMap.put("362700","福建");

		youBianHashMap.put("363000","福建");

		youBianHashMap.put("363100","福建");

		youBianHashMap.put("363200","福建");

		youBianHashMap.put("363300","福建");

		youBianHashMap.put("363400","福建");

		youBianHashMap.put("363500","福建");

		youBianHashMap.put("363600","福建");

		youBianHashMap.put("363700","福建");

		youBianHashMap.put("363900","福建");

		youBianHashMap.put("364000","福建");

		youBianHashMap.put("364100","福建");

		youBianHashMap.put("364200","福建");

		youBianHashMap.put("364300","福建");

		youBianHashMap.put("364400","福建");

		youBianHashMap.put("365000","福建");

		youBianHashMap.put("365200","福建");

		youBianHashMap.put("365300","福建");

		youBianHashMap.put("365400","福建");

		youBianHashMap.put("365500","福建");

		youBianHashMap.put("366000","福建");

		youBianHashMap.put("366100","福建");

		youBianHashMap.put("366200","福建");

		youBianHashMap.put("366300","福建");

		youBianHashMap.put("400000","重庆");

		youBianHashMap.put("404000","重庆");

		youBianHashMap.put("408000","重庆");

		youBianHashMap.put("409000","重庆");

		youBianHashMap.put("410000","湖南");

		youBianHashMap.put("410100","湖南");

		youBianHashMap.put("410200","湖南");

		youBianHashMap.put("410300","湖南");

		youBianHashMap.put("410400","湖南");

		youBianHashMap.put("410500","湖南");

		youBianHashMap.put("410600","湖南");

		youBianHashMap.put("411100","湖南");

		youBianHashMap.put("411300","湖南");

		youBianHashMap.put("411400","湖南");

		youBianHashMap.put("411500","湖南");

		youBianHashMap.put("412100","湖南");

		youBianHashMap.put("412200","湖南");

		youBianHashMap.put("412300","湖南");

		youBianHashMap.put("412400","湖南");

		youBianHashMap.put("412500","湖南");

		youBianHashMap.put("413000","湖南");

		youBianHashMap.put("413100","湖南");

		youBianHashMap.put("413200","湖南");

		youBianHashMap.put("413400","湖南");

		youBianHashMap.put("413500","湖南");

		youBianHashMap.put("414100","湖南");

		youBianHashMap.put("414200","湖南");

		youBianHashMap.put("414300","湖南");

		youBianHashMap.put("414400","湖南");

		youBianHashMap.put("415100","湖南");

		youBianHashMap.put("415200","湖南");

		youBianHashMap.put("415300","湖南");

		youBianHashMap.put("415400","湖南");

		youBianHashMap.put("415500","湖南");

		youBianHashMap.put("415600","湖南");

		youBianHashMap.put("415700","湖南");

		youBianHashMap.put("415800","湖南");

		youBianHashMap.put("415900","湖南");

		youBianHashMap.put("416000","湖南");

		youBianHashMap.put("416100","湖南");

		youBianHashMap.put("416200","湖南");

		youBianHashMap.put("416300","湖南");

		youBianHashMap.put("416400","湖南");

		youBianHashMap.put("416500","湖南");

		youBianHashMap.put("416600","湖南");

		youBianHashMap.put("416700","湖南");

		youBianHashMap.put("416800","湖南");

		youBianHashMap.put("416900","湖南");

		youBianHashMap.put("417000","湖南");

		youBianHashMap.put("417100","湖南");

		youBianHashMap.put("417600","湖南");

		youBianHashMap.put("418000","湖南");

		youBianHashMap.put("418100","湖南");

		youBianHashMap.put("418200","湖南");

		youBianHashMap.put("418300","湖南");

		youBianHashMap.put("418400","湖南");

		youBianHashMap.put("418500","湖南");

		youBianHashMap.put("419100","湖南");

		youBianHashMap.put("419200","湖南");

		youBianHashMap.put("419300","湖南");

		youBianHashMap.put("419400","湖南");

		youBianHashMap.put("419500","湖南");

		youBianHashMap.put("419600","湖南");

		youBianHashMap.put("421155","湖南");

		youBianHashMap.put("421200","湖南");

		youBianHashMap.put("421300","湖南");

		youBianHashMap.put("421400","湖南");

		youBianHashMap.put("421500","湖南");

		youBianHashMap.put("421600","湖南");

		youBianHashMap.put("421700","湖南");

		youBianHashMap.put("421800","湖南");

		youBianHashMap.put("422000","湖南");

		youBianHashMap.put("422100","湖南");

		youBianHashMap.put("422200","湖南");

		youBianHashMap.put("422300","湖南");

		youBianHashMap.put("422400","湖南");

		youBianHashMap.put("422500","湖南");

		youBianHashMap.put("422600","湖南");

		youBianHashMap.put("422700","湖南");

		youBianHashMap.put("422800","湖南");

		youBianHashMap.put("422900","湖南");

		youBianHashMap.put("423000","湖南");

		youBianHashMap.put("423300","湖南");

		youBianHashMap.put("423400","湖南");

		youBianHashMap.put("423500","湖南");

		youBianHashMap.put("423600","湖南");

		youBianHashMap.put("424100","湖南");

		youBianHashMap.put("424200","湖南");

		youBianHashMap.put("424300","湖南");

		youBianHashMap.put("424400","湖南");

		youBianHashMap.put("424500","湖南");

		youBianHashMap.put("425000","湖南");

		youBianHashMap.put("425100","湖南");

		youBianHashMap.put("425200","湖南");

		youBianHashMap.put("425300","湖南");

		youBianHashMap.put("425400","湖南");

		youBianHashMap.put("425500","湖南");

		youBianHashMap.put("425600","湖南");

		youBianHashMap.put("425700","湖南");

		youBianHashMap.put("425800","湖南");

		youBianHashMap.put("425900","湖南");

		youBianHashMap.put("430000","湖北");

		youBianHashMap.put("430100","湖北");

		youBianHashMap.put("430200","湖北");

		youBianHashMap.put("431400","湖北");

		youBianHashMap.put("431500","湖北");

		youBianHashMap.put("431600","湖北");

		youBianHashMap.put("431700","湖北");

		youBianHashMap.put("431800","湖北");

		youBianHashMap.put("431900","湖北");

		youBianHashMap.put("432100","湖北");

		youBianHashMap.put("432200","湖北");

		youBianHashMap.put("432300","湖北");

		youBianHashMap.put("432400","湖北");

		youBianHashMap.put("432500","湖北");

		youBianHashMap.put("432600","湖北");

		youBianHashMap.put("432800","湖北");

		youBianHashMap.put("433000","湖北");

		youBianHashMap.put("433100","湖北");

		youBianHashMap.put("433200","湖北");

		youBianHashMap.put("434000","湖北");

		youBianHashMap.put("434100","湖北");

		youBianHashMap.put("434200","湖北");

		youBianHashMap.put("434300","湖北");

		youBianHashMap.put("434400","湖北");

		youBianHashMap.put("434500","湖北");

		youBianHashMap.put("435000","湖北");

		youBianHashMap.put("435100","湖北");

		youBianHashMap.put("435200","湖北");

		youBianHashMap.put("435600","河南");

		youBianHashMap.put("436000","湖北");

		youBianHashMap.put("436100","湖北");

		youBianHashMap.put("436200","湖北");

		youBianHashMap.put("436300","湖北");

		youBianHashMap.put("436400","湖北");

		youBianHashMap.put("436500","湖北");

		youBianHashMap.put("436600","湖北");

		youBianHashMap.put("436700","湖北");

		youBianHashMap.put("437000","湖北");

		youBianHashMap.put("437200","湖北");

		youBianHashMap.put("437300","湖北");

		youBianHashMap.put("437400","湖北");

		youBianHashMap.put("437500","湖北");

		youBianHashMap.put("437600","湖北");

		youBianHashMap.put("441000","湖北");

		youBianHashMap.put("441100","湖北");

		youBianHashMap.put("441300","湖北");

		youBianHashMap.put("441400","湖北");

		youBianHashMap.put("441500","湖北");

		youBianHashMap.put("441600","湖北");

		youBianHashMap.put("441700","湖北");

		youBianHashMap.put("441800","湖北");

		youBianHashMap.put("441900","湖北");

		youBianHashMap.put("442000","湖北");

		youBianHashMap.put("442100","湖北");

		youBianHashMap.put("442200","湖北");

		youBianHashMap.put("442300","湖北");

		youBianHashMap.put("442400","湖北");

		youBianHashMap.put("442500","湖北");

		youBianHashMap.put("442600","湖北");

		youBianHashMap.put("443000","湖北");

		youBianHashMap.put("443200","湖北");

		youBianHashMap.put("443300","湖北");

		youBianHashMap.put("443400","湖北");

		youBianHashMap.put("443500","湖北");

		youBianHashMap.put("443600","湖北");

		youBianHashMap.put("443700","湖北");

		youBianHashMap.put("444100","湖北");

		youBianHashMap.put("444200","湖北");

		youBianHashMap.put("444300","湖北");

		youBianHashMap.put("445000","湖北");

		youBianHashMap.put("445300","湖北");

		youBianHashMap.put("445400","湖北");

		youBianHashMap.put("445500","湖北");

		youBianHashMap.put("445600","湖北");

		youBianHashMap.put("445700","湖北");

		youBianHashMap.put("445800","湖北");

		youBianHashMap.put("450000","河南");

		youBianHashMap.put("450100","河南");

		youBianHashMap.put("451100","河南");

		youBianHashMap.put("451200","河南");

		youBianHashMap.put("451400","河南");

		youBianHashMap.put("452100","河南");

		youBianHashMap.put("452200","河南");

		youBianHashMap.put("452300","河南");

		youBianHashMap.put("452400","河南");

		youBianHashMap.put("452570","河南");

		youBianHashMap.put("452600","河南");

		youBianHashMap.put("453000","河南");

		youBianHashMap.put("453100","河南");

		youBianHashMap.put("453200","河南");

		youBianHashMap.put("453300","河南");

		youBianHashMap.put("453400","河南");

		youBianHashMap.put("453500","河南");

		youBianHashMap.put("453700","河南");

		youBianHashMap.put("453800","河南");

		youBianHashMap.put("454100","河南");

		youBianHashMap.put("454350","河南");

		youBianHashMap.put("454450","河南");

		youBianHashMap.put("454600","河南");

		youBianHashMap.put("454750","河南");

		youBianHashMap.put("454800","河南");

		youBianHashMap.put("454950","河南");

		youBianHashMap.put("455000","河南");

		youBianHashMap.put("456100","河南");

		youBianHashMap.put("456250","河南");

		youBianHashMap.put("456350","河南");

		youBianHashMap.put("456450","河南");

		youBianHashMap.put("456500","河南");

		youBianHashMap.put("456600","河南");

		youBianHashMap.put("457000","河南");

		youBianHashMap.put("457300","河南");

		youBianHashMap.put("457400","河南");

		youBianHashMap.put("457500","河南");

		youBianHashMap.put("457600","河南");

		youBianHashMap.put("461000","河南");

		youBianHashMap.put("461100","河南");

		youBianHashMap.put("461200","河南");

		youBianHashMap.put("461300","河南");

		youBianHashMap.put("461500","河南");

		youBianHashMap.put("462000","河南");

		youBianHashMap.put("462100","河南");

		youBianHashMap.put("462300","河南");

		youBianHashMap.put("462400","河南");

		youBianHashMap.put("462500","河南");

		youBianHashMap.put("462600","河南");

		youBianHashMap.put("463000","河南");

		youBianHashMap.put("463100","河南");

		youBianHashMap.put("463200","河南");

		youBianHashMap.put("463300","河南");

		youBianHashMap.put("463400","河南");

		youBianHashMap.put("463500","河南");

		youBianHashMap.put("463600","河南");

		youBianHashMap.put("463700","河南");

		youBianHashMap.put("463800","河南");

		youBianHashMap.put("464000","河南");

		youBianHashMap.put("464100","河南");

		youBianHashMap.put("464200","河南");

		youBianHashMap.put("464300","河南");

		youBianHashMap.put("464400","河南");

		youBianHashMap.put("465150","河南");

		youBianHashMap.put("465250","河南");

		youBianHashMap.put("465350","河南");

		youBianHashMap.put("465450","河南");

		youBianHashMap.put("466000","河南");

		youBianHashMap.put("466100","河南");

		youBianHashMap.put("466200","河南");

		youBianHashMap.put("466300","河南");

		youBianHashMap.put("466600","河南");

		youBianHashMap.put("466700","河南");

		youBianHashMap.put("466750","河南");

		youBianHashMap.put("467000","河南");

		youBianHashMap.put("467100","河南");

		youBianHashMap.put("467200","河南");

		youBianHashMap.put("467346","河南");

		youBianHashMap.put("467400","河南");

		youBianHashMap.put("467500","河南");

		youBianHashMap.put("471000","河南");

		youBianHashMap.put("471100","河南");

		youBianHashMap.put("471200","河南");

		youBianHashMap.put("471300","河南");

		youBianHashMap.put("471400","河南");

		youBianHashMap.put("471500","河南");

		youBianHashMap.put("471600","河南");

		youBianHashMap.put("471700","河南");

		youBianHashMap.put("471800","河南");

		youBianHashMap.put("471900","河南");

		youBianHashMap.put("472000","河南");

		youBianHashMap.put("472200","河南");

		youBianHashMap.put("472300","河南");

		youBianHashMap.put("472400","河南");

		youBianHashMap.put("472500","河南");

		youBianHashMap.put("473000","河南");

		youBianHashMap.put("473100","河南");

		youBianHashMap.put("473200","河南");

		youBianHashMap.put("473300","河南");

		youBianHashMap.put("473400","河南");

		youBianHashMap.put("473500","河南");

		youBianHashMap.put("474100","河南");

		youBianHashMap.put("474200","河南");

		youBianHashMap.put("474350","河南");

		youBianHashMap.put("474450","河南");

		youBianHashMap.put("474550","河南");

		youBianHashMap.put("474600","河南");

		youBianHashMap.put("474700","河南");

		youBianHashMap.put("475000","河南");

		youBianHashMap.put("475100","河南");

		youBianHashMap.put("475200","河南");

		youBianHashMap.put("475300","河南");

		youBianHashMap.put("475400","河南");

		youBianHashMap.put("476100","河南");

		youBianHashMap.put("476200","河南");

		youBianHashMap.put("476300","河南");

		youBianHashMap.put("476400","河南");

		youBianHashMap.put("476600","河南");

		youBianHashMap.put("476700","河南");

		youBianHashMap.put("476800","河南");

		youBianHashMap.put("476900","河南");

		youBianHashMap.put("477100","河南");

		youBianHashMap.put("477250","河南");

		youBianHashMap.put("510000","广东");

		youBianHashMap.put("510800","广东");

		youBianHashMap.put("511100","广东");

		youBianHashMap.put("511200","广东");

		youBianHashMap.put("511300","广东");

		youBianHashMap.put("511400","广东");

		youBianHashMap.put("511500","广东");

		youBianHashMap.put("511600","广东");

		youBianHashMap.put("511700","广东");

		youBianHashMap.put("512000","广东");

		youBianHashMap.put("512100","广东");

		youBianHashMap.put("512200","广东");

		youBianHashMap.put("512300","广东");

		youBianHashMap.put("512400","广东");

		youBianHashMap.put("512500","广东");

		youBianHashMap.put("512600","广东");

		youBianHashMap.put("512700","广东");

		youBianHashMap.put("513000","广东");

		youBianHashMap.put("513100","广东");

		youBianHashMap.put("513200","广东");

		youBianHashMap.put("513300","广东");

		youBianHashMap.put("513400","广东");

		youBianHashMap.put("514000","广东");

		youBianHashMap.put("514100","广东");

		youBianHashMap.put("514200","广东");

		youBianHashMap.put("514300","广东");

		youBianHashMap.put("514400","广东");

		youBianHashMap.put("514600","广东");

		youBianHashMap.put("515000","广东");

		youBianHashMap.put("515100","广东");

		youBianHashMap.put("515200","广东");

		youBianHashMap.put("515300","广东");

		youBianHashMap.put("515400","广东");

		youBianHashMap.put("515500","广东");

		youBianHashMap.put("515600","广东");

		youBianHashMap.put("515700","广东");

		youBianHashMap.put("515800","广东");

		youBianHashMap.put("515900","广东");

		youBianHashMap.put("516000","广东");

		youBianHashMap.put("516100","广东");

		youBianHashMap.put("516200","广东");

		youBianHashMap.put("516300","广东");

		youBianHashMap.put("516400","广东");

		youBianHashMap.put("516500","广东");

		youBianHashMap.put("516600","广东");

		youBianHashMap.put("516700","广东");

		youBianHashMap.put("517000","广东");

		youBianHashMap.put("517100","广东");

		youBianHashMap.put("517200","广东");

		youBianHashMap.put("517300","广东");

		youBianHashMap.put("517400","广东");

		youBianHashMap.put("518000","广东");

		youBianHashMap.put("518100","广东");

		youBianHashMap.put("519000","广东");

		youBianHashMap.put("519100","广东");

		youBianHashMap.put("524000","广东");

		youBianHashMap.put("524100","广东");

		youBianHashMap.put("524200","广东");

		youBianHashMap.put("524400","广东");

		youBianHashMap.put("524500","广东");

		youBianHashMap.put("525000","广东");

		youBianHashMap.put("525100","广东");

		youBianHashMap.put("525200","广东");

		youBianHashMap.put("525300","广东");

		youBianHashMap.put("525400","广东");

		youBianHashMap.put("526000","广东");

		youBianHashMap.put("526200","广东");

		youBianHashMap.put("526300","广东");

		youBianHashMap.put("526400","广东");

		youBianHashMap.put("526500","广东");

		youBianHashMap.put("526600","广东");

		youBianHashMap.put("527100","广东");

		youBianHashMap.put("527200","广东");

		youBianHashMap.put("527300","广东");

		youBianHashMap.put("527400","广东");

		youBianHashMap.put("528100","广东");

		youBianHashMap.put("528200","广东");

		youBianHashMap.put("528300","广东");

		youBianHashMap.put("528400","广东");

		youBianHashMap.put("528600","广东");

		youBianHashMap.put("529000","广东");

		youBianHashMap.put("529100","广东");

		youBianHashMap.put("529200","广东");

		youBianHashMap.put("529300","广东");

		youBianHashMap.put("529400","广东");

		youBianHashMap.put("529500","广东");

		youBianHashMap.put("529600","广东");

		youBianHashMap.put("529700","广东");

		youBianHashMap.put("530000","广西");

		youBianHashMap.put("530100","广西");

		youBianHashMap.put("530200","广西");

		youBianHashMap.put("530300","广西");

		youBianHashMap.put("530400","广西");

		youBianHashMap.put("530500","广西");

		youBianHashMap.put("530600","广西");

		youBianHashMap.put("530700","广西");

		youBianHashMap.put("530800","广西");

		youBianHashMap.put("531400","广西");

		youBianHashMap.put("531500","广西");

		youBianHashMap.put("532100","广西");

		youBianHashMap.put("532200","广西");

		youBianHashMap.put("532300","广西");

		youBianHashMap.put("532400","广西");

		youBianHashMap.put("532500","广西");

		youBianHashMap.put("532600","广西");

		youBianHashMap.put("532700","广西");

		youBianHashMap.put("532800","广西");

		youBianHashMap.put("533000","广西");

		youBianHashMap.put("533100","广西");

		youBianHashMap.put("533200","广西");

		youBianHashMap.put("533300","广西");

		youBianHashMap.put("533400","广西");

		youBianHashMap.put("533500","广西");

		youBianHashMap.put("533600","广西");

		youBianHashMap.put("533700","广西");

		youBianHashMap.put("533800","广西");

		youBianHashMap.put("533900","广西");

		youBianHashMap.put("535000","广西");

		youBianHashMap.put("535300","广西");

		youBianHashMap.put("535400","广西");

		youBianHashMap.put("535500","广西");

		youBianHashMap.put("535600","广西");

		youBianHashMap.put("536000","广西");

		youBianHashMap.put("536100","广西");

		youBianHashMap.put("537100","广西");

		youBianHashMap.put("537200","广西");

		youBianHashMap.put("537300","广西");

		youBianHashMap.put("537400","广西");

		youBianHashMap.put("537500","广西");

		youBianHashMap.put("537600","广西");

		youBianHashMap.put("537700","广西");

		youBianHashMap.put("541000","广西");

		youBianHashMap.put("541100","广西");

		youBianHashMap.put("541200","广西");

		youBianHashMap.put("541300","广西");

		youBianHashMap.put("541400","广西");

		youBianHashMap.put("541500","广西");

		youBianHashMap.put("541600","广西");

		youBianHashMap.put("541700","广西");

		youBianHashMap.put("541800","广西");

		youBianHashMap.put("541900","广西");

		youBianHashMap.put("542400","广西");

		youBianHashMap.put("542500","广西");

		youBianHashMap.put("542600","广西");

		youBianHashMap.put("542700","广西");

		youBianHashMap.put("542800","广西");

		youBianHashMap.put("543000","广西");

		youBianHashMap.put("543100","广西");

		youBianHashMap.put("543200","广西");

		youBianHashMap.put("543300","广西");

		youBianHashMap.put("545000","广西");

		youBianHashMap.put("545100","广西");

		youBianHashMap.put("545200","广西");

		youBianHashMap.put("545300","广西");

		youBianHashMap.put("545400","广西");

		youBianHashMap.put("545500","广西");

		youBianHashMap.put("545600","广西");

		youBianHashMap.put("545700","广西");

		youBianHashMap.put("545800","广西");

		youBianHashMap.put("545900","广西");

		youBianHashMap.put("546100","广西");

		youBianHashMap.put("546200","广西");

		youBianHashMap.put("546300","广西");

		youBianHashMap.put("546400","广西");

		youBianHashMap.put("546500","广西");

		youBianHashMap.put("546600","广西");

		youBianHashMap.put("546700","广西");

		youBianHashMap.put("546800","广西");

		youBianHashMap.put("547000","广西");

		youBianHashMap.put("547100","广西");

		youBianHashMap.put("547200","广西");

		youBianHashMap.put("547300","广西");

		youBianHashMap.put("547400","广西");

		youBianHashMap.put("547500","广西");

		youBianHashMap.put("547600","广西");

		youBianHashMap.put("550000","贵州");

		youBianHashMap.put("550100","贵州");

		youBianHashMap.put("550200","贵州");

		youBianHashMap.put("550300","贵州");

		youBianHashMap.put("550400","贵州");

		youBianHashMap.put("550500","贵州");

		youBianHashMap.put("550600","贵州");

		youBianHashMap.put("550700","贵州");

		youBianHashMap.put("550800","贵州");

		youBianHashMap.put("551100","贵州");

		youBianHashMap.put("551200","贵州");

		youBianHashMap.put("551300","贵州");

		youBianHashMap.put("551400","贵州");

		youBianHashMap.put("551500","贵州");

		youBianHashMap.put("551600","贵州");

		youBianHashMap.put("551700","贵州");

		youBianHashMap.put("551800","贵州");

		youBianHashMap.put("552100","贵州");

		youBianHashMap.put("552200","贵州");

		youBianHashMap.put("552300","贵州");

		youBianHashMap.put("552400","贵州");

		youBianHashMap.put("553000","贵州");

		youBianHashMap.put("553100","贵州");

		youBianHashMap.put("553200","贵州");

		youBianHashMap.put("553300","贵州");

		youBianHashMap.put("553400","贵州");

		youBianHashMap.put("554000","贵州");

		youBianHashMap.put("554100","贵州");

		youBianHashMap.put("554200","贵州");

		youBianHashMap.put("554300","贵州");

		youBianHashMap.put("554400","贵州");

		youBianHashMap.put("555100","贵州");

		youBianHashMap.put("556000","贵州");

		youBianHashMap.put("556100","贵州");

		youBianHashMap.put("556200","贵州");

		youBianHashMap.put("556300","贵州");

		youBianHashMap.put("556400","贵州");

		youBianHashMap.put("556500","贵州");

		youBianHashMap.put("556600","贵州");

		youBianHashMap.put("556700","贵州");

		youBianHashMap.put("557100","贵州");

		youBianHashMap.put("557200","贵州");

		youBianHashMap.put("557300","贵州");

		youBianHashMap.put("557400","贵州");

		youBianHashMap.put("557500","贵州");

		youBianHashMap.put("557600","贵州");

		youBianHashMap.put("557700","贵州");

		youBianHashMap.put("557800","贵州");

		youBianHashMap.put("558000","贵州");

		youBianHashMap.put("558100","贵州");

		youBianHashMap.put("558200","贵州");

		youBianHashMap.put("558300","贵州");

		youBianHashMap.put("558400","贵州");

		youBianHashMap.put("561000","贵州");

		youBianHashMap.put("561100","贵州");

		youBianHashMap.put("561200","贵州");

		youBianHashMap.put("561300","贵州");

		youBianHashMap.put("561400","贵州");

		youBianHashMap.put("561500","贵州");

		youBianHashMap.put("561600","贵州");

		youBianHashMap.put("562100","贵州");

		youBianHashMap.put("562200","贵州");

		youBianHashMap.put("562300","贵州");

		youBianHashMap.put("563000","贵州");

		youBianHashMap.put("563100","贵州");

		youBianHashMap.put("563200","贵州");

		youBianHashMap.put("563300","贵州");

		youBianHashMap.put("563400","贵州");

		youBianHashMap.put("563500","贵州");

		youBianHashMap.put("564100","贵州");

		youBianHashMap.put("564200","贵州");

		youBianHashMap.put("564300","贵州");

		youBianHashMap.put("564400","贵州");

		youBianHashMap.put("564500","贵州");

		youBianHashMap.put("564600","贵州");

		youBianHashMap.put("564700","贵州");

		youBianHashMap.put("565100","贵州");

		youBianHashMap.put("565200","贵州");

		youBianHashMap.put("570000","海南");

		youBianHashMap.put("571100","海南");

		youBianHashMap.put("571200","海南");

		youBianHashMap.put("571300","海南");

		youBianHashMap.put("571400","海南");

		youBianHashMap.put("571500","海南");

		youBianHashMap.put("571600","海南");

		youBianHashMap.put("571700","海南");

		youBianHashMap.put("571800","海南");

		youBianHashMap.put("571900","海南");

		youBianHashMap.put("572000","海南");

		youBianHashMap.put("572200","海南");

		youBianHashMap.put("572300","海南");

		youBianHashMap.put("572400","海南");

		youBianHashMap.put("572500","海南");

		youBianHashMap.put("572600","海南");

		youBianHashMap.put("572700","海南");

		youBianHashMap.put("572800","海南");

		youBianHashMap.put("572900","海南");

		youBianHashMap.put("578000","海南");

		youBianHashMap.put("610000","四川");

		youBianHashMap.put("610200","四川");

		youBianHashMap.put("610400","四川");

		youBianHashMap.put("610500","四川");

		youBianHashMap.put("611100","四川");

		youBianHashMap.put("611230","四川");

		youBianHashMap.put("611300","四川");

		youBianHashMap.put("611430","四川");

		youBianHashMap.put("611500","四川");

		youBianHashMap.put("611630","四川");

		youBianHashMap.put("611730","四川");

		youBianHashMap.put("611830","四川");

		youBianHashMap.put("611900","四川");

		youBianHashMap.put("611930","四川");

		youBianHashMap.put("612100","四川");

		youBianHashMap.put("612260","四川");

		youBianHashMap.put("612300","四川");

		youBianHashMap.put("612400","四川");

		youBianHashMap.put("612500","四川");

		youBianHashMap.put("612600","四川");

		youBianHashMap.put("612700","四川");

		youBianHashMap.put("614000","四川");

		youBianHashMap.put("614100","四川");

		youBianHashMap.put("614200","四川");

		youBianHashMap.put("614300","四川");

		youBianHashMap.put("614400","四川");

		youBianHashMap.put("614500","四川");

		youBianHashMap.put("614600","四川");

		youBianHashMap.put("615000","四川");

		youBianHashMap.put("615100","四川");

		youBianHashMap.put("615200","四川");

		youBianHashMap.put("615300","四川");

		youBianHashMap.put("615400","四川");

		youBianHashMap.put("615500","四川");

		youBianHashMap.put("615600","四川");

		youBianHashMap.put("615700","四川");

		youBianHashMap.put("615800","四川");

		youBianHashMap.put("616150","四川");

		youBianHashMap.put("616250","四川");

		youBianHashMap.put("616350","四川");

		youBianHashMap.put("616550","四川");

		youBianHashMap.put("616750","四川");

		youBianHashMap.put("616850","四川");

		youBianHashMap.put("617100","四川");

		youBianHashMap.put("617200","四川");

		youBianHashMap.put("617300","云南");

		youBianHashMap.put("618000","四川");

		youBianHashMap.put("618100","四川");

		youBianHashMap.put("618200","四川");

		youBianHashMap.put("618300","四川");

		youBianHashMap.put("618400","四川");

		youBianHashMap.put("621100","四川");

		youBianHashMap.put("621600","四川");

		youBianHashMap.put("621700","四川");

		youBianHashMap.put("622100","四川");

		youBianHashMap.put("622550","四川");

		youBianHashMap.put("622600","四川");

		youBianHashMap.put("623000","四川");

		youBianHashMap.put("623100","四川");

		youBianHashMap.put("623300","四川");

		youBianHashMap.put("624000","四川");

		youBianHashMap.put("625000","四川");

		youBianHashMap.put("625100","四川");

		youBianHashMap.put("625200","四川");

		youBianHashMap.put("625300","四川");

		youBianHashMap.put("625400","四川");

		youBianHashMap.put("625500","四川");

		youBianHashMap.put("625600","四川");

		youBianHashMap.put("625700","四川");

		youBianHashMap.put("626000","四川");

		youBianHashMap.put("628000","四川");

		youBianHashMap.put("628100","四川");

		youBianHashMap.put("628200","四川");

		youBianHashMap.put("628300","四川");

		youBianHashMap.put("628400","四川");

		youBianHashMap.put("629000","四川");

		youBianHashMap.put("629100","四川");

		youBianHashMap.put("629200","四川");

		youBianHashMap.put("630700","四川");

		youBianHashMap.put("630800","四川");

		youBianHashMap.put("630900","四川");

		youBianHashMap.put("631120","四川");

		youBianHashMap.put("631220","四川");

		youBianHashMap.put("631320","四川");

		youBianHashMap.put("631420","四川");

		youBianHashMap.put("631520","四川");

		youBianHashMap.put("632000","四川");

		youBianHashMap.put("632260","四川");

		youBianHashMap.put("632360","四川");

		youBianHashMap.put("632460","四川");

		youBianHashMap.put("632560","四川");

		youBianHashMap.put("632660","四川");

		youBianHashMap.put("632760","四川");

		youBianHashMap.put("634200","四川");

		youBianHashMap.put("634300","四川");

		youBianHashMap.put("634400","四川");

		youBianHashMap.put("634500","四川");

		youBianHashMap.put("634600","四川");

		youBianHashMap.put("634700","四川");

		youBianHashMap.put("634800","四川");

		youBianHashMap.put("634900","四川");

		youBianHashMap.put("635000","四川");

		youBianHashMap.put("635100","四川");

		youBianHashMap.put("635200","四川");

		youBianHashMap.put("635300","四川");

		youBianHashMap.put("635400","四川");

		youBianHashMap.put("635500","四川");

		youBianHashMap.put("635600","四川");

		youBianHashMap.put("635700","四川");

		youBianHashMap.put("636150","四川");

		youBianHashMap.put("636250","四川");

		youBianHashMap.put("636350","四川");

		youBianHashMap.put("637100","四川");

		youBianHashMap.put("637200","四川");

		youBianHashMap.put("637300","四川");

		youBianHashMap.put("637600","四川");

		youBianHashMap.put("638150","四川");

		youBianHashMap.put("638250","四川");

		youBianHashMap.put("638350","四川");

		youBianHashMap.put("638450","四川");

		youBianHashMap.put("638550","四川");

		youBianHashMap.put("638650","四川");

		youBianHashMap.put("641000","四川");

		youBianHashMap.put("641200","四川");

		youBianHashMap.put("641300","四川");

		youBianHashMap.put("641400","四川");

		youBianHashMap.put("641500","四川");

		youBianHashMap.put("642100","四川");

		youBianHashMap.put("642350","四川");

		youBianHashMap.put("642400","四川");

		youBianHashMap.put("643000","四川");

		youBianHashMap.put("643100","四川");

		youBianHashMap.put("643200","四川");

		youBianHashMap.put("644000","四川");

		youBianHashMap.put("644100","四川");

		youBianHashMap.put("644200","四川");

		youBianHashMap.put("644300","四川");

		youBianHashMap.put("644400","贵州");

		youBianHashMap.put("644400","四川");

		youBianHashMap.put("644500","四川");

		youBianHashMap.put("644600","四川");

		youBianHashMap.put("645150","四川");

		youBianHashMap.put("645250","四川");

		youBianHashMap.put("645350","四川");

		youBianHashMap.put("646000","四川");

		youBianHashMap.put("646200","四川");

		youBianHashMap.put("646300","四川");

		youBianHashMap.put("646400","四川");

		youBianHashMap.put("646500","四川");

		youBianHashMap.put("648100","四川");

		youBianHashMap.put("648200","四川");

		youBianHashMap.put("648300","四川");

		youBianHashMap.put("648400","四川");

		youBianHashMap.put("648500","四川");

		youBianHashMap.put("650000","云南");

		youBianHashMap.put("650300","云南");

		youBianHashMap.put("650400","云南");

		youBianHashMap.put("650500","云南");

		youBianHashMap.put("650600","云南");

		youBianHashMap.put("651100","云南");

		youBianHashMap.put("651200","云南");

		youBianHashMap.put("651300","云南");

		youBianHashMap.put("651400","云南");

		youBianHashMap.put("651500","云南");

		youBianHashMap.put("651600","云南");

		youBianHashMap.put("651700","云南");

		youBianHashMap.put("652200","云南");

		youBianHashMap.put("652300","云南");

		youBianHashMap.put("652400","云南");

		youBianHashMap.put("652500","云南");

		youBianHashMap.put("652600","云南");

		youBianHashMap.put("652700","云南");

		youBianHashMap.put("652800","云南");

		youBianHashMap.put("653100","云南");

		youBianHashMap.put("653200","云南");

		youBianHashMap.put("653300","云南");

		youBianHashMap.put("653400","云南");

		youBianHashMap.put("654100","云南");

		youBianHashMap.put("654200","云南");

		youBianHashMap.put("654300","云南");

		youBianHashMap.put("654400","云南");

		youBianHashMap.put("654600","云南");

		youBianHashMap.put("654800","云南");

		youBianHashMap.put("655000","云南");

		youBianHashMap.put("655100","云南");

		youBianHashMap.put("655200","云南");

		youBianHashMap.put("655400","云南");

		youBianHashMap.put("655500","云南");

		youBianHashMap.put("655600","云南");

		youBianHashMap.put("655700","云南");

		youBianHashMap.put("655800","云南");

		youBianHashMap.put("657000","云南");

		youBianHashMap.put("657100","云南");

		youBianHashMap.put("657200","云南");

		youBianHashMap.put("657300","云南");

		youBianHashMap.put("657400","云南");

		youBianHashMap.put("657500","云南");

		youBianHashMap.put("657600","云南");

		youBianHashMap.put("657700","云南");

		youBianHashMap.put("657800","云南");

		youBianHashMap.put("657900","云南");

		youBianHashMap.put("661100","云南");

		youBianHashMap.put("661200","云南");

		youBianHashMap.put("661300","云南");

		youBianHashMap.put("661400","云南");

		youBianHashMap.put("661500","云南");

		youBianHashMap.put("661600","云南");

		youBianHashMap.put("662200","云南");

		youBianHashMap.put("662400","云南");

		youBianHashMap.put("662500","云南");

		youBianHashMap.put("663000","云南");

		youBianHashMap.put("663100","云南");

		youBianHashMap.put("663200","云南");

		youBianHashMap.put("663300","云南");

		youBianHashMap.put("663400","云南");

		youBianHashMap.put("663500","云南");

		youBianHashMap.put("663600","云南");

		youBianHashMap.put("663700","云南");

		youBianHashMap.put("665000","云南");

		youBianHashMap.put("665100","云南");

		youBianHashMap.put("665600","云南");

		youBianHashMap.put("665700","云南");

		youBianHashMap.put("665800","云南");

		youBianHashMap.put("665900","云南");

		youBianHashMap.put("666100","云南");

		youBianHashMap.put("666200","云南");

		youBianHashMap.put("666300","云南");

		youBianHashMap.put("666400","云南");

		youBianHashMap.put("666500","云南");

		youBianHashMap.put("671000","云南");

		youBianHashMap.put("671200","云南");

		youBianHashMap.put("671300","云南");

		youBianHashMap.put("671400","云南");

		youBianHashMap.put("671500","云南");

		youBianHashMap.put("671600","云南");

		youBianHashMap.put("672100","云南");

		youBianHashMap.put("672400","云南");

		youBianHashMap.put("672500","云南");

		youBianHashMap.put("672600","云南");

		youBianHashMap.put("672700","云南");

		youBianHashMap.put("673200","云南");

		youBianHashMap.put("673400","云南");

		youBianHashMap.put("673500","云南");

		youBianHashMap.put("674100","云南");

		youBianHashMap.put("674200","云南");

		youBianHashMap.put("674300","云南");

		youBianHashMap.put("674400","云南");

		youBianHashMap.put("674500","云南");

		youBianHashMap.put("674600","云南");

		youBianHashMap.put("675000","云南");

		youBianHashMap.put("675100","云南");

		youBianHashMap.put("675200","云南");

		youBianHashMap.put("675300","云南");

		youBianHashMap.put("675400","云南");

		youBianHashMap.put("675500","云南");

		youBianHashMap.put("675600","云南");

		youBianHashMap.put("675700","云南");

		youBianHashMap.put("675800","云南");

		youBianHashMap.put("675900","云南");

		youBianHashMap.put("676200","云南");

		youBianHashMap.put("677000","云南");

		youBianHashMap.put("677300","云南");

		youBianHashMap.put("677400","云南");

		youBianHashMap.put("677500","云南");

		youBianHashMap.put("677600","云南");

		youBianHashMap.put("677700","云南");

		youBianHashMap.put("678000","云南");

		youBianHashMap.put("678100","云南");

		youBianHashMap.put("678200","云南");

		youBianHashMap.put("678300","云南");

		youBianHashMap.put("678400","云南");

		youBianHashMap.put("678500","云南");

		youBianHashMap.put("678600","云南");

		youBianHashMap.put("678700","云南");

		youBianHashMap.put("679100","云南");

		youBianHashMap.put("679200","云南");

		youBianHashMap.put("679300","云南");

		youBianHashMap.put("710000","陕西");

		youBianHashMap.put("710100","陕西");

		youBianHashMap.put("710200","陕西");

		youBianHashMap.put("710300","陕西");

		youBianHashMap.put("710400","陕西");

		youBianHashMap.put("710500","陕西");

		youBianHashMap.put("710600","陕西");

		youBianHashMap.put("711200","陕西");

		youBianHashMap.put("711300","陕西");

		youBianHashMap.put("711400","陕西");

		youBianHashMap.put("711500","陕西");

		youBianHashMap.put("711600","陕西");

		youBianHashMap.put("711700","陕西");

		youBianHashMap.put("712000","陕西");

		youBianHashMap.put("712200","陕西");

		youBianHashMap.put("713100","陕西");

		youBianHashMap.put("713200","陕西");

		youBianHashMap.put("713300","陕西");

		youBianHashMap.put("713400","陕西");

		youBianHashMap.put("713500","陕西");

		youBianHashMap.put("713600","陕西");

		youBianHashMap.put("713700","陕西");

		youBianHashMap.put("713800","陕西");

		youBianHashMap.put("714000","陕西");

		youBianHashMap.put("714100","陕西");

		youBianHashMap.put("714200","陕西");

		youBianHashMap.put("714300","陕西");

		youBianHashMap.put("715100","陕西");

		youBianHashMap.put("715200","陕西");

		youBianHashMap.put("715300","陕西");

		youBianHashMap.put("715400","陕西");

		youBianHashMap.put("715500","陕西");

		youBianHashMap.put("715600","陕西");

		youBianHashMap.put("715700","陕西");

		youBianHashMap.put("716000","陕西");

		youBianHashMap.put("716100","陕西");

		youBianHashMap.put("716200","陕西");

		youBianHashMap.put("717100","陕西");

		youBianHashMap.put("717200","陕西");

		youBianHashMap.put("717300","陕西");

		youBianHashMap.put("717400","陕西");

		youBianHashMap.put("717500","陕西");

		youBianHashMap.put("717600","陕西");

		youBianHashMap.put("718000","陕西");

		youBianHashMap.put("718100","陕西");

		youBianHashMap.put("718200","陕西");

		youBianHashMap.put("718300","陕西");

		youBianHashMap.put("718400","陕西");

		youBianHashMap.put("718500","陕西");

		youBianHashMap.put("718600","陕西");

		youBianHashMap.put("719000","陕西");

		youBianHashMap.put("719100","陕西");

		youBianHashMap.put("719200","陕西");

		youBianHashMap.put("719300","陕西");

		youBianHashMap.put("719400","陕西");

		youBianHashMap.put("721000","陕西");

		youBianHashMap.put("721100","陕西");

		youBianHashMap.put("721200","陕西");

		youBianHashMap.put("721300","陕西");

		youBianHashMap.put("721400","陕西");

		youBianHashMap.put("721500","陕西");

		youBianHashMap.put("721600","陕西");

		youBianHashMap.put("721700","陕西");

		youBianHashMap.put("722200","陕西");

		youBianHashMap.put("722300","陕西");

		youBianHashMap.put("722400","陕西");

		youBianHashMap.put("723000","陕西");

		youBianHashMap.put("723100","陕西");

		youBianHashMap.put("723200","陕西");

		youBianHashMap.put("723300","陕西");

		youBianHashMap.put("723400","陕西");

		youBianHashMap.put("723500","陕西");

		youBianHashMap.put("724100","陕西");

		youBianHashMap.put("724200","陕西");

		youBianHashMap.put("724300","陕西");

		youBianHashMap.put("724400","陕西");

		youBianHashMap.put("725000","陕西");

		youBianHashMap.put("725100","陕西");

		youBianHashMap.put("725200","陕西");

		youBianHashMap.put("725300","陕西");

		youBianHashMap.put("725400","陕西");

		youBianHashMap.put("725500","陕西");

		youBianHashMap.put("725600","陕西");

		youBianHashMap.put("725700","陕西");

		youBianHashMap.put("725800","陕西");

		youBianHashMap.put("726000","陕西");

		youBianHashMap.put("726100","陕西");

		youBianHashMap.put("726200","陕西");

		youBianHashMap.put("726300","陕西");

		youBianHashMap.put("726400","陕西");

		youBianHashMap.put("727000","陕西");

		youBianHashMap.put("727100","陕西");

		youBianHashMap.put("727200","陕西");

		youBianHashMap.put("727300","陕西");

		youBianHashMap.put("727400","陕西");

		youBianHashMap.put("727500","陕西");

		youBianHashMap.put("730000","甘肃");

		youBianHashMap.put("730085","甘肃");

		youBianHashMap.put("730100","甘肃");

		youBianHashMap.put("730200","甘肃");

		youBianHashMap.put("730300","甘肃");

		youBianHashMap.put("730400","甘肃");

		youBianHashMap.put("730500","甘肃");

		youBianHashMap.put("730600","甘肃");

		youBianHashMap.put("730900","甘肃");

		youBianHashMap.put("730913","甘肃");

		youBianHashMap.put("731200","甘肃");

		youBianHashMap.put("731300","甘肃");

		youBianHashMap.put("731400","甘肃");

		youBianHashMap.put("731500","甘肃");

		youBianHashMap.put("731600","甘肃");

		youBianHashMap.put("731700","甘肃");

		youBianHashMap.put("731780","甘肃");

		youBianHashMap.put("731800","甘肃");

		youBianHashMap.put("733000","甘肃");

		youBianHashMap.put("733100","甘肃");

		youBianHashMap.put("733200","甘肃");

		youBianHashMap.put("733300","甘肃");

		youBianHashMap.put("734000","甘肃");

		youBianHashMap.put("734100","甘肃");

		youBianHashMap.put("734200","甘肃");

		youBianHashMap.put("734300","甘肃");

		youBianHashMap.put("734400","甘肃");

		youBianHashMap.put("734500","甘肃");

		youBianHashMap.put("735000","甘肃");

		youBianHashMap.put("735100","甘肃");

		youBianHashMap.put("735200","甘肃");

		youBianHashMap.put("735300","甘肃");

		youBianHashMap.put("735400","内蒙古");

		youBianHashMap.put("736100","甘肃");

		youBianHashMap.put("736200","甘肃");

		youBianHashMap.put("736300","甘肃");

		youBianHashMap.put("736400","甘肃");

		youBianHashMap.put("737100","甘肃");

		youBianHashMap.put("737300","内蒙古");

		youBianHashMap.put("737400","四川");

		youBianHashMap.put("741200","甘肃");

		youBianHashMap.put("741300","甘肃");

		youBianHashMap.put("741400","甘肃");

		youBianHashMap.put("741500","甘肃");

		youBianHashMap.put("741600","甘肃");

		youBianHashMap.put("742100","甘肃");

		youBianHashMap.put("742200","甘肃");

		youBianHashMap.put("742300","甘肃");

		youBianHashMap.put("742400","甘肃");

		youBianHashMap.put("742500","甘肃");

		youBianHashMap.put("743000","甘肃");

		youBianHashMap.put("743200","甘肃");

		youBianHashMap.put("743300","甘肃");

		youBianHashMap.put("743400","甘肃");

		youBianHashMap.put("744000","甘肃");

		youBianHashMap.put("744100","甘肃");

		youBianHashMap.put("744200","甘肃");

		youBianHashMap.put("744300","甘肃");

		youBianHashMap.put("744400","甘肃");

		youBianHashMap.put("744500","甘肃");

		youBianHashMap.put("744600","甘肃");

		youBianHashMap.put("745000","甘肃");

		youBianHashMap.put("745100","甘肃");

		youBianHashMap.put("745200","甘肃");

		youBianHashMap.put("745300","甘肃");

		youBianHashMap.put("745400","甘肃");

		youBianHashMap.put("745600","甘肃");

		youBianHashMap.put("745700","甘肃");

		youBianHashMap.put("746300","甘肃");

		youBianHashMap.put("746400","甘肃");

		youBianHashMap.put("746500","甘肃");

		youBianHashMap.put("747000","甘肃");

		youBianHashMap.put("747100","甘肃");

		youBianHashMap.put("747200","甘肃");

		youBianHashMap.put("747300","甘肃");

		youBianHashMap.put("747400","甘肃");

		youBianHashMap.put("747500","甘肃");

		youBianHashMap.put("747600","甘肃");

		youBianHashMap.put("748100","甘肃");

		youBianHashMap.put("748200","甘肃");

		youBianHashMap.put("748300","甘肃");

		youBianHashMap.put("748400","甘肃");

		youBianHashMap.put("748500","甘肃");

		youBianHashMap.put("750000","宁夏");

		youBianHashMap.put("750100","宁夏");

		youBianHashMap.put("750200","宁夏");

		youBianHashMap.put("750300","内蒙古");

		youBianHashMap.put("751100","宁夏");

		youBianHashMap.put("751200","宁夏");

		youBianHashMap.put("751300","宁夏");

		youBianHashMap.put("751400","宁夏");

		youBianHashMap.put("751500","宁夏");

		youBianHashMap.put("751600","宁夏");

		youBianHashMap.put("751700","宁夏");

		youBianHashMap.put("753000","宁夏");

		youBianHashMap.put("753200","宁夏");

		youBianHashMap.put("753400","宁夏");

		youBianHashMap.put("753500","宁夏");

		youBianHashMap.put("753600","宁夏");

		youBianHashMap.put("756000","宁夏");

		youBianHashMap.put("756100","宁夏");

		youBianHashMap.put("756200","宁夏");

		youBianHashMap.put("756300","宁夏");

		youBianHashMap.put("756400","宁夏");

		youBianHashMap.put("756500","宁夏");

		youBianHashMap.put("810000","青海");

		youBianHashMap.put("810100","青海");

		youBianHashMap.put("810300","青海");

		youBianHashMap.put("810400","青海");

		youBianHashMap.put("810500","青海");

		youBianHashMap.put("810700","青海");

		youBianHashMap.put("810800","青海");

		youBianHashMap.put("810900","青海");

		youBianHashMap.put("811100","青海");

		youBianHashMap.put("811200","青海");

		youBianHashMap.put("811300","青海");

		youBianHashMap.put("811400","青海");

		youBianHashMap.put("811500","青海");

		youBianHashMap.put("811600","青海");

		youBianHashMap.put("811700","青海");

		youBianHashMap.put("811800","青海");

		youBianHashMap.put("812100","青海");

		youBianHashMap.put("812200","青海");

		youBianHashMap.put("812300","青海");

		youBianHashMap.put("813000","青海");

		youBianHashMap.put("813100","青海");

		youBianHashMap.put("813200","青海");

		youBianHashMap.put("814000","青海");

		youBianHashMap.put("815000","青海");

		youBianHashMap.put("816000","青海");

		youBianHashMap.put("816100","青海");

		youBianHashMap.put("817000","青海");

		youBianHashMap.put("817100","青海");

		youBianHashMap.put("817200","青海");

		youBianHashMap.put("817300","青海");

		youBianHashMap.put("817400","青海");

		youBianHashMap.put("817500","青海");

		youBianHashMap.put("830000","新疆");

		youBianHashMap.put("831100","新疆");

		youBianHashMap.put("831200","新疆");

		youBianHashMap.put("831400","新疆");

		youBianHashMap.put("831500","新疆");

		youBianHashMap.put("831700","新疆");

		youBianHashMap.put("831800","新疆");

		youBianHashMap.put("831900","新疆");

		youBianHashMap.put("832000","新疆");

		youBianHashMap.put("832100","新疆");

		youBianHashMap.put("832200","新疆");

		youBianHashMap.put("833000","新疆");

		youBianHashMap.put("833200","新疆");

		youBianHashMap.put("833300","新疆");

		youBianHashMap.put("833400","新疆");

		youBianHashMap.put("833500","新疆");

		youBianHashMap.put("834000","新疆");

		youBianHashMap.put("834400","新疆");

		youBianHashMap.put("834700","新疆");

		youBianHashMap.put("835200","新疆");

		youBianHashMap.put("835300","新疆");

		youBianHashMap.put("835400","新疆");

		youBianHashMap.put("835500","新疆");

		youBianHashMap.put("835600","新疆");

		youBianHashMap.put("835700","新疆");

		youBianHashMap.put("835800","新疆");

		youBianHashMap.put("836500","新疆");

		youBianHashMap.put("838000","新疆");

		youBianHashMap.put("838200","新疆");

		youBianHashMap.put("839000","新疆");

		youBianHashMap.put("839200","新疆");

		youBianHashMap.put("839300","新疆");

		youBianHashMap.put("841000","新疆");

		youBianHashMap.put("841100","新疆");

		youBianHashMap.put("841200","新疆");

		youBianHashMap.put("841300","新疆");

		youBianHashMap.put("841400","新疆");

		youBianHashMap.put("841500","新疆");

		youBianHashMap.put("841600","新疆");

		youBianHashMap.put("841800","新疆");

		youBianHashMap.put("841900","新疆");

		youBianHashMap.put("842000","新疆");

		youBianHashMap.put("842100","新疆");

		youBianHashMap.put("842200","新疆");

		youBianHashMap.put("842300","新疆");

		youBianHashMap.put("843000","新疆");

		youBianHashMap.put("843100","新疆");

		youBianHashMap.put("843200","新疆");

		youBianHashMap.put("843400","新疆");

		youBianHashMap.put("843500","新疆");

		youBianHashMap.put("843600","新疆");

		youBianHashMap.put("843800","新疆");

		youBianHashMap.put("844000","新疆");

		youBianHashMap.put("844100","新疆");

		youBianHashMap.put("844200","新疆");

		youBianHashMap.put("844300","新疆");

		youBianHashMap.put("844400","新疆");

		youBianHashMap.put("844500","新疆");

		youBianHashMap.put("844600","新疆");

		youBianHashMap.put("844700","新疆");

		youBianHashMap.put("844800","新疆");

		youBianHashMap.put("844900","新疆");

		youBianHashMap.put("845150","新疆");

		youBianHashMap.put("845250","新疆");

		youBianHashMap.put("845350","新疆");

		youBianHashMap.put("845450","新疆");

		youBianHashMap.put("845550","新疆");

		youBianHashMap.put("848000","新疆");

		youBianHashMap.put("848100","新疆");

		youBianHashMap.put("848200","新疆");

		youBianHashMap.put("848400","新疆");

		youBianHashMap.put("850000","西藏");

		youBianHashMap.put("850400","西藏");

		youBianHashMap.put("850600","西藏");

		youBianHashMap.put("850700","西藏");

		youBianHashMap.put("850800","西藏");

		youBianHashMap.put("851300","西藏");

		youBianHashMap.put("851400","西藏");

		youBianHashMap.put("852000","西藏");

		youBianHashMap.put("852100","西藏");

		youBianHashMap.put("852200","西藏");

		youBianHashMap.put("852300","西藏");

		youBianHashMap.put("852500","西藏");

		youBianHashMap.put("852600","西藏");

		youBianHashMap.put("854000","西藏");

		youBianHashMap.put("854100","西藏");

		youBianHashMap.put("854500","西藏");

		youBianHashMap.put("854600","西藏");

		youBianHashMap.put("855400","西藏");

		youBianHashMap.put("855700","西藏");

		youBianHashMap.put("856000","西藏");

		youBianHashMap.put("857000","西藏");

		youBianHashMap.put("857200","西藏");

		youBianHashMap.put("859300","西藏");


	}
	
	
	/**
	 * 初始化数据
	 * @author 刘虻
	 * 2007-2-2  下午06:02:40
	 */
	protected void init() {
		putYouBian(); //设置邮编
		putQuHao(); //设置区号
	}

	/**
	 * 通过邮编获取省名
	 * @author 刘虻
	 * @param yb 邮编s
	 * @return 省名
	 * 2007-2-2  下午06:04:36
	 */
	public String getAddressByYB(String yb) {
		
		if (yb==null) {
			return "";
		}
		yb = yb.trim();
		if (yb.length()==0) {
			return "";
		}
		//截取头两位
		if (yb.length()>2) {
			yb = yb.substring(0,2);
		}
		
		String reStr = youBianHashMap.get(yb);
		if (reStr==null) {
			reStr = "";
		}
		return reStr;
	}
	
	
	/**
	 * 通过区号获取省名
	 * @author 刘虻
	 * @param qh 区号
	 * @return 省名
	 * 2007-2-2  下午06:07:36
	 */
	public String getAddressByQH(String qh) {
		
		if (qh==null) {
			return "";
		}
		qh = qh.trim();
		if (qh.length()==0) {
			return "";
		}
		String reStr = quhaoHashMap.get(qh);
		if (reStr==null) {
			reStr = "";
		}
		return reStr;
	}
	
	
	
	/**
	 * 测试入口
	 * @author 刘虻
	 * @param args 导入参数
	 * 2007-2-2  下午05:56:20
	 */
	public static void main(String[] args) {
		
		//构造测试类
		YouBianQuHaoTool ybb = new YouBianQuHaoTool();
		
		System.out.println(ybb.getAddressByYB("220045"));
	}
}
