/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年8月4日
 * V4.0
 */
package com.jphenix.share.tools;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.jphenix.share.lang.SString;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 操作系统相关工具
 * 
 * 2019-04-02 增加了获取当前服务器IP地址信息序列
 * 
 * 
 * @author MBG
 * 2016年8月4日
 */
@ClassInfo({"2019-04-02 12:29","操作系统相关工具"})
public class SysUtil {

	/**
	 * windows 操作系统
	 */
	public final static int TYPE_SYSTEM_WINDOWS = 1;
	
	/**
	 * linux 操作系统
	 */
	public final static int TYPE_SYSTEM_LINUX = 2;
	
	/**
	 * 未知系统
	 */
	public final static int TYPE_SYSTEM_UNKNOWN = 0;
	
	
	
	/**
	 * 获取操作系统类型
	 * @return 操作系统类型
	 * 2016年8月4日
	 * @author MBG
	 */
	public static int getSystemType() {
		//操作系统信息
		String os = 
				SString.valueOf(System.getProperty("os.name")).toLowerCase();  
		if(os.indexOf("win")>-1) {
			return TYPE_SYSTEM_WINDOWS;
		}else if(os.indexOf("linux")>-1) {
			return TYPE_SYSTEM_LINUX;
		}
		return TYPE_SYSTEM_UNKNOWN;
	}
	
	/**
	 * 获取本机IP地址序列
	 * @return 本机IP地址序列
	 * 2019年4月2日
	 * @author MBG
	 */
	public static List<String> getLocalNetAddress(){
		//构建返回值
		List<String> res = new ArrayList<String>();
		try {
			Enumeration<NetworkInterface> rs        = NetworkInterface.getNetworkInterfaces(); //网卡信息迭代
			Enumeration<InetAddress>      address;  //地址信息迭代
			InetAddress                   ele;      //地址元素
			String                        ip;       //地址信息
			while(rs.hasMoreElements()) {
				address = rs.nextElement().getInetAddresses();
				while(address.hasMoreElements()) {
					ele = address.nextElement();
					if(ele.isLoopbackAddress()) {
						//排除环回地址
						continue;
					}
					ip = ele.getHostAddress();
					if(ip==null || ip.indexOf(":")>0) {
						continue;
					}
					res.add(ip);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * 获取本地网卡Mac地址
	 * @return Mac地址
	 * 2016年11月11日
	 * @author MBG
	 */
	public static String getLocalMac() {
		//构建返回值
		StringBuffer sb = new StringBuffer();
		InetAddress ia = null;
		try {
			ia = InetAddress.getLocalHost();
			//获取网卡，获取地址
			byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
			for(int i=0; i<mac.length; i++) {
				if(i!=0) {
					sb.append("-");
				}
				//字节转换为整数
				int temp = mac[i]&0xff;
				String str = Integer.toHexString(temp);
				if(str.length()==1) {
					sb.append("0"+str);
				}else {
					sb.append(str);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString().toUpperCase();
	}
}
