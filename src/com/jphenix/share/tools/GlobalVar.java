/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Properties;

import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 读取配置文件的静态类
 * 配置文件的路径是类的根路径，名字是该类的名字.ini
 * 
 * 被怀疑造成cpu资源百分之百
 * 
 * 比如：/WEB-INF/classes/GlobalVar.ini
 * @author 刘虻 
 * 2006-11-30  上午10:06:20
 */
@ClassInfo({"2014-06-12 20:02","读取配置文件的静态类"})
public class GlobalVar {

	@SuppressWarnings("rawtypes")
    public static final HashMap globalVarHashMap = new HashMap(); // 全局变量容器
	
	/**
	 * 构造函数 
	 * @author 刘虻
	 * 2006-11-30 上午10:06:20
	 */
	public GlobalVar() {
		super();
	}
	
	
	
	/**
	 * 从全局变量容器中获取一个变量
	 * @author 刘虻
	 * @param key 变量主键
	 * @return 变量值
	 * 2006-11-30  上午10:08:25
	 */
	@SuppressWarnings("unchecked")
    public static String getVar(String key) {
		
		if (globalVarHashMap.isEmpty()) {
			File tmpFile = null; //参数配置文件
			FileInputStream fileStream = null; //文件流
			
			try {
				//获取文件对象
				tmpFile = getFileByName("globalvar.ini");
				
				if (tmpFile==null || !tmpFile.exists()) {
					globalVarHashMap.put("null","null");
					System.out.println("警告：没有获得全局变量配置文件路径");
					return "";
				}
				//构造文件流
				fileStream = new FileInputStream(tmpFile);
				//构造参数处理
				Properties properties = new Properties();
				properties.load(fileStream);  //将文件输入属性对象
				//放入参数内容
				globalVarHashMap.putAll(properties);
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				if (fileStream!=null) {
					try {
						fileStream.close();
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				tmpFile = null;
			}
		}
		String reStr = (String)globalVarHashMap.get(key);
		if (reStr==null) {
			reStr = "";
		}
		return reStr;
	}
	
	
	
	
	/**
     * 通过URL方式获得文件对象
     * @author 刘虻
     * @param filePathStr 文件相对路径
     * @return 对应的文件对象
     * @exception 获取文件时发生异常
     * 2006-4-8  14:01:57
     */
	protected static File getFileByName(
            String filePathStr) throws Exception {
        //导入参数合法化
        if (filePathStr == null || filePathStr.length()==0) {
            return null;
        }
        URL configURL =  
        	GlobalVar.class.getClassLoader().getResource(filePathStr);
        if (configURL == null) {
        	configURL = ClassLoader.getSystemResource(filePathStr);
        }
        //整理路径
        filePathStr = BaseUtil.swapString(filePathStr,"\\","/");
        if (filePathStr.startsWith("/") 
                || filePathStr.indexOf(":") > -1) {
            try {
                return new File(filePathStr);
            }catch(Exception e) {
                e.printStackTrace();
                throw new Exception("没有找到路径："+filePathStr);
            }
        }
        try {
        	//文件路径
        	String filePath = configURL.getPath();
        	try {
        		//从Class中获取到的路径都是UTF-8编码格式的
        		filePath = URLDecoder.decode(filePath,"UTF-8");
        	}catch(Exception e) {}
	        //构造xml文件对象
	        return new File(filePath);
        }catch(Exception e) {
            String errMsg = null; //构造错误信息
            if (configURL == null) {
                configURL = 
                    GlobalVar.class.getClassLoader().getResource(".");
                if (configURL == null) {
                    configURL = ClassLoader.getSystemResource(".");
                }
                if (configURL == null) {
                    errMsg = "没有找到指定路径:"+filePathStr;
                }else {
                    errMsg = "没有找到指定路径:\nURL:"+configURL.getPath()+"\nfilePath:"+filePathStr;
                }
            }else {
                errMsg = "没有找到指定路径:\nURL:"+configURL.getPath()+"\nfilePath:"+filePathStr;
            }
            throw new Exception(errMsg);
        }
    }
    
    
    /**
     * 测试入口
     * @author 刘虻
     * @param arg0 导入
     * 2006-11-30  上午10:37:53
     */
    public static void main(String[] arg0) {
    	
    	System.out.println(">>>"+getVar("webhttp"));
    }

}
