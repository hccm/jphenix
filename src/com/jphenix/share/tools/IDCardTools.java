/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年3月25日
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 身份证处理工具
 * @author 马宝刚
 * 2015年3月25日
 */
@ClassInfo({"2015-03-25 20:12","身份证处理工具"})
public class IDCardTools {

    /**
     * 18位身份证转15位身份证
     * @param id18 18位身份证号
     * @return 15位身份证号
     * 2015年3月25日
     * @author 马宝刚
     */
    public static String idto15(String id18) {
        if(id18==null) {
            return "";
        }
        if(id18.length()==15) {
        	return id18;
        }
        if(id18.length()!=18) {
        	return "";
        }
        return id18.substring(0,6)+id18.substring(8,17);
    }
    
    
    /**
     * 15位身份证号转18位身份证号
     * @param id15 15位身份证号
     * @return 18位身份证号
     * 2015年3月25日
     * @author 马宝刚
     */
    public static String idto18(String id15) {
        if(id15==null) {
            return "";
        }
        if(id15.length()==18) {
        	return id15;
        }
        if(id15.length()!=15) {
        	return "";
        }
        //构建返回值  头17位
        String reId = id15.substring(0,6)+"19"+id15.substring(6);
        return reId+getVerifyCode(reId);
    }
    
    /**
     * 校验18位身份证号是否正确
     * @param id18 18位身份证号
     * @return 校验是否通过
     * 2015年3月25日
     * @author 马宝刚
     */
    public static boolean verfy(String id18) {
        if(id18==null || id18.length()!=18) {
            return false;
        }
        String id15 = idto15(id18); //截取出15位身份证号
        return idto18(id15).equals(id18);
    }
    
    
    
    /** 
     * 获取头17位的校验码
     * @param card17 身份证头17位
     * @return 第18位校验码
     * 2015年3月25日
     * @author 马宝刚
     */
    public static char getVerifyCode(String card17) {  
        char[] Ai = card17.toCharArray();  //17位字符数组
        int[] Wi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};  
        //待返回的校验码数组
        char[] verifyCode = {'1','0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'};  
        int S = 0;  
        int Y;  
        for(int i = 0; i < Wi.length; i++){  
            S += (Ai[i] - '0') * Wi[i];  
        }  
        Y = S % 11;  
        return verifyCode[Y];  
    }  
}
