/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.share.bean.instancea.FilesUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.log.ILog;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;


/**
 * 将指定的URL生成静态页面文件
 * @author 刘虻
 * 2007-1-24上午10:14:21
 */
@ClassInfo({"2014-06-12 20:02","生成静态页面"})
public class StaticHtml {
    
    /**
     * 调用指定URL返回URL内容
     * @author 刘虻
     * 2007-6-30下午01:48:06
     * @param sUrl 调用路径
     * @param encode 输出内容编码
     * @param log 日志处理类
     * @return URL内容
     * @throws Exception 执行发生异常
     */
    public static String getUrlString(
    		String sUrl,String encode,ILog log) throws Exception {
    	if(log!=null) {
    		log.log("Begin Visit Url:["+sUrl+"]");
    	}
        if (encode==null || encode.length()==0) {
        	//encode = System.getProperty("file.encoding");
        	encode = "UTF-8";
        }
        ByteArrayOutputStream bos = null; //字节输出流
        InputStream is = null; //读入信息流
        byte[] readBytes = new byte[1024]; //读入缓存
        try {
        	//构造URL
            URL url = new URL(sUrl);
            //构造链接
            URLConnection urlc = url.openConnection();
            //建立链接
            urlc.connect();
            //获取信息流
            is = urlc.getInputStream();
            //建立字节输出流
            bos = new ByteArrayOutputStream();
            
            int readCount; //读取字节
            while ((readCount = is.read(readBytes))!=-1) {
            	bos.write(readBytes,0,readCount);
            }
            if(log!=null) {
	            log.log("Visit Url Completed:["+sUrl+"]");
            }
            return new String(bos.toByteArray(),encode);
        } catch (IOException e) {
            throw new Exception(e);
        }finally {
            try {
            	is.close();
            } catch (Exception e2) {}
            try {
            	bos.close();
            }catch(Exception e2) {}
        }
    }
    
    /**
     * 检测路径是否能正常访问
     * @author 刘虻
     * 2007-6-30下午02:02:44
     * @param sUrl 访问路径
     * @param log 日志处理类
     * @return true正常访问
     */
    public static boolean checkUrl(String sUrl,ILog log) {
    	try {
    		//获取返回信息
    		String reStr = getUrlString(sUrl,null,log);
            return reStr != null && reStr.length() != 0;
        }catch(Exception e) {
    		return false;
    	}
    }
	
    /**
     * 生成静态页面
     * @author 刘虻
     * 2006-9-6下午10:49:13
     * @param sUrl 动态页面路径
     * @param strName 静态页面文件名
     * @param log 日志处理类
     * @return 是否成功
     */
    public boolean doStaticHtml(
    		String sUrl
    		,String filePath
    		,ILog log) throws Exception {
    	if(log!=null) {
	    	log.log("Begin Build Static Page Url::["+sUrl+"]\nTo:["+filePath+"]");
    	}
        if (filePath==null || filePath.length()<1) {
        	return false;
        }
        FileOutputStream fos = null; //文件写入流
        InputStream is = null; //读入信息流
        byte[] readBytes = new byte[1024]; //读入缓存
        File tmpFile = null; //临时文件
        try {
        	//构造URL
            URL url = new URL(sUrl);
            //构造链接
            URLConnection urlc = url.openConnection();
            //建立链接
            urlc.connect();
            //获取信息流
            is = urlc.getInputStream();
            String tmpPath = filePath+".tmp"; //构建临时文件
            tmpFile = FilesUtil.newInstance().createFile(tmpPath);
            //建立文件写入流
            fos = new FileOutputStream(tmpFile);
            int readCount; //读取字节
            while ((readCount = is.read(readBytes))!=-1) {
            	fos.write(readBytes,0,readCount);
            }
            if(log!=null) {
	            log.log("Build Static Page Completed:["+filePath+"] Url:["+sUrl+"]");
            }
        } catch (IOException e) {
            throw new Exception(e);
        }finally {
            try {
            	is.close();
            } catch (Exception e2) {}
            try {
            	fos.close();
            }catch(Exception e2) {}
        }
        if (tmpFile!=null) {
        	if (tmpFile.length()>0) {
        		//目标文件
        		File objFile = new File(filePath);
        		if (objFile.exists()) {
        			objFile.delete();
        		}
        		tmpFile.renameTo(objFile);
        		return true;
        	}else {
        		tmpFile.delete();
        	}
        }
        return false;
    }
}
