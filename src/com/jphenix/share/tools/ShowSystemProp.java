/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import java.util.Enumeration;
import java.util.Properties;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 显示所有系统参数
 * com.jphenix.share.tools.ShowSystemProp
 * @author 刘虻
 * 2006-3-31  19:59:36
 */
@ClassInfo({"2014-06-12 20:02","显示所有系统参数ShowSystemProp"})
public class ShowSystemProp {

    /**
     * 调用入口
     * @author 刘虻
     * @param args
     * 2006-3-31  20:00:05
     */
    public static void main(String[] args) {
        Properties prop = System.getProperties();
        Enumeration<?> keys = prop.keys();
        while(keys.hasMoreElements()) {
            String keyStr = (String)keys.nextElement();
            System.out.println(keyStr + ">" + prop.getProperty(keyStr));
        }
    }
}
