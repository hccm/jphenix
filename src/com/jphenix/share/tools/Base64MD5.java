/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-12
 * V4.0
 */
package com.jphenix.share.tools;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 密码加密类
 * @author 刘虻
 * 2007-10-10下午05:11:03
 */
@ClassInfo({"2014-06-12 20:02","密码加密类"})
public class Base64MD5 {

	protected MD5 md5 = new MD5(); //md5处理类
	
	/**
	 * 构造函数
	 * 2007-10-10下午05:11:03
	 */
	public Base64MD5() {
		super();
	}
	
	/**
	 * 获取加密后的密码
	 * @author 刘虻
	 * 2007-10-10下午05:12:25
	 * @param source 源编码
	 * @return 加密后的编码
	 */
	public String getBase64MD5Code(String source) {
		return new String(Base64.encode(md5.getMD5ofStr(source).getBytes()));
	}

	/**
	 * 获取加密后的密码
	 * @author 刘虻
	 * 2007-10-10下午05:12:25
	 * @param source 源编码
	 * @return 加密后的编码
	 */
	public static String base64MD5Code(String source) {
		return (new Base64MD5()).getBase64MD5Code(source);
	}
	
	
	/**
	 * @author 刘虻
	 * 2007-10-10下午05:11:04
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(base64MD5Code("123"));
	}

}
