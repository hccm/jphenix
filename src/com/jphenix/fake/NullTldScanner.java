/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2018年6月27日
 * V4.0
 */
package com.jphenix.fake;

import javax.servlet.ServletContext;

import org.apache.tomcat.JarScanFilter;
import org.apache.tomcat.JarScanType;
import org.apache.tomcat.JarScanner;
import org.apache.tomcat.JarScannerCallback;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 解决：Tomcat中，避免扫描 .tld 文件的很操蛋的功能。
 * 
 * 
 * 8以后的版本，修改配置文件conf/catalina.properties
 * tomcat.util.scan.StandardJarScanFilter.jarsToSkip=*.jar
 * tomcat.util.scan.StandardJarScanFilter.jarsToScan=
 * 
 * 
 * 
 * 在tomcat默认的context.xml文件中，做如下配置：
 * 
 * <Context processTlds="false">
 *   <JarScanner className="com.jphenix.fake.NullTldScanner"  />
 * </Context>
 * 
 * 2020-01-09 增加了注释，可以不用这个类，只需要修改一下tomcat配置文件即可
 * 
 * 
 * com.jphenix.fake.NullTldScanner
 * @author MBG
 * 2018年6月27日
 */
@ClassInfo({"2020-01-09 12:30","tomcat搜索tld文件空的处理类"})
public class NullTldScanner implements JarScanner {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public NullTldScanner() {
		super();
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void scan(JarScanType paramJarScanType, ServletContext paramServletContext,
			JarScannerCallback paramJarScannerCallback) {
		System.out.println("└(^o^)┘ Skip Scan The Stupid tld Files. By Niubility's [com.jphenix.fake.NullTldScanner]");
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public JarScanFilter getJarScanFilter() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setJarScanFilter(JarScanFilter paramJarScanFilter) {}
}
