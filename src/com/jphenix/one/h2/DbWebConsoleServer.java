/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.one.h2;


import com.jphenix.kernel.baseobject.instanceb.ABase;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;

import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * 数据库控制台服务器
 * com.jphenix.one.h2.DbWebConsoleServer
 * @author 马宝刚
 * 2009-7-28 下午02:54:17
 */
@ClassInfo({"2014-06-04 20:19","数据库控制台服务器"})
public class DbWebConsoleServer extends ABase {

	/**
	 * 服务器类路径
	 */
	protected String SERVER_CLASSPATH = "org.h2.tools.Server";
	
	protected String webDbPort = null; //数据库控制台服务监听端口
	protected boolean enabledWebDbSsl = false; //是否启用SSL方式访问控制台
	protected boolean webAllowOthers = false; //是否允许其它计算机访问控制台
	protected boolean enabledWebConsole = false; //是否允许启动控制台服务器
	protected Object webServer = null; //数据库控制台服务
	
	
	/**
	 * 构造函数
	 * @author 马宝刚
	 */
	public DbWebConsoleServer() {
		super();
	}
	
	
	/**
	 * 停止服务
	 * 马宝刚
	 * 2009-7-28 下午02:38:31
	 */
	public void stop(){
		if (webServer==null) {
			return;
		}
		try {
			//构建终止方法
			Method shutdownMh = 
				webServer.getClass().getMethod("stop");
			//执行终止
			shutdownMh.invoke(webServer);
		}catch(Exception e) {}
	}
	
	/**
	 * 启动服务
	 * 马宝刚
	 * 2009-7-28 下午02:38:09
	 * @throws Exception 执行发生异常
	 */
	public void start() throws Exception {
		if (!isEnabledWebConsole()) {
			return;
		}
		//构建配置参数
		ArrayList<String> argList = new ArrayList<String>();
		argList.add("-webPort");
		argList.add(getWebDbPort());
		if (isEnabledWebDbSsl()) {
			argList.add( "-webSSL");
		}
		if(isWebAllowOthers()) {
			argList.add("-webAllowOthers");
		}
		//构建服务类
		Class<?> serverCls = Class.forName(SERVER_CLASSPATH);
		//构建服务方法
		Method cts = 
			serverCls.getDeclaredMethod(
					"createWebServer", String[].class);
		//构建数据库TCP服务
		webServer = cts.invoke(null, BaseUtil.listToArray(argList,String.class));
		//构建启动方法
		Method startMh = 
			webServer.getClass().getMethod("start");
		log.startLog("Start H2 DbServer Console Port:["
				+getWebDbPort()+"] SSL:["+(isEnabledWebDbSsl()?"true":"false")
				+"] AllowOthers:["+(isWebAllowOthers()?"true":"false")+"]");
		//启动服务
		startMh.invoke(webServer);
	}
	
	
	/**
	 * 是否允许其它计算机访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:06:21
	 * @return 是否允许其它计算机访问控制台
	 */
	public boolean isWebAllowOthers() {
		return webAllowOthers;
	}
	
	/**
	 * 设置是否允许其它计算机访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:05:33
	 * @param webAllowOthers 是否允许其它计算机访问控制台
	 */
	public void setWebAllowOthers(boolean webAllowOthers) {
		this.webAllowOthers = webAllowOthers;
	}
	
	/**
	 * 设置是否允许其它计算机访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:05:33
	 * @param webAllowOthersStr 是否允许其它计算机访问控制台
	 */
	public void setWebAllowOthers(String webAllowOthersStr) {
		webAllowOthers = SBoolean.valueOf(webAllowOthersStr);
	}
	
	/**
	 * 是否启用SSL方式访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:04:51
	 * @return 是否启用SSL方式访问控制台
	 */
	public boolean isEnabledWebDbSsl() {
		return enabledWebDbSsl;
	}
	
	/**
	 * 设置是否启用SSL方式访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:04:25
	 * @param enabledWebDbSsl 是否启用SSL方式访问控制台
	 */
	public void setEnabledWebDbSsl(boolean enabledWebDbSsl) {
		this.enabledWebDbSsl = enabledWebDbSsl;
	}
	
	
	/**
	 * 设置是否启用SSL方式访问控制台
	 * 马宝刚
	 * 2009-7-28 下午03:03:44
	 * @param enabledWebDbSslStr 是否启用SSL方式访问控制台
	 */
	public void setEnabledWebDbSsl(String enabledWebDbSslStr) {
		enabledWebDbSsl = SBoolean.valueOf(enabledWebDbSslStr);
	}
	
	
	/**
	 * 设置数据库控制台服务监听端口
	 * 马宝刚
	 * 2009-7-28 下午03:01:05
	 * @param webDbPort 数据库控制台服务监听端口
	 */
	public void setWebDbPort(String webDbPort) {
		this.webDbPort = webDbPort;
	}
	
	/**
	 * 获取数据库控制台服务监听端口
	 * 马宝刚
	 * 2009-7-28 下午03:02:50
	 * @return 数据库控制台服务监听端口
	 */
	public String getWebDbPort() {
		if(webDbPort==null) {
			webDbPort = "8355";
		}
		return webDbPort;
	}
	
	
	/**
	 * 是否允许启动控制台服务器
	 * 马宝刚
	 * 2009-7-28 下午03:14:35
	 * @return 是否允许启动控制台服务器
	 */
	public boolean isEnabledWebConsole() {
		return enabledWebConsole;
	}

	/**
	 * 设置是否允许启动控制台服务器
	 * 马宝刚
	 * 2009-7-28 下午03:13:59
	 * @param enabledWebConsoleStr 是否允许启动控制台服务器
	 */
	public void setEnabledWebConsole(String enabledWebConsoleStr) {
		enabledWebConsole = SBoolean.valueOf(enabledWebConsoleStr);
	}
	
	/**
	 * 设置是否允许启动控制台服务器
	 * 马宝刚
	 * 2009-7-28 下午03:14:08
	 * @param enabledWebConsole 是否允许启动控制台服务器
	 */
	public void setEnabledWebConsole(boolean enabledWebConsole) {
		this.enabledWebConsole = enabledWebConsole;
	}
	
	
}
