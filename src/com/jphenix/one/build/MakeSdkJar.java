/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年1月26日
 * V4.0
 */
package com.jphenix.one.build;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.share.tools.JarTools;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.ver.PhenixVer;

/**
 * 框架包打包程序
 * 
 * 原本使用Java SDK打包，但是包含其它文件，
 * 或者排除什么文件，原生的就搞不定了
 * 
 * 入参: 1生成打包文件存放的绝对路径
 * 
 * com.jphenix.one.build.MakeSdkJar 生成打包文件存放的绝对路径
 * 
 * 2018-08-01 增加了排除多个指定路径方法
 * 2018-12-17 增加了排除git，svn等操蛋隐藏文件
 * 2020-09-12 如果配置文件中没有配置生成目标路径，比如源码包路径，则不生成目标文件，比如源码包文件
 * 2020-09-12 不需要配置文件，直接将jar包和原密码包生成到目标路径中
 * 2021-09-12 取消通过配置文件生成打包文件，直接通过入参设置输出目标文件夹即可
 *         
 * @author MBG
 * 2017年1月26日
 */
@ClassInfo({"2021-09-12 18:51","框架包打包程序"})
public class MakeSdkJar {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public MakeSdkJar() {
		super();
	}

	/**
	 * 执行入口
	 * @param args 传入参数  0配置文件绝对路径
	 * 2017年1月26日
	 * @author MBG
	 */
	public static void main(String[] args) {
		if(args==null || args.length<1 || args[0]==null) {
			System.err.println("Mast Import The First Parameter For Target Full Path.");
			return;
		}
		String targetPath   = args[0]; // 目标文件夹
		String projBasePath = SFilesUtil.getBaseClassPath(MakeSdkJar.class);
		projBasePath        = SFilesUtil.getAllFilePath("../../..",projBasePath);
		
		// jar包信息文件
		Map<String,String> manifestMap = new HashMap<>();
		manifestMap.put("Main-Class","com.jphenix.one.boot.WebBootStartup");
		
		//构建实体类
		MakeSdkJar msj = new MakeSdkJar();
		try {
			//编译脚本源码
			System.out.println("\n\n------(1/3)Begin Build Script Source File To Class File...");
			BuildScriptClasses.build();
			System.out.println("\n+++(1/3)Build Script Source File To Class File Completed+++");
			
			//将源码打包
			System.out.println("\n\n------(2/3)Begin Build Source Jar File...");
			msj.buildSource(targetPath,projBasePath,manifestMap);
			System.out.println("\n+++(3/3)Build Source Jar File Completed+++");
			
			System.out.println("\n\n------(3/3)Begin Build Binary Jar File...");
			//打包编译后的文件
			msj.buildBinary(targetPath,projBasePath,manifestMap);
			System.out.println("\n+++(3/3)Build Binary Jar File Completed+++");
			
			System.out.println("\n\n\n++++++Finally Build The Jar File All Completed++++++\n\n\n");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 打包编译后的文件
	 * @param  targetPath   目标文件夹
	 * @param  projBasePath 项目根路径
	 * @param  manifestMap  压缩包信息容器
	 * @throws Exception    异常
	 * 2017年1月27日
	 * @author MBG
	 */
	public void buildBinary(String targetPath,String projBasePath,Map<String,String> manifestMap) throws Exception {
		//目标压缩包路径
		String jarFilePath = SFilesUtil.getAllFilePath("/jphenix_sdk.jar.new",targetPath);
		//目标压缩包
		File file = new File(jarFilePath);
		if(file.exists()) {
			file.delete();
		}
		//构建打包类
		JarTools jt = new JarTools();
		//构建压缩包对象
		Object jar = jt.createJarFile(file.getPath());
		//搜索根路径
		String basePath = projBasePath+"/webroot/WEB-INF/classes";
		//搜索目标文件序列
		List<String> fileList = new ArrayList<>(); 
		String fileAllPath; //文件全路径
		File   eleFile;     //文件元素
		// 搜索编译文件夹
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("script_classes/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,subPath);
		}
		
		// 放入包内资源配置文件
		jt.addJarFileElement(jar,"/resources/.jpxres",getJpxresContent());
		
		// 放入servlet-api.jar 内置的web服务需要用到
		jt.addJarFileElement(jar,projBasePath+"/webroot/WEB-INF/lib/servlet-api.jar","/resources/lib/servlet-api.jar");
		
		// 放入配置文件
		jt.addJarFileElement(jar,projBasePath+"/webroot/WEB-INF/resfiles/base.xml","/resources/resfiles/base.xml");
		
		// 放入编译前的脚本文件
		basePath = projBasePath+"/webroot/WEB-INF/script_src";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("script_src/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/script_src/"+subPath);
		}
		
		// 放入编译前的可选脚本文件
		basePath = projBasePath+"/webroot/WEB-INF/script_optional";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("/script_optional/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/script_optional/"+subPath);
		}
		
		// 放入编译后的脚本类文件
		basePath = projBasePath+"/webroot/WEB-INF/script_classes";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("/script_classes/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/script_classes/"+subPath);
		}
		
		// 放入页面文件
		basePath = projBasePath+"/webroot/adm";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory()) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/web/adm/"+subPath);
		}
		
		//添加信息
		jt.addManifest(jar,manifestMap);
		//关闭jar文件
		jt.closeJarFile(jar,jarFilePath);
		
		System.out.println("\n           Build Source Jar File Completed ["+jarFilePath+"]");
	}
	

	/**
	 * 将源码打包
	 * @param  targetPath   目标文件夹
	 * @param  projBasePath 项目根路径
	 * @param  manifestMap  压缩包信息容器
	 * @throws Exception    异常
	 * 2017年1月27日
	 * @author MBG
	 */
	public void buildSource(String targetPath,String projBasePath,Map<String,String> manifestMap) throws Exception {
		//目标压缩包路径
		String jarFilePath = SFilesUtil.getAllFilePath("/jphenix_sdk_src.zip",targetPath);
		//目标压缩包
		File file = new File(jarFilePath);
		if(file.exists()) {
			file.delete();
		}
		//构建打包类
		JarTools jt = new JarTools();
		//构建压缩包对象
		Object jar = jt.createJarFile(file.getPath());
		//搜索根路径
		String basePath = projBasePath+"/src";
		//搜索目标文件序列
		List<String> fileList = new ArrayList<>(); 
		String fileAllPath; //文件全路径
		File   eleFile;     //文件元素
		// 搜索java源码
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory()) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/src/"+subPath);
		}
		
		// 放入包内资源配置文件
		jt.addJarFileElement(jar,"/resources/.jpxres",getJpxresContent());
		
		// 放入servlet-api.jar 内置的web服务需要用到
		jt.addJarFileElement(jar,projBasePath+"/webroot/WEB-INF/lib/servlet-api.jar","/resources/lib/servlet-api.jar");
		
		// 放入配置文件
		jt.addJarFileElement(jar,projBasePath+"/webroot/WEB-INF/resfiles/base.xml","/resources/resfiles/base.xml");
		jt.addJarFileElement(jar,projBasePath+"/webroot/WEB-INF/resfiles/native_script_build_base.xml","/resources/resfiles/native_script_build_base.xml");
		
		// 放入编译前的脚本文件
		basePath = projBasePath+"/webroot/WEB-INF/script_src";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("script_src/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/script_src/"+subPath);
		}
		
		// 放入编译前的可选脚本文件
		basePath = projBasePath+"/webroot/WEB-INF/script_optional";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory() 
					|| fileAllPath.indexOf("/script_optional/test")>-1) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/script_optional/"+subPath);
		}
		
		// 放入页面文件
		basePath = projBasePath+"/webroot/adm";
		fileList.clear();
		SFilesUtil.getFileList(fileList,basePath,null,null,true,true,true);
		for(String subPath:fileList) {
			fileAllPath = SFilesUtil.getAllFilePath(subPath,basePath);
			eleFile = new File(fileAllPath);
			if(eleFile.isDirectory()) {
				continue;
			}
			jt.addJarFileElement(jar,fileAllPath,"/resources/web/adm/"+subPath);
		}
		
		//添加信息
		jt.addManifest(jar,manifestMap);
		//关闭jar文件
		jt.closeJarFile(jar,jarFilePath);
		
		System.out.println("\n           Build Source Jar File Completed ["+jarFilePath+"]");
	}
	
	
	/**
	 * 返回包内的资源信息配置文件内容
	 * @return            文件内容
	 * @throws Exception  异常
	 * 2021年9月12日
	 * @author MBG
	 */
	private byte[] getJpxresContent() throws Exception {
		return new StringBuffer()
				.append("#版本信息\n")
				.append("ver=").append(PhenixVer.getVER()).append("\n")
				.append("\n")
				.append("[mapping]\n")
				.append("/adm=/web\n")
				.append("/error=/web\n")
				.toString()
				.getBytes("UTF-8");
	}
}