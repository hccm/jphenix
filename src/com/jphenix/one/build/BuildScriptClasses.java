package com.jphenix.one.build;

import java.util.ArrayList;

import com.jphenix.kernel.objectloader.FBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.kernel.script.ScriptLoader;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 执行该类用来重新编译脚本
 * 该类通常用在项目中，在打包之前执行，编译生成内部脚本class
 * 
 * 2021-09-12 由于迁移了文件路径，重新做的适配
 * 
 * com.jphenix.one.build.BuildScriptClasses
 */
@ClassInfo({"2021-09-12 18:51","执行该类用来重新编译脚本"})
public class BuildScriptClasses {
  
  /**
   * 构造函数
   */
  public BuildScriptClasses(){
    super();
  }


  /**
   * 执行入口
   * @param {*} args 无用
   */
  public static void main(String[] args){
    try{
    	BuildScriptClasses.build();
    }catch(Exception e){
      e.printStackTrace();
    }
  }


  /**
   * 启动编译
   * @throws Exception 异常
   */
  public static void build() throws Exception {
    // 项目根路径
    String webinfPath     = SFilesUtil.getBaseClassPath(BuildScriptClasses.class); // /jphenix/webroot/WEB-INF/classes/
    webinfPath            = SFilesUtil.getAllFilePath("..", webinfPath);           // /jphenix/webroot/WEB-INF
    // 类加载器配置文件路径
    String configFilePath = webinfPath+"/resfiles/native_script_build_base.xml";

    // 构建类加载器
    IBeanFactory bf = FBeanFactory.getNewBeanFactory(configFilePath,false,true); 

    FBeanFactory.initBeanFactory(bf,configFilePath,webinfPath,new ArrayList<String>()); 
    
    ScriptLoader sl = new ScriptLoader();
    bf.setObject(ScriptLoader.class,sl); // 设置到类加载器中
    
    ((IBean)sl).setBeanFactory(bf); // 设置类加载器
    
    sl.setSourceBasePath(webinfPath+"/script_src");      // 设置脚本源文件根路径
    sl.setScriptClassPath(webinfPath+"/script_classes"); // 设置编译后的脚本类文件根路径
    sl.setDeleteBuildSource(true); // 不保留编译后的java文件
    sl.startNative(true);          // 执行启动并编译全部脚本
    
    sl.stop(); // 终止脚本加载器
    bf.destroy(IBeanFactory.STATE_REST_NONE); // 终止类加载器
  }
}
