/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年4月27日
 * V4.0
 */
package com.jphenix.one.maven;

import java.util.List;

import com.jphenix.driver.nodehandler.FNodeHandler;
import com.jphenix.share.tools.HttpCall;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 集合中的模块信息类
 * @author MBG
 * 2019年4月27日
 */
@ClassInfo({"2019-04-27 12:51","集合中的模块信息类"})
public class ArtifactInfo {

	
	/**
	 * 输出指定包中全部模块到控制台
	 * @param groupId     包名
	 * @throws Exception  异常
	 * 2019年4月27日
	 * @author MBG
	 */
	public static void showArtifact(String groupId) throws Exception {
		int pageNo = 1; //页号
		while(showArtifact(groupId,pageNo++)) {}
	}
	
	/**
	 * 分页获取全部模块信息
	 * @param groupId     包名
	 * @param pageNo      页号
	 * @return            存在记录返回真
	 * @throws Exception  异常
	 * 2019年4月27日
	 * @author MBG
	 */
	private static boolean showArtifact(String groupId,int pageNo) throws Exception {
		//获取页面
		String htmlContent = HttpCall.call(Go.BASE_URL+"/search?q="+groupId+"&p="+pageNo);
		//构建页面对象
		IViewHandler mainVh = FNodeHandler.newBody(htmlContent).fcid("maincontent");
		//总共记录数
		String resultsCount = mainVh.fnn("h2").fnn("b").nt();
		//元素信息段
		List<IViewHandler> cList = mainVh.getChildNodesByAttribute("class","im");
		if(cList.size()<1) {
			return false;
		}
		if(pageNo==1) {
			System.out.println("包名：["+groupId+"] 总记录数：["+resultsCount+"] 当前页号：["+pageNo+"]");
			System.out.println("[序号] [模块主键] [模块名]");
		}
		String index;            //序号
		String title;            //模块标题
		List<IViewHandler> cvhs; //模块信息序列
		String moduleName;       //模块名
		for(IViewHandler ele:cList) {
			ele   = ele.getFirstChildNodeByAttribute("class","im-header");
			index = ele.fnn("span").nt();
			index = BaseUtil.swap(index,".",""," ","");
			title = ele.fnn("a").nt();
			ele   = ele.fnn("p");
			cvhs  = ele.cnn("a");
			if(cvhs.size()>0) {
				moduleName = cvhs.get(cvhs.size()-1).nt();
			}else {
				continue;
			}
			System.out.println("["+index+"] ["+moduleName+"] ["+title+"]");
		}
		return true;
	}
}
