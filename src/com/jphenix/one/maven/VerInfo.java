/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年4月27日
 * V4.0
 */
package com.jphenix.one.maven;

import java.util.List;

import com.jphenix.driver.nodehandler.FNodeHandler;
import com.jphenix.share.tools.HttpCall;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 模块版本号信息类
 * @author MBG
 * 2019年4月27日
 */
@ClassInfo({"2019-04-27 14:02","模块版本号信息类"})
public class VerInfo {


	/**
	 * 显示指定模块全部版本号信息
	 * @param groupId     包主键
	 * @param artifactId  模块主键
	 * @throws Exception  异常
	 * 2019年4月27日
	 * @author MBG
	 */
	public static void showVer(String groupId,String artifactId) throws Exception {
		//获取页面
		String htmlContent = HttpCall.call(Go.BASE_URL+"/artifact/"+groupId+"/"+artifactId);
		//构建页面对象
		List<IViewHandler> bodyList = 
				FNodeHandler.newBody(htmlContent)
				  .getFirstChildNodeByAttribute("class","grid versions")
				    .cnn("tbody");
		System.out.println("[版本号] [更新日期]");
		List<IViewHandler> trList; //行节点序列
		List<IViewHandler> tdList; //列节点序列
		String             ver;    //版本号              
		for(IViewHandler body:bodyList) {
			trList = body.cnn("tr");
			for(IViewHandler tr:trList) {
				tdList = tr.cnn("td");
				if(tdList.size()<3) {
					continue;
				}
				ver = tdList.get(0).fnn("a").nt();
				if(ver.length()<1) {
					ver = tdList.get(1).fnn("a").nt();
				}
				System.out.println("["+ver+"] ["+tdList.get(tdList.size()-1).nt()+"]");
			}
		}
	}

}
