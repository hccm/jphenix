/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年4月27日
 * V4.0
 */
package com.jphenix.one.maven;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.driver.nodehandler.FNodeHandler;
import com.jphenix.driver.threadpool.ThreadSession;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 从Maven中下载指定包以及依赖的包
 * 
 * 2019-04-29 增加了内部类调用方法
 * 2019-07-18 完善了代码，但是对方服务器增加了防爬机制，访问一会儿就提示403
 * 
 * com.jphenix.one.maven.Go
 * 
 * @author MBG
 * 2019年4月27日
 */
@ClassInfo({"2019-07-18 14:28","从Maven中下载指定包以及依赖的包"})
public class Go {
	
	/**
	 * 根URL
	 */
	public static final String BASE_URL = "https://mvnrepository.com";

	/**
	 * 构造函数
	 * @author MBG
	 */
	public Go() {
		super();
	}

	/**
	 * 执行入口
	 * @param args 传入参数 (请看help方法）
	 * 2019年4月27日
	 * @author MBG
	 */
	public static void main(String[] args) {
		//构建类实例
		Go go = new Go();
		try {
			go.doGo(args); //执行
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 执行
	 * @param args 传入参数
	 * @throws Exception 异常
	 * 2019年4月29日
	 * @author MBG
	 */
	public void doGo(String[] args) throws Exception {
		if(args==null || args.length<1) {
			help(); //在控制台输出帮助信息
			return;
		}
		if("pom".equalsIgnoreCase(args[0])) {
			if(args.length<3) {
				help(); //在控制台输出帮助信息
				return;
			}
			//解析POM文件
			downloadPom(args[1],args[2]);
			return;
		}
		if(args.length<2) {
			//列出指定包中的所有模块名字(artifactId)以及说明
			ArtifactInfo.showArtifact(args[0]);
			return;
		}
		if(args.length<3) {
			//列出指定模块的版本号
			VerInfo.showVer(args[0],args[1]);
			return;
		}
		if(args.length<4) {
			help();
			return;
		}
		//保存会话的容器
		Map<String,String> headerMap = new HashMap<String,String>();
		//执行下载包和依赖的包
		new PackageDownloader(args[3]).download(args[0],args[1],args[2],headerMap);
		
		System.out.println("\n\n\n*************************\n下载任务执行完毕\n*********************************\n\n\n");
	}

	/**
	 * 下载指定包以及依赖的包
	 * @param pomFilePath   POM文件绝对路径
	 * @param objPath       保存目标绝对路径
	 * @throws Exception    异常
	 * 2019年4月27日
	 * @author MBG
	 */
	public void downloadPom(String pomFilePath,String objPath) throws Exception {
		//下载加密协议改用TLSv1.2
		ThreadSession.put("HTTPCALL_SSL_CONTEXT_TYPE","TLSv1.2");
		//构建包下载器
		PackageDownloader pd = new PackageDownloader(objPath);
		//构建XML解析类
		IViewHandler nh = FNodeHandler.newPath(pomFilePath);
		//保存会话的容器
		Map<String,String> headerMap = new HashMap<String,String>();
		//需要下载的包的节点序列
		List<IViewHandler> pList = nh.cnn("dependency");
		for(IViewHandler ele:pList) {
			pd.download(ele.fnn("groupId").nt(),ele.fnn("artifactId").nt(),ele.fnn("version").nt(),headerMap);
		}
	}
	
	/**
	 * 在控制台输出帮助信息
	 * 2019年4月27日
	 * @author MBG
	 */
	public void help() {
		System.out.println();
		System.out.println("传入参数说明： 注意：参数中不包含尖括号");
		System.out.println();
		System.out.println("场景1 传参：   com.jphenix.one.maven.Go <pom> <pom.xml文件绝对路径> <下载到目标绝对路径>");
		System.out.println("说明： 指定pom配置文件，由程序下载配置文件中描述的包，以及依赖的包，pom文件名可以是任意名");
		System.out.println();
		System.out.println("场景2 传参：   com.jphenix.one.maven.Go <需要下载的包名> <集合中指定的名字> <版本号> <下载到目标绝对路径>");
		System.out.println("说明： 指定要下载的包名，即：pom.xml中的groupId 值。集合中指定的名字，即：pom.xml中的artifactId");
		System.out.println();
		System.out.println("场景3 传参：   com.jphenix.one.maven.Go <需要下载的包名>");
		System.out.println("说明： 列出指定包中的所有模块主键(artifactId)以及说明");
		System.out.println();
		System.out.println("场景4 传参：   com.jphenix.one.maven.Go <需要下载的包名> <模块主键>");
		System.out.println("说明： 列出指定包中指定模块名字的所有版本号");
	}
}
