/*
 * Copyright (c) 2009, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.jphenix.sdk.ftp.impl;

import com.jphenix.sdk.ftp.FtpClientProvider;

/**
 * Default FtpClientProvider.
 * Uses sun.net.ftp.FtpCLient.
 * @author mabg
 */
public class DefaultFtpClientProvider extends FtpClientProvider {

    @Override
    public com.jphenix.sdk.ftp.FtpClient createFtpClient() {
        return com.jphenix.sdk.ftp.impl.FtpClient.create();
    }

}
