/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.exception.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 异常管理类
 * @author 马宝刚
 * 2009-12-3 下午02:05:13
 */
@ClassInfo({"2014-06-04 17:20","异常管理类"})
public interface IExceptionManage {

	/**
	 * 处理异常
	 * 马宝刚
	 * 2009-12-3 下午02:07:11
	 * @param e 异常类
	 * @throws Exception 执行发生异常
	 */
	void catchException(Exception e) throws Exception;
}
