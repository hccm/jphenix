/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.instanceb;

import com.jphenix.kernel.objectloader.exception.BeanException;
import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanRegister;
import com.jphenix.kernel.objectloader.vo.BeanVO;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.share.lang.SInteger;
import com.jphenix.share.lang.SString;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.share.util.ClassUtil;
import com.jphenix.share.util.DebugUtil;
import com.jphenix.standard.beans.IParent;
import com.jphenix.standard.beans.IShadow;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.lang.ILangType;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 类容器工具
 * 
 * 原来类信息的处理方法也是放在了BeanVO中，但因为需要加载到内存中的
 * BeanVO数量很大，所以需要精简BeanVO，避免内存占用过多
 * 
 * 2018-06-23 增加了一处返回空时，写入警告日志
 * 2018-09-13 简化了整个架构中日志初始化步骤
 * 2020-04-03 增加了执行初始化后的方法报错时提示具体哪个类信息
 * 2021-09-12 修改了代码，完善了报错信息
 * @author 刘虻
 * 2010-9-1 下午04:58:42
 */
@ClassInfo({"2021-09-12 18:49","类容器工具"})
public class BeanVOUtil {
	
	/**
	 * 清除类信息
	 * 刘虻
	 * 2010-9-1 下午11:17:00
	 * @param beanVO 类信息容器
	 */
	public static void clearVO(BeanVO beanVO) {
		beanVO.parentID = null;			//父类主键
		beanVO.beanRegister = null;		//调用注册类主键
		beanVO.beanVer = null;				//类版本信息
		beanVO.classTitle = null;				//类标题
		beanVO.clientBeanVer = null;		//客户端类版本信息
		beanVO.errorMsg = null;			//错误信息
		beanVO.errorCode = null;			//错误代码 因为错误信息涉及到多语言，这里用错误代码设置
		beanVO.error = false;
		beanVO.classPath = null;			//类路径
		beanVO.clientBeanClassPath = null; //客户端类路径
		beanVO.afterStartMethodName = null;//类加载器启动后调用的方法
		beanVO.initMethodName = null;		//初始化方法
		beanVO.destoryMethodName = null;	//终止方法
		beanVO.propClasses = new ArrayList<Object>();			//属性类型序列
		beanVO.propObjects = new ArrayList<Object>();			//属性值类序列
		beanVO.propNames = new ArrayList<String>();			//属性名序列
		beanVO.propIsVars = new ArrayList<Boolean>();              //属性名中保存的是否为变量名序列
		beanVO.propMethodMap = new HashMap<String, ArrayList<Class<?>>>(); 		//该类方法容器
		beanVO.singletonLevel = 0;			//驻留内存优先等级  0不驻留内存  1~无穷大  优先级由低到高
		beanVO.isParent = false;			//是否为抽象父类
		beanVO.isSetPropInfo = false;		//是否已经设置参数信息
		beanVO.noSetClassPath = true;		//获取在初始化配置文件时，是否加载了类路径
		beanVO.dependLoadOK = false;		//依赖类是否加载完毕
		beanVO.beanClass = null;			//类
		beanVO.bean = null;				//常驻内存类实例
		beanVO.registOK = false;			//在调用注册类主键不为空的情况下，是否执行注册完毕
		beanVO.invokeCount = 0;			//调用次数
		beanVO.state = 0;					//状态信息
	}
	
	/**
	 * 执行终止bean方法
	 * 刘虻
	 * 2010-9-1 下午11:16:43
	 * @param beanVO 类信息容器
	 * @throws Exception 执行发生异常
	 */
	public static void executeDestroyMethod(BeanVO beanVO) throws Exception {
		if (beanVO.destoryMethodName==null 
				|| beanVO.destoryMethodName.length()<1 
				|| beanVO.bean==null) {
			beanVO.bean = null;
			return;
		}
		// 构建将要调用的方法对象
		Method runMethod = null;
		try {
			runMethod = 
				beanVO.bean
					.getClass()
						.getMethod(beanVO.destoryMethodName);
		} catch (Exception e) {
			beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
			beanVO.errorMsg = "Get Bean [" + beanVO.id
			+ "] Destroy Method ["+beanVO.destoryMethodName
			+"] Exception:"+DebugUtil.getExceptionString(e);
			beanVO.error = true;
		}
		if (runMethod!=null) {
			// 调用设置参数方法
			try {
				runMethod.invoke(beanVO.bean);
			} catch (Exception e) {
				beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
				beanVO.errorMsg = "Invoke Bean [" + beanVO.id
						+ "] Destroy Method ["+beanVO.destoryMethodName+"] Exception:"
						+DebugUtil.getExceptionString(e);
				beanVO.error = true;
			}
		}
		beanVO.bean = null;
	}
	
	
	/**
	 * 设置指定变量值
	 * 刘虻
	 * 2010-5-5 下午04:30:24
	 * @param obj 指定对象
	 * @param fieldName 变量名
	 * @param value 变量值
	 */
	protected static void setFieldValue(
			Object obj,Class<?> cls,String fieldName,String value) {
		try {
			//设置值的字段  变量名必须在父类中定义
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(obj,value);
		}catch(Exception e) {
			e.printStackTrace(); //通常不会到这步
		}
	}
	
	
	/**
	 * 获取对应的父类配置信息容器
	 * 刘虻
	 * 2010-6-19 上午07:58:58
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @return 对应的父类配置信息容器 不存在父类加载器时返回空
	 */
	protected static BeanVO getParentVO(
			BeanFactory bfm,BeanVO beanVO) {
		if(beanVO.hasParent==true) {
			if(beanVO.parentBeanVO==null) {
				if(beanVO.parentID==null || beanVO.parentID.length()<1) {
					beanVO.hasParent = false;
					return null;
				}
				try {
					beanVO.parentBeanVO = 
						bfm.getBeanVO(beanVO.parentID,null);
				}catch(Exception e) {
					e.printStackTrace();
				}
				if(beanVO.parentBeanVO==null) {
					bfm.getLog().warning("Not Find The ParentBeanInfo ParentBeanID:["
							+beanVO.parentID+"] From BeanID:["+beanVO.id
							+"] ConfigPath:["+beanVO.sourceInfo+"]",null);
				
					beanVO.hasParent = false;
					return null;
				}
				return beanVO.parentBeanVO;
			}else {
				return beanVO.parentBeanVO;
			}
		}
		return null;
	}
	
	/**
	 * 获取类路径
	 * 刘虻
	 * 2010-2-9 下午02:20:45
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @return 类路径
	 */
	public static String getClassPath(
			BeanFactory bfm,BeanVO beanVO) {
		if(beanVO.classPath==null) {
			if(beanVO.beanClass==null) {
				//获取父类信息容器
				BeanVO parentVO = BeanVOUtil.getParentVO(bfm,beanVO);
				if(parentVO!=null) {
					beanVO.classPath = getClassPath(bfm,parentVO);
				}else {
					beanVO.classPath = "";
				}
			}else {
				beanVO.classPath = beanVO.beanClass.getName();
			}
		}
		return beanVO.classPath;
	}
	
	
	/**
	 * 获取类
	 * 刘虻
	 * 2010-2-10 下午05:55:26
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @return 类
	 * @throws Exception 执行发生异常
	 */
	public static Class<?> getBeanClass(
			BeanFactory bfm,BeanVO beanVO) throws Exception {
		if(beanVO.beanClass==null) {
			try {
				//因为实现模块热替换，就得使用自己的类加载器
				beanVO.beanClass = null;
				try {
					if(beanVO.classLoader!=null) {
						//用专用类加载
						beanVO.beanClass = beanVO.classLoader.loadClass(getClassPath(bfm,beanVO));
					}else {
						beanVO.beanClass = bfm.getClassLoader().loadClass(getClassPath(bfm,beanVO));
					}
				}catch(NoClassDefFoundError e) {
					e.printStackTrace();
				}
				//获取类基本信息
				String[] values = ClassUtil.getClassInfo(beanVO.beanClass);
				beanVO.beanVer = values[0];
				beanVO.classTitle = values[1];
			}catch(Exception e) {
				throw new BeanException(e);
			}
		}
		return beanVO.beanClass;
	}
	
	/**
	 * 加载类内部的配置信息
	 * 
	 * 需要先设置VO中的  classPath
	 * 
	 * 如果需要设置xml中的类配置信息，需要在
	 * 设置前调用此方法
	 * 
	 * @param bfm 类加载器
	 * @param beanVO 类信息容器
	 * 2015年4月1日
	 * @author 马宝刚
	 */
    public static void loadClassInfo(BeanFactory bfm,BeanVO beanVO) {
	    //获取类对象
        if(beanVO.classPath==null || beanVO.classPath.length()<1) {
            return;
        }
	    try {
            beanVO.beanClass = null;
            try {
                if(beanVO.classLoader!=null) {
                	//采用专用类加载器加载
                	beanVO.beanClass = beanVO.classLoader.loadClass(beanVO.classPath);
                }else {
                	beanVO.beanClass = bfm.getClassLoader().loadClass(beanVO.classPath);
                }
			}catch(NoClassDefFoundError e) {
				e.printStackTrace();
			}
	    }catch(Exception e) {}
	    if(beanVO.beanClass==null) {
	        return;
	    }
        //bean信息
        String[] clsInfos = ClassUtil.getBeanInfo(beanVO.beanClass);
        if(beanVO.id==null || beanVO.id.length()<1) {
            //通常都先获取到了类主键
            beanVO.id = clsInfos[0];
        }
        beanVO.isParent = SBoolean.valueOf(clsInfos[1]);
        beanVO.parentID = clsInfos[2];
        
        //运行时信息  
        clsInfos = ClassUtil.getBeanRunning(beanVO.beanClass);
        beanVO.singletonLevel = SInteger.valueOf(clsInfos[0]);
        beanVO.afterStartMethodName = clsInfos[1];
        beanVO.initMethodName = clsInfos[2];
        beanVO.destoryMethodName = clsInfos[3];
        
        //注册信息
        beanVO.beanRegister = ClassUtil.getBeanRegisterID(beanVO.beanClass);
        if(beanVO.beanRegister==null || beanVO.beanRegister.length()<1) {
            beanVO.beanRegister = ClassUtil.getBeanRegisterID(beanVO.beanClass.getSuperclass());
        }
	}
	
	
	/**
	 * 从objClasses中找到和sourceClass相同的类
	 * @author 刘虻 
	 * 2007-4-19下午03:06:09
	 * @param sourceClass 源类
	 * @param objClasses 目标类序列
	 * @return 相同的类
	 */
	protected static Class<?> getClassFromInterface(Class<?> sourceClass,
			ArrayList<Class<?>> objClasses) {
		// 判断当前类是否符合
		if (objClasses.contains(sourceClass)) {
			return sourceClass;
		}
		// 判断当前类实现的接口类型是否有符合
		Class<?>[] interfaceClasses = sourceClass.getInterfaces();
		if (interfaceClasses != null) {
			for (int i = 0; i < interfaceClasses.length; i++) {
				if (interfaceClasses[i] == null) {
					continue;
				}
				if (objClasses.contains(interfaceClasses[i])) {
					return interfaceClasses[i];
				} else {
					//构建返回值
					Class<?> reCls = 
						getClassFromInterface(interfaceClasses[i],objClasses);
					if (reCls!=null) {
						return reCls;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取当前类型可以强制转换成指定类型容器中的哪种类型 (递归函数） 
	 * 比如参数为类实例，但方法导入参数类型是一个接口类等等
	 * @author 刘虻 
	 * 2006-10-17上午10:07:11
	 * @param sourceClass 当前类
	 * @param objClasses 指定类容器
	 * @return 可以强制转换的类
	 */
	protected static Class<?> getLikeBeanClass(
			Class<?> sourceClass, ArrayList<Class<?>> objClasses) {
		// 判断当前类是否符合
		if (objClasses.contains(sourceClass)) {
			return sourceClass;
		}
		// 获取返回值
		Class<?> reCls = getClassFromInterface(sourceClass, objClasses);
		if (reCls != null) {
			return reCls;
		}
		// 获取父类
		Class<?> superClass = sourceClass.getSuperclass();
		if (superClass != null) {
			// 判断父类
			return getLikeBeanClass(superClass, objClasses);
		}
		return null;
	}
	
	
	/**
	 * 获取指定参数类型
	 * @author 刘虻 
	 * 2006-10-17上午10:07:11
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param propClass 参数类型
	 * @param propObject 参数值
	 * @param methodPropArrl 调用方法中的参数序列（每个方法只有一个传入参数）
	 * @throws Exception 执行发生异常
	 * @return 可以强制转换的参数类型
	 */
	protected static Class<?> getLikePropClass(
			BeanFactory bfm
			,BeanVO beanVO
			,Object propClass
			,Object propObject
			,ArrayList<Class<?>> methodPropArrl) throws Exception {
		Class<?> reCls = null; // 构造返回值
		if (SString.valueOf(propClass).equalsIgnoreCase(ILangType.TYPE_ref)) {
			reCls = getBeanClass(bfm,beanVO);
		} else if (SString.valueOf(propClass).equalsIgnoreCase(ILangType.TYPE_refs)) {
			reCls = Array.newInstance(
					getBeanClass(bfm,beanVO),
					((String[]) propObject).length).getClass();
		} else if (propClass instanceof Class[]) {
			reCls = ((Class[]) propClass).getClass();
		} else {
			reCls = (Class<?>) propClass;
		}
		return getLikeBeanClass(reCls, methodPropArrl);
	}
	
	/**
	 * 获取整理后的bean的所有方法容器
	 * @author 刘虻
	 * 2006-10-17上午09:46:32
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @return 方法容器    
	 * 				HashMap  key 方法名  value 
	 * 										ArrayList element 方法参数类型
	 * @throws Exception 执行发生异常
	 */
	protected static HashMap<String,ArrayList<Class<?>>> getAllBeanMethodPropType (
			BeanFactory bfm,BeanVO beanVO) throws Exception{
		//构造返回值
		HashMap<String,ArrayList<Class<?>>> beanMethodHasm = new HashMap<String,ArrayList<Class<?>>>();
		Class<?> beanClass = getBeanClass(bfm,beanVO); //类
		//获取类的所有方法
		Method[] beanMethods = beanClass.getMethods();
		if (beanMethods!=null) {
			for (int i=0;i<beanMethods.length;i++) {
				if (beanMethods[i] == null) {
					continue;
				}
				//获取该方法所有的参数类型
				Class<?>[] propTypes = 
					beanMethods[i].getParameterTypes();
				if (propTypes==null || propTypes.length!=1) {
					//不处理多参数方法和无参数方法
					continue;
				}
				if (propTypes[0]==null) {
					continue;
				}
				//获取方法名
				String methodName = beanMethods[i].getName();
				ArrayList<Class<?>> methodArrl = beanMethodHasm.get(methodName); //相同方法名的方法（不同参数类型）
				if (methodArrl==null) {
					methodArrl = new ArrayList<Class<?>>();
				}
				//放入序列
				methodArrl.add(propTypes[0]);
				//放入容器
				beanMethodHasm.put(methodName,methodArrl);
			}
		}
		return beanMethodHasm;
	}

	/**
	 * 获取初始化后的参数方法容器
	 * 刘虻
	 * 2010-9-2 上午09:50:01
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @return 初始化后的参数方法容器
	 * @throws Exception 执行发生异常
	 */
	protected static HashMap<String,ArrayList<Class<?>>> getPropMethodMap(
			BeanFactory bfm,BeanVO beanVO) throws Exception {
		if(beanVO.propMethodMap.isEmpty()) {
			//获取bean所有的方法中传入的参数类型序列容器
			HashMap<String,ArrayList<Class<?>>> allMethodPropTypeMap = 
				getAllBeanMethodPropType(bfm,beanVO);
			for (int j=0;j<beanVO.propNames.size();j++) {
				//放入容器
				beanVO
				    .propMethodMap.put(
    						beanVO.propNames.get(j)
    						,allMethodPropTypeMap.get(beanVO.propNames.get(j)));
			}
		}
		return beanVO.propMethodMap;
	}
	
	/**
	 * 将参数设置到指定bean实例中
	 * @deprecated
	 * @author 刘虻 
	 * 2006-10-12下午01:47:56
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param beanObject 核心类
	 * @throws Exception 设置参数时发生异常
	 */
	protected static void setObjectProp(
			BeanFactory bfm
			,BeanVO beanVO,Object beanObject) throws Exception {
		// 循环设置参数
	    String classPath = getClassPath(bfm,beanVO); //类路径
	    Object setPropClass; //参数类
	    Object setPropObject; //参数对象
	    String setPropMethodName; //方法名
	    boolean setPropIsVar; //是否直接设置到变量中
		for (int i=0; i<beanVO.propNames.size(); i++) {
			// 当前bean中包含多个同名方法

			// 获取配置文件中要设置的参数类型
			setPropClass = beanVO.propClasses.get(i);
			setPropObject = beanVO.propObjects.get(i);
			setPropMethodName = beanVO.propNames.get(i);
			setPropIsVar = SBoolean.valueOf(beanVO.propIsVars.get(i));
			
			if (setPropClass==null) {
				// 设置的参数值为空，无法知道这个空参数的类型
				bfm.getLog()
					.startLog(
							(new StringBuffer()) 
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.append("The Bean [")
								.append(classPath)
								.append("] Has More Method[")
								.append(setPropMethodName)
								.append("] But Set Null In Prop,I can't Guess Set Witch One")
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.toString());
				continue;
			}else if(setPropClass.equals(ILangType.TYPE_ref)) {
				// 获取将要设置属性的对象
				//类主键
				String beanID = (String)setPropObject;
				setPropObject = bfm.getObject(beanID,null);
				setPropClass = setPropObject.getClass();
			}else if(setPropClass.equals(ILangType.TYPE_refs)) {
				// 获取将要设置属性的对象
				// 构造bean数组
				Object[] propBeans = (Object[]) Array.newInstance(String.class,
						((String[]) setPropObject).length);
				setPropClass = Array.newInstance(String.class,
						((String[]) setPropObject).length);
				String beanID; //类主键
				for (int j=0; j<propBeans.length; j++) {
					//类主键
					beanID = ((String[]) setPropObject)[j];
					propBeans[j] = bfm.getObject(beanID,null);
					((Class[])setPropClass)[j] = propBeans[j].getClass();
				}
				setPropObject = propBeans;
			}
			if(setPropIsVar) {
			    Field varField = null; 
			    try {
			        varField = getBeanField(getBeanClass(bfm,beanVO),setPropMethodName);
			    }catch(Exception e) {
			        e.printStackTrace();
			        bfm.getLog()
	                    .startLog(
	                            (new StringBuffer())
	                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
	                                .append("Create Bean DeclarerdField Error. Bean PK:[")
	                                .append(beanVO.id)
	                                .append("] DeclarerdField Name:[")
	                                .append(setPropMethodName)
	                                .append("] DeclarerdField Class:[")
	                                .append(setPropClass)
	                                .append("]\n")
	                                .append(e)
	                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
	                                .toString());
			    }
			    if(varField==null) {
			    	bfm.getLog().startLog("********Error:The Bean:["
			    			+beanVO.id+"] Not Find The PropMethodName {"+setPropMethodName+"]");
			    	continue;
			    }
			    try {
			        //设置变量值
			        varField.setAccessible(true);
			        varField.set(beanObject,setPropObject);
			    }catch(Exception e) {
			        e.printStackTrace();
			        bfm.getLog()
	                    .startLog(
	                            (new StringBuffer())
	                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
	                                .append("Set Bean DeclarerdField Error. Bean PK:[")
	                                .append(beanVO.id)
	                                .append("] DeclarerdField Name:[")
	                                .append(setPropMethodName)
	                                .append("] DeclarerdField Class:[")
	                                .append(setPropClass)
	                                .append("]\n")
	                                .append(e)
	                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
	                                .toString());
	                continue;
			    }
			    //变量值设置完毕，无需往下运行
			    continue;
			}
	         // 获取参数类型，从多个方法参数类型中（找到能转换的类型）
            ArrayList<Class<?>> methodParaTypeArrl = 
                    getPropMethodMap(bfm,beanVO).get(setPropMethodName);
            if (methodParaTypeArrl==null) {
            	bfm.getLog()
                    .startLog(
                            new StringBuffer()
                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n") 
                                .append("The Bean [")
                                .append(classPath)
                                .append("] Not Find The Method [")
                                .append(setPropMethodName)
                                .append("] From The Source [")
                                .append(beanVO.sourceInfo==null?"":beanVO.sourceInfo)
                                .append("]\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
                                .toString());
                continue;
            }
            //获取参数类行
            Class<?> thisPropClass = 
                getLikePropClass(bfm,beanVO,setPropClass,setPropObject,methodParaTypeArrl);
            if (thisPropClass==null) {
            	bfm.getLog()
                    .startLog(
                            (new StringBuffer())
                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
                                .append("The Bean:[")
                                .append(classPath)
                                .append("] Method:[")
                                .append(setPropMethodName)
                                .append("] Can't Set Prop Class. Config Prop Class:[")
                                .append(setPropClass)
                                .append("] Bean Prop Class:[")
                                .append(getPropMethodMap(bfm,beanVO).get(setPropMethodName))
                                .append("]\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
                                .toString());
                continue;
            }
            if(thisPropClass.getName().equals(classPath)) {
            	bfm.getLog()
                    .startLog(
                            (new StringBuffer())
                                .append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
                                .append("The Bean:[")
                                .append(classPath)
                                .append("] Method:[")
                                .append(setPropMethodName)
                                .append("] Can't Set Prop Himself. Config Prop Class:[")
                                .append(setPropClass)
                                .append("] Bean Prop Class:[")
                                .append(getPropMethodMap(bfm,beanVO).get(setPropMethodName))
                                .append("]\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
                                .toString());
                continue;
            }
			// 构建将要调用的方法对象
			Method runMethod = null;
			try {
				runMethod = getBeanClass(bfm,beanVO).getMethod(setPropMethodName,
						thisPropClass);
			} catch (Exception e) {
				e.printStackTrace();
				bfm.getLog()
					.startLog(
							(new StringBuffer())
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.append("Create Bean Property Error. Bean PK:[")
								.append(beanVO.id)
								.append("] Property Name:[")
								.append(setPropMethodName)
								.append("] Property Class:[")
								.append(setPropClass)
								.append("]\n")
								.append(e)
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.toString());
				continue;
			}

			// 调用设置参数方法
			try {
				runMethod.invoke(beanObject, setPropObject);
			} catch (Exception e) {
				e.printStackTrace();
				bfm.getLog()
					.startLog(
							(new StringBuffer())
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.append("Set Bean Property Error Bean PK:[")
								.append(beanVO.id)
								.append("] Property Name:[")
								.append(setPropMethodName)
								.append("] Property Class:[")
								.append(setPropClass)
								.append("] Source:[")
								.append(beanVO.sourceInfo==null?"":beanVO.sourceInfo)
								.append("]\n")
								.append(e)
								.append("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n")
								.toString());
				continue;
			}
		}
	}
	
	/**
	 * 获取指定类中的变量对象（如果当前类没有，就从父类中获取）
	 * @param beanCls 指定类
	 * @param varName 变量名 
	 * @return 变量对象
	 * 2015年3月31日
	 * @author 马宝刚
	 */
	@SuppressWarnings("rawtypes")
    protected static Field getBeanField(Class beanCls,String varName) {
	    if(beanCls==null) {
	        return null;
	    }
	    try {
	        return beanCls.getDeclaredField(varName);
	    }catch(Exception e) {
	        beanCls = beanCls.getSuperclass();
	        if(beanCls==Object.class) {
	            return null;
	        }
	        return getBeanField(beanCls,varName);
	    }
	}
	    
	
	/**
	 * 获取类实例
	 * 刘虻
	 * 2010-9-1 下午11:17:25
	 * @param beanVO 类信息容器
	 * @return 类实例
	 * @throws Exception 执行发生异常
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(
			BeanFactory bfm,BeanVO beanVO) throws Exception {
		synchronized(beanVO) {
			if(beanVO.singletonLevel>0 && beanVO.bean!=null) {
				//累加调用次数
				if(beanVO.invokeCount==Long.MAX_VALUE) {
					beanVO.invokeCount = 0;
				}else {
					++beanVO.invokeCount;
				}
				if(beanVO.bean instanceof IShadow) {
				    return ((IShadow)beanVO.bean).shadowInstance();
				}
				return (T)beanVO.bean;
			}
			//类
			if(beanVO.beanClass==null) {
				if(beanVO.classPath==null 
						|| beanVO.classPath.length()<1) {
					throw new BeanException("The Bean ClassPath Is Null ID:"+beanVO.id);
				}
				try {
					//因为实现模块热替换，就得使用自己的类加载器
					beanVO.beanClass = null;
					try {
						if(beanVO.classLoader!=null) {
							beanVO.beanClass = beanVO.classLoader.loadClass(beanVO.classPath);
						}else {
							beanVO.beanClass = bfm.getClassLoader().loadClass(beanVO.classPath);
						}
					}catch(NoClassDefFoundError e) {
						e.printStackTrace();
					}
					//获取类基本信息
					String[] values = ClassUtil.getClassInfo(beanVO.beanClass);
					beanVO.beanVer = values[0];
					beanVO.classTitle = values[1];
				}catch(Exception e) {
					throw new BeanException(e);
				}
			}
			T reBean = null;
			try {
				reBean = (T)beanVO.beanClass.newInstance();
			}catch(Exception e) {
				e.printStackTrace();
				throw new BeanException(
						"Get Bean:["+beanVO.id+"] new Instance Exception:"
						+DebugUtil.getExceptionString(e));
			}
			if(reBean instanceof IBean) {
				((IBean)reBean).setBeanFactory(bfm); //设置类加载器
				if(reBean instanceof BaseBean) {
					setFieldValue(reBean,BaseBean.class,"_beanID",beanVO.id);
				}
			}
			//被放到初始化中执行，所以，所有BeanVO在使用之前一定要初始化
			//getPropInfo(this); //设置类参数信息，只设置一次
			setObjectProp(bfm,beanVO,reBean); //设置类的配置参数 每次都要设置
	
			/*
			 * 设置父类 
			 */
			if(reBean instanceof IParent) {
				//从父类加载器获取对应的父类
				BeanVO parentBeanVO = bfm.getBeanVO(beanVO.id,null);
				if(parentBeanVO!=null) {
					((IParent)reBean).setParent(getBean(bfm,parentBeanVO));
				}
			}
			
			if(beanVO.singletonLevel>0) {
				beanVO.bean = reBean;
				if((beanVO.initMethodName==null 
						|| beanVO.initMethodName.length()<1)
					&& (beanVO.afterStartMethodName==null 
								|| beanVO.afterStartMethodName.length()<1)) {
					//如果初始化方法不为空，则标记为需要等待初始化完毕
					if(bfm.isDebugMode()) {
						beanVO.state = IBeanFactory.STATE_RUN_TEST;
					}else {
						//如果有初始化方法，需要等待执行完初始化后标记运行中
						beanVO.state = IBeanFactory.STATE_RUN_MORMAL;
					}
				}else {
					beanVO.state = IBeanFactory.STATE_START_NEED_INIT;
				}
			}else {
				//在进入这块时，已经被设置成待命了，否则无法进入该方法
				//但在重启类时，就要进到这里被设置状态，否则往后获取类实例时就会一直是重启状态
				beanVO.state = IBeanFactory.STATE_RUN_STANDBY;
			}
			/*
			 * 注意：不能在构建类方法中调用该类的初始化方法，
			 * 因为如果自定义的初始化方法中含有启动线程，会导致
			 * 构建方法线程不能正常终止
			 * executeInitMethod();
			 */
			//累加调用次数
			if(beanVO.invokeCount==Long.MAX_VALUE) {
				beanVO.invokeCount = 0;
			}else {
				++beanVO.invokeCount;
			}
			if(reBean instanceof IShadow) {
			    return ((IShadow)reBean).shadowInstance();
			}
			return reBean;
		}
	}

	
	/**
	 * 执行注册类实例
	 * 递归函数
	 * 刘虻
	 * 2010-4-22 下午03:50:40
	 * @param bfm 类工厂
	 * @param beanVO 指定要注册的类信息
	 * @throws Exception 执行发生异常
	 */
	public static void registBean(
			BeanFactory bfm,BeanVO beanVO) throws Exception {
		if(beanVO.isParent 
				|| beanVO.beanRegister==null 
				|| beanVO.beanRegister.length()<1 
				|| beanVO.registOK) {
			return;
		}
		//获取用逗号分割的多个注册类主键
		List<String> regBeanIDList = BaseUtil.splitToList(beanVO.beanRegister,",");
		//获取日志类
		//获取待注册类
		Object bean = getBean(bfm,beanVO);
		if(bean==null) {
			bfm.getLog().warning("************The Regist Bean GetBean Is NULL\n"+beanVO,null);
			return;
		}
		for(String regBeanID:regBeanIDList) {
			if(regBeanID==null || regBeanID.length()<1) {
				continue;
			}
			if(!bfm.beanExists(regBeanID)) {
				bfm.getLog().warning("************The Regist Bean ID:["+regBeanID+"] Not Found. Bean ID:["+beanVO.id+"]",null);
				continue;
			}
			//获取注册类的类信息容器
			BeanVO regBeanVO = bfm.getBeanVO(regBeanID,null);
			if(regBeanVO==null) {
				throw new BeanException("Not Find The Bean ID:["
						+SString.valueOf(beanVO.id)+"] BeanRegister ID:["
						+regBeanID+"]");
			}
			if(regBeanVO.error) {
				throw new BeanException("The Bean ID:["
						+SString.valueOf(beanVO.id)+"] BeanRegister ID:["
						+regBeanID+"] Error:"
						+SString.valueOf(regBeanVO.errorMsg)
						+"] Code:["+SString.valueOf(regBeanVO.errorCode)+"]");
			}
			if(regBeanVO.beanRegister!=null 
					&& regBeanVO.beanRegister.length()>0 
					&& !regBeanVO.registOK) {
				registBean(bfm,regBeanVO); //递归注册
			}
			//获取注册类实例
			Object regBean = getBean(bfm,regBeanVO);
			if(regBean==null) {
				throw new BeanException("The Bean ID:["
						+SString.valueOf(beanVO.id)+"]  BeanRegister ID:["
						+regBeanID+"] Is Null");
			}
			if(!(regBean instanceof IBeanRegister)) {
				throw new BeanException("The Bean ID:["
						+SString.valueOf(beanVO.id)+"]  BeanRegister ID:["
						+regBeanID+"]  Not instanceof IBeanRegister");
			}
			//执行注册
			if(!((IBeanRegister)regBean).regist(bean)) {
				//注册失败
				throw new BeanException("Regist Bean ID:["
						+SString.valueOf(beanVO.id)+"]  BeanRegister ID:["
						+regBeanID+"] Object:["+bean+"] Fail");
			}
		}
		beanVO.registOK = true;
	}
	
	
	
	/**
	 * 执行初始化方法
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * 刘虻
	 * 2010-2-10 下午02:42:48
	 */
	public static void executeInitMethod(
			BeanFactory bfm,BeanVO beanVO) {
		//获取初始化方法
		if (beanVO.initMethodName==null 
				|| beanVO.initMethodName.length()<1 
				|| beanVO.bean==null) {
			return;
		}
		// 构建将要调用的方法对象
		Method runMethod = null;
		try {
			runMethod = beanVO.bean.getClass().getMethod(
					beanVO.initMethodName);
		} catch (Exception e) {
			beanVO.state = IBeanFactory.STATE_EXCEPTION_INFO;
			beanVO.errorMsg = "Get Bean ["+beanVO.id
					+"] Init Method ["+beanVO.initMethodName+"] Exception:"
					+DebugUtil.getExceptionString(e);
			beanVO.error = true;
			return;
		}
		// 调用设置参数方法
		try {
			runMethod.invoke(beanVO.bean);
		} catch (Exception e) {
			beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
			beanVO.errorMsg = "Invoke Bean [" + beanVO.id
					+ "] Init Method ["+beanVO.initMethodName+"] Exception:"
					+DebugUtil.getExceptionString(e);
			beanVO.error = true;
			return;
		}
		//调用初始化方法是类初始化的最后一步，执行后标记初始化执行成功
		if(bfm.isDebugMode()) {
			beanVO.state = IBeanFactory.STATE_RUN_TEST;
		}else {
			beanVO.state = IBeanFactory.STATE_RUN_MORMAL;
		}
	}
	
	


	/**
	 * 获得整理后的设置参数
	 * 刘虻
	 * 2010-2-10 下午04:55:08
	 * @param beanVO 类信息容器
	 * @throws Exception 执行发生异常
	 */
	protected static void getPropInfo(BeanVO beanVO) throws Exception {
		if(beanVO.isSetPropInfo || beanVO==null) {
			return;
		}
		// 获取当前bean中要设置的参数名
		for (int i=0;i<beanVO.propNames.size(); i++) {
			// 获取一个参数信息
			Object onePropCls = beanVO.propClasses.get(i);
			Object onePropObj = beanVO.propObjects.get(i);
			if (onePropCls == null) {
				throw new BeanException(
						"Class Property Type Is Null,Bean ID:["
							+beanVO.id+"] SourceInfo:["
							+beanVO.sourceInfo+"]");
			}
			if (onePropObj == null) {
				throw new BeanException(
						"Class Property Value Is Null,Bean ID:["
							+beanVO.id+"] SourceInfo:["
							+beanVO.sourceInfo+"]");
			}
			if (SString.valueOf(onePropCls).equalsIgnoreCase(ILangType.TYPE_ref)) {
				//注意：不能在这里处理ref值，因为目标类可能没初始化，容易锁死
//				// 获取将要设置属性的对象
//				Object propBean = beanFactory.getObject((String)onePropObj,null);
//				if (propBean == null) {
//					beanFactory.getObject((String)onePropObj,null);
//					throw new BeanException(
//							"Set Ref Property Is Null Method,Bean ID:["
//							+beanVO.getID()+"] SourceInfo:["+beanVO.getSourceInfo()+"]");
//				}
//				// 设置到参数容器中
//				beanVO.getPropClasses().set(i,propBean.getClass());
//				beanVO.getPropObjects().set(i,propBean);
			} else if (SString.valueOf(onePropCls).equalsIgnoreCase(ILangType.TYPE_refs)) {
				//注意：不能在这里处理ref值，因为目标类可能没初始化，容易锁死
//				// 获取将要设置属性的对象
//				Object propBean = 
//					beanFactory.getObject(((String[]) onePropObj)[0],null);
//				if (propBean == null) {
//					throw new BeanException(
//							"Set Ref Property Is Null,Bean ID:["
//							+beanVO.getID()+"] SourceInfo:["+beanVO.getSourceInfo()+"]");
//				}
//				Class beanCls = propBean.getClass();
//				// 构造bean数组
//				Object[] propBeans = (Object[]) Array.newInstance(beanCls,
//						((String[]) onePropObj).length);
//
//				for (int j=0; j<propBeans.length; j++) {
//					propBeans[j] = 
//						beanFactory.getObject(((String[]) onePropObj)[0],null);
//				}
//				// 设置到参数容器中
//				beanVO.getPropClasses().set(i,propBean.getClass());
//				beanVO.getPropObjects().set(i,propBean);
			}
		}
		beanVO.isSetPropInfo = true;
	}
	

	/**
	 * 设置父类的参数配置信息
	 * 刘虻
	 * 2010-9-2 下午04:41:00
	 * @param bfm 类工厂
	 * @param beanVO 当前类信息容器
	 * @param parentVO 父类信息容器
	 */
	protected static void addParentPropInfo(
			BeanFactory bfm,BeanVO beanVO,BeanVO parentVO) {
		if(parentVO==null) {
			return;
		}
		for(int i=0;i<parentVO.propNames.size();i++) {
			//获取参数方法名
			String propName = 
				SString.valueOf(parentVO.propNames.get(i));
			//父类的配置方法不能覆盖当前子类的
			if(beanVO.propNames.contains(propName)) {
				continue;
			}
			beanVO.propNames.add(propName);
			beanVO.propClasses.add(parentVO.propClasses.get(i));
			beanVO.propObjects.add(parentVO.propObjects.get(i));
			beanVO.propIsVars.add(parentVO.propIsVars.get(i));
		}
		addParentPropInfo(bfm,beanVO,getParentVO(bfm,parentVO)); //尝试获取父类的父类
	}
	
	/**
	 * 判断设置启动后执行的方法名
	 * 刘虻
	 * 2010-9-2 下午04:44:33
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param parentVO 父类信息容器
	 */
	protected static void setAfterStartMethodName(
			BeanFactory bfm,BeanVO beanVO,BeanVO parentVO) {
		if(parentVO==null) {
			return;
		}
		if(beanVO.afterStartMethodName==null 
				|| beanVO.afterStartMethodName.length()<1) { 
			beanVO.afterStartMethodName = parentVO.afterStartMethodName;
			//如果没获取到，则再尝试从父类的父类获取
			setAfterStartMethodName(bfm,beanVO,getParentVO(bfm,parentVO));
		}
	}
	
	/**
	 * 设置注册父类主键
	 * 刘虻
	 * 2010-9-2 下午04:46:14
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param parentVO 父类信息容器
	 */
	protected static void setBeanRegister(
			BeanFactory bfm,BeanVO beanVO,BeanVO parentVO) {
		if(parentVO==null) {
			return;
		}
		if(beanVO.beanRegister==null 
				|| beanVO.beanRegister.length()<1) { 
			beanVO.beanRegister = parentVO.beanRegister;
			//如果没获取到，则再尝试从父类的父类获取
			setBeanRegister(bfm,beanVO,getParentVO(bfm,parentVO));
		}
	}
	
	/**
	 * 设置初始化方法名
	 * 刘虻
	 * 2010-9-2 下午04:46:14
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param parentVO 父类信息容器
	 */
	protected static void setInitMethodName(
			BeanFactory bfm,BeanVO beanVO,BeanVO parentVO) {
		if(parentVO==null) {
			return;
		}
		if(beanVO.initMethodName==null 
				|| beanVO.initMethodName.length()<1) { 
			beanVO.initMethodName = parentVO.initMethodName;
			//如果没获取到，则再尝试从父类的父类获取
			setInitMethodName(bfm,beanVO,getParentVO(bfm,parentVO));
		}
	}
	
	
	/**
	 * 设置失效调用方法名
	 * 刘虻
	 * 2010-9-2 下午04:46:14
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * @param parentVO 父类信息容器
	 */
	protected static void setDestroyMethodName(
			BeanFactory bfm,BeanVO beanVO,BeanVO parentVO) {
		if(parentVO==null) {
			return;
		}
		if(beanVO.destoryMethodName==null 
				|| beanVO.destoryMethodName.length()<1) { 
			beanVO.destoryMethodName = parentVO.destoryMethodName;
			//如果没获取到，则再尝试从父类的父类获取
			setDestroyMethodName(bfm,beanVO,getParentVO(bfm,parentVO));
		}
	}
	
	/**
	 * 执行初始化
	 * 刘虻
	 * 2010-4-22 下午03:17:17
	 * @throws Exception 执行发生异常
	 */
	public static void init(
			BeanFactory bfm,BeanVO beanVO) throws Exception {
		if(beanVO.isParent) {
			//抽象类不用初始化
			beanVO.state = IBeanFactory.STATE_RUN_STANDBY;
			return;
		}
		//这段程序需要放在判断是否为抽象父类的上面，因为抽象父类也允许存在抽象父类
		//父类信息容器
		BeanVO parentVO = getParentVO(bfm,beanVO);
		if(parentVO!=null) {
			//设置父类的参数配置信息
			addParentPropInfo(bfm,beanVO,parentVO);
			//设置启动后调用的方法名
			setAfterStartMethodName(bfm,beanVO,parentVO);
			//设置注册父类主键
			setBeanRegister(bfm,beanVO,parentVO);
			//设置初始化方法名
			setInitMethodName(bfm,beanVO,parentVO);
			//设置失效调用方法名
			setDestroyMethodName(bfm,beanVO,parentVO);
		}
		//注册类的方法放到了commitBean方法中执行，防止一部分类没初始化完毕导致锁死
		getPropInfo(beanVO); //设置类参数信息，只设置一次
		beanVO.state = IBeanFactory.STATE_RUN_STANDBY;
		if(beanVO.singletonLevel>0) {
			getBean(bfm,beanVO);
			executeInitMethod(bfm,beanVO);
		}
	}
	
	
	

	/**
	 * 执行启动类加载器完毕后调用的方法
	 * @param bfm 类工厂
	 * @param beanVO 类信息容器
	 * 刘虻
	 * 2010-4-30 下午12:50:14
	 */
	public static void executeAfterStartMethod(BeanFactory bfm,BeanVO beanVO) {
		//获取执行启动类加载器完毕后调用的方法名
		if (beanVO.afterStartMethodName==null 
				|| beanVO.afterStartMethodName.length()<1) {
			if(bfm.isDebugMode()) {
				beanVO.state = IBeanFactory.STATE_RUN_TEST;
			}else {
				beanVO.state = IBeanFactory.STATE_RUN_MORMAL;
			}
			return;
		}
		//由于有些脚本的初始化后执行方法非常耗时，如果不采用线程方式执行
		//会阻塞进程导致系统迟迟无法进入正常工作状态，甚至导致系统无法启动完毕
		//注意：如果某各类在初始化时需要调用另外的类，那么那个类就不能用初始化后
		//执行的方法了，而是初始化方法，但是在初始化方法中要绝对避免阻塞，通常避免
		//在初始化时调用另外的需要初始化的程序，也避免使用初始化方法，提倡使用初始化后
		//执行的方法
	    (new Thread() {
	    	
        	private BeanFactory bfm   = null;  //类工厂（类加载器）
        	private BeanVO      beanVO = null;            //信息类
        	
        	/**
        	 * 设置线程中需要用到的参数
        	 * @param bfm           类工厂（类加载器）
        	 * @param beanVO        信息类
        	 * @return              当前线程类实例
        	 * 2017年11月12日
        	 * @author MBG
        	 */
        	public Thread setInfo(BeanFactory bfm,BeanVO beanVO) {
        		this.bfm    = bfm;
        		this.beanVO = beanVO;
        		return this;
        	}
        	
        	/**
        	 * 覆盖方法
        	 */
        	@Override
            public void run() {
        		//类实例
        		Object bean = null;
        		try {
        			bean = getBean(bfm,beanVO);
        		}catch(Exception e) {
        			beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
        			beanVO.errorMsg = "Get Bean [" + beanVO.id
        					+ "] After Start Method ["
        					+beanVO.afterStartMethodName+"] Exception:"
        					+DebugUtil.getExceptionString(e);
        			beanVO.state = IBeanFactory.STATE_EXCEPTION_INFO;
        			beanVO.error = true;
        			return;
        		}
        		// 构建将要调用的方法对象
        		Method runMethod = null;
        		try {
        			runMethod = bean.getClass().getMethod(
        					beanVO.afterStartMethodName);
        		} catch (Exception e) {
        			beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
        			beanVO.errorMsg = "Get Bean [" + beanVO.id
        					+ "] After Start Method ["
        					+beanVO.afterStartMethodName+"] Exception:"
        					+DebugUtil.getExceptionString(e);
        			beanVO.state = IBeanFactory.STATE_EXCEPTION_INFO;
        			beanVO.error = true;
        			return;
        		}
        		// 调用设置参数方法
        		bfm.getLog().startLog("***********Begin Invoke AfterStartMethod:["+beanVO.afterStartMethodName+"] BeanID:["+beanVO.id+"] ClassPath:["+beanVO.classPath+"]");
        		try {
        			runMethod.invoke(bean);
        		} catch (Exception e) {
        			e.printStackTrace();
        			bfm.getLog().error("Bean:["+beanVO.id+"] ["+beanVO.classPath+"] RunMethod:["+beanVO.afterStartMethodName+"] Exception",e);
        			beanVO.state = IBeanFactory.STATE_REST_EXCEPTION;
        			beanVO.errorMsg = "Invoke Bean [" + beanVO.id
        					+ "] After Start Method ["
        					+beanVO.afterStartMethodName
        					+"] Exception:"+DebugUtil.getExceptionString(e);
        			beanVO.error = true;
        		}
        		if(bfm.isDebugMode()) {
        			beanVO.state = IBeanFactory.STATE_RUN_TEST;
        		}else {
        			beanVO.state = IBeanFactory.STATE_RUN_MORMAL;
        		}
        	}
        }).setInfo(bfm,beanVO).start();
	}
}
