/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.instanceb;

import com.jphenix.kernel.objectloader.interfaceclass.IBean;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 根类
 * @author 刘虻
 * 2010-2-1 下午04:06:51
 */
@ClassInfo({"2017-12-01 10:28","根类"})
public class BaseBean implements IBean {

	//类加载器
	protected IBeanFactory _beanFactory = null;
	
	protected String _beanID = null;				//类主键
		
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public BaseBean() {
		super();
	}

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:06:51
	 */
	@Override
    public IBeanFactory getBeanFactory() {
		return _beanFactory;
	}
	

	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-2-1 下午04:06:51
	 */
	@Override
    public void setBeanFactory(IBeanFactory beanFactory) {
		this._beanFactory = beanFactory;
	}
	
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-5-5 下午04:25:35
	 */
	@Override
    public String getBeanID() {
		if(_beanID==null) {
			_beanID = "";
		}
		return _beanID;
	}
	
	
	/**
	 * 返回 WEB-INF 文件夹的绝对路径
	 * @return WEB-INF 文件夹的绝对路径
	 * 2017年12月1日
	 * @author MBG
	 */
	@Override
    public String webInfPath() {
		return _beanFactory.getWebInfPath();
	}
	
	/**
	 * 返回是否存在扩展包文件路径
	 * 
	 * 有时候，为了要支持某一个功能，要加载大量jar包，比如poi
	 * 如果都放到lib文件夹中，就显得这个文件夹中文件太多太杂，不好管理
	 * 于是就支持了可以将支持某一个功能的包，放入lib的子文件夹中
	 * 
	 * @param extLibPath 相对于 /WEB-INF/lib 的文件路径
	 *                    比如 /WEB-INF/lib/poi  则传入 poi
	 *                    比如 /WEB-INF/lib/ms/ws 则传入 ms/ws
	 * @return           true存在
	 * 2018年3月30日
	 * @author MBG
	 */
	public boolean hasExtLib(String extLibPath) {
		return _beanFactory.hasExtLib(extLibPath);
	}
}
