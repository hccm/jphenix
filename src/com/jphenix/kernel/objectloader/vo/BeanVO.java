/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.vo;

import com.jphenix.standard.docs.ClassInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 类信息容器
 * 
 * 原来类信息的处理方法也是放在了BeanVO中，但因为需要加载到内存中的
 * BeanVO数量很大，所以需要精简BeanVO，避免内存占用过多
 * 
 * 2018-08-06 整理了代码格式
 * 
 * @author 刘虻
 * 2010-2-3 上午09:16:01
 */
@ClassInfo({"2018-08-06 16:19","类信息容器"})
public class BeanVO {

	public String 
						id = null					//类主键
						,parentID = null			//父类主键
						,beanRegister = null		//调用注册类主键
						,beanVer = null				//类版本信息
						,classTitle = null				//类标题
						,sourceInfo = null			//来源信息
						,clientBeanVer = null		//客户端类版本信息
						,errorMsg = null			//错误信息
						,errorCode = null			//错误代码 因为错误信息涉及到多语言，这里用错误代码设置
						,classPath = null			//类路径
						,clientBeanClassPath = null //客户端类路径
						,afterStartMethodName = null//类加载器启动完毕后调用的方法
						,initMethodName = null		//初始化方法
						,destoryMethodName = null;	//终止方法
	
	public ArrayList<String> propNames   = new ArrayList<String>();      //属性名序列
	public ArrayList<Object> propClasses = new ArrayList<Object>();      //属性类型序列
	public ArrayList<Object> propObjects = new ArrayList<Object>();		 //属性值类序列
	public ArrayList<Boolean> propIsVars = new ArrayList<Boolean>();     //属性名是否为变量名序列
	
	public HashMap<String,ArrayList<Class<?>>> propMethodMap = new HashMap<String,ArrayList<Class<?>>>(); 		//该类方法容器
	
	public boolean
						registOK = false			//在调用注册类主键不为空的情况下，是否执行注册完毕
						,isParent = false			//是否为抽象父类
						,error	  = false			//是否存在错误
						,isSetPropInfo = false		//是否已经设置参数信息
						//该类是否存在对应的抽象父类配置信息
						//默认为真，在首次初始化是判断是否存在抽象父类信息
						,hasParent = true
						,dependLoadOK = false //依赖类是否加载完毕
						
		//在通过类路径名获取配置文件路径（类名必须和配置文件名相同）时
		//是否加载了路径 返回 真
		//通过配置文件直接获取到类路径时 返回假
		,noSetClassPath = true; //获取在初始化配置文件时，是否加载了类路径

	public ClassLoader classLoader = null; //专用类加载器
	public Class<?> beanClass = null;				//类
	public Object bean = null;					//常驻内存类实例
	public BeanVO parentBeanVO = null;			//父类信息容器
	public long invokeCount = 0;					//调用次数
	public int state = 0;						//状态信息
	public int singletonLevel = 0; //常驻内存优先等级  0不常驻内存   1~无穷大    优先级有低到高
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public BeanVO() {
		super();
	}
	
	/**
	 * 覆盖方法
	 * 刘虻
	 * 2010-9-2 上午11:04:13
	 */
	@Override
    public String toString() {
		return 
			(new StringBuffer())
				.append("ID:[")
				.append(id)
				.append("]\n")
				.append("beanRegister:[")
				.append(beanRegister)
				.append("] State:[")
				.append(state)
				.append("] ErrorMsg:[")
				.append(errorMsg)
				.append("] ErrorCode:[")
				.append(errorCode)
				.append("] SingletonLevel:[")
				.append(singletonLevel)
				.append("] IsParent:[")
				.append(isParent)
				.append("] Ver:[")
				.append(beanVer)
				.append("] Title:[ ")
				.append(classTitle)
				.append("] ClassPath:[")
				.append(classPath)
				.append("]")
				.toString();
	}
}
