/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.vo;

import com.jphenix.standard.docs.ClassInfo;



/**
 * 待加载类信息容器
 * @author 刘虻
 * 2010-2-11 下午12:58:04
 */
@ClassInfo({"2014-06-04 19:18","待加载类信息容器"})
public class LoadBeanVO {
	
	public String
						beanID = null			//类主键
						,errorMsg = null;		//错误信息
	
	public boolean isError = false;			//是否出错
	
	public Class<?> interfaceCls = null;		//待加载类接口
}
