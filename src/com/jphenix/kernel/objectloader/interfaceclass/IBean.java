/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 类加载器管理的类接口
 * @author 刘虻
 * 2010-1-27 下午05:40:36
 */
@ClassInfo({"2017-12-01 10:28","类加载器管理的类接口"})
public interface IBean {
	
	
	/**
	 * 获取当前的类加载器
	 * 刘虻
	 * 2010-1-27 下午03:51:35
	 * @return 当前的类加载器
	 */
	IBeanFactory getBeanFactory();
	
	/**
	 * 设置当前的类加载器
	 * 刘虻
	 * 2010-1-27 下午03:51:46
	 * @param beanFactory 当前的类加载器
	 */
	void setBeanFactory(IBeanFactory beanFactory);
	
	/**
	 * 获取当前类的类主键
	 * 刘虻
	 * 2010-5-5 下午04:23:59
	 * @return 当前类的类主键
	 */
	String getBeanID();
	
	/**
	 * 返回 WEB-INF 文件夹的绝对路径
	 * @return WEB-INF 文件夹的绝对路径
	 * 2017年12月1日
	 * @author MBG
	 */
	String webInfPath();
}
