/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.interfaceclass;

import com.jphenix.kernel.objectloader.vo.BeanVO;
import com.jphenix.standard.docs.ClassInfo;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * 类加载器管理接口
 * 
 * 2018-09-05 将getConfProp()方法迁移到了IBeanFactory接口中。并去掉了setConfProp()方法
 * 2018-09-13 简化了整个架构中日志初始化步骤
 * 
 * @author 刘虻
 * 2010-2-9 上午11:09:54
 */
@ClassInfo({"2018-09-13 13:24","类加载器管理接口"})
public interface IBeanFactoryManager extends IBeanFactory {

	
	/**
	 * 添加子类加载器
	 * 刘虻
	 * 2010-2-9 上午11:23:58
	 * @param beanFactory 子类加载器
	 */
	void addChildBeanFactory(IBeanFactory beanFactory);
	
	/**
	 * 获取根类加载器
	 * 刘虻
	 * 2010-8-12 上午10:32:52
	 * @return 根类加载器
	 */
	IBeanFactoryManager getRoot();
	
	/**
	 * 获取父类加载器
	 * 马宝刚
	 * 2009-11-18 下午04:27:13
	 * @return 父类加载器
	 */
	IBeanFactoryManager getParent();
	
	
	/**
	 * 设置父类加载器
	 * 刘虻
	 * 2010-2-2 下午05:14:17
	 * @param beanFactory 父类加载器
	 */
	void setParent(IBeanFactoryManager beanFactory);
	
	
	/**
	 * 设置远程类工厂管理类
	 * 刘虻
	 * 2010-3-1 下午04:34:32
	 * @param remoteBeanFactory 远程类工厂管理类实例
	 */
	void setRemoteBeanFactory(IRemoteBeanFactory remoteBeanFactory);
	
	
	/**
	 * 获取远程类工厂管理类
	 * 刘虻
	 * 2010-3-1 下午04:36:09
	 * @return 远程类工厂管理类
	 */
	IRemoteBeanFactory getRemoteBeanFactory();
	
	/**
	 * 设置已经初始化好的，常驻内存的类实例
	 * 刘虻
	 * 2010-4-2 下午01:00:41
	 * @param beanID 类主键
	 * @param bean 类实例
	 * @param destroyMethod 注销方法
	 */
	void setSingletonObject(
			String beanID,Object bean,String destroyMethod);
	
	/**
	 * 获取子类工厂序列
	 * element:value:IBeanFactoryManager
	 * 刘虻
	 * 2010-4-21 上午11:15:13
	 * @return 子类工厂序列
	 */
	List<IBeanFactory> getChildBeanFactoryList();
	
	/**
	 * 获取有效的类信息容器
	 * 刘虻
	 * 2010-2-3 下午01:34:34
	 * @return 有效的类信息容器
	 */
	Map<String,BeanVO> getBeanVOMap();
	
	
	/**
	 * 从当前类加载器中获取类信息容器
	 * 
	 * 获取类信息业务逻辑
	 * 
	 * 如果当前类加载器没有，就从父类加载器中尝试获取
	 * 不去子类加载器或者其它类加载器中获取，因为需要保持隔离
	 * 
	 * 
	 * 刘虻
	 * 2010-4-2 下午04:09:46
	 * @param beanID 类主键
	 * @param loader 加载者
	 * @throws Exception 执行发生异常
	 * @return 类信息容器
	 */
    @Override
    BeanVO getBeanVO(String beanID,Object loader) throws Exception;
	
	
	/**
	 * 获取一个新构建的子类加载器
	 * 刘虻
	 * 2010-4-22 下午05:57:53
	 * @param factoryBasePath 工厂类根路径
	 * @return 新的子类加载器
	 * @throws Exception 执行发生异常
	 */
	IBeanFactoryManager getNewChildBeanFactory(String factoryBasePath) throws Exception;
	
	
	/**
	 * 获取内部的类加载器
	 * 刘虻
	 * 2010-5-7 下午02:03:40
	 * @return 内部的类加载器
	 */
	ClassLoader getClassLoader();
	
	
    /**
     * 设置配置文件路径
     * 刘虻
     * 2010-2-2 下午04:09:39
     * @param configFilePath 配置文件路径
     */
    void setConfigFilePath(String configFilePath);

    
	/**
	 * 设置配置文件根路径
	 * 刘虻
	 * 2010-9-10 下午04:11:21
	 * @param webInfPath WEB-INF路径
	 */
	void setWebInfPath(String webInfPath);
	
	/**
	 * 加入新的资源文件
	 * 
	 * 热替换类时，需要将管理类中的类加载器变成新的，否则构建类时，因为旧的类
	 * 已经加载过，会抛重复加载的异常
	 * 
	 * 刘虻
	 * 2010-10-29 下午03:49:38
	 * @param basePath 类路径
	 * @param file 类文件对象
	 */
	void addNewSource(String basePath,File file);
	
	
	/**
	 * 执行启动后执行的方法
	 * @param beanIdList 指定主键序列
	 * 刘虻
	 * 2010-11-5 上午11:08:10
	 * @throws Exception 执行发生异常
	 */
	void doBeansAfterStartMethod(List<String> beanIdList) throws Exception;
	
	
	/**
	 * 执行注册类
	 * @param beanIdList 指定主键序列
	 * 刘虻
	 * 2010-11-5 上午11:07:57
	 * @throws Exception 执行发生异常
	 */
	void doRegistBeans(List<String> beanIdList) throws Exception;
	
	
	/**
	 * @deprecated 框架内部调用方法
	 * 注意：该方法返回的类实例通常为需要调用初始化方法，常驻内存的服务
	 *       ，由于在启动时可能会遇到交叉调用，比如类实例A在调用初始化方法
	 *       内部加载了类实例B，但是类实例B还在调用初始化方法，没有结束初始化
	 *       就会导致锁死
	 * @param cls         目标类型
	 * @param loader      调用者
	 * @return            目标类实例
	 * @throws Exception  异常
	 * 2016年12月14日
	 * @author MBG
	 */
	Object getNativeObject(Class<?> cls,Object loader) throws Exception;
}
