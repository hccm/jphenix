/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 类注册器接口
 * 
 * 如果类配置信息中指定了类注册器，则在类初始化完毕后
 * 调用指定接口方法，将类实例传入方法
 * 
 * @author 刘虻
 * 2010-4-22 上午10:11:18
 */
@ClassInfo({"2014-06-04 19:09","类注册器接口"})
public interface IBeanRegister {
	
	/**
	 * 执行注册
	 * 
	 * 设计时，原本加入了当前类工厂作为参数，
	 * 但后来发现，如果是递归注册（注册类中也声明了其它注册类）时
	 * 无法将注册类对应的类加载器传入
	 * 
	 * 刘虻
	 * 2010-4-22 上午10:18:37
	 * @param bean 待注册的类
	 * @return 是否注册成功
	 */
	boolean regist(Object bean);
	
	
	/**
	 * 执行反注册
	 * @param bean 需要做反注册的类
	 * 2014年7月31日
	 * @author 马宝刚
	 */
	void unRegist(Object bean);
}
