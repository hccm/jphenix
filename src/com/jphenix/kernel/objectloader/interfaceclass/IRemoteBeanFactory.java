/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.interfaceclass;

import com.jphenix.kernel.objectloader.vo.BeanVO;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 远程类工厂
 * @author 刘虻
 * 2010-3-1 下午03:16:37
 */
@ClassInfo({"2014-06-04 19:10","远程类工厂"})
public interface IRemoteBeanFactory {

	/**
	 * 获取远程的类实例
	 * 刘虻
	 * 2010-3-1 下午03:31:39
	 * @param beanVO 类信息容器
	 * @return 远程类实例
	 */
	Object getBean(BeanVO beanVO);
}
