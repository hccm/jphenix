/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年9月18日
 * V4.0
 */
package com.jphenix.kernel.objectloader.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 被注册子类接口
 * @author 马宝刚
 * 2015年9月18日
 */
@ClassInfo({"2014-06-04 19:09","被注册子类接口"})
public interface IBeanRegisterChild {

    /**
     * 设置注册管理类实例
     * @param register 注册管理类实例
     * 2015年9月18日
     * @author 马宝刚
     */
    void setRegister(Object register);
    
    /**
     * 注册后执行的方法
     * 2015年9月18日
     * @author 马宝刚
     */
    void afterRegister();
    
    
    /**
     * 取消注册前执行的方法
     * 2015年9月18日
     * @author 马宝刚
     */
    void beforeUnRegister();
}
