/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.exception;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 类加载器异常
 * @author 刘虻
 * 2010-2-3 上午11:17:06
 */
@ClassInfo({"2014-06-04 17:20","类加载器异常"})
public class BeanException extends Exception {

	/**
	 * 串行标识
	 */
	private static final long serialVersionUID = -4689072154938993053L;

	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public BeanException() {
		super();
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param message
	 */
	public BeanException(String message) {
		super(message);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param cause
	 */
	public BeanException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构造函数
	 * @author 刘虻
	 * @param message
	 * @param cause
	 */
	public BeanException(String message, Throwable cause) {
		super(message, cause);
	}
}
