/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年2月22日
 * V4.0
 */
package com.jphenix.kernel.objectloader.util;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 压缩包内部元素对象容器
 * @author MBG
 * 2017年2月22日
 */
@ClassInfo({"2017-02-22 14:15","压缩包内部元素对象容器"})
public class JarResourceVO {
	
	/**
	 * jar文件对象主键
	 */
	public ZipFile jarFile = null;
	
	/**
	 * 文件对象元素
	 */
	public ZipEntry entrie = null;
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public JarResourceVO(ZipFile jarFile,ZipEntry entrie) {
		super();
		this.jarFile = jarFile;
		this.entrie = entrie;
	}
}
