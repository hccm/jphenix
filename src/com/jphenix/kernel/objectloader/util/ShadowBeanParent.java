/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader.util;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 接口影子类父类
 * @author 刘虻
 * 2010-4-29 上午09:18:26
 */
@ClassInfo({"2014-06-04 19:15","接口影子类父类"})
public abstract class ShadowBeanParent {

	protected Object _bean = null; //获取到的类实例
	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ShadowBeanParent() {
		super();
	}

	/**
	 * 设置类实例
	 * 刘虻
	 * 2010-4-29 上午09:23:18
	 * @param obj 类实例
	 */
	public void _setObject(Object obj) throws Exception {
		_bean = obj;
		_init(); //执行初始化
	}
	
	/**
	 * 初始化检测接口方法是否匹配
	 * 刘虻
	 * 2010-4-29 上午10:23:59
	 * @throws Exception 执行发生异常
	 */
	public abstract void _init() throws Exception;
}
