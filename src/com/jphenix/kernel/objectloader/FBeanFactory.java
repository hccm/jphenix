/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectloader;

import java.util.List;

import com.jphenix.kernel.objectloader.instanceb.BeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactoryManager;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 静态获取类加载器
 * @author 刘虻
 * 2010-1-27 下午04:14:28
 */
@ClassInfo({"2014-06-04 19:18","静态获取类加载器"})
public class FBeanFactory {
	
	//根类工厂 第一个设置到容器中的类工厂为根类工厂
	protected static IBeanFactory rootBeanFactory = null;
	
	/**
	 * 获取指定的类加载器
	 * 刘虻
	 * 2010-1-27 下午04:21:21
	 * @param factoryName 类加载器名
	 * @return 类加载器实例
	 */
	public static IBeanFactory getBeanFactory() {
		return rootBeanFactory;
	}
	
	
	/**
	 * 获取新构造的类工厂
	 * @param webInfPath WEB-INF 文件夹的绝对路径
	 * @param needInit 是否需要初始化
	 * @param isRoot 是否作为根类工厂
	 * @return 新构造的类工厂
	 * 2014-3-5
	 * @author mbg
	 */
	public static IBeanFactory getNewBeanFactory(String webInfPath,boolean needInit,boolean isRoot) {
	    //构建返回值
		IBeanFactory bf = new BeanFactory();
		((IBeanFactoryManager)bf).setWebInfPath(webInfPath);
		if(needInit) {
			try {
				bf.init(false);
			}catch(Exception e) {}
		}
		if(rootBeanFactory==null || isRoot) {
		    rootBeanFactory = bf;
		}else {
		    ((IBeanFactoryManager)rootBeanFactory).addChildBeanFactory(bf);
		}
		return bf;
	}
	
    /**
     * 获取类加载器
     * 刘虻
     * 2010-5-24 下午03:47:41
     * @param bf 类工厂
     * @param configFilePath 配置文件路劲个（相对WEB-INF路径）
     * @param webInfBasePath WEB-INF绝对路径
     * @param classPathList 需要放入类工厂的类路径
     * @return 类工厂实例
     * @throws Exception 执行发生异常
     */
    public synchronized static void initBeanFactory(
    		IBeanFactory bf,String configFilePath,String webInfBasePath,List<String> classPathList) throws Exception {
        if(bf==null) {
          return;
        }
        //不用设置类根路径，采用默认的类根路径
        try {
        	((IBeanFactoryManager)bf).setWebInfPath(webInfBasePath);
            ((IBeanFactoryManager)bf).setConfigFilePath(configFilePath);
            bf.init(false);
        }catch(Exception e) {
            e.printStackTrace();
        }
        if(classPathList!=null) {
        	for(String path:classPathList) {
        		bf.addClassPath(path);
        	}
        }
        if(rootBeanFactory==null) {
            rootBeanFactory = bf;
        }else if(!rootBeanFactory.equals(bf)){
            ((IBeanFactoryManager)rootBeanFactory).addChildBeanFactory(bf);
        }
        bf.commitInitBean(null); //提交类
    }
	
	
	/**
	 * 设置类工厂到容器中
	 * @author 刘虻
	 * 2008-2-20下午09:05:39
	 * @param factoryName 工厂名
	 * @param beanFactory 类工厂
	 */
	public static void setBeanFactory(IBeanFactory beanFactory) {
		if (rootBeanFactory==null) {
			rootBeanFactory = beanFactory;
		}else {
		    ((IBeanFactoryManager)rootBeanFactory).addChildBeanFactory(beanFactory);
		}
	}
	
	
	/**
     * 构建类加载器
     * 刘虻
     * 2011-9-27 下午04:59:40
     * @param configFilePath 配置文件路径
     * @param webInfPath     WEB-INF路径
     * @return 类加载器
     * @throws Exception 执行发生异常
     */
    public static synchronized IBeanFactory newInstance(
                                        String configFilePath,String webInfPath) throws Exception {
        //构建返回值
        IBeanFactory bf = new BeanFactory();
        //不用设置类根路径，采用默认的类根路径
        try {
        	((IBeanFactoryManager)bf).setWebInfPath(webInfPath);
            ((IBeanFactoryManager)bf).setConfigFilePath(configFilePath);
            bf.init(false);
        }catch(Exception e) {
            e.printStackTrace();
        }
        if(rootBeanFactory==null) {
            rootBeanFactory = bf;
        }else {
            ((IBeanFactoryManager)rootBeanFactory).addChildBeanFactory(bf);
        }
        bf.commitInitBean(null); //提交类
        return bf;
    }
    
    
	
	/**
     * 构建类加载器
     * 刘虻
     * 2011-9-27 下午04:59:40
     * @param configFilePath 配置文件路径
     * @param webInfPath     WEB-INF路径
     * @param classPathList  需要类加载器在初始化前加载的类文件全路径序列
     * @return 类加载器
     * @throws Exception 执行发生异常
     */
    public static synchronized IBeanFactory newInstance(
               String configFilePath,String webInfPath,List<String> classPathList) throws Exception {
        //构建返回值
        IBeanFactory bf = new BeanFactory();
        if(classPathList!=null) {
        	for(String path:classPathList) {
        		bf.addClassPath(path);
        	}
        }
        //不用设置类根路径，采用默认的类根路径
        try {
        	((IBeanFactoryManager)bf).setWebInfPath(webInfPath);
            ((IBeanFactoryManager)bf).setConfigFilePath(configFilePath);
            bf.init(false);
        }catch(Exception e) {
            e.printStackTrace();
        }
        if(rootBeanFactory==null) {
            rootBeanFactory = bf;
        }else {
            ((IBeanFactoryManager)rootBeanFactory).addChildBeanFactory(bf);
        }
        bf.commitInitBean(null); //提交类
        return bf;
    }
}
