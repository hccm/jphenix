/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.classloader;

import java.net.URL;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 资源信息元素类
 * 
 * 为什么不设置get set 方法，而用直接变量方式？
 * 
 * 因为该类元素数量有很多，都需要加载到内存中，如果在这个类中写方法
 * 则会消耗大量内存
 * 
 * 2020-06-23 修改了toUrl方法拼装的字符串，从原来的 jar:file:// 改为 jar:file:/
 * 
 * @author 刘虻
 * 2010-8-30 下午08:54:06
 */
@ClassInfo({"2014-06-04 19:14","资源信息元素类"})
public class ResourceEntry {

	/**
	 * 资源是否在Jar文件中
	 */
	public boolean inJar = false;
	
	/**
	 * 上次修改时间，如果inJar为true，则为jar的修改时间
	 */
	public long lastModified = -1;
	
	/**
	 * 已经加载的类
	 */
	public Class<?> loadedClass = null;
	
	/**
	 * 如果inJar为true，则该值为jar文件来源
	 */
	public String codeBase = null;
	
	/**
	 * 资源相对路径
	 */
	public String sourceSubPath = null;
	
	/**
	 * 资源全路径
	 */
	public String source = null;

	
	/**
	 * 构造函数
	 * @author 刘虻
	 */
	public ResourceEntry() {
		super();
	}
	
	/**
	 * 返回资源路径URL
	 * @return 资源路径URL
	 * @throws Exception 异常
	 * 2019年1月17日
	 * @author MBG
	 */
	public URL toUrl() throws Exception {
		if(inJar) {
			//jar:file:/C:/Users/mbg/AppData/Local/Temp/servlet-api.jar!/javax/servlet/LocalStrings.properties
			//原本写法是这样的： "jar:file:/"+codeBase+"!"+sourceSubPath
			//拼出来的url是这样的：jar:file://
			//这种写法在windows上是没问题的，但是在linux中就不认了。于是改为现在这种情况，将//改为/ 
			//然后测试在windows中也正常 2020-06-23 mbg
			return new URL("jar:file:"+codeBase+"!"+sourceSubPath);
		}else {
			return new URL(source);
		}
	}
}
