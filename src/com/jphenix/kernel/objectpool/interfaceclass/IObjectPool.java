/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectpool.interfaceclass;


import java.util.List;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 增强对象池管理类
 * 根据objectpool的经验，重新编写的代码
 * @author 刘虻
 * 2006-8-12  16:51:06
 */
@ClassInfo({"2014-06-04 19:37","增强对象池管理类"})
public interface IObjectPool {


	/**
	 * 空闲超时
	 */
	String TAG_OBJECT_POOL_NOT_IN_USE_TILE_OUT = "notUseTimeOut";
	

    /**
     * 释放池元素
     * @author 刘虻
     * @param element 池元素
     * 2006-8-13  12:52:05
     */
	void freeElement(IObjectElement element);
	
    /**
     * 关闭池元素
     * @author 刘虻
     * @param element 池元素
     * 2006-8-13  13:47:45
     */
	void closeElement(IObjectElement element);


    /**
     * 获得对象实例
     * @author 刘虻
     * @param type 对象类型 
     * @param pk 对象主键
     * @return 对象实例
     * @throws Exception 执行发生异常
     * 2006-8-12  17:06:51
     */
    IObjectElement getObjectInstance(String type, String pk) throws Exception;

    /**
     * 启动池
     * @author 刘虻
     * 2006-8-13  14:23:28
     */
    void startPool();

    /**
     * 终止元素
     * @author 刘虻
     * 2006-8-13  13:48:42
     */
    void terminat();


    /**
     * 获取所有类型序列
     * @author 刘虻
     * 2007-5-15下午07:41:55
     * @return 所有类型序列
     */
    List<String> getAllTypeList();
    
    
    /**
     * 获取指定类型容器中，对象的数量
     * @author 刘虻
     * 2007-5-15下午07:43:47
     * @param type 类型
     * @return 对象的数量
     */
    int getObjectCountByType(String type);

    
    /**
     * 强制关闭一种类型的池元素 pk为空时关闭该类型所有元素
     * @author 刘虻
     * 2006-9-8下午04:42:39
     * @param type 元素类型
     * @param pk 元素主键
     */
    void closeElement(String type,String pk);
    
    /**
     * 设置类实例
     * @author 刘虻
     * 2007-5-14下午12:51:28
     * @param objectElement 将要放入容器的类实例
     */
    void setObjectInstance(IObjectElement objectElement);
    
    
    /**
     * 清除指定类型下的所有元素
     * @author 刘虻
     * 2007-5-14下午01:49:00
     * @param type 指定类型
     */
    void clearByType(String type);
    
    /**
     * 清除所有内容
     * @author 刘虻
     * 2007-5-14下午01:47:04
     * @return 当前类实例
     */
    void clearAllObject();
	
	/**
	 * 获取类名 用来标出线程的名字
	 * @author 刘虻
	 * 2007-7-5下午12:24:31
	 * @return 类名
	 */
	String getClassName();
	
	
	/**
	 * 判断是否存在指定类实例
	 * 刘虻
	 * 2010-5-6 下午01:05:12
	 * @param type 指定类型
	 * @param pk 指定主键
	 * @return true存在
	 */
	boolean hasInstance(String type,String pk);
	
	
	/**
	 * 判断池中是否有该类型
	 * 刘虻
	 * 2010-5-6 下午01:04:02
	 * @param type 类型值
	 * @return true存在指定类型
	 */
	boolean hasType(String type);
}
