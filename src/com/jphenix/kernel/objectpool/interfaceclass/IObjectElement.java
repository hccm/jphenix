/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectpool.interfaceclass;

import com.jphenix.standard.docs.ClassInfo;


/**
 * 增强对象池元素类
 * 根据开发objectElement的经验，重新编写了代码
 * @author 刘虻
 * 2006-8-12  16:53:30
 */
@ClassInfo({"2014-06-04 19:36","增强对象池元素类"})
public interface IObjectElement {
    
    /**
     * 获取空闲间隔时间
     * @author 刘虻
     * @return 空闲间隔时间  毫秒
     * 2006-8-12  17:55:05
     */
    long getFreeTimeOut();

    /**
     * 获取最大类实例并发数
     * @author 刘虻
     * @return 最大类实例并发数
     * 2006-8-12  17:56:03
     */
    long getMaxObjectCount();
    

    /**
     * 设置占用间隔时间
     * @author 刘虻
     * @return 占用间隔时间 毫秒
     * 2006-8-12  17:56:48
     */
    long getUseTimeOut();
    
    
    /**
     * 刷新动作时间
     * @author 刘虻
     * @return 返回当前类实例
     * 2006-8-12  18:03:15
     */
    void refreshAction();

    
    /**
     * 获取元素主键
     * @author 刘虻
     * @return 元素主键
     * 2006-8-12  18:08:43
     */
    String getPK();
    

    /**
     * 设置元素主键
     * 刘虻
     * 2010-5-4 下午01:57:46
     * @param pk 元素主键
     */
    void setPK(String pk);
    
    /**
     * 获取元素类型
     * @author 刘虻
     * @return 元素类型
     * 2006-8-12  18:09:07
     */
    String getType();
    
    /**
     * 设置类型
     * 刘虻
     * 2010-5-6 下午12:42:54
     * @param type 类型
     */
    void setType(String type);
    
    /**
     * 终止元素
     * @author 刘虻
     * 2006-8-13  13:48:42
     */
    void terminat();
    
    
    /**
     * 获取池管理类
     * @author 刘虻
     * @return 池管理类
     * 2006-8-13  16:44:53
     */
    IObjectPool getPool();
    
    
    /**
     * 释放元素实例
     * @author 刘虻
     * @return 返回当前类实例
     * 2006-8-13  16:45:32
     */
    void freeElement();

    /**
     * 获取是否占用
     * @author 刘虻
     * @return 是否占用 true是
     * 2006-8-13  17:31:13
     */
    boolean isPossess();
    
    /**
     * 是否忽略异常（对象占用超时,是否强制关闭true或者抛出异常false）
     * 刘虻
     * 2010-5-4 下午12:49:17
     * @return true是
     */
    boolean isIgnoreException();
    
    /**
     * 是否占用超时
     * 刘虻
     * 2010-5-4 上午11:14:38
     * @return true是
     */
    boolean isPossessTimeOut();
    
    
    /**
     * 是否空闲超时
     * 刘虻
     * 2010-5-4 上午11:14:48
     * @return true是
     */
    boolean isNotUseTimeOut();
    
    
    /**
     * 是否为私有元素
     * @return 是否为私有元素
     * 2015年11月22日
     * @author 马宝刚
     */
    boolean isPrivate();
    
    
    /**
     * 设置是否为私有元素
     * @param isPrivate 是否为私有元素
     * 2015年11月22日
     * @author 马宝刚
     */
    void isPrivate(boolean isPrivate);
    
    
    /**
     * 结束元素
     * 刘虻
     * 2010-5-4 下午01:48:45
     */
    void terminatElement();
}
