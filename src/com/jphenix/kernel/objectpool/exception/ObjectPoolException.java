/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014-06-04
 * V4.0
 */
package com.jphenix.kernel.objectpool.exception;

import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.exceptions.MsgException;

/**
 * 对象池异常处理类
 * @author 刘虻
 * 2006-2-3  9:50:27
 */
@ClassInfo({"2014-06-04 19:27","对象池异常管理类"})
public class ObjectPoolException extends MsgException {

	private static final long serialVersionUID = -8324298935649672271L; //版本编号
    
    /**
     * 构造函数 
     * @param boss 发出异常类
     * @param errorMsgStr 异常信息
     * @author 刘虻
     * 2006-2-3 9:50:28
     */
    public ObjectPoolException(
            Object boss,String errorMsgStr) {
        super(boss,errorMsgStr);
    }
}
