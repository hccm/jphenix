/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年3月31日
 * V4.0
 */
package com.jphenix.kernel.script;

import java.util.List;
import java.util.Map;

import com.jphenix.share.lang.SListMap;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.script.IScriptBean;
import com.jphenix.standard.script.IScriptLoader;
import com.jphenix.standard.script.IScriptNoManager;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 空的脚本加载器
 * 
 * 2018-12-04 增加了通过ScriptVO保存脚本信息
 * 2019-01-16 去掉了获取脚本类加载器(ScriptClassLoader)，改用每个脚本自己的类加载器
 * 2019-01-21 又TM改回去了，重新恢复了获取脚本类加载器方法，不能使用脚本自己的类加载器，具体原因请看： ScriptVO 中的 注意（190121）
 * 2019-01-29 增加了两个空方法，没啥用
 * 2019-06-13 将Map<String,?>改为Map
 * 2019-08-20 增加了接口中的空方法
 * 2019-08-21 增加了接口中的空方法
 * 2019-09-30 增加了接口中的空方法
 * 2019-10-23 增加了接口中的空方法
 * 2019-10-30 增加了接口中的空方法
 * 2019-12-10 增加了接口中的空方法
 * 2020-03-09 为savePathInfo方法增加了返回保存后的文件全路径（无用）
 * 2020-07-20 增加了接口中的空方法
 * 2020-09-09 增加了接口中的空方法
 * 
 * @author MBG
 * 2017年3月31日
 */
@ClassInfo({"2020-09-09 15:00","空的脚本加载器"})
public class NullScriptLoader implements IScriptLoader {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public NullScriptLoader() {
		super();
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO getScriptInfo(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptBean getScript(String scriptID) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, Map inParaMap) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, IActionContext ac,int invokeType) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public <T> T invokeScript(Object invoker, String scriptID) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void initScript(String scriptID) throws Exception {}

	/**
	 * 覆盖方法
	 */
	@Override
	public void destoryScript(String scriptID) throws Exception {}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler getSourceVh(String subPath) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO saveSourceVh(IViewHandler sourceVh) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void saveSource(ScriptVO sVO) {}
	
	/**
	 * 覆盖方法
	 */
	@Override
	public List<FileVO> getPathList(String subPath) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<FileVO> getAllInfoList() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String savePathInfo(FileVO fileVO) {
		return "";
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public FileVO getPathInfo(String subPath) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void rebuildAll() {}

	/**
	 * 覆盖方法
	 */
	@Override
	public void deleteScript(String scriptID) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public void reBuildScript(String scriptID, String subPath) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean hasScript(String scriptId) {
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getBuildSourceContent(String scriptID, boolean outError) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public SListMap<String> getScriptInfoList() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getScriptTitle(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO moveSource(String scriptID, String subPath) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String createAllSourceBak() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getScriptBak(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getBuildSourceContent(IViewHandler sourceVh, boolean outError) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO getScriptVOObject(String scriptID) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<ScriptVO> getForInclude(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<String> getForIncludeScriptIDList(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getScriptHeader(String path) {
		return null;
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
    public String getScriptFooter(String path) {
    	return null;
    }

	/**
	 * 覆盖方法
	 */
	@Override
	public String getSourceBasePath() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getClassBasePath() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getBakBasePath() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<String> getRepeatScriptInfo() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void backupScript(String scriptID) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean isClassOnly() {
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public List<String> getScriptIdList() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public ScriptVO loadSource(String scriptId, String subPath) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String getSn() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptBean getScriptNoException(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptBean script(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setScriptNoManager(IScriptNoManager snm) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptNoManager getScriptNoManager() {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public String compileScriptByPath(String subPath) {
		return null;
	}

	/**
	 * 已废弃，可以使用sVO.loadClass();
	 * 覆盖方法
	 */
	@Override
	public Class<?> getScriptClass(ScriptVO sVO) throws Exception {
		return null;
	}
	
	/**
	 * 覆盖方法
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object invokeRouteScript(
			String groupKey
			,Object invoker
			,String scriptID
			,Map inParaMap
			,int invokeType
			,boolean noReturnValue) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, Map inParaMap, int invokeType)
			throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, int invokeType) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void clearScriptRunError(String id) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setScriptRunError(String id, String ts, String errorMsg) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public SListMap<String> findScript(String scriptID) {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public SListMap<String> findScriptByTitle(String titleKey) {
		return null;
	}
	
	/**
	 * 覆盖方法
	 */
	@Override
	public boolean hasExtLib(String libSubPath) {
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	@SuppressWarnings({"rawtypes"})
	public <T> T invokeScript(Object invoker, String scriptID, Map inParaMap, int invokeType, boolean noReturnValue)
			throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, IActionContext ac, int invokeType, boolean noReturnValue)
			throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public <T> T invokeScript(Object invoker, String scriptID, int invokeType, boolean noReturenValue)
			throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void deleteScript(String scriptID, String message) {}

	/**
	 * 覆盖方法
	 */
	@Override
	public String remoteScriptUpload(Object invoker, String scriptID, String params, List<String> uploadNameList,
			List<String> fileNameList, List<String> filePathList) throws Exception {
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IScriptBean getScriptNoException(String scriptID, Map<String, ?> paraMap) {
		return null;
	}
}
