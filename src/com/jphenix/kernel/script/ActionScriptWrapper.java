/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2019年1月23日
 * V4.0
 */
package com.jphenix.kernel.script;

import java.util.Map;

import com.jphenix.driver.json.Json;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.script.IScriptBean;
import com.jphenix.standard.servlet.IActionBean;
import com.jphenix.standard.servlet.IActionContext;
import com.jphenix.standard.viewhandler.IViewHandler;

/**
 * 包含了扩展包路径的动作脚本类外壳
 * 
 * 2021-03-15 增加了两个父类方法
 * 
 * @author MBG
 * 2019年1月23日
 */
@ClassInfo({"2021-03-15 22:39", "包含了扩展包路径的动作脚本类外壳"})
public class ActionScriptWrapper extends ScriptWrapper implements IActionBean, IScriptBean {

	/**
	 * 构造函数
	 * @author MBG
	 */
	public ActionScriptWrapper(ScriptVO sVO) throws Exception {
		super(sVO);
	}


	/**
	 * 覆盖方法
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getActionMap() {
		try {
			return (Map<String, String>)runMethod("getActionMap",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setModelParameterMap(Map<String, String> parameterMap) {
		try {
			runMethod("setModelParameterMap",new Class[] {Map.class},new Object[] {parameterMap});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler getReVh() {
		try {
			return (IViewHandler)runMethod("getReVh",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler setReVh(IViewHandler vh) {
		try {
			return (IViewHandler)runMethod("setReVh",new Class[] {IViewHandler.class},new Object[] {vh});
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler setReVh(String viewID) throws Exception {
		try {
			return (IViewHandler)runMethod("setReVh",new Class[] {String.class},new Object[] {viewID});
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler newVh() {
		try {
			return (IViewHandler)runMethod("newVh",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler getVh(String nodeID) throws Exception {
		try {
			return (IViewHandler)runMethod("getVh",new Class[] {String.class},new Object[] {nodeID});
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean beforeRunAction() throws Exception {
		try {
			return (Boolean)runMethod("beforeRunAction",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean afterRunAction() throws Exception {
		try {
			return (Boolean)runMethod("afterRunAction",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setActionContext(IActionContext actionContext) {
		try {
			runMethod("setActionContext",new Class[] {IActionContext.class},new Object[] {actionContext});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void errorAction(Exception e) throws Exception {
		try {
			runMethod("errorAction",new Class[] {Exception.class},new Object[] {e});
		}catch(Exception e2) {
			e2.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IActionContext getActionContext() {
		try {
			return (IActionContext)runMethod("getActionContext",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public Json getReJson() {
		try {
			return (Json)runMethod("getReJson",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public IViewHandler getReXml() {
		try {
			return (IViewHandler)runMethod("getReXml",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public int getOutContentType() {
		try {
			return (Integer)runMethod("getOutContentType",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void invokeAction() throws Exception {
		try {
			runMethod("invokeAction",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean isNoReturnInfo() {
		try {
			return (Boolean)runMethod("isNoReturnInfo",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean isOver() {
		try {
			return (Boolean)runMethod("isOver",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void over(boolean isOver) {
		try {
			runMethod("over",new Class[] {boolean.class},new Object[] {isOver});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public boolean loadAllPage() {
		try {
			return (Boolean)runMethod("loadAllPage",new Class[0],new Object[0]);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void buildActionParameter(Map<String, ?> paraMap) throws Exception {
		try {
			runMethod("buildActionParameter",new Class[] {Map.class},new Object[] {paraMap});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
	@Override
	public void setContentType(int contentType) {
		try {
			runMethod("setContentType",new Class[] {int.class},new Object[] {contentType});
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 覆盖方法
	 */
  @Override
  public IViewHandler beforeOutView(IViewHandler vh) {
		try {
			return (IViewHandler)runMethod("beforeOutView",new Class[]{IViewHandler.class},new Object[]{vh});
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
  }

	/**
	 * 覆盖方法
	 */
  @Override
  public Json beforeOutJson(Json json) {
		try {
			return (Json)runMethod("beforeOutJson",new Class[]{Json.class},new Object[]{json});
		}catch(Exception e) {
			e.printStackTrace();
		}
    return null;
  }
}
