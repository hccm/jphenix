/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2017年3月31日
 * V4.0
 */
package com.jphenix.kernel.script;

import com.jphenix.kernel.objectloader.FBeanFactory;
import com.jphenix.kernel.objectloader.interfaceclass.IBeanFactory;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.script.IScriptLoader;

/**
 * 脚本加载器工厂类
 * 主要提供外部程序调用
 * @author MBG
 * 2017年3月31日
 */
@ClassInfo({"2017-03-31 12:37","脚本加载器工厂类"})
public class FScript {


	/**
	 * 获取脚本类加载器实例
	 * @return 脚本类加载器实例
	 * 2017年3月31日
	 * @author MBG
	 */
	public static IScriptLoader loader() {
		//获取类工厂
		IBeanFactory bf = FBeanFactory.getBeanFactory();
		if(bf!=null) {
			if(bf.beanExists(IScriptLoader.class)) {
				try {
					return bf.getObject(IScriptLoader.class,"[FScript]");
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return new NullScriptLoader();
	}
}
