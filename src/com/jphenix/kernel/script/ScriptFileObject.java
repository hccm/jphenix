/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年4月11日
 * V4.0
 */
package com.jphenix.kernel.script;

import com.jphenix.clazz.ClassFile;
import com.jphenix.share.tools.FileCopyTools;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.script.IScriptLoader;

import javax.tools.SimpleJavaFileObject;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author 马宝刚
 * 2016年4月11日
 */
public class ScriptFileObject extends SimpleJavaFileObject {

    private String    scriptID  = null; //类主键
    private String    className = null; //类名
    private byte[]    clsBytes  = null; //脚本内容数组
    private ClassFile classFile = null; //类文件对象
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ScriptFileObject(String path,Kind kind) {
        super((new File(path)).toURI(),kind);
        init(); //执行初始化
    }
    
    /**
     * 构造函数
     * @author MBG
     */
    public ScriptFileObject(String scriptID,InputStream is) {
    	super((new File("")).toURI(),Kind.CLASS);
    	this.scriptID = scriptID;
    	className = IScriptLoader.SCRIPT_PACKAGE+"."+scriptID;
    	
    	try {
    		clsBytes = FileCopyTools.copyToByteArray(is);
    	}catch(IOException e) {
    		e.printStackTrace();
    	}
    	
    }
    
    /**
     * 执行初始化
     * 2016年4月11日
     * @author 马宝刚
     */
    private void init() {
        scriptID = SFilesUtil.getFileBeforeName(super.getName());
        className = IScriptLoader.SCRIPT_PACKAGE+"."+scriptID;
    }
    
    /**
     * 获取类主键
     * @return 类主键
     * 2016年4月11日
     * @author 马宝刚
     */
    public String getID() {
        return scriptID;
    }
    
    
    
    /**
     * 覆盖函数
     */
    @Override
    public String getName() {
        return className;
    }
    
    /**
     * 重新加载
     * 2016年4月15日
     * @author MBG
     */
    public void reload() {
    	clsBytes = null;
    	classFile = null;
    }
    
    /**
     * 获取脚本类的字节数组
     * @return 脚本类的字节数组
     * 2016年4月12日
     * @author MBG
     */
    public byte[] toByteArray() {
    	if(clsBytes==null) {
    		try {
    			clsBytes = FileCopyTools.copyToByteArray(new File(toUri()));
    		}catch(Exception e) {}
    	}
    	return clsBytes;
    }
    
    /**
     * 覆盖函数
     */
    @Override
    public InputStream openInputStream() throws IOException {
    	//构建返回数据
    	byte[] data = toByteArray();
    	if(data==null) {
    		return new ByteArrayInputStream(new byte[0]);
    	}
        return new ByteArrayInputStream(data);
    }
    
    
    /**
     * 覆盖函数
     */
    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
    	//构建返回数据
    	byte[] data = toByteArray();
    	if(data==null || data.length<1) {
    		return "".subSequence(0,0);
    	}
    	//构建返回字符串
    	String reStr = new String(data, StandardCharsets.UTF_8);
    	return reStr.subSequence(0,reStr.length());
    }
    
    /**
     * 覆盖函数
     */
    @Override
    public OutputStream openOutputStream() throws IOException {
        return new FileOutputStream(new File(toUri()));
    }
    
    
    /**
     * 获取对应的类文件对象
     * @return 对应的类文件对象
     * 2017年2月12日
     * @author MBG
     */
    public ClassFile getClassFile() {
    	if(!super.getKind().equals(Kind.CLASS)) {
    		return null;
    	}
    	if(classFile==null) {
    		try {
    			classFile = new ClassFile(toByteArray());
    		}catch(Exception e) {
    			e.printStackTrace();
    		}
    	}
    	return classFile;
    }
}
