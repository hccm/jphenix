/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2015年10月15日
 * V4.0
 */
package com.jphenix.kernel.script;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本文件信息类
 * @author 马宝刚
 * 2015年10月15日
 */
@ClassInfo({"2015-10-15 13:33","脚本文件信息类"})
public class ScriptFileBean {

    public String id = null; //脚本主键
    public String filePath = null; //脚本源文件所在位置
    public long lastUpdate = 0; //上次更新时间
    public long checkTime = 0; //检测时间
    
    /**
     * 构造函数
     * @author 马宝刚
     */
    public ScriptFileBean() {
        super();
    }
}
