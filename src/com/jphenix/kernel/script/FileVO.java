/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年8月5日
 * V4.0
 */
package com.jphenix.kernel.script;

import com.jphenix.driver.nodehandler.FNodeHandler;
import com.jphenix.share.lang.SBoolean;
import com.jphenix.share.lang.SString;
import com.jphenix.share.util.BaseUtil;
import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;
import com.jphenix.standard.script.IScriptLoader;
import com.jphenix.standard.viewhandler.INodeHandler;

import java.io.File;
import java.io.InputStream;

/**
 * 文件、路径信息容器
 * 
 * 2020-07-18 增加了后缀信息，文件夹级别的后缀
 * 
 * 
 * @author 马宝刚
 * 2014年8月5日
 */
@ClassInfo({"2020-07-18 23:16","文件、路径信息容器"})
public class FileVO {

	public boolean loaded   = false; //是否加载成功
	public boolean isAdd    = false; //是否为新增文件夹
	public String  id       = null;  //脚本文件主键
	public boolean isPath   = false; //是否为路径
	public String  subPath  = null;  //相对路径
	public String  upPath   = null;  //上一级路径
	public String  aname    = null;  //路径、文件名称
	public String  explain  = null;  //路径、文件说明
	public String  atype    = null;  //类型 N内部脚本  T测试脚本
	public String  allName  = null;  //全路径文字说明
	public String  ver      = null;  //版本号
	public boolean isNative = false; //是否为内置资源
	public boolean hidden   = false; //是否隐藏不显示（比如内部脚本）
	public String  footer   = null;  //脚本名后缀
	
	/**
	 * 构造函数
	 * @author 马宝刚
	 */
	public FileVO() {
		super();
	}
	
	/**
	 * 清空容器
	 * 2016年7月19日
	 * @author MBG
	 */
	public void clear() {
		loaded 		= false; 	//是否加载成功
		isAdd 		= false; 	//是否为新增文件夹
		id 			= null; 	//脚本文件主键
		isPath 		= false; 	//是否为路径
		subPath 	= null; 	//相对路径
		aname 		= null; 	//路径、文件名称
		explain 	= null; 	//路径、文件说明
		atype 		= null; 	//类型 N内部脚本  T测试脚本
		allName 	= null; 	//全路径文字说明
		ver 		= null; 	//版本号
		isNative    = false;    //是否为内置资源
		hidden      = false;    //是否隐藏不显示
		upPath      = null;     //上一级路径
		footer      = null;     //脚本名后缀
	}
	
	
	/**
	 * 解析文件内容（从压缩包中获取到的）
	 * @param content 文件内容
	 * @param subPath 相对路径
	 * @return
	 * 2016年7月19日
	 * @author MBG
	 */
	private FileVO parseContent(String content,String subPath,String basePath) {
		clear();
		if(content==null || content.length()<1) {
			return this;
		}
		//构建文件解析类
		INodeHandler nh = FNodeHandler.newNodeHandler("UTF-8");
		nh.setXmlStyle(true);
		try {
			nh.setNodeBody(content);
		}catch(Exception e) {
			e.printStackTrace();
			return this;
		}
		isPath = true;
		this.subPath = subPath;
		aname   = nh.getFirstChildNodeByNodeName("name").nt();
		explain = nh.getFirstChildNodeByNodeName("explain").nt();
		atype   = nh.getFirstChildNodeByNodeName("type").nt();
		ver     = nh.getFirstChildNodeByNodeName("ver").nt();
		hidden  = SBoolean.valueOf(nh.getFirstChildNodeByNodeName("hidden").nt());
		footer  = nh.getFirstChildNodeByNodeName("footer").nt();
		
		//处理上一级文件夹
		if(subPath.length()>1 && subPath.endsWith("/")) {
			subPath = subPath.substring(0,subPath.length()-1);
		}
		int point = subPath.lastIndexOf("/");
		if(point<1) {
			upPath = "";
			allName =aname;
		}else {
			upPath = subPath.substring(0,point);
		}
		loaded = true;
		return this;
	}
	
	/**
	 * 解析路径信息文件
	 * @param subPath 相对路径（可以包含文件名，也可以不包含）
	 * @param basePath 根路径
	 * @param isScriptFile 是否为脚本资源
	 * @return 设置信息后的当前类实例
	 * 2016年7月19日
	 * @author MBG
	 */
	public FileVO parse(String subPath,String basePath,boolean isScriptFile) {
		clear();
		
		if(basePath!=null && (new File(basePath)).isFile()) {
			//此乃压缩包是也
			return parseContent(
					SFilesUtil.getZipFileContent(
							subPath,basePath,"UTF-8"),subPath,basePath);
		}
		if(subPath==null || subPath.length()<1) {
			return this;
		}
		subPath = BaseUtil.swapString(subPath,"\\","/");
		if(!subPath.startsWith("/")) {
			subPath = "/"+subPath;
		}
		if(subPath.endsWith(IScriptLoader.SCRIPT_FOLDER_INFO_NAME)) {
			subPath = SFilesUtil.getFilePath(subPath,false);
		}
		if(!subPath.endsWith("/")) {
			subPath += "/";
		}
		//文件夹
		File cFile;
		try {
			cFile = SFilesUtil.getFileByName(subPath+IScriptLoader.SCRIPT_FOLDER_INFO_NAME,basePath);
		}catch(Exception e) {
			//没找到该文件夹中默认信息文件
			return this;
		}
		return parse(cFile,subPath,basePath,isScriptFile);
	}
	
	
	/**
	 * 解析路径信息文件
	 * @param file    配置文件信息
	 * @param subPath 相对路径（可以包含文件名，也可以不包含）
	 * @param basePath 根路径
	 * @param isScriptFile 是否为脚本资源
	 * @return 设置信息后的当前类实例
	 * 2016年7月19日
	 * @author MBG
	 */
	public FileVO parse(File file,String subPath,String basePath,boolean isScriptFile) {
		if(!file.exists()) {
			return this;
		}
		//构建文件解析类
		INodeHandler nh = FNodeHandler.newNodeHandler("UTF-8");
		nh.setXmlStyle(true);
		try {
			nh.setFile(file);
		}catch(Exception e) {
			e.printStackTrace();
			return this;
		}
		isPath = !isScriptFile;
		this.subPath = subPath;
		if(isScriptFile) {
			id      = SFilesUtil.getFileBeforeName(subPath);
			aname   = nh.getFirstChildNodeByNodeName("title").nt();
			explain = nh.getFirstChildNodeByNodeName("explain").nt();
			atype   = "";
			ver     = nh.getFirstChildNodeByNodeName("ver").nt();
			hidden  = false;
			footer  = "";
		}else {
			aname   = nh.getFirstChildNodeByNodeName("name").nt();
			explain = nh.getFirstChildNodeByNodeName("explain").nt();
			atype   = nh.getFirstChildNodeByNodeName("type").nt();
			ver     = nh.getFirstChildNodeByNodeName("ver").nt();
			hidden  = SBoolean.valueOf(nh.getFirstChildNodeByNodeName("hidden").nt());
			footer  = nh.getFirstChildNodeByNodeName("footer").nt();
		}
		//处理上一级文件夹
		if(subPath.length()>1 && subPath.endsWith("/")) {
			subPath = subPath.substring(0,subPath.length()-1);
		}
		int point = subPath.lastIndexOf("/");
		if(point<1) {
			upPath = "";
			allName =aname;
		}else {
			upPath = subPath.substring(0,point);
			//构造父节点
			FileVO pVO = new FileVO();
			pVO.parse(upPath,basePath,false);
			allName = SString.valueOf(pVO.allName)+"/"+aname;
		}
		loaded = true;
		return this;
	}
	
	/**
	 * 解析读入流
	 * @param is      读入流
	 * @param path    文件相对路径
	 * @param isScriptFile 是否为脚本资源
	 * @return        解析后的文件信息对象
	 * 2017年2月1日
	 * @author MBG
	 */
	public FileVO parse(InputStream is,String path,boolean isScriptFile) {		//构建文件解析类
		INodeHandler nh = FNodeHandler.newNodeHandler("UTF-8");
		nh.setXmlStyle(true);
		try {
			nh.setStream(is,path);
		}catch(Exception e) {
			e.printStackTrace();
			return this;
		}
		isPath = !isScriptFile;
		this.subPath = path;
		if(isScriptFile) {
			id      = SFilesUtil.getFileBeforeName(subPath);
			aname   = nh.getFirstChildNodeByNodeName("title").nt();
			explain = nh.getFirstChildNodeByNodeName("explain").nt();
			atype   = "";
			ver     = nh.getFirstChildNodeByNodeName("ver").nt();
			hidden  = false;
			footer  = "";
		}else {
			aname   = nh.getFirstChildNodeByNodeName("name").nt();
			explain = nh.getFirstChildNodeByNodeName("explain").nt();
			atype   = nh.getFirstChildNodeByNodeName("type").nt();
			ver     = nh.getFirstChildNodeByNodeName("ver").nt();
			hidden  = SBoolean.valueOf(nh.getFirstChildNodeByNodeName("hidden").nt());
			footer  = nh.getFirstChildNodeByNodeName("footer").nt();
		}
		
		//处理上一级文件夹
		if(subPath.length()>1 && subPath.endsWith("/")) {
			subPath = subPath.substring(0,subPath.length()-1);
		}
		int point = subPath.lastIndexOf("/");
		if(point<1) {
			upPath = "";
			allName =aname;
		}else {
			upPath = subPath.substring(0,point);
		}
		
		loaded = true;
		return this;
	}
	
	
	/**
	 * 转换输出为XML字符串格式
	 * @return XML字符串格式内容
	 * 2016年7月26日
	 * @author MBG
	 */
	public String toXml() {
		//构建返回值
		StringBuffer reSbf = new StringBuffer();
		reSbf
			.append("<?xml version=\"1.0\" encoding='UTF-8'?>\n")
			.append("<root>\n")
		    .append("  <name>").append(SString.valueOf(aname)).append("</name>\n")
			.append("  <explain>").append(SString.valueOf(explain)).append("</explain>\n")
			.append("  <type>").append(SString.valueOf(atype)).append("</type>\n")
			.append("  <footer>").append(SString.valueOf(footer)).append("</footer>\n")
			.append("  <ver>").append(SString.valueOf(ver)).append("</ver>\n");
		
		if(hidden) {
			reSbf.append("  <hidden>1</ver>\n");
		}
		reSbf.append("</root>");
		return reSbf.toString();
	}
	
	
	
	/**
	 * 覆盖方法
	 */
	@Override
    public String toString() {
		return (new StringBuffer("FileVO:"))
					.append(" isPath:[").append(isPath).append("]")
					.append(" subPath:[").append(subPath).append("]")
					.append(" upPath:[").append(upPath).append("]")
					.append(" aname:[").append(aname).append("]")
					.append(" explain:[").append(explain).append("]")
					.append(" atype:[").append(atype).append("]")
					.append(" footer:[").append(footer).append("]")
					.append(" allName:[").append(allName).append("]")
					.append(" hidden:[").append(hidden).append("]")
					.toString();
	}
}
