/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2014年7月23日
 * V4.0
 */
package com.jphenix.kernel.script;

import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本参数信息类
 * <fields>
 *		<field>
 *		    <name>aname</name>
 *		    <type>String</type>
 *		    <not_null>no</not_null>
 *          <default_value></default_value>
 *          <base64_deccode></base64_deccode>
 *		    <explain>
 *		        <![CDATA[用户名 作为筛选条件]]>
 *		    </explain>
 *		</field>
 * </fields>
 * 
 * 2019-06-13 增加了参数defValue（默认值），needB64Dec是否需要Base64解码。这两个参数仅限于基本类型，Json，XML
 * 
 * @author 马宝刚
 * 2014年7月23日
 */
@ClassInfo({"2014-07-23 10:35","脚本参数信息类"})
public class ScriptFieldVO {

	public String name        = null;  //参数名
	public String type        = null;  //参数类型
	public String defValue    = null;  //默认值
	public boolean needB64Dec = false; //获取到的值（包括默认值）是否需要做Base64解码
	public boolean notNull    = false; //是否为必传参数
	
	//如果设置为不为空， 参数说明可以分为两段，用 //分隔。
    //分隔符前面的文字会返回到页面上，分隔符后面的文字不会返回到页面上
	//这样既给开发人员提供了详细的参数说明，又能简单的返回到页面，给用户提示
	public String  explain      = null;  //参数说明 
	public boolean isStaticPara = false; //是否为静态全局设置参数（无论什么脚本传入参数中都会存在该参数）
	
	/**
	 * 构造函数
	 * @author 马宝刚
	 */
	public ScriptFieldVO() {
		super();
	}
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public ScriptFieldVO(String name,String type,String defValue
			,String explain,boolean notNull,boolean isStaticPara,boolean needB64Dec) {
		super();
		this.name         = name;
		this.type         = type;
		this.defValue     = defValue;
		this.explain      = explain;
		this.notNull      = notNull;
		this.isStaticPara = isStaticPara;
		this.needB64Dec   = needB64Dec;
	}
	
	/**
	 * 克隆
	 */
	@Override
    public ScriptFieldVO clone() {
		//构建返回值
		ScriptFieldVO res = new ScriptFieldVO();
		res.name         = name;
		res.type         = type;
		res.defValue     = defValue;
		res.notNull      = notNull;
		res.explain      = explain;
		res.isStaticPara = isStaticPara;
		res.needB64Dec   = needB64Dec;
		return res;
	}
	
	/**
	 * 覆盖函数
	 */
	@Override
    public String toString() {
		//构建返回值
		StringBuffer sbf = new StringBuffer();
		sbf
			.append("name:[").append(name).append("] ")
			.append("type:[").append(type).append("] ")
			.append("defValue:[").append(defValue).append("]\n")
			.append("notNull:[").append(notNull).append("]")
			.append("isStaticPara:[").append(isStaticPara).append("]")
			.append("needB64Dec:[").append(needB64Dec).append("]\n")
			.append("explain:[").append(explain).append("]");
		return sbf.toString();
	}
}
