/*
 * 代号：凤凰
 * http://www.jphenix.org
 * 2016年7月19日
 * V4.0
 */
package com.jphenix.kernel.script;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jphenix.share.util.SFilesUtil;
import com.jphenix.standard.docs.ClassInfo;

/**
 * 脚本文件容器（用来做版本比较）
 * 该程序用在了内置脚本中
 * 
 * 2019-10-08 将原来写错的名字ScriptFileVecotr改对
 * 
 * @author MBG
 * 2016年7月19日
 * com.jphenix.kernel.script.ScriptFileVector
 */
@ClassInfo({"2019-10-08 14:26","脚本文件容器（用来做版本比较）"})
public class ScriptFileVector {

	protected String basePath = null; //根路径
	
	//文件序列
	protected List<String> pathList = new ArrayList<String>();
	//文件信息类容器
	protected Map<String,FileVO> fileVOMap = null;
	//脚本信息类容器 key:脚本文件相对路径
	protected Map<String,ScriptVO> scriptPathVOMap = null;
	//脚本信息类容器 key:脚本主键
	protected Map<String,ScriptVO> scriptVOMap = null;
	
	/**
	 * 构造函数
	 * @author MBG
	 */
	public ScriptFileVector() {
		super();
	}
	
	
	/**
	 * 加载脚本信息
	 * @param path 脚本路径（文件夹或压缩包 全路径）
	 * 2016年7月19日
	 * @author MBG
	 */
	public void load(String path) {
		//构建路径对象
		load(new File(path));
	}
	
	/**
	 * 加载脚本信息
	 * @param path 脚本路径对象（文件夹或压缩包 全路径）
	 * 2016年7月19日
	 * @author MBG
	 */
	public void load(File pathObj) {
		if(pathObj==null) {
			return;
		}
		if(pathObj.isDirectory()) {
			try {
				setPath(pathObj);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else if(pathObj.isFile()) {
			try {
				setZipFile(pathObj);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 通过脚本主键获取脚本信息类
	 * @param id 脚本主键
	 * @return 脚本信息类
	 * 2016年7月19日
	 * @author MBG
	 */
	public ScriptVO getScriptVO(String id) {
		if(scriptVOMap==null) {
			return null;
		}
		return scriptVOMap.get(id);
	}
	
	
	/**
	 * 获取路径信息类或者脚本信息类
	 * @param path 相对路径
	 * @return 路径信息类或者脚本信息类
	 * 2016年7月19日
	 * @author MBG
	 */
	public Object getInfo(String path) {
		if(path==null) {
			return null;
		}
		if(path.toLowerCase().endsWith("_default.xml")) {
			return getPathInfo(path);
		}
		return getScriptInfo(path);
	}
	
	/**
	 * 获取脚本信息类
	 * @param path 脚本文件路径（相对路径）
	 * @return 脚本信息类
	 * 2016年7月19日
	 * @author MBG
	 */
	public ScriptVO getScriptInfo(String path) {
		if(scriptPathVOMap==null) {
			return null;
		}
		return scriptPathVOMap.get(path);
	}
	
	/**
	 * 获取路径描述信息对象
	 * @param path 相对路径
	 * @return 路径信息描述对象
	 * 2016年7月19日
	 * @author MBG
	 */
	public FileVO getPathInfo(String path) {
		if(fileVOMap==null) {
			return null;
		}
		return fileVOMap.get(path);
	}
	
	/**
	 * 获取根路径
	 * @return 根路径
	 * 2016年7月19日
	 * @author MBG
	 */
	public String getBasePath() {
		if(basePath==null) {
			basePath = "";
		}
		return basePath;
	}
	
	
	/**
	 * 获取文件路径序列（相对路径）
	 * @return 文件路径序列
	 * 2016年7月19日
	 * @author MBG
	 */
	public List<String> getFileList(){
		if(pathList==null) {
			pathList = new ArrayList<String>();
		}
		return pathList;
	}
	
	
	
	/**
	 * 设置并执行搜索指定压缩文件
	 * @param path 文件对象
	 * @throws Exception 异常
	 * 2016年7月19日
	 * @author MBG
	 */
	protected void setZipFile(File path) throws Exception {
		basePath = path.getPath();
		
		pathList = new ArrayList<String>();
		fileVOMap = new HashMap<String,FileVO>();
		scriptPathVOMap = new HashMap<String,ScriptVO>();
		scriptVOMap = new HashMap<String,ScriptVO>();
		
		//文件路径序列
		List<String> subPathList = new ArrayList<String>();
		
		//执行搜索
		SFilesUtil.searchZipFile(subPathList,basePath,null,"xml",true);
		
		FileVO fVO; //文件信息类
		ScriptVO sVO; //脚本信息类
		for(String ele:subPathList) {
			pathList.add(ele);
			if(ele.toLowerCase().endsWith("_default.xml")) {
				//文件夹信息
				fVO = (new FileVO()).parse(ele,basePath,false);
				if(fVO.loaded) {
					fileVOMap.put(ele,fVO);
				}
			}else {
				//脚本信息
				sVO = (new ScriptVO()).parse(ele,basePath);
				
				scriptPathVOMap.put(ele,sVO);
				scriptVOMap.put(sVO.id,sVO);
			}
		}
	}
	
	
	/**
	 * 设置并执行搜索指定文件夹
	 * @param path 文件夹对象
	 * @throws Exception 异常
	 * 2016年7月19日
	 * @author MBG
	 */
	protected void setPath(File path) throws Exception {
		basePath = path.getPath();
		pathList = new ArrayList<String>();
		fileVOMap = new HashMap<String,FileVO>();
		scriptPathVOMap = new HashMap<String,ScriptVO>();
		scriptVOMap = new HashMap<String,ScriptVO>();
		
		//文件路径序列
		List<String> subPathList = new ArrayList<String>();
		
		//执行搜索
		SFilesUtil.getFileList(subPathList,basePath,null,"xml",true,true);
		
		FileVO fVO; //文件信息类
		ScriptVO sVO; //脚本信息类
		for(String ele:subPathList) {
			pathList.add(ele);
			
			if(ele.toLowerCase().endsWith("_default.xml")) {
				//文件夹信息
				fVO = (new FileVO()).parse(ele,basePath,false);
				if(fVO.loaded) {
					fileVOMap.put(ele,fVO);
				}
			}else {
				//脚本信息
				sVO = (new ScriptVO()).parse(ele,basePath);
				
				scriptPathVOMap.put(ele,sVO);
				scriptVOMap.put(sVO.id,sVO);
			}
		}
	}
}
