/* source:/adm/js/src/popup_layer.js */
function getEventTarget(){if(window.event){return window.event.srcElement;}
var caller=arguments.callee.caller;while(caller.caller!=null){caller=caller.caller;}
if(caller.arguments!=null&&caller.arguments.length>0){return caller.arguments[0].target;}
if(window.event){return window.event.target;}
return null;}
Function.prototype.binding=function(){if(arguments.length<2&&typeof arguments[0]=="undefined")return this;var __method=this,args=jQuery.makeArray(arguments),object=args.shift();return function(){return __method.apply(object,args.concat(jQuery.makeArray(arguments)));}}
var Class=function(subclass){subclass.setOptions=function(options){this.options=jQuery.extend({},this.options,options);for(var key in options){if(/^on[A-Z][A-Za-z]*$/.test(key)){$(this).bind(key,options[key]);}}}
var fn=function(){if(subclass._init&&typeof subclass._init=='function'){this._init.apply(this,arguments);}}
if(typeof subclass=='object'){fn.prototype=subclass;}
return fn;}
var PopupLayer=new Class({options:{trigger:null,popupBlk:null,closeBtn:null,eventType:"click",offsets:{x:0,y:-41},useFx:true,useOverlay:true,isresize:true,onBeforeStart:function(){}},_init:function(options){var body=document.body;this.setOptions(options);this.isSetPosition=this.isDoPopup=this.isOverlay=true;if(this.options.trigger){this.trigger=$(this.options.trigger);}
if(!this.trigger){this.trigger=this.options.trigger;if(this.trigger){this.trigger=$(this.trigger);}else{this.trigger=$(getEventTarget());}}
this.popupBlk=this.options.popupBlk;if(this.popupBlk){this.popupBlk=$(this.popupBlk);}else{this.popupBlk=$("#path_popup_area");}
if(!this.popupLayer){var pos=this.popupBlk.css("position");if(pos=="absolute"||pos=="fixed"){this.popupLayer=this.popupBlk;}else{this.popupLayer=$(['<div style="display:none;background-color:#FFF;position:absolute;z-index:1001;width:',this.popupBlk.width(),'px;height:',this.popupBlk.height(),'px;"></div>'].join(''));$(body).append(this.popupLayer);}}
if(this.options.useOverlay){this.overlay=$(['<div popup_layer_bg style="background-color:#000;opacity:0.2;position:absolute;z-index:1000;left:0px;top:0px;display:none;width:',body.scrollWidth,'px;height:',body.scrollHeight,'px;"></div>'].join(''));$(body).append(this.overlay);}
this.closeBtn=this.options.closeBtn;if(this.closeBtn){this.closeBtn=$(this.closeBtn);}else{this.closeBtn=$("#close_popup_bt");}
$(this).trigger("onBeforeStart");this.popupBlk.show();this.popupLayer.append(this.popupBlk);this.isresize?$(window).bind("resize",this.doresize.binding(this)):null;this.closeBtn.bind("click",this.close.binding(this));this.refreshPosition();(this.isOverlay&&this.options.useOverlay)?this.overlay.show():null;if(this.isDoPopup&&(this.popupLayer.css("display")=="none")){this.options.useFx?this.doEffects("open"):this.popupLayer.show();}},doresize:function(){if(this.overlay){var body=document.body;this.overlay.css({width:body.clientWidth+"px",height:body.clientHeight+"px"})}
this.refreshPosition();},refreshPosition:function(){var left;var top;var trigger=this.trigger;var offset=trigger?trigger.offset():null;var body=document.body;var node=this.popupLayer;var nWidth=node.width();var nHeight=node.height();if(trigger==null||offset==null||trigger.length<1){left=(body.offsetWidth-nWidth)/2+body.scrollLeft;top=(body.offsetHeight-nHeight)/2+body.scrollTop;}else{left=(offset.left+this.options.offsets.x);top=(offset.top+this.options.offsets.y+trigger[0].offsetHeight);if(left+nWidth>body.scrollWidth){left=body.scrollWidth-nWidth;}
if(top+nHeight>body.scrollHeight){top=body.scrollHeight-nHeight;}}
node.css({left:left+"px",top:top+"px"});if(this.options.useOverlay){this.overlay.css({width:body.scrollWidth,height:body.scrollHeight});}},doEffects:function(way){var that=this;way=="open"?this.popupLayer.show("fast",function(){if(that.options.useOverlay){var body=document.body;that.overlay.css({width:body.scrollWidth,height:body.scrollHeight});}}):this.popupLayer.hide("fast");},close:function(){this.options.useOverlay?this.overlay.remove():null;this.options.useFx?this.doEffects("close"):this.popupLayer.hide();}});﻿
