/* /js/func.js */
if(typeof Object.assign!='function'){Object.assign=function(target){'use strict';if(target==null){throw new TypeError('Cannot convert undefined or null to object');}
target=Object(target);for(var index=1;index<arguments.length;index++){var source=arguments[index];if(source!=null){for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key];}}}}
return target;};}
(function(types){types.forEach(function(type){if(!type.prototype.includes){type.prototype.includes=function(search,start){if(typeof start!=='number'){start=0;}
if(start+search.length>this.length){return false;}else{return this.indexOf(search,start)!==-1;}};}});})([String,Array]);Date.prototype.format=function(fmt){var o={'M+':this.getMonth()+1,'d+':this.getDate(),'H+':this.getHours(),'m+':this.getMinutes(),'s+':this.getSeconds(),'S+':this.getMilliseconds()};if(/(y+)/.test(fmt)){fmt=fmt.replace(RegExp.$1,(this.getFullYear()+'').substr(4-RegExp.$1.length));}
for(var k in o){if(new RegExp('('+k+')').test(fmt)){fmt=fmt.replace(RegExp.$1,(RegExp.$1.length==1)?(o[k]):(('00'+o[k]).substr(String(o[k]).length)));}}
return fmt;};function getChexkBoxVal(node){if(node.length<1){return"";}
if(node[0].checked){return node.val();}
return"";}
function getRequest(key,paramUrl){var paraMap=getLocationParameterMap(paramUrl);var res=paraMap.get(key);if(!res){return"";}
return decodeURI(res);}
function getLocationParameterMap(paramUrl){if(!paramUrl){paramUrl=window.location.href;}
var point=paramUrl.lastIndexOf("?");if(point>-1){paramUrl=paramUrl.substring(point+1,paramUrl.length);return paraStr2Map(paramUrl);}
return new ValueMap();}
function paraStr2Map(paraStr){var reMap=new ValueMap();var subPara;var point;while(true){var fixOver=false;point=paraStr.indexOf("&");if(point>-1){subPara=paraStr.substring(0,point);paraStr=paraStr.substring(point+1,paraStr.length);}else{subPara=paraStr;fixOver=true;}
point=subPara.indexOf("=");if(point>-1){reMap.put(subPara.substring(0,point),subPara.substring(point+1,subPara.length));}
if(fixOver){break;}}
return reMap;}
function map2ParamStr(map){if(!map){return"";}
var keyList=map.getKeyList();var key;var reStr="";for(var i=0;i<keyList.size();i++){key=keyList.get(i);if(reStr!=""){reStr+="&";}
reStr+=key+"="+map.get(key);}
return reStr;}
function postData(url,data){var xmlHttp;try{if(window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}else if(window.ActiveXObject){xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
if(url.indexOf("?")>-1){url+="&";}else{url+="?";}
url+="_js=1&_ts_="+(new Date()).valueOf();if(data==null||data==""){xmlHttp.open("GET",url,false);xmlHttp.setRequestHeader("CONTENT-TYPE","application/x-www-form-urlencoded");xmlHttp.send(null);}else{xmlHttp.open("POST",url,false);xmlHttp.setRequestHeader("CONTENT-TYPE","application/x-www-form-urlencoded");xmlHttp.send(data);}
return xmlHttp.responseText;}catch(ex){if(debug){alert("Error:\nAction:["+url+"]\nParams:["+data+"]\n"+ex);}}}
function postJson(url,json){var method;if(json){method="POST";}else{method="GET";}
if(!(typeof(json)=="string")){json=JSON.stringify(json);}
if(url.indexOf("?")>-1){url+="&";}else{url+="?";}
url+="_js=1&_ts_="+(new Date()).valueOf();var res=$.ajax({type:method,url:url,dataType:"json",beforeSend:function(xhr){xhr.setRequestHeader("base64_enc","1");},cache:false,async:false,type:"post",data:Base64.e64(json)}).responseText;if(res==""){return null;}
var dataJson=null;try{eval("dataJson = "+res);if(dataJson.status==-99){return null;}}catch(e){}
return dataJson;}
function postXml(url,xml){var method;if(xml){method="POST";}else{method="GET";}
if(url.indexOf("?")>-1){url+="&";}else{url+="?";}
url+="_js=1&_ts_="+(new Date()).valueOf();var res=$.ajax({type:method,url:url,dataType:"xml",beforeSend:function(xhr){xhr.setRequestHeader("base64_enc","1");},cache:false,async:false,type:"post",data:Base64.e64(xml)}).responseText;if(res==""){return null;}
var dataJson=null;try{eval("dataJson = "+res);if(dataJson.status==-99){return null;}}catch(e){}
return dataJson;}
function queryXml(url,xml,base64Enc){var method;if(xml){method="POST";}else{method="GET";}
if(url.indexOf("?")>-1){url+="&";}else{url+="?";}
url+="_js=1&_ts_="+(new Date()).valueOf();var res=$.ajax({type:method,url:url,dataType:"xml",beforeSend:function(xhr){if(base64Enc){xhr.setRequestHeader("base64_enc","1");}},cache:false,async:false,type:"post",data:base64Enc?Base64.e64(xml):xml});if(res==null){return null;}
return $(res.responseXML);}
function getJson(url,params,reCallFunc,errorFunc,debug,reTry,noCheckStatus){var method;if(params){method="POST";}else{method="GET";}
if(url.indexOf("?")>-1){url+="&";}else{url+="?";}
url+="_js=1&_ts_="+(new Date()).valueOf();if(reCallFunc){$.ajax({type:method,url:url,cache:false,dataType:"text",data:params,success:function(result){if(debug){console.log(result);}
var dataJson=null;try{eval("dataJson = "+result);}catch(e){}
reCallFunc(dataJson);},error:errorFunc});return;}
var res=$.ajax({type:method,url:url,cache:false,async:false,data:params}).responseText;if(debug){console.log(res);}
if(res==""){return null;}
var dataJson=null;try{eval("dataJson = "+res);if(!noCheckStatus&&dataJson.status==-99){if(reTry){try{logout();}catch(e){}
return dataJson;}
if(checkUser()){return getJson(url,params,reCallFunc,errorFunc,debug,true);}
return dataJson;}}catch(e){}
return dataJson;}
function getUrlValue(url,params,innerTag,invokeMethod,append,debug){var method;if(params){method="POST";}else{method="GET";}
if(innerTag||invokeMethod){if(innerTag){if(typeof(innerTag)=="string"){innerTag=$("#"+innerTag);}
innerTag.append("<div mark=\"_wait_img_\" style=\"width:100%;text-align:center;margin-top:20px;margin-bottom:50px;\"><img src=\"/mnc/images/wait.gif\" /></div>");if(!invokeMethod){invokeMethod=function(data){if(debug){console.log(data);}
innerTag.find("[mark='_wait_img_']").remove();if(append){innerTag.append($(data));}else{innerTag.html(data);}
if(typeof(_AfterGetUrlValue)!="undefined"){_AfterGetUrlValue();}}}}
if(debug){console.log("准备调用动作:["+url+"] 传入参数:["+params+"] 是否调试模式:["+debug+"]");}
$.ajax({type:method,url:url,data:params,cache:false,success:invokeMethod});}else{if(invokeMethod){$.ajax({type:method,url:url,data:params,cache:false,success:invokeMethod});}else{return $.ajax({type:method,url:url,cache:false,async:false,data:params}).responseText;}}}
function getParentNode(node,attributeValue,attributeName){if(!node){return null;}
if(!(node instanceof jQuery)){node=$(node);}
if(node[0].nodeName=="#document"){return null;}
var res=node.find("["+attributeName+"="+attributeValue+"]");if(res.length<1){return getParentNode(node[0].parentNode,attributeValue,attributeName);}
return res;}
function urlEncode(str){var ret="";var strSpecial="!\"#$%&'()*+,/:;<=>?[]^`{|}~%";var tt="";for(var i=0;i<str.length;i++){var chr=str.charAt(i);var c=str2asc(chr);tt+=chr+":"+c+"n";if(parseInt("0x"+c)>0x7f){ret+="%"+c.slice(0,2)+"%"+c.slice(-2);}else{if(chr==" "){ret+="+";}else if(strSpecial.indexOf(chr)!=-1){ret+="%"+c.toString(16);}else{ret+=chr;}}}
return ret;}
function urlDecode(str){var ret="";for(var i=0;i<str.length;i++){var chr=str.charAt(i);if(chr=="+"){ret+=" ";}else if(chr=="%"){var asc=str.substring(i+1,i+3);if(parseInt("0x"+asc)>0x7f){ret+=asc2str(parseInt("0x"+asc+str.substring(i+4,i+6)));i+=5;}else{ret+=asc2str(parseInt("0x"+asc));i+=2;}}else{ret+=chr;}}
return ret;}
function str2asc(strstr){return("0"+strstr.charCodeAt(0).toString(16)).slice(-2);}
function asc2str(ascasc){return String.fromCharCode(ascasc);}
function ValueMap(){var keyList=new ValueList();var vMap=({});this.typeName="Map";this.put=doPut;this.get=doGet;this.index=doIndex;this.key=doKey;this.clear=doClear;this.keys=doGetKeys;this.getKeys=doGetKeys;this.remove=doRemove;this.getKeyString=doGetKeyString;this.getKeyList=doGetKeyList;this.toJavaString=getJavaString;this.toEncodeJavaString=getEncodeJavaString;this.size=keyList.size;this.putAll=doPutAll;this.containsKey=containsKey;this.containsValue=containsValue;this.toString=doToString;this.removeValue=doRemoveValue;this.clone=doClone;function doToString(){var res="";var keyList=doGetKeyList();for(var i=0;i<keyList.size();i++){var key=keyList.get(i);var value=doGet(key);if(value==null){res+="["+key+"#null]";}else if(value instanceof ValueList){res+="["+key+"#List:"+value.toString()+"]";}else if(value instanceof ValueList){res+="["+key+"#Map:"+value.toString()+"]";}else{res+="["+key+"#"+value+"]";}
res+="\n";}
res+="{total:"+i+"}\n";return res;}
function doClone(){var res=new ValueMap();var keys=doGetKeys();for(var i=0;i<keys.length;i++){res.put(keys[i],vMap[keys[i]]);}
return res;}
function containsKey(key){return doGetKeyList().contains(key);}
function containsValue(value){for(key in vMap){if(vMap[key]==value){return true;}}
return false;}
function doIndex(index){return vMap[keyList.get(index)];}
function doKey(index){return keyList.get(index);}
function doPutAll(map){if(map==null||map.typeName!="Map"){return;}
var keyList=map.getKeyList();if(keyList==null){return;}
for(var i=0;i<keyList.size();i++){var key=keyList.get(i);doPut(key,map.get(key));}}
function getEncodeJavaString(){return urlEncode(getObjectJavaString(this));}
function getJavaString(){return getObjectJavaString(this);}
function doRemove(key){var res=doGet(key);vMap[key]=null;if(keyList.contains(key)){keyList.removeByElement(key);}
return res;}
function doGetKeys(){return keyList.getList();}
function doRemoveValue(value){var keyList=doGetKeyList();var key;for(var i=0;i<keyList.size();i++){key=keyList.get(i);if(doGet(key)==value){doRemove(key);return key;}}}
function doGetKeyList(){return keyList;}
function doClear(){keyList.clearAll();vMap=({});}
function doPut(key,value){vMap[key]=value;if(!keyList.contains(key)){keyList.add(key);}}
function doGet(key){var res=vMap[key];if(!res){return"";}
return res;}
function doGetKeyString(){return keyList.getListString();}}
function ValueList(arr){var listValue=arr?arr:[];var currentPoint=0;this.typeName="List";this.size=getSize;this.add=addElement;this.removeByElement=removeByElement;this.removeByIndex=removeByIndex;this.remove=removeByIndex;this.get=getElement;this.set=setElement;this.contains=containsElement;this.clearAll=clearAll;this.clear=clearAll;this.getList=getElementList;this.getListString=getElementString;this.toJavaString=getJavaString;this.toEncodeJavaString=getEncodeJavaString;this.toArray=doToArray;this.addAll=doAddAll;this.toString=doToString;this.setArray=doSetArray;this.hasNext=doHasNext;this.nextValue=doNextValue;this.value=doValue;this.replaceValue=doReplaceValue;this.sort=doSort;this.indexOf=doIndexOf;this.clone=doClone;function doReplaceValue(ele){if(getSize()<1||currentPoint<1||currentPoint>getSize()){return;}
return setElement(currentPoint-1,ele);}
function doClone(){var res=new ValueList();for(var i=0;i<getSize();i++){res.add(getElement(i));}
return res;}
function doValue(){if(getSize()<1){return null;}else if(currentPoint<1){return getElement(0);}else if(currentPoint>getSize()){return getElement(getSize()-1);}
return getElement(currentPoint-1);}
function doNextValue(){return getElement(currentPoint++);}
function doHasNext(){if(getSize()>currentPoint){return true;}
return false;}
function doSetArray(arr){listValue=arr;if(arr){listValue=arr;}else{listValue=[];}}
function doToString(){var res="";var size=getSize();for(var i=0;i<size;i++){var ele=getElement(i);if(ele==null){res+="[null]";}else if(ele instanceof ValueList){res+="[List:"+ele.toString()+"]";}else if(ele instanceof ValueList){res+="[Map:"+ele.toString()+"]";}else{res+="["+ele+"]";}
res+="\n";}
res+="{total:"+i+"}\n";return res;}
function doAddAll(list){if(list==null||list.typeName!="List"){return;}
for(var i=0;i<list.size();i++){addElement(list.get(i));}}
function doToArray(){var res=[];for(var i=0;i<getSize();i++){res[i]=getElement(i);}
return res;}
function doSort(){var arr=doToArray();arr.sort();doSetArray(arr);}
function getSize(){return listValue.length;}
function getJavaString(){return getObjectJavaString(this);}
function getEncodeJavaString(){return urlEncode(getObjectJavaString(this));}
function clearAll(){listValue=[];}
function doIndexOf(element){if(!element){return-1;}
for(var i=0;i<listValue.length;i++){if(listValue[i]==element){return i;}}
return-1;}
function addElement(element,index){if(index==null||index==""){listValue[listValue.length]=element;return;}
var indexInt;try{indexInt=parseInt(index);}catch(e){return;}
if(indexInt<0){return;}
var length=listValue.length;while(length>indexInt){listValue[length]=listValue[length-1];length--;}
listValue[indexInt]=element;}
function removeByElement(element){var afterRemoveList=[];for(var i=0;i<listValue.length;i++){if(listValue[i]==element){continue;}
afterRemoveList[afterRemoveList.length]=listValue[i];}
listValue=afterRemoveList;return element;}
function removeByIndex(index){if(typeof(index)!="number"){return removeByElement(index);}
var indexInt;try{indexInt=parseInt(index);}catch(e){return null;}
var res=getElement(indexInt);var afterRemoveList=[];for(var i=0;i<listValue.length;i++){if(i==indexInt){continue;}
afterRemoveList[afterRemoveList.length]=listValue[i];}
listValue=afterRemoveList;return res;}
function getElement(index,notNull){var indexInt;try{indexInt=parseInt(index);}catch(e){if(notNull){return"";}
return null;}
try{var res=listValue[index];if(notNull&&!res){return"";}
return res;}catch(e){if(notNull){return"";}
return null;}}
function setElement(index,ele){var indexInt;try{indexInt=parseInt(index);}catch(e){return;}
if(indexInt<1||indexInt>=getSize()){return;}
listValue[index]=ele;}
function containsElement(element){for(var i=0;i<listValue.length;i++){if(listValue[i]==element){return true;}}
return false;}
function getElementList(){return listValue;}
function getElementString(){var reStr="";for(var i=0;i<listValue.length;i++){if(i>0){reStr+=",";}
if(listValue[i]!=null){reStr+=listValue[i];}}
return reStr;}}
function parsePage(debug){var params=window.location.href;var point=params.indexOf("?");if(point>-1){params=params.substring(point+1,params.length);}else{params="";}
var nodes=$("[_execute='js']");for(var i=0;i<nodes.length;i++){_parseOneNode(nodes[i],params,debug);}}
function setArraySelectNode(nodeId,data,defValue){if(!nodeId){return;}
if(typeof(nodeId)=="string"){node=$("#"+nodeId);}else{node=nodeId;}
var txtData;var txt;for(var i=0;i<data.length;i++){txtData=str(data[i][1]);if(txtData!=""){txt="("+data[i][0]+") "+txtData;}else{txt=data[i][0];}
if(defValue!=null&&defValue==data[i][0]){node.append($("<option _data_=\"1\" value=\""+data[i][0]+"\" title_value=\""+txtData+"\" selected>"+txt+"</option>"));}else{node.append($("<option _data_=\"1\" value=\""+data[i][0]+"\" title_value=\""+txtData+"\">"+txt+"</option>"));}}}
function setSelectNode(nodeId,debug){var node;if(!nodeId){return;}
if(typeof(nodeId)=="string"){node=$("#"+nodeId);}else{node=nodeId;}
var action=node.attr("_url");if(!action){return;}
var params=node.attr("_params");if(action.substring(0,1)!="/"){action="/"+action;}
var listKey=node.attr("_listkey");if(!listKey){listKey="";}
var point=action.indexOf("?");if(point>-1){action=action.substring(0,point)+action.substring(point,action.length);}
if(debug){alert("URL:"+action);alert("处理前的下拉框：\n"+node.html());}
var data=getJson(action,params,debug);if(data==null){return;}
if(listKey!=""){var listKeys=listKey.split(".");for(var i=0;i<listKeys.length;i++){data=data[listKeys[i]];}}
if(data&&data.rs){data=data.rs;}
setSelect(nodeId,data,node.attr("_value"),node.attr("_valueKey"),node.attr("_textKey"));}
function setSelect(nodeId,data,defValue,valueKey,textKey){var node;if(!nodeId){return;}
if(typeof(nodeId)=="string"){node=$("#"+nodeId);}else{node=nodeId;}
if(!defValue){defValue="";}
if(!valueKey){valueKey="";}
if(!textKey){textKey="";}
node.find("[_data_='1']").remove();if(!data){return;}
if(valueKey==""&&textKey==""){for(var i=0;i<data.length;i++){if(defValue!=""&&defValue==data[i]){node.append($("<option _data_=\"1\" value=\""+data[i]+"\" selected>"+data[i]+"</option>"));}else{node.append($("<option _data_=\"1\" value=\""+data[i]+"\">"+data[i]+"</option>"));}}}else{for(var i=0;i<data.length;i++){if(defValue!=""&&defValue==data[i][valueKey]){node.append($("<option _data_=\"1\" value=\""+data[i][valueKey]+"\" selected>"+data[i][textKey]+"</option>"));}else{node.append($("<option _data_=\"1\" value=\""+data[i][valueKey]+"\">"+data[i][textKey]+"</option>"));}}}}
function parseAction(nodeId,action,params,debug){var node;if(!nodeId){return;}
if(typeof(nodeId)=="string"){node=$("#"+nodeId);}else{node=nodeId;}
if(action.substring(0,1)!="/"){action="/"+action;}
var point=action.indexOf("?");if(point>-1){action=action.substring(0,point)+(action.indexOf(".ha")<0?".ha":"")+action.substring(point,action.length);}else if(action.indexOf(".ha")<0){action+=".ha";}
if(debug){console.log("URL:"+action);console.log("处理前的HTML模板：\n"+node.html());}
var data=getJson(action,params,debug);if(data==null){console.log("没有获取到数据:["+action+"] ["+params+"]");return;}
var dataEle;var setNode;for(var key in data){setNode=node.find("#"+key);if(setNode.length<1){setNode=node;}
dataEle=data[key];if(dataEle["allcount"]){_fixIntoRs(setNode,dataEle);}else if(typeof(dataEle)=="string"){_setNodeValue(setNode,dataEle);}else{var fields=setNode.find("[_field]");for(var j=0;j<fields.length;j++){_fixIntoField($(fields[j]),dataEle);}}}
if(debug){console.log("处理后的HTML模板：\n"+node.html());}}
function _parseOneNode(node,params,debug){node=$(node);var action=node.attr("_action");if(!action){action=node.attr("_js_action");if(!action){node.hide();return;}}
if(debug){alert("URL:"+action+"\nparams:"+params);alert("处理前的HTML模板：\n"+node.html());}
var data=getJson(action,params,debug);if(data==null){return;}
var dataEle;var setNode;for(var key in data){if(node.attr("id")==key){setNode=node;}else{setNode=node.find("#"+key);}
if(setNode.length<1){continue;}
dataEle=data[key];if(dataEle["allcount"]){_fixIntoRs(setNode,dataEle);}else if(typeof(dataEle)=="string"){_setNodeValue(setNode,dataEle);}else{var fields=setNode.find("[_field]");for(var j=0;j<fields.length;j++){_fixIntoField($(fields[j]),dataEle);}}}
if(debug){alert("处理后的HTML模板：\n"+node.html());}}
function _fixIntoRs(rsNode,rsData){rsNode.find("#tr_data").remove();var trMode=rsNode.find("#tr");trMode.hide();trMode=trMode.clone();trMode.show();trMode.attr("id","tr_data");var rsList=rsData["rs"];if(!rsList||!rsList.length||rsList.length<1){return;}
rsNode.find("#_nodata").remove();var tr;var fields;for(var i=0;i<rsList.length;i++){tr=trMode.clone();fields=tr.find("[_field]");for(var j=0;j<fields.length;j++){_fixIntoField($(fields[j]),rsList[i]);}
rsNode.append(tr);}}
function _fixIntoField(fNode,dataMap){if(fNode.length<1){return;}
var fieldIds=(""+fNode.attr("_field")).split(",");var value;for(var i=0;i<fieldIds.length;i++){if(fieldIds[i]==null||fieldIds[i]==""){continue;}
value=dataMap[fieldIds[i]];_setNodeValue(fNode,value,i+1);}}
function _setNodeValue(fNode,value,index){if(fNode.length<1){return;}
var nodeName=fNode[0].nodeName;if(nodeName){nodeName=nodeName.toLowerCase();}else{nodeName="";}
if(index==null){index=1;}
var keyWord=fNode.attr("_keyword");if(!keyWord){keyWord=fNode.attr("_keyword"+index);}
var setValueAttr;var checkType=fNode.attr("_checktype");if(!checkType){setValueAttr=fNode.attr("_checktype"+index);}
if(!checkType){checkType=fNode.attr("_check");}
if(!checkType){setValueAttr=fNode.attr("_check"+index);}
value=_checkValue(checkType,value,fNode);setValueAttr=fNode.attr("_set");if(!setValueAttr){setValueAttr=fNode.attr("_set"+index);}
if(setValueAttr){if(keyWord){value=(""+fNode.attr(setValueAttr)).replace(keyWord,value);}
if(setValueAttr=="class"){fNode.addClass(value);}else{fNode.attr(setValueAttr,value);}}else{if(nodeName=="input"){var atype=fNode.attr("type");if(atype){atype=atype.toLowerCase();}else{atype="text";}
if(atype=="image"){fNode.attr("src",value);}else if(atype=="checkbox"||atype=="radio"){if(fNode.attr("value")==value){fNode.attr("checked","checked");}else{fNode.attr("checked","");}}else{if(keyWord){value=(""+fNode.attr("value")).replace(keyWord,value);}
fNode.attr("value",value);}}else if(nodeName=="select"){setSelectNode(fNode);var options=fNode.find("option");var opt;for(var j=0;j<options.length;j++){opt=$(options[j]);if(opt.attr("value")==value){opt.attr("selected","selected");break;}}}else{if(keyWord){value=(""+fNode.text()).replace(keyWord,value);}
fNode.text(value);}}}
function _checkValue(checkType,value,node){if(!checkType){return value;}
if(value==null){value="";}
if(checkType=="date"){if(value&&value!=""){value=formatDate(value,"yyyy-mm-dd");}}else if(startsWith(checkType,"date:[")){if(value&&value!=""){var format=checkType.substring(6,checkType.length);var point=format.lastIndexOf("]");if(point>0){format=format.substring(0,point);}
format=trim(format);value=formatDate(value,format);}}else if(checkType=="trim"){value=trim(value);}else if(checkType=="showint"){value=addCommas(value);}else if(checkType=="time"){if(value&&value!=""){value=formatDate(value,"hh:mm:ss");}}else if(checkType=="datemd"){if(value&&value!=""){value=formatDate(value,"mm-dd");}}else if(checkType.indexOf("switch:")==0){checkType=checkType.substring(7,checkType.length);var infoMap=fixStringToMap(checkType,";",":");if(infoMap.containsKey(value)){value=infoMap.get(value);}}else if(checkType.indexOf("cut:")==0){checkType=checkType.substring(4,checkType.length);var fength=checkType.indexOf(":");var addStr=null;if(toLength<0){toLength=checkType.length;addStr="";}else{addStr=checkType.substring(toLength+1,checkType.length);}
var cutPoint=parseInt(checkType.substring(0,toLength));value=cutHtmlString(value,cutPoint,addStr);}else if(checkType.indexOf("swap:")==0){checkType=checkType.substring(5,checkType.length);var toLength=checkType.indexOf(":");if(toLength>0){var startCount=parseInt(checkType.substring(0,toLength));checkType=checkType.substring(toLength+1);if(value.length>startCount){var newValue=value.substring(0,startCount);value=value.substring(startCount,value.length);if(value.length>checkType.length){value=newValue+checkType+value.substring(checkType.length+value.length);}else{value=newValue+checkType;}}else{value=checkType;}}}else if(checkType.indexOf("float:")==0){checkType=checkType.substring(6,checkType.length);var toLength=checkType.indexOf(":");var allLength=0;var pointLength=0;if(toLength>0){allLength=parseInt(checkType.substring(0,toLength));pointLength=parseInt(checkType.substring(toLength+1,checkType.length));}else{pointLength=parseInt(checkType);}
try{value=roundNumber(value,pointLength);}catch(e){value="--";}}else if(checkType.indexOf("head:")==0){return checkType.substring(5,checkType.length)+value;}else if(checkType.indexOf("foot:")==0){return value+checkType.substring(5,checkType.length);}else if(checkType=="tohtml"){return fixHtmlString(value);}else if(startsWith(checkType,"if:")){checkType=checkType.substring(3,checkType.length);var point=checkType.lastIndexOf("{");if(point<0){return null;}
var srcAttr=checkType.substring(point+1,checkType.length);checkType=checkType.substring(0,point);var objAttr=null;point=srcAttr.lastIndexOf("}");if(point>0){srcAttr=srcAttr.substring(0,point);}
if(srcAttr.toLowerCase()=="hidden"){srcAttr=null;objAttr="hidden";}else if(srcAttr.toLowerCase()=="!hidden"){srcAttr="hidden";objAttr=null;}else{point=srcAttr.lastIndexOf(":");if(point<0){return null;}
objAttr=srcAttr.substring(point+1,srcAttr.length);srcAttr=srcAttr.substring(0,point);}
var checkValue;if(startsWith(checkType,">=")){checkValue=checkType.substring(2,checkType.length);if(parseFloat(value)>=parseFloat(checkValue)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"<=")){checkValue=checkType.substring(2,checkType.length);if(parseFloat(value)<=parseFloat(checkValue)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"<")){checkValue=checkType.substring(1,checkType.length);if(parseFloat(value)<parseFloat(checkValue)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,">")){checkValue=checkType.substring(1,checkType.length);if(parseFloat(value)>parseFloat(checkValue)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"==")){checkValue=checkType.substring(2,checkType.length);if(value==checkValue){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"=")){checkValue=checkType.substring(1,checkType.length);if(value==checkValue){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"||")){checkValue=checkType.substring(2,checkType.length);var testInt1=parseInt(value);var testInt2=parseInt(checkValue);var testInt=testInt1/testInt2;if(testInt1==(testInt2*testInt)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else if(startsWith(checkType,"(")){checkValue=checkType.substring(1,checkType.length);point=checkValue.lastIndexOf(")");if(point<0){return null;}
var checkList=new ValueList();checkList.setArray(checkValue.substring(0,point).split(","));if(checkList.contains(value)){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}else{if(value==checkType){if(srcAttr==null){node.hide();}else if(objAttr!=null){node.removeAttr(objAttr);node.attr(objAttr,node.attr(srcAttr));node.removeAttr(srcAttr);}}else{if(objAttr==null){node.hide();}else if(srcAttr!=null){node.removeAttr(srcAttr);}}}
return null;}
return value;}
function formatDate(dateStr,fmt){var d=new Date(dateStr);var o={"M+":d.getMonth()+1,"o+":d.getMonth()+1,"d+":d.getDate(),"h+":d.getHours(),"m+":d.getMinutes(),"s+":d.getSeconds(),"q+":Math.floor((d.getMonth()+3)/3),"n+":d.getMilliseconds(),"S":d.getMilliseconds()};if(/(y+)/.test(fmt)){fmt=fmt.replace(RegExp.$1,(d.getFullYear()+"").substr(4-RegExp.$1.length));}
for(var key in o){if(new RegExp("("+key+")").test(fmt)){fmt=fmt.replace(RegExp.$1,(RegExp.$1.length==1)?(o[key]):(("00"+o[key]).substr((""+o[key]).length)));}}
return fmt;}
function ltrim(s,f){if(!f){f="";}
if(s){return s.replace(/^\s*/,f);}
return"";}
function rtrim(s,f){if(!f){f="";}
if(s){return s.replace(/\s*$/,f);}
return"";}
function trim(s,f){return rtrim(ltrim(s,f),f);}
function innerInt(s){if(s){s=s.replace(/[^0-9]/ig,"");if(s==""){return 0;}
return parseInt(s);}
return 0;}
function startsWith(str,substring){if(str==null||substring==null){return false;}
if(str.length==substring.length){if(str==substring){return true;}
return false;}
if(str.substring(0,substring.length)==substring){return true;}
return false;}
String.prototype.startsWith=function(substring){if(substring==null){return false;}
if(this.length==substring.length){if(str==substring){return true;}
return false;}
if(this.substring(0,substring.length)==substring){return true;}
return false;}
 
function endsWith(str,substring){if(str==null||substring==null){return false;}
if(str.length==substring.length){if(str==substring){return true;}
return false;}
if(str.substring(str.length-substring.length,str.length)==substring){return true;}
return false;}
String.prototype.endsWith=function(substring){if(substring==null){return false;}
if(this.length==substring.length){if(this==substring){return true;}
return false;}
if(this.substring(this.length-substring.length,this.length)==substring){return true;}
return false;}
function addCommas(nStr,fgNum){if(fgNum==null){fgNum=3;}
nStr+='';x=nStr.split('.');x1=x[0];x2=x.length>1?'.'+x[1]:'';var rgx=eval("/(\\d+)(\\d{"+fgNum+"})/");while(rgx.test(x1)){x1=x1.replace(rgx,'$1'+','+'$2');}
return x1+x2;}
function fixStringToMap(str,eleSpec,keySpec){var reMap=new ValueMap();if(!str){return reMap;}
str=trim(str);if(str.length<1){return reMap;}
var eles=str.split(eleSpec);var infos=null;for(var i=0;i<eles.length;i++){infos=eles[i].split(keySpec);if(infos.length>1){reMap.put(infos[0],infos[1]);}}
return reMap;}
function cutHtmlString(source,cutSize,footStr){if(!source){return"";}
if(!footStr){footStr="";}
var length=source.length;if(length<=cutSize){return source;}
if(source.indexOf("<")<0){return source.substring(0,cutSize)+footStr;}
var readPoint=0;var reSbf="";var isAppend=true;var notAppendFoot=true;var insideTag=false;var onOutTag=false;for(var i=0;i<length;i++){var oneChar=source.substring(i,i+1);if(oneChar=="<"){insideTag=true;}else if(oneChar==">"){insideTag=false;onOutTag=true;}else if(!insideTag){readPoint++;}
if(readPoint>cutSize){if(insideTag){isAppend=true;}else{isAppend=false;}
if(notAppendFoot){reSbf+=footStr;notAppendFoot=false;}}
if(isAppend||onOutTag){if(onOutTag){onOutTag=false;}
reSbf+=oneChar;}}
return reSbf.replace("<br/>","").replace("<br>","");}
function roundNumber(number,decimals){number=parseFloat(number);var newString;decimals=Number(decimals);if(decimals<1){newString=(Math.round(number)).toString();}else{var numString=number.toString();if(numString.lastIndexOf(".")==-1){numString+=".";}
var cutoff=numString.lastIndexOf(".")+decimals;var d1=Number(numString.substring(cutoff,cutoff+1));var d2=Number(numString.substring(cutoff+1,cutoff+2));if(d2>=5){if(d1==9&&cutoff>0){while(cutoff>0&&(d1==9||isNaN(d1))){if(d1!="."){cutoff-=1;d1=Number(numString.substring(cutoff,cutoff+1));}else{cutoff-=1;}}}
d1+=1;}
if(d1==10){numString=numString.substring(0,numString.lastIndexOf("."));var roundedNum=Number(numString)+1;newString=roundedNum.toString()+'.';}else{newString=numString.substring(0,cutoff)+d1.toString();}}
if(newString.lastIndexOf(".")==-1){newString+=".";}
var decs=(newString.substring(newString.lastIndexOf(".")+1)).length;for(var i=0;i<decimals-decs;i++)newString+="0";document.roundform.roundedfield.value=newString;}
function fixHtmlString(html){if(!html){return"";}
return html.replace(/&/g,"&amp;").replace(/#/g,"&#35;").replace(/\?/g,"&#63;").replace(/%/g,"&#37;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\r\n/g,"<br />").replace(/\r/g,"<br />").replace(/\n/g,"<br />");}
function urlEncodePro(plaintext){if(!plaintext||plaintext==""){return"";}
return plaintext.replace(/%/g,"%25").replace(/=/g,"%3D").replace(/\+/g,"%2B").replace(/ /g,"+").replace(/\n/g,"%0D%0A").replace(/#/g,"%23").replace(/\'/g,"%27").replace(/\,/g,"%2C").replace(/\"/g,"%22").replace(/\;/g,"%3B").replace(/\./g,"%2E").replace(/\-/g,"%2D").replace(/:/g,"%3A").replace(/\//g,"%2F").replace(/\r/g,"").replace(/\t/g,"%09").replace(/&/g,"%26").replace(/\?/g,"%3F").replace(/!/g,"%21").replace(/</g,"%3C").replace(/>/g,"%3E").replace(/\\/g,"%5C");}
function getFormData(nodeId){if(typeof(nodeId)=="string"){nodeId=$("#"+nodeId);}
var res="";var ele;var key;var value;var nodes=nodeId.find("input[type='hidden']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
var nodes=nodeId.find("input[type='text']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
var nodes=nodeId.find("input[type='time']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
var nodes=nodeId.find("input[type='date']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
nodes=nodeId.find("input[type='radio']");for(i=0;i<nodes.length;i++){if(!nodes[i].checked){continue;}
ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
nodes=nodeId.find("input[type='checkbox']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
if(nodes[i].checked){value=ele.val();}else{value=ele.attr("no_check");if(!value){continue;}}
if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
nodes=nodeId.find("select");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
nodes=nodeId.find("textarea");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();if(res.length>0){res+="&";}
res+=key+"="+urlEncodePro(value);}
return res;}
function getFormDataObj(nodeId){if(typeof(nodeId)=="string"){nodeId=$("#"+nodeId);}
var res={};var ele;var key;var value;var nodes=nodeId.find("input[type='hidden']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
var nodes=nodeId.find("input[type='text']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
var nodes=nodeId.find("input[type='time']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
var nodes=nodeId.find("input[type='date']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
nodes=nodeId.find("input[type='radio']");for(i=0;i<nodes.length;i++){if(!nodes[i].checked){continue;}
ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
nodes=nodeId.find("input[type='checkbox']");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
if(nodes[i].checked){value=ele.val();}else{value=ele.attr("no_check");if(!value){continue;}}
if(res[key]==null||res[key]==""){res[key]=value;}else{res[key]+=","+value;}}
nodes=nodeId.find("select");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
nodes=nodeId.find("textarea");for(i=0;i<nodes.length;i++){ele=$(nodes[i]);if(ele.attr("no_submit")=="1"){continue;}
key=ele.attr("name");if(!key){key=ele.attr("id");}
if(!key){continue;}
value=ele.val();res[key]=value;}
return res;}
function str(s,defVal){if(s){return s;}
if(defVal&&defVal!=""){return defVal;}
return"";}
function sid(){return""+(new Date()).getTime();}
function setDataById(nodeOrId,data){if(typeof(nodeOrId)=="string"){nodeOrId=$("#"+nodeOrId);}
clearForm(nodeOrId);if(!data){return;}
var nodes;for(ele in data){nodes=nodeOrId.find("#"+ele);setNodeValue(nodes,data[ele]);nodes=nodeOrId.find("[name='"+ele+"']");setNodeValue(nodes,data[ele]);}}
function setNodeValue(nodeOrId,value){if(typeof(nodeOrId)=="string"){nodeOrId=$("#"+nodeOrId);}
var nodeName;var node;var checkType
var keyword;var attrib;for(var i=0;i<nodeOrId.length;i++){nodeName=nodeOrId[i].nodeName;if(!nodeName){continue;}
nodeName=nodeName.toLowerCase();node=$(nodeOrId[i]);checkType=node.attr("_checktype");if(!checkType){checkType=node.attr("_check");}
if(checkType){_checkValue(checkType,value,node)}
keyword=node.attr("keyword");if(!keyword){keyword=node.attr("kw");}
if(nodeName=="input"){attrib=node.attr("type");if(!attrib){continue;}
attrib=attrib.toLowerCase();if(attrib=="text"||attrib=="time"||attrib=="date"||attrib=="hidden"){if(keyword){value=(""+node.val()).replace(keyword,value);}
node.val(value);}else if(attrib=="checkbox"||attrib=="radio"){if(node.attr("value")==value){node.prop("checked",true);}else{node.prop("checked",false);}}}else if(nodeName=="select"||nodeName=="textarea"){if(keyword){value=(""+node.val()).replace(keyword,value);}
node.val(value);}else{attrib=node.attr("_set");if(attrib){if(keyword){value=(""+node.attr(attrib)).replace(keyword,value);}
node.attr(attrib,value);}else{if(keyword){value=(""+node.html()).replace(keyword,value);}
node.html(value);}}}}
function clearForm(nodeOrId){if(typeof(nodeOrId)=="string"){nodeOrId=$("#"+nodeOrId);}
var nodes=nodeOrId.find("input[type='hidden'][def!='1']");if(nodes.length>0){nodes.val("");}
var nodes=$("input[type='text']");if(nodes.length>0){nodes.val("");}
nodes=$("input[type='time']");if(nodes.length>0){nodes.val("");}
nodes=$("input[type='date']");if(nodes.length>0){nodes.val("");}
nodes=$("input[type='radio']");if(nodes.length>0){nodes.prop("checked",false);}
nodes=$("input[type='checkbox']");if(nodes.length>0){nodes.prop("checked",false);}
nodes=$("select");if(nodes.length>0){nodes.val("");}
nodes=$("textarea");if(nodes.length>0){nodes.val("");}}
function cookie(key,value,sec){if(!sec){sec=1000*60*60*24*365;}else{sec=sex*1000;}
var exp=new Date();exp.setTime(exp.getTime()+sec);document.cookie=key+"="+escape(value)+";expires="+exp.toGMTString();}
function cookieDel(key){var exp=new Date();exp.setTime(exp.getTime()-10000);document.cookie=key+"=; expire="+exp.toGMTString();}
function cookie(key){var arr,reg=new RegExp("(^| )"+key+"=([^;]*)(;|$)");if(arr=document.cookie.match(reg)){return unescape(arr[2]);}
return"";}
function setOuterWidth(node,width){if(!node){return;}
var outerWidth=innerInt(node.css("margin-left"))
+innerInt(node.css("margin-right"))
+innerInt(node.css("padding-left"))
+innerInt(node.css("padding-right"))
+innerInt(node.css("border-left-width"))
+innerInt(node.css("border-right-width"));width-=outerWidth;if(width<0){width=0;}
node.width(width);}
function outerHeight(jqNode){var height;if(jqNode.length>0&&jqNode[0].scrollHeight){height=jqNode[0].scrollHeight;}else{height=jqNode.height();}
return innerInt(jqNode.css("margin-top"))
+innerInt(jqNode.css("padding-top"))
+innerInt(jqNode.css("border-top-width"))
+height
+innerInt(jqNode.css("margin-bottom"))
+innerInt(jqNode.css("padding-bottom"))
+innerInt(jqNode.css("border-bottom-width"));}
function outerWidth(jqNode){var width;if(jqNode.length>0&&jqNode[0].scrollWidth){width=jqNode[0].scrollWidth;}else{width=jqNode.width();}
return innerInt(jqNode.css("margin-left"))
+innerInt(jqNode.css("padding-left"))
+innerInt(jqNode.css("border-left-width"))
+width
+innerInt(jqNode.css("margin-right"))
+innerInt(jqNode.css("padding-right"))
+innerInt(jqNode.css("border-right-width"));}
function setOuterHeight(node,height){if(!node){return;}
var outerHeight=innerInt(node.css("margin-top"))
+innerInt(node.css("margin-bottom"))
+innerInt(node.css("padding-top"))
+innerInt(node.css("padding-bottom"))
+innerInt(node.css("border-top-width"))
+innerInt(node.css("border-bottom-width"));height-=outerHeight;if(height<0){height=0;}
node.height(height);}
function now(){var date=new Date();var res=date.getFullYear()+"-";var val=date.getMonth()+1;if(val<10){res+="0"+val;}else{res+=val;}
res+="-";val=date.getDate();if(val<10){res+="0"+val;}else{res+=val;}
res+=" ";val=date.getHours();if(val<10){res+="0"+val;}else{res+=val;}
res+=":";val=date.getMinutes();if(val<10){res+="0"+val;}else{res+=val;}
res+=":";val=date.getSeconds();if(val<10){res+="0"+val;}else{res+=val;}
return res;}
function sdata(key,value,del){if(!sessionStorage){return"";}
if(del){if(key){value=str(sessionStorage.getItem(key));sessionStorage.removeItem(key);}else{sessionStorage.clear();value="";}
return value;}
if(value&&value!=""){sessionStorage.setItem(key,value);}else{value=str(sessionStorage.getItem(key));}
return value;}
function ldata(key,value,del){if(!localStorage){return"";}
if(del){if(key){value=str(localStorage.getItem(key));localStorage.removeItem(key);}else{localStorage.clear();value="";}
return value;}
if(value&&value!=""){localStorage.setItem(key,value);}else{value=str(localStorage.getItem(key));}
return value;}
function getEvent(){if(window.event){return window.event;}
return null;}
function stopEvent(evt){if(!evt){evt=getEvent();}
if(!evt){return;}
if(evt.preventDefault){evt.preventDefault();evt.stopImmediatePropagation();}else{evt.cancelBubble=true;evt.returnValue=false;}}
function cssNum(str){var res=parseFloat((str+"").replace(/[^\d.]/g,""));if(res){return res;}
return 0;}
function cssValue(str,totalValue,lastSuffix){if(!str||str.length<1){if(lastSuffix){return"0px";}
return 0;}
str=(str+"").toLowerCase();if(endsWith(str,"px")){if(lastSuffix){return str;}
return parseFloat(str.substr(0,str.length-2));}else if(endsWith(str,"%")){str=parseFloat(str.substr(0,str.length-1));if(totalValue){totalValue=cssNum(totalValue)/100;if(lastSuffix){return(totalValue*str)+"px";}
return totalValue*str;}
if(lastSuffix){return str+"px";}
return str;}
if(lastSuffix){return str+"px";}
return parseFloat(str.replace(/[^\d.]/g,""));}
function clickEvent(node,triggerCount,method,delayTime){var evt=getEvent();stopEvent(evt);if(!(node instanceof jQuery)){node=$(node);}
if(!delayTime){delayTime=400;}
var timeOutHandle=node.attr("_time_out_handle");if(timeOutHandle){node.removeAttr("_time_out_handle");window.clearTimeout(timeOutHandle);}
var clickCount=node.attr("_click_count");if(clickCount){clickCount++;}else{clickCount=1;}
node.attr("_click_count",clickCount);window.setTimeout(function(){timeOutHandle=_doClickEvent(node,triggerCount,method);node.attr("_time_out_handle",timeOutHandle);},delayTime);}
function _doClickEvent(node,triggerCount,method){var clickCount=node.attr("_click_count");node.removeAttr("_click_count");if(!method||clickCount!=triggerCount){return;}
method(node);}
var NID_INDEX_VALUE=0;function nid(){var day=new Date();var res="JS"+day.getFullYear();var value=day.getMonth()+1;if(value<10){res+="0"+value;}else{res+=value;}
value=day.getDate();if(value<10){res+="0"+value;}else{res+=value;}
value=day.getHours();if(value<10){res+="0"+value;}else{res+=value;}
value=day.getMinutes();if(value<10){res+="0"+value;}else{res+=value;}
value=day.getSeconds();if(value<10){res+="0"+value;}else{res+=value;}
if(NID_INDEX_VALUE>=9999){NID_INDEX_VALUE=0;}
value=""+(NID_INDEX_VALUE++);while(value.length<4){value="0"+value;}
return res+value;}
var FIX_AUTO_SIZE_SN=0;function fixAutoSize(node){if(FIX_AUTO_SIZE_SN>9999){FIX_AUTO_SIZE_SN=0;}else{FIX_AUTO_SIZE_SN++;}
if(!node){node=$("html");}
fixAutoWidth(node,FIX_AUTO_SIZE_SN);fixAutoHeight(node,FIX_AUTO_SIZE_SN);}
function doFixWidth(node,sn){if(node.attr("sn_auto_swidth")==sn){return;}
node.attr("sn_auto_swidth",sn);var pNode=findPNode(node,"swidth",sn);if(pNode){doFixWidth(pNode,sn);}
var pWidth=node.width();if(!pWidth||pWidth==0){pWidth=WIDTH;}
var lessWidth=pWidth;var width;var mLeft;var mRight;var pLeft;var pRight;var borderVal;var eNode;var noList=new ValueList();node.find(">").each(function(){eNode=$(this);if(eNode.attr("no_swidth")!=undefined){return;}
if(eNode.css("position")=="absolute"){return;}
if(eNode.css("display")=="none"){return;}
width=str(this.style.width);if(width==""||eNode.attr("swidth_no_set")=="1"){noList.add(eNode);return;}
mLeft=cssValue(eNode.css("margin-left"));mRight=cssValue(eNode.css("margin-right"));pLeft=cssValue(eNode.css("padding-left"));pRight=cssValue(eNode.css("padding-right"));borderVal=cssValue(eNode.css("border-left-width"))+cssValue(eNode.css("border-right-width"));lessWidth-=cssValue(width,pWidth)+mLeft+mRight+pLeft+pRight+borderVal;});var noListSize=noList.size();var average=lessWidth/noListSize;for(var i=0;i<noListSize;i++){eNode=noList.get(i);eNode.attr("swidth_no_set","1");mLeft=cssValue(eNode.css("margin-left"));mRight=cssValue(eNode.css("margin-right"));pLeft=cssValue(eNode.css("padding-left"));pRight=cssValue(eNode.css("padding-right"));borderVal=cssValue(eNode.css("border-left-width"))+cssValue(eNode.css("border-right-width"));if(i+1==noListSize){width=lessWidth-mLeft-mRight-pLeft-pRight-borderVal;}else{width=average-mLeft-mRight-pLeft-pRight-borderVal;lessWidth-=average;}
eNode.width(width);}}
function fixAutoWidth(pNode,sn){pNode.find("[swidth]").each(function(){doFixWidth($(this),sn);});}
function findPNode(node,key,sn){var pNode=node.parent();if(pNode.length<1||pNode[0].nodeName=="HTML"){return null;}
if(pNode.attr(key)!=undefined&&pNode.attr("sn_auto_"+key)!=sn){return pNode;}
return findPNode(pNode,key,sn);}
function doFixHeight(node,sn){if(node.attr("sn_auto_sheight")==sn){return;}
node.attr("sn_auto_sheight",sn);var pNode=findPNode(node,"sheight",sn);if(pNode){doFixHeight(pNode,sn);}
var pHeight=node.height();if(!pHeight||pHeight==0){pHeight=HEIGHT;}
var lessHeight=pHeight;var height;var mTop;var mBottom;var pTop;var pBottom;var borderVal;var eNode;var parentMTop;var noList=new ValueList();var peNode;node.find(">").each(function(){eNode=$(this);if(eNode.attr("no_sheight")!=undefined){return;}
if(eNode.css("position")=="absolute"){return;}
if(eNode.css("display")=="none"){return;}
height=str(this.style.height);if(height==""||eNode.attr("sheight_no_set")=="1"){noList.add(eNode);return;}
mTop=cssValue(eNode.css("margin-top"));mBottom=cssValue(eNode.css("margin-bottom"));pTop=cssValue(eNode.css("padding-top"));pBottom=cssValue(eNode.css("padding-bottom"));borderVal=cssValue(eNode.css("border-top-width"))+cssValue(eNode.css("border-bottom-width"));peNode=eNode.parent();if(peNode.find(">")[0]==this&&peNode.css("overflow")!="hidden"){parentMTop=cssValue(peNode.css("margin-top"));if(parentMTop>0){mTop=mTop-parentMTop;if(mTop<0){mTop=0;}}}
lessHeight-=cssValue(height,pHeight)+mTop+mBottom+pTop+pBottom+borderVal;});var noListSize=noList.size();var average=lessHeight/noListSize;for(var i=0;i<noListSize;i++){eNode=noList.get(i);eNode.attr("sheight_no_set","1");mTop=cssValue(eNode.css("margin-top"));mBottom=cssValue(eNode.css("margin-bottom"));pTop=cssValue(eNode.css("padding-top"));pBottom=cssValue(eNode.css("padding-bottom"));borderVal=cssValue(eNode.css("border-top-width"))+cssValue(eNode.css("border-bottom-width"));peNode=eNode.parent();if(peNode.find(">")[0]==eNode[0]&&peNode.css("overflow")!="hidden"){parentMTop=cssValue(peNode.css("margin-top"));if(parentMTop>0){mTop=mTop-parentMTop;if(mTop<0){mTop=0;}}}
if(i+1==noListSize){height=lessHeight-mTop-mBottom-pTop-pBottom-borderVal;}else{height=average-mTop-mBottom-pTop-pBottom-borderVal;lessHeight-=average;}
eNode.height(height);}}
function fixAutoHeight(pNode,sn){pNode.find("[sheight]").each(function(){doFixHeight($(this),sn);});}
function activeCss(node){if(!node){return;}
node.find("link").each(function(){var lNode=$(this);if(lNode.attr("type")!="text/css"){return;}
loadCss(lNode.attr("href"));});}
function loadCss(path,reload,id,doc){if(!doc){doc=window.document;}
var hasExist=false;var head=$(doc).find("head");if(id){var node=head.find("#skin_css");if(node.length<1){head.append(['<link rel="stylesheet" type="text/css" media="screen" href="',path,'" id="',id,'"/>'].join(''));}else if(reload){node.attr("href",path);}
return;}
head.find("link").each(function(){var node=$(this);if(node.attr("type")!="text/css"){return;}
if(node.attr("href")==path){if(reload){node.remove();}else{hasExist=true;}}});if(hasExist){return;}
head.append(['<link rel="stylesheet" type="text/css" media="screen" href="',path,'" />'].join(''));}
function activeScript(node){var sList=new ValueList();var deferSList=new ValueList();node.find("script").each(function(){var sNode=$(this);var src=sNode.attr("src");if(!src||src==""){return;}
if(sNode.prop("defer")){deferSList.add(src);}else{sList.add(src);}});for(var i=0;i<sList.size();i++){loadScript(sList.get(i));}
for(var i=0;i<deferSList.size();i++){loadScript(deferSList.get(i));}}
function loadScript(path,afterEvt){jQuery.getScript(path,afterEvt);}
function fixArrayEmpty(arrs){var res={};if(arrs){for(var key in arrs){if(arrs[key]===null||arrs[key]===undefined||arrs[key]===""){continue;}
res[key]=arrs[key];}}
return res;}
function getAbsPosition(node,win,popWin){if(!win){win=window;}
if(!popWin){popWin=win;}
if(!node){return[0,0];}else if(typeof(node)=="string"){node=$(node);if(node.length<1){return[0,0];}}else if(!(node instanceof jQuery)){node=$(node);if(node.length<1){return[0,0];}}
var offset=node.offset();var body=node.parents("body")[0];var res=[offset.left+body.scrollLeft,offset.top+body.scrollTop];if(win!=popWin){for(var i=0;i<win.parent.frames.length;i++){if(win.parent.frames[i]==win){var pRes=getAbsPosition(win.parent.frames[i].frameElement,win.parent,popWin);res[0]=res[0]+pRes[0];res[1]=res[1]+pRes[1];}}}
return res;}
function getScrollLeft(win){if(win==null){return 0;}
var val=win.document.body.scrollLeft;if(win.parent!=null&&win.parent!=win){val+=getScrollLeft(win.parent);}
return val;}
function getScrollTop(win){if(win==null){return 0;}
var val=win.document.body.scrollTop;if(win.parent!=null&&win.parent!=win){val+=getScrollTop(win.parent);}
return val;}
function download(url,post,fileName,readyChangeEvent){var xmlHttp;if(window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}else{xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
if(post&&post!=""){xmlHttp.open("POST",url,true);}else{xmlHttp.open('GET',url,true);}
xmlHttp.setRequestHeader("CONTENT-TYPE","application/x-www-form-urlencoded");xmlHttp.responseType="blob";xmlHttp.onload=function(){if(this.status===200){var blob=this.response;if(window.navigator&&window.navigator.msSaveBlob){window.navigator.msSaveBlob(blob,fileName);}else{var reader=new FileReader();reader.readAsDataURL(blob);reader.onload=function(e){var aNode=document.createElement('a');aNode.download=fileName;aNode.href=e.target.result;$("body").append(aNode);aNode.click();$(aNode).remove();}}}};if(readyChangeEvent){xmlHttp.onreadystatechange=function(){if(xmlHttp.readyState==4){readyChangeEvent();}}}
if(post&&post!=""){xmlHttp.send(post);}else{xmlHttp.send(null);}}
function today(){var d=new Date();var month=d.getMonth()+1;var day=d.getDate();return d.getFullYear()+"-"+(month<10?"0"+month:month)+"-"+(day<10?"0"+day:day);}
function stopBubble(e){if(e&&e.stopPropagation){e.stopPropagation();}else{window.event.cancelBubble=true;}}
function setVar(src,obj){if(src==null){return;}
if(Array.isArray(src)){if(!Array.isArray(obj)){obj=[];}
setArray(src,obj);return obj;}
var keys=Object.keys(src);if(keys.length<1){return src;}
var okeys=obj==null?[]:Object.keys(obj);if(okeys.length<1){obj={};}
setObject(src,obj);return obj;}
function setObject(src,obj){if(src==null||obj==null){return;}
var keys=Object.keys(src);var srcVal;var objVal;for(var i=0;i<keys.length;i++){srcVal=src[keys[i]];if(srcVal==null){continue;}
objVal=obj[keys[i]];if(typeof(srcVal)=="string"){if(objVal==null){obj[keys[i]]=srcVal;}
continue;}
if(Array.isArray(srcVal)){if(objVal==null||!Array.isArray(objVal)){objVal=[];obj[keys[i]]=objVal;}
setArray(srcVal,objVal);}else{var cKeys=null;if(srcVal){try{cKeys=Object.keys(srcVal);}catch(e){}}
if(cKeys==null||cKeys.length<1){if(objVal==null){obj[keys[i]]=srcVal;}}else{if(objVal==null){objVal={};obj[keys[i]]=objVal;}else{var coKey=objVal==null?[]:Object.keys(objVal);if(coKey.length<1){objVal={};obj[keys[i]]=objVal;}}
setObject(srcVal,objVal);}}}}
function setArray(src,obj){if(src==null||obj==null||!Array.isArray(src)||!Array.isArray(obj)){return;}
var keys;var objEle;for(var i=0;i<src.length;i++){if(src[i]==null){continue;}
if(typeof(src[i])=="string"){obj.push(src[i]);continue;}
if(Array.isArray(src[i])){objEle=[];obj.push(objEle);setArray(src[i],objEle);continue;}
keys=src[i]==null?[]:Object.keys(src[i]);if(keys.length<1){obj.push(src[i]);continue;}
objEle={};obj.push(objEle);setObject(src[i],objEle);}}
function topWindow(win){if(win==null){return null;}
if(win==win.parent){return win;}
return topWindow(win.parent);}
function obj2post(obj){var res=[];if(obj){var keys=Object.keys(obj);for(var i=0;i<keys.length;i++){res.push(keys[i]+"="+urlEncodePro(obj[keys[i]]));}}
return res.join('&');}
function urlExist(url){if(!url||url==""){return false;}
var oXmlHttp;if(window.XMLHttpRequest){oXmlHttp=new XMLHttpRequest();}else if(window.ActiveXObject){oXmlHttp=new ActiveXObject("MsXml2.XmlHttp");}
oXmlHttp.open("GET",url,false);oXmlHttp.setRequestHeader("CONTENT-TYPE","application/x-www-form-urlencoded");oXmlHttp.send();source=oXmlHttp.responseText;if(oXmlHttp.status==200&&source&&source!=""){return true;}
return false;}
function optimizeTable(tb,tbArea){if(typeof(tb)=="string"){tb=$("#"+tb);}
if(tb.length<1){return;}
if(!tbArea){tbArea=tb.parent();}
var thead=tb.find("thead");var tbody=tb.find("tbody");if(thead.length<1||tbody.length<1){return;}
var headHeight=thead.outerHeight();var bodyTds=[];var oHeadtrs=[];var fixHeadTr=null;var tdName="th";var tbodyTrTds=tbody.find("tr:first").find("td");var tbodyTdWidths=[];tbodyTrTds.each(function(){var node=$(this);var width=node.width();bodyTds.push(node);tbodyTdWidths.push(parseInt(width)+1);});for(var i=0;i<bodyTds.length;i++){var txt=bodyTds[i].html();bodyTds[i].empty().append(['<div style="width:',tbodyTdWidths[i],'px;">',txt,'</div>'].join(''));}
if(bodyTds.length<1){return;}
var headThs=null;thead.find("tr").each(function(){if(headThs!=null){return;}
var node=$(this);var ths=node.find(tdName);if(ths.length<1){tdName="td";ths=node.find(tdName);}
if(ths.length==bodyTds.length){headThs=ths;fixHeadTr=node;}else{node.hide();oHeadtrs.push(node);}});if(headThs==null){return;}
for(var i=0;i<tbodyTdWidths.length;i++){var node=headThs.eq(i);var txt=node.text();node.empty().append(['<div style="width:',tbodyTdWidths[i],'px;">',txt,'</div>'].join(''));}
if(tbArea==null){tbArea=tb.parent();}else if(!(tbArea instanceof jQuery)){tbArea=$(tbArea);}
if(tbArea.length<1){return;}
tbArea.css("position","relative");var offset=tb.offset();var offsetTop=parseInt(offset.top);thead.css("position","absolute").css("left",offset.left+"px").css("top",offset.top+"px");tb.css("margin-top",(headHeight+4)+"px");offset=tb.offset();offsetTop=parseInt(offset.top);tbArea.on("scroll",function(){var scrollTop;if(this==document){scrollTop=this.documentElement.scrollTop;}else{scrollTop=this.scrollTop;}
if(scrollTop<offsetTop){thead.css("top",(offsetTop-headHeight+1)+"px");}else{thead.css("top",scrollTop+"px");}});thead.css("position","absolute").css("left",offset.left+"px").css("top",(offsetTop-headHeight+1)+"px");tb.css("margin-top",(headHeight+4)+"px");if(fixHeadTr){var fixWidth=fixHeadTr.width();tb.width(fixWidth);var tds;var td;var width;var autoWidthTds;for(var i=0;i<oHeadtrs.length;i++){tds=oHeadtrs[i].find(tdName);autoWidthTds=[];for(var j=0;j<tds.length;j++){td=tds.eq(j);width=cssValue(td.css("width"));if(width>0){fixWidth-=width;}else{autoWidthTds.push(td);}}
if(autoWidthTds.length>1){fixWidth=parseInt(fixWidth/autoWidthTds.length);}
if(fixWidth>30){fixWidth-=30;}
for(var j=0;j<autoWidthTds.length;j++){autoWidthTds[j].width(fixWidth);}
oHeadtrs[i].show();}}
$(window).on("resize",function(){tb.css("margin-top",(headHeight+4)+"px");offset=tb.offset();offsetTop=parseInt(offset.top);thead.css("top",(offsetTop-headHeight+1)+"px");});}
function swap(str,findStr,replaceStr){if(arguments.length<1){return"";}
if(arguments.length<2){return arguments[0];}
var args=[];for(var i=0;i<arguments.length;i++){args.push(arguments[i]);}
if(args.length<3){args.push("");}
var reStr=args[0];var point=2;while(point<args.length){while(reStr.indexOf(args[point-1])!=-1){reStr=reStr.replace(args[point-1],args[point]);}
point+=2;}
return reStr;}
function sleep(delay){var start=(new Date()).getTime();while((new Date()).getTime()-start<delay){continue;}}
function d(){return(new Date()).format("yyyy-MM-dd");}
function dt(){return(new Date()).format("yyyy-MM-dd HH:mm:ss");}
function ts(){return(new Date()).format("yyyy-MM-dd HH:mm:ss SSS");}

